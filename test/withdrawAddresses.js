const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const WithdrawAddresses = artifacts.require('WithdrawAddresses');

describe('WithdrawAddresses', () => {
  const reverter = new Reverter();

  let withdrawAddresses;
  let MAIN;
  let ALIAS;

  before('setup', async () => {
    MAIN = await accounts(1);
    ALIAS = await accounts(2);

    withdrawAddresses = await WithdrawAddresses.new();
    await withdrawAddresses.initialize();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('change', () => {
    it('should change resolving addr', async () => {
      assert.equal(await withdrawAddresses.resolve(MAIN), MAIN);
      await withdrawAddresses.change(ALIAS, { from: MAIN });
      assert.equal(await withdrawAddresses.resolve(MAIN), MAIN);
      await withdrawAddresses.finalize(MAIN, ALIAS);
      assert.equal(await withdrawAddresses.resolve(MAIN), ALIAS);
    });
  });

  describe('finalize', () => {
    it('change should revert', async () => {
      await withdrawAddresses.finalize(MAIN, '0x0000000000000000000000000000000000000000');
      await truffleAssert.reverts(withdrawAddresses.change(ALIAS, { from: MAIN }));
    });
  });

  describe('ownership', () => {
    it('change should revert', async () => {
      await truffleAssert.reverts(withdrawAddresses.finalize(MAIN, ALIAS, { from: MAIN }));
    });
  });
});
