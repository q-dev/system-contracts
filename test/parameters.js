const { accounts } = require('./helpers/utils.js');
const makeProxy = require('./helpers/makeProxy');

const { artifacts } = require('hardhat');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Parameters = artifacts.require('./Constitution.sol');

let ParametersInstance;

describe('Parameters.sol', () => {
  let DEFAULT;
  let PARAMETERS_VOTING;

  before(async () => {
    DEFAULT = await accounts(0);
    PARAMETERS_VOTING = await accounts(1);

    const registry = await ContractRegistry.new();
    await registry.initialize([DEFAULT], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    const parametersInitialList = [
      'qTokenHoldersTransactionFeeShare',
      'qTokenHoldersNativeAppsFeeShare',
      'qTokenHoldersCoinbaseSubsidyShare',
      'qidHoldersTransactionFeeShare',
      'qidHoldersNativeAppsFeeShare',
      'qidHoldersCoinbaseSubsidyShare',
      'validatorNodesTransactionFeeShare',
      'validatorNodesNativeAppsFeeShare',
      'validatorNodesCoinbaseSubsidyShare',
      'rootNodesTransactionFeeShare',
      'rootNodesNativeAppsFeeShare',
      'rootNodesCoinbaseSubsidyShare',
      'expertLimit',
      'maxNRootNodes',
      'rootWithdrawTime',
      'constitution.expertPanels.maxNExperts',
      'constitution.expertPanels.changeParamVP',
      'constitution.expertPanels.changeParamRNVALPRD',
      'constitution.expertPanels.changeParamRMAJ',
    ];
    const parametersValues = [
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      333,
      100, // expert panels
      100,
      100,
      100,
    ];

    ParametersInstance = await makeProxy(registry.address, Parameters);
    await ParametersInstance.initialize(registry.address, [], [], [], [], [], [], [], []);

    for (let i = 0; i < parametersInitialList.length; ++i) {
      await ParametersInstance.setUint(parametersInitialList[i], parametersValues[i], { from: PARAMETERS_VOTING });
    }
  });

  it('Should create new uint parameter', async () => {
    await ParametersInstance.setUint('testUintParameter', 10, { from: PARAMETERS_VOTING });
  });

  it('Should get uint parameter', async () => {
    assert.equal(await ParametersInstance.getUint('testUintParameter'), 10, { from: PARAMETERS_VOTING });
  });

  it('Should get uint parameter keys', async () => {
    const parametersUintList = [
      'qTokenHoldersTransactionFeeShare',
      'qTokenHoldersNativeAppsFeeShare',
      'qTokenHoldersCoinbaseSubsidyShare',
      'qidHoldersTransactionFeeShare',
      'qidHoldersNativeAppsFeeShare',
      'qidHoldersCoinbaseSubsidyShare',
      'validatorNodesTransactionFeeShare',
      'validatorNodesNativeAppsFeeShare',
      'validatorNodesCoinbaseSubsidyShare',
      'rootNodesTransactionFeeShare',
      'rootNodesNativeAppsFeeShare',
      'rootNodesCoinbaseSubsidyShare',
      'expertLimit',
      'maxNRootNodes',
      'rootWithdrawTime',
      'constitution.expertPanels.maxNExperts',
      'constitution.expertPanels.changeParamVP',
      'constitution.expertPanels.changeParamRNVALPRD',
      'constitution.expertPanels.changeParamRMAJ',
      'testUintParameter',
    ];
    assert.equal(JSON.stringify(await ParametersInstance.getUintKeys()), JSON.stringify(parametersUintList));
  });

  it('Should create new address parameter', async () => {
    await ParametersInstance.setAddr('testAddressParameter', DEFAULT, { from: PARAMETERS_VOTING });
  });

  it('Should get address parameter', async () => {
    assert.equal(await ParametersInstance.getAddr('testAddressParameter'), DEFAULT);
  });

  it('Should get address parameter keys', async () => {
    assert.equal(JSON.stringify(await ParametersInstance.getAddrKeys()), JSON.stringify(['testAddressParameter']));
  });

  it('Should create new string parameter', async () => {
    await ParametersInstance.setString('testStringParameter', 'test', { from: PARAMETERS_VOTING });
  });

  it('Should get string parameter', async () => {
    assert.equal(await ParametersInstance.getString('testStringParameter'), 'test');
  });

  it('Should get string parameter keys', async () => {
    assert.equal(JSON.stringify(await ParametersInstance.getStringKeys()), JSON.stringify(['testStringParameter']));
  });

  it('Should create new bytes32 parameter', async () => {
    await ParametersInstance.setBytes32('testBytes32Parameter', web3.utils.fromAscii('test'), {
      from: PARAMETERS_VOTING,
    });
  });

  it('Should get bytes32 parameter', async () => {
    assert.equal(
      web3.utils.toAscii(await ParametersInstance.getBytes32('testBytes32Parameter')).replace(/\0/g, ''),
      'test'
    );
  });

  it('Should get bytes32 parameter keys', async () => {
    assert.equal(JSON.stringify(await ParametersInstance.getBytes32Keys()), JSON.stringify(['testBytes32Parameter']));
  });

  it('Should create new bool parameter', async () => {
    await ParametersInstance.setBool('testBoolParameter', true, { from: PARAMETERS_VOTING });
  });

  it('Should get bool parameter', async () => {
    assert.equal(await ParametersInstance.getBool('testBoolParameter'), true);
  });

  it('Should get bool parameter keys', async () => {
    assert.equal(JSON.stringify(await ParametersInstance.getBoolKeys()), JSON.stringify(['testBoolParameter']));
  });
});
