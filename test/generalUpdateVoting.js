const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');
const { approximateAssert, lockWeight } = require('./helpers/votingHelper');

const { artifacts } = require('hardhat');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting.sol');
const QVault = artifacts.require('./QVault');
const Parameters = artifacts.require('./Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Roots = artifacts.require('./Roots');
const VotingWeightProxy = artifacts.require('VotingWeightProxy');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

const DelegationStatus = Object.freeze({
  SELF: 0,
  DELEGATED: 1,
  PENDING: 2,
});

const CONSTITUTION_DECIMAL = toBN(10).pow(27);

describe('GeneralUpdateVoting', () => {
  const reverter = new Reverter();

  let crKeeperFactory;
  let generalUpdateVote;
  let qVault;
  let parameters;
  let registry;
  let roots;
  let votingWeightProxy;

  const remark = 'https://example.com';

  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;

  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const proposalExecutionP = 2592000;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(3);
    PARAMETERS_VOTING = await accounts(2);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    parameters = await makeProxy(registry.address, Parameters);
    await parameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await parameters.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month

    await registry.setAddress('governance.constitution.parameters', parameters.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [OWNER, USER1]);
    await registry.setAddress('governance.rootNodes', roots.address);

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['governance.rootNodes', 'tokeneconomics.qVault'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    const proposalCounter = 0;

    it('should be possible to createProposal', async () => {
      const result = await generalUpdateVote.createProposal(remark);

      assert.equal(
        (await generalUpdateVote.proposalCount()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await generalUpdateVote.proposals(0)).remark.toString(), remark, 'set correct url');

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });

    it('should create a proposal with the correct values', async () => {
      const votingStartTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(votingStartTime);
      await generalUpdateVote.createProposal(remark);

      const resultBase = await generalUpdateVote.proposals(proposalCounter);
      assert.equal((await generalUpdateVote.proposalCount()).toString(), 1);
      assert.equal(resultBase.params.votingStartTime, votingStartTime);
      assert.equal(resultBase.params.votingEndTime, votingPeriod + votingStartTime);
      assert.equal(resultBase.params.vetoEndTime, vetoPeriod + votingPeriod + votingStartTime);
      assert.equal(resultBase.params.requiredMajority, requiredMajority.toNumber());
      assert.equal(resultBase.params.requiredQuorum, requiredQuorum);
    });

    it('should get PENDING status after createProposal', async () => {
      await generalUpdateVote.createProposal(remark);
      assert.equal(await generalUpdateVote.getStatus(proposalCounter), ProposalStatus.PENDING, 'Not pending');
    });
  });

  describe('vote', () => {
    const proposalID = 0;

    const amount = toBN(10 ** 18);

    beforeEach(async () => {
      await generalUpdateVote.createProposal(remark);

      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.deposit({ value: amount, from: USER1 });
      await qVault.lock(amount, { from: OWNER });
      await qVault.lock(amount, { from: USER1 });
    });

    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      const result = await generalUpdateVote.voteFor(proposalID);

      assert.equal((await generalUpdateVote.proposals(proposalID)).counters.weightFor.toString(), amount.toString());
      assert.isTrue(await generalUpdateVote.hasUserVoted(proposalID, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
      assert.equal(result.logs[0].args._amount, amount.toString());
    });

    it('should not be possible to vote for not pending proposal', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.voteAgainst(1), reason);

      assert.equal((await generalUpdateVote.proposals(1)).counters.weightAgainst.toString(), 0);
      assert.equal((await generalUpdateVote.proposals(1)).counters.weightFor.toString(), 0);
      assert.isFalse(await generalUpdateVote.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote with zero total voting weight', async () => {
      const reason = '[QEC-029003]-The total voting weight must be greater than zero, failed to vote.';
      await truffleAssert.reverts(generalUpdateVote.voteFor(0, { from: USER2 }), reason);

      assert.equal((await generalUpdateVote.proposals(proposalID)).counters.weightAgainst.toString(), 0);
      assert.equal((await generalUpdateVote.proposals(proposalID)).counters.weightFor.toString(), 0);
      assert.isFalse(await generalUpdateVote.hasUserVoted(proposalID, USER2));
    });

    it('should not be possible to vote if already voted', async () => {
      await generalUpdateVote.voteAgainst(0);

      assert.equal((await generalUpdateVote.proposals(0)).counters.weightAgainst.toString(), amount.toString());
      assert.equal((await generalUpdateVote.proposals(0)).counters.weightFor.toString(), 0);
      assert.isTrue(await generalUpdateVote.hasUserVoted(proposalID, OWNER));

      const reason = '[QEC-029002]-The caller has already voted for the proposal.';
      await truffleAssert.reverts(generalUpdateVote.voteAgainst(0), reason);

      assert.equal((await generalUpdateVote.proposals(0)).counters.weightAgainst.toString(), amount.toString());
      assert.equal((await generalUpdateVote.proposals(0)).counters.weightFor.toString(), 0);
      assert.isTrue(await generalUpdateVote.hasUserVoted(proposalID, OWNER));
    });
  });

  describe('veto', () => {
    const proposalID = 0;
    const voteEndTime = votingPeriod + 1;

    const amount = toBN(10 ** 18);

    beforeEach(async () => {
      await generalUpdateVote.createProposal(remark);

      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await generalUpdateVote.voteFor(proposalID);
    });

    it('should veto on proposal', async () => {
      await setTime((await getCurrentBlockTime()) + voteEndTime);

      assert.equal((await generalUpdateVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);
      await generalUpdateVote.veto(proposalID);

      const proposal = await generalUpdateVote.proposals(proposalID);

      assert.equal(proposal.counters.vetosCount, 1);

      assert.isTrue(await generalUpdateVote.hasRootVetoed(proposalID, OWNER));
    });

    it('should get exception, sender already put veto on this proposal', async () => {
      const reason = '[QEC-029005]-The sender has already vetoed this proposal, veto failed.';

      await setTime((await getCurrentBlockTime()) + voteEndTime);

      assert.equal((await generalUpdateVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);

      await generalUpdateVote.veto(proposalID);

      const proposal = await generalUpdateVote.proposals(proposalID);

      assert.equal(proposal.counters.vetosCount, 1);
      assert.equal((await generalUpdateVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);

      assert.isTrue(await generalUpdateVote.hasRootVetoed(proposalID, OWNER));

      await truffleAssert.reverts(generalUpdateVote.veto(proposalID), reason);
    });

    it('should get exception, invalid proposal status', async () => {
      const reason = '[QEC-029004]-Proposal status must be accepted, veto failed.';
      await truffleAssert.reverts(generalUpdateVote.veto(proposalID), reason);
    });

    it('should get rejected status, all roots vetoed proposal', async () => {
      await setTime((await getCurrentBlockTime()) + voteEndTime);
      await generalUpdateVote.veto(proposalID);
      await generalUpdateVote.veto(proposalID, { from: USER1 });

      const proposal = await generalUpdateVote.proposals(proposalID);
      assert.equal((await generalUpdateVote.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
      assert.equal(proposal.counters.vetosCount, 2);
    });

    it('should get exception, non exist proposal', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.veto(1), reason);
    });

    it('should get exception, call by non root', async () => {
      const reason = '[QEC-029007]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(generalUpdateVote.veto(proposalID, { from: USER2 }), reason);
    });
  });

  describe('getStatus', () => {
    const voteEndTime = votingPeriod + 1;
    const amount = toBN(10 ** 18);
    const lessAmount = amount.dividedBy(4);

    beforeEach('status setup', async () => {
      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await qVault.deposit({ value: lessAmount, from: USER1 });
      await qVault.lock(lessAmount, { from: USER1 });
    });

    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0);

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of votingParams results proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteAgainst(0, { from: USER1 });

      assert.equal((await generalUpdateVote.proposals(0)).counters.weightAgainst.toString(), lessAmount.toString());

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.REJECTED);
    }).retries(3);

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await parameters.setUint('constitution.voting.changeQnotConstQRM', getPercentageFormat(50), {
        from: PARAMETERS_VOTING,
      });
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0);

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return ACCEPTED status on a accepted because of votingParams results proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0, { from: USER1 });
      await generalUpdateVote.voteFor(0, { from: OWNER });

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.ACCEPTED);
    });

    it('should return PASSED status on a passed because of votingParams results proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0);
      await generalUpdateVote.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      assert.equal(
        toBN((await generalUpdateVote.proposals(0)).counters.weightAgainst).toString(),
        lessAmount.toString()
      );

      await setTime((await getCurrentBlockTime()) + voteEndTime * 2);
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXECUTED status on a executed proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0);
      await generalUpdateVote.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      assert.equal(
        toBN((await generalUpdateVote.proposals(0)).counters.weightAgainst).toString(),
        lessAmount.toString()
      );

      await setTime((await getCurrentBlockTime()) + voteEndTime * 2);
      await generalUpdateVote.execute(0);

      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.EXECUTED);
    });

    it('should return EXPIRED status on a exired proosal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0);
      await generalUpdateVote.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      const vetoEndTime = toBN((await generalUpdateVote.proposals(0)).params.vetoEndTime).toNumber();
      await setTime(vetoEndTime + proposalExecutionP + 1);

      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });
  });

  describe('execute', () => {
    const voteEndTime = votingPeriod + 1;
    const amount = toBN(10 ** 18);
    const lessAmount = amount.dividedBy(4);
    const proposalID = 0;

    beforeEach('status setup', async () => {
      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await qVault.deposit({ value: lessAmount, from: USER1 });
      await qVault.lock(lessAmount, { from: USER1 });
    });

    it('should successfully execute a PASSED proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(proposalID);

      assert.equal((await generalUpdateVote.proposals(proposalID)).counters.weightFor.toString(), amount.toString());
      assert.equal((await generalUpdateVote.proposals(proposalID)).counters.weightAgainst.toString(), 0);

      await setTime((await getCurrentBlockTime()) + voteEndTime * 2);

      let status = (await generalUpdateVote.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      const txReceipt = await generalUpdateVote.execute(proposalID, { from: USER2 });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalID);

      status = (await generalUpdateVote.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');
    });

    it('should not be possible to execute EXPIRED proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      await generalUpdateVote.voteFor(0, { from: OWNER });
      await generalUpdateVote.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), amount.toString());

      const vetoEndTime = toBN((await generalUpdateVote.proposals(0)).params.vetoEndTime).toNumber();
      await setTime(vetoEndTime + proposalExecutionP + 1);

      const reason = '[QEC-029000]-Proposal must be PASSED before excecuting.';
      await truffleAssert.reverts(generalUpdateVote.execute(0), reason);

      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should only be able to execute a PASSED proposal', async () => {
      await generalUpdateVote.createProposal(remark);

      const reason = '[QEC-029000]-Proposal must be PASSED before excecuting.';
      await truffleAssert.reverts(generalUpdateVote.execute(proposalID), reason);

      assert.equal((await generalUpdateVote.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });
  });

  describe('test getters', () => {
    const proposalID = 0;
    const amount = toBN(10 ** 18);

    beforeEach('init proposal', async () => {
      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await generalUpdateVote.createProposal(remark);
    });

    it('should successfuly return votes weight "FOR"', async () => {
      await generalUpdateVote.voteFor(proposalID);
      assert.equal((await generalUpdateVote.getVotesFor(proposalID)).toString(), amount.toString());
    });

    it('should successfuly return votes weight "AGAINST"', async () => {
      await generalUpdateVote.voteAgainst(proposalID);
      assert.equal((await generalUpdateVote.getVotesAgainst(proposalID)).toString(), amount.toString());
    });
  });

  describe('getVetosNumber', () => {
    const voteEndTime = votingPeriod + 1;
    const amount = toBN(10).pow(18);
    const proposalID = 0;

    it('should return correct vetos number', async () => {
      await generalUpdateVote.createProposal(remark);

      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await generalUpdateVote.voteFor(proposalID);

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      await generalUpdateVote.veto(proposalID);
      await generalUpdateVote.veto(proposalID, { from: USER1 });

      const proposal = await generalUpdateVote.proposals(proposalID);
      assert.equal(proposal.counters.vetosCount, await generalUpdateVote.getVetosNumber(proposalID));
    });

    it('should get exceprion, proposal does not exist', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.getVetosNumber(1), reason);
    });
  });

  describe('getVetosPercentage', () => {
    const voteEndTime = votingPeriod + 1;
    const amount = toBN(10).pow(18);
    const proposalID = 0;

    it('should return correct vetos percentage', async () => {
      await generalUpdateVote.createProposal(remark);

      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await generalUpdateVote.voteFor(proposalID);

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      await generalUpdateVote.veto(proposalID);

      const proposal = await generalUpdateVote.proposals(proposalID);
      const vetosCount = proposal.counters.vetosCount;
      const expectedVetoPercentage = toBN(vetosCount).times(CONSTITUTION_DECIMAL).dividedBy(2);
      assert.equal(expectedVetoPercentage.toNumber(), await generalUpdateVote.getVetosPercentage(proposalID));
    });

    it('should get exceprion, proposal does not exist', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.getVetosPercentage(1), reason);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const prop = await generalUpdateVote.createProposal(remark);
      const propID = Number(prop.logs[0].args._id);
      const propBase = await generalUpdateVote.proposals.call(propID);

      const expectedBase = await generalUpdateVote.getProposal(propID);

      assert.equal(expectedBase.remark, propBase.remark);
      assert.equal(expectedBase.executed, propBase.executed);
      assert.equal(expectedBase.params.votingEndTime, propBase.params.votingEndTime);
    });

    it('should get exceprion, proposal does not exist', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.getProposal(1), reason);
    });
  });

  describe('getProposalStats', () => {
    const amount = toBN(10 ** 18);

    before('status setup', async () => {
      await qVault.deposit({ value: amount, from: OWNER });
      await qVault.lock(amount, { from: OWNER });

      await qVault.deposit({ value: amount, from: USER2 });
      await qVault.lock(amount, { from: USER2 });
    });

    it('should return correct voting stats structure', async () => {
      const result = await generalUpdateVote.createProposal(remark);
      const propID = Number(result.logs[0].args._id);

      await generalUpdateVote.voteFor(0, { from: OWNER });
      await generalUpdateVote.voteFor(0, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      assert.equal((await generalUpdateVote.getStatus(0)).toString(), ProposalStatus.ACCEPTED);

      const blockNumber = (await generalUpdateVote.veto(0, { from: USER1 })).receipt.blockNumber;
      const propStats = await generalUpdateVote.getProposalStats(propID);
      const prop = await generalUpdateVote.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.params.requiredQuorum);

      const totalWeight = toBN(prop.counters.weightFor).plus(prop.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.counters.weightFor, totalWeight);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);

      assert.isTrue(approximateAssert(totalWeight, blockNumber, propStats.currentQuorum));

      const vetosCount = prop.counters.vetosCount;
      const expectedVetoPercentage = toBN(vetosCount).times(CONSTITUTION_DECIMAL).dividedBy(2);
      assert.equal(propStats.currentVetoPercentage, expectedVetoPercentage.toNumber());
    });

    it('should get exceprion, proposal does not exist', async () => {
      const reason = '[QEC-029006]-Proposal does not exist.';
      await truffleAssert.reverts(generalUpdateVote.getProposalStats(1), reason);
    });
  });

  describe('getVotingWeightInfo()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await generalUpdateVote.createProposal(remark);
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully return the voting weight info', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await generalUpdateVote.voteFor(proposalID, { from: USER1 });

      const votingWeightInfo = await generalUpdateVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.ownWeight, lockedWeightForUser.toString());
      assert.equal(votingWeightInfo.base.votingAgent, USER1);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.SELF);
      assert.equal(votingWeightInfo.base.lockedUntil, await votingWeightProxy.getLockedUntil(USER1));
      assert.equal(votingWeightInfo.hasAlreadyVoted, true);
      assert.equal(votingWeightInfo.canVote, false);
    });

    it('should return PENDING status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });

      const votingWeightInfo = await generalUpdateVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.PENDING);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
    });

    it('should return DELEGATED status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });
      const delegationInfo = await votingWeightProxy.delegationInfos(USER1);
      await setTime((await getCurrentBlockTime()) + toBN(delegationInfo.votingAgentPassOverTime).plus(1).toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER1 });

      await generalUpdateVote.createProposal('remark');
      await generalUpdateVote.voteFor(0, { from: USER2 });

      const votingWeightInfo = await generalUpdateVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.DELEGATED);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER2)).toString());
    });

    it('should revert if the slashing proposal does not exist', async () => {
      await truffleAssert.reverts(
        generalUpdateVote.getVotingWeightInfo(10001, { from: USER1 }),
        '[QEC-029006]-Proposal does not exist.'
      );
    });
  });
});
