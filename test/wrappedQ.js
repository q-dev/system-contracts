const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const { getBalance, toBN } = require('./helpers/utils');

const WQ = artifacts.require('WQ');
const OnReceiveRevertMock = artifacts.require('OnReceiveRevertMock');
describe('WQ', () => {
  const reverter = new Reverter();

  let USER1;
  let USER2;

  let wq;
  let onReceiveRevertMock;

  const ether = toBN(10).pow(18);

  before('setup', async () => {
    USER1 = await accounts(0);
    USER2 = await accounts(1);

    wq = await WQ.new();
    onReceiveRevertMock = await OnReceiveRevertMock.new();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('deposit', () => {
    it('should deposit', async () => {
      await wq.deposit({ from: USER1, value: 1000 });
      assert.equal(await wq.balanceOf(USER1), 1000);

      await web3.eth.sendTransaction({
        from: USER1,
        to: wq.address,
        value: 1000,
      });
      assert.equal(await wq.balanceOf(USER1), 2000);

      await truffleAssert.reverts(wq.deposit({ from: USER1, value: 0 }), 'WQ: Zero deposit amount');
    });

    it('should deposit to Other user', async () => {
      await wq.depositTo(USER2, { from: USER1, value: 1000 });
      assert.equal(await wq.balanceOf(USER2), 1000);
      assert.equal(await wq.balanceOf(USER1), 0);
    });
  });

  describe('withdraw', () => {
    it('should withdraw', async () => {
      const initialBalance = toBN(await getBalance(USER1));

      await wq.deposit({ from: USER1, value: ether });
      await wq.withdraw(ether.div(2), { from: USER1 });

      assert.equal(toBN(await wq.balanceOf(USER1)).toString(), ether.div(2).toString());
      assert.approximately(
        toBN(await getBalance(USER1)).toNumber(),
        initialBalance.minus(ether.div(2)).toNumber(),
        100000
      );

      await truffleAssert.reverts(wq.withdraw(0, { from: USER1 }), 'WQ: Zero withdraw amount');
    });

    it('should withdraw to Other user', async () => {
      const initialBalance = toBN(await getBalance(USER2));

      await wq.deposit({ from: USER1, value: ether });
      await wq.withdrawTo(USER2, ether.div(2), { from: USER1 });

      assert.equal(toBN(await wq.balanceOf(USER2)).toString(), '0');
      assert.equal(toBN(await wq.balanceOf(USER1)).toString(), ether.div(2).toString());

      assert.approximately(
        toBN(await getBalance(USER2)).toNumber(),
        initialBalance.plus(ether.div(2)).toNumber(),
        100000
      );
    });

    it('should revert if amount was not withdrawn', async () => {
      await wq.deposit({ from: USER1, value: ether });

      await truffleAssert.reverts(
        wq.withdrawTo(onReceiveRevertMock.address, ether.div(2), { from: USER1 }),
        'WQ: Failed to transfer.'
      );
    });
  });
});
