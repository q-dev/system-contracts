const { accounts } = require('../helpers/utils.js');
const truffleAssert = require('truffle-assertions');
const Reverter = require('../helpers/reverter');

const { artifacts } = require('hardhat');
const Paymaster = artifacts.require('GSNPaymaster');
const StableCoin = artifacts.require('StableCoin');
const ContractRegistry = artifacts.require('ContractRegistry');

describe('Paymaster', () => {
  const reverter = new Reverter();

  let paymaster;
  let registry;
  let stableCoin;
  let forwarder;

  let SOMEBODY;

  before('setup', async () => {
    SOMEBODY = await accounts(1);

    registry = await ContractRegistry.new();
    forwarder = registry;
    stableCoin = await StableCoin.new(registry.address, 'QUSD', 'QUSD', [], forwarder.address);
    paymaster = await Paymaster.new(stableCoin.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('Whitelist', () => {
    it('addToWhitelist should be accessed only for owner', async () => {
      await paymaster.addToWhitelist([]);
      await truffleAssert.reverts(paymaster.addToWhitelist([], { from: SOMEBODY }));
    });

    it('dropFromWhitelist should be accessed only for owner', async () => {
      await paymaster.dropFromWhitelist([]);
      await truffleAssert.reverts(paymaster.dropFromWhitelist([], { from: SOMEBODY }));
    });

    it('addToWhitelist should emit event', async () => {
      truffleAssert.eventEmitted(await paymaster.addToWhitelist([SOMEBODY]), 'AddressAddedToWhitelist');
      truffleAssert.eventNotEmitted(await paymaster.addToWhitelist([SOMEBODY]), 'AddressAddedToWhitelist');
    });

    it('dropFromWhitelist should emit event', async () => {
      truffleAssert.eventEmitted(await paymaster.addToWhitelist([SOMEBODY]), 'AddressAddedToWhitelist');

      truffleAssert.eventEmitted(await paymaster.dropFromWhitelist([SOMEBODY]), 'AddressDroppedFromWhitelist');
      truffleAssert.eventNotEmitted(await paymaster.dropFromWhitelist([SOMEBODY]), 'AddressDroppedFromWhitelist');
    });
  });

  describe('RelayCall', () => {
    it('preRelay call should be accessable only for senders in whitelist', async () => {
      const relayRequest = {
        request: {
          from: SOMEBODY,
          to: stableCoin.address,
          value: 123, // dummy value
          gas: 5000, // dummy value
          nonce: 123, // dummy value
          data: web3.utils.asciiToHex(''),
          validUntil: 123123, // dummy value
        },
        relayData: {
          // dummy value
          gasPrice: 256,
          pctRelayFee: 256,
          baseRelayFee: 256,
          paymaster: paymaster.address,
          relayWorker: registry.address, // dummy value
          forwarder: forwarder.address, // dummy value
          paymasterData: web3.utils.asciiToHex(''),
          clientId: 2345,
        },
      };
      await truffleAssert.reverts(
        paymaster.preRelayedCall(relayRequest, web3.utils.asciiToHex(''), web3.utils.asciiToHex(''), 5000), // dummy value
        'Sender is not in Whitelist'
      );
      await paymaster.addToWhitelist([SOMEBODY]);

      await truffleAssert.reverts(
        paymaster.preRelayedCall(relayRequest, web3.utils.asciiToHex(''), web3.utils.asciiToHex(''), 5000), // dummy value
        'Forwarder is not trusted'
      );
    });

    it('preRelay call should be accessable only for 1 stableCoinInstance', async () => {
      const relayRequest = {
        request: {
          from: SOMEBODY,
          to: (await StableCoin.new(registry.address, 'Q', 'Q', [], forwarder.address)).address,
          value: 123, // dummy value
          gas: 5000, // dummy value
          nonce: 123, // dummy value
          data: web3.utils.asciiToHex(''),
          validUntil: 123123, // dummy value
        },
        relayData: {
          // dummy value
          gasPrice: 256,
          pctRelayFee: 256,
          baseRelayFee: 256,
          paymaster: paymaster.address,
          relayWorker: registry.address, // dummy value
          forwarder: forwarder.address, // dummy value
          paymasterData: web3.utils.asciiToHex(''),
          clientId: 2345,
        },
      };

      await paymaster.addToWhitelist([SOMEBODY]);
      await truffleAssert.reverts(
        paymaster.preRelayedCall(relayRequest, web3.utils.asciiToHex(''), web3.utils.asciiToHex(''), 5000), // dummy value
        'Wrong target'
      );
    });
  });
});
