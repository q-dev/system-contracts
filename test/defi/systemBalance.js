const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');

const { toBN } = require('../helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const ERC677 = artifacts.require('ERC677Mock');
const StableToken = artifacts.require('StableCoinMock');
const SystemBalance = artifacts.require('SystemBalance');
const SystemReserve = artifacts.require('SystemReserve');
const LiquidationAuction = artifacts.require('LiquidationAuction');
const Saving = artifacts.require('Saving');
const SystemDebtAuction = artifacts.require('SystemDebtAuction');
const ParametersEPQFI = artifacts.require('./EPQFI_Parameters');

const AuctionStatus = {
  NONE: 0,
  ACTIVE: 1,
  CLOSED: 2,
};

describe('Defi:SystemBalance', () => {
  const reverter = new Reverter();

  let qethToken;
  let expertsParameters;
  let registry;
  let stableQUSDToken;
  let systemBalance;
  let systemReserve;
  let liquidationAuction;
  let saving;
  let systemDebtAuction;
  let crKeeperFactory;

  const SURPLUS_THRESHOLD = toBN(10).pow(18).times(100);
  const DEBT_THRESHOLD = toBN(10).pow(21);
  const RESERVE_LOT = toBN(10).pow(19);
  const SURPLUS_LOT = toBN(10).pow(19);
  const DEBT_AUCTION_PERIOD = toBN(100);

  let OWNER;
  let USER1;
  let PARAMETERS_VOTING;
  let LIQUIDATION_AUCTION;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    PARAMETERS_VOTING = await accounts(2);
    LIQUIDATION_AUCTION = await accounts(3);

    // never return exponential notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    qethToken = await ERC677.new('qeth', 'qeth');

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], forwarder);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await expertsParameters.setAddr('governed.EPDR.WETH_address', qethToken.address, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_surplusThreshold', SURPLUS_THRESHOLD, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QUSD_debtThreshold', DEBT_THRESHOLD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_surplusLot', SURPLUS_LOT, { from: PARAMETERS_VOTING });

    liquidationAuction = await makeProxy(registry.address, LiquidationAuction);
    await liquidationAuction.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.liquidationAuction', liquidationAuction.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);

    saving = await makeProxy(registry.address, Saving);
    await saving.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.saving', saving.address);

    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    await registry.setAddress('defi.QUSD.liquidationAuction', LIQUIDATION_AUCTION);

    const parametersInitialList = ['governed.EPQFI.reserveCoolDownThreshold', 'governed.EPQFI.reserveCoolDownP'];
    const parametersValues = [toBN(10000 * 10 ** 18), toBN(604800)];

    const epqfiParameters = await makeProxy(registry.address, ParametersEPQFI);
    await epqfiParameters.initialize(registry.address, parametersInitialList, parametersValues, [], [], [], [], [], []);

    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParameters.address);

    systemReserve = await makeProxy(registry.address, SystemReserve);
    await systemReserve.initialize(registry.address, ['defi.QUSD.systemDebtAuction']);

    systemDebtAuction = await makeProxy(registry.address, SystemDebtAuction);
    await systemDebtAuction.initialize(registry.address, 'QUSD');

    await expertsParameters.setUint('governed.EPDR.debtAuctionP', DEBT_AUCTION_PERIOD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.reserveLot', RESERVE_LOT, { from: PARAMETERS_VOTING });

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');

    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);
    await registry.setAddress('tokeneconomics.systemReserve', systemReserve.address);
    await registry.setAddress('defi.QUSD.systemDebtAuction', systemDebtAuction.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('Surplus handling', () => {
    it('should be possible to get correct Surplus amount', async () => {
      const amountOfSurplus = toBN(500000000000000000);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);

      assert.equal((await systemBalance.getSurplus()).toString(), amountOfSurplus.toString());
    });
  });

  describe('Debt handling', () => {
    it('should be possible to increase Debt', async () => {
      const debtAmount = toBN(500000000000000000);

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });
      assert.equal((await systemBalance.getDebt()).toString(), debtAmount.toString());
    });

    it('only the eligible contracts can increase system debt', async () => {
      const debtAmount = toBN(500000000000000000);
      const debtBeforeIncreasing = toBN(await systemBalance.getDebt());

      await truffleAssert.reverts(
        systemBalance.increaseDebt(debtAmount),
        '[QEC-023002]-Permission denied - only the Liquidation auction and Saving contracts have access.'
      );

      const debtAfterIncreasing = toBN(await systemBalance.getDebt());
      assert.equal(debtAfterIncreasing.toString(), debtBeforeIncreasing.toString());
    });
  });

  describe('Balance handling', () => {
    it('should be possible to get correct Balance amount', async () => {
      const amountOfSurplus = toBN(500000000000000000);
      const debtAmount = toBN(500000000000000000);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);
      assert.equal((await systemBalance.getSurplus()).toString(), amountOfSurplus.toString());

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });

      assert.equal(
        (await systemBalance.getBalance.call()).toString(),
        toBN(amountOfSurplus.minus(debtAmount)).toString()
      );
    });

    it('should recalculate Balance amount', async () => {
      const debtAmount = toBN(1000000000000000);
      const amountOfSurplus = toBN(500000000000000000);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);
      assert.equal((await systemBalance.getSurplus()).toString(), amountOfSurplus.toString());

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });

      assert.equal(
        (await systemBalance.getBalance.call()).toString(),
        toBN(amountOfSurplus.minus(debtAmount)).toString()
      );

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });
      assert.equal(
        (await systemBalance.getBalance.call()).toString(),
        toBN(amountOfSurplus.minus(debtAmount.multipliedBy(2))).toString()
      );
    });
    it('should return balance below zero', async () => {
      const amountOfSurplus = toBN(500000000000000000);
      const debtAmount = toBN(600000000000000000);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);
      assert.equal((await systemBalance.getSurplus()).toString(), amountOfSurplus.toString());

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });

      assert.equal(
        (await systemBalance.getBalance.call()).toString(),
        toBN(amountOfSurplus.minus(debtAmount)).toString()
      );
    });
  });

  describe('Netting', () => {
    it('should be perform netting correctly', async () => {
      const debtAmount = toBN(1000000000000000);
      const amountOfSurplus = toBN(500000000000000000);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);
      assert.equal((await systemBalance.getSurplus()).toString(), amountOfSurplus.toString());

      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });

      assert.equal(
        (await systemBalance.getBalance.call()).toString(),
        toBN(amountOfSurplus.minus(debtAmount)).toString()
      );

      await systemBalance.performNetting();

      assert.equal((await systemBalance.getDebt()).toString(), toBN(0).toString());
      assert.equal((await systemBalance.getSurplus()).toString(), toBN(amountOfSurplus.minus(debtAmount)).toString());
    });
  });

  describe('getBalanceDetails', () => {
    it('should return correct values and isAuctionPossible is false', async () => {
      const debtAmount = toBN(10).pow(18);
      const amountOfSurplus = toBN(10).pow(18).times(5);

      await stableQUSDToken.transfer(systemBalance.address, amountOfSurplus);
      await systemBalance.increaseDebt(debtAmount, { from: LIQUIDATION_AUCTION });
      const systemBalanceDetails = await systemBalance.getBalanceDetails();

      assert.equal(toBN(systemBalanceDetails.currentDebt).toString(), debtAmount.toString());
      assert.equal(toBN(systemBalanceDetails.currentSurplus).toString(), amountOfSurplus.toString());
      assert.equal(toBN(systemBalanceDetails.debtThreshold).toString(), DEBT_THRESHOLD.toString());
      assert.equal(toBN(systemBalanceDetails.surplusThreshold).toString(), SURPLUS_THRESHOLD.toString());
      assert.equal(systemBalanceDetails.isDebtAuctionPossible, false);
      assert.equal(systemBalanceDetails.isSurplusAuctionPossible, false);
    });

    it('should return isAuctionPossible false when the current surplus is below auction lot', async () => {
      await expertsParameters.setUint('governed.EPDR.QUSD_surplusLot', SURPLUS_THRESHOLD.plus(1), {
        from: PARAMETERS_VOTING,
      });
      await stableQUSDToken.transfer(systemBalance.address, SURPLUS_THRESHOLD);

      const systemBalanceDetails = await systemBalance.getBalanceDetails();
      assert.equal(
        systemBalanceDetails.isSurplusAuctionPossible,
        false,
        'Surplus is above threshold, but not enough for auction lot'
      );
    });

    it('should return isAuctionPossible true when the current debt is above the threshold', async () => {
      await systemBalance.increaseDebt(DEBT_THRESHOLD + 1, { from: LIQUIDATION_AUCTION });

      const systemBalanceDetails = await systemBalance.getBalanceDetails();

      assert.equal(
        systemBalanceDetails.isDebtAuctionPossible,
        true,
        'auction must be possible if debt after netting > threshold'
      );
    });

    it('should return isAuctionPossible true when the current surplus is above the threshold', async () => {
      await stableQUSDToken.transfer(systemBalance.address, SURPLUS_THRESHOLD);

      const systemBalanceDetails = await systemBalance.getBalanceDetails();

      assert.equal(systemBalanceDetails.isSurplusAuctionPossible, true);
    });

    it('should return isAuctionPossible false when the current debt is above the threshold, but only before netting', async () => {
      await systemBalance.increaseDebt(DEBT_THRESHOLD, { from: LIQUIDATION_AUCTION });
      await stableQUSDToken.transfer(systemBalance.address, 1);

      const systemBalanceDetails = await systemBalance.getBalanceDetails();

      assert.equal(systemBalanceDetails.isDebtAuctionPossible, false, 'Threshold has to be considered after netting');
    });

    it('should return isAuctionPossible false when the current surplus is above the threshold, but only before netting', async () => {
      await stableQUSDToken.transfer(systemBalance.address, SURPLUS_THRESHOLD);
      await systemBalance.increaseDebt(1, { from: LIQUIDATION_AUCTION }); // netting results in surplus < threshold

      const systemBalanceDetails = await systemBalance.getBalanceDetails();
      assert.equal(
        systemBalanceDetails.isSurplusAuctionPossible,
        false,
        'Threshold has to be considered after netting'
      );
    });

    it('should return isAuctionPossible false when the current debt is above the threshold and debt auction is active', async () => {
      const bid = toBN(100);
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      const auctionID = 0;
      assert.equal((await systemDebtAuction.auctions(auctionID)).status, AuctionStatus.NONE);

      await systemBalance.increaseDebt(DEBT_THRESHOLD.times(2), { from: LIQUIDATION_AUCTION });
      await systemReserve.send(DEBT_THRESHOLD.times(2));

      assert.equal((await systemBalance.getBalanceDetails()).isDebtAuctionPossible, true);
      await setTime((await getCurrentBlockTime()) + 1);
      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionID)).status.toString(), AuctionStatus.ACTIVE);

      assert.equal((await systemBalance.getBalanceDetails()).isDebtAuctionPossible, false);
    });
  });
});
