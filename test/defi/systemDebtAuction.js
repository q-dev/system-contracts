const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');

const { toBN, getPercentageFormat } = require('../helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const ERC677 = artifacts.require('ERC677Mock');
const StableToken = artifacts.require('StableCoinMock');
const SystemBalance = artifacts.require('SystemBalanceMock');
const SystemReserve = artifacts.require('SystemReserve');
const SystemDebtAuction = artifacts.require('SystemDebtAuction');
const PushPayments = artifacts.require('PushPayments');
const ParametersEPQFI = artifacts.require('./EPQFI_Parameters');

const AuctionStatus = {
  NONE: 0,
  ACTIVE: 1,
  CLOSED: 2,
};

describe('Defi:SystemDebtAuction', () => {
  const reverter = new Reverter();

  let qethToken;
  let expertsParameters;
  let registry;
  let stableQUSDToken;
  let systemBalance;
  let systemReserve;
  let systemDebtAuction;
  let epqfiParameters;
  let pushPayments;

  const DEBT_AUCTION_PERIOD = toBN(100);
  const DEBT_THRESHOLD = toBN('100000000000000000000');
  const RESERVE_LOT = toBN('1000000000000000000');

  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    PARAMETERS_VOTING = await accounts(4);

    // never return exponantial notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    qethToken = await ERC677.new('qeth', 'qeth');

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], forwarder);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await expertsParameters.setAddr('governed.EPDR.QETH_address', qethToken.address, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await systemBalance.setDebt(DEBT_THRESHOLD.plus(1));

    const parametersInitialList = ['governed.EPQFI.reserveCoolDownThreshold', 'governed.EPQFI.reserveCoolDownP'];
    const parametersValues = [toBN(10000 * 10 ** 18), toBN(604800)];

    epqfiParameters = await makeProxy(registry.address, ParametersEPQFI);
    await epqfiParameters.initialize(registry.address, parametersInitialList, parametersValues, [], [], [], [], [], []);

    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParameters.address);

    systemReserve = await makeProxy(registry.address, SystemReserve);
    await systemReserve.initialize(registry.address, ['defi.QUSD.systemDebtAuction']);

    systemDebtAuction = await makeProxy(registry.address, SystemDebtAuction);
    await systemDebtAuction.initialize(registry.address, 'QUSD');

    await expertsParameters.setUint('governed.EPDR.debtAuctionP', DEBT_AUCTION_PERIOD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_debtThreshold', DEBT_THRESHOLD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.reserveLot', RESERVE_LOT, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_surplusThreshold', 0, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', getPercentageFormat(50), {
      from: PARAMETERS_VOTING,
    });

    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);
    await registry.setAddress('tokeneconomics.systemReserve', systemReserve.address);
    await registry.setAddress('defi.QUSD.systemDebtAuction', systemDebtAuction.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('startAuction', () => {
    it('should be possible to start auction', async () => {
      const bid = toBN(100);
      const auctionId = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionId)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      assert.equal((await systemBalance.getBalanceDetails()).isDebtAuctionPossible, true);
      const result = await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionId)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionId)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionId)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionId)).reserveLot.toString(), RESERVE_LOT);
      assert.approximately(
        (await systemDebtAuction.auctions(auctionId)).endTime.toNumber(),
        (await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(1).toNumber(),
        1,
        'unexpected auction end time'
      );

      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'AuctionStarted');
      assert.equal(result.logs[0].args.bidder, USER1);
      assert.equal(result.logs[0].args.bid.toString(), bid.toString());
    });

    it('should not be possible to start auction if there is active one', async () => {
      const bid = toBN(100);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await truffleAssert.reverts(
        systemDebtAuction.startAuction(bid, { from: USER1 }),
        '[QEC-026000]-Only one system debt auction can run at a time.'
      );

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());
    });

    it('should not be possible to start auction if debt_treshold is not reached', async () => {
      const bid = toBN(100);
      const auctionCount = 0; // rename into auctionId
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemBalance.setDebt(DEBT_THRESHOLD.minus(1));

      await truffleAssert.reverts(
        systemDebtAuction.startAuction(bid, { from: USER1 }),
        '[QEC-026001]-System debt is below auction threshold.'
      );
    });

    it('should not be possible to start auction if bit transfer fails', async () => {
      const bid = toBN(100);
      const auctionCount = 0;
      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await truffleAssert.reverts(
        systemDebtAuction.startAuction(bid, { from: USER1 }),
        'ERC20: transfer amount exceeds balance'
      );
    });

    it('should not be possible to start auction if wihdraw from SystemReserve fails', async () => {
      const bid = toBN(100);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);

      await truffleAssert.reverts(
        systemDebtAuction.startAuction(bid, { from: USER1 }),
        '[QEC-026002]-Failed to withdraw from the SystemReserve contract, failed to start the auction.'
      );
    });

    it('should correctly increment global id counter', async () => {
      const bid = toBN(100);
      await systemReserve.send(toBN('10000000000000000000'));

      await stableQUSDToken.transfer(USER1, bid.multipliedBy(2));
      await stableQUSDToken.approve(systemDebtAuction.address, bid.multipliedBy(2), { from: USER1 });

      await systemDebtAuction.startAuction(bid, { from: USER1 });
      assert.equal((await systemDebtAuction.currentAuctionId()).toString(), '0');

      await setTime((await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(2).toNumber());
      await systemDebtAuction.execute();

      await systemBalance.setDebt(DEBT_THRESHOLD.multipliedBy(2));
      await systemDebtAuction.startAuction(bid, { from: USER1 });
      assert.equal((await systemDebtAuction.currentAuctionId()).toString(), '1');
    });
  });

  describe('bid()', () => {
    it('should be possible to bid and transfer previous bid to the previuos bidder', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      const result = await systemDebtAuction.bid(bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER2);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid2.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Bid');
      assert.equal(result.logs[0].args.bidder, USER2);
      assert.equal(result.logs[0].args.bid.toString(), bid2.toString());
    });

    it('should not be possible to bid if auction has status NONE', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.NONE);

      await truffleAssert.reverts(
        systemDebtAuction.bid(bid2, { from: USER2 }),
        '[QEC-026003]-The auction is not active, failed to bid.'
      );

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.NONE);
    });

    it('should not be possible to bid if auction has status CLOSED', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      let auction = await systemDebtAuction.auctions(auctionCount);
      const endTime = auction.endTime.toNumber();
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder.toString(), USER1);
      assert.equal(auction.highestBid.toString(), bid.toString());
      assert.equal(auction.reserveLot.toString(), RESERVE_LOT);
      assert.approximately(
        endTime,
        (await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(1).toNumber(),
        1,
        'unexpected auction end'
      );
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await setTime(endTime + 1);
      await systemDebtAuction.execute();

      auction = await systemDebtAuction.auctions(auctionCount);
      assert.equal(auction.status.toString(), AuctionStatus.CLOSED);

      await truffleAssert.reverts(
        systemDebtAuction.bid(bid2, { from: USER2 }),
        '[QEC-026003]-The auction is not active, failed to bid.'
      );
    });

    it('should not be possible to bid if endTime has passed', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      let time = 1;
      await setTime((await getCurrentBlockTime()) + time);
      await systemDebtAuction.startAuction(bid, { from: USER1 });
      let auction = await systemDebtAuction.auctions(auctionCount);

      if (auction.status.toString() !== AuctionStatus.ACTIVE) {
        // give an extra time unit to switch to active
        time++;
        await setTime((await getCurrentBlockTime()) + time);
        auction = await systemDebtAuction.auctions(auctionCount);
      }
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder.toString(), USER1);
      assert.equal(auction.highestBid.toString(), bid.toString());
      assert.equal(auction.reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      time += DEBT_AUCTION_PERIOD.plus(1).toNumber();
      await setTime((await getCurrentBlockTime()) + time);
      const blockTime = (await web3.eth.getBlock('latest')).timestamp;

      if (blockTime <= auction.endTime) {
        // give an extra time unit to pass endtime
        time++;
        await setTime((await getCurrentBlockTime()) + time);
        await systemDebtAuction.auctions(auctionCount);
      }

      await truffleAssert.reverts(
        systemDebtAuction.bid(bid2, { from: USER2 }),
        '[QEC-026004]-The auction is finished, failed to bid.'
      );
    });

    it('should not be possible to bid with less than highest bid + auctionMinIncrement', async () => {
      const FIRST_BID = toBN(100);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, FIRST_BID);
      await stableQUSDToken.approve(systemDebtAuction.address, FIRST_BID, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(FIRST_BID, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), FIRST_BID.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await truffleAssert.reverts(
        systemDebtAuction.bid(FIRST_BID.plus(1), { from: USER2 }),
        '[QEC-026005]-The bid amount must exceed the highest bid by the minimum increment percentage or more.'
      );

      const RIGHT_BID = FIRST_BID.plus(FIRST_BID.dividedBy(2));
      await stableQUSDToken.transfer(USER2, RIGHT_BID);
      await stableQUSDToken.approve(systemDebtAuction.address, RIGHT_BID, { from: USER2 });

      await systemDebtAuction.bid(RIGHT_BID, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER2);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), RIGHT_BID.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());
    });
  }).retries(3);

  describe('getRaisingBid()', () => {
    it('should be possible to get raising bid', async () => {
      const bid = toBN(100);
      const auctionId = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionId)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      const raisingBid = toBN(await systemDebtAuction.getRaisingBid(auctionId));
      const expectedRaisingBid = bid.plus(bid.dividedBy(2));
      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should return previous highest bid + 1 if auctionMinIncrement = 0%', async () => {
      await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', 0, { from: PARAMETERS_VOTING });

      const bid = toBN(100);
      const auctionId = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionId)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      const raisingBid = toBN(await systemDebtAuction.getRaisingBid(auctionId));
      const expectedRaisingBid = bid.plus(1);
      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should not be possible to to get raising bid if the auction does not exist', async () => {
      await truffleAssert.reverts(systemDebtAuction.getRaisingBid(1000), '[QEC-026009]-The auction does not exist.');
    });
  });

  describe('execute()', () => {
    it('should be possible to execute', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await systemDebtAuction.bid(bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER2);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid2.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      const user2Balance = toBN(await web3.eth.getBalance(USER2));

      await setTime((await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(2).toNumber());
      const result = await systemDebtAuction.execute();
      await systemBalance.performNetting();

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.CLOSED);
      assert.equal((await systemBalance.getDebt()).toString(), DEBT_THRESHOLD.plus(1).minus(bid2).toString());
      assert.equal(await web3.eth.getBalance(USER2), user2Balance.plus(RESERVE_LOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.bidder, USER2);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.highestBid.toString(), bid2.toString());
    }).retries(3);

    it('should not be possible to execute if endtime is not passed', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await systemDebtAuction.bid(bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER2);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid2.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await truffleAssert.reverts(systemDebtAuction.execute(), '[QEC-026006]-The auction is not finished.');

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
    });

    it('should not be possible to execute if auction has status NONE', async () => {
      const auctionCount = 0;

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await setTime((await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(2).toNumber());
      await truffleAssert.reverts(
        systemDebtAuction.execute(),
        '[QEC-026007]-The auction is not active, execution failed.'
      );

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.NONE);
    });

    it('should not be possible to execute if auction has status CLOSED', async () => {
      const bid = toBN(100);
      const bid2 = toBN(200);
      const auctionCount = 0;
      await stableQUSDToken.transfer(USER1, bid);
      await stableQUSDToken.approve(systemDebtAuction.address, bid, { from: USER1 });
      await stableQUSDToken.transfer(USER2, bid2);
      await stableQUSDToken.approve(systemDebtAuction.address, bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status, AuctionStatus.NONE);
      await systemReserve.send(toBN('10000000000000000000'));

      await systemDebtAuction.startAuction(bid, { from: USER1 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER1);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      await systemDebtAuction.bid(bid2, { from: USER2 });

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.ACTIVE);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).bidder.toString(), USER2);
      assert.equal((await systemDebtAuction.auctions(auctionCount)).highestBid.toString(), bid2.toString());
      assert.equal((await systemDebtAuction.auctions(auctionCount)).reserveLot.toString(), RESERVE_LOT);
      assert.equal((await web3.eth.getBalance(systemDebtAuction.address)).toString(), RESERVE_LOT.toString());

      const user2Balance = toBN(await web3.eth.getBalance(USER2));

      await setTime((await getCurrentBlockTime()) + DEBT_AUCTION_PERIOD.plus(2).toNumber());
      const result = await systemDebtAuction.execute();
      await systemBalance.performNetting();

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.CLOSED);
      assert.equal((await systemBalance.getDebt()).toString(), DEBT_THRESHOLD.plus(1).minus(bid2).toString());
      assert.equal(await web3.eth.getBalance(USER2), user2Balance.plus(RESERVE_LOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.bidder, USER2);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.highestBid.toString(), bid2.toString());

      await truffleAssert.reverts(
        systemDebtAuction.execute(),
        '[QEC-026007]-The auction is not active, execution failed.'
      );

      assert.equal((await systemDebtAuction.auctions(auctionCount)).status.toString(), AuctionStatus.CLOSED);
    });
  }).retries(3);
});
