'use strict';
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');

const { artifacts } = require('hardhat');
const DefiParamsMock = artifacts.require('DefiParamsMock');

describe('DefiParams', () => {
  const reverter = new Reverter();
  let defiParams;

  before(async () => {
    defiParams = await DefiParamsMock.new();
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('oracle', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_oracle';
      const result = await defiParams.oracle('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('ceiling', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_ceiling';
      const result = await defiParams.ceiling('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('collateralizationRatio', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_collateralizationRatio';
      const result = await defiParams.collateralizationRatio('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('liquidationRatio', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_liquidationRatio';
      const result = await defiParams.liquidationRatio('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('liquidationFee', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_liquidationFee';
      const result = await defiParams.liquidationFee('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('interestRate', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_QUSD_interestRate';
      const result = await defiParams.interestRate('WETH', 'QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('surplusThreshold', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.QUSD_surplusThreshold';
      const result = await defiParams.surplusThreshold('QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('surplusLot', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.QUSD_surplusLot';
      const result = await defiParams.surplusLot('QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('savingRate', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.QUSD_savingRate';
      const result = await defiParams.savingRate('QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('debtThreshold', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.QUSD_debtThreshold';
      const result = await defiParams.debtThreshold('QUSD');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });

  describe('contractAddress', () => {
    it('should successfully concatenate defi parameters', async () => {
      const expectedResult = 'governed.EPDR.WETH_address';
      const result = await defiParams.contractAddress('WETH');
      assert.equal(expectedResult, result.toString(), 'concate message should match');
    });
  });
});
