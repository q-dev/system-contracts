const { accounts, accountsArray } = require('../helpers/utils.js');
const { assert } = require('chai');
const makeProxy = require('../helpers/makeProxy');
const Reverter = require('../helpers/reverter');

const { artifacts } = require('hardhat');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const TokenBridgeAdminProxy = artifacts.require('TokenBridgeAdminProxy');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const BridgeValidatorsMock = artifacts.require('BridgeValidatorsMock');
const Validators = artifacts.require('./ValidatorsMock.sol');

describe('Defi:TokenBridgeAdminProxy', () => {
  const reverter = new Reverter();
  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;

  let tokenBridgeAdminProxy;
  let addressStorageStakesSorted;
  let addressStorageStakes;
  let bridgeValidatorsMock;
  let expertsParameters;
  let validators;
  let registry;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    PARAMETERS_VOTING = await accounts(3);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);

    registry = await ContractRegistry.new();
    await registry.initialize(
      [OWNER],
      ['governance.constitution.parametersVoting', 'governance.experts.EPDR.parametersVoting'],
      [PARAMETERS_VOTING, PARAMETERS_VOTING]
    );

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    bridgeValidatorsMock = await makeProxy(registry.address, BridgeValidatorsMock);

    tokenBridgeAdminProxy = await makeProxy(registry.address, TokenBridgeAdminProxy);
    await tokenBridgeAdminProxy.initialize(registry.address, bridgeValidatorsMock.address);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(
      registry.address,
      ['governed.EPDR.maxTBvalidators'],
      [11],
      [],
      [],
      [],
      [],
      [],
      []
    );

    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('updateTokenbridgeValidators', () => {
    it('should correct update token bridge validators if max validators count < current snapshot', async () => {
      await validators.setSnapshot(await accountsArray(10));
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), []);

      const maxCount = 3;
      await expertsParameters.setUint('governed.EPDR.maxTBvalidators', maxCount, { from: PARAMETERS_VOTING });
      const txReceipt = await tokenBridgeAdminProxy.updateTokenbridgeValidators();

      assert.equal(txReceipt.logs.length, maxCount);

      const currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < maxCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i));
      }
    });

    it('should correct update token bridge validators if max validators count > current snapshot', async () => {
      await validators.setSnapshot(await accountsArray(10));
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), []);

      const txReceipt = await tokenBridgeAdminProxy.updateTokenbridgeValidators();

      assert.equal(txReceipt.logs.length, 10);

      const currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < accounts.length; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i));
      }
    });

    it('should correct update token bridge validators several times', async () => {
      let currentValidatorsCount = 3;
      await validators.setSnapshot(await accountsArray(10));
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), []);

      await expertsParameters.setUint('governed.EPDR.maxTBvalidators', currentValidatorsCount, {
        from: PARAMETERS_VOTING,
      });
      let txReceipt = await tokenBridgeAdminProxy.updateTokenbridgeValidators();

      assert.equal(txReceipt.logs.length, currentValidatorsCount);

      let currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i));
      }

      let previousValidatorsCount = 3;
      currentValidatorsCount = 7;
      await expertsParameters.setUint('governed.EPDR.maxTBvalidators', currentValidatorsCount, {
        from: PARAMETERS_VOTING,
      });
      txReceipt = await tokenBridgeAdminProxy.updateTokenbridgeValidators();

      assert.equal(txReceipt.logs.length, currentValidatorsCount - previousValidatorsCount);

      currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount - previousValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].event, 'ValidatorAdded');
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i + previousValidatorsCount));
      }

      previousValidatorsCount = 7;
      currentValidatorsCount = 10;
      await expertsParameters.setUint('governed.EPDR.maxTBvalidators', currentValidatorsCount, {
        from: PARAMETERS_VOTING,
      });
      txReceipt = await tokenBridgeAdminProxy.updateTokenbridgeValidators();

      assert.equal(txReceipt.logs.length, currentValidatorsCount - previousValidatorsCount);

      currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount - previousValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].event, 'ValidatorAdded');
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i + previousValidatorsCount));
      }
    });
  });

  describe('test fallback', () => {
    it('should correct call different functions', async () => {
      const testInstance = await BridgeValidatorsMock.at(tokenBridgeAdminProxy.address);

      await testInstance.addValidator(USER1);
      await testInstance.addValidator(USER2);

      assert.deepEqual(await bridgeValidatorsMock.validatorList(), await testInstance.validatorList());
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), [USER1, USER2]);
    });
  });
});
