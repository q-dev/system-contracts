const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');

const { toBN, getPercentageFormat } = require('../helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const DefaultAllocationProxy = artifacts.require('DefaultAllocationProxy');
const StableToken = artifacts.require('StableCoinMock');
const SystemSurplusAuction = artifacts.require('SystemSurplusAuction');
const SystemBalance = artifacts.require('SystemBalance');
const SystemDebtAuction = artifacts.require('SystemDebtAuction');
const PushPayments = artifacts.require('PushPayments');
const Saving = artifacts.require('Saving');

describe('Defi:SystemSurplusAuction', () => {
  const reverter = new Reverter();

  let surplusAuction;
  let registry;
  let defaultAllocProxy;
  let stableCoin;
  let systemBalance;
  let expertsParameters;
  let systemDebtAuction;
  let pushPayments;
  let saving;
  let crKeeperFactory;

  const Q = require('../helpers/defiHelper').Q;

  const AUCTION_PERIOD = toBN(100);
  const LOT = Q.multipliedBy(10);
  const THRESHOLD = LOT;

  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;
  let LIQUIDATION_AUCTION;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    PARAMETERS_VOTING = await accounts(4);
    LIQUIDATION_AUCTION = await accounts(3);

    // never return exponantial notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);

    defaultAllocProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocProxy.initialize(registry.address, [], []);

    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocProxy.address);

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    stableCoin = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], registry.address);
    await registry.setAddress('defi.QUSD.coin', stableCoin.address);

    surplusAuction = await makeProxy(registry.address, SystemSurplusAuction);
    await surplusAuction.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemSurplusAuction', surplusAuction.address);

    systemDebtAuction = await makeProxy(registry.address, SystemDebtAuction);
    await systemDebtAuction.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemDebtAuction', systemDebtAuction.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);

    saving = await makeProxy(registry.address, Saving);
    await saving.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.saving', saving.address);

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);

    await expertsParameters.setUint('governed.EPDR.surplusAuctionP', AUCTION_PERIOD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_surplusLot', LOT, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_surplusThreshold', THRESHOLD, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_debtThreshold', 0, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', getPercentageFormat(50), {
      from: PARAMETERS_VOTING,
    });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('startAuction', () => {
    it('happy path', async () => {
      const DEBT = Q;

      await registry.setAddress('defi.QUSD.liquidationAuction', LIQUIDATION_AUCTION);
      await systemBalance.increaseDebt(DEBT, { from: LIQUIDATION_AUCTION });

      const SURPLUS = DEBT.plus(THRESHOLD).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = (await getCurrentBlockTime()) + 1;
      await setTime(startTime);
      const BID1 = Q;
      const sysBalDetails = await systemBalance.getBalanceDetails();
      assert.isTrue(
        sysBalDetails.isSurplusAuctionPossible,
        'System balance details must already indicate, that auction is possible'
      );

      const result = await surplusAuction.startAuction({ from: USER1, value: BID1 });
      const auctionId = 0;
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'AuctionStarted');
      assert.equal(result.logs[0].args._auctionId, auctionId);
      assert.equal(result.logs[0].args._bidder, USER1);
      assert.equal(result.logs[0].args._bid.toString(), BID1.toString());

      let auction = await surplusAuction.auctions(auctionId);

      const endTime = AUCTION_PERIOD.plus(startTime);
      assert.approximately(auction.endTime.toNumber(), endTime.toNumber(), 1, 'unexpected end time');
      assert.equal(auction.bidder, USER1, 'unexpected bidder');
      assert.equal(auction.highestBid.toString(), BID1.toString(), 'unexpected highest bid');
      assert.equal(auction.lot.toString(), LOT.toString(), 'unexpected lot');

      const surplusAfter = await stableCoin.balanceOf(systemBalance.address);
      assert.equal(
        surplusAfter.toString(),
        SURPLUS.minus(DEBT).minus(LOT).toString(),
        'unexpected value of surplus after auction is created'
      );

      assert.equal(
        (await stableCoin.balanceOf(surplusAuction.address)).toString(),
        LOT.toString(),
        'surplus lot was not locked'
      );

      const bidder1BalanceBefore = await web3.eth.getBalance(USER1);

      const BID2 = BID1.plus(Q);
      await surplusAuction.bid(auctionId, { from: USER2, value: BID2 });

      auction = await surplusAuction.auctions(auctionId);
      assert.equal(auction.bidder, USER2, 'bidder was not updated');
      assert.equal(auction.highestBid.toString(), BID2.toString(), 'highest bid was not updated');

      const bidder1BalanceAfter = await web3.eth.getBalance(USER1);
      assert.equal(bidder1BalanceAfter, toBN(bidder1BalanceBefore).plus(BID1).toString(), 'bid was not returned');

      const defaultAllocProxyBalanceBefore = await web3.eth.getBalance(defaultAllocProxy.address);
      await setTime((await getCurrentBlockTime()) + endTime.toNumber() + 1);
      await surplusAuction.execute(auctionId);

      const winnerBalance = await stableCoin.balanceOf(USER2);
      assert.equal(winnerBalance.toString(), LOT.toString(), 'unexpected winner balance');

      const defaultAllocProxyBalanceAfter = await web3.eth.getBalance(defaultAllocProxy.address);
      assert.equal(
        toBN(defaultAllocProxyBalanceBefore).plus(BID2).toString(),
        defaultAllocProxyBalanceAfter.toString(),
        'highest bid was not transferred to DefaultAllocationProxy'
      );
    });

    it('should not startAuction if surplus is below threshold', async () => {
      const SURPLUS = THRESHOLD.minus(1);
      await stableCoin.transfer(systemBalance.address, SURPLUS);
      const sysBalDetails = await systemBalance.getBalanceDetails();
      assert.isFalse(
        sysBalDetails.isSurplusAuctionPossible,
        'System balance details must already indicate, that auction is not possible'
      );

      await truffleAssert.reverts(
        surplusAuction.startAuction({ value: Q }),
        '[QEC-025002]-Surplus after netting is below surplus auction threshold.'
      );
    });

    it('should not startAuction if surplus smaller than lot', async () => {
      await expertsParameters.setUint('governed.EPDR.QUSD_surplusThreshold', 0, { from: PARAMETERS_VOTING });
      const SURPLUS = LOT.minus(1);
      await stableCoin.transfer(systemBalance.address, SURPLUS);
      // const sysBalDetails = await systemBalance.getBalanceDetails()
      // assert.isFalse(sysBalDetails.isSurplusAuctionPossible,
      //   'System balance details must already indicate, that auction is not possible')

      await truffleAssert.reverts(
        surplusAuction.startAuction({ value: Q }),
        '[QEC-025003]-Not enough surplus to fill the auction lot.'
      );
    });

    it('should not startAuction without first bid', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      await truffleAssert.reverts(
        surplusAuction.startAuction(),
        '[QEC-025000]-Invalid bid amount, failed start the auction.'
      );
    });
  });

  describe('bid', () => {
    it('happy path', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);
      const BID1 = Q;
      await surplusAuction.startAuction({ from: USER1, value: BID1 });

      const bidder1BalanceBefore = await web3.eth.getBalance(USER1);

      const auctionId = 0;
      const BID2 = BID1.plus(Q);
      const result = await surplusAuction.bid(auctionId, { from: USER2, value: BID2 });
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Bid');
      assert.equal(result.logs[0].args._auctionId, auctionId);
      assert.equal(result.logs[0].args._bidder, USER2);
      assert.equal(result.logs[0].args._bid.toString(), BID2.toString());

      const auction = await surplusAuction.auctions(auctionId);
      assert.equal(auction.bidder, USER2, 'bidder was not updated');
      assert.equal(auction.highestBid.toString(), BID2.toString(), 'highest bid was not updated');

      const bidder1BalanceAfter = await web3.eth.getBalance(USER1);
      assert.equal(bidder1BalanceAfter, toBN(bidder1BalanceBefore).plus(BID1).toString(), 'bid was not returned');
    });

    it('should not be possible to bid with less than highest bid + auctionMinIncrement', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const FIRST_BID = Q;
      await surplusAuction.startAuction({ value: FIRST_BID });

      const BID = FIRST_BID.plus(1);
      const auctionId = 0;

      await truffleAssert.reverts(
        surplusAuction.bid(auctionId, { from: USER1, value: BID }),
        '[QEC-025006]-The bid amount must exceed the highest bid by the minimum increment percentage or more.'
      );

      const RIGHT_BID = FIRST_BID.plus(FIRST_BID.dividedBy(2)); // because auctionMinIncrement == 50%
      await surplusAuction.bid(auctionId, { from: USER1, value: RIGHT_BID });

      const auction = await surplusAuction.auctions.call(auctionId);
      assert.equal(RIGHT_BID.toString(), toBN(auction.highestBid).toString());
      assert.equal(USER1, auction.bidder);
    });

    it('should not bid after surplusAuctionP is over', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);

      const FIRST_BID = Q;
      await surplusAuction.startAuction({ value: FIRST_BID });
      const auctionId = 0;

      const BID = FIRST_BID.plus(Q);

      await setTime((await getCurrentBlockTime()) + AUCTION_PERIOD.plus(startTime + 1).toNumber());
      await truffleAssert.reverts(
        surplusAuction.bid(auctionId, { from: USER1, value: BID }),
        '[QEC-025005]-The auction is finished, failed to bid.'
      );
    });

    it('should not be possible to bid if the auction does not exist', async () => {
      await truffleAssert.reverts(
        surplusAuction.bid(1000, { from: USER1, value: Q }),
        '[QEC-025004]-The auction does not exist.'
      );
    });
  });

  describe('getRaisingBid()', () => {
    it('should be possible to get raising bid', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);
      const BID = Q;
      await surplusAuction.startAuction({ from: USER1, value: BID });
      const auctionId = 0;

      const raisingBid = toBN(await surplusAuction.getRaisingBid(auctionId));
      const expectedRaisingBid = BID.plus(BID.dividedBy(2));
      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should return previous highest bid + 1 if auctionMinIncrement = 0%', async () => {
      await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', 0, { from: PARAMETERS_VOTING });

      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);
      const BID = Q;
      await surplusAuction.startAuction({ from: USER1, value: BID });
      const auctionId = 0;

      const raisingBid = toBN(await surplusAuction.getRaisingBid(auctionId));
      const expectedRaisingBid = BID.plus(1);
      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should not be possible to to get raising bid if the auction does not exist', async () => {
      await truffleAssert.reverts(surplusAuction.getRaisingBid(1000), '[QEC-025004]-The auction does not exist.');
    });
  });

  describe('execute', () => {
    it('happy path', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);
      const BID1 = Q;
      await surplusAuction.startAuction({ from: USER1, value: BID1 });

      const defaultAllocProxyBalanceBefore = await web3.eth.getBalance(defaultAllocProxy.address);

      const auctionId = 0;
      await setTime((await getCurrentBlockTime()) + AUCTION_PERIOD.plus(startTime + 10).toNumber());
      const result = await surplusAuction.execute(auctionId);
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args._auctionId, auctionId);
      assert.equal(result.logs[0].args._bidder, USER1);
      const auctionInfo = result.logs[0].args._info;
      assert.equal(auctionInfo.highestBid.toString(), BID1.toString());

      const winnerBalance = await stableCoin.balanceOf(USER1);
      assert.equal(winnerBalance.toString(), LOT.toString(), 'unexpected winner balance');

      const defaultAllocProxyBalanceAfter = await web3.eth.getBalance(defaultAllocProxy.address);
      assert.equal(
        defaultAllocProxyBalanceAfter.toString(),
        toBN(defaultAllocProxyBalanceBefore).plus(BID1).toString(),
        'highest bid was not transferred to DefaultAllocationProxy'
      );
    });

    it('should not execute before auction is over', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      await surplusAuction.startAuction({ value: Q });

      const auctionId = 0;
      await truffleAssert.reverts(surplusAuction.execute(auctionId), '[QEC-025008]-The auction is not finished.');
    });

    it('should not be possible to execute twice', async () => {
      const SURPLUS = THRESHOLD.plus(LOT).plus(Q);
      await stableCoin.transfer(systemBalance.address, SURPLUS);

      const startTime = 1;
      await setTime((await getCurrentBlockTime()) + startTime);

      await surplusAuction.startAuction({ value: Q });
      const auctionId = 0;

      await setTime((await getCurrentBlockTime()) + AUCTION_PERIOD.plus(startTime + 1).toNumber());
      await surplusAuction.execute(auctionId);

      await truffleAssert.reverts(
        surplusAuction.execute(auctionId),
        '[QEC-025009]-The auction has already been executed.'
      );
    });
  });
});
