const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const assertBNInRange = require('../helpers/assertBnInRange');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');

const { toBN, getPercentageFormat } = require('../helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const BorrowingCore = artifacts.require('BorrowingCore');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const FxPriceFeed = artifacts.require('FxPriceFeed');
const ERC677 = artifacts.require('ERC677Mock');
const StableToken = artifacts.require('StableCoin');
const LiquidationAuction = artifacts.require('LiquidationAuction');
const Saving = artifacts.require('Saving');
const SystemBalance = artifacts.require('SystemBalance');

const AuctionStatus = {
  NONE: 0,
  ACTIVE: 1,
  CLOSED: 2,
};

describe('Defi:LiquidationAuction', () => {
  const reverter = new Reverter();

  let borrowingCore;
  let crKeeperFactory;
  let qethToken;
  let qethPriceFeed;
  let expertsParameters;
  let registry;
  let stableQUSDToken;
  let liquidationAuction;
  let saving;
  let systemBalance;

  const DECIMAL = require('../helpers/defiHelper').DECIMAL;
  const INTEREST_RATE = new BigNumber(0.01);
  const LIQUIDATION_AUCTION_PERIOD = toBN(100);
  const LIQUIDATION_FEE_PERCENT = toBN(10).pow(toBN(26)); // 10%

  const checkInvariants = async (borrowingCore, token, systemBalance) => {
    // check total token supply invariant
    const totalMinted = toBN((await borrowingCore.userVaults(USER1, 0)).mintedAmount)
      .plus(toBN((await borrowingCore.userVaults(USER2, 0)).mintedAmount))
      .plus(toBN((await borrowingCore.userVaults(USER3, 0)).mintedAmount));

    const debt = toBN(await systemBalance.getDebt());
    const totalSupply = await token.totalSupply();
    assert.equal(totalMinted.plus(debt).toString(), totalSupply.toString());

    // check borrowing totals invariant
    const totals = await borrowingCore.getAggregatedTotals();
    const expectedOutstandingDebt = toBN(totals.mintedAmount).plus(toBN(totals.owedBorrowingFees));
    assert.equal(totals.outstandingDebt, expectedOutstandingDebt.toString(), 'wrong aggregation of outstanding debt');
    assert.equal(totalMinted.toString(), totals.mintedAmount, 'wrong aggregation of minted amount');

    // check tokens backed by collateral
    const stcBackedByQeth = await borrowingCore.totalStcBackedByCol(qethToken.address);
    assert.equal(stcBackedByQeth.toString(), totalMinted.toString(), 'wrong STC backed by QETH amount');
  };

  const generateSomeSTC = async (address, amount) => {
    const colKey = 'QETH';
    const depositValue = toBN('50000000000000000000');
    await qethToken.transfer(address, depositValue);
    await borrowingCore.createVault(colKey, { from: address });
    await qethToken.approve(borrowingCore.address, depositValue, { from: address });
    await borrowingCore.depositCol(0, depositValue, { from: address });
    await borrowingCore.generateStc(0, amount, { from: address });
  };

  let OWNER;
  let USER1;
  let USER2;
  let USER3;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    USER3 = await accounts(3);
    PARAMETERS_VOTING = await accounts(4);

    // never return exponential notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    qethToken = await ERC677.new('qeth', 'qeth');

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], forwarder);
    qethPriceFeed = await FxPriceFeed.new('QETH_QUSD', 18, [OWNER], qethToken.address, stableQUSDToken.address);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await expertsParameters.setAddr('governed.EPDR.QETH_address', qethToken.address, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_liquidationFee', LIQUIDATION_FEE_PERCENT, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QUSD_step', toBN(10), { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    liquidationAuction = await makeProxy(registry.address, LiquidationAuction);
    await liquidationAuction.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.liquidationAuction', liquidationAuction.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    borrowingCore = await makeProxy(registry.address, BorrowingCore);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await borrowingCore.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.borrowing', borrowingCore.address);

    saving = await makeProxy(registry.address, Saving);
    await saving.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.saving', saving.address);

    await expertsParameters.setAddr('governed.EPDR.QETH_QUSD_oracle', qethPriceFeed.address, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_collateralizationRatio', DECIMAL.multipliedBy(1.5), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_liquidationRatio', DECIMAL.multipliedBy(150), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_ceiling', DECIMAL.multipliedBy(1.0), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_interestRate', DECIMAL.multipliedBy(INTEREST_RATE), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.liquidationAuctionP', LIQUIDATION_AUCTION_PERIOD, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', getPercentageFormat(50), {
      from: PARAMETERS_VOTING,
    });

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('startAuction', () => {
    it('should be possible to startAuction and trigger liquidation with it', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000);
      const amountOfStcToGenerate = toBN(999999999999999909);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);

      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await borrowingCore.updateCompoundRate(colKey);

      const result = await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });
      auction = await liquidationAuction.auctions(USER1, vaultId);

      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);

      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'AuctionStarted');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      assert.equal(result.logs[0].args.bidder, USER2);
      assert.equal(result.logs[0].args.bid.toString(), bid.toString());
    });

    it('should be possible to startAuction on already liquidated vault', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await borrowingCore.updateCompoundRate(colKey);
      await borrowingCore.liquidate(USER1, vaultId);

      const result = await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      auction = await liquidationAuction.auctions(USER1, vaultId);

      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);

      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'AuctionStarted');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      assert.equal(result.logs[0].args.bidder, USER2);
      assert.equal(result.logs[0].args.bid.toString(), bid.toString());
    });

    it('should not be possible to startAuction if already created one for this vault', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await borrowingCore.updateCompoundRate(colKey);
      await borrowingCore.liquidate(USER1, vaultId);

      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      await truffleAssert.reverts(
        liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 }),
        '[QEC-024000]-A liquidation auction for this borrowing vault has already been started.'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );
    });

    it('should not be possible to startAuction if bid Transfer fails', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await borrowingCore.updateCompoundRate(colKey);
      await truffleAssert.reverts(
        liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 }),
        'ERC20: transfer amount exceeds balance'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);
    });
  });

  describe('bid()', () => {
    it('should be possible to bid and transfer previous bid to the previous bidder', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      assert.equal((await stableQUSDToken.balanceOf(USER2)).toString(), bid.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER3)).toString(), secondBid.toString());

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber() - 1);
      await borrowingCore.updateCompoundRate(colKey);

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal((await stableQUSDToken.balanceOf(USER2)).toString(), 0);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      await borrowingCore.updateCompoundRate(colKey);
      const result = await liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal((await stableQUSDToken.balanceOf(USER3)).toString(), 0);
      assert.equal((await stableQUSDToken.balanceOf(USER2)).toString(), bid.toString());
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER3);
      assert.equal(auction.highestBid.toString(), secondBid);

      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Bid');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      assert.equal(result.logs[0].args.bidder, USER3);
      assert.equal(result.logs[0].args.bid.toString(), secondBid.toString());
    });

    it('should not be possible to bid if auction has status NONE', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);

      await borrowingCore.updateCompoundRate(colKey);
      await truffleAssert.reverts(
        liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 }),
        '[QEC-024001]-The auction is not active, failed to bid.'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.NONE);
    });

    it('should not be possible to bid if auction has status CLOSED', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      await borrowingCore.updateCompoundRate(colKey);

      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);

      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      const result = await liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER3);
      assert.equal(auction.highestBid.toString(), secondBid);

      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Bid');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      assert.equal(result.logs[0].args.bidder, USER3);
      assert.equal(result.logs[0].args.bid.toString(), secondBid.toString());

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      const passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      await liquidationAuction.execute(USER1, vaultId);
      await truffleAssert.reverts(
        liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 }),
        '[QEC-024001]-The auction is not active, failed to bid.'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.CLOSED);
    });

    it('should not be possible to bid if endTime has passed', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      await borrowingCore.updateCompoundRate(colKey);

      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);

      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      await borrowingCore.updateCompoundRate(colKey);
      const result = await liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER3);
      assert.equal(auction.highestBid.toString(), secondBid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Bid');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      assert.equal(result.logs[0].args.bidder, USER3);
      assert.equal(result.logs[0].args.bid.toString(), secondBid.toString());

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      await setTime(
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(toBN(2)).toNumber()
      );
      await truffleAssert.reverts(
        liquidationAuction.bid(USER1, vaultId, secondBid, { from: USER3 }),
        '[QEC-024002]-The auction for this vault is finished, failed to bid.'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER3);
      assert.equal(auction.highestBid.toString(), secondBid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );
    });

    it('should not be possible to bid with less than highest bid + auctionMinIncrement', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      await borrowingCore.updateCompoundRate(colKey);

      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let auction = await liquidationAuction.auctions(USER1, vaultId);
      const endTime = (await getCurrentBlockTime()) + LIQUIDATION_AUCTION_PERIOD.toNumber();
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      await borrowingCore.updateCompoundRate(colKey);

      await truffleAssert.reverts(
        liquidationAuction.bid(USER1, vaultId, bid, { from: USER3 }),
        '[QEC-024003]-The bid amount must exceed the highest bid by the minimum increment percentage or more.'
      );

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER2);
      assert.equal(auction.highestBid.toString(), bid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );

      const rightBid = bid.plus(bid.dividedBy(2));
      await liquidationAuction.bid(USER1, vaultId, rightBid, { from: USER3 });

      auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
      assert.equal(auction.bidder, USER3);
      assert.equal(auction.highestBid.toString(), rightBid);
      assert.approximately(
        auction.endTime.toNumber(),
        endTime,
        1,
        'auction end time must be the auction start time plus auction period'
      );
    });
  }).retries(3);

  describe('getRaisingBid()', () => {
    it('should be possible to get raising bid', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      assert.equal((await stableQUSDToken.balanceOf(USER2)).toString(), bid.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER3)).toString(), secondBid.toString());

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      const raisingBid = toBN(await liquidationAuction.getRaisingBid(USER1, vaultId));
      const expectedRaisingBid = bid.plus(bid.dividedBy(2));
      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should return previous highest bid + 1 if auctionMinIncrement = 0%', async () => {
      await expertsParameters.setUint('governed.EPDR.auctionMinIncrement', 0, { from: PARAMETERS_VOTING });

      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const secondBid = toBN(200);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await generateSomeSTC(USER3, secondBid);
      await stableQUSDToken.approve(liquidationAuction.address, secondBid, { from: USER3 });

      assert.equal((await stableQUSDToken.balanceOf(USER2)).toString(), bid.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER3)).toString(), secondBid.toString());

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      const raisingBid = toBN(await liquidationAuction.getRaisingBid(USER1, vaultId));
      const expectedRaisingBid = bid.plus(1);

      assert.equal(raisingBid.toString(), expectedRaisingBid.toString());
    });

    it('should not be possible to to get raising bid if the auction is not active', async () => {
      await truffleAssert.reverts(
        liquidationAuction.getRaisingBid(USER1, 10000),
        '[QEC-024006]-The auction is not active.'
      );
    });
  });

  describe('execute()', () => {
    it('should be possible to execute and perform all token transfers correctly if mintedAmount < highestBid < fullDebt + liquidationFee and surplus > debt', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000);
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(100);
      const bid = toBN(1993898600548612459);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const surplusPart = bid.minus(amountOfStcToGenerate);

      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const result = await liquidationAuction.execute(USER1, vaultId);

      await systemBalance.performNetting();

      const auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.CLOSED);
      assert.equal((await systemBalance.getDebt()).toString(), 0);
      assert.equal((await systemBalance.getSurplus()).toString(), surplusPart.toString());
      assert.equal((await qethToken.balanceOf(USER2)).toString(), depositValue.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });

    it('should be possible to execute and perform all token transfers correctly if mintedAmount < highestBid < fullDebt + liquidationFee and surplus < debt', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(1000000000000000909);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const surplusPart = bid.minus(amountOfStcToGenerate);

      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const result = await liquidationAuction.execute(USER1, vaultId);

      await systemBalance.performNetting();

      assert.equal((await systemBalance.getDebt()).toString(), 0);
      assert.equal((await systemBalance.getSurplus()).toString(), surplusPart);
      assert.equal((await qethToken.balanceOf(USER2)).toString(), depositValue.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });

    it('should be possible to execute and perform all token transfers correctly if highestBid < fullDebt + liquidationFee', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1500000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(999999999999999909);
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      const passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const result = await liquidationAuction.execute(USER1, vaultId);

      assert.equal((await systemBalance.getDebt()).toString(), amountOfStcToGenerate.minus(bid).toString());
      assert.equal((await systemBalance.getSurplus()).toString(), 0);
      assert.equal((await qethToken.balanceOf(USER2)).toString(), depositValue.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });

    it('should be possible to execute and perform all token transfers correctly if highestBid = fullDebt + liquidationFee', async () => {
      const price = toBN('100000000000000000000'); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN('1203053799952796881');
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);

      const fullDebt = toBN((await borrowingCore.userVaults(USER1, vaultId)).liquidationFullDebt);
      const interestFee = fullDebt.minus(amountOfStcToGenerate);

      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);

      const result = await liquidationAuction.execute(USER1, vaultId);

      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);

      assert.equal((await systemBalance.getDebt()).toString(), 0);
      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      assertBNInRange(await systemBalance.getSurplus(), bid.minus(fullDebt.minus(interestFee)), '1000');
      assert.equal((await qethToken.balanceOf(USER2)).toString(), depositValue.toString());
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate.toString());

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });
    it('should be possible to execute and perform all token transfers correctly if highestBid > fullDebt + liquidationFee', async () => {
      const price = toBN('100000000000000000000'); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN('5303053799952796881');
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      let passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const fullDebt = toBN((await borrowingCore.userVaults(USER1, vaultId)).liquidationFullDebt);
      const interestFee = fullDebt.minus(amountOfStcToGenerate);
      const liquidationFee = fullDebt.times(LIQUIDATION_FEE_PERCENT).idiv(toBN(10).pow(toBN(27)));

      passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const result = await liquidationAuction.execute(USER1, vaultId);

      assert.equal((await systemBalance.getDebt()).toString(), 0);
      assert.equal((await systemBalance.getSurplus()).toString(), liquidationFee.plus(interestFee).toString());
      assert.equal((await qethToken.balanceOf(USER2)).toString(), depositValue.toString());
      assert.equal(
        (await stableQUSDToken.balanceOf(USER1)).toString(),
        amountOfStcToGenerate.plus(bid.minus(fullDebt).minus(liquidationFee)).toString()
      );

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });
    it('should be possible to execute and perform all token transfers correctly if fullDebt < highestBid < (fullDebt + liquidationFee)', async () => {
      const price = toBN('100000000000000000000'); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN('1193685272684360801');
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });
      const mintedAmount = toBN((await borrowingCore.userVaults(USER1, vaultId)).mintedAmount);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      const passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      const result = await liquidationAuction.execute(USER1, vaultId);

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      const surplus = await systemBalance.getSurplus();

      assert.equal(
        (await systemBalance.getDebt()).toString(),
        0,
        'there must not be a system debt if highest bid is above outstanding debt'
      );
      assert.equal(
        surplus.toString(),
        bid.minus(mintedAmount).toString(),
        'anything above minted amount should be system surplus'
      );
      assert.equal(
        (await qethToken.balanceOf(USER2)).toString(),
        depositValue.toString(),
        'bidder must have the collateral after execution'
      );
      assert.equal(
        (await stableQUSDToken.balanceOf(USER1)).toString(),
        amountOfStcToGenerate.toString(),
        'stc balance of liquidation victim must not change'
      );

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Executed');
      assert.equal(result.logs[0].args.user, USER1);
      assert.equal(result.logs[0].args.vaultId, vaultId);
      const auctionInfo = result.logs[0].args.info;
      assert.equal(auctionInfo.bidder, USER2);
      assert.equal(auctionInfo.highestBid.toString(), bid.toString());
    });

    it('should not be possible to execute if endTime has not passed', async () => {
      const price = toBN('100000000000000000000'); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN('1200000000000000000');
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      await setTime(
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).minus(toBN(50)).toNumber()
      );
      await truffleAssert.reverts(
        liquidationAuction.execute(USER1, vaultId),
        '[QEC-024004]-The auction is not finished yet, failed to execute the liquidation.'
      );

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);

      const auction = await liquidationAuction.auctions(USER1, vaultId);
      assert.equal(auction.status.toString(), AuctionStatus.ACTIVE);
    });
    it('should not be possible to execute if status is not ACTIVE', async () => {
      const price = toBN('100000000000000000000'); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN('1200000000000000000');
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      const passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).minus(2).toNumber();
      await setTime(passedAuction);
      await truffleAssert.reverts(
        liquidationAuction.execute(USER1, vaultId),
        '[QEC-024005]-The auction has already been executed.'
      );

      await checkInvariants(borrowingCore, stableQUSDToken, systemBalance);
    });
    it('should not be possible to reuse vault after executed liquidation (QDEV-2592)', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN('1500000000000000000'); // 1.5 QETH
      const amountOfStcToGenerate = toBN('999999999999999909');
      const liquidationTime = toBN(10);
      const bid = toBN(100);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });
      assert.equal((await stableQUSDToken.balanceOf(USER1)).toString(), amountOfStcToGenerate);

      await generateSomeSTC(USER2, bid);
      await stableQUSDToken.approve(liquidationAuction.address, bid, { from: USER2 });

      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await borrowingCore.updateCompoundRate(colKey);
      await setTime((await getCurrentBlockTime()) + liquidationTime.toNumber());
      await liquidationAuction.startAuction(USER1, vaultId, bid, { from: USER2 });

      const passedAuction =
        (await getCurrentBlockTime()) + liquidationTime.plus(LIQUIDATION_AUCTION_PERIOD).plus(2).toNumber();
      await setTime(passedAuction);
      await liquidationAuction.execute(USER1, vaultId);

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await truffleAssert.reverts(
        borrowingCore.depositCol(vaultId, depositValue, { from: USER1 }),
        '[QEC-021000]' /* -The vault is liquidated.'*/
      );
    });
  }).retries(4);
});
