const { accounts, accountsArray } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const ForeignChainTokenBridgeAdminProxy = artifacts.require('ForeignChainTokenBridgeAdminProxy');
const BridgeValidatorsMock = artifacts.require('BridgeValidatorsMock');

describe('Defi:ForeignChainTokenBridgeAdminProxy', () => {
  const reverter = new Reverter();

  let foreignChainTokenBridgeAdminProxy;
  let bridgeValidatorsMock;

  let OWNER;
  let USER1;
  let USER2;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    bridgeValidatorsMock = await BridgeValidatorsMock.new();

    foreignChainTokenBridgeAdminProxy = await ForeignChainTokenBridgeAdminProxy.new();
    await foreignChainTokenBridgeAdminProxy.initialize(bridgeValidatorsMock.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('updateTokenbridgeValidators', () => {
    it('should update token bridge validators if validators count < new validators', async () => {
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), []);

      const txReceipt = await foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(await accountsArray(3));
      assert.equal(txReceipt.logs.length, 3);

      const validatorCount = await bridgeValidatorsMock.validatorCount();
      const currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < validatorCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i));
      }
    });

    it('should correct update token bridge validators several times', async () => {
      let validators;
      let currentValidatorsCount = 3;
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), []);

      validators = await accountsArray(currentValidatorsCount);
      let txReceipt = await foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(validators);

      assert.equal(txReceipt.logs.length, currentValidatorsCount);

      let currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i));
      }

      let previousValidatorsCount = 3;
      currentValidatorsCount = 7;
      validators = await accountsArray(currentValidatorsCount);
      txReceipt = await foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(validators);

      assert.equal(txReceipt.logs.length, currentValidatorsCount - previousValidatorsCount);

      currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount - previousValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].event, 'ValidatorAdded');
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i + previousValidatorsCount));
      }

      previousValidatorsCount = 7;
      currentValidatorsCount = 10;
      validators = await accountsArray(currentValidatorsCount);
      txReceipt = await foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(validators);

      assert.equal(txReceipt.logs.length, currentValidatorsCount - previousValidatorsCount);

      currentBridgeValidatorsArr = await bridgeValidatorsMock.validatorList();
      for (let i = 0; i < currentValidatorsCount - previousValidatorsCount; i++) {
        assert.equal(currentBridgeValidatorsArr[i], await accounts(i));
        assert.equal(txReceipt.logs[i].event, 'ValidatorAdded');
        assert.equal(txReceipt.logs[i].args._validator, await accounts(i + previousValidatorsCount));
      }
    });
  });

  describe('test permissions', () => {
    it('should call methods only as owner', async () => {
      await foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(await accountsArray(5), { from: OWNER });

      await truffleAssert.reverts(
        foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(await accountsArray(3), { from: USER1 }),
        'Ownable: caller is not the owner'
      );

      await truffleAssert.reverts(
        foreignChainTokenBridgeAdminProxy.updateTokenbridgeValidators(await accountsArray(3), { from: USER2 }),
        'Ownable: caller is not the owner'
      );

      const testInstance = await BridgeValidatorsMock.at(foreignChainTokenBridgeAdminProxy.address);
      await truffleAssert.reverts(
        testInstance.addValidator(USER1, { from: USER1 }),
        'Ownable: caller is not the owner'
      );
    });
  });

  describe('test fallback', () => {
    it('should correct call different functions', async () => {
      const testInstance = await BridgeValidatorsMock.at(foreignChainTokenBridgeAdminProxy.address);

      await testInstance.addValidator(USER1);
      await testInstance.addValidator(USER2);

      assert.deepEqual(await bridgeValidatorsMock.validatorList(), await testInstance.validatorList());
      assert.deepEqual(await bridgeValidatorsMock.validatorList(), [USER1, USER2]);
    });
  });
});
