const { accounts } = require('../../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../../helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { setTime, getCurrentBlockTime } = require('../../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const FxPriceFeed = artifacts.require('./FxPriceFeed');
const CustomDecimalCoin = artifacts.require('../CustomDecimalsCoin');

describe('FxPriceFeed', () => {
  const reverter = new Reverter();

  const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000';

  let OWNER;
  let MAINTAINER1;
  let MAINTAINER2;
  let NOT_MAINTAINER;

  let fxPriceFeed;
  let baseToken;
  let quoteToken;

  before(async () => {
    OWNER = await accounts(0);
    MAINTAINER1 = await accounts(1);
    MAINTAINER2 = await accounts(2);
    NOT_MAINTAINER = await accounts(3);

    baseToken = (await CustomDecimalCoin.new('Token 1', 'TK1', 8)).address;
    quoteToken = (await CustomDecimalCoin.new('Token 2', 'TK2', 8)).address;
    fxPriceFeed = await FxPriceFeed.new('QBTC_QUSD', 100, [MAINTAINER1], baseToken, quoteToken);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('access', () => {
    it('should revert if pair is empty', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('', 100, [MAINTAINER1, MAINTAINER2], baseToken, quoteToken),
        '[QEC-019001]-Invalid pair, the initialization failed.'
      );
    });

    it('should revert if decimals is zero', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 0, [MAINTAINER1, MAINTAINER2], baseToken, quoteToken),
        '[QEC-019002]-Invalid decimal, the initialization failed.'
      );
    });

    it('should revert if base token is Address Zero', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 100, [MAINTAINER1, MAINTAINER2], ADDRESS_ZERO, quoteToken),
        '[QEC-019003]-Invalid base token address, the initialization failed.'
      );
    });

    it('should revert if quote token is Address Zero', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 100, [MAINTAINER1, MAINTAINER2], baseToken, ADDRESS_ZERO),
        '[QEC-019004]-Invalid quote token address, the initialization failed.'
      );
    });

    it('should revert if base token is not a contract address', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 100, [MAINTAINER1, MAINTAINER2], NOT_MAINTAINER, quoteToken),
        '[QEC-019005]-The base token address is not a contract, the initialization failed.'
      );
    });

    it('should revert if quote token is not a contract address', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 100, [MAINTAINER1, MAINTAINER2], baseToken, NOT_MAINTAINER),
        '[QEC-019006]-The quote token address is not a contract, the initialization failed.'
      );
    });
  });

  describe('setExchangeRate', () => {
    it('set price with not maintainer', async () => {
      const reason = '[QEC-019000]-Permission denied - only maintainers have access.';
      await truffleAssert.reverts(fxPriceFeed.setExchangeRate(200, 1, { from: NOT_MAINTAINER }), reason);
    });

    it('set price with maintainer', async () => {
      await fxPriceFeed.setExchangeRate(200, 1, { from: MAINTAINER1 });
      assert.equal((await fxPriceFeed.exchangeRate()).toString(), 200, 'set correct hash');
    });

    it('owner does no longer have permission to modify anything', async () => {
      await truffleAssert.reverts(
        fxPriceFeed.setExchangeRate(200, 1),
        '[QEC-019000]-Permission denied - only maintainers have access.'
      );
    });

    it('set 0 exchange rate', async () => {
      const reason = '[QEC-019007]-Invalid exchange rate, failed to set the exchange rate.';
      await truffleAssert.reverts(fxPriceFeed.setExchangeRate(0, 1, { from: MAINTAINER1 }), reason);
    });

    it('set exchange in past', async () => {
      await fxPriceFeed.setExchangeRate(100, 100, { from: MAINTAINER1 });
      const reason = '[QEC-019009]';
      await truffleAssert.reverts(fxPriceFeed.setExchangeRate(100, 100, { from: MAINTAINER1 }), reason);
      await truffleAssert.reverts(fxPriceFeed.setExchangeRate(100, 50, { from: MAINTAINER1 }), reason);
    });

    it('set exchange in future', async () => {
      await fxPriceFeed.setExchangeRate(100, 100, { from: MAINTAINER1 });
      const reason = '[QEC-019010]';
      await setTime((await getCurrentBlockTime()) + 420);
      await truffleAssert.reverts(fxPriceFeed.setExchangeRate(100, 420000, { from: MAINTAINER1 }), reason);
    });
  });

  describe('getMaintainers()', () => {
    it('should get the list of maintainers', async () => {
      const expectedMaintainers = [MAINTAINER1];
      const actualMaintainers = await fxPriceFeed.getMaintainers();

      assert.equal(expectedMaintainers.toString(), actualMaintainers.toString());
    });
  });

  describe('setMaintainer()', () => {
    it('should successfully set the maintainer', async () => {
      await fxPriceFeed.setMaintainer(MAINTAINER2, { from: MAINTAINER1 });
      const maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1, MAINTAINER2].toString());
    });

    it('should not set the maintainer if the caller is not the maintainer', async () => {
      await truffleAssert.reverts(
        fxPriceFeed.setMaintainer(MAINTAINER2, { from: NOT_MAINTAINER }),
        '[QEC-019000]-Permission denied - only maintainers have access.'
      );
      const maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1].toString());
    });

    it('owner does no longer have permission to modify anything', async () => {
      const maintainers = await fxPriceFeed.getMaintainers();

      await truffleAssert.reverts(
        fxPriceFeed.setMaintainer(MAINTAINER2),
        '[QEC-019000]-Permission denied - only maintainers have access.'
      );

      const actualMaintainers = await fxPriceFeed.getMaintainers();
      assert.equal(maintainers.toString(), actualMaintainers.toString());
    });
  });

  describe('leaveMaintainers()', () => {
    it('should successfully leave the maintainers', async () => {
      await fxPriceFeed.setMaintainer(MAINTAINER2, { from: MAINTAINER1 });
      let maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1, MAINTAINER2].toString());

      await fxPriceFeed.leaveMaintainers({ from: MAINTAINER2 });
      maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1].toString());
    });

    it('should revert if the caller is not a maintainer', async () => {
      await fxPriceFeed.setMaintainer(MAINTAINER2, { from: MAINTAINER1 });
      let maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1, MAINTAINER2].toString());

      await truffleAssert.reverts(
        fxPriceFeed.leaveMaintainers({ from: NOT_MAINTAINER }),
        '[QEC-035000]-Failed to remove the address from the address storage.'
      );
      maintainers = await fxPriceFeed.getMaintainers();

      assert.equal(maintainers.toString(), [MAINTAINER1, MAINTAINER2].toString());
    });
    it('should revert if the caller is a last maintainer', async () => {
      await truffleAssert.reverts(fxPriceFeed.leaveMaintainers({ from: MAINTAINER1 }));
      const maintainers = await fxPriceFeed.getMaintainers();
      assert.equal(maintainers.toString(), [MAINTAINER1].toString());
    });
  });

  describe('setFuturePricingTolerance', () => {
    it('should update tolerance', async () => {
      let tolerance;

      tolerance = await fxPriceFeed.futurePricingTolerance();
      assert.equal(tolerance.toString(), 60 * 30);

      await fxPriceFeed.setFuturePricingTolerance(420, { from: MAINTAINER1 });

      tolerance = await fxPriceFeed.futurePricingTolerance();
      assert.equal(tolerance.toString(), 420);
    });

    it('should failed ', async () => {
      await truffleAssert.reverts(
        fxPriceFeed.setFuturePricingTolerance(420),
        '[QEC-019000]-Permission denied - only maintainers have access.'
      );
    });
  });
});
