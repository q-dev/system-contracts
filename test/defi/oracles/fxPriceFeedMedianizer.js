const truffleAssert = require('truffle-assertions');

const { assert } = require('chai');
const { artifacts } = require('hardhat');

const Reverter = require('../../helpers/reverter');

const { accounts, accountsArray } = require('../../helpers/utils.js');
const { setTime, getCurrentBlockTime, setNextBlockTime } = require('../../helpers/hardhatTimeTraveller');

const FxPriceFeed = artifacts.require('FxPriceFeedMedianizer');
const CustomDecimalCoin = artifacts.require('CustomDecimalsCoin');

describe('FxPriceFeedMedianizer', () => {
  const reverter = new Reverter();

  const roundTime = 1000;

  let baseToken;
  let fxPriceFeedMedianizer;

  let OWNER;
  let SUBFEED1;
  let SUBFEED2;
  let SUBFEED3;
  let SUBFEED4;
  let NOT_SUBFEED;

  before(async () => {
    OWNER = await accounts(0);
    SUBFEED1 = await accounts(1);
    SUBFEED2 = await accounts(2);
    SUBFEED3 = await accounts(3);
    SUBFEED4 = await accounts(4);
    NOT_SUBFEED = await accounts(5);

    baseToken = (await CustomDecimalCoin.new('Token 1', 'TK1', 8)).address;
    fxPriceFeedMedianizer = await FxPriceFeed.new(
      'QBTC_QUSD',
      100,
      [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4],
      baseToken,
      roundTime,
      2,
      4
    );

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('access', () => {
    it('should revert if pair is empty', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('', 100, [SUBFEED1, SUBFEED2], baseToken, roundTime, 2, 4, { from: OWNER }),
        '[QEC-041001]-Invalid pair, the initialization failed.'
      );
    });

    it('should revert if decimals is zero', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 0, [SUBFEED1, SUBFEED2], baseToken, roundTime, 2, 4, { from: OWNER }),
        '[QEC-041002]-Invalid decimal, the initialization failed.'
      );
    });

    it('should revert if base token is not a contract address', async () => {
      await truffleAssert.reverts(
        FxPriceFeed.new('QBTC_QUSD', 100, [SUBFEED1, SUBFEED2], NOT_SUBFEED, roundTime, 2, 4, { from: OWNER }),
        '[QEC-041003]-The base token address is not a contract, the initialization failed.'
      );
    });
  });

  describe('submit()', () => {
    it('should correctly submit price', async () => {
      const currentTime = await getCurrentBlockTime();
      await setNextBlockTime(currentTime + 10);

      const tx = await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });

      truffleAssert.eventEmitted(tx, 'ExchangeRateSubmitted');

      assert.equal(await fxPriceFeedMedianizer.roundId(), 0);
      assert.isTrue(await fxPriceFeedMedianizer.isOpenRound());
      assert.equal(Number(await fxPriceFeedMedianizer.roundStarted()), currentTime + 10);

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), [200], 0, currentTime + 10);
    });

    it('should correctly update price', async () => {
      const currentTime = await getCurrentBlockTime();

      await setTime(currentTime + 10);

      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });
      await fxPriceFeedMedianizer.submit(205, { from: SUBFEED3 });
      const tx = await fxPriceFeedMedianizer.submit(205, { from: SUBFEED4 });

      truffleAssert.eventEmitted(tx, 'ExchangeRateUpdated');

      assert.equal(await fxPriceFeedMedianizer.roundId(), 1);
      assert.isFalse(await fxPriceFeedMedianizer.isOpenRound());

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), [200, 210, 205, 205], 205, currentTime + 10);
    });

    it('should recompute exchange rate after min submissions count is reached', async () => {
      const currentTime = await getCurrentBlockTime();

      await setTime(currentTime + 10);

      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });

      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 205);

      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED3 });

      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 210);

      await fxPriceFeedMedianizer.submit(205, { from: SUBFEED4 });

      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 207);

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), [200, 210, 210, 205], 207, currentTime + 10);
    });

    it('should close round if round time is over', async () => {
      const currentTime = await getCurrentBlockTime();

      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });

      assert.equal(await fxPriceFeedMedianizer.roundId(), 0);

      await setTime(currentTime + roundTime + 10);

      await fxPriceFeedMedianizer.submit(250, { from: SUBFEED1 });

      assert.equal(await fxPriceFeedMedianizer.roundId(), 1);

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), [200], 0, currentTime);
      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(1), [250], 0, currentTime + roundTime + 10);
    });

    it('should revert if non subfeed try to submit rate value', async () => {
      await truffleAssert.reverts(
        fxPriceFeedMedianizer.submit(200, { from: NOT_SUBFEED }),
        '[QEC-041000]-Permission denied - only subFeeds have access.'
      );
    });

    it('should revert if new rate less than the min rate value', async () => {
      await fxPriceFeedMedianizer.setMinRateValue(100);

      await truffleAssert.reverts(
        fxPriceFeedMedianizer.submit(99, { from: SUBFEED2 }),
        '[QEC-041004]-Submitted rate is less than minRateValue.'
      );
    });

    it('should revert if subfeed already submitted value', async () => {
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });

      await truffleAssert.reverts(
        fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 }),
        '[QEC-041005]-Already submitted in this round.'
      );
    });
  });

  describe('submit():median', () => {
    let subFeeds;

    beforeEach('setup', async () => {
      subFeeds = [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4];

      for (let i = 5; i < 8; i++) {
        const currentAddr = await accounts(i);

        await fxPriceFeedMedianizer.addSubFeed(currentAddr);

        subFeeds.push(currentAddr);
      }

      await fxPriceFeedMedianizer.setMinSubmissionsCount(5);
      await fxPriceFeedMedianizer.setMaxSubmissionsCount(5);

      assert.deepEqual(await fxPriceFeedMedianizer.getSubFeeds(), subFeeds);
      assert.equal(await fxPriceFeedMedianizer.minSubmissionsCount(), 5);
    });

    it('should correctly count exchange rate if minSubmissions count is odd', async () => {
      let currentTime = await getCurrentBlockTime();
      let rates = [150, 200, 220, 140, 180];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), rates, 180, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 180);

      currentTime = await getCurrentBlockTime();
      rates = [100, 110, 120, 120, 150];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(1), rates, 120, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 120);

      currentTime = await getCurrentBlockTime();
      rates = [200, 190, 180, 170, 150];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(2), rates, 180, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 180);
    });

    it('should correctly count exchange rate if minSubmissions count is even', async () => {
      await fxPriceFeedMedianizer.setMinSubmissionsCount(6);
      await fxPriceFeedMedianizer.setMaxSubmissionsCount(6);

      let currentTime = await getCurrentBlockTime();
      let rates = [200, 150, 180, 220, 140, 100];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(0), rates, 165, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 165);

      currentTime = await getCurrentBlockTime();
      rates = [100, 110, 120, 130, 150, 200];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(1), rates, 125, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 125);

      currentTime = await getCurrentBlockTime();
      rates = [180, 200, 190, 100, 170, 150];

      for (let i = 0; i < rates.length; i++) {
        await fxPriceFeedMedianizer.submit(rates[i], { from: subFeeds[i] });
      }

      checkRoundInfo(await fxPriceFeedMedianizer.getRoundInfo(2), rates, 175, currentTime);
      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 175);
    });
    it('should correctly works with more than 7 rates', async () => {
      const accounts = await accountsArray(10);
      const fx = await FxPriceFeed.new('QBTC_QUSD', 100, accounts, baseToken, roundTime, 2, 10, { from: OWNER });
      await fx.setMinSubmissionsCount(8);
      const currentTime = await getCurrentBlockTime();
      const rates = [170, 180, 170, 190, 180, 180, 180, 180];

      for (let i = 0; i < rates.length; i++) {
        await fx.submit(rates[i], { from: accounts[i] });
      }

      checkRoundInfo(await fx.getRoundInfo(0), rates, 180, currentTime);
      assert.equal(await fx.exchangeRate(), 180);
    });
  });

  describe('getSubFeeds()', () => {
    it('should get the list of maintainers', async () => {
      const expectedSubfeeds = [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4];
      const actualMaintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.deepEqual(expectedSubfeeds, actualMaintainers);
    });
  });

  describe('addSubFeed()', () => {
    it('should successfully set the subFeed', async () => {
      await fxPriceFeedMedianizer.addSubFeed(SUBFEED2);
      const maintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.equal(maintainers.toString(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4].toString());
    });

    it('should not set the subfeed if the caller is not the owner', async () => {
      await truffleAssert.reverts(fxPriceFeedMedianizer.addSubFeed(SUBFEED3, { from: NOT_SUBFEED }));
      const subFeeds = await fxPriceFeedMedianizer.getSubFeeds();

      assert.deepEqual(subFeeds.toString(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4].toString());
    });
  });

  describe('removeSubFeed()', () => {
    it('should successfully rm subfeed', async () => {
      await fxPriceFeedMedianizer.setMinSubmissionsCount(1);
      await fxPriceFeedMedianizer.addSubFeed(SUBFEED2);
      let maintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.equal(maintainers.toString(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4].toString());

      await fxPriceFeedMedianizer.removeSubFeed(SUBFEED2);
      maintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.equal(maintainers.toString(), [SUBFEED1, SUBFEED4, SUBFEED3].toString());
    });

    it('should revert if the parameter is not a maintainer', async () => {
      await fxPriceFeedMedianizer.addSubFeed(SUBFEED2);
      let maintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.equal(maintainers.toString(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4].toString());

      await truffleAssert.reverts(fxPriceFeedMedianizer.removeSubFeed(NOT_SUBFEED, { from: NOT_SUBFEED }));
      maintainers = await fxPriceFeedMedianizer.getSubFeeds();

      assert.equal(maintainers.toString(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4].toString());
    });

    it('should revert if the subfeedCount is minimal or less', async () => {
      await fxPriceFeedMedianizer.setMinSubmissionsCount(4);

      await truffleAssert.reverts(
        fxPriceFeedMedianizer.removeSubFeed(SUBFEED1),
        '[QEC-041006]-Count of subFeeds is minimal.'
      );

      assert.deepEqual(await fxPriceFeedMedianizer.getSubFeeds(), [SUBFEED1, SUBFEED2, SUBFEED3, SUBFEED4]);
    });
  });

  describe('getters/setters', () => {
    it('should return true if subfeed submitted rate in current round', async () => {
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });

      assert.isTrue(await fxPriceFeedMedianizer.isSubFeedSubmitted(0, SUBFEED1));
      assert.isFalse(await fxPriceFeedMedianizer.isSubFeedSubmitted(0, SUBFEED2));
    });

    it('should return correct rate and time', async () => {
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED2 });
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED3 });

      const updateTime = (await getCurrentBlockTime()) + 120;
      await setNextBlockTime(updateTime);

      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED4 });

      const rateAndTime = await fxPriceFeedMedianizer.getRateAndTime();
      assert.equal(rateAndTime[0], 200);
      assert.equal(rateAndTime[1], updateTime);
    });

    it('should set round time', async () => {
      await fxPriceFeedMedianizer.setRoundTime(1000);

      assert.equal(await fxPriceFeedMedianizer.roundTime(), 1000);

      await truffleAssert.reverts(fxPriceFeedMedianizer.setRoundTime(1000, { from: SUBFEED1 }));
    });

    it('should revert if min rate is submitted by not a owner', async () => {
      await truffleAssert.reverts(fxPriceFeedMedianizer.setMinRateValue(1000, { from: SUBFEED1 }));
    });

    it('should revert if min submissions count is set by not a owner', async () => {
      await truffleAssert.reverts(fxPriceFeedMedianizer.setMinSubmissionsCount(10, { from: SUBFEED1 }));
    });

    it('should revert if max submissions count is set by not a owner', async () => {
      await truffleAssert.reverts(fxPriceFeedMedianizer.setMaxSubmissionsCount(10, { from: SUBFEED1 }));
    });
  });

  function checkRoundInfo(roundInfoArr, expectedRates, expectedRoundRate, expectedStartedAt) {
    const ratesArr = roundInfoArr[0];

    for (let i = 0; i < ratesArr.length; i++) {
      assert.equal(ratesArr[i].toString(), expectedRates[i]);
    }

    assert.equal(roundInfoArr[1].toString(), expectedRoundRate);
    assert.closeTo(roundInfoArr[2].toNumber(), expectedStartedAt, 10);
  }
});
