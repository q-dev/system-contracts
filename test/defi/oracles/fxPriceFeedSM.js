const { accounts } = require('../../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const { toBN } = require('../../helpers/defiHelper');

const { setTime, getCurrentBlockTime, setNextBlockTime } = require('../../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const FxPriceFeed = artifacts.require('./FxPriceFeedMedianizer');
const FxPriceFeedSM = artifacts.require('./FxPriceFeedSMMock');
const CustomDecimalCoin = artifacts.require('../CustomDecimalsCoin');

describe('FxPriceFeedSM', () => {
  const reverter = new Reverter();

  const ADDRESS_NULL = '0x0000000000000000000000000000000000000000';

  const roundTime = 1000;
  const updatePeriod = toBN(4096);

  let fxPriceFeedMedianizer;
  let fxPriceFeedSM;
  let baseCurr;

  let OWNER;
  let SUBFEED1;
  let SUBFEED2;
  let NOT_SUBFEED;

  before(async () => {
    OWNER = await accounts(0);
    SUBFEED1 = await accounts(1);
    SUBFEED2 = await accounts(2);
    NOT_SUBFEED = await accounts(3);

    baseCurr = (await CustomDecimalCoin.new('Token 1', 'TK1', 8)).address;
    fxPriceFeedMedianizer = await FxPriceFeed.new('btc/usd', 100, [SUBFEED1, SUBFEED2], baseCurr, roundTime, 2, 5, {
      from: OWNER,
    });

    fxPriceFeedSM = await FxPriceFeedSM.new(fxPriceFeedMedianizer.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    reverter.revert();
  });

  describe('creation', () => {
    it('should return correct data', async () => {
      assert.equal(await fxPriceFeedSM.pair(), 'btc/usd');
      assert.equal(await fxPriceFeedSM.baseTokenAddr(), baseCurr);
      assert.equal(await fxPriceFeedSM.decimalPlaces(), 100);
    });

    it('should get exception if pass zero address to constructor', async () => {
      const reason = '[QEC-043000]-Zero price feed address.';

      await truffleAssert.reverts(FxPriceFeedSM.new(ADDRESS_NULL), reason);
    });
  });

  describe('updatePriceFeed', () => {
    it('should correctly update price feed address', async () => {
      const newPriceFeed = await FxPriceFeed.new('btc/usd', 100, [SUBFEED1, SUBFEED2], baseCurr, roundTime, 2, 5, {
        from: OWNER,
      });

      const tx = await fxPriceFeedSM.updatePriceFeed(newPriceFeed.address);

      truffleAssert.eventEmitted(tx, 'PriceFeedUpdated');

      assert.equal(await fxPriceFeedSM.priceFeed(), newPriceFeed.address);
    });

    it('should get exception if non owner try to call this function', async () => {
      const reason = 'Ownable: caller is not the owner';

      await truffleAssert.reverts(fxPriceFeedSM.updatePriceFeed(ADDRESS_NULL, { from: SUBFEED1 }), reason);
    });

    it('should get exception if pass zero address as a new price feed address', async () => {
      const reason = '[QEC-043001]-Zero new price feed address.';

      await truffleAssert.reverts(fxPriceFeedSM.updatePriceFeed(ADDRESS_NULL), reason);
    });
  });

  describe('setPriceValidFlag', () => {
    it('should correctly update price valid flag value', async () => {
      const tx = await fxPriceFeedSM.setPriceValidFlag(true);

      truffleAssert.eventEmitted(tx, 'PriceValidFlagUpdated');

      assert.equal(await fxPriceFeedSM.isPriceValid(), true);
    });

    it('should get exception if new falue equals to current value', async () => {
      const reason = '[QEC-043002]-Incorrect new value.';

      await truffleAssert.reverts(fxPriceFeedSM.setPriceValidFlag(false), reason);
    });

    it('should get exception if non owner try to call this function', async () => {
      const reason = 'Ownable: caller is not the owner';

      await truffleAssert.reverts(fxPriceFeedSM.setPriceValidFlag(false, { from: SUBFEED1 }), reason);
    });
  });

  describe('pause/unpause', () => {
    it('should correctly pause and unpause exchange rate updating', async () => {
      const reason = 'Pausable: paused';

      await fxPriceFeedSM.pause();

      await truffleAssert.reverts(fxPriceFeedSM.updateExchangeRate(), reason);

      await fxPriceFeedSM.unpause();

      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });

      await setTime(updatePeriod.times(2).plus(100).toNumber());

      await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });
    });

    it('should get exception if non owner try to call this function', async () => {
      const reason = 'Ownable: caller is not the owner';

      await truffleAssert.reverts(fxPriceFeedSM.pause({ from: SUBFEED1 }), reason);

      await truffleAssert.reverts(fxPriceFeedSM.unpause({ from: SUBFEED1 }), reason);
    });
  });

  describe('updateExchangeRate', () => {
    beforeEach('setup', async () => {
      await fxPriceFeedMedianizer.submit(200, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });

      assert.equal(await fxPriceFeedMedianizer.exchangeRate(), 205);
    });

    it('should correctly update exchange rate', async () => {
      await fxPriceFeedSM.setPriceValidFlag(true);

      await setTime(updatePeriod.times(2).plus(100).toNumber());

      let updateTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updateTime);

      const tx = await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });

      truffleAssert.eventEmitted(tx, 'ExchangeRateUpdated');

      assert.equal(toBN(await fxPriceFeedSM.exchangeRate()).toString(), 0);
      assert.equal(toBN(await fxPriceFeedSM.nextExchangeRate()).toString(), 205);
      assert.equal(toBN(await fxPriceFeedSM.updateTime()).toString(), updateTime.toString());

      await setTime(updatePeriod.times(3).toNumber());

      await fxPriceFeedMedianizer.submit(220, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });

      updateTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updateTime);

      await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });

      assert.equal(toBN(await fxPriceFeedSM.exchangeRate()).toString(), 205);
      assert.equal(toBN(await fxPriceFeedSM.nextExchangeRate()).toString(), 215);
      assert.equal(toBN(await fxPriceFeedSM.updateTime()).toString(), updateTime.toString());
    });

    // Initially price valid flag is false
    it('should correctly update exchange rate if price valid flag is false', async () => {
      await setTime(updatePeriod.times(2).plus(100).toNumber());

      let updateTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updateTime);

      const tx = await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });

      truffleAssert.eventEmitted(tx, 'ExchangeRateUpdated');

      assert.equal(toBN(await fxPriceFeedSM.exchangeRate()).toString(), 0);
      assert.equal(toBN(await fxPriceFeedSM.nextExchangeRate()).toString(), 205);
      assert.equal(toBN(await fxPriceFeedSM.updateTime()).toString(), updateTime.toString());

      await setTime(updatePeriod.times(3).toNumber());

      await fxPriceFeedMedianizer.submit(220, { from: SUBFEED1 });
      await fxPriceFeedMedianizer.submit(210, { from: SUBFEED2 });

      updateTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updateTime);

      await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });

      assert.equal(toBN(await fxPriceFeedSM.exchangeRate()).toString(), 0);
      assert.equal(toBN(await fxPriceFeedSM.nextExchangeRate()).toString(), 215);
      assert.equal(toBN(await fxPriceFeedSM.updateTime()).toString(), updateTime.toString());
    });

    it('should correctly update price for different OSM contracts', async () => {
      const baseCurr2 = (await CustomDecimalCoin.new('Token 2', 'TK2', 18)).address;
      const fxPriceFeedMedianizer2 = await FxPriceFeed.new(
        'eth/usd',
        100,
        [SUBFEED1, SUBFEED2],
        baseCurr2,
        roundTime,
        2,
        5,
        { from: OWNER }
      );

      await setTime(updatePeriod.times(2).plus(100).toNumber());

      const fxPriceFeedSM2 = await FxPriceFeedSM.new(fxPriceFeedMedianizer2.address);

      const updatePeriodFor1 = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updatePeriodFor1);

      await fxPriceFeedSM.updateExchangeRate({ from: NOT_SUBFEED });

      await fxPriceFeedMedianizer2.submit(220, { from: SUBFEED1 });
      await fxPriceFeedMedianizer2.submit(210, { from: SUBFEED2 });

      const updatePeriodFor2 = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(updatePeriodFor2);

      await fxPriceFeedSM2.updateExchangeRate({ from: NOT_SUBFEED });

      assert.equal(toBN(await fxPriceFeedSM.updateTime()).toString(), updatePeriodFor1.toString());
      assert.equal(toBN(await fxPriceFeedSM2.updateTime()).toString(), updatePeriodFor2.toString());

      assert.equal(toBN(await fxPriceFeedSM.exchangeRate()).toString(), 0);
      assert.equal(toBN(await fxPriceFeedSM.nextExchangeRate()).toString(), 205);

      assert.equal(toBN(await fxPriceFeedSM2.exchangeRate()).toString(), 0);
      assert.equal(toBN(await fxPriceFeedSM2.nextExchangeRate()).toString(), 215);
    });

    it('should get exception if update period time does not reached', async () => {
      const reason = '[QEC-043003]-Time for the update has not been reached.';

      await truffleAssert.reverts(fxPriceFeedSM.updateExchangeRate(), reason);
    });
  });

  describe('getPrevEra', () => {
    it('should return correct era with different timestamps', async () => {
      let currentTimestamp = await getCurrentBlockTime();
      let era = currentTimestamp - (currentTimestamp % updatePeriod);

      assert.equal(era.toString(), 0);

      await setTime(updatePeriod.times(1.5).toNumber());

      currentTimestamp = await getCurrentBlockTime();
      era = currentTimestamp - (currentTimestamp % updatePeriod);

      assert.equal(era.toString(), updatePeriod.toString());

      await setTime(updatePeriod.times(2).plus(100).toNumber());

      currentTimestamp = await getCurrentBlockTime();
      era = currentTimestamp - (currentTimestamp % updatePeriod);

      assert.equal(era.toString(), updatePeriod.times(2).toString());
    });
  });
});
