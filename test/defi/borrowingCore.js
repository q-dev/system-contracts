const BigNumber = require('bignumber.js');
const truffleAssert = require('truffle-assertions');

const { assert } = require('chai');
const { artifacts } = require('hardhat');

const Reverter = require('../helpers/reverter');
const makeProxy = require('../helpers/makeProxy');

const { accounts } = require('../helpers/utils.js');
const {
  getCurrentBlockTime,
  setTime,
  setNextBlockTime,
  getPlusOneBlockTime,
} = require('../helpers/hardhatTimeTraveller');
const {
  toBN,
  getTransferFeePercentage,
  getAmountAfterTransfer,
  getNextCompoundRate,
  normalizeAmount,
  denormalizeAmount,
} = require('../helpers/defiHelper');

const FxPriceFeed = artifacts.require('FxPriceFeed');
const ERC20WithFee = artifacts.require('ERC20WithFee');
const StableToken = artifacts.require('StableCoinMock');
const Parameters = artifacts.require('EPDR_Parameters');
const BorrowingCore = artifacts.require('BorrowingCore');
const SystemBalance = artifacts.require('SystemBalance');
const ContractRegistry = artifacts.require('ContractRegistry');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const CustomDecimalsCoin = artifacts.require('CustomDecimalsCoin');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');

describe('Defi:BorrowingCore', () => {
  const reverter = new Reverter();

  let crKeeperFactory;
  let borrowingCore;

  let colToken;
  let colPriceFeed;
  let colWithFeePriceFeed;
  let expertsParameters;
  let registry;
  let stableQUSDToken;
  let systemBalance;

  let regVaultCompoundRateKeeper;

  let colTokenWithFee;

  const DECIMAL = require('../helpers/defiHelper').DECIMAL;
  const ONE = new BigNumber(1e18);
  const NORMALIZATION_FACTOR = new BigNumber(1e9);

  const INTEREST_RATE = DECIMAL.times(0.01);
  const COL_RATIO = DECIMAL.multipliedBy(2);
  const LIQUIDATION_RATIO = DECIMAL.multipliedBy(1.5);

  const COL_NAME = 'QBTC';
  const COL_DECIMALS = 8;
  const COL_ONE = toBN(10).pow(COL_DECIMALS);

  const COL_WITH_FEE_NAME = 'FT';
  const COL_WITH_FEE_DECIMALS = 18;
  const COL_WITH_FEE_ONE = toBN(10).pow(COL_WITH_FEE_DECIMALS);

  const toDepositAmount = COL_ONE;
  const toDepositAmountWithFee = COL_WITH_FEE_ONE;

  let feePercentage;

  const regularTokenVaultId = 0;
  const tokenWithFeeVaultId = 1;

  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;
  let LIQUIDATION_AUCTION;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    PARAMETERS_VOTING = await accounts(3);
    LIQUIDATION_AUCTION = await accounts(4);

    // never return exponential notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], forwarder);
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);

    borrowingCore = await makeProxy(registry.address, BorrowingCore);
    await borrowingCore.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.borrowing', borrowingCore.address);

    // Fee is 1% == 100 basic points. See ERC20WithFee.sol
    colTokenWithFee = await ERC20WithFee.new('Fee Token', 'FT', COL_WITH_FEE_DECIMALS, 100);
    await expertsParameters.setAddr(`governed.EPDR.${COL_WITH_FEE_NAME}_address`, colTokenWithFee.address, {
      from: PARAMETERS_VOTING,
    });

    colToken = await CustomDecimalsCoin.new(COL_NAME, COL_NAME, toBN(COL_DECIMALS));
    await expertsParameters.setAddr(`governed.EPDR.${COL_NAME}_address`, colToken.address, { from: PARAMETERS_VOTING });

    const configCollateralToken = async (tokenAddress, tokenName, exchangeRate) => {
      const exchangeRateDecimals = 18;
      const priceFeed = await FxPriceFeed.new(
        `${tokenName}_QUSD`,
        exchangeRateDecimals,
        [OWNER],
        tokenAddress,
        stableQUSDToken.address
      );
      await priceFeed.setExchangeRate(
        toBN(exchangeRate).multipliedBy(toBN(10).pow(exchangeRateDecimals)),
        await getCurrentBlockTime(),
        { from: OWNER }
      );

      await expertsParameters.setAddr(`governed.EPDR.${tokenName}_QUSD_oracle`, priceFeed.address, {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint(`governed.EPDR.${tokenName}_QUSD_collateralizationRatio`, COL_RATIO, {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint(`governed.EPDR.${tokenName}_QUSD_liquidationRatio`, LIQUIDATION_RATIO, {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint(`governed.EPDR.${tokenName}_QUSD_ceiling`, ONE.multipliedBy(10000000), {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint(`governed.EPDR.${tokenName}_QUSD_interestRate`, INTEREST_RATE, {
        from: PARAMETERS_VOTING,
      });

      return priceFeed;
    };

    // 1 col = 200 QUSD
    colPriceFeed = await configCollateralToken(colToken.address, COL_NAME, 200);
    colWithFeePriceFeed = await configCollateralToken(colTokenWithFee.address, COL_WITH_FEE_NAME, 200);

    await expertsParameters.setUint('governed.EPDR.QUSD_step', toBN(10), { from: PARAMETERS_VOTING });

    await registry.setAddress('defi.QUSD.liquidationAuction', LIQUIDATION_AUCTION);

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);

    await colToken.mint(USER1, toDepositAmount);
    await colToken.approve(borrowingCore.address, toDepositAmount, { from: USER1 });

    await colTokenWithFee.mint(USER1, toDepositAmountWithFee);
    await colTokenWithFee.approve(borrowingCore.address, toDepositAmountWithFee, { from: USER1 });

    feePercentage = await getTransferFeePercentage(colTokenWithFee, OWNER, USER1);

    await borrowingCore.createVault(COL_NAME, { from: USER1 });
    await borrowingCore.createVault(COL_WITH_FEE_NAME, { from: USER1 });

    const compoundRateKeeperAddress = await borrowingCore.compoundRateKeeper(COL_NAME);
    regVaultCompoundRateKeeper = await CompoundRateKeeper.at(compoundRateKeeperAddress);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createVault', () => {
    it('should be possible to create new vault with valid collateral', async () => {
      const txReceipt = await borrowingCore.createVault(COL_NAME, { from: USER2 });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'VaultCreated');
      assert.equal(txReceipt.logs[0].args.user, USER2);
      assert.equal(txReceipt.logs[0].args.vaultId, 0);
      assert.equal(txReceipt.logs[0].args.colKey, COL_NAME);

      const userVault = await borrowingCore.userVaults(USER2, 0);
      assert.equal(userVault.colKey, COL_NAME);
      assert.equal(userVault.colAsset, 0);
      assert.equal(userVault.normalizedDebt, 0);

      const vaultsCount = await borrowingCore.userVaultsCount(USER2);
      assert.equal(vaultsCount.toString(), '1');
    });

    it('should be possible to create few vaults with same collateral', async () => {
      await borrowingCore.createVault(COL_NAME, { from: USER2 });
      await borrowingCore.createVault(COL_NAME, { from: USER2 });

      const vaultsCount = await borrowingCore.userVaultsCount(USER2);
      assert.equal(vaultsCount.toString(), '2');
    });

    it('should be possible to create few vaults with different collaterals', async () => {
      await borrowingCore.createVault(COL_NAME, { from: USER2 });

      const firstVaultColKey = (await borrowingCore.userVaults(USER2, 0)).colKey;
      assert.equal(firstVaultColKey, COL_NAME);

      const COL2_NAME = 'QBTC';
      await expertsParameters.setAddr(`governed.EPDR.${COL2_NAME}_address`, colToken.address, {
        from: PARAMETERS_VOTING,
      });

      await borrowingCore.createVault(COL2_NAME, { from: USER2 });

      const secondVaultColKey = (await borrowingCore.userVaults(USER2, 1)).colKey;
      assert.equal(secondVaultColKey, COL2_NAME);
    });

    it('should not be possible to create fault with invalid collateral', async () => {
      await truffleAssert.reverts(
        borrowingCore.createVault('NON_EXISTING', { from: USER2 }),
        `[QEC-009007]-The parameter 'governed.EPDR.NON_EXISTING_address' does not exist.`
      );
    });
  });

  describe('depositCol', () => {
    it('should be possible to deposit valid collateral', async () => {
      const txReceipt = await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });

      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ColDeposited');
      assert.equal(txReceipt.logs[0].args.user, USER1);
      assert.equal(txReceipt.logs[0].args.vaultId, regularTokenVaultId);
      assert.equal(txReceipt.logs[0].args.amount.toString(), toDepositAmount);

      const userVaultBalance = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).colAsset.toString();
      assert.equal(userVaultBalance, toDepositAmount);

      const borrowingBalance = await colToken.balanceOf(borrowingCore.address);
      assert.equal(userVaultBalance, borrowingBalance.toString());
    });

    it('should have a correct balance after depositing the token with the fees', async () => {
      await borrowingCore.depositCol(tokenWithFeeVaultId, toDepositAmountWithFee, { from: USER1 });

      const actuallyTransferredBalance = await getAmountAfterTransfer(feePercentage, toDepositAmountWithFee);

      const userVaultBalance = (await borrowingCore.userVaults(USER1, tokenWithFeeVaultId)).colAsset.toString();
      assert.equal(userVaultBalance, actuallyTransferredBalance);

      const borrowingBalance = await colTokenWithFee.balanceOf(borrowingCore.address);
      assert.equal(userVaultBalance, borrowingBalance.toString());
    });

    it('should not be possible to deposit to non-existing vault', async () => {
      await truffleAssert.reverts(
        borrowingCore.depositCol(99, toDepositAmount, { from: USER1 }),
        '[QEC-021005]-The vault does not exist.'
      );
    });

    it('should not be possible to deposit zero value', async () => {
      await truffleAssert.reverts(
        borrowingCore.depositCol(regularTokenVaultId, 0, { from: USER1 }),
        '[QEC-021004]-Collateral deposit must not be zero.'
      );
    });

    it('should not be possible to deposit if transferFrom fails', async () => {
      await truffleAssert.reverts(
        borrowingCore.depositCol(regularTokenVaultId, toDepositAmount.pow(2), { from: USER1 }),
        'ERC20: transfer amount exceeds balance'
      );
    });

    it('should not be possible to deposit if vault is liquidated', async () => {
      await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });

      await borrowingCore.generateStc(regularTokenVaultId, ONE.multipliedBy(100), { from: USER1 });

      // at interest rate of 1% per second, this more than doubles the debt
      await setTime((await getCurrentBlockTime()) + 1000);

      await borrowingCore.updateCompoundRate(COL_NAME);

      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });

      await truffleAssert.reverts(
        borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 }),
        '[QEC-021000]-The vault is liquidated.'
      );
    });
  });

  describe('generateStc', () => {
    beforeEach('setup', async () => {
      await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });
      await borrowingCore.depositCol(tokenWithFeeVaultId, toDepositAmount, { from: USER1 });
    });

    it('should be possible to generateStc', async () => {
      const vaultStats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);
      const amountOfStcToGenerate = toBN(vaultStats.stcStats.availableToBorrow);

      const txReceipt = await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'StcGenerated');
      assert.equal(txReceipt.logs[0].args.user, USER1);
      assert.equal(txReceipt.logs[0].args.vaultId, regularTokenVaultId);
      assert.equal(txReceipt.logs[0].args.amount.toString(), amountOfStcToGenerate.toString());

      const expectedColRatio = await getRegVaultColRatio(USER1, regularTokenVaultId, 0);

      const actualColRatio = await borrowingCore.getCurrentColRatio(USER1, regularTokenVaultId);
      assert.equal(actualColRatio.toString(), expectedColRatio.toString());

      const totalGeneratedStc = await borrowingCore.totalStcBackedByCol(colToken.address);
      assert.equal(totalGeneratedStc.toString(), amountOfStcToGenerate.toString());

      const fullDebt = await borrowingCore.getFullDebt(USER1, regularTokenVaultId);
      assert.equal(fullDebt.toString(), amountOfStcToGenerate.toString());
    });

    it('should be possible to generateStc with col with fee', async () => {
      const vaultStats = await borrowingCore.getVaultStats(USER1, tokenWithFeeVaultId);
      const amountOfStcToGenerate = toBN(vaultStats.stcStats.availableToBorrow);

      await truffleAssert.passes(
        borrowingCore.generateStc(tokenWithFeeVaultId, amountOfStcToGenerate, { from: USER1 }),
        'passes'
      );
    });

    it('should be possible to generateStableTokens second time with enough collateral', async () => {
      const oldCompoundRate = await regVaultCompoundRateKeeper.getCurrentRate();
      const lastUpdate = await regVaultCompoundRateKeeper.getLastUpdate();

      const amountOfStcToGenerate = ONE;
      await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });
      await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });

      const updateCompoundRateTime = await getPlusOneBlockTime();
      await setNextBlockTime(updateCompoundRateTime);

      await borrowingCore.updateCompoundRate(COL_NAME);

      const period = updateCompoundRateTime - lastUpdate;
      const expectedRate = getNextCompoundRate(INTEREST_RATE, period, oldCompoundRate).toString();

      const expectedDebt = toBN(2).times(expectedRate);

      const fullDebt = toBN(await borrowingCore.getFullDebt(USER1, regularTokenVaultId));
      assert.equal(fullDebt.toString(), expectedDebt.toString());

      const totalGeneratedStc = await borrowingCore.totalStcBackedByCol(colToken.address);
      assert.equal(totalGeneratedStc.toString(), amountOfStcToGenerate.times(2));
    });

    it('should not be possible to generateSTC if vault is liquidated', async () => {
      const amountOfStcToGenerate = ONE.times(100);
      await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });

      // make it undercollateralized by adding some interest
      await setTime((await getCurrentBlockTime()) + 100);
      await borrowingCore.updateCompoundRate(COL_NAME);
      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });

      await truffleAssert.reverts(
        borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 }),
        '[QEC-021000]-The vault is liquidated.'
      );
    });

    it('should not be possible to generate less than the acceptable minimum', async () => {
      await truffleAssert.reverts(
        borrowingCore.generateStc(regularTokenVaultId, toBN(1), { from: USER1 }),
        '[QEC-021007]-The amount of STC is less than the acceptable minimum.'
      );
    });

    it('should fail to borrow more than availableToBorrow (undercollateralized)', async () => {
      const vaultStats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);
      const amountOfStcToGenerate = toBN(vaultStats.stcStats.availableToBorrow);

      await truffleAssert.reverts(
        borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate.plus(1), { from: USER1 }),
        '[QEC-021009]-Not enough collateral.'
      );

      await truffleAssert.passes(
        borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 }),
        'passes'
      );
    });

    it('should not be possible to generate if total stc backed by collateral exceeds limit', async () => {
      const amountOfStcToGenerate = ONE;
      await expertsParameters.setUint('governed.EPDR.QBTC_QUSD_ceiling', amountOfStcToGenerate.minus(1), {
        from: PARAMETERS_VOTING,
      });

      await truffleAssert.reverts(
        borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 }),
        '[QEC-021008]-The amount of STC exceeds the ceiling.'
      );
    });

    it('QDEV-2194 should handle remainder during normalization correctly', async () => {
      await borrowingCore.updateCompoundRate(COL_NAME);
      const amountOfStcToGenerate = ONE.multipliedBy(0.1);

      const txReceipt = await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });

      const oldCompoundRate = await regVaultCompoundRateKeeper.getCurrentRate();
      const lastUpdate = await regVaultCompoundRateKeeper.getLastUpdate();

      // we are loosing remainder here
      const actuallyMintedStc = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).mintedAmount;
      assert.equal(actuallyMintedStc.toString(), amountOfStcToGenerate.toString());
      assert.equal(txReceipt.logs[0].args.amount.toString(), amountOfStcToGenerate.toString());

      const expectedNormalizedDebt = normalizeAmount(amountOfStcToGenerate, oldCompoundRate);
      const actualNormalizedDebt = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).normalizedDebt;
      assert.equal(actualNormalizedDebt.toString(), expectedNormalizedDebt.toString());

      const expectedDebtBeforeUpdate = ONE.multipliedBy(0.1);
      const actualDebtBeforeUpdate = await borrowingCore.getFullDebt(USER1, regularTokenVaultId);
      assert.equal(actualDebtBeforeUpdate.toString(), expectedDebtBeforeUpdate.toString());

      const updateCompoundRateTime = await getPlusOneBlockTime();
      await setNextBlockTime(updateCompoundRateTime);
      await borrowingCore.updateCompoundRate(COL_NAME);

      await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });

      const period = updateCompoundRateTime - lastUpdate;
      const expectedRate = getNextCompoundRate(INTEREST_RATE, period, oldCompoundRate).toString();
      const expectedNewNormalizedDebt = toBN(normalizeAmount(amountOfStcToGenerate, expectedRate)).plus(
        expectedNormalizedDebt
      );
      const actualNewNormalizedDebt = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).normalizedDebt;
      assert.equal(actualNewNormalizedDebt.toString(), expectedNewNormalizedDebt.toString());

      const expectedNewDebt = denormalizeAmount(expectedNewNormalizedDebt, expectedRate);
      const actualFullDebt = toBN(await borrowingCore.getFullDebt(USER1, regularTokenVaultId));
      assert.equal(actualFullDebt.toString(), expectedNewDebt.toString());

      const expectedMintedStcAfterUpdate = amountOfStcToGenerate.times(2);
      const actualMintedStcAfterUpdate = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).mintedAmount;
      assert.equal(actualMintedStcAfterUpdate.toString(), expectedMintedStcAfterUpdate.toString());
    }).retries(5);
  });

  describe('withdrawCol', () => {
    beforeEach('setup', async () => {
      await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });
      await borrowingCore.depositCol(tokenWithFeeVaultId, toDepositAmountWithFee, { from: USER1 });

      // Generate STC only for the first vault
      await borrowingCore.generateStc(regularTokenVaultId, ONE, { from: USER1 });
    });

    it('should be possible to withdraw if left col is enough to cover minColRatio', async () => {
      let stats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);

      const txReceipt = await borrowingCore.withdrawCol(regularTokenVaultId, stats.colStats.withdrawableAmount, {
        from: USER1,
      });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ColWithdrawn');
      assert.equal(txReceipt.logs[0].args.amount.toString(), stats.colStats.withdrawableAmount);

      stats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);
      assert.equal(
        stats.colStats.withdrawableAmount,
        '0',
        'after withdrawing complete withdrawal amount, this must be zero'
      );
    });

    it('should be possible to withdraw col with fee', async () => {
      let stats = await borrowingCore.getVaultStats(USER1, tokenWithFeeVaultId);

      await truffleAssert.passes(
        borrowingCore.withdrawCol(tokenWithFeeVaultId, stats.colStats.withdrawableAmount, { from: USER1 }),
        'passes'
      );

      stats = await borrowingCore.getVaultStats(USER1, tokenWithFeeVaultId);
      assert.equal(
        stats.colStats.withdrawableAmount,
        '0',
        'after withdrawing complete withdrawal amount, this must be zero'
      );
    });

    it('should not be possible to withdraw if left col is sharply not enough to cover minColRatio', async () => {
      const withdrawValue = COL_ONE.times(0.999);
      await truffleAssert.reverts(
        borrowingCore.withdrawCol(regularTokenVaultId, withdrawValue, { from: USER1 }),
        '[QEC-021011]-Dropping below minimum collateralization ratio, withdrawal failed.'
      );
    });

    it('should not be possible to withdraw if vault is liquidated', async () => {
      await setTime((await getCurrentBlockTime()) + 1000);
      await borrowingCore.updateCompoundRate(COL_NAME);
      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });

      await truffleAssert.reverts(
        borrowingCore.withdrawCol(regularTokenVaultId, toBN(1), { from: USER1 }),
        '[QEC-021000]-The vault is liquidated.'
      );
    });

    it('should not be possible to withdraw more than existing col', async () => {
      const withdrawValue = COL_ONE.times(2);
      await truffleAssert.reverts(
        borrowingCore.withdrawCol(regularTokenVaultId, withdrawValue, { from: USER1 }),
        '[QEC-021018]-Withdrawal amount is greater than the available collateral asset.'
      );
    });
  });

  describe('payBackStc', () => {
    let initialSupply;
    let amountOfStcToGenerate;

    beforeEach('setup', async () => {
      amountOfStcToGenerate = ONE;

      await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });
      await borrowingCore.depositCol(tokenWithFeeVaultId, toDepositAmountWithFee, { from: USER1 });

      initialSupply = (await stableQUSDToken.totalSupply()).toString();

      // Generate STC only for the first vault
      await borrowingCore.generateStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });
    });

    it('should be paying back full amount correctly', async () => {
      const stcTotalSupply = await stableQUSDToken.totalSupply();
      const expectedStcTotalSupply = toBN(initialSupply).plus(amountOfStcToGenerate);
      assert.equal(stcTotalSupply.toString(), expectedStcTotalSupply.toString());

      await stableQUSDToken.increaseAllowance(borrowingCore.address, amountOfStcToGenerate, { from: USER1 });
      await borrowingCore.payBackStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 });

      const actualSupply = await stableQUSDToken.totalSupply();
      assert.equal(actualSupply.toString(), initialSupply, 'stable coin supply must be unaffected');

      const actualTotalStcBackedByCol = await borrowingCore.totalStcBackedByCol(colToken.address);
      assert.equal(actualTotalStcBackedByCol.toString(), '0', 'no more tokens must backed by the col must be left');

      const actualFullDebt = await borrowingCore.getFullDebt(USER1, regularTokenVaultId);
      assert.equal(actualFullDebt.toString(), '0', 'no more outstanding debt must be left in the vault');

      const userVaultStats = await borrowingCore.userVaults(USER1, regularTokenVaultId);
      assert.equal(userVaultStats.mintedAmount.toString(), '0', 'no more minted coins must be left to be burnt');
      assert.equal(userVaultStats.colAsset.toString(), toDepositAmount.toString(), 'col balance must be unaffected');
    });

    it('should handle repay amount > accrued interest', async () => {
      const oldCompoundRate = await regVaultCompoundRateKeeper.getCurrentRate();
      const lastUpdate = await regVaultCompoundRateKeeper.getLastUpdate();

      const updateCompoundRateTime = await getPlusOneBlockTime();
      await setNextBlockTime(updateCompoundRateTime);
      await borrowingCore.updateCompoundRate(COL_NAME);

      const period = updateCompoundRateTime - lastUpdate;
      const expectedRate = getNextCompoundRate(INTEREST_RATE, period, oldCompoundRate).toString();
      const expectedNormalizedDebt = toBN(normalizeAmount(amountOfStcToGenerate, oldCompoundRate));

      const expectedFullDebtBeforePayback = denormalizeAmount(expectedNormalizedDebt, expectedRate);
      let actualFullDebt = toBN(await borrowingCore.getFullDebt(USER1, regularTokenVaultId));
      assert.equal(actualFullDebt.toString(), expectedFullDebtBeforePayback.toString());

      const paybackAmount = ONE.times(0.5);
      await stableQUSDToken.approve(borrowingCore.address, paybackAmount, { from: USER1 });
      await borrowingCore.payBackStc(regularTokenVaultId, paybackAmount, { from: USER1 });

      actualFullDebt = toBN(await borrowingCore.getFullDebt(USER1, regularTokenVaultId));
      assert.equal(actualFullDebt.toString(), expectedFullDebtBeforePayback.minus(paybackAmount).toString());

      const surplus = toBN(await systemBalance.getSurplus());
      const expectedSurplus = expectedFullDebtBeforePayback.minus(amountOfStcToGenerate);
      assert.equal(surplus.toString(), expectedSurplus.toString());

      const totalSupply = await stableQUSDToken.totalSupply();
      // surplus == accrued interest
      const expectedTotalSupply = toBN(initialSupply).plus(amountOfStcToGenerate).minus(paybackAmount).plus(surplus);

      assert.equal(totalSupply.toString(), expectedTotalSupply.toString(), 'total supply has changed');

      const actualTotalStcBackedByCol = await borrowingCore.totalStcBackedByCol(colToken.address);
      const expectedTotalStcBackedByCol = toBN(amountOfStcToGenerate).minus(paybackAmount).plus(surplus);
      assert.equal(actualTotalStcBackedByCol.toString(), expectedTotalStcBackedByCol.toString());

      const actualMintedAmount = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).mintedAmount;
      assert.equal(actualMintedAmount.toString(), expectedTotalStcBackedByCol.toString());
    }).retries(5);

    it('should handle repay amount < accrued interest', async () => {
      const oldCompoundRate = await regVaultCompoundRateKeeper.getCurrentRate();
      const lastUpdate = await regVaultCompoundRateKeeper.getLastUpdate();

      const updateCompoundRateTime = (await getCurrentBlockTime()) + 120;
      await setNextBlockTime(updateCompoundRateTime);
      await borrowingCore.updateCompoundRate(COL_NAME);

      const paybackAmount = ONE.times(0.5);
      await stableQUSDToken.approve(borrowingCore.address, paybackAmount, { from: USER1 });
      await borrowingCore.payBackStc(regularTokenVaultId, paybackAmount, { from: USER1 });

      const period = updateCompoundRateTime - lastUpdate;
      const expectedRate = getNextCompoundRate(INTEREST_RATE, period, oldCompoundRate).toString();
      const expectedNormalizedDebt = toBN(normalizeAmount(paybackAmount, expectedRate));

      const initialNormalizedDebt = toBN(normalizeAmount(amountOfStcToGenerate, oldCompoundRate));
      const normalizedDebtAfterPayback = initialNormalizedDebt.minus(expectedNormalizedDebt);
      const expectedFullDebtAfterPayback = denormalizeAmount(normalizedDebtAfterPayback, expectedRate);
      let actualFullDebt = toBN(await borrowingCore.getFullDebt(USER1, regularTokenVaultId));
      assert.equal(actualFullDebt.toString(), expectedFullDebtAfterPayback.toString());

      const actualSurplus = await systemBalance.getSurplus();
      assert.equal(actualSurplus.toString(), paybackAmount.toString(), 'surplus must be equal to payback amount');

      // total supply must not change, because user has paid back less than accrued interest
      const expectedTotalSupply = toBN(initialSupply).plus(amountOfStcToGenerate);
      const actualTotalSupply = await stableQUSDToken.totalSupply();
      assert.equal(actualTotalSupply.toString(), expectedTotalSupply.toString());

      const actualTotalStcBackedByCol = await borrowingCore.totalStcBackedByCol(colToken.address);
      assert.equal(actualTotalStcBackedByCol.toString(), amountOfStcToGenerate.toString());

      const actualMintedAmount = (await borrowingCore.userVaults(USER1, regularTokenVaultId)).mintedAmount;
      assert.equal(actualMintedAmount.toString(), amountOfStcToGenerate.toString());
    }).retries(5);

    it('should be not possible after liquidate', async () => {
      await setTime((await getCurrentBlockTime()) + 1000);
      await borrowingCore.updateCompoundRate(COL_NAME);

      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });
      await truffleAssert.reverts(
        borrowingCore.payBackStc(regularTokenVaultId, amountOfStcToGenerate, { from: USER1 }),
        '[QEC-021000]-The vault is liquidated.'
      );
    });

    it('should not be possible to pay back zero', async () => {
      await truffleAssert.reverts(
        borrowingCore.payBackStc(regularTokenVaultId, 0, { from: USER1 }),
        '[QEC-021014]-Payback amount must not be zero.'
      );
    });

    it(`QDEV-4785 User can't do “Repay” at the “QBTC->QUSD” Vault and gets reverted.`, async () => {
      const currentTime = await getCurrentBlockTime();
      await colPriceFeed.setExchangeRate('20662119999999997640704', currentTime, { from: OWNER });
      await expertsParameters.setUint(
        `governed.EPDR.${COL_NAME}_QUSD_collateralizationRatio`,
        '1500000000000000000000000000',
        { from: PARAMETERS_VOTING }
      );
      await expertsParameters.setUint(
        `governed.EPDR.${COL_NAME}_QUSD_liquidationRatio`,
        '1800000000000000000000000000',
        {
          from: PARAMETERS_VOTING,
        }
      );
      await expertsParameters.setUint(`governed.EPDR.${COL_NAME}_QUSD_ceiling`, '1000000000000000000000000', {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint(`governed.EPDR.${COL_NAME}_QUSD_interestRate`, '3022260000000000000', {
        from: PARAMETERS_VOTING,
      });

      await borrowingCore.createVault(COL_NAME, { from: USER1 });
      const vaultId = 2;

      await borrowingCore.updateCompoundRate(COL_NAME);

      // Enter “7499 QBTC” to the Deposit input and confirm transaction.
      const deposit = ONE.multipliedBy(74);
      await colToken.mint(USER1, deposit);
      await colToken.approve(borrowingCore.address, deposit, { from: USER1 });
      await borrowingCore.depositCol(vaultId, deposit, { from: USER1 });

      // Enter  “95.33737” QUSD to the Borrow input and confirm it.
      const stcAmount = ONE.multipliedBy(95.33737);
      await borrowingCore.generateStc(vaultId, stcAmount, { from: USER1 });

      // Enter “78.675894586595555555” QUSD to the Repay input and confirm it.
      const stcAmount1h = ONE.multipliedBy(78.675894586595555555);
      await stableQUSDToken.increaseAllowance(borrowingCore.address, stcAmount1h, { from: USER1 });
      await borrowingCore.payBackStc(vaultId, stcAmount1h, { from: USER1 });

      // At the withdraw input click the “max” button and confirm transaction.
      const stats = await borrowingCore.getVaultStats(USER1, vaultId);
      await borrowingCore.withdrawCol(vaultId, stats.colStats.withdrawableAmount, { from: USER1 });

      //  repay
      const stcAmount2h = stcAmount.minus(stcAmount1h);
      await stableQUSDToken.increaseAllowance(borrowingCore.address, stcAmount2h, { from: USER1 });
      await borrowingCore.payBackStc(vaultId, stcAmount2h, { from: USER1 });
    });
  });

  describe('liquidate', () => {
    beforeEach('setup', async () => {
      await borrowingCore.depositCol(regularTokenVaultId, toDepositAmount, { from: USER1 });
      await borrowingCore.depositCol(tokenWithFeeVaultId, toDepositAmountWithFee, { from: USER1 });

      // Generate STC only for the first vault
      await borrowingCore.generateStc(regularTokenVaultId, ONE, { from: USER1 });
    });

    it('should be possible to liquidate if currentColRatio < minColRatio', async () => {
      await setTime((await getCurrentBlockTime()) + 1000);
      await borrowingCore.updateCompoundRate(COL_NAME);

      const txReceipt = await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'Liquidated');
      assert.equal(txReceipt.logs[0].args.user, USER1);
      assert.equal(txReceipt.logs[0].args.vaultId, regularTokenVaultId);

      const userVaultStats = await borrowingCore.userVaults(USER1, regularTokenVaultId);
      assert.equal(userVaultStats.isLiquidated, true);
    });

    it('should be possible to clear vault with col with fee', async () => {
      await borrowingCore.generateStc(tokenWithFeeVaultId, ONE, { from: USER1 });

      await setTime((await getCurrentBlockTime()) + 500);
      await borrowingCore.updateCompoundRate(COL_WITH_FEE_NAME);

      await borrowingCore.liquidate(USER1, tokenWithFeeVaultId, { from: LIQUIDATION_AUCTION });

      await stableQUSDToken.transfer(LIQUIDATION_AUCTION, ONE.times(0.5), { from: USER1 });
      await stableQUSDToken.approve(borrowingCore.address, ONE.times(0.5), { from: LIQUIDATION_AUCTION });

      await borrowingCore.clearVault(USER1, tokenWithFeeVaultId, ONE.times(0.5), OWNER, { from: LIQUIDATION_AUCTION });

      const userVaultStats = await borrowingCore.userVaults(USER1, tokenWithFeeVaultId);
      assert.equal(userVaultStats.colAsset, '0');
      assert.equal(userVaultStats.normalizedDebt, '0');
      assert.equal(userVaultStats.mintedAmount, '0');
    });

    it('should fail if current col ratio is greater than liquidation ratio', async () => {
      await expertsParameters.setUint(`governed.EPDR.${COL_NAME}_QUSD_interestRate`, '50000000000000000000', {
        from: PARAMETERS_VOTING,
      });

      const threeYears = 3 * 365 * 24 * 60 * 60;
      await setNextBlockTime((await getCurrentBlockTime()) + threeYears);
      await borrowingCore.updateCompoundRate(COL_NAME);

      const currentColRatio = toBN(await borrowingCore.getCurrentColRatio(USER1, regularTokenVaultId));
      assert.isTrue(currentColRatio.lt(COL_RATIO));
      assert.isTrue(currentColRatio.gt(LIQUIDATION_RATIO));

      await truffleAssert.reverts(
        borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION }),
        '[QEC-021016]-Vault is above liquidation ratio.'
      );
    });

    it('should be possible to liquidate if the price equals liquidationPrice from vault stats', async () => {
      await truffleAssert.reverts(
        borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION }),
        '[QEC-021016]-Vault is above liquidation ratio.'
      );

      let userVaultStats = await borrowingCore.userVaults(USER1, regularTokenVaultId);
      assert.equal(userVaultStats.isLiquidated, false);

      const stats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);
      const liquidationPrice = toBN(stats.colStats.liquidationPrice);
      await colPriceFeed.setExchangeRate(liquidationPrice, await getCurrentBlockTime());

      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });

      userVaultStats = await borrowingCore.userVaults(USER1, regularTokenVaultId);
      assert.equal(userVaultStats.isLiquidated, true);
    });

    it('liquidation must be possible if outstanding debt exceeds liquidationLimit', async () => {
      await setTime((await getCurrentBlockTime()) + 1000);
      await borrowingCore.updateCompoundRate(COL_NAME);

      const stats = await borrowingCore.getVaultStats(USER1, regularTokenVaultId);
      assert.isTrue(toBN(stats.stcStats.outstandingDebt).gte(toBN(stats.stcStats.liquidationLimit)));

      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });

      const userVaultStats = await borrowingCore.userVaults(USER1, regularTokenVaultId);
      assert.equal(userVaultStats.isLiquidated, true);
    });

    it('liquidate should fail second time', async () => {
      await setTime((await getCurrentBlockTime()) + 1000);
      await borrowingCore.updateCompoundRate(COL_NAME);

      await borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION });
      await truffleAssert.reverts(
        borrowingCore.liquidate(USER1, regularTokenVaultId, { from: LIQUIDATION_AUCTION }),
        '[QEC-021000]-The vault is liquidated.'
      );
    });
  });

  describe('getVaultStats', () => {
    const newDepositAmount = COL_ONE.times(10);

    beforeEach('setup', async () => {
      await borrowingCore.createVault(COL_NAME, { from: USER2 });

      await colToken.mint(USER2, newDepositAmount);
      await colToken.approve(borrowingCore.address, newDepositAmount, { from: USER2 });
      await borrowingCore.depositCol(regularTokenVaultId, newDepositAmount, { from: USER2 });
    });

    it('should return correct vault stats', async () => {
      await expertsParameters.setUint(`governed.EPDR.${COL_NAME}_QUSD_liquidationRatio`, DECIMAL.times(1.25), {
        from: PARAMETERS_VOTING,
      });

      const stcAmount = ONE.times(300);
      const normalizedStcAmount = stcAmount.times(NORMALIZATION_FACTOR);
      await borrowingCore.generateStc(regularTokenVaultId, stcAmount, { from: USER2 });

      const stats = await borrowingCore.getVaultStats(USER2, regularTokenVaultId);
      assert.equal(stats.colStats.key, COL_NAME);
      assert.equal(stats.colStats.balance, newDepositAmount.toString());
      assert.equal(stats.colStats.price.toString(), toBN(200).times(toBN(10).pow(18)).toString());
      assert.equal(stats.colStats.withdrawableAmount, COL_ONE.times(7).toString());
      assert.equal(stats.colStats.liquidationPrice, ONE.times(37.5).toString());

      assert.equal(stats.stcStats.key, 'QUSD');
      assert.equal(stats.stcStats.outstandingDebt, stcAmount.toString());
      assert.equal(stats.stcStats.normalizedDebt, normalizedStcAmount.toString());
      assert.equal(stats.stcStats.compoundRate, DECIMAL.div(NORMALIZATION_FACTOR).toString());
      assert.equal(stats.stcStats.borrowingLimit, ONE.times(1000).toString());
      assert.equal(stats.stcStats.availableToBorrow, ONE.times(700).toString());
      assert.equal(stats.stcStats.liquidationLimit, ONE.times(1600).toString());
      assert.equal(stats.stcStats.borrowingFee, INTEREST_RATE.toString());
    });

    it('should not revert if collateral price is zero', async () => {
      const colPriceFeed = await FxPriceFeed.new('QETH_QUSD', 18, [OWNER], colToken.address, stableQUSDToken.address);
      await expertsParameters.setAddr(`governed.EPDR.${COL_NAME}_QUSD_oracle`, colPriceFeed.address, {
        from: PARAMETERS_VOTING,
      });

      const stats = await borrowingCore.getVaultStats(USER2, regularTokenVaultId);
      assert.equal(
        stats.colStats.withdrawableAmount,
        newDepositAmount.toString(),
        'If there is no debt, then the complete collateral must be withdrawable (no matter the exchange rate)'
      );
    });

    it('should not be possible to get vault stats for non-existing vault', async () => {
      await truffleAssert.reverts(borrowingCore.getVaultStats(USER2, 128), '[QEC-021005]-The vault does not exist.');
    });
  });

  describe('getColRatio', () => {
    it('should return max uint for zero debt', async () => {
      await borrowingCore.createVault(COL_NAME, { from: USER2 });
      const regularTokenVaultId = 0;

      const maxUint = toBN('115792089237316195423570985008687907853269984665640564039457584007913129639935');
      const colRatio = toBN(await borrowingCore.getCurrentColRatio(USER2, regularTokenVaultId, { from: USER2 }));
      assert.equal(toBN(colRatio).toString(), maxUint.toString());
    });
  });

  async function getRegVaultColRatio(user, vaultId, additionalStc) {
    const fullDebt = toBN(await borrowingCore.getFullDebt(user, vaultId)).plus(additionalStc);

    if (fullDebt.isZero()) {
      return new BigNumber(2).pow(256).minus(1);
    }

    const exchangeRate = toBN(await colPriceFeed.exchangeRate());
    const exchangeRateDecimals = toBN(await colPriceFeed.decimalPlaces());

    const colDecimals = await colToken.decimals();
    const stcDecimals = await stableQUSDToken.decimals();

    const colPrice = exchangeRate.times(10 ** stcDecimals).div(10 ** exchangeRateDecimals);

    const DECIMAL = toBN(10).pow(27);

    const partOne = colPrice.times(DECIMAL).div(toBN(10).pow(colDecimals));

    const colAsset = toBN((await borrowingCore.userVaults(user, vaultId)).colAsset);

    return partOne.times(colAsset).div(fullDebt);
  }
});
