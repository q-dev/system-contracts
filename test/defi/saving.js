const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const truffleAssert = require('truffle-assertions');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');
const getFromPercentageFormat = require('../helpers/defiHelper').getFromPercentageFormat;

const { toBN, getPercentageFormat } = require('../helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const Parameters = artifacts.require('EPDR_Parameters');
const Constitution = artifacts.require('Constitution');
const ContractRegistry = artifacts.require('ContractRegistry');
const LiquidationAuction = artifacts.require('LiquidationAuction');
const Saving = artifacts.require('Saving');
const StableToken = artifacts.require('StableCoinMock');
const SystemBalance = artifacts.require('SystemBalance');
const FxPriceFeed = artifacts.require('FxPriceFeed');
const ERC677 = artifacts.require('ERC677Mock');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const BorrowingCore = artifacts.require('BorrowingCore');

describe('Defi:Saving', () => {
  const reverter = new Reverter();

  let expertsParameters;
  let registry;
  let constitution;
  let liquidationAuction;
  let saving;
  let qethToken;
  let stableQUSDToken;
  let systemBalance;
  let qethPriceFeed;
  let borrowingCore;
  let crKeeperFactory;

  const Q = require('../helpers/defiHelper').Q;
  const DECIMAL = require('../helpers/defiHelper').DECIMAL;
  const savingMaxClaimP = 1000;
  const savingRate = 0.0025;
  const INTEREST_RATE = getPercentageFormat(100);

  let OWNER;
  let USER;
  let USER2;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER = await accounts(1);
    USER2 = await accounts(2);
    PARAMETERS_VOTING = await accounts(4);

    // never return exponantial notation
    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);

    liquidationAuction = await makeProxy(registry.address, LiquidationAuction);
    await liquidationAuction.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.liquidationAuction', liquidationAuction.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address, { from: OWNER });

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);

    qethToken = await ERC677.new('qeth', 'qeth');
    await expertsParameters.setAddr('governed.EPDR.QETH_address', qethToken.address, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_collateralizationRatio', DECIMAL.multipliedBy(1.5), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_liquidationRatio', DECIMAL.multipliedBy(1.5), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_ceiling', DECIMAL.multipliedBy(100), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_interestRate', INTEREST_RATE, { from: PARAMETERS_VOTING });
    await expertsParameters.setUint('governed.EPDR.QUSD_step', toBN(10), { from: PARAMETERS_VOTING });

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(
      registry.address,
      'QUSD',
      'QUSD',
      ['defi.QUSD.saving', 'governance.experts.EPDR.parametersVoting', 'defi.QUSD.borrowing'],
      forwarder
    );
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    qethPriceFeed = await FxPriceFeed.new('QETH_QUSD', 18, [OWNER], qethToken.address, stableQUSDToken.address);
    await expertsParameters.setAddr('governed.EPDR.QETH_QUSD_oracle', qethPriceFeed.address, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QUSD_savingRate', getPercentageFormat(savingRate), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.savingMaxClaimP', savingMaxClaimP, { from: PARAMETERS_VOTING });

    saving = await makeProxy(registry.address, Saving);
    await saving.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.saving', saving.address);

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);

    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    borrowingCore = await makeProxy(registry.address, BorrowingCore);
    await borrowingCore.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.borrowing', borrowingCore.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('deposit()', () => {
    const DEPOSIT = Q;

    beforeEach(async () => {
      await stableQUSDToken.transfer(USER, DEPOSIT, { from: OWNER });
      await stableQUSDToken.approve(saving.address, DEPOSIT, { from: USER });
    });

    it('should be possible to deposit from USER', async () => {
      const userBalanceBeforeDeposit = toBN(await stableQUSDToken.balanceOf(USER));

      const result = await saving.deposit(DEPOSIT, { from: USER });

      const userBalanceAfterDeposit = toBN(await stableQUSDToken.balanceOf(USER));
      const userSavingBalanceAfterDeposit = toBN(await saving.getBalance({ from: USER }));

      assert.equal(userBalanceBeforeDeposit.minus(DEPOSIT).toString(), userBalanceAfterDeposit.toString());
      assert.equal(DEPOSIT.toString(), userSavingBalanceAfterDeposit.toString());

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserDeposited');
      assert.equal(result.logs[0].args.depositAmount.toString(), DEPOSIT.toString());
      assert.equal(result.logs[0].args.user.toString(), USER.toString());
    });

    it('must correctly calculate normalized capital', async () => {
      await saving.deposit(DEPOSIT, { from: USER });

      const userSavingBalanceAfterDeposit = toBN(await saving.getBalance({ from: USER }));
      const compoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const compoundRate = toBN(await compoundRateKeeper.getCurrentRate());

      const expectedNormalizedCapital = calculateNormalizedCapital(
        userSavingBalanceAfterDeposit,
        getFromPercentageFormat(compoundRate)
      );
      const actualNormalizedCapital = toBN(await saving.normalizedCapitals(USER));

      assert.equal(expectedNormalizedCapital.toString(), actualNormalizedCapital.toString());
    });

    it('must correctly calculate aggregated normalized capital', async () => {
      await saving.deposit(DEPOSIT, { from: USER });

      const aggregatedNormalizedCapital = toBN(await saving.aggregatedNormalizedCapital());
      const expectedAggregatedNormalizedCapital = await calculateAggregatedNormalizedCapital(saving, [USER]);

      assert.equal(aggregatedNormalizedCapital.toString(), expectedAggregatedNormalizedCapital.toString());
    });

    it('should not be possible to deposit 0 from USER', async () => {
      const userBalanceBeforeDeposit = toBN(await stableQUSDToken.balanceOf(USER));

      await truffleAssert.reverts(saving.deposit(0, { from: USER }), '[QEC-022000]-Deposit amount must not be zero.');

      const userBalanceAfterDeposit = toBN(await stableQUSDToken.balanceOf(USER));
      const userSavingBalanceAfterDeposit = toBN(await saving.getBalance({ from: USER }));

      assert.equal(userBalanceBeforeDeposit.toString(), userBalanceAfterDeposit.toString());
      assert.equal(userSavingBalanceAfterDeposit.toString(), 0);
    });

    it('should not be possible to deposit more then USER have', async () => {
      const userBalanceBeforeDeposit = toBN(await stableQUSDToken.balanceOf(USER));

      await truffleAssert.fails(saving.deposit(DECIMAL, { from: USER }));

      const userBalanceAfterDeposit = toBN(await stableQUSDToken.balanceOf(USER));
      const userSavingBalanceAfterDeposit = toBN(await saving.getBalance({ from: USER }));

      assert.equal(userBalanceBeforeDeposit.toString(), userBalanceAfterDeposit.toString());
      assert.equal(userSavingBalanceAfterDeposit.toString(), 0);
    });
  });

  describe('withdraw()', () => {
    const DEPOSIT = Q.dividedBy(2);
    let userSavingBalanceBeforeWithdrawal;
    let userBalanceBeforeWithdrawal;

    beforeEach('balance setup', async () => {
      await stableQUSDToken.transfer(USER, DEPOSIT, { from: OWNER });
      await stableQUSDToken.approve(saving.address, DEPOSIT, { from: USER });
      await saving.deposit(DEPOSIT, { from: USER });
      userSavingBalanceBeforeWithdrawal = toBN(await saving.getBalance({ from: USER }));
      userBalanceBeforeWithdrawal = toBN(await stableQUSDToken.balanceOf(USER));
    });

    it('should be possible to withdraw STC', async () => {
      const amountToWithdraw = toBN(1000);
      const result = await saving.withdraw(amountToWithdraw, { from: USER });

      const userSavingBalanceAfterWithdrawal = toBN(await saving.getBalance({ from: USER }));
      const userBalanceAfterWithdrawal = toBN(await stableQUSDToken.balanceOf(USER));

      assert.equal(
        userSavingBalanceBeforeWithdrawal.minus(amountToWithdraw).toString(),
        userSavingBalanceAfterWithdrawal.toString()
      );
      assert.equal(
        userBalanceBeforeWithdrawal.plus(amountToWithdraw).toString(),
        userBalanceAfterWithdrawal.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserWithdrawn');
      assert.equal(result.logs[0].args.withdrawAmount.toString(), amountToWithdraw.toString());
      assert.equal(result.logs[0].args.user.toString(), USER.toString());
    });

    it('should withdraw the whole saving balance if the user has not enough STC on the saving balance', async () => {
      const amountToWithdraw = Q;
      const result = await saving.withdraw(amountToWithdraw, { from: USER });

      const userSavingBalanceAfterWithdrawal = toBN(await saving.getBalance({ from: USER }));
      const userBalanceAfterWithdrawal = toBN(await stableQUSDToken.balanceOf(USER));

      assert.equal(
        userSavingBalanceBeforeWithdrawal.minus(DEPOSIT).toString(),
        userSavingBalanceAfterWithdrawal.toString()
      );
      assert.equal(userBalanceBeforeWithdrawal.plus(DEPOSIT).toString(), userBalanceAfterWithdrawal.toString());

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserWithdrawn');
      assert.equal(result.logs[0].args.withdrawAmount.toString(), DEPOSIT.toString());
      assert.equal(result.logs[0].args.user.toString(), USER.toString());
    });

    it('must correctly calculate normalized capital', async () => {
      const amountToWithdraw = toBN(1000);
      await saving.withdraw(amountToWithdraw, { from: USER });

      const userSavingBalanceAfterWithdrawal = toBN(await saving.getBalance({ from: USER }));
      const compoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const compoundRate = toBN(await compoundRateKeeper.getCurrentRate());

      const expectedNormalizedCapital = calculateNormalizedCapital(
        userSavingBalanceAfterWithdrawal,
        getFromPercentageFormat(compoundRate)
      );
      const actualNormalizedCapital = toBN(await saving.normalizedCapitals(USER));

      assert.equal(expectedNormalizedCapital.toString(), actualNormalizedCapital.toString());
    });

    it('must correctly calculate aggregated normalized capital', async () => {
      const amountToWithdraw = toBN(1000);
      await saving.withdraw(amountToWithdraw, { from: USER });

      const aggregatedNormalizedCapital = toBN(await saving.aggregatedNormalizedCapital());
      const expectedAggregatedNormalizedCapital = await calculateAggregatedNormalizedCapital(saving, [USER]);

      assert.equal(aggregatedNormalizedCapital.toString(), expectedAggregatedNormalizedCapital.toString());
    });

    it('should get exception, try to call withdraw with zero balance', async () => {
      const amount = 10000;
      await truffleAssert.reverts(
        saving.withdraw(amount, { from: USER2 }),
        '[QEC-022001]-The caller does not have any balance to withdraw.'
      );
    });
  });

  describe('getBalance()', () => {
    const DEPOSIT = Q;

    beforeEach(async () => {
      await stableQUSDToken.transfer(USER, DEPOSIT, { from: OWNER });
      await stableQUSDToken.approve(saving.address, DEPOSIT, { from: USER });
      await saving.deposit(DEPOSIT, { from: USER });
    });

    it('should be possible get saving balance by user', async () => {
      const userSavingBalance = toBN(await saving.getBalance({ from: USER }));
      assert.equal(DEPOSIT.toString(), userSavingBalance.toString());
    });
  });

  describe('updateCompoundRate()', () => {
    const DEPOSIT = Q;

    beforeEach(async () => {
      await stableQUSDToken.transfer(USER, DEPOSIT, { from: OWNER });
      await stableQUSDToken.approve(saving.address, DEPOSIT, { from: USER });
      await saving.deposit(DEPOSIT, { from: USER });
    });

    it('should be possible to update a compound rate', async () => {
      await stableQUSDToken.transfer(systemBalance.address, Q, { from: OWNER });

      const compoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const compoundRate = toBN(await compoundRateKeeper.getCurrentRate());
      const lastUpdate = toBN(await compoundRateKeeper.getLastUpdate());
      const savingBalanceBeforeUpdate = toBN(await stableQUSDToken.balanceOf(saving.address));
      const systemBalanceBeforeUpdate = toBN(await stableQUSDToken.balanceOf(systemBalance.address));

      const timePassed = 1000;
      await setTime((await getCurrentBlockTime()) + lastUpdate.plus(timePassed).toNumber());

      await saving.updateCompoundRate();

      const compoundRateAfterUpdating = toBN(await compoundRateKeeper.getCurrentRate());

      const aggregatedNormalizedCapital = await calculateAggregatedNormalizedCapital(saving, [USER]);
      const expectedAccuredSavings = calculateAccruedSavings(
        compoundRateAfterUpdating,
        compoundRate,
        aggregatedNormalizedCapital,
        true
      );
      const savingBalanceAfterUpdate = toBN(await stableQUSDToken.balanceOf(saving.address));

      assert.equal(
        savingBalanceAfterUpdate.toString(),
        savingBalanceBeforeUpdate.plus(expectedAccuredSavings).toString()
      );

      const systemBalanceAfterUpdate = toBN(await stableQUSDToken.balanceOf(systemBalance.address));

      assert.equal(
        systemBalanceAfterUpdate.toString(),
        systemBalanceBeforeUpdate.minus(expectedAccuredSavings).toString()
      );
    });

    it('should mint if SystemSurplus is not sufficient and saving Rate <= borrowing fee', async () => {
      await expertsParameters.setUint('governed.EPDR.QETH_QUSD_interestRate', getPercentageFormat(0.5), {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint('governed.EPDR.QUSD_savingRate', getPercentageFormat(0.1), {
        from: PARAMETERS_VOTING,
      });
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);

      const colKey = 'QETH';
      await borrowingCore.createVault(colKey, { from: USER });
      const vaultId = 0;

      const depositValue = toBN(1000000000000000000); // 1 QETH
      await qethToken.transfer(USER, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER });

      const amountOfStcToGenerate = toBN(500000000000000000);
      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER });

      const borrowingCompoundRateKeeper = await CompoundRateKeeper.at(await borrowingCore.compoundRateKeeper(colKey));
      const borrowingLatestClaim = toBN(await borrowingCompoundRateKeeper.getLastUpdate());

      const savingCompoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const savingLastClaim = toBN(await savingCompoundRateKeeper.getLastUpdate());

      const timePassed = savingLastClaim.plus(borrowingLatestClaim).plus(10);
      await setTime((await getCurrentBlockTime()) + timePassed.toNumber());
      await borrowingCore.updateCompoundRate(colKey);

      const aggregatedTotals = await borrowingCore.getAggregatedTotals();
      const owedBorrowingFees = toBN(aggregatedTotals.owedBorrowingFees);
      assert.isTrue(owedBorrowingFees.isGreaterThan(0));

      const stats = await borrowingCore.getVaultStats(USER, vaultId);
      const outstandingDebt = toBN(stats.stcStats.outstandingDebt);
      assert.equal(owedBorrowingFees.plus(amountOfStcToGenerate).toString(), outstandingDebt.toString());

      const systemSurplus = await systemBalance.getSurplus();
      assert.equal(systemSurplus.toString(), 0);

      await saving.updateCompoundRate();

      const systemDebt = toBN(await systemBalance.getDebt());
      assert.isTrue(systemDebt.isLessThanOrEqualTo(owedBorrowingFees));
    });

    it('should not mint if SystemSurplus is not sufficient and saving Rate > borrowing fee', async () => {
      await expertsParameters.setUint('governed.EPDR.QETH_QUSD_interestRate', getPercentageFormat(0.005), {
        from: PARAMETERS_VOTING,
      });
      await expertsParameters.setUint('governed.EPDR.QUSD_savingRate', getPercentageFormat(0.1), {
        from: PARAMETERS_VOTING,
      });
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);

      const colKey = 'QETH';
      await borrowingCore.createVault(colKey, { from: USER });
      const vaultId = 0;

      const depositValue = toBN(1000000000000000000); // 1 QETH
      await qethToken.transfer(USER, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER });

      const amountOfStcToGenerate = toBN(500000000000000000);
      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER });

      const borrowingCompoundRateKeeper = await CompoundRateKeeper.at(await borrowingCore.compoundRateKeeper(colKey));
      const borrowingLatestClaim = toBN(await borrowingCompoundRateKeeper.getLastUpdate());

      const savingCompoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const savingLastClaim = toBN(await savingCompoundRateKeeper.getLastUpdate());

      const timePassed = savingLastClaim.plus(borrowingLatestClaim).plus(100);
      await setTime((await getCurrentBlockTime()) + timePassed.toNumber());
      await borrowingCore.updateCompoundRate(colKey);

      const aggregatedTotals = await borrowingCore.getAggregatedTotals();
      const owedBorrowingFees = toBN(aggregatedTotals.owedBorrowingFees);
      assert.isTrue(owedBorrowingFees.isGreaterThan(0));

      const stats = await borrowingCore.getVaultStats(USER, vaultId);
      const outstandingDebt = toBN(stats.stcStats.outstandingDebt);
      assert.equal(owedBorrowingFees.plus(amountOfStcToGenerate).toString(), outstandingDebt.toString());

      const systemSurplus = await systemBalance.getSurplus();
      assert.equal(systemSurplus.toString(), 0);

      await truffleAssert.reverts(
        saving.updateCompoundRate(),
        '[QEC-022002]-System debt exceeds owed borrowing fees, failed to update compound rate.'
      );

      const systemDebt = toBN(await systemBalance.getDebt());
      assert.isTrue(systemDebt.isLessThanOrEqualTo(owedBorrowingFees));
    });
  }).retries(3);

  describe('getBalanceDetails()', () => {
    it('should be possible to get balance details', async () => {
      await stableQUSDToken.transfer(USER, Q, { from: OWNER });
      await stableQUSDToken.approve(saving.address, Q, { from: USER });
      await saving.deposit(Q, { from: USER });
      await stableQUSDToken.transfer(systemBalance.address, Q, { from: OWNER });

      const compoundRateKeeper = await CompoundRateKeeper.at(await saving.compoundRateKeeper());
      const lastUpdate = toBN(await compoundRateKeeper.getLastUpdate());

      const timePassed = 1000;
      await setTime((await getCurrentBlockTime()) + lastUpdate.plus(timePassed).toNumber());

      await saving.updateCompoundRate();

      const balanceDetails = await saving.getBalanceDetails({ from: USER });

      assert.equal(balanceDetails.currentBalance, await saving.getBalance({ from: USER }));
      assert.equal(balanceDetails.normalizedBalance, await saving.normalizedCapitals(USER));
      assert.equal(balanceDetails.compoundRate, await compoundRateKeeper.getCurrentRate());
      assert.equal(balanceDetails.lastUpdateOfCompoundRate, await compoundRateKeeper.getLastUpdate());
      assert.equal(balanceDetails.interestRate, getPercentageFormat(savingRate).toString());
    });
  });
});

function calculateNormalizedCapital(newBalance, compoundRate) {
  return toBN(newBalance).dividedBy(toBN(compoundRate).dividedBy(100));
}

async function calculateAggregatedNormalizedCapital(saving, users) {
  let aggregatedNormalizedCapital = toBN(0);
  for (let i = 0; i < users.length; i++) {
    const normalizedCapital = toBN(await saving.normalizedCapitals(users[i]));
    aggregatedNormalizedCapital = aggregatedNormalizedCapital.plus(normalizedCapital);
  }

  return aggregatedNormalizedCapital;
}

function calculateAccruedSavings(newCmpoundRate, oldCmpoundRate, aggregatedNormalizedCapital, roundUp = false) {
  if (roundUp) {
    return toBN(aggregatedNormalizedCapital)
      .multipliedBy(toBN(newCmpoundRate).minus(oldCmpoundRate))
      .dividedBy(1e27)
      .integerValue();
  }

  return toBN(aggregatedNormalizedCapital)
    .multipliedBy(toBN(newCmpoundRate).minus(oldCmpoundRate))
    .dividedBy(1e27)
    .minus(1)
    .integerValue();
}
