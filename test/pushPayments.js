const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const PushPayments = artifacts.require('PushPayments');
const GreedyReceiver = artifacts.require('GreedyReceiverMock');

describe('PushPayments', () => {
  const reverter = new Reverter();

  const AMOUNT = 1000000000;

  let pushPayments;
  let receiver;

  let SOMEBODY;

  before('setup', async () => {
    SOMEBODY = await accounts(0);

    pushPayments = await PushPayments.new();
    receiver = await GreedyReceiver.new();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('safeTransfer', () => {
    it('should be transferred', async () => {
      await receiver.changeMode(false);
      await pushPayments.safeTransferTo(receiver.address, { from: SOMEBODY, value: AMOUNT });
      const balance = await pushPayments.balanceOf(receiver.address);
      assert.equal(balance, 0);
    });

    it('should be held', async () => {
      await receiver.changeMode(true);
      await pushPayments.safeTransferTo(receiver.address, { from: SOMEBODY, value: AMOUNT });
      const balance = await pushPayments.balanceOf(receiver.address);
      assert.equal(balance, AMOUNT);
    });
  });

  describe('withdraw', () => {
    it('should be withdrawn', async () => {
      await receiver.changeMode(true);
      await pushPayments.safeTransferTo(receiver.address, { from: SOMEBODY, value: AMOUNT });
      await receiver.doWithdraw(pushPayments.address);
      await truffleAssert.reverts(
        receiver.doWithdraw(pushPayments.address),
        '[QEC-033001]-The caller does not have any balance to withdraw.'
      );
    });
  });
});
