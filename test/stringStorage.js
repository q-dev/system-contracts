const { assert } = require('chai');
const Reverter = require('./helpers/reverter');

const { artifacts } = require('hardhat');
const StringStorage = artifacts.require('StringStorage');

describe('StringStorage', () => {
  const reverter = new Reverter();

  const A = 'A';
  const B = 'B';
  const C = 'C';

  let stringStorage;

  before('setup', async () => {
    stringStorage = await StringStorage.new([]);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('adding', () => {
    it('should add A, B and C', async () => {
      await stringStorage.mustAdd(A);
      await stringStorage.mustAdd(B);
      await stringStorage.mustAdd(C);
      const size = await stringStorage.size();

      assert.equal(size, 3);
    });
  });

  describe('removing', () => {
    it('should add A and remove it', async () => {
      await stringStorage.mustAdd(A);
      await stringStorage.mustRemove(A);
      const size = await stringStorage.size();

      assert.equal(size, 0);
    });

    it('should add A, B and remove B', async () => {
      await stringStorage.mustAdd(A);
      await stringStorage.mustAdd(B);
      await stringStorage.mustRemove(B);
      const size = await stringStorage.size();

      assert.equal(size, 1);
      assert.isFalse(await stringStorage.contains(B));
      assert.isTrue(await stringStorage.contains(A));
    });

    it('should remove A, B and C', async () => {
      await stringStorage.mustAdd(A);
      await stringStorage.mustAdd(B);
      await stringStorage.mustAdd(C);

      await stringStorage.mustRemove(C);

      let size = await stringStorage.size();
      assert.equal(size, 2);

      await stringStorage.mustRemove(A);

      size = await stringStorage.size();
      assert.equal(size, 1);

      await stringStorage.mustRemove(B);

      size = await stringStorage.size();
      assert.equal(size, 0);
    });
  });

  describe('clear', () => {
    it.skip('gas usage analysis', async () => {
      const addressNumbers = [0, 1, 10, 50, 100, 200];
      // eslint-disable-next-line guard-for-in
      for (const addressNumber of addressNumbers) {
        console.log('addressNumber: ', addressNumber);
        for (let i = 1; i <= addressNumber; i++) {
          const newAddress = '0x' + i.toString().padStart(40, '0');
          await stringStorage.mustAdd(newAddress);
        }

        const result = await stringStorage.clear();
        console.log('\tgasUsed: ', result.receipt.gasUsed);
      }
    });
  });
});
