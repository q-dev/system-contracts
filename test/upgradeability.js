const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const BigNumber = require('bignumber.js');
const makeProxy = require('./helpers/makeProxy');
const { toBN } = require('./helpers/defiHelper');

const { artifacts } = require('hardhat');
const BorrowingCore = artifacts.require('BorrowingCore');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const FxPriceFeed = artifacts.require('FxPriceFeed');
const ERC677 = artifacts.require('ERC677Mock');
const StableToken = artifacts.require('StableCoinMock');
const SystemBalance = artifacts.require('SystemBalance');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');

describe('TransparentUpgradeableProxy', () => {
  let crKeeperFactory;
  let borrowingCore;
  let qethToken;
  let qethPriceFeed;
  let expertsParameters;
  let registry;
  let stableQUSDToken;
  let systemBalance;

  const DECIMAL = require('./helpers/defiHelper').DECIMAL;
  const INTEREST_RATE = new BigNumber(0.01);

  let OWNER;
  let USER1;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    // // never return exponential notation
    // BigNumber.config({EXPONENTIAL_AT: 1e+9});
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    PARAMETERS_VOTING = await accounts(2);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    qethToken = await ERC677.new('qeth', 'qeth');

    // dummy address
    const forwarder = registry.address;
    stableQUSDToken = await StableToken.new(registry.address, 'QUSD', 'QUSD', ['defi.QUSD.borrowing'], forwarder);
    qethPriceFeed = await FxPriceFeed.new('QETH_QUSD', 18, [OWNER], qethToken.address, stableQUSDToken.address);

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);
    await expertsParameters.setAddr('governed.EPDR.QETH_address', qethToken.address, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);
    await registry.setAddress('defi.QUSD.coin', stableQUSDToken.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);

    borrowingCore = await makeProxy(registry.address, BorrowingCore);
    await registry.setAddress('defi.QUSD.borrowing', borrowingCore.address);

    await borrowingCore.initialize(registry.address, 'QUSD');

    await expertsParameters.setAddr('governed.EPDR.QETH_QUSD_oracle', qethPriceFeed.address, {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_collateralizationRatio', toBN(150 * 10 ** 27), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_ceiling', toBN(100 * 10 ** 27), {
      from: PARAMETERS_VOTING,
    });

    await expertsParameters.setUint('governed.EPDR.QETH_QUSD_interestRate', DECIMAL.multipliedBy(INTEREST_RATE), {
      from: PARAMETERS_VOTING,
    });
    await expertsParameters.setUint('governed.EPDR.QUSD_step', toBN(10), { from: PARAMETERS_VOTING });

    systemBalance = await makeProxy(registry.address, SystemBalance);
    await systemBalance.initialize(registry.address, 'QUSD');
    await registry.setAddress('defi.QUSD.systemBalance', systemBalance.address);
  });

  describe('upgrade', () => {
    it('should be upgraded successfully ', async () => {
      const price = toBN(100000000000000000000); // 100 QUSD per 1 QETH
      await qethPriceFeed.setExchangeRate(price, 1);
      const depositValue = toBN(1000000000000000000); // 1 QETH
      const amountOfStcToGenerate = toBN(500000000000000000);
      const colKey = 'QETH';

      await borrowingCore.createVault(colKey, { from: USER1 });
      const vaultId = 0;

      await qethToken.transfer(USER1, depositValue);
      await qethToken.approve(borrowingCore.address, depositValue, { from: USER1 });
      await borrowingCore.depositCol(vaultId, depositValue, { from: USER1 });

      await borrowingCore.generateStc(vaultId, amountOfStcToGenerate, { from: USER1 });

      const colRatio = '200000000000000000000000000000';

      assert.equal((await borrowingCore.getCurrentColRatio(USER1, vaultId)).toString(), colRatio);
      assert.equal(
        (await borrowingCore.totalStcBackedByCol(qethToken.address)).toString(),
        amountOfStcToGenerate.toString()
      );
      assert.equal((await borrowingCore.getFullDebt(USER1, vaultId)).toString(), amountOfStcToGenerate.toString());

      const newBorrowing = await BorrowingCore.new();
      await registry.upgradeContract(borrowingCore.address, newBorrowing.address, { from: OWNER });

      // re init just in case
      borrowingCore = await BorrowingCore.at(await registry.mustGetAddress('defi.QUSD.borrowing'));

      assert.equal((await borrowingCore.getCurrentColRatio(USER1, vaultId)).toString(), colRatio);
      assert.equal(
        (await borrowingCore.totalStcBackedByCol(qethToken.address)).toString(),
        amountOfStcToGenerate.toString()
      );
      assert.equal((await borrowingCore.getFullDebt(USER1, vaultId)).toString(), amountOfStcToGenerate.toString());
    });
  });
});
