const { accounts } = require('./helpers/utils.js');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const createExceptionStorage = require('./helpers/exceptionStorage');

describe('ExceptionStorage', () => {
  const reverter = new Reverter();

  let OWNER;
  let SOMEBODY;

  let exceptionStorage;
  before('setup', async () => {
    OWNER = await accounts(0);
    SOMEBODY = await accounts(1);
    exceptionStorage = await createExceptionStorage();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('ownable', () => {
    it('should not be possible to change message if not owner', async () => {
      await exceptionStorage.setException(123123, 'TestTestTest', { from: OWNER });
      await truffleAssert.reverts(
        exceptionStorage.setException(123124, 'TestTestTest', { from: SOMEBODY }),
        'Ownable: caller is not the owner'
      );
    });
  });

  describe('setExceptions', () => {
    it('should set multiple messages in one trx', async () => {
      await exceptionStorage.setExceptions([1, 2, 3], ['A', 'B', 'C'], { from: OWNER });
      await truffleAssert.reverts(exceptionStorage.throwException(1), 'A');
      await truffleAssert.reverts(exceptionStorage.throwException(4), 'Error with unknown code');
    });

    it('should contain qVault exceptions', async () => {
      // , '[QEC-017001]'
      await truffleAssert.reverts(exceptionStorage.throwException(17001, { from: OWNER }));
    });
  });
});
