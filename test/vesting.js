const { accounts } = require('./helpers/utils.js');
const assertEqualBN = require('./helpers/assertEqualBN');
const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const Vesting = artifacts.require('./Vesting');
const Constitution = artifacts.require(`Constitution`);
const ContractRegistry = artifacts.require('./ContractRegistry');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting.sol');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('Vesting', () => {
  const reverter = new Reverter();

  const Q = require('./helpers/defiHelper').Q;
  const RATE = toBN(0.00000000302226);
  const qi = getPercentageFormat(RATE);

  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const proposalExecutionP = 2592000;

  let registry;
  let constitution;
  let epqfiParams;
  let vesting;
  let votingWeightProxy;
  let generalUpdateVote;

  let USER1;
  let USER2;
  let SOMEBODY;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    USER1 = await accounts(0);
    USER2 = await accounts(1);
    SOMEBODY = await accounts(1);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([USER1], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    const qfiUintKeys = [
      'expertPanels.QFI.tokeneconomics.QholderInterestRate',
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.stakeDelegationFactor',
      'governed.EPQFI.Q_rewardPoolInterest',
    ];
    const qfiUintParams = [qi, 3, getPercentageFormat(1000), getPercentageFormat(2)];

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(registry.address, qfiUintKeys, qfiUintParams, [], [], [], [], [], []);
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await constitution.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    const parametersInitialList = ['constitution.maxNValidators', 'constitution.maxNStandbyValidators'];
    const parametersValues = [20, 20];

    for (let i = 0; i < parametersInitialList.length; ++i) {
      await constitution.setUint(parametersInitialList[i], parametersValues[i], { from: PARAMETERS_VOTING });
    }

    vesting = await makeProxy(registry.address, Vesting);
    await vesting.initialize(registry.address);
    await registry.setAddress('tokeneconomics.vesting', vesting.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.vesting'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('purgeTimeLocks', () => {
    it('should purge time locks', async () => {
      const depositAmount = Q.times(100);
      const releaseStart = toBN(10000);
      const releaseEnd = toBN(20000);

      await vesting.depositOnBehalfOf(USER1, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      await vesting.depositOnBehalfOf(USER1, toBN(12000), toBN(24000), { from: USER1, value: depositAmount });
      const timeLocksBefore = await vesting.getTimeLocks(USER1);
      assert.equal(timeLocksBefore.length, 2);

      await setTime((await getCurrentBlockTime()) + 20000);
      await vesting.withdraw(depositAmount, { from: USER1 });
      await vesting.purgeTimeLocks(USER1, { from: USER1 });

      let timeLocksAfter = await vesting.getTimeLocks(USER1);
      assert.equal(timeLocksAfter.length, 1);

      await setTime((await getCurrentBlockTime()) + 25000);
      await vesting.withdraw(depositAmount, { from: USER1 });
      await vesting.purgeTimeLocks(USER1, { from: USER1 });

      timeLocksAfter = await vesting.getTimeLocks(USER1);
      assert.equal(timeLocksAfter.length, 0);
    });
  });

  describe('depositOnBehalfOf', () => {
    const maxTimelocksNumber = toBN(10);

    it('should self deposit', async () => {
      const depositAmount = Q.times(50);
      const releaseStart = toBN(0);
      const releaseEnd = toBN(10);

      await vesting.depositOnBehalfOf(USER1, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      assert.equal(depositAmount.toString(), (await vesting.balanceOf(USER1)).toString());
    });

    it('should deposit on behalf of other user', async () => {
      const depositAmount = Q.times(20);
      const releaseStart = toBN(0);
      const releaseEnd = toBN(5);

      await vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      assert.equal(depositAmount.toString(), (await vesting.balanceOf(USER2)).toString());
    });

    it('QDEV-2944 LockInfo Issue. should deposit on behalf of other user', async () => {
      const setupDepositAmount = Q.times(10);
      const depositAmount = setupDepositAmount;
      const releaseStart = toBN(0);
      const releaseEnd = toBN(5);

      await vesting.depositOnBehalfOf(USER1, releaseStart, releaseEnd, { from: USER1, value: setupDepositAmount });
      assert.equal(setupDepositAmount.toString(), (await vesting.balanceOf(USER1)).toString());
      assert.equal(toBN(0).toString(), (await vesting.balanceOf(USER2)).toString());

      await vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      assert.equal(setupDepositAmount.toString(), (await vesting.balanceOf(USER1)).toString());
      assert.equal(depositAmount.toString(), (await vesting.balanceOf(USER2)).toString());
    });

    it('should not be possible to deposit with incorrect release dates', async () => {
      const depositAmount = Q.times(100);
      const releaseStart = toBN(6);
      const releaseEnd = toBN(3);

      await truffleAssert.reverts(
        vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: depositAmount }),
        'Time lock release end must be after release start.'
      );
    });

    it('should get exception, try to deposit less than minimum amount', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      const wrongDepositAmount = Q.times(10).minus(1);
      const releaseStart = toBN(50);
      const releaseEnd = toBN(100);

      const reason = '[QEC-032002]-Given amount is below deposit minimum.';
      await truffleAssert.reverts(
        vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: wrongDepositAmount }),
        reason
      );

      const lockInfo = await vesting.getLockInfo({ from: USER2 });
      const minimumBalance = await vesting.getMinimumBalance(USER2, currentTime);
      assertEqualBN(await vesting.balanceOf(USER2), 0);
      assertEqualBN(lockInfo.lockedAmount, 0);
      assertEqualBN(minimumBalance, 0);
    });

    it('should get exception, try to deposit maxTimelocksNumber + 1 times', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      const depositAmount = Q.times(10);
      const releaseStart = toBN(5000);
      const releaseEnd = toBN(100000);

      for (let i = 0; i < maxTimelocksNumber; i++) {
        vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      }

      const reason = '[QEC-032003]-Failed to deposit amount, too many timelocks.';
      await truffleAssert.reverts(
        vesting.depositOnBehalfOf(USER2, releaseStart, releaseEnd, { from: USER1, value: depositAmount }),
        reason
      );

      const firstLockInfo = await vesting.getLockInfo({ from: USER2 });
      const minimumBalance = await vesting.getMinimumBalance(USER2, currentTime);
      assert.equal((await vesting.balanceOf(USER2)).toString(), '100000000000000000000');
      assertEqualBN(firstLockInfo.lockedAmount, '100000000000000000000');
      assertEqualBN(minimumBalance, '100000000000000000000');
    });
  });

  describe('withdraw', () => {
    it('should withdraw correct amount from vesting', async () => {
      const depositAmount = Q.times(1000);
      const releaseStart = toBN((await getCurrentBlockTime()) + 10000);
      const releaseEnd = toBN((await getCurrentBlockTime()) + 20000);
      await vesting.depositOnBehalfOf(USER1, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      await vesting.unlock(depositAmount, { from: USER1 });

      const minimumBalance1 = await vesting.getMinimumBalance(USER1, releaseStart);
      assert.equal(toBN(minimumBalance1).toString(), depositAmount.toString());
      await truffleAssert.reverts(
        vesting.withdraw(Q.times(100), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );

      const lockedTime = releaseStart.toNumber() + 3000;
      await setNextBlockTime(lockedTime);
      const minimumBalance2 = await vesting.getMinimumBalance(USER1, lockedTime);
      assert.equal(minimumBalance2.toString(), Q.times(700).toString());

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(400), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );

      const withdrawableBalance = await vesting.withdrawableBalanceOf(USER1);
      assert.equal(withdrawableBalance.toString(), Q.times(300).toString());
      await vesting.withdraw(Q.times(300), { from: USER1 });
      let userBalance = await vesting.balanceOf(USER1);
      assert.equal(userBalance.toString(), Q.times(700).toString());

      const lockedTime2 = (await getCurrentBlockTime()) + 4000 - 1;
      await setNextBlockTime(lockedTime2);
      const minimumBalance3 = await vesting.getMinimumBalance(USER1, toBN(lockedTime2));
      assert.equal(minimumBalance3.toString(), Q.times(300).toString());

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(500), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );
      await vesting.withdraw(Q.times(400), { from: USER1 });
      userBalance = await vesting.balanceOf(USER1);
      assert.equal(userBalance.toString(), Q.times(300).toString());

      const unlockedTime = (await getCurrentBlockTime()) + 20000;
      await setTime(unlockedTime);

      const minimumBalance4 = await vesting.getMinimumBalance(USER1, toBN(unlockedTime));
      assert.equal(minimumBalance4.toString(), Q.times(0).toString());
      await vesting.withdraw(Q.times(300), { from: USER1 });

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(100), { from: USER1 }),
        '[QEC-031001]-Insufficient balance for vesting withdrawal.'
      );

      await vesting.depositOnBehalfOf(USER1, releaseStart, releaseEnd, { from: USER1, value: depositAmount });
      await vesting.announceUnlock(Q.times(200), { from: USER1 });
      await vesting.withdraw(Q.times(300), { from: USER1 });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, USER1);
      assert.equal(currentLockInfo.lockedAmount, Q.times(700));
      assert.equal(currentLockInfo.pendingUnlockAmount, Q.times(0));
    }).retries(5);

    it('should withdraw correct amount from vesting with 2 timelocks', async () => {
      const depositAmount = Q.times(100);
      const releaseStart1 = toBN((await getCurrentBlockTime()) + 10000);
      const releaseEnd1 = toBN((await getCurrentBlockTime()) + 20000);
      const releaseStart2 = toBN((await getCurrentBlockTime()) + 20000);
      const releaseEnd2 = toBN((await getCurrentBlockTime()) + 30000);
      await vesting.depositOnBehalfOf(USER1, releaseStart1, releaseEnd1, { from: USER1, value: depositAmount });
      await vesting.depositOnBehalfOf(USER1, releaseStart2, releaseEnd2, { from: USER1, value: depositAmount });
      await vesting.unlock(depositAmount.times(2), { from: USER1 });

      const minimumBalance1 = await vesting.getMinimumBalance(USER1, toBN(releaseStart1));
      assert.equal(minimumBalance1.toString(), depositAmount.times(2).toString());
      await truffleAssert.reverts(
        vesting.withdraw(Q.times(10), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );

      const lockedTime = releaseStart1.toNumber() + 3000;
      await setNextBlockTime(lockedTime);
      const minimumBalance2 = await vesting.getMinimumBalance(USER1, toBN(lockedTime));
      assert.equal(minimumBalance2.toString(), Q.times(170).toString());

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(40), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );
      const withdrawableBalance = await vesting.withdrawableBalanceOf(USER1);
      assert.equal(withdrawableBalance.toString(), Q.times(30).toString());
      await vesting.withdraw(Q.times(30), { from: USER1 });
      let userBalance = await vesting.balanceOf(USER1);
      assert.equal(userBalance.toString(), Q.times(170).toString());

      const lockedTime2 = lockedTime + 4000;
      await setNextBlockTime(lockedTime2);
      const minimumBalance3 = await vesting.getMinimumBalance(USER1, toBN(lockedTime2));
      assert.equal(minimumBalance3.toString(), Q.times(130).toString());

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(60), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );
      await vesting.withdraw(Q.times(40), { from: USER1 });
      userBalance = await vesting.balanceOf(USER1);
      assert.equal(userBalance.toString(), Q.times(130).toString());

      const unlockedTime = releaseStart2.toNumber();
      await setNextBlockTime(unlockedTime);
      const minimumBalance4 = await vesting.getMinimumBalance(USER1, toBN(unlockedTime));
      assert.equal(minimumBalance4.toString(), Q.times(100).toString());
      await vesting.withdraw(Q.times(30), { from: USER1 });

      await truffleAssert.reverts(
        vesting.withdraw(Q.times(30), { from: USER1 }),
        '[QEC-031002]-Vesting withdrawal prevented by timelock(s).'
      );
    }).retries(3);
  }).retries(5);

  describe('lock', () => {
    const deposit = Q.times(400);
    const amount = deposit.dividedBy(4);

    beforeEach('initial deposit', async () => {
      await vesting.depositOnBehalfOf(SOMEBODY, 0, 1, { from: SOMEBODY, value: deposit });
    });

    it('should implicitly lock amount', async () => {
      await vesting.unlock(deposit.minus(amount), { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(amount.toString(), toBN(currentLockInfo.lockedAmount).toString());
    });

    it('should increase locked amount', async () => {
      await vesting.unlock(deposit.minus(amount), { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(amount.toString(), toBN(currentLockInfo.lockedAmount).toString());

      await vesting.lock(amount, { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(amount.times(2).toString(), toBN(currentLockInfo.lockedAmount).toString());
    });

    it('should get exception, try to lock amount greater than available balance', async () => {
      await vesting.unlock(deposit.minus(amount), { from: SOMEBODY });

      const reason = '[QEC-031004]-The lock amount must not exceed the available balance.';
      await truffleAssert.reverts(vesting.lock(amount.times(4), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.toString());
    });

    it('should get exception, try to lock zero amount', async () => {
      await vesting.unlock(deposit, { from: SOMEBODY });

      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await truffleAssert.reverts(vesting.lock(0, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
    });
  });

  describe('announceUnlock', () => {
    const deposit = Q.times(400);
    const announceUnlockAmount = deposit.dividedBy(2);
    const amount = deposit.dividedBy(4);

    beforeEach('setup', async () => {
      await vesting.depositOnBehalfOf(SOMEBODY, 0, 1, { from: SOMEBODY, value: deposit.plus(amount) });
      await vesting.unlock(amount, { from: SOMEBODY });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
    });

    it('Should announce unlock successfully', async () => {
      await vesting.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should increase pending unlock amount', async () => {
      await vesting.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      const secondAnnouncedUnlockedAmount = announceUnlockAmount.plus(amount);
      await vesting.announceUnlock(secondAnnouncedUnlockedAmount, { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);

      const totalAnnounceUnlockedAmount = secondAnnouncedUnlockedAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        deposit.minus(totalAnnounceUnlockedAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceUnlockedAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should get exception, try to announce unlock amount greater than locked amount', async () => {
      const reason = '[QEC-028003]-Cannot unlock more than is currently locked.';
      await truffleAssert.reverts(vesting.announceUnlock(deposit.plus(1), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    it('should successfully announce unlock zero amount', async () => {
      await vesting.announceUnlock(0, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });
  });

  describe('unlock', () => {
    const deposit = Q.times(2).times(400);
    const lockedAmount = deposit.dividedBy(2);
    const announceUnlockAmount = lockedAmount.dividedBy(2);
    const unlockAmount = announceUnlockAmount.dividedBy(2);

    beforeEach('setup', async () => {
      await vesting.depositOnBehalfOf(SOMEBODY, 0, 1, { from: SOMEBODY, value: deposit });
      await vesting.unlock(lockedAmount, { from: SOMEBODY });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: SOMEBODY });

      await vesting.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });

    it('should unlock successfully', async () => {
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await vesting.unlock(unlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceUnlockAmount.minus(unlockAmount).toString()
      );
    });

    it('should unlock successfully multiple times', async () => {
      await vesting.announceUnlock(announceUnlockAmount.times(2), { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.times(2).toString());

      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await vesting.unlock(unlockAmount, { from: SOMEBODY });
      await vesting.unlock(unlockAmount.times(2), { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      const totalUnlockAmount = unlockAmount.times(3);
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceUnlockAmount.times(2).minus(totalUnlockAmount).toString()
      );
    });

    it('should get exception, try to unlock before the expiration of the pending unlock time', async () => {
      const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).minus(10).toNumber());
      await truffleAssert.reverts(vesting.unlock(unlockAmount, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });

    it('should get exception, try to unlock amount greater than pending unlock amount', async () => {
      const reason = '[QEC-028005]-Smart unlock not possible, tokens are still locked by recent voting.';
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());

      await generalUpdateVote.createProposal('lorem ipsum');
      await generalUpdateVote.voteFor(1, { from: SOMEBODY });

      await truffleAssert.reverts(vesting.unlock(unlockAmount.times(4), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });

    it('should get exception, try to unlock zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await truffleAssert.reverts(vesting.unlock(0, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(vesting.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });
  });
});
