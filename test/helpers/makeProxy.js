const Proxy = artifacts.require('TransparentUpgradeableProxy');

async function makeProxy(adminAddress, logicContract) {
  const logicContractInstance = await logicContract.new();
  const proxyInstance = await Proxy.new(logicContractInstance.address, adminAddress, []);
  // "await" should not be used redundantly
  // old: return await logicContract.at(proxyInstance.address);
  return logicContract.at(proxyInstance.address);
}

module.exports = makeProxy;
