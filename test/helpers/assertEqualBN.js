const { assert } = require('chai');

function assertEqualBN(actual, expected) {
  return assert.equal(actual.toString(), expected.toString());
}

module.exports = assertEqualBN;
