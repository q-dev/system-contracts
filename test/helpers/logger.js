function Logger(web3) {
  this.printEventInfo = async (log, eventName) => {
    assert.equal(log.event, eventName);
    console.log('-------');
    console.log(eventName);
    for (const key in log) {
      if (Object.prototype.hasOwnProperty.call(log, key)) {
        console.log(key, log[key]);
      }
    }
  };
}

module.exports = Logger;
