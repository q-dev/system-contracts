const BigNumber = require('bignumber.js');

const Q = toBN(10).pow(18);
const DECIMAL = toBN(10).pow(27);

function toBN(num) {
  return new BigNumber(num);
}

function getPercentageFormat(number) {
  return DECIMAL.multipliedBy(number).dividedBy(100);
}

function getFromPercentageFormat(percentage) {
  return toBN(percentage).multipliedBy(toBN(100)).dividedBy(DECIMAL);
}

function calculatePercentage(part, amount) {
  if (toBN(amount).isEqualTo(0)) {
    return toBN(0);
  }
  return DECIMAL.multipliedBy(part).dividedBy(amount);
}

async function getTransferFeePercentage(token, owner, user) {
  const transferAmount = toBN(100);

  const initialBalance = toBN(await token.balanceOf(user));

  await token.transfer(user, transferAmount, { from: owner });

  const balanceAfterTransfer = toBN(await token.balanceOf(user));

  const fee = transferAmount.minus(toBN(balanceAfterTransfer.minus(initialBalance)));

  return fee.div(transferAmount);
}

function getAmountAfterTransfer(feePercentage, amount) {
  const fee = amount.multipliedBy(feePercentage);

  return amount.minus(fee).toString();
}

function getNextCompoundRate(interestRate, period, currentRate) {
  const rpow = toBN(interestRate).plus(DECIMAL).pow(period);
  const precision = rpow.precision(true);
  const rpowPrecision = rpow.multipliedBy(DECIMAL).dividedToIntegerBy(toBN(10).pow(precision - 1));

  return toBN(currentRate).multipliedBy(rpowPrecision).dividedToIntegerBy(DECIMAL);
}

function normalizeAmount(targetAmount, rate) {
  const normalizedAmount = targetAmount.multipliedBy(DECIMAL).dividedToIntegerBy(rate);
  const actualLookAhead = denormalizeAmount(normalizedAmount, rate);

  if (actualLookAhead.isLessThan(targetAmount)) {
    // try to compensate for rounding errors
    const normalizedLookAhead = normalizedAmount.plus(1);
    const actualLookAhead = denormalizeAmount(normalizedLookAhead, rate);

    if (actualLookAhead.isLessThanOrEqualTo(targetAmount)) {
      return normalizedLookAhead;
    }
  }

  return normalizedAmount;
}

function denormalizeAmount(normalizedAmount, rate) {
  return toBN(normalizedAmount.multipliedBy(rate).dividedToIntegerBy(DECIMAL).toFixed(0, 1));
}

module.exports = {
  Q: Q,
  DECIMAL: DECIMAL,
  toBN: toBN,
  getPercentageFormat: getPercentageFormat,
  getFromPercentageFormat: getFromPercentageFormat,
  calculatePercentage: calculatePercentage,
  getTransferFeePercentage,
  getAmountAfterTransfer,
  getNextCompoundRate,
  normalizeAmount,
  denormalizeAmount,
};
