const ExceptionStorage = artifacts.require('ExceptionStorage');
const fs = require('fs');
const csv = require('csv-parser');

async function createExceptionStorage() {
  const s = await ExceptionStorage.new();
  await fillExceptionStorage(s);
  return s;
}

async function fillExceptionStorage(storageContract) {
  const contracts = [
    // 17, // qVault
  ];
  if (contracts.length == 0) {
    return;
  }
  const entries = await readCsvBatch();
  const exceptionData = entries.map(mapCsvEntry).filter((e) => contracts.includes(e.contractId));
  const chunkSize = 50;
  let start = 0;

  const batches = [];

  do {
    const chunkInput = exceptionData.slice(start, start + chunkSize);
    const codes = chunkInput.map((e) => e.code);
    const messages = chunkInput.map((e) => e.message);
    batches.push(storageContract.setExceptions(codes, messages));
    start += chunkSize;
  } while (start < exceptionData.length);

  return Promise.all(batches);
}

async function readCsvBatch() {
  return new Promise((resolve, reject) => {
    const entries = [];
    try {
      fs.createReadStream('error-codes.csv')
        .pipe(csv())
        .on('data', async (data) => {
          entries.push(data);
        })
        .on('end', () => resolve(entries));
    } catch (e) {
      reject(e);
    }
  });
}

function mapCsvEntry(data) {
  return {
    code: Number.parseInt(data.Code),
    contractId: Math.floor(data.Code / 1000),
    message: '[QEC-' + data.Code + ']-' + cleanString(data.Message),
  };
}

function cleanString(str) {
  const symbolsForRemove = ['\r', '"'];
  while (symbolsForRemove.findIndex((e) => e == str[0]) > -1) {
    str = str.slice(1);
  }
  while (symbolsForRemove.findIndex((e) => e == str[str.length - 1]) > -1) {
    str = str.slice(0, str.length - 1);
  }
  return str;
}

module.exports = createExceptionStorage;
