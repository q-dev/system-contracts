const { toBN } = require('./defiHelper');
const calculatePercentage = require('./defiHelper').calculatePercentage;

const INITIAL_Q_AMOUNT = toBN(10).pow(9);
const Q = toBN(10).pow(18);

function approximateAssert(totalWeight, blockNumber, currentQuorum) {
  const firstQamount = toBN(blockNumber).minus(1).times(1.5).plus(INITIAL_Q_AMOUNT).times(Q);
  const firstExpectedQuorum = calculatePercentage(totalWeight, firstQamount);

  const secondQamount = toBN(blockNumber).plus(1).times(1.5).plus(INITIAL_Q_AMOUNT).times(Q);
  const secondExpectedQuorum = calculatePercentage(totalWeight, secondQamount);

  return firstExpectedQuorum.gte(currentQuorum) && secondExpectedQuorum.lte(currentQuorum);
}

async function lockWeight(bank, voter, weight) {
  await bank.deposit({ from: voter, value: toBN(weight) });
  await bank.lock(toBN(weight), { from: voter });
}

const getVetoTimeHelper = async (voting_, proposalId_) => {
  return toBN((await voting_.proposals(proposalId_)).base.params.vetoEndTime).toNumber();
};

const getVoteTimeHelper = async (voting_, proposalId_) => {
  return toBN((await voting_.proposals(proposalId_)).base.params.votingEndTime).toNumber();
};

module.exports = {
  approximateAssert: approximateAssert,
  lockWeight: lockWeight,
  getVetoTimeHelper,
  getVoteTimeHelper,
};
