const { assert } = require('chai');

function assertBnINRange(actual, expected, range) {
  const start = expected.minus(range);
  const end = expected.plus(range);
  assert.isTrue(
    end.isGreaterThanOrEqualTo(actual) || start.isLessThanOrEqualTo(actual),
    'Actual value "' + actual + '" out of range [' + start + '; ' + end + '].'
  );
}

module.exports = assertBnINRange;
