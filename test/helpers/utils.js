const BigNumber = require('bignumber.js');
const { network } = require('hardhat');

const { numberToRpcQuantity } = require('hardhat/internal/core/jsonrpc/types/base-types');

const toBN = (value) => new BigNumber(value);

const wei = (value, decimal = 18) => {
  return toBN(value).times(toBN(10).pow(decimal)).toFixed();
};

const fromWei = (value, decimal = 18) => {
  return toBN(value).div(toBN(10).pow(decimal)).toFixed();
};

const accounts = async (index) => {
  return (await web3.eth.getAccounts())[index];
};

const impersonateAccount = async (address) => {
  await network.provider.request({
    method: 'hardhat_impersonateAccount',
    params: [address],
  });
};

const setBalance = async (address, balance = 100000n) => {
  const quantity = numberToRpcQuantity(balance);

  await network.provider.request({
    method: 'hardhat_setBalance',
    params: [address, quantity],
  });
};

const getBalance = async (address) => {
  return await network.provider.request({
    method: 'eth_getBalance',
    params: [address],
  });
};

const accountsArray = async (_len, _from = 0) => {
  const accountsPromises = [];

  for (let i = _from; i < _from + _len; i++) {
    accountsPromises.push(accounts(i));
  }

  return Promise.all(accountsPromises);
};

module.exports = {
  toBN,
  accounts,
  wei,
  fromWei,
  setBalance,
  getBalance,
  accountsArray,
  impersonateAccount,
};
