const { accounts, accountsArray } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { setTime, getCurrentBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const ContractRegistry = artifacts.require('./ContractRegistry');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting');
const Parameters = artifacts.require('./Constitution');

describe('VotingWeightProxy', () => {
  const reverter = new Reverter();

  const proposalExecutionP = 2592000;
  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const Q = require('./helpers/defiHelper').Q;

  const checkDelegationInvariant = async (users) => {
    let totalRecievedWeights = toBN(0);
    let totalDelegatedWeights = toBN(0);

    for (let i = 0; i < users.length; i++) {
      const delegationInfo = await votingWeightProxy.delegationInfos(users[i]);
      if (!delegationInfo.isPending) {
        totalDelegatedWeights = totalDelegatedWeights.plus(await votingWeightProxy.getLockedAmount(users[i]));
      }
      totalRecievedWeights = totalRecievedWeights.plus(delegationInfo.receivedWeight);
    }

    assert.equal(totalRecievedWeights.toString(), totalDelegatedWeights.toString());
  };

  let OWNER;
  let USER;
  let USER2;
  let USER3;
  let Q_VAULT;
  let ROOTS;
  let VALIDATORS;
  let NON_TOKEN_LOCK_SOURCE;
  let PARAMETERS_VOTING;
  const ADDRESS_NULL = '0x0000000000000000000000000000000000000000';

  let registry;
  let votingWeightProxy;
  let generalUpdateVote;
  let constitutionParameters;

  before(async () => {
    OWNER = await accounts(0);
    USER = await accounts(1);
    USER2 = await accounts(2);
    USER3 = await accounts(3);
    Q_VAULT = await accounts(4);
    ROOTS = await accounts(5);
    VALIDATORS = await accounts(6);
    NON_TOKEN_LOCK_SOURCE = await accounts(7);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    constitutionParameters = await makeProxy(registry.address, Parameters);
    await constitutionParameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitutionParameters.setUint('constitution.proposalExecutionP', proposalExecutionP, {
      from: PARAMETERS_VOTING,
    }); // month
    await constitutionParameters.setUint('constitution.voting.changeQnotConstVP', votingPeriod, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, {
      from: PARAMETERS_VOTING,
    });
    await registry.setAddress('governance.constitution.parameters', constitutionParameters.address);

    await registry.setAddress('governance.rootNodes', ROOTS, { from: OWNER });
    await registry.setAddress('tokeneconomics.qVault', Q_VAULT, { from: OWNER });
    await registry.setAddress('governance.validators', VALIDATORS, { from: OWNER });

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['governance.rootNodes', 'tokeneconomics.qVault', 'governance.validators'],
      ['governance.constitution.parametersVoting', 'governance.generalUpdateVoting']
    );

    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('getVotingWeight', () => {
    const lockedAmount = Q.times(100);
    const announceUnlockAmount = Q.times(40);
    const lockedNeadedUntil = toBN(votingPeriod).plus(10);

    beforeEach('setup', async () => {
      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: USER });
    });

    afterEach('check delegation invariant', async () => {
      await checkDelegationInvariant(await accountsArray(10), await accountsArray(10));
    });

    it('should return the correct weight when the user does not have a voting agent', async () => {
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      const currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER, lockedNeadedUntil.minus(20)));

      assert.equal(currentVotingWeight.toString(), lockedAmount.toString());
    });

    it('should return the correct weight for the voting agent when it does not have its own locked weight; the user can vote with their announced tokens', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      await setTime((await getCurrentBlockTime()) + lockedNeadedUntil.toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      let currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER2, lockedNeadedUntil));
      assert.equal(currentVotingWeight.toString(), lockedAmount.minus(announceUnlockAmount).toString());

      currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER, lockedNeadedUntil.minus(100)));
      assert.equal(currentVotingWeight.toString(), 0);

      await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER, lockedNeadedUntil.minus(100)));
      assert.equal(currentVotingWeight.toString(), lockedAmount.toString());
    });

    it('should return the correct weight for the voting agent', async () => {
      await votingWeightProxy.lock(USER2, lockedAmount, { from: Q_VAULT });
      await votingWeightProxy.lock(USER3, lockedAmount, { from: Q_VAULT });

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER3 });
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      await setTime((await getCurrentBlockTime()) + lockedNeadedUntil.toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER3 });

      const currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER2, lockedNeadedUntil.minus(20)));
      assert.equal(currentVotingWeight.toString(), lockedAmount.times(3).minus(announceUnlockAmount).toString());
    });

    it('should return the correct voting weight for the voting agent who delegated their weight to another voting agent', async () => {
      await votingWeightProxy.lock(USER2, lockedAmount, { from: Q_VAULT });

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.announceNewVotingAgent(USER3, { from: USER2 });
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      await setTime((await getCurrentBlockTime()) + lockedNeadedUntil.toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER2 });

      let currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER2, lockedNeadedUntil));
      assert.equal(currentVotingWeight.toString(), lockedAmount.minus(announceUnlockAmount).toString());

      currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER3, lockedNeadedUntil));
      assert.equal(currentVotingWeight.toString(), lockedAmount.toString());
    });

    it(
      'should return the correct voting weight ' + 'for the user when he is the voting agent for himself',
      async () => {
        await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });

        await votingWeightProxy.lock(USER, lockedAmount, { from: Q_VAULT });
        await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

        const lockedTime = (await getCurrentBlockTime()) + lockedNeadedUntil.toNumber();
        await setTime(lockedTime);
        await votingWeightProxy.setNewVotingAgent({ from: USER });

        const currentVotingWeight = toBN(await votingWeightProxy.getVotingWeight(USER, lockedTime));
        assert.equal(currentVotingWeight.toString(), lockedAmount.times(2).minus(announceUnlockAmount).toString());
      }
    );
  });

  describe('Double usage of voting power example', () => {
    const lockedAmount = Q.times(1000);

    it('should not use voting power twice with voting agent', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      let votingPowerUSER2 = toBN(await votingWeightProxy.getVotingWeight(USER2, 100));
      assert.equal(votingPowerUSER2.toString(), '0');

      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: USER2 });

      const lockedUntil = toBN(await votingWeightProxy.getLockedUntil(USER2));

      let votingPowerUSER = await votingWeightProxy.getVotingWeight(USER, lockedUntil);
      assert.equal(votingPowerUSER.toString(), '0');

      votingPowerUSER2 = await votingWeightProxy.getVotingWeight(USER2, lockedUntil);

      assert.equal(votingPowerUSER2.toString(), lockedAmount.toString());

      await votingWeightProxy.announceUnlock(USER, lockedAmount, lockedUntil.plus(1), { from: VALIDATORS });

      votingPowerUSER = toBN(await votingWeightProxy.getVotingWeight(USER, lockedUntil));

      assert.equal(votingPowerUSER.toString(), 0);

      const reason = '[QEC-029003]-The total voting weight must be greater than zero, failed to vote.';

      await truffleAssert.reverts(generalUpdateVote.voteFor(0, { from: USER }), reason);
    });

    it('should not use voting power twice with self voting agent', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      let votingPowerUSER2 = toBN(await votingWeightProxy.getVotingWeight(USER2, 100));
      assert.equal(votingPowerUSER2.toString(), '0');

      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: USER2 });

      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), lockedAmount.toString());

      const lockedUntil = toBN(await votingWeightProxy.getLockedUntil(USER2));

      let votingPowerUSER = await votingWeightProxy.getVotingWeight(USER, lockedUntil);
      assert.equal(votingPowerUSER.toString(), '0');

      votingPowerUSER2 = await votingWeightProxy.getVotingWeight(USER2, lockedUntil);

      assert.equal(votingPowerUSER2.toString(), lockedAmount.toString());

      await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });
      await votingWeightProxy.announceUnlock(USER, lockedAmount, lockedUntil.plus(1), { from: VALIDATORS });

      votingPowerUSER = toBN(await votingWeightProxy.getVotingWeight(USER, lockedUntil));

      const reason = '[QEC-029003]-The total voting weight must be greater than zero, failed to vote.';

      await truffleAssert.reverts(generalUpdateVote.voteFor(0, { from: USER }), reason);
    });

    afterEach('check delegation invariant', async () => {
      await checkDelegationInvariant(await accountsArray(10));
    });
  });

  describe('getLockInfo', () => {
    it('should return correct lock info', async () => {
      let currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), toBN(0).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), toBN(0).toString());

      await votingWeightProxy.lock(USER, 1000, { from: VALIDATORS });

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.announceUnlock(USER, 400, 0, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), toBN(600).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), toBN(400).toString());

      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);
    });

    it('should get exception, token lock source is not contained in the list', async () => {
      const reason = '[QEC-028001]-Unknown token lock source.';
      await truffleAssert.reverts(votingWeightProxy.getLockInfo(NON_TOKEN_LOCK_SOURCE, USER), reason);
    });
  });

  describe('getLockedAmount', () => {
    it('should return correct locked amount', async () => {
      let currentLockedAmount = toBN(await votingWeightProxy.getLockedAmount(USER));
      assert.equal(currentLockedAmount.toString(), toBN(0).toString());

      const amount = 1000;
      await votingWeightProxy.lock(USER, amount, { from: VALIDATORS });
      await votingWeightProxy.lock(USER, amount, { from: ROOTS });
      await votingWeightProxy.lock(USER, amount, { from: Q_VAULT });

      currentLockedAmount = toBN(await votingWeightProxy.getLockedAmount(USER));
      assert.equal(currentLockedAmount.toString(), toBN(amount).times(3).toString());

      currentLockedAmount = toBN(await votingWeightProxy.getLockedAmount(USER2));
      assert.equal(currentLockedAmount.toString(), toBN(0).toString());

      await votingWeightProxy.lock(USER2, amount, { from: VALIDATORS });
      await votingWeightProxy.lock(USER2, amount, { from: Q_VAULT });

      currentLockedAmount = toBN(await votingWeightProxy.getLockedAmount(USER2));

      assert.equal(currentLockedAmount.toString(), toBN(amount).times(2).toString());
    });

    it('should return zero if nothing is locked', async () => {
      const currentLockedAmount = toBN(await votingWeightProxy.getLockedAmount(USER));
      assert.equal(currentLockedAmount.toString(), toBN(0).toString());
    });
  });

  describe('lock', () => {
    it('should increase lockedAmount after lock', async () => {
      let currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 0);

      const result = await votingWeightProxy.lock(USER, 1000, { from: Q_VAULT });

      currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 1000);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'LockedAmountChanged');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._newLockedAmount, 1000);
    });

    it('should get exception, call from not token lock source', async () => {
      let currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 0);

      const reason = '[QEC-028007]-Permission denied - only the token lock sources have access.';
      await truffleAssert.reverts(votingWeightProxy.lock(USER, 1000, { from: NON_TOKEN_LOCK_SOURCE }), reason);

      currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 0);
    });

    it('should get exception, try to lock zero amount', async () => {
      let currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 0);

      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await truffleAssert.reverts(votingWeightProxy.lock(USER, 0, { from: ROOTS }), reason);

      currentLockedAmount = (await votingWeightProxy.getLockInfo(Q_VAULT, USER)).lockedAmount;
      assert.equal(currentLockedAmount, 0);
    });
  });

  describe('announceUnlock', () => {
    const amount = toBN(10).pow(18);
    const announceUnlockAmount = amount.dividedBy(2);

    beforeEach(async () => {
      await votingWeightProxy.lock(USER, amount, { from: VALIDATORS });
    });

    it('should announce unlock successfully', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      const result = await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      const currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);

      assert.equal(result.logs.length, 2);
      assert.equal(result.logs[1].event, 'PendingUnlockChanged');
      assert.equal(result.logs[1].args._who, USER);
      assert.equal(result.logs[1].args._newPendingUnlockAmount, announceUnlockAmount.toString());
    });

    it('should announce successful unlock several times', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      let currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);

      const secondAnnounceUnlockAmount = amount.dividedBy(4);

      await setTime((await getCurrentBlockTime()) + 10);
      await votingWeightProxy.announceUnlock(USER, secondAnnounceUnlockAmount, 0, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(secondAnnounceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), secondAnnounceUnlockAmount.toString());
      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);
    });

    it('should get exception, attempt announce unlock amount greater than locked amount', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });

      let currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);

      const reason = '[QEC-028003]-Cannot unlock more than is currently locked.';
      await truffleAssert.reverts(
        votingWeightProxy.announceUnlock(USER, amount.plus(announceUnlockAmount), 0, { from: VALIDATORS }),
        reason
      );

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);
    });

    it('should successfully announce unlock zero amount', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.announceUnlock(USER, 0, 0, { from: VALIDATORS });

      const currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), toBN(0).toString());
      assert.equal(toBN(0).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should announce unlock successfully at specific time', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      const announceUnlockTime = 2;
      const result = await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, announceUnlockTime, {
        from: VALIDATORS,
      });

      const currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      assert.equal(currentLockInfo.pendingUnlockTime, announceUnlockTime);

      assert.equal(result.logs.length, 2);
      assert.equal(result.logs[1].event, 'PendingUnlockChanged');
      assert.equal(result.logs[1].args._who, USER);
      assert.equal(result.logs[1].args._newPendingUnlockAmount, announceUnlockAmount.toString());
      assert.equal(result.logs[1].args._newPendingUnlockTime, announceUnlockTime.toString());
    });

    it('should decrease lock amount, increase pendingUnlockAmount', async () => {
      await setTime((await getCurrentBlockTime()) + 1);

      const firstUnlockAmount = announceUnlockAmount;
      const firstResult = await votingWeightProxy.announceUnlock(USER, firstUnlockAmount, 0, { from: VALIDATORS });
      const firstLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(firstLockInfo.lockedAmount).toString(), amount.minus(firstUnlockAmount).toString());
      assert.equal(toBN(firstLockInfo.pendingUnlockAmount).toString(), firstUnlockAmount.toString());

      const secondUnlockAmount = announceUnlockAmount.plus(1);
      const secondResult = await votingWeightProxy.announceUnlock(USER, secondUnlockAmount, 0, { from: VALIDATORS });
      const secondLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(secondLockInfo.lockedAmount).toString(), amount.minus(secondUnlockAmount).toString());
      assert.equal(toBN(secondLockInfo.pendingUnlockAmount).toString(), secondUnlockAmount.toString());

      assert.equal(firstResult.logs.length, 2);
      assert.equal(firstResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(firstResult.logs[1].args._who, USER);
      assert.equal(firstResult.logs[1].args._newPendingUnlockAmount, firstUnlockAmount.toString());
      assert.equal(secondResult.logs.length, 2);
      assert.equal(secondResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(secondResult.logs[1].args._who, USER);
      assert.equal(secondResult.logs[1].args._newPendingUnlockAmount, secondUnlockAmount.toString());
    });

    it('should increase lock amount, decrease pendingUnlockAmount', async () => {
      await setTime((await getCurrentBlockTime()) + 1);

      const firstUnlockAmount = announceUnlockAmount;
      const firstResult = await votingWeightProxy.announceUnlock(USER, firstUnlockAmount, 0, { from: VALIDATORS });
      const firstLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(firstLockInfo.lockedAmount).toString(), amount.minus(firstUnlockAmount).toString());
      assert.equal(toBN(firstLockInfo.pendingUnlockAmount).toString(), firstUnlockAmount.toString());

      const secondUnlockAmount = announceUnlockAmount.minus(1);
      const secondResult = await votingWeightProxy.announceUnlock(USER, secondUnlockAmount, 0, { from: VALIDATORS });
      const secondLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(secondLockInfo.lockedAmount).toString(), amount.minus(secondUnlockAmount).toString());
      assert.equal(toBN(secondLockInfo.pendingUnlockAmount).toString(), secondUnlockAmount.toString());

      assert.equal(firstResult.logs.length, 2);
      assert.equal(firstResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(firstResult.logs[1].args._who, USER);
      assert.equal(firstResult.logs[1].args._newPendingUnlockAmount, firstUnlockAmount.toString());
      assert.equal(secondResult.logs.length, 2);
      assert.equal(secondResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(secondResult.logs[1].args._who, USER);
      assert.equal(secondResult.logs[1].args._newPendingUnlockAmount, secondUnlockAmount.toString());
    });

    it('should increase pendingUnlockTime', async () => {
      await setTime((await getCurrentBlockTime()) + 1);

      const firstLockedUntilTime = 0;
      const firstResult = await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, firstLockedUntilTime, {
        from: VALIDATORS,
      });
      const firstLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(firstLockInfo.pendingUnlockTime).toString(), firstLockedUntilTime.toString());

      const secondLockedUntilTime = 10;
      const secondResult = await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, secondLockedUntilTime, {
        from: VALIDATORS,
      });
      const secondLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(secondLockInfo.pendingUnlockTime).toString(), secondLockedUntilTime.toString());

      const thirdLockedUntilTime = 20;
      const thirdResult = await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, thirdLockedUntilTime, {
        from: VALIDATORS,
      });
      const thirdLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.equal(toBN(thirdLockInfo.pendingUnlockTime).toString(), thirdLockedUntilTime.toString());

      assert.equal(firstResult.logs.length, 2);
      assert.equal(firstResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(firstResult.logs[1].args._who, USER);
      assert.equal(firstResult.logs[1].args._newPendingUnlockTime, firstLockedUntilTime.toString());
      assert.equal(secondResult.logs.length, 2);
      assert.equal(secondResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(secondResult.logs[1].args._who, USER);
      assert.equal(secondResult.logs[1].args._newPendingUnlockTime, secondLockedUntilTime.toString());
      assert.equal(thirdResult.logs.length, 2);
      assert.equal(thirdResult.logs[1].event, 'PendingUnlockChanged');
      assert.equal(thirdResult.logs[1].args._who, USER);
      assert.equal(thirdResult.logs[1].args._newPendingUnlockTime, thirdLockedUntilTime.toString());
    });
  });

  describe('unlock', () => {
    const amount = toBN(10).pow(18);
    const unlockAmount = amount.dividedBy(2);
    let currentLockInfo;

    beforeEach(async () => {
      await votingWeightProxy.lock(USER, amount, { from: VALIDATORS });
      await votingWeightProxy.announceUnlock(USER, amount, 0, { from: VALIDATORS });
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.toString());
      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);
    });

    it('should unlock successfully', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      const result = await votingWeightProxy.unlock(USER, unlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.minus(unlockAmount).toString());

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'PendingUnlockAmountChanged');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._newPendingUnlockAmount, amount.minus(unlockAmount).toString());
    });

    it('should unlock successfully multiple times', async () => {
      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.unlock(USER, unlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.minus(unlockAmount).toString());

      const secondUnlockAmount = unlockAmount.dividedBy(2);
      await votingWeightProxy.unlock(USER, secondUnlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      const totalUnlockAmount = secondUnlockAmount.plus(unlockAmount);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.minus(totalUnlockAmount).toString());
    });

    it('should not get exception, try to unlock before the expiration of the pending unlock time', async () => {
      // const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.unlock(USER, unlockAmount, { from: VALIDATORS });
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.minus(unlockAmount).toString());
    });

    it('should not get exception, try to unlock amount greater than pending unlock amount', async () => {
      // const reason = '[QEC-028005]-Smart unlock not possible, tokens are still locked by recent voting.';

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.unlock(USER, unlockAmount.plus(1), { from: VALIDATORS });
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), unlockAmount.minus(1).toString());
    });

    it(`should reduce voting agent's weight if smart unlock reduces locked amount`, async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER });
      await votingWeightProxy.lock(USER, amount, { from: VALIDATORS });

      let currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      let delegationInfo = await votingWeightProxy.delegationInfos(USER2);
      assert.equal(
        currentLockInfo.lockedAmount.toString(),
        delegationInfo.receivedWeight.toString(),
        'received weight of user2 must match locked amount of user1'
      );

      await setTime((await getCurrentBlockTime()) + 1);
      await votingWeightProxy.unlock(USER, amount.times(1.1), { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      delegationInfo = await votingWeightProxy.delegationInfos(USER2);

      assert.equal(
        currentLockInfo.lockedAmount.toString(),
        delegationInfo.receivedWeight.toString(),
        'received weight of user2 must match locked amount of user1'
      );
    });

    it('should get exception, try to unlock zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';

      await setTime((await getCurrentBlockTime()) + 1);
      await truffleAssert.reverts(votingWeightProxy.unlock(USER, 0, { from: VALIDATORS }), reason);
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.toString());
    });
  });

  describe('forceUnlock', () => {
    const amount = toBN(10).pow(18);
    const announceUnlockAmount = amount.dividedBy(2);
    let unlockAmount;
    let currentLockInfo;

    beforeEach(async () => {
      unlockAmount = amount.dividedBy(4);
      await votingWeightProxy.lock(USER, amount, { from: VALIDATORS });
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
      assert.equal(currentLockInfo.pendingUnlockTime, currentLockInfo.lockedUntil);
    });

    it('should successfully force unlock if the amount is less than the pending unlock amount', async () => {
      const result = await votingWeightProxy.forceUnlock(USER, unlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceUnlockAmount.minus(unlockAmount).toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'PendingUnlockAmountChanged');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._newPendingUnlockAmount, announceUnlockAmount.minus(unlockAmount).toString());
    });

    it('should successfully force unlock if the amount is less than the locked amount but greater than pending unlock amount', async () => {
      unlockAmount = announceUnlockAmount.plus(1);
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      assert.isTrue(unlockAmount > currentLockInfo.pendingUnlockAmount);

      await votingWeightProxy.forceUnlock(USER, unlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(unlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), toBN(0).toString());
    });

    it(`should reduce voting agent's weight if forced unlock reduces locked amount`, async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      let currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      let delegationInfo = await votingWeightProxy.delegationInfos(USER2);

      assert.equal(
        currentLockInfo.lockedAmount.toString(),
        delegationInfo.receivedWeight.toString(),
        'received weight of user2 must match locked amount of user1'
      );

      unlockAmount = announceUnlockAmount.times(1.1);
      await votingWeightProxy.forceUnlock(USER, unlockAmount, { from: VALIDATORS });

      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);
      delegationInfo = await votingWeightProxy.delegationInfos(USER2);

      assert.equal(
        currentLockInfo.lockedAmount.toString(),
        delegationInfo.receivedWeight.toString(),
        'received weight of user2 must match locked amount of user1'
      );
    });

    it('should get exception, try to force unlock amount greater than locked amount', async () => {
      const reason = '[QEC-028006]-Cannot enforce to unlock more than is currently locked.';

      await setTime((await getCurrentBlockTime()) + 1);
      await truffleAssert.reverts(votingWeightProxy.forceUnlock(USER, amount.plus(1), { from: VALIDATORS }), reason);
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });

    it('should get exception, try to force unlock zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';

      await setTime((await getCurrentBlockTime()) + 1);
      await truffleAssert.reverts(votingWeightProxy.forceUnlock(USER, 0, { from: VALIDATORS }), reason);
      currentLockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER);

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });
  });

  describe('announceNewVotingAgent', () => {
    const lockedAmount = Q.times(1000);
    let delegationInfo;

    beforeEach('setup', async () => {
      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER, 'by default, users should be their own voting agent');
      assert.equal(delegationInfo.isPending, false);
    });

    afterEach('check delegation invariant', async () => {
      await checkDelegationInvariant(await accountsArray(10));
    });

    it('should correctly announce the voting agent for the first time', async () => {
      const result = await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER2);
      assert.equal(delegationInfo.isPending, true);

      assert.equal(
        toBN(delegationInfo.votingAgentPassOverTime).plus(2).toFixed(),
        toBN((await getCurrentBlockTime()) + votingPeriod).toFixed()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'NewVotingAgentAnnounced');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._prevVotingAgent, USER);
      assert.equal(result.logs[0].args._newVotingAgent, USER2);
    });

    it('should correctly announce the voting agent', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });

      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.toString()
      );

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(toBN(await votingWeightProxy.getLockedAmount(USER)).toString(), lockedAmount.toString());
      assert.equal(delegationInfo.votingAgent, USER2);
      assert.equal(delegationInfo.isPending, false);

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(1, { from: USER2 });

      await votingWeightProxy.announceNewVotingAgent(USER3, { from: USER });
      assert.equal(toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(), 0);

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER3);
      assert.equal(delegationInfo.isPending, true);

      assert.equal(
        toBN(delegationInfo.votingAgentPassOverTime).plus(2).toFixed(),
        toBN((await getCurrentBlockTime()) + votingPeriod).toFixed()
      );
    });

    it('should correctly announce the voting agent from someone else', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER });

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.toString()
      );

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(toBN(await votingWeightProxy.getLockedAmount(USER)).toString(), lockedAmount.toString());
      assert.equal(delegationInfo.votingAgent, USER2);
      assert.equal(delegationInfo.isPending, false);

      await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });
      assert.equal(toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(), 0);

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER);
      assert.equal(delegationInfo.isPending, true);

      assert.isTrue(
        toBN(delegationInfo.votingAgentPassOverTime)
          .minus(votingPeriod + 10)
          .lte(5)
      );
    });

    it('should correctly announce the voting agent from yourself to yourself', async () => {
      const result = await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER);
      assert.equal(delegationInfo.isPending, true);

      assert.equal(
        toBN(delegationInfo.votingAgentPassOverTime).plus(2).toFixed(),
        toBN((await getCurrentBlockTime()) + votingPeriod).toFixed()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'NewVotingAgentAnnounced');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._prevVotingAgent, USER);
      assert.equal(result.logs[0].args._newVotingAgent, USER);
    });

    it('should correctly announce the voting agent from yourself to yourself several times', async () => {
      for (let i = 0; i < 5; i++) {
        await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });
        await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });

        await setTime((await getCurrentBlockTime()) + votingPeriod);
        await votingWeightProxy.setNewVotingAgent({ from: USER });

        delegationInfo = await votingWeightProxy.delegationInfos(USER);
        assert.equal(toBN(delegationInfo.receivedWeight).toString(), lockedAmount.times(i + 2).toString());
        assert.equal(delegationInfo.votingAgent, USER);
        assert.equal(delegationInfo.isPending, false);

        assert.equal(
          toBN(delegationInfo.votingAgentPassOverTime)
            .plus(4)
            .plus(3 * i)
            .toFixed(),
          toBN((await getCurrentBlockTime()) - votingPeriod * i).toFixed()
        );
      }
    });

    it('should get exception if pass zero address as a new voting agent address', async () => {
      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });

      const reason = '[028011]-New voting agent cannot be a zero address.';

      await truffleAssert.reverts(votingWeightProxy.announceNewVotingAgent(ADDRESS_NULL, { from: USER }), reason);
    });

    it('should correctly set votingAgentPassOverTime and _getMaxLockedUntil must return correct value', async () => {
      const startTime = toBN(1000).plus(await getCurrentBlockTime());
      const USER4 = await accounts(8);

      await setTime(startTime.toNumber());
      await generalUpdateVote.createProposal('');

      await votingWeightProxy.lock(USER2, lockedAmount, { from: VALIDATORS });

      assert.equal(toBN(await votingWeightProxy.getLockedAmount(USER2)).toString(), lockedAmount.toString());

      await votingWeightProxy.announceNewVotingAgent(USER3, { from: USER2 });
      await votingWeightProxy.setNewVotingAgent({ from: USER2 });

      let delegationInfo = await votingWeightProxy.delegationInfos(USER2);

      assert.equal(delegationInfo.votingAgent, USER3);

      await generalUpdateVote.voteFor(1, { from: USER3 });

      await votingWeightProxy.announceNewVotingAgent(USER4, { from: USER2 });

      delegationInfo = await votingWeightProxy.delegationInfos(USER2);

      assert.equal(delegationInfo.votingAgent, USER4);
      assert.equal(delegationInfo.isPending, true);

      assert.closeTo(
        toBN(delegationInfo.votingAgentPassOverTime).toNumber(),
        startTime.plus(votingPeriod).toNumber(),
        10
      );

      const lockInfo = await votingWeightProxy.getLockInfo(VALIDATORS, USER2);

      assert.closeTo(toBN(lockInfo.lockedUntil).toNumber(), startTime.plus(votingPeriod).toNumber(), 10);

      const reason = '[QEC-028005]-Smart unlock not possible, tokens are still locked by recent voting.';
      await truffleAssert.reverts(votingWeightProxy.unlock(USER2, lockedAmount, { from: VALIDATORS }), reason);
    });
  });

  describe('setNewVotingAgent', async () => {
    const lockedAmount = Q.times(1000);
    const announceUnlockAmount = Q.times(500);
    let delegationInfo;

    beforeEach('setup', async () => {
      await votingWeightProxy.lock(USER, lockedAmount, { from: VALIDATORS });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.votingAgent, USER, 'by default, users should be their own voting agent');
      assert.equal(delegationInfo.isPending, false);
    });

    afterEach('check delegation invariant', async () => {
      await checkDelegationInvariant(await accountsArray(10));
    });

    it('should correctly change the voting agent to itself', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER, { from: USER });
      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);

      const result = await votingWeightProxy.setNewVotingAgent({ from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);
      assert.equal(delegationInfo.isPending, false);

      assert.equal(result.logs[0].event, 'VotingAgentChanged');
      assert.equal(result.logs[0].args._who, USER);
      assert.equal(result.logs[0].args._votingAgent, USER);
      assert.equal(toBN(result.logs[0].args._delegatedAmount).toString(), lockedAmount.toString());

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER)).receivedWeight).toString(),
        lockedAmount.toString()
      );
    });

    it('should change the voting agent correctly several times', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      let result = await votingWeightProxy.setNewVotingAgent({ from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);

      assert.equal(result.logs[0].event, 'VotingAgentChanged');
      assert.equal(result.logs[0].args._votingAgent, USER2);
      assert.equal(toBN(result.logs[0].args._delegatedAmount).toString(), lockedAmount.toString());

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.toString()
      );

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(1, { from: USER2 });

      await votingWeightProxy.announceNewVotingAgent(USER3, { from: USER });

      await setTime((await getCurrentBlockTime()) + 2 * votingPeriod);
      await votingWeightProxy.lock(USER, lockedAmount, { from: Q_VAULT });
      result = await votingWeightProxy.setNewVotingAgent({ from: USER });

      delegationInfo = await votingWeightProxy.delegationInfos(USER);

      assert.equal(result.logs[0].event, 'VotingAgentChanged');
      assert.equal(result.logs[0].args._votingAgent, USER3);
      assert.equal(toBN(result.logs[0].args._delegatedAmount).toString(), lockedAmount.times(2).toString());

      assert.equal(toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(), 0);
      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER3)).receivedWeight).toString(),
        lockedAmount.times(2).toString()
      );
    });

    it('should change the voting agent correctly with announce unlock', async () => {
      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      const result = await votingWeightProxy.setNewVotingAgent({ from: USER });

      assert.equal(result.logs[0].event, 'VotingAgentChanged');
      assert.equal(result.logs[0].args._votingAgent, USER2);
      assert.equal(
        toBN(result.logs[0].args._delegatedAmount).toString(),
        lockedAmount.minus(announceUnlockAmount).toString()
      );

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.minus(announceUnlockAmount).toString()
      );
    });

    it('should correctly update the total amount delegated to the voting agent', async () => {
      await votingWeightProxy.lock(USER3, lockedAmount, { from: VALIDATORS });
      await generalUpdateVote.voteFor(0, { from: USER3 });

      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER3 });

      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      await votingWeightProxy.setNewVotingAgent({ from: USER });
      await votingWeightProxy.setNewVotingAgent({ from: USER3 });

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.times(2).minus(announceUnlockAmount).toString()
      );
    });

    it('should correctly update the values after the lock and announcement of the unlock of tokens', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });
      await votingWeightProxy.lock(USER, lockedAmount, { from: Q_VAULT });

      await setTime((await getCurrentBlockTime()) + votingPeriod + 1);
      const result = await votingWeightProxy.setNewVotingAgent({ from: USER });

      assert.equal(result.logs[0].args._votingAgent, USER2);
      assert.equal(toBN(result.logs[0].args._delegatedAmount).toString(), lockedAmount.times(2).toString());

      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.times(2).toString()
      );

      await votingWeightProxy.announceUnlock(USER, announceUnlockAmount, 0, { from: VALIDATORS });
      assert.equal(
        toBN((await votingWeightProxy.delegationInfos(USER2)).receivedWeight).toString(),
        lockedAmount.times(2).minus(announceUnlockAmount).toString()
      );
    });

    it('should get exception, new voting agent not announced', async () => {
      const reason = '[QEC-028008]-Changing the voting agent has to be announced, failed to set new voting agent.';
      await truffleAssert.reverts(votingWeightProxy.setNewVotingAgent({ from: USER }), reason);
    });

    it('should get exception, passover time is not reached', async () => {
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER });

      const reason = '[QEC-028009]-Cannot change voting agent before passover time is reached.';
      await truffleAssert.reverts(votingWeightProxy.setNewVotingAgent({ from: USER }), reason);
    });
  });
});
