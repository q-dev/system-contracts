'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');

const { getVoteTimeHelper } = require('./helpers/votingHelper');
const { setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ValidatorsSlashingVoting = artifacts.require('./ValidatorsSlashingVoting');
const ValidatorSlashingEscrow = artifacts.require('./ValidatorSlashingEscrow');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const ValidationRewardPools = artifacts.require('./ValidationRewardPools');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require('Constitution');
const Roots = artifacts.require('Roots');
const Validators = artifacts.require('Validators');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const VotingWeightProxy = artifacts.require('VotingWeightProxy');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

describe('ValidatorsSlashingVoting', () => {
  const proposalExecutionP = 2592000;

  const Q = require('./helpers/defiHelper').Q;
  const CR_UPDATE_MINIMUM_BASE = Q.multipliedBy(1000);

  const reverter = new Reverter();

  let roots;
  let validators;
  let registry;
  let addressStorageStakesSorted;
  let addressStorageStakes;
  let constitution;
  let remark;
  let percentage;
  let validatorsSlashingVoting;
  let validatorSlashingEscrow;
  let validationRewardPools;
  let epqfiParams;
  let votingWeightProxy;

  let OWNER;
  let ROOT;
  let ROOT2;
  let ROOT3;
  let CANDIDATE;
  let NON_EXISTING_ROOT;
  let NON_EXISTING_VALIDATOR;
  let PARAMETERS_VOTING;

  before(async () => {
    OWNER = await accounts(0);
    ROOT = await accounts(1);
    ROOT2 = await accounts(6);
    ROOT3 = await accounts(3);
    CANDIDATE = await accounts(7);
    NON_EXISTING_ROOT = await accounts(2);
    NON_EXISTING_VALIDATOR = await accounts(8);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    const crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.voting.valSlashingVP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.valSlashingQRM', 0, { from: PARAMETERS_VOTING });
    const requiredMajority = getPercentageFormat(49); // it's 49% from 10 ** 27
    await constitution.setUint('constitution.voting.valSlashingRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.valSlashingOBJP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.valSlashingAppealP', 86400, { from: PARAMETERS_VOTING });

    await constitution.setUint('constitution.maxNValidators', 20, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.maxNStandbyValidators', 20, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.valWithdrawP', 21, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.cliqueEpochLength', 1, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month

    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT, ROOT2, ROOT3]);
    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);

    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    validatorsSlashingVoting = await makeProxy(registry.address, ValidatorsSlashingVoting);
    await validatorsSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingVoting', validatorsSlashingVoting.address);

    validatorSlashingEscrow = await makeProxy(registry.address, ValidatorSlashingEscrow);
    await validatorSlashingEscrow.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingEscrow', validatorSlashingEscrow.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(registry.address, ['governance.rootNodes', 'governance.validators'], []);
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    remark = 'https://ethereum.org';
    percentage = getPercentageFormat(60); // it's 60% from 10 ** 27

    const amount = 100000;
    await validators.commitStake({ from: CANDIDATE, value: amount });
    await validators.enterShortList({ from: CANDIDATE });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    const proposalID = 0;

    it('should create a proposal with existing root node', async () => {
      const result = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      assert.equal((await validatorsSlashingVoting.proposalCount()).toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.remark.toString(), remark);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).candidate.toString(), CANDIDATE);

      const validatorNodeStake = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      const expectedAmountToSlash = (percentage * validatorNodeStake) / getPercentageFormat(100);
      assert.equal(
        (await validatorsSlashingVoting.proposals(proposalID)).amountToSlash.toString(),
        expectedAmountToSlash.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);
    });

    it('should get exception, caller is not a root node', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: NON_EXISTING_ROOT }),
        '[QEC-006004]-Permission denied - only root nodes have access.'
      );

      assert.equal((await validatorsSlashingVoting.proposalCount()).toString(), 0);
    });

    it('should get exception, candidate is not a validator', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.createProposal(remark, NON_EXISTING_VALIDATOR, percentage, { from: ROOT }),
        '[QEC-006000]-The candidate does not have any stake to slash.'
      );

      assert.equal((await validatorsSlashingVoting.proposalCount()).toString(), 0);
    });

    it('should not be possible to vote if _percentage parameter is invalid', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(101), { from: ROOT }),
        '[QEC-006002]-Invalid percentage parameter, slashing proposal creation failed.'
      );

      assert.equal((await validatorsSlashingVoting.proposalCount()).toString(), 0);
    });

    it('should revert, try to create several proposals with same proposer and victim pair', async () => {
      const result = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      assert.equal((await validatorsSlashingVoting.proposalCount()).toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.remark.toString(), remark);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).candidate.toString(), CANDIDATE);

      const validatorNodeStake = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      const expectedAmountToSlash = (percentage * validatorNodeStake) / getPercentageFormat(100);
      assert.equal(
        (await validatorsSlashingVoting.proposals(proposalID)).amountToSlash.toString(),
        expectedAmountToSlash.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);

      const reason = '[QEC-006007]-The slashing proposal with same creator and victim exists.';
      await truffleAssert.reverts(
        validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT }),
        reason
      );

      const pendingSlashingProposals = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(
        pendingSlashingProposals.length,
        1,
        'pendingSlashingProposals should have only 1 proposal for CANDIDATE - proposer pair'
      );
    });
  });

  describe('vote', () => {
    let proposalID;
    beforeEach('init proposal', async () => {
      const res = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should be possible to vote for an existing pending proposal', async () => {
      const result = await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await validatorsSlashingVoting.voted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should be possible to vote against an existing pending proposal', async () => {
      const result = await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 0);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);
      assert.isTrue(await validatorsSlashingVoting.voted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 2);
    });

    it('should not be possible to vote if already voted', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await validatorsSlashingVoting.voted(0, ROOT));

      await truffleAssert.reverts(
        validatorsSlashingVoting.voteFor(proposalID, { from: ROOT }),
        '[QEC-006003]-The caller has already voted for the proposal.'
      );

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await validatorsSlashingVoting.voted(0, ROOT));
    });

    it('should not be possible to vote if sender not a root node', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.voteFor(proposalID, { from: NON_EXISTING_ROOT }),
        '[QEC-006004]-Permission denied - only root nodes have access.'
      );

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 0);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
    });

    it('should not be able to vote for non-existing proposal', async () => {
      const nonExistingID = proposalID + 1;
      await truffleAssert.reverts(
        validatorsSlashingVoting.voteAgainst(nonExistingID, { from: ROOT }),
        '[QEC-006005]-The slashing proposal does not exist.'
      );

      assert.equal((await validatorsSlashingVoting.proposals(nonExistingID)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await validatorsSlashingVoting.proposals(nonExistingID)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await validatorsSlashingVoting.voted(1, ROOT));
    });
  });

  describe('execute', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const res = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should successfully consume a PASSED proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 2);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      let status = (await validatorsSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      const validatorStake = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      const expectedAmountToSlash = (await validatorsSlashingVoting.proposals(proposalID)).amountToSlash;
      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));

      const txReceipt = await validatorsSlashingVoting.execute(proposalID, { from: ROOT });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalID);
      assert.equal(txReceipt.logs[0].args._candidate, CANDIDATE);
      assert.equal(txReceipt.logs[0].args._amountToSlash.toString(), expectedAmountToSlash.toString());

      status = (await validatorsSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is consumed');

      const validatorStakeAfterSlashing = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      const expectedStake = validatorStake - expectedAmountToSlash;
      assert.equal(expectedStake, validatorStakeAfterSlashing.toString());

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      assert.equal(escrowBalance.plus(expectedAmountToSlash).toString(), escrowBalanceAfterSlashing.toString());
    });

    it('should not be possible to execute expired proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 2);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      let status = (await validatorsSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      await setTime(voteEndTime + proposalExecutionP + 1);

      await truffleAssert.reverts(
        validatorsSlashingVoting.execute(proposalID, { from: ROOT }),
        '[QEC-006006]-The slashing proposal has not passed.'
      );

      status = (await validatorsSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXPIRED, status, 'unexpected status after proposal is consumed');
    });

    it('should only be able to consume a PASSED proposal', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.execute(proposalID, { from: ROOT }),
        '[QEC-006006]-The slashing proposal has not passed.'
      );

      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });
  });

  describe('getStatus', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const res = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await validatorsSlashingVoting.getStatus(1)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT2 });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 2);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await constitution.setUint('constitution.voting.valSlashingQRM', getPercentageFormat(80), {
        from: PARAMETERS_VOTING,
      });

      const result = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });
      const proposalID = Number(result.logs[0].args._id);

      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 2);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED on expired proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime + proposalExecutionP + 1);

      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a consumed proposal', async () => {
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 1);
      assert.equal((await validatorsSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
      await setTime(voteEndTime);

      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });
      assert.equal((await validatorsSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.EXECUTED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const firstProp = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const firstPropID = Number(firstProp.logs[0].args._id);
      const firstPropBase = (await validatorsSlashingVoting.proposals.call(firstPropID)).base;

      let expectedBase = await validatorsSlashingVoting.getProposal(firstPropID);

      assert.equal(expectedBase.remark, firstPropBase.remark);
      assert.equal(expectedBase.executed, firstPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, firstPropBase.params.votingEndTime);

      const secondProp = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });
      const secondPropID = Number(secondProp.logs[0].args._id);
      const secondPropBase = (await validatorsSlashingVoting.proposals.call(secondPropID)).base;

      expectedBase = await validatorsSlashingVoting.getProposal(secondPropID);

      assert.equal(expectedBase.remark, secondPropBase.remark);
      assert.equal(expectedBase.executed, secondPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, secondPropBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.getProposal(1),
        '[QEC-006005]-The slashing proposal does not exist.'
      );
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      const result = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const propID = Number(result.logs[0].args._id);

      await validatorsSlashingVoting.voteFor(propID, { from: ROOT2 });
      await validatorsSlashingVoting.voteAgainst(propID, { from: ROOT3 });

      const propStats = await validatorsSlashingVoting.getProposalStats(propID);
      const prop = await validatorsSlashingVoting.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const votesCount = await toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, votesCount);
      const rootsCount = 3;
      const currentQuorum = calculatePercentage(votesCount, rootsCount);

      assert.equal(prop.base.counters.weightFor, 1);
      assert.equal(await votesCount.toNumber(), 2);

      assert.equal(await currentMajority.toNumber(), propStats.currentMajority);
      assert.equal(await currentQuorum.toNumber(), propStats.currentQuorum);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(
        validatorsSlashingVoting.getProposalStats(1),
        '[QEC-006005]-The slashing proposal does not exist.'
      );
    });
  });
});
