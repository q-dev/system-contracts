'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const RootNodesSlashingVoting = artifacts.require('RootNodesSlashingVoting');
const RootNodeSlashingEscrow = artifacts.require('RootNodeSlashingEscrow');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require('Constitution');
const Roots = artifacts.require('Roots');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const DefaultAllocationProxy = artifacts.require('DefaultAllocationProxy');
const PushPayments = artifacts.require('PushPayments');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const QVault = artifacts.require('QVault');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ArbitrationStatus = Object.freeze({
  NONE: 0,
  OPEN: 1,
  ACCEPTED: 2,
  PENDING: 3,
  DECIDED: 4,
  EXECUTED: 5,
});

describe('RootNodeSlashingEscrow', () => {
  const reverter = new Reverter();

  let registry;
  let constitution;
  let rootNodesSlashingVoting;
  let rootNodeSlashingEscrow;
  let roots;
  let epqfiParams;
  let defaultAllocationProxy;
  let pushPayments;
  let votingWeightProxy;
  let crKeeperFactory;
  let qVault;

  let rootsAddress;
  const rootSlashingOBJP = 86400;
  const rootSlashingAppealP = 86400;
  const rootSlashingVP = 86400;
  const requiredMajority = getPercentageFormat(50); // it's 50% from 10 ** 27

  const commitAmount = 100 * 10 ** 18;

  let OWNER;
  let ROOT;
  let ROOT2;
  let ROOT3;
  let NOT_ROOT;
  let ROOTS_CONTRACT;
  let SLASHING_VOTING_CONTRACT;
  let PARAMETERS_VOTING;

  before(async () => {
    OWNER = await accounts(0);
    ROOT = await accounts(1);
    ROOT2 = await accounts(2);
    ROOT3 = await accounts(3);
    NOT_ROOT = await accounts(4);
    ROOTS_CONTRACT = await accounts(8);
    SLASHING_VOTING_CONTRACT = await accounts(8);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });
    await constitution.setUint('constitution.proposalExecutionP', 1000, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingVP', rootSlashingVP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingQRM', 0, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingRNVALP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingSQRM', getPercentageFormat(1), {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.rootSlashingSMAJ', getPercentageFormat(80), {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.rootSlashingOBJP', rootSlashingOBJP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.rootSlashingAppealP', rootSlashingAppealP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.rootSlashingReward', getPercentageFormat(5), { from: PARAMETERS_VOTING });

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.rootNodes', 'governance.validators'],
      ['governance.rootNodes.slashingVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address, { from: OWNER });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT, ROOT2, ROOT3]);
    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });
    rootsAddress = roots.address;

    rootNodesSlashingVoting = await makeProxy(registry.address, RootNodesSlashingVoting);
    rootNodesSlashingVoting.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.rootNodes.slashingVoting', rootNodesSlashingVoting.address, { from: OWNER });

    rootNodeSlashingEscrow = await makeProxy(registry.address, RootNodeSlashingEscrow);
    await rootNodeSlashingEscrow.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.rootNodes.slashingEscrow', rootNodeSlashingEscrow.address, { from: OWNER });

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(registry.address, [], []);
    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocationProxy.address, { from: OWNER });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('open()', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://ethereum.com';
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: ROOT2 });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should be possible to open the arbitration', async () => {
      const amount = toBN(1000);
      const slashingVotingBalance = toBN(await web3.eth.getBalance(ROOTS_CONTRACT));
      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));

      const result = await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: amount });

      const gasUsed = toBN(result.receipt.gasUsed);
      const slashingVotingBalanceAfterTransaction = toBN(await web3.eth.getBalance(ROOTS_CONTRACT));
      const escrowBalanceAfterTransaction = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));

      // For berlin hardfork
      const gasPrice = rootNodeSlashingEscrow.constructor.class_defaults.gasPrice;
      assert.equal(
        slashingVotingBalance.minus(amount).minus(gasUsed.multipliedBy(gasPrice)).toString(),
        slashingVotingBalanceAfterTransaction.toString()
      );

      // For london hardfork
      // assert.equal(
      //   slashingVotingBalance.minus(amount).minus(gasUsed.multipliedBy(result.receipt.effectiveGasPrice)).toString(),
      //   slashingVotingBalanceAfterTransaction.toString(),
      // );
      assert.equal(escrowBalance.plus(amount).toString(), escrowBalanceAfterTransaction.toString());
      assert.equal(
        (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount.toString(),
        amount.toString()
      );
    });

    it('should not be possible to open the arbitration if arbitration for the proposal already exists', async () => {
      const amount = toBN(100000000);
      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));

      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: amount });

      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await registry.setAddress('governance.rootNodes.slashingVoting', SLASHING_VOTING_CONTRACT, { from: OWNER });
      await truffleAssert.reverts(
        roots.applySlashing(proposalID, ROOT, amount, { from: SLASHING_VOTING_CONTRACT }),
        '[QEC-027000]-The arbitration already exists.'
      );
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
      await registry.setAddress('governance.rootNodes.slashingVoting', rootNodesSlashingVoting.address, {
        from: OWNER,
      });

      const escrowBalanceAfterTransaction = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));

      assert.equal(escrowBalance.plus(amount).toString(), escrowBalanceAfterTransaction.toString());
    });

    it('should not be possible to open the arbitration if caller is not a RootNodesSlashingVoting contract', async () => {
      const amount = toBN(2000);
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.open(proposalID, { from: ROOT, value: amount }),
        '[QEC-027013]-Permission denied - only the associated contract has access.'
      );
    });
  });

  describe('getStatus()', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://ethereum.com';
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: ROOT2 });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
    });

    it('should return status OPEN if objection period is open', async () => {
      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);
      await setTime(objectionEndTime.minus(30).toNumber());

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);
    });

    it('should return status PENDING', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should return status DECIDED', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });
      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      await rootNodeSlashingEscrow.confirmDecision(proposalID, info.decision.hash, { from: ROOT3 });
    });

    it('should return status EXECUTED', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });
      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      await rootNodeSlashingEscrow.confirmDecision(proposalID, info.decision.hash, { from: ROOT3 });
      await rootNodeSlashingEscrow.execute(proposalID);

      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should return status ACCEPTED', async () => {
      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);

      await setTime(objectionEndTime.plus(rootSlashingVP + 1).toNumber());

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.ACCEPTED);
    });

    it('should return status NONE', async () => {
      assert.equal((await rootNodeSlashingEscrow.getStatus(100)).toString(), ArbitrationStatus.NONE);
    });
  });

  describe('getProposal()', () => {
    let proposalID;
    const percentage = getPercentageFormat(60);
    const remark = 'http://remark.com';

    beforeEach('init proposal', async () => {
      await roots.commitStake({ from: ROOT2, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT2, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should get the proposal', async () => {
      const proposalFromEscrow = await rootNodeSlashingEscrow.getProposal(proposalID);
      const proposalFromSlashingVoting = await rootNodesSlashingVoting.getProposal(proposalID);

      assert.equal(proposalFromEscrow.remark, remark);
      assert.equal(proposalFromEscrow.toString(), proposalFromSlashingVoting.toString());
    });

    it('should not get the non-existing proposal', async () => {
      await truffleAssert.reverts(rootNodeSlashingEscrow.getProposal(100));
    });
  });

  describe('castObjection()', () => {
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://remark.com';
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: ROOT2 });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should successfully cast the objection', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 10000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);

      await rootNodeSlashingEscrow.castObjection(proposalID, newRemark, { from: ROOT });

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), newRemark);
    });

    it('should emit correct event', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 10000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      const result = await rootNodeSlashingEscrow.castObjection(proposalID, newRemark, { from: ROOT });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'RemarkUpdated');
      assert.equal(result.logs[0].args._id, proposalID);
      assert.equal(result.logs[0].args._remark, newRemark);
    });

    it('should not cast the objection if the caller is not a slashing victim', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 10000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.castObjection(proposalID, newRemark, { from: NOT_ROOT }),
        '[QEC-027014]-Permission denied - only the slashing target has access.'
      );

      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);
    });

    it('should not cast the objection if the arbitration does not exist', async () => {
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.castObjection(proposalID, newRemark, { from: ROOT }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should not cast the objection if the arbitration is not open', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 10000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);

      await setTime(objectionEndTime.plus(rootSlashingOBJP + 10).toNumber());

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.castObjection(proposalID, newRemark, { from: ROOT }),
        '[QEC-027001]-The given arbitration is not open, objection failed.'
      );
    });
  });

  describe('setRemark()', () => {
    const remark = 'http://remark.com';
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: ROOT2 });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should successfully set the remark', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await rootNodeSlashingEscrow.castObjection(proposalID, remark, { from: ROOT });
      await rootNodeSlashingEscrow.setRemark(proposalID, newRemark, { from: ROOT });

      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), newRemark);
    });

    it('should emit correct event', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await rootNodeSlashingEscrow.castObjection(proposalID, remark, { from: ROOT });
      const result = await rootNodeSlashingEscrow.setRemark(proposalID, newRemark, { from: ROOT });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'RemarkUpdated');
      assert.equal(result.logs[0].args._id, proposalID);
      assert.equal(result.logs[0].args._remark, newRemark);
    });

    it('should not set the remark if the caller is not a slashing victim', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await rootNodeSlashingEscrow.castObjection(proposalID, remark, { from: ROOT });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.setRemark(proposalID, newRemark, { from: NOT_ROOT }),
        '[QEC-027014]-Permission denied - only the slashing target has access.'
      );

      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), remark);
    });

    it('should not set the remark if the arbitration does not exist', async () => {
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.setRemark(proposalID, newRemark, { from: ROOT }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should not set the remark if the arbitration is not pending', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.setRemark(proposalID, newRemark, { from: ROOT }),
        '[QEC-027018]-The given arbitration is not pending.'
      );

      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), '');
    });
  });

  describe('setProposerRemark()', () => {
    const remark = 'http://remark.com';
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: ROOT2 });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should successfully confirm the appeal', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await rootNodeSlashingEscrow.castObjection(proposalID, remark, { from: ROOT });
      const txReceipt = await rootNodeSlashingEscrow.setProposerRemark(proposalID, newRemark, true, { from: ROOT2 });

      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).proposerRemark, newRemark);
      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).remark, remark);
      assert.equal((await rootNodeSlashingEscrow.arbitrationInfos(proposalID)).appealConfirmed, true);

      assert.equal(txReceipt.receipt.logs[0].event, 'ProposerRemarkUpdated');
      assert.equal(txReceipt.receipt.logs[0].args._id, proposalID);
      assert.equal(txReceipt.receipt.logs[0].args._proposerRemark, newRemark);
      assert.equal(txReceipt.receipt.logs[0].args._appealConfirmed, true);
    });

    it('should not set the remark if the arbitration is not pending', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.setProposerRemark(proposalID, newRemark, false, { from: ROOT2 }),
        '[QEC-027018]-The given arbitration is not pending.'
      );
    });

    it('should get exception if caller does not have access', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await rootNodeSlashingEscrow.castObjection(proposalID, remark, { from: ROOT });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.setProposerRemark(proposalID, newRemark, false, { from: ROOT }),
        '[QEC-027020]-Permission denied - only the original proposer has access.'
      );
    });
  });

  describe('proposeDecision()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal('remark', ROOT, getPercentageFormat(60), {
        from: ROOT2,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
    });

    it('should be possible to propose the decision', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      const reference = 'https://decision.com';
      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      await rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, reference, { from: ROOT2 });
      const decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT2.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), reference);
    });

    it('should be possible to propose a new decision after timeout', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      const reference = 'https://decision.com';
      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      const confirmationCount = 1;
      await rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, reference, { from: ROOT2 });
      let decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(
        decision.confirmationCount.toString(),
        confirmationCount.toString(),
        'Confirmation count must be 1 after first decision proposal'
      );

      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);
      await setTime(toBN(arbitrationInfo.decision.endDate).plus(1).toNumber());

      await rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, reference, { from: ROOT3 });
      decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(
        decision.confirmationCount.toString(),
        confirmationCount.toString(),
        'Confirmation count must be 1 after second decision proposal'
      );
    });

    it('should not be possible to propose the decision if the arbitration is not pending', async () => {
      const percentage = getPercentageFormat(75);

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, false, '', { from: ROOT2 }),
        '[QEC-027018]-The given arbitration is not pending.'
      );
    });

    it('should not be possible to propose the decision if current decision period is not over', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      const notAppealed = false;
      const percentage = getPercentageFormat(80);
      await rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, false, '', { from: ROOT2 });

      let decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT2.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '');

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, false, '', { from: ROOT2 }),
        '[QEC-027002]-The current decision period is not over.'
      );

      decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT2.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '');
    });

    it('should not be possible to propose the decision if caller has already proposed a decision', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      await rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, '', { from: ROOT2 });
      let decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT2.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '');

      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);
      await setTime(toBN(arbitrationInfo.decision.endDate).plus(1).toNumber());

      const newPercentage = getPercentageFormat(20);
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, newPercentage, false, '', { from: ROOT2 }),
        '[QEC-027003]-The caller has already proposed the previous decision.'
      );

      decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT2.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '');
    });

    it('should not be possible to propose the decision if percentage value is invalid', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      const percentage = getPercentageFormat(10000);

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, false, '', { from: ROOT2 }),
        '[QEC-027004]-Invalid percentage value, failed to propose a decision.'
      );
    });

    it('should not be possible to propose the decision if appeal period is not over and _notAppealed == true', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      const percentage = getPercentageFormat(40);

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, true, '', { from: ROOT2 }),
        '[QEC-027005]-The appeal period is not over.'
      );
    });

    it('should not be possible to propose the decision if _notAppealed == true and percentage value is not equals 100%', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);

      await setTime(toBN(arbitrationInfo.params.appealEndTime).plus(1).toNumber());

      const percentage = getPercentageFormat(50);

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, percentage, true, '', { from: ROOT2 }),
        '[QEC-027006]-If the no appeal was submitted, the slashing percentage should be 100%.'
      );
    });

    it('should not be possible to propose the decision if caller is not a root node', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, 'https://decision.com', {
          from: NOT_ROOT,
        }),
        '[QEC-027015]-Permission denied - only the root node has access.'
      );
    });
  });

  describe('confirmDecision()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal('remark', ROOT, getPercentageFormat(60), {
        from: ROOT2,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should be possible to confirm a decision', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const result = await rootNodeSlashingEscrow.confirmDecision(proposalID, info.decision.hash, { from: ROOT3 });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ArbitrationDecided');
      assert.equal(result.logs[0].args._id, proposalID);

      const decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 2);
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.DECIDED);
    });

    it('should not be possible to confirm a decision if caller is not a root node', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(proposalID, info.decision.hash),
        '[QEC-027015]-Permission denied - only the root node has access.'
      );

      const decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 1);
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should not be possible to confirm a decision if no decision period is open', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(proposalID, '0x123', { from: ROOT2 }),
        '[QEC-027019]-The arbitration is not open for decision.'
      );
    });

    it('should not be possible to confirm a decision if decision has already decided', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;

      await rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 }),
        '[QEC-027007]-This arbitration is already decided.'
      );
    });

    it('should not be possible to confirm a decision if caller has already confirmed', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(proposalID, '0x123', { from: ROOT2 }),
        '[QEC-027008]-The caller has already confirmed.'
      );

      const decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 1);
    });

    it('should not be possible to confirm a decision if arbitration does not exist', async () => {
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(100, '0x123', { from: ROOT2 }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should failed by incorrect hash', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      const hash = '0x123';

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 }),
        '[QEC-027021]-'
      );
    });
  });

  describe('execute()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal('remark', ROOT, getPercentageFormat(60), {
        from: ROOT2,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
    });

    it('should be possible to execute slashing', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });
      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 });

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await rootNodeSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN((await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount);

      let expectedRestAmount = amountToSlash.multipliedBy(0.1);
      const expectedAmountToReturn = amountToSlash.minus(expectedRestAmount);
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.plus(expectedAmountToReturn).toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('case with the 100%', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(100), false, '1', { from: ROOT2 });
      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 });

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await rootNodeSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN((await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount);

      let expectedRestAmount = amountToSlash;
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('case with 0%', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(0), false, '1', { from: ROOT2 });

      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 });

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await rootNodeSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN((await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.plus(amountToSlash).toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should be possible to execute slashing if the arbitration status is ACCEPTED', async () => {
      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const arbitrationInfo = await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID);

      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);
      await setTime(objectionEndTime.plus(rootSlashingVP + 1).toNumber());

      await rootNodeSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN((await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount);

      let expectedRestAmount = amountToSlash;
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should not be possible to execute slashing if there is no decided decisions', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '', { from: ROOT2 });

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await truffleAssert.reverts(
        rootNodeSlashingEscrow.execute(proposalID),
        '[QEC-027009]-The arbitration is still undecided. decision.'
      );

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT2));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      assert.equal(escrowBalance.toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should not be possible to execute slashing if arbitration does not exist', async () => {
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '', { from: ROOT2 });

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await truffleAssert.reverts(rootNodeSlashingEscrow.execute(100), '[QEC-027017]-The arbitration does not exist.');

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal(escrowBalance.toString(), escrowBalanceAfterSlashing.toString());
    });
  });

  describe('getDecisionStats()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal('remark', ROOT, getPercentageFormat(60), {
        from: ROOT2,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.rootNodes', ROOTS_CONTRACT, { from: OWNER });
    });

    it('should be possible to get the decision stats', async () => {
      await rootNodeSlashingEscrow.open(proposalID, { from: ROOTS_CONTRACT, value: 1000 });
      await registry.setAddress('governance.rootNodes', rootsAddress, { from: OWNER });
      await rootNodeSlashingEscrow.castObjection(proposalID, 'remark', { from: ROOT });
      await rootNodeSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT2 });

      const info = await rootNodeSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      const result = await rootNodeSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ArbitrationDecided');
      assert.equal(result.logs[0].args._id, proposalID);

      const decision = (await rootNodeSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 2);
      assert.equal((await rootNodeSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.DECIDED);

      const stats = await rootNodeSlashingEscrow.getDecisionStats(proposalID);

      const expectedPercentage = 666666666666666666666666666;
      assert.equal(stats.confirmationCount, 2);
      assert.equal(stats.requiredConfirmations, 2);
      assert.equal(stats.currentConfirmationPercentage, expectedPercentage);
    });

    it('should not be possible to get the decision stats if arbitration does not exist', async () => {
      await truffleAssert.reverts(
        rootNodeSlashingEscrow.getDecisionStats(100),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });
  });
});
