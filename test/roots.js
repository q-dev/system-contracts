const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const Roots = artifacts.require('Roots');
const Parameters = artifacts.require('Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('Roots', () => {
  const reverter = new Reverter();

  let constitution;
  let roots;
  let registry;

  let OWNER;
  let ROOTS_VOTING;
  let INITIAL_ROOT;
  let ROOT1;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    ROOTS_VOTING = await accounts(1);
    INITIAL_ROOT = await accounts(2);
    ROOT1 = await accounts(3);
    PARAMETERS_VOTING = await accounts(4);

    registry = await ContractRegistry.new();

    await registry.initialize(
      [OWNER],
      ['governance.rootNodes.membershipVoting', 'governance.constitution.parametersVoting'],
      [ROOTS_VOTING, PARAMETERS_VOTING]
    );
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    constitution = await makeProxy(registry.address, Parameters);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.maxNRootNodes', 10, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [INITIAL_ROOT], { from: OWNER });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('constructor', () => {
    it('should contain initial root node list', async () => {
      const members = await roots.getMembers();
      assert.deepEqual([INITIAL_ROOT], members, 'unexpected initial root node members');
    });
  });

  describe('addMember', () => {
    it('should add new member', async () => {
      await roots.addMember(ROOT1, { from: ROOTS_VOTING });
      await assert.isTrue(await roots.isMember(ROOT1), 'expected to contain new root node');
    });

    it('should be able to add new member only from rootsVoting', async () => {
      const CALLER = await accounts(9);
      await truffleAssert.reverts(roots.addMember(ROOT1, { from: CALLER }));
    });
  });

  describe('swapMember', () => {
    it('should change root', async () => {
      await roots.swapMember(INITIAL_ROOT, ROOT1, { from: ROOTS_VOTING });
      assert.isTrue(await roots.isMember(ROOT1));
      assert.isFalse(await roots.isMember(INITIAL_ROOT));
    });

    it('should be able to change root only from rootsVoting', async () => {
      await truffleAssert.reverts(roots.swapMember(INITIAL_ROOT, ROOT1));
    });
  });

  describe('commitStake', () => {
    it('should fail without value', async () => {
      await truffleAssert.reverts(roots.commitStake({ from: await accounts(5) }), '[QEC-002012]');
    });
  });
});
