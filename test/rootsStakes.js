const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const assertEqualBN = require('./helpers/assertEqualBN');
const BigNumber = require('bignumber.js');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const truffleAssert = require('truffle-assertions');

const { getCurrentBlockTime, setTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const Roots = artifacts.require('./Roots.sol');
const RootNodesSlashingVoting = artifacts.require('./RootNodesSlashingVoting');
const Parameters = artifacts.require('./Constitution.sol');
const ContractRegistry = artifacts.require('./ContractRegistry');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting.sol');
const RootNodeSlashingEscrow = artifacts.require('./RootNodeSlashingEscrow');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');
const WithdrawAddresses = artifacts.require('WithdrawAddresses');

describe('Roots.sol', () => {
  const reverter = new Reverter();

  const proposalExecutionP = 2592000;

  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const rootWithdrawP = 3000;
  const Q = require('./helpers/defiHelper').Q;

  const hour = toBN(3600);
  const day = toBN(24 * hour);
  const week = toBN(7 * day);
  const month = toBN(30 * day);

  let rootNodes;
  let rootNodesSlashingVoting;
  let generalUpdateVote;
  let votingWeightProxy;
  let rootNodeSlashingEscrow;
  let registry;
  let parameters;
  let withdrawAddresses;
  let remark;
  let percentage;

  const checkInvariant = async (currentLockInfo, userAddress) => {
    const totalLockedAmount = toBN(currentLockInfo.lockedAmount).plus(currentLockInfo.pendingUnlockAmount);
    assert.isTrue(toBN(await rootNodes.getRootNodeStake(userAddress)).gte(totalLockedAmount));
  };

  let OWNER;
  let PARAMETERS_VOTING;
  let ROOT;
  let ROOT2;
  let CANDIDATE;

  before(async () => {
    OWNER = await accounts(0);
    PARAMETERS_VOTING = await accounts(1);
    ROOT = await accounts(3);
    ROOT2 = await accounts(4);
    CANDIDATE = await accounts(7);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    parameters = await makeProxy(registry.address, Parameters);
    await parameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await parameters.setUint('constitution.rootWithdrawP', rootWithdrawP, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.maxNRootNodes', 10, { from: PARAMETERS_VOTING });

    await parameters.setUint('constitution.voting.rootSlashingVP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.rootSlashingRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.rootSlashingQRM', 0, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await parameters.setUint('constitution.voting.rootSlashingRNVALP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.rootSlashingSQRM', getPercentageFormat(1), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.rootSlashingSMAJ', getPercentageFormat(80), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });

    await parameters.setUint('constitution.voting.rootSlashingOBJP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.rootSlashingAppealP', 86400, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.constitution.parameters', parameters.address);
    rootNodes = await makeProxy(registry.address, Roots);
    await rootNodes.initialize(registry.address, [ROOT, ROOT2, CANDIDATE]);
    await registry.setAddress('governance.rootNodes', rootNodes.address);

    rootNodesSlashingVoting = await makeProxy(registry.address, RootNodesSlashingVoting);
    await rootNodesSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.rootNodes.slashingVoting', rootNodesSlashingVoting.address);

    rootNodeSlashingEscrow = await makeProxy(registry.address, RootNodeSlashingEscrow);
    await rootNodeSlashingEscrow.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.rootNodes.slashingEscrow', rootNodeSlashingEscrow.address, { from: OWNER });

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(registry.address, ['governance.rootNodes'], ['governance.generalUpdateVoting']);

    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    remark = 'https://ethereum.org';
    percentage = getPercentageFormat(60); // it's 60% from 10 ** 27
    withdrawAddresses = await WithdrawAddresses.new();
    await withdrawAddresses.initialize();
    await registry.setAddress('tokeneconomics.withdrawAddresses', withdrawAddresses.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('depositOnBehalfOf() + _onTimeLockedDeposit()', () => {
    const commitStakeAmount = Q.times(10);
    const depositStakeAmount = Q.times(10);
    let withdrawalAmount = Q.times(5);

    const maxTimelocksNumber = toBN(10);
    const startReleaseDate = day;
    const endReleaseDate = startReleaseDate.plus(day);

    beforeEach('setup', async () => {
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      const currentTime = await getCurrentBlockTime();

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, currentTime);
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), commitStakeAmount.toString());
      assertEqualBN(commitStakeAmount, firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, 0);

      await rootNodes.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
        from: ROOT,
        value: commitStakeAmount,
      });

      const lockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), lockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount);
    });

    it('should deposit several times', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(firstCurrentTime);

      await rootNodes.depositOnBehalfOf(CANDIDATE, week, month.plus(week), { from: ROOT, value: depositStakeAmount });

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(2)), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(2)), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount.times(2));

      const secondCurrentTime = (await getCurrentBlockTime()) + 10;
      await setNextBlockTime(secondCurrentTime);

      await rootNodes.depositOnBehalfOf(CANDIDATE, month.plus(week), month.times(2).plus(week), {
        from: ROOT,
        value: depositStakeAmount,
      });

      const thirdLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(3)), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(3)), thirdLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount.times(3));
    });

    it('QDEV-2944 LockInfo Issue. should deposit on behalf of other user', async () => {
      const setupDepositAmount = Q.times(10);
      const depositAmount = setupDepositAmount;
      const releaseStart = toBN(0);
      const releaseEnd = toBN(5);

      await rootNodes.depositOnBehalfOf(ROOT, releaseStart, releaseEnd, { from: ROOT, value: setupDepositAmount });
      assert.equal(setupDepositAmount.toString(), (await rootNodes.getRootNodeStake(ROOT)).toString());
      assert.equal(toBN(0).toString(), (await rootNodes.getRootNodeStake(ROOT2)).toString());

      await rootNodes.depositOnBehalfOf(ROOT2, releaseStart, releaseEnd, { from: ROOT, value: depositAmount });
      assert.equal(setupDepositAmount.toString(), (await rootNodes.getRootNodeStake(ROOT)).toString());
      assert.equal(depositAmount.toString(), (await rootNodes.getRootNodeStake(ROOT2)).toString());
    });

    it('should automatically release part of timeLocked amount at specific time', async () => {
      const period = endReleaseDate.minus(startReleaseDate);
      const secondCurrentTime = startReleaseDate.plus(period.div(2)).toNumber();
      await setTime((await getCurrentBlockTime()) + secondCurrentTime);

      const lockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);

      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), lockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount.div(2).toFixed(0, BigNumber.ROUND_DOWN));
    });

    it('should withdraw amount after timelock is over', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setTime(firstCurrentTime);

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);

      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const secondCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
      await setTime(secondCurrentTime);
      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount),
        await rootNodes.getRootNodeStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount), secondLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, 0);
    });

    it('should withdraw available amount before timelock is over', async () => {
      const firstCurrentTime = await getCurrentBlockTime();
      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);

      withdrawalAmount = Q.times(10);

      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber();
      await setNextBlockTime(secondCurrentTime);
      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const totalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(depositStakeAmount, await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(depositStakeAmount, totalLockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount);
    });

    it(
      'should get exception, try to withdraw unavailable ' + '(timelock constraint) amount before timelock is over',
      async () => {
        const firstCurrentTime = await getCurrentBlockTime();
        const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
        const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
        assertEqualBN(firstMinimumBalance, depositStakeAmount);

        withdrawalAmount = Q.times(15);

        await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

        const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber() - 100;
        await setTime(secondCurrentTime);
        const reason = '[QEC-002013]-Withdrawal prevented by timelock(s).';
        await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

        const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
        const totalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
        const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, firstCurrentTime);
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), totalLockedAmount);
        assertEqualBN(secondMinimumBalance, depositStakeAmount);
      }
    );

    it('should withdraw amount after timelock is over during pendingSlashing', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber();
      await setTime(firstCurrentTime);

      withdrawalAmount = Q.times(5);
      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(75), { from: ROOT });

      const secondCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
      await setTime(secondCurrentTime);

      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount),
        await rootNodes.getRootNodeStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount), secondLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, 0);
    });

    it('should get exception, try to withdraw unavailable (pendingSlashing constraint) amount after timelock is over during pendingSlashing', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
      await setTime(firstCurrentTime);

      withdrawalAmount = Q.times(6);

      await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(75), { from: ROOT });
      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });
      const withdrawal = await rootNodes.withdrawals.call(CANDIDATE);

      const secondCurrentTime = Number(withdrawal.endTime) + 1;
      await setTime(secondCurrentTime);

      const reason = '[QEC-002005]-The withdrawal is blocked by pending slashing proposals.';
      await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const secondTotalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), secondTotalLockedAmount);
      assertEqualBN(secondMinimumBalance, 0);
    });

    it('should get exception, try to withdraw unavailable (timelock constraint) before after timelock is over during pendingSlashing', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + votingPeriod;
      await setTime(firstCurrentTime);

      withdrawalAmount = Q.times(11);
      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(75), { from: ROOT });

      const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.minus(hour).toNumber();
      await setTime(secondCurrentTime);

      const reason = '[QEC-002013]-Withdrawal prevented by timelock(s).';
      await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const secondTotalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
      const secondMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), secondTotalLockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount);
    });

    it('should get exception, try to deposit less than minimum amount', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      const wrongDepositAmount = Q.times(10).minus(1);

      const reason = '[QEC-032002]-Given amount is below deposit minimum.';
      await truffleAssert.reverts(
        rootNodes.depositOnBehalfOf(CANDIDATE, week, month.plus(week), { from: ROOT, value: wrongDepositAmount }),
        reason
      );

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await rootNodes.getRootNodeStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);
    });

    it('should get exception, try to deposit maxTimelocksNumber + 1 times', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      for (let i = 1; i < maxTimelocksNumber; i++) {
        rootNodes.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
          from: ROOT,
          value: depositStakeAmount,
        });
      }

      const reason = '[QEC-032003]-Failed to deposit amount, too many timelocks.';
      await truffleAssert.reverts(
        rootNodes.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
          from: ROOT,
          value: depositStakeAmount,
        }),
        reason
      );

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount.times(maxTimelocksNumber)),
        await rootNodes.getRootNodeStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(maxTimelocksNumber)), firstLockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimelocksNumber));
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('commitStake() + lock()', () => {
    const commitStakeAmount = Q;

    beforeEach('setup', async () => {
      const result = await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'CommittedStake');
      assert.equal(result.logs[0].args._root, CANDIDATE);
      assert.equal(result.logs[0].args._amount.toString(), commitStakeAmount.toString());

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), commitStakeAmount.toString());
    });

    it('should increase committed and locked amount', async () => {
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      let currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.times(2).toString());
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), commitStakeAmount.times(2).toString());

      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.times(3).toString());
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), commitStakeAmount.times(3).toString());
    });

    it('should get exception, try to commit and lock zero amount', async () => {
      const reason = '[QEC-002012]-Additional stake must not be zero.';
      await truffleAssert.reverts(rootNodes.commitStake({ from: CANDIDATE, value: 0 }), reason);

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('announceWithdrawal() + announceUnlock()', () => {
    const commitStakeAmount = Q;
    const announceWithdrawalAmount = Q.dividedBy(2);

    beforeEach('setup', async () => {
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });
      assert.equal(
        toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(),
        commitStakeAmount.toString()
      );

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
    });

    it('Should Announce Withdrawal and Unlock successfully', async () => {
      const result = await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });
      const withdrawal = await rootNodes.withdrawals.call(CANDIDATE);

      assert.equal(toBN(withdrawal[Object.keys(withdrawal)[1]]).toString(), announceWithdrawalAmount.toString());
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'WithdrawalAnnounced');
      assert.equal(result.logs[0].args._root, CANDIDATE);
      assert.equal(result.logs[0].args._amount.toString(), announceWithdrawalAmount.toString());

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
    });

    it('should increase pending unlock and withdrawal amount, announce bigger amount', async () => {
      const firstAnnounceWithdrawalAmount = announceWithdrawalAmount;
      await rootNodes.announceWithdrawal(firstAnnounceWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(firstAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), firstAnnounceWithdrawalAmount.toString());

      const secondAnnounceWithdrawalAmount = firstAnnounceWithdrawalAmount.plus(1);
      await rootNodes.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      const totalAnnounceUnlockedAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(totalAnnounceUnlockedAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceUnlockedAmount.toString());
    });

    it('should decrease pending unlock and withdrawal amount, announce less amount', async () => {
      const firstAnnounceWithdrawalAmount = announceWithdrawalAmount;
      await rootNodes.announceWithdrawal(firstAnnounceWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(firstAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), firstAnnounceWithdrawalAmount.toString());

      const secondAnnounceWithdrawalAmount = firstAnnounceWithdrawalAmount.minus(1);
      await rootNodes.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      const totalAnnounceUnlockedAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(totalAnnounceUnlockedAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceUnlockedAmount.toString());
    });

    it('should not change pending unlock and withdrawal amount, announce same amount', async () => {
      const firstAnnounceWithdrawalAmount = announceWithdrawalAmount;
      await rootNodes.announceWithdrawal(firstAnnounceWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(firstAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), firstAnnounceWithdrawalAmount.toString());

      const secondAnnounceWithdrawalAmount = firstAnnounceWithdrawalAmount;
      await rootNodes.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      const totalAnnounceUnlockedAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(totalAnnounceUnlockedAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceUnlockedAmount.toString());
    });

    it('should get exception, try to announce unlock and withdraw amount greater than locked amount', async () => {
      const reason = '[QEC-002001]-Announced withdrawal must not exceed current stake.';
      await truffleAssert.reverts(rootNodes.announceWithdrawal(commitStakeAmount.plus(1), { from: CANDIDATE }), reason);

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    it('should successfully announce unlock and withdraw zero amount', async () => {
      await rootNodes.announceWithdrawal(0, { from: CANDIDATE });

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('withdraw() + unlock()', () => {
    const commitStakeAmount = Q.times(100);
    const withdrawalAmount = Q.times(20);
    const announceWithdrawalAmount = Q.times(40);

    beforeEach('setup', async () => {
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)), commitStakeAmount.toString());

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });

      await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
    });

    it('should successfully unlock and withdraw, partial withdrawal', async () => {
      await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());
      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount).toString()
      );

      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)),
        commitStakeAmount.minus(withdrawalAmount).toString()
      );
    });

    it('should successfully unlock and withdraw, full withdrawal', async () => {
      await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());
      await rootNodes.withdraw(announceWithdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(announceWithdrawalAmount).toString()
      );

      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
    });

    it('should successfully partial withdraw and then withdraw remainder', async () => {
      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());

      const firstLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(firstLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(firstLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), commitStakeAmount.toString());

      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(secondLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(secondLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount).toString()
      );
      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount).toString()
      );

      await rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const thirdLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(thirdLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(thirdLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount.times(2)).toString()
      );
      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount.times(2)).toString()
      );
    });

    it('should get exception, attempt to withdraw the amount without announcing the withdrawal', async () => {
      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());
      const reason = '[QEC-002004]-Cannot withdraw more than the announced amount.';
      await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount, ROOT, { from: ROOT }), reason);

      assert.equal(toBN(await rootNodes.getRootNodeStake(ROOT)), 0);
    });

    it('should get exception, try to withdraw amount before the period expires', async () => {
      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const reason = '[QEC-002003]-Not enough time has elapsed since the announcement of the withdrawal.';
      await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)), commitStakeAmount.toString());
    });

    it('should get exception, try to withdraw amount greater than announced', async () => {
      await rootNodes.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());

      const reason = '[QEC-002004]-Cannot withdraw more than the announced amount.';
      await truffleAssert.reverts(rootNodes.withdraw(withdrawalAmount.plus(1), CANDIDATE, { from: CANDIDATE }), reason);

      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)), commitStakeAmount.toString());
    });

    it('should get exception, try to unlock and withdraw zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());
      await truffleAssert.reverts(rootNodes.withdraw(0, CANDIDATE, { from: CANDIDATE }), reason);

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
    });

    it("should withdraw to withdrawAddresses's account", async () => {
      const reciever = '0x1234123412341234123412341234123412341234';
      await withdrawAddresses.change(reciever, { from: CANDIDATE });
      await withdrawAddresses.finalize(CANDIDATE, reciever);
      assert.equal(await withdrawAddresses.resolve(CANDIDATE), reciever);

      await setTime((await getCurrentBlockTime()) + toBN(rootWithdrawP).plus(10).toNumber());
      await rootNodes.withdraw(100, CANDIDATE, { from: CANDIDATE });
      assert.equal(await web3.eth.getBalance(reciever), 100);
      await truffleAssert.reverts(rootNodes.withdraw(100, ROOT2, { from: CANDIDATE }), '[QEC-002014]');
      assert.equal(await web3.eth.getBalance(reciever), 100);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('getLockInfo', () => {
    const commitStakeAmount = toBN(10).pow(18);
    const announceWithdrawalAmount = commitStakeAmount.dividedBy(4);

    beforeEach('setup', async () => {
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });

      await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });
    });

    it('should get correct information', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
    });

    it('should get exception, token lock source is not contained in the list', async () => {
      const votingWeightProxy2 = await makeProxy(registry.address, VotingWeightProxy);
      await votingWeightProxy2.initialize(registry.address, [], []);
      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy2.address);

      const reason = '[QEC-028001]-Unknown token lock source.';
      await truffleAssert.reverts(rootNodes.getLockInfo({ from: CANDIDATE }), reason);

      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('applySlashing', () => {
    const commitStakeAmount = Q.times(100);
    let amountToSlash = Q.times(20);
    const announceWithdrawalAmount = Q.times(40);
    let RNSVTemp;
    const proposalID = 0;

    beforeEach('setup', async () => {
      RNSVTemp = await accounts(4);
      await registry.setAddress('governance.rootNodes.slashingVoting', RNSVTemp);
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(proposalID, { from: CANDIDATE });
      assert.equal(
        toBN((await generalUpdateVote.proposals(proposalID)).counters.weightFor).toString(),
        commitStakeAmount.toString()
      );

      await rootNodes.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
    });

    it('should successfully applied slashing with force unlock', async () => {
      await rootNodes.applySlashing(proposalID, CANDIDATE, amountToSlash, { from: RNSVTemp });

      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(amountToSlash).toString()
      );

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(amountToSlash).toString()
      );
    });

    it('should successfully applied slashing with amount greater than stake', async () => {
      amountToSlash = Q.times(120);

      await rootNodes.applySlashing(proposalID, CANDIDATE, amountToSlash, { from: RNSVTemp });

      assert.equal(toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(), 0);

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    it('should successfully applied slashing with needed unlock amount < pending unlock amount', async () => {
      amountToSlash = Q.times(30);

      await rootNodes.applySlashing(proposalID, CANDIDATE, amountToSlash, { from: RNSVTemp });

      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(amountToSlash).toString()
      );

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(announceWithdrawalAmount).toString()
      );

      const neededUnlockAmount = amountToSlash;
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(neededUnlockAmount).toString()
      );
    });

    it('should successfully applied slashing with needed unlock amount > pending unlock amount', async () => {
      amountToSlash = Q.times(90);

      await rootNodes.applySlashing(proposalID, CANDIDATE, amountToSlash, { from: RNSVTemp });

      assert.equal(
        toBN(await rootNodes.getRootNodeStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(amountToSlash).toString()
      );

      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const neededUnlockAmount = amountToSlash;

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        commitStakeAmount.minus(neededUnlockAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('withdrawals slashing', () => {
    let proposalID;
    let endTime;

    beforeEach('init proposal', async () => {
      const commitStakeAmount = 100000;
      await rootNodes.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await rootNodes.announceWithdrawal(commitStakeAmount, { from: CANDIDATE });
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      proposalID = Number(result.logs[0].args._id);
      const proposal = await rootNodesSlashingVoting.proposals.call(proposalID);
      endTime = Number(proposal.base.params.votingEndTime);
    });

    it('should add id to slashing list', async () => {
      const result = await rootNodes.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(result), '["0"]');
    });

    it('purgeSlashing work correctly', async () => {
      await setTime(endTime + 1);
      await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });

      const beforePurge = await rootNodes.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(beforePurge), '["0","1"]');

      await rootNodes.purgePendingSlashings(CANDIDATE);
      const afterPurge = await rootNodes.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(afterPurge), '["1"]');
    });

    it('purgeSlashing does not do unnecessary purging', async () => {
      await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });
      const beforePurge = await rootNodes.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(beforePurge), '["0","1"]');

      await rootNodes.purgePendingSlashings(CANDIDATE);
      const afterPurge = await rootNodes.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(afterPurge), '["0","1"]');
    });

    it('calculate should ignore slashing after end-time', async () => {
      await setTime(endTime + 1);

      const amountToWithdraw = 50000;
      await rootNodes.announceWithdrawal(amountToWithdraw, { from: CANDIDATE });
      const withdrawal = await rootNodes.withdrawals.call(CANDIDATE);

      await setTime(Number(withdrawal.endTime) + 1);

      const beforeWithdrawal = toBN(await rootNodes.getRootNodeStake(CANDIDATE));
      const result = await rootNodes.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE });
      const afterWithdrawal = toBN(await rootNodes.getRootNodeStake(CANDIDATE));
      const expectedWithdrawAmount = beforeWithdrawal.minus(afterWithdrawal);

      assert.equal(expectedWithdrawAmount.toString(), amountToWithdraw);
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'Withdrawn');
      assert.equal(result.logs[0].args._root, CANDIDATE);
      assert.equal(result.logs[0].args._amount.toString(), amountToWithdraw.toString());
    });

    it('should fail to withdraw when remaining will be not enough for slashing', async () => {
      const amountToWithdraw = 50000;
      await rootNodes.announceWithdrawal(amountToWithdraw, { from: CANDIDATE });
      const withdrawal = await rootNodes.withdrawals.call(CANDIDATE);

      await setTime(Number(withdrawal.endTime) + 1);

      await truffleAssert.reverts(
        rootNodes.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE }),
        '[QEC-002005]-The withdrawal is blocked by pending slashing proposals.'
      );
    });
  });
});
