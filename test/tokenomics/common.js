const { toBN } = require('../helpers/utils');
const { assert } = require('chai');

function getNewExpectedBuckets(payment, numberOfBuckets, expectedBuckets) {
  for (let i = 0; i < numberOfBuckets - 1; i++) {
    expectedBuckets[i] = toBN(expectedBuckets[i])
      .plus(payment.dividedToIntegerBy(toBN(numberOfBuckets)))
      .toString();
  }

  const reminder = payment.minus(payment.dividedToIntegerBy(toBN(numberOfBuckets)).times(numberOfBuckets));

  expectedBuckets[numberOfBuckets - 1] = toBN(expectedBuckets[numberOfBuckets - 1])
    .plus(payment.dividedToIntegerBy(toBN(numberOfBuckets)))
    .plus(reminder);

  expectedBuckets.lenght = numberOfBuckets;

  return expectedBuckets;
}

async function assetToAllocateAndBuckets(
  paymentStretchingBuckets,
  expectedToAllocate,
  expectedBuckets,
  initialBalance,
  expectedToReceive,
  treasuryAccount
) {
  assert.equal(String(await paymentStretchingBuckets.allocatableBalance()), String(expectedToAllocate));
  await assetBuckets(
    paymentStretchingBuckets,
    expectedBuckets.map((bucket) => toBN(bucket).toString())
  );

  const expectedBalance = toBN(initialBalance).plus(expectedToReceive).toString();
  const actualBalance = await web3.eth.getBalance(treasuryAccount);
  assert.equal(actualBalance.toString(), expectedBalance.toString());
}

async function assetBuckets(contract, expected) {
  const actualBuckets = (await contract.getNormalizedBuckets()).map((bucket) => bucket.toString());

  expected = Array.from(expected).filter((value) => String(value) !== '0');

  assert.deepEqual(actualBuckets, expected);
}

module.exports = {
  getNewExpectedBuckets,
  assetToAllocateAndBuckets,
  assetBuckets,
};
