const { assert } = require('chai');
const { artifacts } = require('hardhat');

const { assetToAllocateAndBuckets, assetBuckets } = require('./common');

const { toBN } = require('../helpers/utils');
const { accounts } = require('../helpers/utils.js');

const Reverter = require('../helpers/reverter');
const makeProxy = require('../helpers/makeProxy');
const { setNextBlockTime, getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const DefaultAllocationThrottle = artifacts.require('DefaultAllocationThrottle');

describe('DefaultAllocationThrottle', () => {
  const reverter = new Reverter();

  const BUCKET_LIMIT = 5;
  const TIME_UNIT = 60 * 60; // 60 minutes

  let USER1;
  let PROXY_ADMIN;
  let TREASURY;

  let paymentSBAdvanced;

  before('setup', async () => {
    USER1 = await accounts(0);
    PROXY_ADMIN = await accounts(1);
    TREASURY = await accounts(2);

    paymentSBAdvanced = await makeProxy(PROXY_ADMIN, DefaultAllocationThrottle);

    await paymentSBAdvanced.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, BUCKET_LIMIT);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('access', async () => {
    it('should set a new recipient', async () => {
      await paymentSBAdvanced.setNewRecipient();

      assert.equal(await paymentSBAdvanced.recipient(), '0x2fe3180D143665fFD0513b87CB4D0787c2444eBd');
    });
  });

  describe('deposit', () => {
    const PAYMENT = toBN('100');

    it('should stretch payment correctly', async () => {
      await web3.eth.sendTransaction({
        from: USER1,
        to: paymentSBAdvanced.address,
        value: PAYMENT,
      });

      const expectedBuckets = Array(BUCKET_LIMIT).fill(PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).toString());

      await assetBuckets(paymentSBAdvanced, expectedBuckets);
    });

    it('should stretch payment with first non empty bucket', async () => {
      const maxBucketCount = toBN(await paymentSBAdvanced.MAX_BUCKET_COUNT()).minus(1);

      const payment = PAYMENT.times(maxBucketCount);

      await paymentSBAdvanced.deposit(maxBucketCount, { value: payment });

      let expectedBuckets = Array(toBN(maxBucketCount).toNumber() + 1).fill('100');
      expectedBuckets[0] = '0';

      await assetBuckets(paymentSBAdvanced, expectedBuckets);

      await paymentSBAdvanced.deposit(maxBucketCount, { value: payment });
      expectedBuckets = expectedBuckets.map((bucket, index) => {
        if (index === 0) {
          return '0';
        }

        return toBN(bucket).plus(100).toString();
      });

      await assetBuckets(paymentSBAdvanced, expectedBuckets);
    });

    it('should stretch payment correctly after some time', async () => {
      await web3.eth.sendTransaction({
        from: USER1,
        to: paymentSBAdvanced.address,
        value: PAYMENT,
      });

      const oneTimePayment = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT));
      let expectedBuckets = Array(BUCKET_LIMIT).fill(oneTimePayment.toString());

      const timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      const numberOfPassedAllocations = 3;
      const updateTime =
        (await getCurrentBlockTime()) + timeUpToNextBucket + numberOfPassedAllocations * TIME_UNIT + TIME_UNIT / 2;
      await setNextBlockTime(updateTime);

      await web3.eth.sendTransaction({
        from: USER1,
        to: paymentSBAdvanced.address,
        value: PAYMENT,
      });

      expectedBuckets = expectedBuckets.map((bucket, index) => {
        if (index === 0) {
          return toBN(bucket).div(2).toString();
        }

        if (index < BUCKET_LIMIT - numberOfPassedAllocations) {
          return toBN(bucket).plus(oneTimePayment).toString();
        }

        return oneTimePayment.toString();
      });

      expectedBuckets.push(oneTimePayment.toString());

      await assetBuckets(paymentSBAdvanced, expectedBuckets);
    });
  });

  describe('allocate', () => {
    const PAYMENT = toBN('100000000000');

    let initialBalance;

    beforeEach('setup', async () => {
      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
    });

    it('should allocate + add portion of current bucket', async () => {
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      const timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      const updateTime = (await getCurrentBlockTime()) + timeUpToNextBucket + 3 * TIME_UNIT + TIME_UNIT / 2;
      await setNextBlockTime(updateTime);

      await paymentSBAdvanced.allocate();

      const oneTimePayment = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT));

      const expectedBalance = initialBalance.plus(oneTimePayment.times(3).plus(oneTimePayment.div(2)));
      assert.equal(String(await web3.eth.getBalance(TREASURY)), expectedBalance.toString());
    });

    it('should allocate two times correctly', async () => {
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      let timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      let updateTime = (await getCurrentBlockTime()) + timeUpToNextBucket + TIME_UNIT / 2;
      await setNextBlockTime(updateTime);

      await paymentSBAdvanced.allocate();

      updateTime += TIME_UNIT / 4;
      await setNextBlockTime(updateTime);

      await paymentSBAdvanced.allocate();

      const oneTimePayment = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT));

      let expectedBalance = initialBalance.plus(oneTimePayment.times(3).div(4));
      assert.equal(String(await web3.eth.getBalance(TREASURY)), expectedBalance.toString());
    });
  });

  describe('getters', () => {
    it('should get release amount with portions', async () => {
      const PAYMENT = toBN('120');
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      let timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      let updateTime = (await getCurrentBlockTime()) + TIME_UNIT + TIME_UNIT / 4 + timeUpToNextBucket;
      await setTime(updateTime);

      let releasedAmount = await paymentSBAdvanced.getReleasedAmount();
      const oneTimePayment = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT));
      assert.equal(toBN(releasedAmount).toString(), oneTimePayment.plus(oneTimePayment.div(4)).toString());

      updateTime = (await getCurrentBlockTime()) + 300 * TIME_UNIT;
      await setTime(updateTime);

      releasedAmount = await paymentSBAdvanced.getReleasedAmount();
      assert.equal(toBN(releasedAmount).toString(), PAYMENT.toString());
    });

    it('should return correctly released amount', async () => {
      const PAYMENT = toBN('120');
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '0');
      await assetBuckets(paymentSBAdvanced, ['72', '72', '72', '72', '72']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '0');
      await assetBuckets(paymentSBAdvanced, ['120', '120', '120', '120', '120']);

      let timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      await setTime((await getCurrentBlockTime()) + timeUpToNextBucket);

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '0');
      await assetBuckets(paymentSBAdvanced, ['120', '120', '120', '120', '120']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });
      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '0');
      await assetBuckets(paymentSBAdvanced, ['120', '168', '168', '168', '168', '48']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '0');
      await assetBuckets(paymentSBAdvanced, ['120', '192', '192', '192', '192', '72']);

      timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      await setTime((await getCurrentBlockTime()) + timeUpToNextBucket);

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '120');
      await assetBuckets(paymentSBAdvanced, ['120', '192', '192', '192', '192', '72']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '120');
      await assetBuckets(paymentSBAdvanced, ['192', '216', '216', '216', '96', '24']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '120');
      await assetBuckets(paymentSBAdvanced, ['192', '240', '240', '240', '120', '48']);

      timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      await setTime((await getCurrentBlockTime()) + timeUpToNextBucket);

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '312');
      await assetBuckets(paymentSBAdvanced, ['192', '240', '240', '240', '120', '48']);

      await paymentSBAdvanced.deposit(BUCKET_LIMIT, { value: PAYMENT });

      await assert.equal((await paymentSBAdvanced.getReleasedAmount()).toString(), '312');
      await assetBuckets(paymentSBAdvanced, ['240', '264', '264', '144', '72', '24']);
    });
  });

  describe('integration', () => {
    it('should handle happy flow', async () => {
      const fourthDefaultAllocationThrottle = await makeProxy(PROXY_ADMIN, DefaultAllocationThrottle);

      const NEW_BUCKET_COUNT = 10;

      let initialBalance;

      await fourthDefaultAllocationThrottle.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, NEW_BUCKET_COUNT);

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthDefaultAllocationThrottle.deposit(NEW_BUCKET_COUNT, { value: 200 });

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      await assetToAllocateAndBuckets(
        fourthDefaultAllocationThrottle,
        '0',
        [0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
        initialBalance,
        0,
        TREASURY
      );

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      let timeUpToNextBucket = TIME_UNIT - ((await getCurrentBlockTime()) % TIME_UNIT);
      let updateTime = (await getCurrentBlockTime()) + timeUpToNextBucket;
      await setTime(updateTime);

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthDefaultAllocationThrottle.deposit(NEW_BUCKET_COUNT, { value: 100 });

      await assetToAllocateAndBuckets(
        fourthDefaultAllocationThrottle,
        '0',
        [20, 30, 30, 30, 30, 30, 30, 30, 30, 30, 10],
        initialBalance,
        0,
        TREASURY
      );

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT * 0.44;
      await setTime(updateTime);

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '8');

      await fourthDefaultAllocationThrottle.allocate();

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      await assetToAllocateAndBuckets(
        fourthDefaultAllocationThrottle,
        '0',
        [12, 30, 30, 30, 30, 30, 30, 30, 30, 30, 10],
        initialBalance,
        8,
        TREASURY
      );

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT * 2.5;
      await setTime(updateTime);

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '70');

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthDefaultAllocationThrottle.allocate();

      assert.equal(await fourthDefaultAllocationThrottle.getReleasedAmount(), '0');

      await assetToAllocateAndBuckets(
        fourthDefaultAllocationThrottle,
        '0',
        [2, 30, 30, 30, 30, 30, 30, 30, 10],
        initialBalance,
        // 12 + 30 + 28
        70,
        TREASURY
      );
    });

    it('should work as expected in the edge cases', async () => {
      const advanced = await makeProxy(PROXY_ADMIN, DefaultAllocationThrottle);
      await advanced.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, 10);

      assert.equal(await advanced.getReleasedAmount(), '0');

      let initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await advanced.send(200);

      assert.equal(await advanced.getReleasedAmount(), '0');

      await assetToAllocateAndBuckets(
        advanced,
        '0',
        [0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
        initialBalance,
        0,
        TREASURY
      );

      await setTime((await getCurrentBlockTime()) + TIME_UNIT * 44);

      assert.equal(await advanced.getReleasedAmount(), '200');

      await advanced.send(200);

      await assetToAllocateAndBuckets(
        advanced,
        '200',
        [0, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
        initialBalance,
        0,
        TREASURY
      );
    });
  });
});
