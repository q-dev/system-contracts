const truffleAssert = require('truffle-assertions');

const { assert } = require('chai');
const { artifacts } = require('hardhat');

const { assetToAllocateAndBuckets, assetBuckets, getNewExpectedBuckets } = require('./common');

const { toBN } = require('../helpers/utils');
const { accounts } = require('../helpers/utils.js');

const Reverter = require('../helpers/reverter');
const makeProxy = require('../helpers/makeProxy');
const { setNextBlockTime, getCurrentBlockTime, setTime } = require('../helpers/hardhatTimeTraveller');

const ERC677Mock = artifacts.require('ERC677Mock');
const PaymentStretchingBuckets = artifacts.require('PaymentStretchingBuckets');

describe('PaymentStretchingBuckets', () => {
  const reverter = new Reverter();

  const BUCKET_LIMIT = 5;
  const TIME_UNIT = 60 * 60; // 60 minutes

  let USER1;
  let PROXY_ADMIN;
  let TREASURY;

  let paymentStretchingBuckets;

  before('setup', async () => {
    USER1 = await accounts(0);
    PROXY_ADMIN = await accounts(1);
    TREASURY = await accounts(2);

    paymentStretchingBuckets = await makeProxy(PROXY_ADMIN, PaymentStretchingBuckets);

    await paymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, BUCKET_LIMIT);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('access', () => {
    let secondPaymentStretchingBuckets;

    beforeEach('setup', async () => {
      secondPaymentStretchingBuckets = await makeProxy(PROXY_ADMIN, PaymentStretchingBuckets);
    });

    it('should initialize only once', async () => {
      await truffleAssert.reverts(
        paymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, 15),
        'Initializable: contract is already initialized'
      );
    });

    it('should initialize default bucket count', async () => {
      await truffleAssert.passes(
        secondPaymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, 22),
        'passes'
      );

      assert.equal(toBN(await secondPaymentStretchingBuckets.defaultBucketCount()).toString(), '22');
    });

    it('should revert if bucketCount is too high', async () => {
      await truffleAssert.reverts(
        secondPaymentStretchingBuckets.PaymentStretchingBuckets__init(
          TREASURY,
          TIME_UNIT,
          toBN(await paymentStretchingBuckets.MAX_BUCKET_COUNT()).plus(1)
        ),
        '[QEC-044000]-Default bucket count is too high.'
      );
    });

    it('should revert if allocationPeriod is zero', async () => {
      await truffleAssert.reverts(
        secondPaymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, 0, 15),
        '[QEC-044001]-Allocation period cannot be zero.'
      );
    });

    it('should revert if recipient is zero', async () => {
      await truffleAssert.reverts(
        secondPaymentStretchingBuckets.PaymentStretchingBuckets__init(
          '0x0000000000000000000000000000000000000000',
          TIME_UNIT,
          15
        ),
        '[QEC-044002]-Recipient cannot be zero.'
      );
    });

    it('should not initialize implementation', async () => {
      const thirdPaymentStretchingBuckets = await PaymentStretchingBuckets.new();

      await truffleAssert.reverts(
        thirdPaymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, BUCKET_LIMIT),
        'Initializable: contract is already initialized'
      );
    });
  });

  describe('deposit', () => {
    it('should stretch payment', async () => {
      const payment = toBN('100');

      await web3.eth.sendTransaction({
        from: USER1,
        to: paymentStretchingBuckets.address,
        value: payment,
      });

      const expectedBuckets = Array(BUCKET_LIMIT).fill(payment.dividedToIntegerBy(toBN(BUCKET_LIMIT)).toString());

      await assetBuckets(paymentStretchingBuckets, expectedBuckets);
    });

    it('should stretch payment with max buckets count after long time', async () => {
      const maxBucketCount = toBN(await paymentStretchingBuckets.MAX_BUCKET_COUNT()).minus(1);

      const payment = toBN('100').times(maxBucketCount);

      await paymentStretchingBuckets.deposit(maxBucketCount, { value: payment });

      let expectedBuckets = Array(toBN(maxBucketCount).toNumber()).fill('100');
      await assetBuckets(paymentStretchingBuckets, expectedBuckets);

      const updateTime = (await getCurrentBlockTime()) + TIME_UNIT + 1;
      await setNextBlockTime(updateTime);

      await paymentStretchingBuckets.deposit(maxBucketCount, { value: payment });

      expectedBuckets = expectedBuckets.map((bucket, index) => {
        return toBN(bucket).plus(100).toString();
      });

      expectedBuckets[maxBucketCount - 1] = '100';

      await assetBuckets(paymentStretchingBuckets, expectedBuckets);
      assert.equal(String(await paymentStretchingBuckets.allocatableBalance()), '100');
    });

    it('should stretch payment after allocation period', async () => {
      const payment = toBN('100');

      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: payment });

      let expectedBuckets = Array(BUCKET_LIMIT).fill(payment.dividedToIntegerBy(toBN(BUCKET_LIMIT)).toString());

      const updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setNextBlockTime(updateTime);

      // As the allocation period has passed, the first bucket should be empty
      expectedBuckets[BUCKET_LIMIT - 1] = toBN(0);
      expectedBuckets[BUCKET_LIMIT - 2] = toBN(0);
      expectedBuckets[BUCKET_LIMIT - 3] = toBN(0);

      const numberOfBuckets = Math.ceil(BUCKET_LIMIT / 2);

      await paymentStretchingBuckets.deposit(numberOfBuckets, { value: payment.div(2) });
      expectedBuckets = getNewExpectedBuckets(payment.div(2), numberOfBuckets, expectedBuckets);

      assert.equal(
        String(await paymentStretchingBuckets.allocatableBalance()),
        payment.dividedToIntegerBy(toBN(BUCKET_LIMIT)).times(3).toString()
      );

      await paymentStretchingBuckets.deposit(numberOfBuckets, { value: payment.div(2) });
      expectedBuckets = getNewExpectedBuckets(payment.div(2), numberOfBuckets, expectedBuckets);

      await assetToAllocateAndBuckets(
        paymentStretchingBuckets,
        payment.dividedToIntegerBy(toBN(BUCKET_LIMIT)).times(3),
        expectedBuckets,
        await web3.eth.getBalance(TREASURY),
        0,
        TREASURY
      );
    });

    it('should revert if amount is zero', async () => {
      await truffleAssert.reverts(
        paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: 0 }),
        '[QEC-044003]-Amount cannot be zero.'
      );
    });

    it('should revert if number of buckets is zero', async () => {
      await truffleAssert.reverts(
        paymentStretchingBuckets.deposit(0, { value: 0 }),
        '[QEC-044005]-Number of buckets cannot be zero.'
      );
    });

    it('should revert if number of buckets bigger than limit.', async () => {
      await truffleAssert.reverts(
        paymentStretchingBuckets.deposit(toBN(await paymentStretchingBuckets.MAX_BUCKET_COUNT()).plus(1), { value: 0 }),
        '[QEC-044006]-Number of buckets cannot be greater than bucket limit.'
      );
    });
  });

  describe('allocate', () => {
    const PAYMENT = toBN('100000000000');

    let initialBalance;

    beforeEach('setup', async () => {
      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
    });

    it('should allocate', async () => {
      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: PAYMENT });

      const updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setNextBlockTime(updateTime);

      await paymentStretchingBuckets.allocate();

      const expectedBalance = initialBalance.plus(PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).times(3));
      assert.equal(String(await web3.eth.getBalance(TREASURY)), expectedBalance.toString());
    });

    it('should allocate specific amount', async () => {
      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: PAYMENT });

      const updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setNextBlockTime(updateTime);

      const amountToAllocate = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).times(2);
      await paymentStretchingBuckets.allocate(amountToAllocate);

      const expectedBalance = initialBalance.plus(amountToAllocate);
      assert.equal(String(await web3.eth.getBalance(TREASURY)), expectedBalance.toString());
    });

    it('should allocate all', async () => {
      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: PAYMENT });

      const updateTime = (await getCurrentBlockTime()) + 255 * TIME_UNIT * 2 + 1;
      await setTime(updateTime);

      await paymentStretchingBuckets.allocate();

      const expectedBalance = initialBalance.plus(PAYMENT);
      assert.equal(String(await web3.eth.getBalance(TREASURY)), expectedBalance.toString());
    });

    it('should revert if allocation failed', async () => {
      const anotherPaymentStretchingBuckets = await makeProxy(PROXY_ADMIN, PaymentStretchingBuckets);

      await anotherPaymentStretchingBuckets.PaymentStretchingBuckets__init(
        (
          await ERC677Mock.new('', '')
        ).address,
        TIME_UNIT,
        BUCKET_LIMIT
      );

      let updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setTime(updateTime);

      const payment = toBN('100000000000');
      await anotherPaymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: payment });

      updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setTime(updateTime);

      await truffleAssert.reverts(anotherPaymentStretchingBuckets.allocate(), '[QEC-044004]-Allocation failed');
    });

    it('should revert if amount is bigger than released amount', async () => {
      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: PAYMENT });

      const updateTime = (await getCurrentBlockTime()) + 3 * TIME_UNIT + 1;
      await setNextBlockTime(updateTime);

      const amountToAllocate = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).times(5);
      await truffleAssert.reverts(
        paymentStretchingBuckets.allocate(amountToAllocate),
        '[QEC-044007]-Allocatable amount is less than requested amount.'
      );
    });
  });

  describe('getters', () => {
    it('should get bucket index', async () => {
      let currentBucketIndex = await paymentStretchingBuckets.getCurrentBucketIndex();
      let expectedBucketIndex = toBN(await getCurrentBlockTime()).dividedToIntegerBy(toBN(TIME_UNIT));
      assert.equal(toBN(currentBucketIndex).toNumber(), expectedBucketIndex.toNumber());

      const updateTime = (await getCurrentBlockTime()) + 17 * TIME_UNIT + 1;
      await setTime(updateTime);

      currentBucketIndex = await paymentStretchingBuckets.getCurrentBucketIndex();
      assert.equal(toBN(currentBucketIndex).toNumber(), expectedBucketIndex.toNumber() + 17);
    });

    it('should get release amount', async () => {
      let releasedAmount = await paymentStretchingBuckets.getReleasedAmount();
      assert.equal(toBN(releasedAmount).toString(), '0');

      let updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      releasedAmount = await paymentStretchingBuckets.getReleasedAmount();
      assert.equal(toBN(releasedAmount).toString(), '0');

      const PAYMENT = toBN('120');

      await paymentStretchingBuckets.deposit(BUCKET_LIMIT, { value: PAYMENT });

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      releasedAmount = await paymentStretchingBuckets.getReleasedAmount();
      assert.equal(toBN(releasedAmount).toString(), PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).toString());

      const amountToAllocate = PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).div(2);
      await paymentStretchingBuckets.allocate(amountToAllocate);

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      releasedAmount = await paymentStretchingBuckets.getReleasedAmount();
      assert.equal(
        toBN(releasedAmount).toString(),
        PAYMENT.dividedToIntegerBy(toBN(BUCKET_LIMIT)).plus(amountToAllocate).toString()
      );

      updateTime = (await getCurrentBlockTime()) + 300 * TIME_UNIT;
      await setTime(updateTime);

      releasedAmount = await paymentStretchingBuckets.getReleasedAmount();
      assert.equal(toBN(releasedAmount).toString(), PAYMENT.minus(amountToAllocate).toString());
    });
  });

  describe('integration', () => {
    it('should handle happy flow', async () => {
      const fourthPaymentStretchingBuckets = await makeProxy(PROXY_ADMIN, PaymentStretchingBuckets);

      const NEW_BUCKET_COUNT = 10;

      let initialBalance = toBN(await web3.eth.getBalance(TREASURY));

      await fourthPaymentStretchingBuckets.PaymentStretchingBuckets__init(TREASURY, TIME_UNIT, NEW_BUCKET_COUNT);

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        initialBalance,
        0,
        TREASURY
      );

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.allocate();

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        initialBalance,
        0,
        TREASURY
      );

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.deposit(NEW_BUCKET_COUNT, { value: 200 });

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [20, 20, 20, 20, 20, 20, 20, 20, 20, 20],
        initialBalance,
        0,
        TREASURY
      );

      let updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.deposit(NEW_BUCKET_COUNT, { value: 100 });

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '20',
        [30, 30, 30, 30, 30, 30, 30, 30, 30, 10],
        initialBalance,
        0,
        TREASURY
      );

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.allocate();

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [30, 30, 30, 30, 30, 30, 30, 30, 30, 10],
        initialBalance,
        20,
        TREASURY
      );

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.deposit(NEW_BUCKET_COUNT, { value: 100 });

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '30',
        [40, 40, 40, 40, 40, 40, 40, 40, 20, 10],
        initialBalance,
        0,
        TREASURY
      );

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.deposit(NEW_BUCKET_COUNT, { value: 200 });

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '70',
        [60, 60, 60, 60, 60, 60, 60, 40, 30, 20],
        initialBalance,
        0,
        TREASURY
      );

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.allocate();

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [60, 60, 60, 60, 60, 60, 40, 30, 20, 0],
        initialBalance,
        130,
        TREASURY
      );

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.allocate();

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [60, 60, 60, 60, 60, 60, 40, 30, 20, 0],
        initialBalance,
        0,
        TREASURY
      );

      updateTime = (await getCurrentBlockTime()) + TIME_UNIT;
      await setTime(updateTime);

      initialBalance = toBN(await web3.eth.getBalance(TREASURY));
      await fourthPaymentStretchingBuckets.allocate();

      await assetToAllocateAndBuckets(
        fourthPaymentStretchingBuckets,
        '0',
        [60, 60, 60, 60, 60, 40, 30, 20, 0, 0],
        initialBalance,
        60,
        TREASURY
      );
    });
  });
});
