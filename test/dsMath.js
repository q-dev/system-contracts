const { assert } = require('chai');
const BigNumber = require('bignumber.js');

const { artifacts } = require('hardhat');
const DSMathMock = artifacts.require('./DSMathMock');

describe('DSMath', () => {
  const decimal = new BigNumber(1e28);

  let dsmath;

  before('setup', async () => {
    dsmath = await DSMathMock.new();
  });

  describe('rpow', () => {
    it('should give a correct result', async () => {
      const x = decimal.multipliedBy(0.05);
      const r = await dsmath.rpow(x, 2, decimal);
      const expected = '25000000000000000000000000';
      assert.equal(expected.toString(), r.toString(), 'unexpected value');
    });
  });
});
