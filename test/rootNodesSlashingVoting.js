'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');
const { getVoteTimeHelper } = require('./helpers/votingHelper');

const { getCurrentBlockTime, setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const RootNodesSlashingVoting = artifacts.require('./RootNodesSlashingVoting');
const RootNodeSlashingEscrow = artifacts.require('./RootNodeSlashingEscrow');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require('Constitution');
const Roots = artifacts.require('Roots');
const RootsVoting = artifacts.require('RootsVoting');
const Validators = artifacts.require('Validators');
const QVault = artifacts.require('QVault');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

const DelegationStatus = Object.freeze({
  SELF: 0,
  DELEGATED: 1,
  PENDING: 2,
});

describe('RootNodesSlashingVoting', () => {
  const EMPTY_ADDR = '0x0000000000000000000000000000000000000000';

  const reverter = new Reverter();

  let roots;
  let registry;
  let validators;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let rootNodesSlashingVoting;
  let rootsVoting;
  let votingWeightProxy;
  let constitution;
  let remark;
  let percentage;
  let rootNodeSlashingEscrow;
  let qVault;
  let crKeeperFactory;

  const Q = require('./helpers/defiHelper').Q;
  const proposalExecutionP = 2592000;
  const votingPeriod = 86400;

  const commitAmount = Q.times(100);

  let OWNER;
  let ROOT;
  let USER1;
  let USER2;
  let CANDIDATE;
  let CANDIDATE2;
  let NON_EXISTING_ROOT;
  let PARAMETERS_VOTING;

  before(async () => {
    OWNER = await accounts(0);
    ROOT = await accounts(1);
    USER1 = await accounts(6);
    USER2 = await accounts(3);
    CANDIDATE = await accounts(7);
    CANDIDATE2 = await accounts(8);
    NON_EXISTING_ROOT = await accounts(2);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.voting.rootSlashingVP', 86400, { from: PARAMETERS_VOTING });
    const requiredMajority = getPercentageFormat(50); // it's 50% from 10 ** 27
    await constitution.setUint('constitution.voting.rootSlashingRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingQRM', 0, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingRNVALP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.rootSlashingSQRM', getPercentageFormat(1), {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.rootSlashingSMAJ', getPercentageFormat(80), {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.rootWithdrawP', 21, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await constitution.setUint('constitution.voting.rootSlashingOBJP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.rootSlashingAppealP', 86400, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.rootNodes', 'governance.validators'],
      ['governance.rootNodes.slashingVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address, { from: OWNER });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT, CANDIDATE, CANDIDATE2]);
    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });
    await roots.commitStake({ from: CANDIDATE, value: toBN(1000000000) });
    await roots.commitStake({ from: CANDIDATE2, value: toBN(1000000000) });

    rootsVoting = await makeProxy(registry.address, RootsVoting);
    await rootsVoting.initialize(registry.address);
    await registry.setAddress('governance.rootNodes.membershipVoting', rootsVoting.address, { from: OWNER });

    rootNodeSlashingEscrow = await makeProxy(registry.address, RootNodeSlashingEscrow);
    await rootNodeSlashingEscrow.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.rootNodes.slashingEscrow', rootNodeSlashingEscrow.address, { from: OWNER });

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();

    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );
    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    rootNodesSlashingVoting = await makeProxy(registry.address, RootNodesSlashingVoting);
    await rootNodesSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.rootNodes.slashingVoting', rootNodesSlashingVoting.address);

    remark = 'https://ethereum.org';
    percentage = getPercentageFormat(60); // it's 60% from 10 ** 27

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    const proposalID = 0;

    it('should create a proposal with existing root node', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      assert.equal((await rootNodesSlashingVoting.proposalCount()).toString(), 1);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.remark.toString(), remark);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).candidate.toString(), CANDIDATE);

      const rootNodeStake = toBN(await roots.getRootNodeStake(CANDIDATE));
      const expectedAmountToSlash = (percentage * rootNodeStake) / getPercentageFormat(100);
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).amountToSlash.toString(),
        expectedAmountToSlash.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);
    });

    it('should revert, candidate does not have any amount to slash', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.createProposal(remark, NON_EXISTING_ROOT, percentage, { from: ROOT }),
        '[QEC-004000]-The candidate does not have any stake to slash.'
      );

      assert.equal((await rootNodesSlashingVoting.proposalCount()).toString(), 0);
    });

    it('should not be possible to vote if _percentage parameter is invalid', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(101), { from: ROOT }),
        '[QEC-004008]-Invalid percentage parameter, slashing proposal creation failed.'
      );

      assert.equal((await rootNodesSlashingVoting.proposalCount()).toString(), 0);
    });

    it('QDEV-3059 should create a proposal with existing root node', async () => {
      await roots.commitStake({ from: CANDIDATE, value: commitAmount });
      await roots.announceWithdrawal(commitAmount, { from: CANDIDATE });
      // eslint-disable-next-line max-len
      const reason = '[QEC-002003]-Not enough time has elapsed since the announcement of the withdrawal.';
      await truffleAssert.reverts(roots.withdraw(commitAmount, CANDIDATE, { from: CANDIDATE }), reason);

      const membershipBeforeRemoving = await roots.isMember(CANDIDATE);
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), EMPTY_ADDR, CANDIDATE, { from: CANDIDATE });
      const membershipAfterRemoving = await roots.isMember(CANDIDATE);
      assert.equal(membershipBeforeRemoving, true);
      assert.equal(membershipAfterRemoving, false);

      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      assert.equal((await rootNodesSlashingVoting.proposalCount()).toString(), 1);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.remark.toString(), remark);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).candidate.toString(), CANDIDATE);

      const rootNodeStake = toBN(await roots.getRootNodeStake(CANDIDATE));
      const expectedAmountToSlash = (percentage * rootNodeStake) / getPercentageFormat(100);
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).amountToSlash.toString(),
        expectedAmountToSlash.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);

      await roots.commitStake({ from: ROOT, value: commitAmount });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });

      const voteEndTime = (await getVoteTimeHelper(rootNodesSlashingVoting, proposalID)) + votingPeriod;
      await setTime(voteEndTime);

      await rootNodesSlashingVoting.execute(proposalID, { from: ROOT });
    });

    it('should revert, try to create several proposals with same proposer and victim pair', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });

      assert.equal((await rootNodesSlashingVoting.proposalCount()).toString(), 1);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.remark.toString(), remark);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).candidate.toString(), CANDIDATE);

      const rootNodeStake = toBN(await roots.getRootNodeStake(CANDIDATE));
      const expectedAmountToSlash = (percentage * rootNodeStake) / getPercentageFormat(100);
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).amountToSlash.toString(),
        expectedAmountToSlash.toString()
      );

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);

      const reason = '[QEC-004012]-The slashing proposal with same creator and victim exists.';
      await truffleAssert.reverts(
        rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT }),
        reason
      );

      const pendingSlashingProposals = await roots.getSlashingProposalIds(CANDIDATE);
      assert.equal(
        pendingSlashingProposals.length,
        1,
        'pendingSlashingProposals should have only 1 proposal for CANDIDATE - proposer pair'
      );
    });
  });

  describe('vote', () => {
    let proposalID;
    beforeEach('init proposal', async () => {
      const res = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      const lockedWeight = toBN(1000);
      await lockWeight(qVault, ROOT, lockedWeight);
      const result = await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeight.toString()
      );
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await rootNodesSlashingVoting.hasUserVoted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should be possible to vote against an existing pending proposal', async () => {
      const lockedWeight = toBN(1000);
      await lockWeight(qVault, ROOT, lockedWeight);
      const result = await rootNodesSlashingVoting.voteAgainst(proposalID, { from: ROOT });
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 0);
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeight.toString()
      );
      assert.isTrue(await rootNodesSlashingVoting.hasUserVoted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 2);
    });

    it('should not be possible to vote if already voted', async () => {
      const lockedWeight = toBN(50000);
      await lockWeight(qVault, ROOT, lockedWeight);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeight.toString()
      );
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await rootNodesSlashingVoting.hasUserVoted(0, ROOT));

      await truffleAssert.reverts(
        rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT }),
        '[QEC-004003]-The caller has already voted for the proposal.'
      );

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeight.toString()
      );
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.isTrue(await rootNodesSlashingVoting.hasUserVoted(0, ROOT));
    });

    it('should not be able to vote for non-existing proposal', async () => {
      const nonExistingID = proposalID + 1;

      const lockedWeight = toBN(1000000);
      await lockWeight(qVault, ROOT, lockedWeight);

      await truffleAssert.reverts(
        rootNodesSlashingVoting.voteAgainst(nonExistingID, { from: ROOT }),
        '[QEC-004005]-The slashing proposal does not exist.'
      );

      assert.equal((await rootNodesSlashingVoting.proposals(nonExistingID)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await rootNodesSlashingVoting.proposals(nonExistingID)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await rootNodesSlashingVoting.hasUserVoted(1, ROOT));
    });

    it('should not be able to vote if the total voting weight equals zero', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 }),
        '[QEC-004007]-The total voting weight must be greater than zero.'
      );

      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await rootNodesSlashingVoting.hasUserVoted(1, USER1));
    });
  });

  describe('execute', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully execute a PASSED proposal', async () => {
      const lockedWeightForRoot = toBN(500000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.toString()
      );

      const voteEndTime = (await getVoteTimeHelper(rootNodesSlashingVoting, proposalID)) + votingPeriod;
      await setTime(voteEndTime);

      let status = (await rootNodesSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      const escrowBalance = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      const rootNodeStake = toBN(await roots.getRootNodeStake(CANDIDATE));
      const expectedAmountToSlash = (await rootNodesSlashingVoting.proposals(proposalID)).amountToSlash;

      const txReceipt = await rootNodesSlashingVoting.execute(proposalID, { from: ROOT });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalID);
      assert.equal(txReceipt.logs[0].args._candidate, CANDIDATE);
      assert.equal(txReceipt.logs[0].args._amountToSlash.toString(), expectedAmountToSlash.toString());

      status = (await rootNodesSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');

      const rootNodeStakeAfterSlashing = toBN(await roots.getRootNodeStake(CANDIDATE));
      const expectedStake = rootNodeStake - expectedAmountToSlash;
      assert.equal(expectedStake, rootNodeStakeAfterSlashing.toString());

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(rootNodeSlashingEscrow.address));
      assert.equal(escrowBalance.plus(expectedAmountToSlash).toString(), escrowBalanceAfterSlashing.toString());
    });

    it('should not be possible to execute expired proposal', async () => {
      const lockedWeightForRoot = toBN(500000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      const lockedWeightForUser2 = toBN(70000);
      await lockWeight(qVault, USER2, lockedWeightForUser2);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER2 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.plus(lockedWeightForUser2).toString()
      );

      const voteEndTime = (await getVoteTimeHelper(rootNodesSlashingVoting, proposalID)) + votingPeriod;
      await setTime(voteEndTime);

      let status = (await rootNodesSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      const proposalEndTime = (await getVoteTimeHelper(rootNodesSlashingVoting, proposalID)) + proposalExecutionP;
      await setTime(proposalEndTime);
      await truffleAssert.reverts(
        rootNodesSlashingVoting.execute(proposalID, { from: ROOT }),
        '[QEC-004006]-The slashing proposal has not passed.'
      );

      status = (await rootNodesSlashingVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXPIRED, status, 'unexpected status after proposal is executed');
    });

    it('should only be able to execute a PASSED proposal', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.execute(proposalID, { from: ROOT }),
        '[QEC-004006]-The slashing proposal has not passed.'
      );

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });
  });

  describe('getStatus()', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await rootNodesSlashingVoting.getStatus(1)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      const lockedWeightForRoot = toBN(500000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status, all roots vetoed proposal', async () => {
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      await rootNodesSlashingVoting.veto(proposalID, { from: CANDIDATE });
      await rootNodesSlashingVoting.veto(proposalID, { from: ROOT });

      await setTime(voteEndTime + votingPeriod + 1);

      const proposal = await rootNodesSlashingVoting.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 2);
      assert.equal(await rootNodesSlashingVoting.getStatus(proposalID), ProposalStatus.REJECTED);
    });

    it('should return ACCEPTED status if veto time window is open ', async () => {
      await lockWeight(qVault, USER1, Q);
      await lockWeight(qVault, USER2, Q);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER2 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      assert.equal(await rootNodesSlashingVoting.getStatus(proposalID), ProposalStatus.ACCEPTED);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      const lockedWeightForRoot = toBN(10000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      const lockedWeightForUser2 = toBN(10000000);
      await lockWeight(qVault, USER2, lockedWeightForUser2);

      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER2 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.plus(lockedWeightForUser2).toString()
      );

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + votingPeriod + 1);

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await constitution.setUint('constitution.voting.rootSlashingQRM', getPercentageFormat(80), {
        from: PARAMETERS_VOTING,
      });

      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE2, percentage, { from: ROOT });
      const proposalID = Number(result.logs[0].args._id);

      const lockedWeightForRoot = toBN(10000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      const lockedWeightForUser2 = toBN(10000000);
      await lockWeight(qVault, USER2, lockedWeightForUser2);

      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER2 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.plus(lockedWeightForUser2).toString()
      );

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + votingPeriod + 1);

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      const lockedWeightForRoot = toBN(70000000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.toString()
      );

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + votingPeriod + 1);

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED on expired proposal', async () => {
      const lockedWeightForRoot = toBN(70000000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.plus(lockedWeightForUser).toString()
      );
      assert.equal((await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + proposalExecutionP + 1);

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a executed proposal', async () => {
      const lockedWeightForRoot = toBN(9999999999);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER1 });

      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightFor.toString(),
        lockedWeightForRoot.toString()
      );
      assert.equal(
        (await rootNodesSlashingVoting.proposals(proposalID)).base.counters.weightAgainst.toString(),
        lockedWeightForUser.toString()
      );

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + votingPeriod + 1);

      await rootNodesSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), ProposalStatus.EXECUTED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const firstProp = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const firstPropID = Number(firstProp.logs[0].args._id);
      const firstPropBase = (await rootNodesSlashingVoting.proposals.call(firstPropID)).base;

      let expectedBase = await rootNodesSlashingVoting.getProposal(firstPropID);

      assert.equal(expectedBase.remark, firstPropBase.remark);
      assert.equal(expectedBase.executed, firstPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, firstPropBase.params.votingEndTime);

      const secondProp = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE2, percentage, { from: ROOT });
      const secondPropID = Number(secondProp.logs[0].args._id);
      const secondPropBase = (await rootNodesSlashingVoting.proposals.call(secondPropID)).base;

      expectedBase = await rootNodesSlashingVoting.getProposal(secondPropID);

      assert.equal(expectedBase.remark, secondPropBase.remark);
      assert.equal(expectedBase.executed, secondPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, secondPropBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.getProposal(1),
        '[QEC-004005]-The slashing proposal does not exist.'
      );
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const proposalID = Number(result.logs[0].args._id);

      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      const propStats = await rootNodesSlashingVoting.getProposalStats(proposalID);
      const proposal = await rootNodesSlashingVoting.proposals.call(proposalID);

      assert.equal(proposal.base.counters.weightFor, lockedWeightForUser);

      assert.equal(propStats.requiredMajority, proposal.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, proposal.base.params.requiredQuorum);

      const totalWeight = toBN(proposal.base.counters.weightFor).plus(proposal.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(proposal.base.counters.weightFor, totalWeight);
      assert.equal(currentMajority.toNumber(), propStats.currentMajority);
    });

    it('should not be possible to get proposal stats if the proposal does not exist', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.getProposalStats(1),
        '[QEC-004005]-The slashing proposal does not exist.'
      );
    });
  });

  describe('veto()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should veto on proposal', async () => {
      await lockWeight(qVault, USER1, Q);
      await lockWeight(qVault, USER2, Q);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER2 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      await rootNodesSlashingVoting.veto(proposalID, { from: ROOT });

      const proposal = await rootNodesSlashingVoting.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 1);
      assert.equal((await rootNodesSlashingVoting.getStatus(proposalID)).toString(), 3);

      assert.isTrue(await rootNodesSlashingVoting.hasRootVetoed(proposalID, ROOT));
    });

    it('should get exception, invalid proposal status', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: CANDIDATE });
      await truffleAssert.reverts(
        rootNodesSlashingVoting.veto(1, { from: ROOT }),
        '[QEC-004002]-Proposal must be ACCEPTED before casting a root node veto.'
      );
    });

    it('should get exception, overrule is accepted', async () => {
      await roots.commitStake({ from: ROOT, value: commitAmount });
      await constitution.setUint('constitution.voting.rootSlashingSQRM', 0, { from: PARAMETERS_VOTING });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: CANDIDATE });
      proposalID = Number(result.logs[0].args._id);

      await lockWeight(qVault, USER1, Q.multipliedBy(1000));
      await rootNodesSlashingVoting.voteFor(1, { from: USER1 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      await truffleAssert.reverts(
        rootNodesSlashingVoting.veto(1, { from: ROOT }),
        '[QEC-004010]-Veto is not possible because of super majority vote.'
      );

      assert.equal(await rootNodesSlashingVoting.getStatus(1), ProposalStatus.ACCEPTED);
    });

    it('should get exception, non existing proposal', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.veto(100, { from: ROOT }),
        '[QEC-004005]-The slashing proposal does not exist.'
      );
    });

    it('should get exception, call by non root', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.veto(proposalID, { from: USER1 }),
        '[QEC-004004]-Permission denied - only root nodes have access.'
      );
    });
  });

  describe('skipVetoPhase()', () => {
    let proposalID;

    beforeEach(async () => {
      await constitution.setUint('constitution.voting.rootSlashingSQRM', 0, { from: PARAMETERS_VOTING });
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully skip veto phase', async () => {
      await lockWeight(qVault, USER1, Q);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime);

      await rootNodesSlashingVoting.skipVetoPhase(proposalID);

      assert.equal(await rootNodesSlashingVoting.getStatus(proposalID), ProposalStatus.PASSED);
    });

    it('should get exception, overrule do not applied', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.skipVetoPhase(proposalID),
        '[QEC-004011]-Veto phase can be skipped only with super majority vote.'
      );
    });
  });

  describe('isOverruleApplied()', () => {
    let proposalID;

    beforeEach(async () => {
      await constitution.setUint('constitution.voting.rootSlashingSQRM', 0, { from: PARAMETERS_VOTING });
      await roots.commitStake({ from: CANDIDATE, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should return true, overrule applied', async () => {
      await lockWeight(qVault, USER1, Q.multipliedBy(100000));
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      assert.isTrue(await rootNodesSlashingVoting.isOverruleApplied(proposalID));
    });

    it('should return false, the proposal did not reach a super-majority', async () => {
      await lockWeight(qVault, USER1, Q.multipliedBy(70000));
      await lockWeight(qVault, USER2, Q.multipliedBy(30000));

      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER2 });

      assert.isFalse(await rootNodesSlashingVoting.isOverruleApplied(proposalID));
    });

    it('should return false, the proposal did not reach a super-quorum', async () => {
      await constitution.setUint('constitution.voting.rootSlashingSQRM', getPercentageFormat(5), {
        from: PARAMETERS_VOTING,
      });
      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: CANDIDATE });
      proposalID = Number(result.logs[0].args._id);

      await lockWeight(qVault, USER1, Q.multipliedBy(90000));
      await lockWeight(qVault, USER2, Q.multipliedBy(10000));

      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });
      await rootNodesSlashingVoting.voteAgainst(proposalID, { from: USER2 });

      assert.isFalse(await rootNodesSlashingVoting.isOverruleApplied(proposalID));
    });
  });

  describe('getVotingWeightInfo()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully return the voting weight info', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER1 });

      const votingWeightInfo = await rootNodesSlashingVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.ownWeight, lockedWeightForUser.toString());
      assert.equal(votingWeightInfo.base.votingAgent, USER1);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.SELF);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
      assert.equal(votingWeightInfo.hasAlreadyVoted, true);
      assert.equal(votingWeightInfo.canVote, false);
    });

    it('should return PENDING status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });

      const votingWeightInfo = await rootNodesSlashingVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.PENDING);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
    });

    it('should return DELEGATED status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });
      const delegationInfo = await votingWeightProxy.delegationInfos(USER1);
      await setTime((await getCurrentBlockTime()) + toBN(delegationInfo.votingAgentPassOverTime).plus(1).toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: USER1 });

      await roots.commitStake({ from: ROOT, value: commitAmount });
      const result = await rootNodesSlashingVoting.createProposal(remark, ROOT, percentage, { from: CANDIDATE });
      proposalID = Number(result.logs[0].args._id);
      await rootNodesSlashingVoting.voteFor(proposalID, { from: USER2 });

      const votingWeightInfo = await rootNodesSlashingVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.DELEGATED);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER2)).toString());
    });

    it('should revert if the slashing proposal does not exist', async () => {
      await truffleAssert.reverts(
        rootNodesSlashingVoting.getVotingWeightInfo(10001, { from: USER1 }),
        '[QEC-004005]-The slashing proposal does not exist.'
      );
    });
  });
  describe('bug test', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const result = await rootNodesSlashingVoting.createProposal(remark, CANDIDATE, getPercentageFormat(100), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should be one candidate in root-list after slashing and commit', async () => {
      const lockedWeightForRoot = toBN(500000000);
      await lockWeight(qVault, ROOT, lockedWeightForRoot);
      const lockedWeightForUser = toBN(10000000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await rootNodesSlashingVoting.voteFor(proposalID, { from: ROOT });

      const voteEndTime = await getVoteTimeHelper(rootNodesSlashingVoting, proposalID);
      await setTime(voteEndTime + votingPeriod + 1);

      await rootNodesSlashingVoting.execute(proposalID, { from: ROOT });
      await roots.commitStake({ from: CANDIDATE, value: 1000 });
      const stakes = await roots.getStakes();
      const count = stakes.map((_) => _.root).filter((_) => _ === CANDIDATE).length;
      assert.equal(count, 1, 'should be one root');
    });
  });
});

async function lockWeight(bank, voter, weight) {
  await bank.deposit({ from: voter, value: toBN(weight) });
  await bank.lock(toBN(weight), { from: voter });
}
