const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const BigNumber = require('bignumber.js');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const ContractRegistryUpgradeVoting = artifacts.require('ContractRegistryUpgradeVoting');
const ContractRegistry = artifacts.require('ContractRegistry');
const Roots = artifacts.require('Roots');
const Constitution = artifacts.require('Constitution');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');
const TransparentUpgradeableProxy = artifacts.require('TransparentUpgradeableProxy');

describe('ContractRegistryUpgradeVoting', () => {
  const reverter = new Reverter();

  let DEFAULT;
  let SOMEBODY;
  let ROOT1;
  let ROOT2;
  let ROOT3;
  let ROOT4;

  let contractRegistryUpgradeVoting;
  let registry;
  let roots;
  let proxy;

  before('setup', async () => {
    DEFAULT = await accounts(0);
    SOMEBODY = await accounts(1);
    ROOT1 = await accounts(2);
    ROOT2 = await accounts(3);
    ROOT3 = await accounts(4);
    ROOT4 = await accounts(5);

    registry = await ContractRegistry.new();
    contractRegistryUpgradeVoting = await ContractRegistryUpgradeVoting.new();
    await registry.initialize([DEFAULT, contractRegistryUpgradeVoting.address], [], []);
    await contractRegistryUpgradeVoting.initialize(registry.address);
    const constitution = await Constitution.new();
    const addressStorageFactory = await AddressStorageFactory.new();
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    proxy = await TransparentUpgradeableProxy.new(registry.address, registry.address, []);
    await constitution.initialize(
      registry.address,
      ['constitution.proposalExecutionP', 'constitution.voting.emgQUpdateRMAJ'],
      ['3600', '500000000000000000000000000'],
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.constitution.parameters', constitution.address);
    roots = await Roots.new();
    await roots.initialize(registry.address, [ROOT1, ROOT2, ROOT3, ROOT4]);
    await registry.setAddress('governance.rootNodes', roots.address);
    await registry.setAddress('governance.rootNodes.membershipVoting', DEFAULT);
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    it('should revert if not owner', async () => {
      await truffleAssert.reverts(
        contractRegistryUpgradeVoting.createProposal(proxy.address, roots.address, { from: SOMEBODY })
      );
    });

    it('should create proposal', async () => {
      await contractRegistryUpgradeVoting.createProposal(proxy.address, roots.address);
      const count = await contractRegistryUpgradeVoting.proposalsCount();
      assert.equal(count.toString(), '1');

      const proposal = await contractRegistryUpgradeVoting.getProposal(0);
      assert.equal(proposal.proxy, proxy.address);
      assert.equal(proposal.implementation, roots.address);
    });
  });
  describe('approve', () => {
    beforeEach('setup for approve', async () => {
      await contractRegistryUpgradeVoting.createProposal(proxy.address, roots.address);
    });

    it('should revert if not rootNode', async () => {
      await truffleAssert.reverts(contractRegistryUpgradeVoting.approve(0, { from: SOMEBODY }), '[QEC-039000]');
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT1 });
    });

    it('should approve once', async () => {
      const voteCountBefore = bn(await contractRegistryUpgradeVoting.voteCount(0));
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT1 });
      await truffleAssert.reverts(contractRegistryUpgradeVoting.approve(0, { from: ROOT1 }), '[QEC-039004]');
      const voteCountAfter = bn(await contractRegistryUpgradeVoting.voteCount(0));

      assert.equal(voteCountBefore.plus(1).toString(), voteCountAfter.toString());
      assert.isFalse((await contractRegistryUpgradeVoting.getProposal(0)).executed);
    });

    it('should change address after 3of4 votes', async () => {
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT1 });
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT2 });
      assert.isFalse((await contractRegistryUpgradeVoting.getProposal(0)).executed);
      const tx = await contractRegistryUpgradeVoting.approve(0, { from: ROOT3 });

      const eventSigns = tx.receipt.rawLogs.map((a) => a.topics[0]);

      // Keccak-256  of Upgraded(address)
      const upgradedSig = '0xbc7cd75a20ee27fd9adebab32041f755214dbc6bffa90cc0225b39da2e5c2d3b';
      // console.log(eventSigns);
      assert.isTrue(eventSigns.includes(upgradedSig));
      assert.isTrue((await contractRegistryUpgradeVoting.getProposal(0)).executed);
    });

    it('should change address after root4 leave', async () => {
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT1 });
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT2 });
      await roots.removeMember(ROOT4);
      await contractRegistryUpgradeVoting.approve(0, { from: ROOT2 });
      assert.isTrue((await contractRegistryUpgradeVoting.getProposal(0)).executed);
    });
  });
});

function bn(num) {
  return new BigNumber(num);
}
