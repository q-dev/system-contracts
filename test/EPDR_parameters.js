const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const ERC20 = artifacts.require('ERC677Mock');
const BorrowingCore = artifacts.require('BorrowingCore');
const CRKeeper = artifacts.require('CompoundRateKeeper');
const CRKeeperFactory = artifacts.require('CompoundRateKeeperFactory');

describe('EPDR_Parameters', () => {
  const reverter = new Reverter();

  let USD;
  let TTT;
  let params;
  let registry;
  let core;

  before(async () => {
    const keeper = await CRKeeper.new();
    const keeperFactory = await CRKeeperFactory.new();
    await keeperFactory.initialize(keeper.address);

    USD = await ERC20.new('UncleSamDollar', 'USD');
    TTT = await ERC20.new('TestTokenT', 'TTT');
    params = await Parameters.new();
    registry = await ContractRegistry.new();
    core = await BorrowingCore.new();
    core.initialize(registry.address, 'USD');
    registry.initialize(
      [await accounts(0)],
      [
        'governance.experts.EPDR.parametersVoting',
        'defi.QUSD.borrowing',
        'governance.experts.EPDR.parameters',
        'common.factory.crKeeper',
      ],
      [await accounts(0), core.address, params.address, keeperFactory.address]
    );
    await params.initialize(
      registry.address,
      [],
      [],
      ['governed.EPDR.USD_address', 'governed.EPDR.TTT_address', 'some'],
      [USD.address, USD.address, await accounts(1)],
      [],
      [],
      [],
      []
    );
    await core.createVault('USD');
    await USD.approve(core.address, 1);
    await core.depositCol(0, 1);
    await USD.balanceOf(core.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('isMutableAddress', () => {
    it('should change addr if not token address', async () => {
      assert.equal(await params.getAddr('some'), await accounts(1));
      assert.isTrue(await params.isMutableAddress('some'));

      await params.setAddr('some', await accounts(2));
      assert.equal(await params.getAddr('some'), await accounts(2));
      assert.isTrue(await params.isMutableAddress('some'));
    });

    it('should return false for tokens', async () => {
      assert.isFalse(await params.isMutableAddress('governed.EPDR.USD_address'));
      assert.isFalse(await params.isMutableAddress('governed.EPDR.TTT_address')); // USD token
      assert.isTrue(await params.isMutableAddress('some'));
      assert.isTrue(await params.isMutableAddress('not setuped param'));
    });

    it('should revert if token deposited into vault', async () => {
      await truffleAssert.reverts(params.setAddr('governed.EPDR.USD_address', TTT.address), 'Immutable');
      await params.setAddr('governed.EPDR.TTT2_address', TTT.address);
      await params.setAddr('governed.EPDR.TTT2_address', USD.address);
      await truffleAssert.reverts(params.setAddr('governed.EPDR.TTT2_address', TTT.address), 'Immutable');
    });

    it('should upgrade address if key format incorrect', async () => {
      await params.setAddr('governed.EPDR.TTT2', USD.address);
      assert.equal(await params.getAddr('governed.EPDR.TTT2'), USD.address);
      await params.setAddr('governed.EPDR.TTT2', TTT.address);
      assert.equal(await params.getAddr('governed.EPDR.TTT2'), TTT.address);
    });
  });
});
