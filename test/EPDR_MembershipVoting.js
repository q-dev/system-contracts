const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');
const { approximateAssert, lockWeight, getVoteTimeHelper, getVetoTimeHelper } = require('./helpers/votingHelper');

const { getCurrentBlockTime, setTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ExpertsVoting = artifacts.require('EPDR_MembershipVoting');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const QVault = artifacts.require('QVault');
const QHolderRewardPool = artifacts.require('QHolderRewardPool');
const Parameters = artifacts.require('EPDR_Parameters');
const Experts = artifacts.require('EPDR_Membership');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Roots = artifacts.require('./Roots');
const Validators = artifacts.require('Validators');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const VotingWeightProxy = artifacts.require('VotingWeightProxy');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = {
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
};

const DelegationStatus = Object.freeze({
  SELF: 0,
  DELEGATED: 1,
  PENDING: 2,
});

const ZERO = 0;

describe('ExpertsVoting', () => {
  const reverter = new Reverter();

  let expertsVoting;
  let qVault;
  let qHolderRewardPool;
  let parameters;
  let experts;
  let registry;
  let roots;
  let validators;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let crKeeperFactory;
  let votingWeightProxy;

  const proposalExecutionP = 2592000;
  const votingPeriod = 100;

  let OWNER;
  let USER1;
  let USER2;
  let USER3;
  let USER4;
  let EXPERTS_VOTING;
  let NON_EXISTING_EXPERT;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    USER3 = await accounts(3);
    USER4 = await accounts(6);
    EXPERTS_VOTING = await accounts(3);
    NON_EXISTING_EXPERT = await accounts(4);
    PARAMETERS_VOTING = await accounts(5);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();

    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );

    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    parameters = await makeProxy(registry.address, Parameters);
    await parameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await parameters.setUint('constitution.EPDR.maxNExperts', 10, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.maxNRootNodes', 1, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootVP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootRNVALP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootQRM', 0, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootRMAJ', getPercentageFormat(50), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.EPDR.addOrRemExpertQRM', getPercentageFormat(0.0000000000001), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.EPDR.addOrRemExpertRMAJ', getPercentageFormat(50), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.EPDR.addOrRemExpertRNVALP', 0, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await registry.setAddress('governance.constitution.parameters', parameters.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [OWNER, USER1]);
    await registry.setAddress('governance.rootNodes', roots.address);

    experts = await makeProxy(registry.address, Experts);
    await experts.initialize(registry.address, []);
    await registry.setAddress('governance.experts.EPDR.membershipVoting', EXPERTS_VOTING);
    await experts.addMember(USER2, { from: EXPERTS_VOTING });
    await experts.addMember(USER4, { from: EXPERTS_VOTING });
    await registry.setAddress('governance.experts.EPDR.membership', experts.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.rootNodes', 'governance.validators'],
      ['governance.experts.EPDR.membershipVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);
  });

  beforeEach('setup', async () => {
    expertsVoting = await makeProxy(registry.address, ExpertsVoting);
    await expertsVoting.initialize(registry.address);
    await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);

    await parameters.setUint('constitution.voting.EPDR.addOrRemExpertVP', votingPeriod, { from: PARAMETERS_VOTING });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createAddExpertProposal', () => {
    it('should be possible to createAddExpertProposal', async () => {
      const votingStartTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(votingStartTime);
      const result = await expertsVoting.createAddExpertProposal('test', USER1);

      assert.equal((await expertsVoting.proposalCount()).toString(), 1);
      assert.equal((await getVoteTimeHelper(expertsVoting, 0)).toString(), votingStartTime + votingPeriod);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should get exception, try to create ADD proposal when expert list size is greater than limit', async () => {
      const newExpertLimit = await experts.getSize();
      await parameters.setUint('constitution.EPDR.maxNExperts', newExpertLimit, { from: PARAMETERS_VOTING });
      const expertLimit = await experts.getLimit();
      assert.equal(expertLimit.toString(), newExpertLimit.toString());

      const reason = '[QEC-008014]-Maximum number of experts in this panel is reached.';
      await truffleAssert.reverts(expertsVoting.createAddExpertProposal('test', USER1), reason);
    });
  }).retries(4);

  describe('createChangeExpertProposal', () => {
    it('should be possible to createChangeExpertProposal', async () => {
      const votingStartTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(votingStartTime);
      const result = await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      assert.equal((await expertsVoting.proposalCount()).toString(), 1);
      assert.equal((await getVoteTimeHelper(expertsVoting, 0)).toString(), votingStartTime + votingPeriod);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, 0);
    }).retries(3);

    it('should get exception, try to create CHANGE proposal when expert list size is greater than limit', async () => {
      const expertNumber = await experts.getSize();
      const newExpertLimit = expertNumber - 1;
      await parameters.setUint('constitution.EPDR.maxNExperts', newExpertLimit, { from: PARAMETERS_VOTING });
      const expertLimit = await experts.getLimit();
      assert.equal(expertLimit, newExpertLimit);

      const reason = '[QEC-008013]-Maximum number of experts in this panel is exceeded.';
      await truffleAssert.reverts(expertsVoting.createChangeExpertProposal('test', USER1, USER2), reason);
    });
  });

  describe('createRemoveExpertProposal', () => {
    it('should be possible to createRemoveExpertProposal from USER1', async () => {
      const votingStartTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(votingStartTime);
      const result = await expertsVoting.createRemoveExpertProposal('test', USER2, { from: USER1 });

      assert.equal((await expertsVoting.proposalCount()).toString(), 1);
      assert.equal((await getVoteTimeHelper(expertsVoting, 0)).toString(), votingStartTime + votingPeriod);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, 0);
    });
  }).retries(3);

  describe('vote', () => {
    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      await qVault.deposit({ from: OWNER, value: 1000 });
      await qVault.lock(1000, { from: OWNER });
      const result = await expertsVoting.voteFor(0);

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor, 1000);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
      assert.equal(result.logs[0].args._amount, 1000);
    });

    it('should be possible to vote against an existing pending proposal with valid amount and expiration', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      await qVault.deposit({ value: 1000 });
      await qVault.lock(1000);

      const result = await expertsVoting.voteAgainst(0);

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 2);
      assert.equal(result.logs[0].args._amount, 1000);
    });

    it('should not be possible to vote for not pending proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      await qVault.deposit({ value: 1000 });
      await qVault.lock(1000);

      await truffleAssert.reverts(
        expertsVoting.voteAgainst(1),
        '[QEC-008003]-Voting is only possible on PENDING proposals.'
      );

      assert.equal((await expertsVoting.proposals(1)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await expertsVoting.proposals(1)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await expertsVoting.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote with zero total voting weight', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      await truffleAssert.reverts(
        expertsVoting.voteFor(0),
        '[QEC-008012]-The total voting weight must be greater than zero.'
      );

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await expertsVoting.hasUserVoted(0, OWNER));
    });

    it('should not be possible to vote if already voted', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      await qVault.deposit({ value: 1000 });
      await qVault.lock(1000);

      await expertsVoting.voteAgainst(0);

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      await truffleAssert.reverts(
        expertsVoting.voteAgainst(0),
        '[QEC-008004]-The caller has already voted for the proposal.'
      );

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
    });
  });

  describe('execute', () => {
    it('should fail to apply result if proposal`s status is not passed', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      await qVault.deposit({ value: 1 });
      await qVault.lock(1);

      await qVault.deposit({ from: USER1, value: 1001 });
      await qVault.lock(1001, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      await expertsVoting.voteFor(0);

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 1);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1001);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
      assert.isTrue(await expertsVoting.hasUserVoted(0, USER1));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      await truffleAssert.reverts(expertsVoting.execute(0), '[QEC-008006]-Proposal must be PASSED before excecuting.');
    });

    it('should apply changeExpert result if proposal is pass', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await qVault.deposit({ from: USER1, value: 999 });
      await qVault.lock(999, { from: USER1 });

      await expertsVoting.voteFor(0);

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 999);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
      assert.isTrue(await expertsVoting.hasUserVoted(0, USER1));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);

      const txReceipt = await expertsVoting.execute(0);
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, 0);

      assert.equal(await experts.isMember(USER1), true);
      assert.equal(await experts.isMember(USER2), false);
    });

    it('should apply addMember result if proposal is pass', async () => {
      await expertsVoting.createAddExpertProposal('test', USER1);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await qVault.deposit({ from: USER1, value: 999 });
      await qVault.lock(999, { from: USER1 });

      await expertsVoting.voteFor(0);

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 999);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
      assert.isTrue(await expertsVoting.hasUserVoted(0, USER1));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);
      await expertsVoting.execute(0);

      assert.equal(await experts.isMember(USER1), true);
      assert.equal(await experts.isMember(USER2), true);
    });

    it('should apply removeExpert result if proposal is pass', async () => {
      await expertsVoting.createRemoveExpertProposal('test', USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await qVault.deposit({ from: USER1, value: 999 });
      await qVault.lock(999, { from: USER1 });

      await expertsVoting.voteFor(0);

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 999);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
      assert.isTrue(await expertsVoting.hasUserVoted(0, USER1));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);
      await expertsVoting.execute(0);

      assert.equal(await experts.isMember(USER2), false);
    });

    it('should not be possible to execute if status is expired', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await qVault.deposit({ from: USER1, value: 999 });
      await qVault.lock(999, { from: USER1 });

      await expertsVoting.voteFor(0);

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 999);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));
      assert.isTrue(await expertsVoting.hasUserVoted(0, USER1));

      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);

      const vetoEndTime = await getVetoTimeHelper(expertsVoting, 0);
      await setTime(vetoEndTime + proposalExecutionP);

      await truffleAssert.reverts(expertsVoting.execute(0), '[QEC-008006]-Proposal must be PASSED before excecuting.');
    });
  });

  describe('createRemoveExpertProposal from owner', () => {
    it('should be possible to createRemoveExpertProposal', async () => {
      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);

      await expertsVoting.createRemoveExpertProposal('test', USER2, { from: USER2 });

      assert.isFalse(await experts.isMember(USER2), 'expert was not removed from experts list');
    });
  });

  describe('test getters', () => {
    const proposalID = 1;

    it('should successfully return votes weight "FOR"', async () => {
      await expertsVoting.getVotesFor(proposalID, { from: NON_EXISTING_EXPERT });
    });

    it('should successfully return votes weight "AGAINST"', async () => {
      await expertsVoting.getVotesAgainst(proposalID, { from: NON_EXISTING_EXPERT });
    });

    it('should successfully return vetoes count', async () => {
      const result = await expertsVoting.getVetosNumber(proposalID, { from: NON_EXISTING_EXPERT });
      assert.equal(result.toString(), ZERO.toString(), 'must be 0');
    });

    it('should successfully return vetoes count', async () => {
      const result = await expertsVoting.getVetosPercentage(proposalID, { from: NON_EXISTING_EXPERT });
      assert.equal(result.toString(), ZERO.toString(), 'must be 0');
    });
  });

  describe('getStatus', () => {
    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await expertsVoting.voteFor(0);

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      await qVault.deposit({ value: 1000 });
      await qVault.lock(1000);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1001 });
      await qVault.lock(1001, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), 1000);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await parameters.setUint('constitution.voting.addOrRemExpertQRM', getPercentageFormat(5), {
        from: PARAMETERS_VOTING,
      });

      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      await qVault.deposit({ value: 10000 });
      await qVault.lock(10000);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1000 });
      await qVault.lock(1000, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal((await expertsVoting.proposals(0)).base.counters.weightFor.toString(), 10000);
      assert.equal((await expertsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1000 });
      await qVault.lock(1000, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED status on a expired proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1000 });
      await qVault.lock(1000, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      const vetoEndTime = await getVetoTimeHelper(expertsVoting, 0);
      await setTime(vetoEndTime + proposalExecutionP + 1);

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a consumed proposal', async () => {
      await expertsVoting.createChangeExpertProposal('test', USER1, USER2);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1000 });
      await qVault.lock(1000, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      assert.equal(toBN((await expertsVoting.proposals(0)).base.counters.weightFor).toString(), voteWeight.toString());
      assert.isTrue(await expertsVoting.hasUserVoted(0, OWNER));

      const vetoEndTime = await getVetoTimeHelper(expertsVoting, 0);
      await setTime(vetoEndTime);

      await registry.setAddress('governance.experts.EPDR.membershipVoting', expertsVoting.address);
      await expertsVoting.execute(0);

      assert.equal((await expertsVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const firstProp = await expertsVoting.createAddExpertProposal('test', USER1);
      const firstPropID = Number(firstProp.logs[0].args._id);
      const firstPropBase = (await expertsVoting.proposals.call(firstPropID)).base;

      const expectedBase = await expertsVoting.getProposal(firstPropID);

      assert.equal(expectedBase.remark, firstPropBase.remark);
      assert.equal(expectedBase.executed, firstPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, firstPropBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(expertsVoting.getProposal(1), '[QEC-008011]-The proposal does not exist.');
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      await parameters.setUint('constitution.voting.EPDR.addOrRemExpertRNVALP', 1000, { from: PARAMETERS_VOTING });

      const result = await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      const propID = Number(result.logs[0].args._id);

      const voteWeight = toBN(10 ** 18);
      await qVault.deposit({ value: voteWeight });
      await qVault.lock(voteWeight);

      await expertsVoting.voteFor(0);

      await qVault.deposit({ from: USER1, value: 1000 });
      await qVault.lock(1000, { from: USER1 });

      await expertsVoting.voteAgainst(0, { from: USER1 });

      const endTime = votingPeriod + 1;
      await setTime((await getCurrentBlockTime()) + endTime);

      const blockNumber = (await expertsVoting.veto(0, { from: USER1 })).receipt.blockNumber;
      const propStats = await expertsVoting.getProposalStats(propID);
      const prop = await expertsVoting.proposals.call(propID);

      assert.equal(toBN(prop.base.counters.weightFor).toString(), voteWeight.toString());
      assert.equal(toBN(prop.base.counters.weightAgainst).toString(), 1000);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const totalWeight = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalWeight);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);

      assert.isTrue(approximateAssert(totalWeight, blockNumber, propStats.currentQuorum));

      const vetoesCount = prop.base.counters.vetosCount;
      assert.equal(vetoesCount, 1);

      const expectedVetoPercentage = toBN(vetoesCount)
        .times(10 ** 27)
        .dividedBy(2);
      assert.equal(propStats.currentVetoPercentage, expectedVetoPercentage.toNumber());
    });

    it('should return correct voting stats structure before the first vote', async () => {
      const result = await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      const propID = Number(result.logs[0].args._id);

      const propStats = await expertsVoting.getProposalStats(propID);
      const prop = await expertsVoting.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const totalWeight = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalWeight);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);
      assert.equal(propStats.currentQuorum, 0);
      assert.equal(propStats.currentVetoPercentage, 0);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(expertsVoting.getProposalStats(1), '[QEC-008011]-The proposal does not exist.');
    });
  });

  describe('getVotingWeightInfo()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await expertsVoting.createChangeExpertProposal('test', USER1, USER2);
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully return the voting weight info', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await expertsVoting.voteFor(proposalID, { from: USER1 });

      const votingWeightInfo = await expertsVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.ownWeight, lockedWeightForUser.toString());
      assert.equal(votingWeightInfo.base.votingAgent, USER1);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.SELF);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
      assert.equal(votingWeightInfo.hasAlreadyVoted, true);
      assert.equal(votingWeightInfo.canVote, false);
    });

    it('should return PENDING status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });

      const votingWeightInfo = await expertsVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.PENDING);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
    });

    it('should return DELEGATED status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });
      const delegationInfo = await votingWeightProxy.delegationInfos(USER1);

      await setTime((await getCurrentBlockTime()) + toBN(delegationInfo.votingAgentPassOverTime).plus(1).toNumber());

      await votingWeightProxy.setNewVotingAgent({ from: USER1 });

      const result = await expertsVoting.createChangeExpertProposal('test', USER3, USER2);
      proposalID = Number(result.logs[0].args._id);
      await expertsVoting.voteFor(proposalID, { from: USER2 });

      const votingWeightInfo = await expertsVoting.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.DELEGATED);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER2)).toString());
    });

    it('should revert if the slashing proposal does not exist', async () => {
      await truffleAssert.reverts(
        expertsVoting.getVotingWeightInfo(10001, { from: USER1 }),
        '[QEC-008011]-The proposal does not exist.'
      );
    });
  });
});
