'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ValidatorsSlashingVoting = artifacts.require('ValidatorsSlashingVoting');
const ValidatorSlashingEscrow = artifacts.require('ValidatorSlashingEscrow');
const ValidationRewardPools = artifacts.require('./ValidationRewardPools');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require('Constitution');
const Roots = artifacts.require('Roots');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Validators = artifacts.require('Validators');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const DefaultAllocationProxy = artifacts.require('DefaultAllocationProxy');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const QVault = artifacts.require('QVault');
const PushPayments = artifacts.require('PushPayments');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ArbitrationStatus = Object.freeze({
  NONE: 0,
  OPEN: 1,
  ACCEPTED: 2,
  PENDING: 3,
  DECIDED: 4,
  EXECUTED: 5,
});

describe('ValidatorSlashingEscrow', () => {
  const reverter = new Reverter();

  let registry;
  let constitution;
  let validatorsSlashingVoting;
  let crKeeperFactory;
  let validatorSlashingEscrow;
  let validationRewardPools;
  let addressStorageStakesSorted;
  let addressStorageStakes;
  let validators;
  let roots;
  let epqfiParams;
  let defaultAllocationProxy;
  let votingWeightProxy;
  let qVault;
  let pushPayments;

  const Q = require('./helpers/defiHelper').Q;
  const CR_UPDATE_MINIMUM_BASE = Q.multipliedBy(1000);

  const valSlashingOBJP = 86400;
  const valSlashingAppealP = 10000;
  const valSlashingVP = 86400;
  const requiredMajority = getPercentageFormat(50); // it's 50% from 10 ** 27

  let OWNER;
  let ROOT;
  let ROOT2;
  let ROOT3;
  let VALIDATOR;
  let VALIDATORS_CONTRACT;
  let PARAMETERS_VOTING;

  before(async () => {
    OWNER = await accounts(0);
    ROOT = await accounts(1);
    ROOT2 = await accounts(2);
    ROOT3 = await accounts(3);
    VALIDATOR = await accounts(4);
    VALIDATORS_CONTRACT = await accounts(8);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    await constitution.setUint('constitution.maxNValidators', 20, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.maxNStandbyValidators', 20, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', 1000, { from: PARAMETERS_VOTING });

    await constitution.setUint('constitution.voting.valSlashingVP', valSlashingVP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.valSlashingQRM', 0, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.valSlashingRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.valSlashingOBJP', valSlashingOBJP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.valSlashingAppealP', valSlashingAppealP, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.valSlashingReward', getPercentageFormat(5), { from: PARAMETERS_VOTING });

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT, ROOT2, ROOT3]);
    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    validatorsSlashingVoting = await makeProxy(registry.address, ValidatorsSlashingVoting);
    validatorsSlashingVoting.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.validators.slashingVoting', validatorsSlashingVoting.address, {
      from: OWNER,
    });

    validatorSlashingEscrow = await makeProxy(registry.address, ValidatorSlashingEscrow);
    validatorSlashingEscrow.initialize(registry.address, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.validators.slashingEscrow', validatorSlashingEscrow.address, { from: OWNER });

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(registry.address, [], []);
    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocationProxy.address, { from: OWNER });

    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    const amount = 100000;
    await validators.commitStake({ from: VALIDATOR, value: amount });
    await validators.enterShortList({ from: VALIDATOR });

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('open()', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://ethereum.com';
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should be possible to open the arbitration', async () => {
      const amount = toBN(1000);
      const slashingVotingBalance = toBN(await web3.eth.getBalance(VALIDATORS_CONTRACT));
      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));

      const result = await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: amount });

      const gasUsed = toBN(result.receipt.gasUsed);
      const slashingVotingBalanceAfterTransaction = toBN(await web3.eth.getBalance(VALIDATORS_CONTRACT));
      const escrowBalanceAfterTransaction = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));

      // For berlin hardfork
      const gasPrice = validatorSlashingEscrow.constructor.class_defaults.gasPrice;
      assert.equal(
        slashingVotingBalance.minus(amount).minus(gasUsed.multipliedBy(gasPrice)).toString(),
        slashingVotingBalanceAfterTransaction.toString()
      );

      // For london hardfork
      // assert.equal(
      //   slashingVotingBalance.minus(amount).minus(gasUsed.multipliedBy(result.receipt.effectiveGasPrice)).toString(),
      //   slashingVotingBalanceAfterTransaction.toString(),
      // );

      assert.equal(escrowBalance.plus(amount).toString(), escrowBalanceAfterTransaction.toString());
      assert.equal(
        (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount.toString(),
        amount.toString()
      );
    });

    it('should not be possible to open the arbitration if arbitration for the proposal already exists', async () => {
      const amount = toBN(100000000);
      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));

      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: amount });

      await truffleAssert.reverts(
        validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: amount }),
        '[QEC-027000]-The arbitration already exists.'
      );
      const escrowBalanceAfterTransaction = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));

      assert.equal(escrowBalance.plus(amount).toString(), escrowBalanceAfterTransaction.toString());
    });

    it('should not be possible to open the arbitration if caller is not a ValidatorsSlashingVoting contract', async () => {
      const amount = toBN(2000);
      await truffleAssert.reverts(
        validatorSlashingEscrow.open(proposalID, { from: ROOT, value: amount }),
        '[QEC-027013]-Permission denied - only the associated contract has access.'
      );
    });
  });

  describe('getStatus()', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://ethereum.com';
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
    });

    it('should return status OPEN if objection period is open', async () => {
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);
      await setTime(objectionEndTime.minus(100).toNumber());

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);
    });

    it('should return status PENDING', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should return status DECIDED', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });
      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.DECIDED);
    });

    it('should return status EXECUTED', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });
      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });
      await validatorSlashingEscrow.execute(proposalID);

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should return status ACCEPTED', async () => {
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);
      await setTime((await getCurrentBlockTime()) + objectionEndTime.plus(valSlashingAppealP + 1).toNumber());

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.ACCEPTED);
    });

    it('should return status NONE', async () => {
      assert.equal((await validatorSlashingEscrow.getStatus(100)).toString(), ArbitrationStatus.NONE);
    });
  });

  describe('getProposal()', () => {
    let proposalID;
    const percentage = getPercentageFormat(60);
    const remark = 'http://remark.com';

    beforeEach('init proposal', async () => {
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should get the proposal', async () => {
      const proposalFromEscrow = await validatorSlashingEscrow.getProposal(proposalID);
      const proposalFromSlashingVoting = await validatorsSlashingVoting.getProposal(proposalID);

      assert.equal(proposalFromEscrow.remark, remark);
      assert.equal(proposalFromEscrow.toString(), proposalFromSlashingVoting.toString());
    });

    it('should not get the non-existing proposal', async () => {
      await truffleAssert.reverts(validatorSlashingEscrow.getProposal(100));
    });
  });

  describe('castObjection()', () => {
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const remark = 'http://remark.com';
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should successfully cast the objection', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 10000 });

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);

      await validatorSlashingEscrow.castObjection(proposalID, newRemark, { from: VALIDATOR });

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), newRemark);
    });

    it('should emit correct event', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 10000 });
      const result = await validatorSlashingEscrow.castObjection(proposalID, newRemark, { from: VALIDATOR });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'RemarkUpdated');
      assert.equal(result.logs[0].args._id, proposalID);
      assert.equal(result.logs[0].args._remark, newRemark);
    });

    it('should not cast the objection if the caller is not a slashing victim', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 10000 });

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);

      await truffleAssert.reverts(
        validatorSlashingEscrow.castObjection(proposalID, newRemark, { from: ROOT }),
        '[QEC-027014]-Permission denied - only the slashing target has access.'
      );

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.OPEN);
    });

    it('should not cast the objection if the arbitration does not exist', async () => {
      await truffleAssert.reverts(
        validatorSlashingEscrow.castObjection(proposalID, newRemark, { from: VALIDATOR }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should not cast the objection if the arbitration is not open', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 10000 });
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);
      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);

      await setTime((await getCurrentBlockTime()) + objectionEndTime.plus(valSlashingOBJP + 10).toNumber());

      await truffleAssert.reverts(
        validatorSlashingEscrow.castObjection(proposalID, newRemark, { from: VALIDATOR }),
        '[QEC-027001]-The given arbitration is not open, objection failed.'
      );
    });
  });

  describe('setRemark()', () => {
    const remark = 'http://remark.com';
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should successfully set the remark', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, remark, { from: VALIDATOR });
      await validatorSlashingEscrow.setRemark(proposalID, newRemark, { from: VALIDATOR });

      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), newRemark);
    });

    it('should emit correct event', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, remark, { from: VALIDATOR });
      const result = await validatorSlashingEscrow.setRemark(proposalID, newRemark, { from: VALIDATOR });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'RemarkUpdated');
      assert.equal(result.logs[0].args._id, proposalID);
      assert.equal(result.logs[0].args._remark, newRemark);
    });

    it('should not set the remark if the caller is not a slashing victim', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, remark, { from: VALIDATOR });

      await truffleAssert.reverts(
        validatorSlashingEscrow.setRemark(proposalID, newRemark, { from: ROOT }),
        '[QEC-027014]-Permission denied - only the slashing target has access.'
      );

      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), remark);
    });

    it('should not set the remark if the arbitration does not exist', async () => {
      await truffleAssert.reverts(
        validatorSlashingEscrow.setRemark(proposalID, newRemark, { from: VALIDATOR }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should not set the remark if the arbitration is not pending', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });

      await truffleAssert.reverts(
        validatorSlashingEscrow.setRemark(proposalID, newRemark, { from: VALIDATOR }),
        '[QEC-027018]-The given arbitration is not pending.'
      );

      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).remark.toString(), '');
    });
  });

  describe('setProposerRemark()', () => {
    const remark = 'http://remark.com';
    const newRemark = 'new remark';
    let proposalID;

    beforeEach('init proposal', async () => {
      const percentage = getPercentageFormat(60);
      const result = await validatorsSlashingVoting.createProposal(remark, VALIDATOR, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should successfully confirm the appeal', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, remark, { from: VALIDATOR });
      const txReceipt = await validatorSlashingEscrow.setProposerRemark(proposalID, newRemark, true, { from: ROOT });

      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).proposerRemark, newRemark);
      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).remark, remark);
      assert.equal((await validatorSlashingEscrow.arbitrationInfos(proposalID)).appealConfirmed, true);

      assert.equal(txReceipt.receipt.logs[0].event, 'ProposerRemarkUpdated');
      assert.equal(txReceipt.receipt.logs[0].args._id, proposalID);
      assert.equal(txReceipt.receipt.logs[0].args._proposerRemark, newRemark);
      assert.equal(txReceipt.receipt.logs[0].args._appealConfirmed, true);
    });

    it('should not set the remark if the arbitration is not pending', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });

      await truffleAssert.reverts(
        validatorSlashingEscrow.setProposerRemark(proposalID, newRemark, false, { from: ROOT }),
        '[QEC-027018]-The given arbitration is not pending.'
      );
    });

    it('should get exception if caller does not have access', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, remark, { from: VALIDATOR });

      await truffleAssert.reverts(
        validatorSlashingEscrow.setProposerRemark(proposalID, newRemark, false, { from: VALIDATOR }),
        '[QEC-027020]-Permission denied - only the original proposer has access.'
      );
    });
  });

  describe('proposeDecision()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      const result = await validatorsSlashingVoting.createProposal('remark', VALIDATOR, getPercentageFormat(60), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
    });

    it('should be possible to propose the decision', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      const reference = 'https://decision.com';
      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      await validatorSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, reference, { from: ROOT });
      const decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), reference);
    });

    it('should not be possible to propose the decision if the arbitration is not pending', async () => {
      const percentage = getPercentageFormat(75);

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, percentage, false, '1', { from: ROOT }),
        '[QEC-027018]-The given arbitration is not pending.'
      );
    });

    it('should not be possible to propose the decision if current decision period is not over', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      const notAppealed = false;
      const percentage = getPercentageFormat(80);
      await validatorSlashingEscrow.proposeDecision(proposalID, percentage, false, '1', { from: ROOT });

      let decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '1');

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, percentage, false, '1', { from: ROOT2 }),
        '[QEC-027002]-The current decision period is not over.'
      );

      decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '1');
    });

    it('should not be possible to propose the decision if caller has already proposed a decision', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      await validatorSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, '1', { from: ROOT });
      let decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '1');

      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);
      await setTime(toBN(arbitrationInfo.decision.endDate).plus(1).toNumber());

      const newPercentage = getPercentageFormat(20);
      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, newPercentage, false, '1', { from: ROOT }),
        '[QEC-027003]-The caller has already proposed the previous decision.'
      );

      decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;

      assert.equal(toBN(decision.percentage).toString(), percentage.toString());
      assert.equal(decision.proposer.toString(), ROOT.toString());
      assert.equal(decision.notAppealed.toString(), notAppealed.toString());
      assert.equal(decision.externalReference.toString(), '1');
    });

    it('should not be possible to propose the decision if percentage value is invalid', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      const percentage = getPercentageFormat(100000);

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, percentage, false, '1', { from: ROOT }),
        '[QEC-027004]-Invalid percentage value, failed to propose a decision.'
      );
    });

    it('should not be possible to propose the decision if appeal period is not over and _notAppealed == true', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      const percentage = getPercentageFormat(40);

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, percentage, true, '1', { from: ROOT }),
        '[QEC-027005]-The appeal period is not over.'
      );
    });

    it('should not be possible to propose the decision if _notAppealed == true and percentage value is not equals 100%', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);

      await setTime(toBN(arbitrationInfo.params.appealEndTime).plus(1).toNumber());

      const percentage = getPercentageFormat(50);

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, percentage, true, '1', { from: ROOT }),
        '[QEC-027006]-If the no appeal was submitted, the slashing percentage should be 100%.'
      );
    });

    it('should not be possible to propose the decision if caller is not a root node', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      await truffleAssert.reverts(
        validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, 'https://decision.com', {
          from: VALIDATOR,
        }),
        '[QEC-027015]-Permission denied - only the root node has access.'
      );
    });
  });

  describe('recallProposedDecision()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      const result = await validatorsSlashingVoting.createProposal('remark', VALIDATOR, getPercentageFormat(60), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
    });

    it('should be possible to recall proposed decision', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(
        proposalID,
        getPercentageFormat(10),
        false,
        'https://decision.com',
        { from: ROOT }
      );
      await validatorSlashingEscrow.recallProposedDecision(proposalID, { from: ROOT });
    });

    it('should not be possible to recall proposed decision if no decision period is open', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(
        proposalID,
        getPercentageFormat(10),
        false,
        'https://decision.com',
        { from: ROOT }
      );
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);

      await setTime(toBN(arbitrationInfo.decision.endDate).plus(1).toNumber());

      await truffleAssert.reverts(
        validatorSlashingEscrow.recallProposedDecision(proposalID, { from: ROOT }),
        '[QEC-027019]-The arbitration is not open for decision.'
      );
    });

    it('should not be possible to recall proposed decision if caller is not decision proposer', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(
        proposalID,
        getPercentageFormat(10),
        false,
        'https://decision.com',
        { from: ROOT }
      );

      await truffleAssert.reverts(
        validatorSlashingEscrow.recallProposedDecision(proposalID, { from: VALIDATOR }),
        '[QEC-027016]-Permission denied - only the decision proposer has access.'
      );
    });
  });

  describe('confirmDecision()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      const result = await validatorsSlashingVoting.createProposal('remark', VALIDATOR, getPercentageFormat(60), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should be possible to confirm a decision', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      const result = await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ArbitrationDecided');
      assert.equal(result.logs[0].args._id, proposalID);

      const decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 2);
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.DECIDED);
    });

    it('should not be possible to confirm a decision if caller is not a root node', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: VALIDATOR }),
        '[QEC-027015]-Permission denied - only the root node has access.'
      );

      const decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 1);
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should not be possible to confirm a decision if no decision period is open', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(proposalID, '0x123', { from: ROOT }),
        '[QEC-027019]-The arbitration is not open for decision.'
      );
    });

    it('should not be possible to confirm a decision if decision has already decided', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT3 }),
        '[QEC-027007]-This arbitration is already decided.'
      );
    });

    it('should not be possible to confirm a decision if caller has already confirmed', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT }),
        '[QEC-027008]-The caller has already confirmed.'
      );

      const decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 1);
    });

    it('should not be possible to confirm a decision if arbitration does not exist', async () => {
      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(100, '0x123', { from: ROOT }),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });

    it('should be possible to confirm subsequent decisions, if previous decisions expired', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });

      // setup first decision
      const percentage = getPercentageFormat(10);
      const notAppealed = false;
      await validatorSlashingEscrow.proposeDecision(proposalID, percentage, notAppealed, '1', { from: ROOT });

      // let it expire
      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      await setTime(toBN(arbitrationInfo.decision.endDate).plus(1).toNumber());

      // make a new proposal
      const newPercentage = getPercentageFormat(20);
      await validatorSlashingEscrow.proposeDecision(proposalID, newPercentage, false, '1', { from: ROOT2 });
      let decision = (await validatorSlashingEscrow.arbitrationInfos(proposalID)).decision;
      assert.equal(decision.confirmationCount, 1);

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      // confirm again with root node 1
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT });
      decision = (await validatorSlashingEscrow.arbitrationInfos(proposalID)).decision;
      assert.equal(decision.confirmationCount, 2, 'root node one must be able to confirm new decison');
    });

    it('should failed by incorrect hash', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const hash = '0x123';
      await truffleAssert.reverts(
        validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 }),
        '[QEC-027021]-'
      );
    });
  });

  describe('execute()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      const result = await validatorsSlashingVoting.createProposal('remark', VALIDATOR, getPercentageFormat(60), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
    });

    it('should be possible to execute slashing', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });
      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await validatorSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN(
        (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount
      );

      let expectedRestAmount = amountToSlash.multipliedBy(0.1);
      const expectedAmountToReturn = amountToSlash.minus(expectedRestAmount);
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.plus(expectedAmountToReturn).toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('case with the 100%', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(100), false, '1', { from: ROOT });
      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await validatorSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN(
        (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount
      );

      let expectedRestAmount = amountToSlash;
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('case with 0%', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(0), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await validatorSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN(
        (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).params.slashedAmount
      );

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.plus(amountToSlash).toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should be possible to execute slashing if the arbitration status is ACCEPTED', async () => {
      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const arbitrationInfo = await validatorSlashingEscrow.arbitrationInfos.call(proposalID);

      const objectionEndTime = toBN(arbitrationInfo.params.objectionEndTime);
      await setTime(objectionEndTime.plus(valSlashingVP + 1).toNumber());

      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.ACCEPTED);
      await validatorSlashingEscrow.execute(proposalID);

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      const amountToSlash = toBN(arbitrationInfo.params.slashedAmount);

      let expectedRestAmount = amountToSlash;
      const expectedProposerReward = expectedRestAmount.multipliedBy(0.05);
      expectedRestAmount = expectedRestAmount.minus(expectedProposerReward);

      assert.equal(escrowBalance.minus(amountToSlash).toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.plus(expectedProposerReward).toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(
        allocationProxyBalance.plus(expectedRestAmount).toString(),
        allocationProxyBalanceAfterSlashing.toString()
      );
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.EXECUTED);
    });

    it('should not be possible to execute slashing if there is no decided decisions', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalance = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalance = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await truffleAssert.reverts(
        validatorSlashingEscrow.execute(proposalID),
        '[QEC-027009]-The arbitration is still undecided. decision.'
      );

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const victimBalanceAfterSlashing = toBN(await web3.eth.getBalance(VALIDATOR));
      const proposerBalanceAfterSlashing = toBN(await web3.eth.getBalance(ROOT));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      assert.equal(escrowBalance.toString(), escrowBalanceAfterSlashing.toString());
      assert.equal(victimBalance.toString(), victimBalanceAfterSlashing.toString());
      assert.equal(proposerBalance.toString(), proposerBalanceAfterSlashing.toString());
      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.PENDING);
    });

    it('should not be possible to execute slashing if arbitration does not exist', async () => {
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const escrowBalance = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const allocationProxyBalance = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      await truffleAssert.reverts(validatorSlashingEscrow.execute(100), '[QEC-027017]-The arbitration does not exist.');

      const escrowBalanceAfterSlashing = toBN(await web3.eth.getBalance(validatorSlashingEscrow.address));
      const allocationProxyBalanceAfterSlashing = toBN(await web3.eth.getBalance(defaultAllocationProxy.address));

      assert.equal(allocationProxyBalance.toString(), allocationProxyBalanceAfterSlashing.toString());
      assert.equal(escrowBalance.toString(), escrowBalanceAfterSlashing.toString());
    });
  });

  describe('getDecisionStats()', () => {
    let proposalID;

    beforeEach('initialization', async () => {
      const result = await validatorsSlashingVoting.createProposal('remark', VALIDATOR, getPercentageFormat(60), {
        from: ROOT,
      });
      proposalID = Number(result.logs[0].args._id);
      await registry.setAddress('governance.validators', VALIDATORS_CONTRACT, { from: OWNER });
    });

    it('should be possible to get the decision stats', async () => {
      await validatorSlashingEscrow.open(proposalID, { from: VALIDATORS_CONTRACT, value: 1000 });
      await validatorSlashingEscrow.castObjection(proposalID, 'remark', { from: VALIDATOR });
      await validatorSlashingEscrow.proposeDecision(proposalID, getPercentageFormat(10), false, '1', { from: ROOT });

      const info = await validatorSlashingEscrow.arbitrationInfos(proposalID);
      const hash = info.decision.hash;
      await validatorSlashingEscrow.confirmDecision(proposalID, hash, { from: ROOT2 });

      const decision = (await validatorSlashingEscrow.arbitrationInfos.call(proposalID)).decision;
      assert.equal(decision.confirmationCount, 2);
      assert.equal((await validatorSlashingEscrow.getStatus(proposalID)).toString(), ArbitrationStatus.DECIDED);

      const stats = await validatorSlashingEscrow.getDecisionStats(proposalID);

      const expectedPercentage = 666666666666666666666666666;
      assert.equal(stats.confirmationCount, 2);
      assert.equal(stats.requiredConfirmations, 2);
      assert.equal(stats.currentConfirmationPercentage, expectedPercentage);
    });

    it('should not be possible to get the decision stats if arbitration does not exist', async () => {
      await truffleAssert.reverts(
        validatorSlashingEscrow.getDecisionStats(100),
        '[QEC-027017]-The arbitration does not exist.'
      );
    });
  });
});
