const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, calculatePercentage, getPercentageFormat } = require('./helpers/defiHelper');
const { getVetoTimeHelper, getVoteTimeHelper } = require('./helpers/votingHelper');

const { setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ParametersVoting = artifacts.require('EPDR_ParametersVotingMock');
const Experts = artifacts.require('EPDR_Membership');
const Parameters = artifacts.require('EPDR_Parameters');
const ContractRegistry = artifacts.require('ContractRegistry');
const Roots = artifacts.require('Roots');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = {
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
};

const ParameterType = {
  NONE: 0,
  ADDRESS: 1,
  UINT: 2,
  STRING: 3,
  BYTE32: 4,
  BOOL: 5,
};

const number = getPercentageFormat(50); // it's 50% from 10 ** 27

describe('EPDR_ParametersVoting', () => {
  const reverter = new Reverter();

  let paramVoting;
  let experts;
  let constitution;
  let expertsParameters;
  let registry;
  let roots;

  const proposalExecutionP = 2592000;

  let OWNER;
  let USER1;
  let USER2;
  let USER3;
  let NOT_EXPERT;

  let EXPERTS_VOTING;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(2);
    USER3 = await accounts(6);
    NOT_EXPERT = await accounts(3);

    EXPERTS_VOTING = await accounts(4);
    PARAMETERS_VOTING = await accounts(5);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.experts.EPDR.parametersVoting'], [PARAMETERS_VOTING]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    const paramsName = [
      'constitution.EPDR.maxNExperts',
      'constitution.voting.EPDR.changeParamVP',
      'constitution.voting.EPDR.changeParamRNVALP',
      'constitution.voting.EPDR.changeParamRMAJ',
      'constitution.voting.EPDR.changeParamQRM',
    ];

    const paramsValue = [100, 100, 100, number, 0];
    constitution = await makeProxy(registry.address, Parameters);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    for (let i = 0; i < paramsName.length; ++i) {
      await constitution.setUint(paramsName[i], paramsValue[i], { from: PARAMETERS_VOTING });
    }
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    const validAssets = ['QUSD', 'QBTC', 'QETH', 'QBTC:QUSD', 'QETH:QUSD'];
    const assetValue = [true, true, true, true, true];

    expertsParameters = await makeProxy(registry.address, Parameters);
    await expertsParameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    for (let i = 0; i < validAssets.length; ++i) {
      await expertsParameters.setBool(validAssets[i], assetValue[i], { from: PARAMETERS_VOTING });
    }
    await registry.setAddress('governance.experts.EPDR.parameters', expertsParameters.address);

    experts = await makeProxy(registry.address, Experts);
    await experts.initialize(registry.address, []);
    await registry.setAddress('governance.experts.EPDR.membership', experts.address);
    await registry.setAddress('governance.experts.EPDR.membershipVoting', EXPERTS_VOTING);
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await experts.addMember(OWNER, { from: EXPERTS_VOTING });
    await experts.addMember(USER1, { from: EXPERTS_VOTING });
    await experts.addMember(USER2, { from: EXPERTS_VOTING });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [OWNER, USER1]);
    await registry.setAddress('governance.rootNodes', roots.address);

    paramVoting = await makeProxy(registry.address, ParametersVoting);
    await paramVoting.initialize(registry.address);

    await registry.setAddress('governance.experts.EPDR.parametersVoting', paramVoting.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createAddrProposal', () => {
    it('should be possible to create address proposal', async () => {
      const result = await paramVoting.createAddrProposal('test', 'test', OWNER);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const param = await paramVoting.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.ADDRESS);
      assert.equal(param.addrValue, OWNER);

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');
      assert.equal((await paramVoting.proposals(0)).parametersSize, 1);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });
  });

  describe('createBoolProposal', () => {
    it('should be possible to create bool proposal', async () => {
      const result = await paramVoting.createBoolProposal('test', 'test', true);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const param = await paramVoting.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.BOOL);
      assert.equal(param.boolValue, true);

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');
      assert.equal((await paramVoting.proposals(0)).parametersSize, 1);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });
  });

  describe('createStrProposal', () => {
    it('should be possible to create string proposal', async () => {
      const result = await paramVoting.createStrProposal('test', 'test', 'test');

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const param = await paramVoting.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.STRING);
      assert.equal(param.strValue, 'test');

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');
      assert.equal((await paramVoting.proposals(0)).parametersSize, 1);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });
  });

  describe('createBytesProposal', () => {
    it('should be possible to create bytes proposal', async () => {
      const value = web3.utils.fromAscii('test');
      const result = await paramVoting.createBytesProposal('test', 'test', value);
      const param1 = await paramVoting.createBytes32Param('test', value);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      await assert.deepEqual(param1, await paramVoting.getParam(0, 0));

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');
      assert.equal((await paramVoting.proposals(0)).parametersSize, 1);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });
  });

  describe('createUintProposal', () => {
    it('should be possible to create uint proposal', async () => {
      const result = await paramVoting.createUintProposal('test', 'test', 100);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const param = await paramVoting.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.UINT);
      assert.equal(param.uintValue, 100);

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');
      assert.equal((await paramVoting.proposals(0)).parametersSize, 1);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });
  });

  describe('createProposal', () => {
    it('should successfully create the proposal with parameters of all types', async () => {
      const param1 = await paramVoting.createAddrParam('addr', OWNER);
      const param2 = await paramVoting.createUintParam('uint', 100);
      const param3 = await paramVoting.createStrParam('str', 'str');
      const param4 = await paramVoting.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await paramVoting.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      const result = await paramVoting.createProposal('test', params);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await paramVoting.getParam(0, i), params[i]);
      }

      assert.equal((await paramVoting.proposals(0)).parametersSize, paramCount);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should successfully create the proposal with many parameters of the same type', async () => {
      const param1 = await paramVoting.createStrParam('one', '1');
      const param2 = await paramVoting.createStrParam('two', '2');
      const param3 = await paramVoting.createStrParam('three', '3');
      const param4 = await paramVoting.createStrParam('four', '4');
      const param5 = await paramVoting.createStrParam('five', '5');
      const params = [param1, param2, param3, param4, param5];

      const result = await paramVoting.createProposal('test', params);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await paramVoting.getParam(0, i), params[i]);
      }

      assert.equal((await paramVoting.proposals(0)).parametersSize, paramCount);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should successfully create the proposal with a different number of parameters of different types', async () => {
      const param1 = await paramVoting.createAddrParam('addr', OWNER);
      const param2 = await paramVoting.createAddrParam('addr2', USER1);
      const param3 = await paramVoting.createAddrParam('addr3', USER2);
      const param4 = await paramVoting.createUintParam('uint', 100);
      const param5 = await paramVoting.createUintParam('uint2', 666);
      const param6 = await paramVoting.createStrParam('str', 'str');
      const param7 = await paramVoting.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param8 = await paramVoting.createBoolParam('true', true);
      const param9 = await paramVoting.createBoolParam('false', false);

      const params = [param1, param2, param3, param4, param5, param6, param7, param8, param9];

      const result = await paramVoting.createProposal('test', params);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      const paramCount = 9;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await paramVoting.getParam(0, i), params[i]);
      }

      assert.equal((await paramVoting.proposals(0)).parametersSize, paramCount);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should get exception, attempt to call a method without passing keys to it', async () => {
      await truffleAssert.reverts(
        paramVoting.createProposal('test', []),
        '[QEC-010016]-Proposal must contain at least one expert parameter.'
      );

      assert.equal((await paramVoting.proposals(0)).parametersSize, 0);
    });
  });

  describe('vote', () => {
    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      const result = await paramVoting.voteFor(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should not be possible to vote for not pending proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(1),
        '[QEC-010012]-Voting is only possible on PENDING proposals.'
      );

      assert.equal((await paramVoting.proposals(1)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(1)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await paramVoting.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote if already voted', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteAgainst(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0),
        '[QEC-010013]-The caller has already voted for the proposal.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));
    });

    it('should not be possible to vote if sender not expert', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0, { from: NOT_EXPERT }),
        '[QEC-010000]-Permission denied - only experts have access.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
    });
  });

  describe('execute', () => {
    it('should be possible to execute passed proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 111);

      await paramVoting.voteFor(0);

      const vetoEndTime = await await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PASSED);

      const txReceipt = await paramVoting.execute(0);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, 0);

      assert.equal((await expertsParameters.getUint('test')).toString(), 111);
    });

    it('should set correct parameters of different types', async () => {
      const param1 = await paramVoting.createAddrParam('addr', OWNER);
      const param2 = await paramVoting.createUintParam('uint', 100);
      const param3 = await paramVoting.createStrParam('str', 'str');
      const param4 = await paramVoting.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await paramVoting.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      await paramVoting.createProposal('test', params);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await paramVoting.getParam(0, i), params[i]);
      }

      assert.equal((await paramVoting.proposals(0)).parametersSize, paramCount);

      await paramVoting.voteFor(0);
      const vetoEndTime = await await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PASSED);

      const result = await paramVoting.execute(0);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalExecuted');
      assert.equal(result.logs[0].args._proposalId, 0);

      assert.equal((await expertsParameters.getAddr('addr')).toString(), OWNER);
      assert.equal((await expertsParameters.getUint('uint')).toString(), 100);
      assert.equal((await expertsParameters.getString('str')).toString(), 'str');
      assert.equal(web3.utils.toAscii(await expertsParameters.getBytes32('bytes')).replace(/\0/g, ''), 'bytes');
      assert.equal(await expertsParameters.getBool('true'), true);
    });

    it('should set correct parameters of the same type', async () => {
      const param1 = await paramVoting.createStrParam('1', '1');
      const param2 = await paramVoting.createStrParam('2', '2');
      const param3 = await paramVoting.createStrParam('3', '3');
      const param4 = await paramVoting.createStrParam('4', '4');
      const param5 = await paramVoting.createStrParam('5', '5');
      const params = [param1, param2, param3, param4, param5];

      await paramVoting.createProposal('test', params);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await paramVoting.getParam(0, i), params[i]);
      }

      assert.equal((await paramVoting.proposals(0)).parametersSize, paramCount);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);

      await paramVoting.voteFor(0);

      const vetoEndTime = await await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PASSED);

      const result = await paramVoting.execute(0);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalExecuted');
      assert.equal(result.logs[0].args._proposalId, 0);

      for (let i = 1; i <= paramCount; i++) {
        assert.equal(await expertsParameters.getString(i.toString()), i);
      }
    });

    it('should not be possible to execute expired proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 111);

      await paramVoting.voteFor(0);
      const vetoEndTime = await await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PASSED);

      await setTime(vetoEndTime + proposalExecutionP + 1);
      await truffleAssert.reverts(paramVoting.execute(0), '[QEC-010010]-Proposal must be PASSED before excecuting.');

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should not be possible to execute not passed proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 111);

      await paramVoting.voteFor(0);

      await truffleAssert.reverts(paramVoting.execute(0), '[QEC-010010]-Proposal must be PASSED before excecuting.');
    });
  });

  describe('getParametersArr', () => {
    it('should return correct parameters array', async () => {
      const param1 = await paramVoting.createAddrParam('addr', OWNER);
      const param2 = await paramVoting.createUintParam('uint', 100);
      const param3 = await paramVoting.createStrParam('str', 'str');
      const param4 = await paramVoting.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await paramVoting.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      await paramVoting.createProposal('test', params);

      assert.deepEqual(await paramVoting.getParametersArr(0), params);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(paramVoting.getParametersArr(1), '[QEC-010002]-The proposal does not exist.');
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const prop = await paramVoting.createUintProposal('test', 'test', 111);
      const propID = Number(prop.logs[0].args._id);
      const propBase = (await paramVoting.proposals.call(propID)).base;

      const expectedBase = await paramVoting.getProposal(propID);

      assert.equal(expectedBase.remark, propBase.remark);
      assert.equal(expectedBase.executed, propBase.executed);
      assert.equal(expectedBase.params.votingEndTime, propBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(paramVoting.getProposal(1), '[QEC-010002]-The proposal does not exist.');
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      await experts.addMember(USER3, { from: EXPERTS_VOTING });
      const result = await paramVoting.createUintProposal('test', 'test', 111);
      const propID = Number(result.logs[0].args._id);

      await paramVoting.voteFor(0);
      await paramVoting.voteFor(0, { from: USER2 });
      await paramVoting.voteAgainst(0, { from: USER3 });

      const voteEndTime = await getVoteTimeHelper(paramVoting, 0);
      await setTime(voteEndTime);

      await paramVoting.veto(0, { from: USER1 });

      const propStats = await paramVoting.getProposalStats(propID);
      const prop = await paramVoting.proposals.call(propID);

      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);
      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);

      const totalVotes = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentQuorum = calculatePercentage(totalVotes, await experts.getSize());
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalVotes);

      assert.equal(await currentQuorum.toString(), toBN(propStats.currentQuorum).toString());
      assert.equal(await currentMajority.toFixed(0, 1), toBN(propStats.currentMajority).toFixed(0, 1));

      const vetoesCount = prop.base.counters.vetosCount;
      assert.equal(vetoesCount, 1);

      const rootsCount = 2;

      const expectedVetoPercentage = await toBN(vetoesCount)
        .multipliedBy(10 ** 27)
        .dividedBy(rootsCount);
      assert.equal(propStats.currentVetoPercentage, await expectedVetoPercentage.toNumber());
    });

    it('should return correct voting stats structure before the first vote', async () => {
      const result = await paramVoting.createUintProposal('test', 'test', 111);
      const propID = Number(result.logs[0].args._id);

      const propStats = await paramVoting.getProposalStats(propID);
      const prop = await paramVoting.proposals.call(propID);

      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);
      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);

      const totalVotes = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentQuorum = calculatePercentage(totalVotes, await experts.getSize());
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalVotes);

      assert.equal(await currentQuorum.toString(), toBN(propStats.currentQuorum).toString());
      assert.equal(await currentMajority.toString(), toBN(propStats.currentMajority).toString());

      assert.equal(propStats.currentVetoPercentage, 0);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(paramVoting.getProposalStats(1), '[QEC-010002]-The proposal does not exist.');
    });
  });

  describe('expertPanels.QFI.QQUSDconversionRate', () => {
    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.QQUSDconversionRate', 100);

      const result = await paramVoting.voteFor(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should not be possible to vote for not pending proposal', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.QQUSDconversionRate', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(1),
        '[QEC-010012]-Voting is only possible on PENDING proposals.'
      );

      assert.equal((await paramVoting.proposals(1)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(1)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await paramVoting.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote if already voted', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.QQUSDconversionRate', 100);

      await paramVoting.voteAgainst(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0),
        '[QEC-010013]-The caller has already voted for the proposal.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));
    });

    it('should not be possible to vote if sender not expert', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.QQUSDconversionRate', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0, { from: NOT_EXPERT }),
        '[QEC-010000]-Permission denied - only experts have access.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
    });
  });

  describe('expertPanels.QFI.transactionFee', () => {
    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.transactionFee', 100);

      const result = await paramVoting.voteFor(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should not be possible to vote for not pending proposal', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.transactionFee', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(1),
        '[QEC-010012]-Voting is only possible on PENDING proposals.'
      );

      assert.equal((await paramVoting.proposals(1)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(1)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await paramVoting.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote if already voted', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.transactionFee', 100);

      await paramVoting.voteAgainst(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0),
        '[QEC-010013]-The caller has already voted for the proposal.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));
    });

    it('should not be possible to vote if sender not expert', async () => {
      await paramVoting.createUintProposal('test', 'expertPanels.QFI.transactionFee', 100);

      await truffleAssert.reverts(
        paramVoting.voteAgainst(0, { from: NOT_EXPERT }),
        '[QEC-010000]-Permission denied - only experts have access.'
      );

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
    });
  });

  describe('veto()', () => {
    const proposalID = 0;

    beforeEach(async () => {
      await paramVoting.createUintProposal('test', 'test', 100);
      await paramVoting.voteFor(proposalID);
    });

    it('should veto on proposal', async () => {
      const voteEndTime = await getVoteTimeHelper(paramVoting, proposalID);
      await setTime(voteEndTime);
      await paramVoting.veto(proposalID);

      const proposal = await paramVoting.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 1);
      assert.equal((await paramVoting.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);

      assert.isTrue(await paramVoting.hasRootVetoed(proposalID, OWNER));
    });

    it('should get exception, sender already put veto on this proposal', async () => {
      const reason = '[QEC-010014]-The caller has already vetoed the proposal.';
      const voteEndTime = await getVoteTimeHelper(paramVoting, proposalID);
      await setTime(voteEndTime);
      await paramVoting.veto(proposalID);
      await truffleAssert.reverts(paramVoting.veto(proposalID), reason);
    });

    it('should get exception, invalid proposal status', async () => {
      const reason = '[QEC-010015]-Proposal must be ACCEPTED before casting a root node veto.';
      await paramVoting.createUintProposal('test', 'test', 100);
      await truffleAssert.reverts(paramVoting.veto(1), reason);
    });

    it('should get rejected status, all roots vetoed proposal', async () => {
      const voteEndTime = await getVoteTimeHelper(paramVoting, proposalID);
      await setTime(voteEndTime);
      await paramVoting.veto(proposalID);

      await paramVoting.veto(proposalID, { from: USER1 });

      const proposal = await paramVoting.proposals(proposalID);
      assert.equal((await paramVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
      assert.equal(proposal.base.counters.vetosCount, 2);
    });

    it('should get exception, non exist proposal', async () => {
      const reason = '[QEC-010002]-The proposal does not exist.';
      await truffleAssert.reverts(paramVoting.veto(137), reason);
    });

    it('should get exception, call by non root', async () => {
      const reason = '[QEC-010001]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(paramVoting.veto(proposalID, { from: await accounts(7) }), reason);
    });
  });

  describe('getStatus', () => {
    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteFor(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteAgainst(0, { from: USER1 });

      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 1);

      const voteEndTime = await getVoteTimeHelper(paramVoting, 0);
      await setTime(voteEndTime);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual majority < constitution majority', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const voteEndTime = await getVoteTimeHelper(paramVoting, 0);
      await setTime(voteEndTime);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteFor(0);
      await paramVoting.voteFor(0, { from: USER1 });
      await paramVoting.voteAgainst(0, { from: USER2 });

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 2);

      const vetoEndTime = await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED status on a expired proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteFor(0);
      await paramVoting.voteFor(0, { from: USER1 });
      await paramVoting.voteAgainst(0, { from: USER2 });

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 2);

      const vetoEndTime = await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime + proposalExecutionP + 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a executed proposal', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);

      await paramVoting.voteFor(0);
      await paramVoting.voteAgainst(0, { from: USER1 });
      await paramVoting.voteFor(0, { from: USER2 });

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 2);

      const vetoEndTime = await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);

      await paramVoting.execute(0);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);
    });

    it('should return rejected status, percentage of votes for less than required majority', async () => {
      await paramVoting.createUintProposal('test', 'test', 100);
      await paramVoting.voteFor(0);
      await paramVoting.voteAgainst(0, { from: USER1 });

      const vetoEndTime = await getVetoTimeHelper(paramVoting, 0);
      await setTime(vetoEndTime);

      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });
  });

  // describe('collaterizationRatio', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal(
  //       'test', 'QBTC_QUSD_CollateralRatio', enoughNumber, 'QBTC:QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues,
  //  [enoughNumber.toString(10)]);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys,
  // ['QBTC_QUSD_CollateralRatio']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when constraint is not met', async () => {
  //     await truffleAssert.reverts(
  //       paramVoting.createAssetUintProposal('test', 'QBTC_QUSD_CollateralRatio', number, 'QBTC:QUSD'),
  //       '[QEC-010004]-Invalid QBTC:QUSD collateral ratio, the value must be above 100 percent.',
  //     );
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA:QUSD parameter is not set';
  //     await truffleAssert.reverts(paramVoting.createAssetUintProposal(
  //       'test', 'QBTC_QUSD_CollateralRatio', enoughNumber, 'QAAA:QUSD'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  //     await paramVoting.createAssetUintProposal('test', 'QBTC_QUSD_CollateralRatio', enoughNumber, 'QBTC:QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  // describe('Liquidation Ratio', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal(
  //       'test', 'QETH_QUSD_LiquidationRatio', enoughNumber, 'QETH:QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  // assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues, [enoughNumber.toString(10)]);
  // assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys, ['QETH_QUSD_LiquidationRatio']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when constraint is not met', async () => {
  //     await truffleAssert.reverts(
  //       paramVoting.createAssetUintProposal('test', 'QETH_QUSD_LiquidationRatio', number, 'QETH:QUSD'),
  //       '[QEC-010007]-Invalid QETH:QUSD liquidation ratio, the value must be above 100 percent.',
  //     );
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA:QUSD parameter is not set';
  //     await truffleAssert.reverts(paramVoting.createAssetUintProposal(
  //       'test', 'QAAA_QUSD_LiquidationRatio', enoughNumber, 'QAAA:QUSD'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  // await paramVoting.createAssetUintProposal('test', 'QETH_QUSD_LiquidationRatio', enoughNumber, 'QETH:QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  // describe('stablecoin ceiling', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal('test', 'QUSD_Ceiling', 10**6, 'QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues, ['1000000']);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys, ['QUSD_Ceiling']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA parameter is not set';
  // await truffleAssert.reverts(paramVoting.createAssetUintProposal('test', 'QAAA_Ceiling', 101, 'QAAA'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  //     await paramVoting.createAssetUintProposal('test', 'QUSD_Ceiling', 10**6, 'QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  // describe('Liquidation fee', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal('test', 'QETH_QUSD_LiquidationFee', 50, 'QETH:QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues, ['50']);
  //    assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys, ['QETH_QUSD_LiquidationFee']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA:QUSD parameter is not set';
  //     await truffleAssert.reverts(paramVoting.createAssetUintProposal(
  //       'test', 'QAAA_QUSD_LiquidationFee', 101, 'QAAA:QUSD'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  //     await paramVoting.createAssetUintProposal('test', 'QETH_QUSD_LiquidationFee', 50, 'QETH:QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  // describe('interest rate borrowing', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal('test', 'QUSD_InterestRateBorrowing', 100, 'QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues, ['100']);
  // assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys, ['QUSD_InterestRateBorrowing']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA parameter is not set';
  //     await truffleAssert.reverts(paramVoting.createAssetUintProposal(
  //       'test', 'QAAA_InterestRateBorrowing', 101, 'QAAA'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  //     await paramVoting.createAssetUintProposal('test', 'QUSD_InterestRateBorrowing', 100, 'QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  // describe('interest rate Saving', async () => {
  //   it('should be possible to makeProposal', async () => {
  //     const result = await paramVoting.createAssetUintProposal('test', 'QUSD_InterestRateSaving', 100, 'QUSD');

  //     assert.equal((await paramVoting.proposalCount()).toString(), 1);
  //     assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.assetUintValues, ['100']);
  //     assert.deepEqual((await paramVoting.proposals(0)).parameterDetails.parameterKeys, ['QUSD_InterestRateSaving']);
  //     assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test');

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'ProposalCreated');
  //     assert.equal(result.logs[0].args._proposal.remark, 'test');
  //     assert.equal(result.logs[0].args._id, 0);
  //   });

  //   it('should get exception, when asset/pair is not supported/DNE', async () => {
  //     const reason = 'QAAA parameter is not set';
  //     await truffleAssert.reverts(paramVoting.createAssetUintProposal(
  //       'test', 'QAAA_InterestRateSaving', 101, 'QAAA'), reason);
  //   });

  //   it('should be possible to vote for proposal', async () => {
  //     await paramVoting.createAssetUintProposal('test', 'QUSD_InterestRateSaving', 100, 'QUSD');

  //     const result = await paramVoting.voteFor(0);

  //     assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
  //     assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

  //     assert.equal(result.logs.length, 1);
  //     assert.equal(result.logs[0].event, 'UserVoted');
  //     assert.equal(result.logs[0].args._proposalId, 0);
  //     assert.equal(result.logs[0].args._votingOption, 1);
  //   });
  // });

  describe('oracle address update', () => {
    it('should be possible to makeProposal', async () => {
      const result = await paramVoting.createAddrProposal('test_link', 'COL_oracle_address', NOT_EXPERT);

      assert.equal((await paramVoting.proposalCount()).toString(), 1);
      assert.equal((await paramVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await paramVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const param = await paramVoting.getParam(0, 0);
      assert.equal(param.paramKey, 'COL_oracle_address');
      assert.equal(param.paramType, ParameterType.ADDRESS);
      assert.equal(param.addrValue, NOT_EXPERT);

      assert.equal((await paramVoting.proposals(0)).base.remark.toString(), 'test_link');

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test_link');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should be possible to vote for proposal', async () => {
      await paramVoting.createAddrProposal('test_link', 'COL_oracle_address', NOT_EXPERT);

      const result = await paramVoting.voteFor(0);

      assert.equal((await paramVoting.proposals(0)).base.counters.weightFor.toString(), 1);
      assert.isTrue(await paramVoting.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });
  });
});
