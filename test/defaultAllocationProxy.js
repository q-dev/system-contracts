const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');

const { artifacts } = require('hardhat');
const DefaultAllocationProxy = artifacts.require('./DefaultAllocationProxy');
const ValidationRewardProxy = artifacts.require('./ValidationRewardProxy');

const RootNodeRewardProxy = artifacts.require('./RootNodeRewardProxy');

const Constitution = artifacts.require('./Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');

describe('DefaultAllocationProxy', () => {
  const reverter = new Reverter();

  let defaultAllocationProxy;
  let validationRewardProxy;
  let rootNodeRewardProxy;

  let registry;
  let constitution;

  const Q = require('./helpers/defiHelper').Q;
  const DECIMAL = require('./helpers/defiHelper').DECIMAL;

  let DEFAULT;
  let PARAMETERS_VOTING;

  before(async () => {
    DEFAULT = await accounts(0);
    PARAMETERS_VOTING = await accounts(1);

    registry = await ContractRegistry.new();
    await registry.initialize([DEFAULT], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(
      registry.address,
      ['constitution.rewardShareValidatorNodes', 'constitution.rewardShareRootNodes'],
      [
        // want more than 1.0 in total,
        // to test a general case
        DECIMAL.times(1.5),
        DECIMAL.times(0.5),
      ],
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    validationRewardProxy = await makeProxy(registry.address, ValidationRewardProxy);
    await validationRewardProxy.initialize(registry.address);
    await registry.setAddress('tokeneconomics.validationRewardProxy', validationRewardProxy.address);

    rootNodeRewardProxy = await makeProxy(registry.address, RootNodeRewardProxy);
    await rootNodeRewardProxy.initialize(registry.address);
    await registry.setAddress('tokeneconomics.rootNodeRewardProxy', rootNodeRewardProxy.address);

    defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(
      registry.address,
      ['tokeneconomics.validationRewardProxy', 'tokeneconomics.rootNodeRewardProxy'],
      ['constitution.rewardShareValidatorNodes', 'constitution.rewardShareRootNodes']
    );

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('allocate', () => {
    it('should correctly distribute balance amongst beneficiaries', async () => {
      const amount = Q;
      await defaultAllocationProxy.send(amount);

      const balance = await web3.eth.getBalance(defaultAllocationProxy.address);
      assert.equal(balance, amount, 'unexpected initial balance');

      const resultTx = await defaultAllocationProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(
        (await web3.eth.getBalance(validationRewardProxy.address)).toString(),
        Q.times(0.75).toString() // 1.5/2.0
      );

      assert.equal(
        (await web3.eth.getBalance(rootNodeRewardProxy.address)).toString(),
        Q.times(0.25).toString() // 0.5/2.0
      );
    });
  });
});
