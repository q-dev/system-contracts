const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const assertEqualBN = require('./helpers/assertEqualBN');
const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const BigNumber = require('bignumber.js');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setTime } = require('./helpers/hardhatTimeTraveller');
const { getVoteTimeHelper } = require('./helpers/votingHelper');

const { artifacts } = require('hardhat');
const Validators = artifacts.require('./Validators.sol');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Parameters = artifacts.require('./Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const QHolderRewardPool = artifacts.require('QHolderRewardPool');
const QVault = artifacts.require('QVault');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const ValidatorsSlashingVoting = artifacts.require(`ValidatorsSlashingVoting`);
const ValidatorSlashingEscrow = artifacts.require('./ValidatorSlashingEscrow');
const Roots = artifacts.require(`Roots`);
const DefaultAllocationProxy = artifacts.require('DefaultAllocationProxy');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting');
const ValidationRewardPools = artifacts.require('ValidationRewardPools');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');
const WithdrawAddresses = artifacts.require('WithdrawAddresses');

describe('Validators.sol', () => {
  const reverter = new Reverter();

  const proposalExecutionP = 2592000;
  const votingPeriod = toBN(1000);
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const valWithdrawP = 3000;

  const Q = require('./helpers/defiHelper').Q;
  const DECIMAL = require('./helpers/defiHelper').DECIMAL;

  const CR_UPDATE_MINIMUM_BASE = Q.multipliedBy(1000);

  const hour = toBN(3600);
  const day = toBN(24 * hour);
  const week = toBN(7 * day);
  const month = toBN(30 * day);

  let validators;
  let constitutionParameters;
  let addressStorageStakesSorted;
  let addressStorageStakes;
  let crKeeperFactory;
  let validatorsSlashingVoting;
  let accountsLength;
  let accountsValue;
  let qVault;
  let epqfiParams;
  let qHolderRewardPool;
  let rootNodes;
  let votingWeightProxy;
  let generalUpdateVote;
  let registry;
  let validationRewardPools;
  let withdrawAddresses;

  let remark;
  let percentage;

  const checkInvariant = async (currentLockInfo, userAddress) => {
    const totalLockedAmount = toBN(currentLockInfo.lockedAmount).plus(currentLockInfo.pendingUnlockAmount);
    const validatorStake = toBN(await validators.getSelfStake(userAddress));
    assert.isTrue(validatorStake.gte(totalLockedAmount));
  };

  let CANDIDATE;
  let DEFAULT;
  let PARAMETERS_VOTING;
  let ROOT;
  let ROOT2;
  let VALIDATOR1;
  let VALIDATOR2;
  let VALIDATOR3;
  let VALIDATOR4;

  before(async () => {
    CANDIDATE = await accounts(1);
    DEFAULT = await accounts(2);
    PARAMETERS_VOTING = await accounts(6);
    ROOT = await accounts(3);
    ROOT2 = await accounts(4);
    VALIDATOR1 = await accounts(7);
    VALIDATOR2 = await accounts(8);
    VALIDATOR3 = await accounts(9);
    VALIDATOR4 = CANDIDATE;

    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    const parametersInitialList = [
      'constitution.maxNValidators',
      'constitution.maxNStandbyValidators',
      'constitution.cliqueEpochLength',
    ];
    const parametersValues = [20, 20, 1];

    registry = await ContractRegistry.new();
    await registry.initialize(
      [await accounts(0)],
      ['governance.constitution.parametersVoting', 'governance.experts.EPQFI.parametersVoting'],
      [PARAMETERS_VOTING, PARAMETERS_VOTING]
    );

    constitutionParameters = await makeProxy(registry.address, Parameters);
    await constitutionParameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    for (let i = 0; i < parametersInitialList.length; ++i) {
      await constitutionParameters.setUint(parametersInitialList[i], parametersValues[i], { from: PARAMETERS_VOTING });
    }

    await constitutionParameters.setUint('constitution.voting.valSlashingVP', 86400, { from: PARAMETERS_VOTING });
    const number = getPercentageFormat(60); // it's 60% from 10 ** 27
    await constitutionParameters.setUint('constitution.voting.valSlashingQRM', number, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.voting.valSlashingRMAJ', 51, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.voting.valSlashingOBJP', 86400, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.valSlashingAppealP', 86400, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.valWithdrawP', 10, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.proposalExecutionP', proposalExecutionP, {
      from: PARAMETERS_VOTING,
    }); // month

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    await constitutionParameters.setUint('constitution.voting.changeQnotConstVP', votingPeriod, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, {
      from: PARAMETERS_VOTING,
    });

    await registry.setAddress('governance.constitution.parameters', constitutionParameters.address);

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    rootNodes = await makeProxy(registry.address, Roots);
    await rootNodes.initialize(registry.address, [ROOT, ROOT2]);
    await registry.setAddress('governance.rootNodes', rootNodes.address);

    const defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(registry.address, [], []);
    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocationProxy.address);

    validatorsSlashingVoting = await makeProxy(registry.address, ValidatorsSlashingVoting);
    await validatorsSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingVoting', validatorsSlashingVoting.address);

    const validatorSlashingEscrow = await makeProxy(registry.address, ValidatorSlashingEscrow);
    await validatorSlashingEscrow.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingEscrow', validatorSlashingEscrow.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['governance.rootNodes', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    accountsLength = 3;
    accountsValue = 100000;

    remark = 'https://ethereum.org';
    percentage = getPercentageFormat(60); // it's 60% from 10 ** 27
    withdrawAddresses = await WithdrawAddresses.new();
    await withdrawAddresses.initialize();
    await registry.setAddress('tokeneconomics.withdrawAddresses', withdrawAddresses.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  it('should get empty array from getValidatorShortList() method', async () => {
    const positiveValidatorStake = await validators.getValidatorShortList();
    assert.equal(positiveValidatorStake.length, 0);
  });

  it('should get validator stake with positive balance', async () => {
    for (let i = 1; i <= accountsLength; ++i) {
      await validators.commitStake({ from: await accounts(i), value: accountsValue });
      await validators.enterShortList({ from: await accounts(i) });
    }

    let stake;
    for (let i = 1; i <= accountsLength; ++i) {
      stake = await validators.getAccountableSelfStake(await accounts(i));
      assert.equal(stake, accountsValue);
    }

    const positiveValidatorStake = await validators.getValidatorShortList();

    for (let i = 0; i < positiveValidatorStake.length; ++i) {
      assert.equal(positiveValidatorStake[i].amount > 0, true);
    }
  });

  describe('depositOnBehalfOf() + _onTimeLockedDeposit()', () => {
    const commitStakeAmount = Q.times(10);
    const depositStakeAmount = Q.times(10);
    const slashingAmount = Q.times(15);
    let withdrawalAmount = Q.times(5);

    const maxTimelocksNumber = toBN(10);
    const startReleaseDate = day;
    const endReleaseDate = startReleaseDate.plus(day);

    beforeEach('setup', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(commitStakeAmount, await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount, firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, 0);

      await validators.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
        from: VALIDATOR1,
        value: commitStakeAmount,
      });

      const lockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), lockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount);
    });

    it('should deposit several times', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setTime(firstCurrentTime);

      await validators.depositOnBehalfOf(CANDIDATE, week, month.plus(week), {
        from: VALIDATOR1,
        value: depositStakeAmount,
      });

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(2)), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(2)), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount.times(2));

      const secondCurrentTime = (await getCurrentBlockTime()) + 10;
      await setTime(secondCurrentTime);

      await validators.depositOnBehalfOf(CANDIDATE, month.plus(week), month.times(2).plus(week), {
        from: VALIDATOR1,
        value: depositStakeAmount,
      });

      const thirdLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(3)), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(3)), thirdLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount.times(3));
    });

    it('QDEV-2944 LockInfo Issue. should deposit on behalf of other user', async () => {
      const setupDepositAmount = Q.times(10);
      const depositAmount = setupDepositAmount;
      const releaseStart = toBN(0);
      const releaseEnd = toBN(5);

      await validators.depositOnBehalfOf(VALIDATOR1, releaseStart, releaseEnd, {
        from: VALIDATOR1,
        value: setupDepositAmount,
      });
      assert.equal(setupDepositAmount.toString(), (await validators.getSelfStake(VALIDATOR1)).toString());
      assert.equal(toBN(0).toString(), (await validators.getSelfStake(VALIDATOR2)).toString());

      await validators.depositOnBehalfOf(VALIDATOR2, releaseStart, releaseEnd, {
        from: VALIDATOR1,
        value: depositAmount,
      });
      assert.equal(setupDepositAmount.toString(), (await validators.getSelfStake(VALIDATOR1)).toString());
      assert.equal(depositAmount.toString(), (await validators.getSelfStake(VALIDATOR2)).toString());
    });

    it('should automatically release part of timeLocked amount at specific time', async () => {
      const period = endReleaseDate.minus(startReleaseDate);
      const secondCurrentTime = startReleaseDate.plus(period.div(2)).toNumber();
      await setTime(secondCurrentTime);

      const lockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);

      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), lockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount.div(2).toFixed(0, BigNumber.ROUND_DOWN));
    });

    it('should withdraw amount after timelock is over', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setTime(firstCurrentTime);

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);

      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const secondCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
      await setTime(secondCurrentTime);
      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount),
        await validators.getSelfStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount), secondLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, 0);
    });

    it('should withdraw available amount before timelock is over', async () => {
      const firstCurrentTime = await getCurrentBlockTime();
      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);

      withdrawalAmount = Q.times(10);

      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber();
      await setTime(secondCurrentTime);
      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const totalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
      const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(depositStakeAmount, await validators.getSelfStake(CANDIDATE));
      assertEqualBN(depositStakeAmount, totalLockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount);
    });

    it('should get exception, try to withdraw unavailable (timelock constraint) before timelock is over', async () => {
      const firstCurrentTime = await getCurrentBlockTime();
      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);

      withdrawalAmount = Q.times(15);

      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber() - 100;
      await setTime(secondCurrentTime);
      const reason = '[QEC-005015]-Withdrawal prevented by timelock(s).';
      await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const totalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
      const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, firstCurrentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), totalLockedAmount);
      assertEqualBN(secondMinimumBalance, depositStakeAmount);
    });

    it('should withdraw amount after timelock is over during pendingSlashing', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber();
      await setTime(firstCurrentTime);

      withdrawalAmount = Q.times(5);
      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, DECIMAL.times(0.75), { from: ROOT });
      const proposalID = 0;
      assert.equal(
        (await validators.getAccountableTotalStake(CANDIDATE)).toString(),
        slashingAmount.toString(),
        'unexpected stake after slashing proposal'
      );

      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      const secondCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
      await setTime(secondCurrentTime);

      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount),
        await validators.getSelfStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount).minus(withdrawalAmount), secondLockInfo.lockedAmount);
      assertEqualBN(secondMinimumBalance, 0);

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      const thirdLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const thirdMinimumBalance = await validators.getMinimumBalance(CANDIDATE, await getCurrentBlockTime());
      assertEqualBN(await validators.getSelfStake(CANDIDATE), 0);
      assertEqualBN(thirdLockInfo.lockedAmount, 0);
      assertEqualBN(thirdMinimumBalance, 0);
    });

    it(
      'should get exception, try to withdraw unavailable (pendingSlashing constraint) ' +
        'amount after timelock is over during pendingSlashing',
      async () => {
        const firstCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.toNumber();
        await setTime(firstCurrentTime);

        withdrawalAmount = Q.times(6);
        await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

        await validatorsSlashingVoting.createProposal(remark, CANDIDATE, DECIMAL.times(0.75), { from: ROOT });
        const proposalID = 0;

        await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
        await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

        const secondCurrentTime = (await getCurrentBlockTime()) + endReleaseDate.toNumber();
        await setTime(secondCurrentTime);

        const reason = '[QEC-005009]-The withdrawal is blocked by pending slashing proposals.';
        await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

        const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
        const secondTotalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
        const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), secondTotalLockedAmount);
        assertEqualBN(secondMinimumBalance, 0);

        await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
        await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

        const thirdLockInfo = await validators.getLockInfo({ from: CANDIDATE });
        const thirdMinimumBalance = await validators.getMinimumBalance(CANDIDATE, await getCurrentBlockTime());
        assertEqualBN(await validators.getSelfStake(CANDIDATE), Q.times(5));
        assertEqualBN(thirdLockInfo.lockedAmount, Q.times(5));
        assertEqualBN(thirdMinimumBalance, 0);
      }
    );

    it(
      'should get exception, try to withdraw unavailable (timelock constraint) ' +
        'before after timelock is over during pendingSlashing',
      async () => {
        const firstCurrentTime = (await getCurrentBlockTime()) + votingPeriod.toNumber();
        await setTime(firstCurrentTime);

        withdrawalAmount = Q.times(11);
        await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

        await validatorsSlashingVoting.createProposal(remark, CANDIDATE, DECIMAL.times(0.75), { from: ROOT });
        const proposalID = 0;

        await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
        await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

        const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.minus(hour).toNumber();
        await setTime(secondCurrentTime);

        const reason = '[QEC-005015]-Withdrawal prevented by timelock(s).';
        await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

        const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
        const secondTotalLockedAmount = toBN(secondLockInfo.lockedAmount).plus(secondLockInfo.pendingUnlockAmount);
        const secondMinimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
        assertEqualBN(commitStakeAmount.plus(depositStakeAmount), secondTotalLockedAmount);
        assertEqualBN(secondMinimumBalance, depositStakeAmount);

        const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);
        await setTime((await getCurrentBlockTime()) + voteEndTime);
        await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

        const thirdLockInfo = await validators.getLockInfo({ from: CANDIDATE });
        const thirdMinimumBalance = await validators.getMinimumBalance(CANDIDATE, (await getCurrentBlockTime()) + 2000);
        assertEqualBN(await validators.getSelfStake(CANDIDATE), Q.times(5));
        assertEqualBN(thirdLockInfo.lockedAmount, Q.times(5));
        assertEqualBN(thirdMinimumBalance, 0);
      }
    );

    it('should get exception, try to deposit less than minimum amount', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      const wrongDepositAmount = Q.times(10).minus(1);

      const reason = '[QEC-032002]-Given amount is below deposit minimum.';
      await truffleAssert.reverts(
        validators.depositOnBehalfOf(CANDIDATE, week, month.plus(week), {
          from: VALIDATOR1,
          value: wrongDepositAmount,
        }),
        reason
      );

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const firstMinimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount), firstLockInfo.lockedAmount);
      assertEqualBN(firstMinimumBalance, depositStakeAmount);
    });

    it('should get exception, try to deposit maxTimelocksNumber + 1 times', async () => {
      const currentTime = (await getCurrentBlockTime()) + 1;
      await setTime(currentTime);

      for (let i = 1; i < maxTimelocksNumber; i++) {
        validators.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
          from: VALIDATOR1,
          value: depositStakeAmount,
        });
      }

      const reason = '[QEC-032003]-Failed to deposit amount, too many timelocks.';
      await truffleAssert.reverts(
        validators.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
          from: VALIDATOR1,
          value: depositStakeAmount,
        }),
        reason
      );

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(
        commitStakeAmount.plus(depositStakeAmount.times(maxTimelocksNumber)),
        await validators.getSelfStake(CANDIDATE)
      );
      assertEqualBN(commitStakeAmount.plus(depositStakeAmount.times(maxTimelocksNumber)), firstLockInfo.lockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimelocksNumber));
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });
  });

  describe('enterShortList()', () => {
    it('should successfully add validator to shortlist', async () => {
      const everything = 1000000000000000000;
      await validators.commitStake({ from: CANDIDATE, value: toBN(everything) });

      assert.equal((await validators.getAccountableSelfStake(CANDIDATE)).toString(), everything);

      const result = await validators.enterShortList({ from: CANDIDATE });

      assert.equal((await validators.getAccountableSelfStake(CANDIDATE)).toString(), everything);
      await validators.makeSnapshot();

      const shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], CANDIDATE);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ValidatorEnteredShortList');
      assert.equal(result.logs[0].args._validator.toString(), CANDIDATE.toString());
    });

    it('should replace validator in shortlist', async () => {
      await constitutionParameters.setUint('constitution.maxNValidators', 1, { from: PARAMETERS_VOTING });
      await constitutionParameters.setUint('constitution.maxNStandbyValidators', 0, { from: PARAMETERS_VOTING });
      const collateral = 1000000000000000000;
      await validators.commitStake({ from: CANDIDATE, value: toBN(collateral).dividedBy(2) });
      await validators.commitStake({ from: VALIDATOR1, value: toBN(collateral) });

      await validators.enterShortList({ from: CANDIDATE });

      await validators.makeSnapshot();

      let shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], CANDIDATE);

      const result = await validators.enterShortList({ from: VALIDATOR1 });

      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], VALIDATOR1);

      assert.equal(result.logs.length, 2);
      assert.equal(result.logs[0].event, 'ValidatorDroppedFromShortList');
      assert.equal(result.logs[0].args._validator.toString(), CANDIDATE.toString());

      assert.equal(result.logs[1].event, 'ValidatorEnteredShortList');
      assert.equal(result.logs[1].args._validator.toString(), VALIDATOR1.toString());
    });

    it('QDEV-4936 should revert if there are not enough stakes to enter the short list', async () => {
      await constitutionParameters.setUint('constitution.maxNValidators', 1, { from: PARAMETERS_VOTING });
      await constitutionParameters.setUint('constitution.maxNStandbyValidators', 0, { from: PARAMETERS_VOTING });
      const collateral = 1000000000000000000;
      await validators.commitStake({ from: VALIDATOR1, value: toBN(collateral) });
      await validators.commitStake({ from: VALIDATOR2, value: toBN(collateral).dividedBy(2) });

      await validators.enterShortList({ from: VALIDATOR1 });

      await validators.makeSnapshot();

      let shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], VALIDATOR1);

      await truffleAssert.reverts(
        validators.enterShortList({ from: VALIDATOR2 }),
        '[QEC-005017]-Not enough stake to enter the short list.'
      );

      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], VALIDATOR1);
    });
  });

  describe('zero stake', () => {
    it('enter Short List', async () => {
      await truffleAssert.reverts(
        validators.enterShortList({ from: CANDIDATE }),
        'Invalid stake, failed to add the address to the storage.'
      );

      assert.equal((await validators.getAccountableSelfStake(CANDIDATE)).toString(), 0);
      await validators.makeSnapshot();

      const shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 0);
    });
  });

  describe('broken short list', () => {
    it('commit collateral after enter short list', async () => {
      const everything = 100000000000000000;
      await validators.commitStake({ from: CANDIDATE, value: toBN(everything) });

      assert.equal((await validators.getValidatorTotalStake(CANDIDATE)).toString(), everything);

      await validators.enterShortList({ from: CANDIDATE });

      assert.equal((await validators.getValidatorTotalStake(CANDIDATE)).toString(), everything);
      await validators.makeSnapshot();

      let shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], CANDIDATE);

      await validators.commitStake({ from: CANDIDATE, value: toBN(everything) });

      assert.equal((await validators.getValidatorTotalStake(CANDIDATE)).toString(), everything * 2);
      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], CANDIDATE);

      await truffleAssert.reverts(
        validators.enterShortList({ from: CANDIDATE }),
        '[QEC-005003]-The validator is already on the short list.'
      );

      assert.equal((await validators.getValidatorTotalStake(CANDIDATE)).toString(), everything * 2);
      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 1);
      assert.equal(shortList[0], CANDIDATE);
    });

    it('increase stake of second validator', async () => {
      const val1Stake = 10 * Q;
      const val2Stake = 20 * Q;
      const val3Stake = 30 * Q;
      const val4Stake = 40 * Q;
      const additionalStake = 10 * Q;

      await validators.commitStake({ from: VALIDATOR1, value: toBN(val1Stake) });
      await validators.commitStake({ from: VALIDATOR2, value: toBN(val2Stake) });
      await validators.commitStake({ from: VALIDATOR3, value: toBN(val3Stake) });
      await validators.commitStake({ from: VALIDATOR4, value: toBN(val4Stake) });

      assert.equal((await validators.getValidatorTotalStake(VALIDATOR1)).toString(), val1Stake);
      assert.equal((await validators.getValidatorTotalStake(VALIDATOR2)).toString(), val2Stake);
      assert.equal((await validators.getValidatorTotalStake(VALIDATOR3)).toString(), val3Stake);
      assert.equal((await validators.getValidatorTotalStake(VALIDATOR4)).toString(), val4Stake);

      await validators.enterShortList({ from: VALIDATOR1 });
      await validators.enterShortList({ from: VALIDATOR2 });
      await validators.enterShortList({ from: VALIDATOR3 });
      await validators.enterShortList({ from: VALIDATOR4 });

      await validators.makeSnapshot();

      let shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 4);
      assert.equal(shortList[0], VALIDATOR4);
      assert.equal(shortList[1], VALIDATOR3);
      assert.equal(shortList[2], VALIDATOR2);
      assert.equal(shortList[3], VALIDATOR1);

      // move validator3 to first place
      await validators.commitStake({ from: VALIDATOR3, value: toBN(additionalStake) });

      assert.equal((await validators.getValidatorTotalStake(VALIDATOR3)).toString(), val3Stake + additionalStake);
      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 4);
      assert.equal(shortList[0], VALIDATOR4);
      assert.equal(shortList[1], VALIDATOR3);
      assert.equal(shortList[2], VALIDATOR2);
      assert.equal(shortList[3], VALIDATOR1);
    });
  });

  describe('isInShortList()', () => {
    it('should add validator and return true', async () => {
      const validator = await accounts(3);
      await validators.commitStake({ from: validator, value: accountsValue });
      await validators.enterShortList({ from: validator });
      assert.equal(await validators.isInShortList(validator), true);
    });

    it('should check for exist not validator and return false', async () => {
      const notValidator = await accounts(3);
      assert.equal(await validators.isInShortList(notValidator), false);
    });
  });

  describe('announceWithdrawal()', () => {
    let amount;

    before(async () => {
      amount = 100500;
    });

    beforeEach(async () => {
      const key = 'constitution.valWithdrawP';
      const value = 300500;
      await constitutionParameters.setUint(key, value, { from: PARAMETERS_VOTING });
    });

    it('should announce correct withdraw', async () => {
      await validators.commitStake({ from: CANDIDATE, value: amount });
      await validators.announceWithdrawal(amount, { from: CANDIDATE });

      const withdrawal = await validators.validatorsInfos(CANDIDATE);
      assert.equal(withdrawal.amount, amount);

      const validatorStake = await validators.getAccountableSelfStake(CANDIDATE);
      assert.equal(validatorStake, 0);
    });

    it('should not be lost amount at the second announce', async () => {
      await validators.commitStake({ from: CANDIDATE, value: accountsValue });

      const firstAmount = 60000;
      await validators.announceWithdrawal(firstAmount, { from: CANDIDATE });
      let validatorStake = await validators.getAccountableSelfStake(CANDIDATE);
      assert.equal(validatorStake, accountsValue - firstAmount);

      const secondAmount = 30000;
      await validators.announceWithdrawal(secondAmount, { from: CANDIDATE });
      validatorStake = await validators.getAccountableSelfStake(CANDIDATE);
      assert.equal(validatorStake, accountsValue - secondAmount);
    });

    it('should successfully announce withdrawal zero amount', async () => {
      await validators.commitStake({ from: CANDIDATE, value: accountsValue });

      assert.equal(accountsValue, await validators.getValidatorTotalStake(CANDIDATE));
      const firstAmount = 60000;
      await validators.announceWithdrawal(firstAmount, { from: CANDIDATE });
      let validatorStake = await validators.getAccountableSelfStake(CANDIDATE);
      assert.equal(validatorStake, accountsValue - firstAmount);

      await validators.announceWithdrawal(0, { from: CANDIDATE });
      validatorStake = await validators.getAccountableSelfStake(CANDIDATE);
      assert.equal(validatorStake, accountsValue);
    });

    it('should change shortlist after announce', async () => {
      const val1Stake = 100000;
      const val2Stake = 200000;
      const val3Stake = 300000;

      await validators.commitStake({ from: VALIDATOR1, value: toBN(val1Stake) });
      await validators.commitStake({ from: VALIDATOR2, value: toBN(val2Stake) });
      await validators.commitStake({ from: VALIDATOR3, value: toBN(val3Stake) });

      assert.equal((await validators.getValidatorTotalStake(VALIDATOR1)).toString(), val1Stake);
      assert.equal((await validators.getValidatorTotalStake(VALIDATOR2)).toString(), val2Stake);
      assert.equal((await validators.getValidatorTotalStake(VALIDATOR3)).toString(), val3Stake);

      await validators.enterShortList({ from: VALIDATOR1 });
      await validators.enterShortList({ from: VALIDATOR2 });
      await validators.enterShortList({ from: VALIDATOR3 });

      await validators.makeSnapshot();

      let shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 3);
      assert.equal(shortList[0], VALIDATOR3);
      assert.equal(shortList[1], VALIDATOR2);
      assert.equal(shortList[2], VALIDATOR1);

      const val1Amount = 10000;
      const val2Amount = 150000;
      const val3Amount = 220000;

      await validators.announceWithdrawal(val1Amount, { from: VALIDATOR1 });
      await validators.announceWithdrawal(val2Amount, { from: VALIDATOR2 });
      await validators.announceWithdrawal(val3Amount, { from: VALIDATOR3 });

      assert.equal(await validators.getAccountableSelfStake(VALIDATOR1), val1Stake - val1Amount);
      assert.equal(await validators.getAccountableSelfStake(VALIDATOR2), val2Stake - val2Amount);
      assert.equal(await validators.getAccountableSelfStake(VALIDATOR3), val3Stake - val3Amount);

      await validators.makeSnapshot();

      shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 3);
      assert.equal(shortList[0], VALIDATOR1);
      assert.equal(shortList[1], VALIDATOR3);
      assert.equal(shortList[2], VALIDATOR2);
    });

    it('should be able to re-enter short list after withdraw everything', async () => {
      const COLLATERAL = toBN(Q);
      await validators.commitStake({ from: VALIDATOR1, value: COLLATERAL });
      await validators.enterShortList({ from: VALIDATOR1 });

      await validators.commitStake({ from: VALIDATOR2, value: COLLATERAL });
      await validators.enterShortList({ from: VALIDATOR2 });
      assert.isTrue(await validators.isInShortList(VALIDATOR2), 'did not enter the short list');

      await validators.announceWithdrawal(COLLATERAL, { from: VALIDATOR2 });

      const withdrawal = await validators.validatorsInfos(VALIDATOR2);

      await setTime(toBN(withdrawal.endTime).plus(1).toNumber());
      await validators.withdraw(COLLATERAL, VALIDATOR2, { from: VALIDATOR2 });
      assert.isFalse(await validators.isInShortList(VALIDATOR2), 'in short list after withdrawal');

      await validators.commitStake({ from: VALIDATOR2, value: COLLATERAL });
      await validators.enterShortList({ from: VALIDATOR2 });
    });

    it('should get exception, validator stake not enough', async () => {
      await validators.commitStake({ from: CANDIDATE, value: amount });
      amount += 1;
      const reason = '[QEC-005005]-Announced withdrawal must not exceed current stake.';
      await truffleAssert.reverts(validators.announceWithdrawal(amount, { from: CANDIDATE }), reason);
    });
  });

  describe('getLockInfo', () => {
    const commitStakeAmount = Q.times(100);
    const lockedAmount = Q.times(100);
    const amount = Q.times(40);

    beforeEach('setup', async () => {
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });

      await validators.announceWithdrawal(amount, { from: CANDIDATE });
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should get correct information', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(amount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should get exception, token lock source is not contained in the list', async () => {
      const votingWeightProxy2 = await makeProxy(registry.address, VotingWeightProxy);
      await votingWeightProxy2.initialize(registry.address, [], []);
      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy2.address);

      const reason = '[QEC-028001]-Unknown token lock source.';
      await truffleAssert.reverts(validators.getLockInfo({ from: CANDIDATE }), reason);

      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);
    });
  });

  describe('commitStake() + lock()', () => {
    const commitStakeAmount = Q.times(100);
    const lockedAmount = Q.times(100);

    beforeEach('setup', async () => {
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should commit and lock amount', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(commitStakeAmount.toString(), toBN(await validators.getSelfStake(CANDIDATE)).toString());
      assert.equal(lockedAmount.toString(), toBN(currentLockInfo.lockedAmount).toString());
    });

    it('should increase committed and locked amount', async () => {
      let currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(lockedAmount.toString(), toBN(currentLockInfo.lockedAmount).toString());
      assert.equal(commitStakeAmount.toString(), toBN(await validators.getSelfStake(CANDIDATE)).toString());

      const newCommitStakeAmount = commitStakeAmount.plus(commitStakeAmount);
      const newLockedAmount = lockedAmount.plus(lockedAmount);
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(newLockedAmount.toString(), toBN(currentLockInfo.lockedAmount).toString());
      assert.equal(newCommitStakeAmount.toString(), toBN(await validators.getSelfStake(CANDIDATE)).toString());
    });

    it('should get exception, try to commit and lock zero amount', async () => {
      const reason = '[QEC-005000]-Additional stake must not be zero.';
      await truffleAssert.reverts(validators.commitStake({ from: CANDIDATE, value: 0 }), reason);

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.toString());
    });
  });

  describe('announceWithdrawal() + announceUnlock()', () => {
    const commitStakeAmount = Q.times(100);
    const lockedAmount = Q.times(100);
    const announcedWithdrawalAmount = Q.times(20);

    beforeEach('setup', async () => {
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });
      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), lockedAmount.toString());

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.toString());
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should announce unlock and withdrawal successfully', async () => {
      await validators.announceWithdrawal(announcedWithdrawalAmount, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announcedWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announcedWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should increase pending unlock and withdrawal amount', async () => {
      await validators.announceWithdrawal(announcedWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announcedWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announcedWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      const secondAnnounceWithdrawalAmount = announcedWithdrawalAmount.plus(1);
      await validators.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      const totalAnnounceWithdrawalAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(totalAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should decrease pending unlock and withdrawal amount', async () => {
      await validators.announceWithdrawal(announcedWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announcedWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announcedWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      const secondAnnounceWithdrawalAmount = announcedWithdrawalAmount.minus(1);
      await validators.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      const totalAnnounceWithdrawalAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(totalAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should not change pending unlock and withdrawal amount', async () => {
      await validators.announceWithdrawal(announcedWithdrawalAmount, { from: CANDIDATE });

      let currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announcedWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announcedWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      const secondAnnounceWithdrawalAmount = announcedWithdrawalAmount;
      await validators.announceWithdrawal(secondAnnounceWithdrawalAmount, { from: CANDIDATE });

      currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      const totalAnnounceWithdrawalAmount = secondAnnounceWithdrawalAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(totalAnnounceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should successfully announce unlock and withdrawal zero amount', async () => {
      await validators.announceWithdrawal(0, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    it('should get exception, try to announce unlock and withdraw amount greater than locked amount', async () => {
      const reason = '[QEC-005005]-Announced withdrawal must not exceed current stake.';
      await truffleAssert.reverts(validators.announceWithdrawal(lockedAmount.plus(1), { from: CANDIDATE }), reason);

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });
  });

  describe('withdraw() + unlock()', () => {
    const commitStakeAmount = Q.times(100);
    const lockedAmount = Q.times(100);
    let withdrawalAmount = Q.times(20);
    const announceWithdrawalAmount = Q.times(40);

    beforeEach('setup', async () => {
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });
      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });

      await validators.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should unlock and withdraw successfully', async () => {
      const currTime = votingPeriod.plus(10);
      await setTime((await getCurrentBlockTime()) + currTime.toNumber());
      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount).toString()
      );
      assert.equal(
        commitStakeAmount.minus(withdrawalAmount).toString(),
        toBN(await validators.getSelfStake(CANDIDATE)).toString()
      );

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currTime);
      assert.equal(minimumBalance, 0);
    });

    it('should unlock and withdraw successfully multiple times', async () => {
      await validators.announceWithdrawal(announceWithdrawalAmount.times(2), { from: CANDIDATE });

      let currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount.times(2)).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.times(2).toString());

      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());

      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });
      assert.equal(
        commitStakeAmount.minus(withdrawalAmount).toString(),
        toBN(await validators.getSelfStake(CANDIDATE)).toString()
      );

      await validators.withdraw(withdrawalAmount.times(2), CANDIDATE, { from: CANDIDATE });
      assert.equal(
        commitStakeAmount.minus(withdrawalAmount.times(3)).toString(),
        toBN(await validators.getSelfStake(CANDIDATE)).toString()
      );

      currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const totalUnlockAmount = withdrawalAmount.times(3);
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.times(2).minus(totalUnlockAmount).toString()
      );
    });

    it('should get exception, try to unlock and withdraw before the expiration of the pending unlock time', async () => {
      const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).minus(10).toNumber());
      await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());
    });

    it('should get exception, try to unlock and withdraw zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await setTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await truffleAssert.reverts(validators.withdraw(0, CANDIDATE, { from: CANDIDATE }), reason);

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());
    });

    it('should successfully partial withdraw and then withdraw remainder', async () => {
      await setTime((await getCurrentBlockTime()) + toBN(valWithdrawP).plus(10).toNumber());

      const firstLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(firstLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(firstLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());

      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const secondLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(secondLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(secondLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount).toString()
      );
      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount).toString()
      );

      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const thirdLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(thirdLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(thirdLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount.times(2)).toString()
      );
      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount.times(2)).toString()
      );
    });

    it('should successfully withdraw when available stake >= amount', async () => {
      await setTime((await getCurrentBlockTime()) + toBN(valWithdrawP).plus(10).toNumber());
      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(withdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount).toString()
      );
    });

    it('should successfully withdraw when needed unlock amount > pending unlock amount and unlocking is possible', async () => {
      withdrawalAmount = Q.times(90);
      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(valWithdrawP).plus(10).toNumber());
      await validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), commitStakeAmount.minus(withdrawalAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(withdrawalAmount).toString()
      );
    });

    it('should get exception, attempt to withdraw the amount without announcing the withdrawal', async () => {
      await validators.commitStake({ from: VALIDATOR1, value: commitStakeAmount });
      const reason = '[QEC-005008]-Cannot withdraw more than the announced amount.';
      await truffleAssert.reverts(validators.withdraw(withdrawalAmount, VALIDATOR1, { from: VALIDATOR1 }), reason);

      assert.equal(toBN(await validators.getSelfStake(VALIDATOR1)), commitStakeAmount.toString());
    });

    it('should get exception, try to withdraw amount before the period expires', async () => {
      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      const reason = '[QEC-005007]-Not enough time has elapsed since the announcement of the withdrawal.';
      await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());
    });

    it('should get exception, try to withdraw amount greater than announced', async () => {
      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(valWithdrawP).plus(10).toNumber());

      const reason = '[QEC-005008]-Cannot withdraw more than the announced amount.';
      await truffleAssert.reverts(
        validators.withdraw(withdrawalAmount.plus(1), CANDIDATE, { from: CANDIDATE }),
        reason
      );

      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());
    });

    it('should get exception, try to unlock needed amount before the period expires', async () => {
      withdrawalAmount = Q.times(60);
      await validators.announceWithdrawal(withdrawalAmount, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + toBN(valWithdrawP).plus(10).toNumber());

      await generalUpdateVote.createProposal('some proposal');
      await generalUpdateVote.voteFor(1, { from: CANDIDATE });
      await validators.announceWithdrawal(announceWithdrawalAmount.plus(1), { from: CANDIDATE });

      const reason = '[QEC-005007]-Not enough time has elapsed since the announcement of the withdrawal.';
      await truffleAssert.reverts(validators.withdraw(withdrawalAmount, CANDIDATE, { from: CANDIDATE }), reason);

      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), commitStakeAmount.toString());

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).minus(1).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.plus(1).toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it("should withdraw to withdrawAddresses's account", async () => {
      const reciever = '0x1234123412341234123412341234123412341234';
      await withdrawAddresses.change(reciever, { from: CANDIDATE });
      await withdrawAddresses.finalize(CANDIDATE, reciever);
      assert.equal(await withdrawAddresses.resolve(CANDIDATE), reciever);

      const currTime = (await getCurrentBlockTime()) + votingPeriod.plus(10).toNumber();
      await setTime(currTime);
      await validators.withdraw(100, CANDIDATE, { from: CANDIDATE });
      assert.equal(await web3.eth.getBalance(reciever), 100);

      await truffleAssert.reverts(validators.withdraw(100, VALIDATOR1, { from: CANDIDATE }), '[QEC-005016]');
      assert.equal(await web3.eth.getBalance(reciever), 100);
    });
  });

  describe('getDelegatorsShare', () => {
    it('should have valid default value', async () => {
      await validators.commitStake({ from: CANDIDATE, value: Q });
      const qsv = (await validationRewardPools.getDelegatorsShare(CANDIDATE)).toNumber();

      assert.equal(qsv, 0, 'unexpected initial delegators share');
    });
  });

  describe('setDelegatorsShare', () => {
    it('should set qsv', async () => {
      await validators.commitStake({ from: CANDIDATE, value: Q });

      const qsv = 50;
      await validationRewardPools.setDelegatorsShare(qsv, { from: CANDIDATE });

      const resQsv = (await validationRewardPools.getDelegatorsShare(CANDIDATE)).toNumber();
      assert.equal(qsv, resQsv, 'unexpected value');
    });

    it('should fail for non-existing address', async () => {
      await truffleAssert.reverts(
        validationRewardPools.setDelegatorsShare(10, { from: CANDIDATE }),
        'The caller is not a validator, failed to set delegators share.'
      );
    });
  });

  describe('getAccountableTotalStake', () => {
    it('QDEV-2354 should update validators ranking after withdrawal announcement', async () => {
      const stake = Q.times(10);
      await validators.commitStake({ from: VALIDATOR1, value: stake });
      await validators.enterShortList({ from: VALIDATOR1 });

      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        stake.toString(),
        'unexpected initial stake'
      );

      await validators.announceWithdrawal(Q.times(5), { from: VALIDATOR1 });
      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        Q.times(5).toString(),
        'unexpected stake after announcement'
      );

      let vals = await validators.getValidatorShortList();
      assert.equal(vals[0][0], VALIDATOR1);
      assert.equal(vals[0][1].toString(), Q.times(5).toString());

      await setTime(
        toBN((await validators.validatorsInfos(VALIDATOR1)).endTime)
          .plus(1)
          .toNumber()
      );
      await validators.withdraw(Q.times(2), VALIDATOR1, { from: VALIDATOR1 });

      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        Q.times(5).toString(),
        'unexpected stake after withdraw'
      );

      vals = await validators.getValidatorShortList();
      assert.equal(vals[0][1].toString(), Q.times(5).toString(), 'short list should have accountable total stake');
    });

    it('should return zero when all stake is in pending withdrawal state', async () => {
      const stake = Q.times(10);
      await validators.commitStake({ from: VALIDATOR1, value: stake });
      await validators.enterShortList({ from: VALIDATOR1 });

      // let's delegate something
      await qVault.deposit({ from: CANDIDATE, value: stake });
      await qVault.delegateStake([VALIDATOR1], [stake], { from: CANDIDATE });

      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        stake.times(2).toString(),
        'unexpected initial stake'
      );

      await validators.announceWithdrawal(stake, { from: VALIDATOR1 });
      assert.equal((await validators.getAccountableTotalStake(VALIDATOR1)).toString(), '0', 'should be zero');
    });
  });

  describe('getters for stake', () => {
    it('should get delegated stake', async () => {
      await qVault.deposit({ from: CANDIDATE, value: toBN(1000) });
      await validators.commitStake({ from: CANDIDATE, value: Q });
      await validators.enterShortList({ from: CANDIDATE });
      await qVault.delegateStake([CANDIDATE], [toBN(1000)], { from: CANDIDATE });

      assert.equal(
        (await validators.getValidatorDelegatedStake(CANDIDATE)).toString(),
        toBN(1000).toString(),
        'unexpected fraction'
      );
    });

    it('should get own stake', async () => {
      const amount = 1000000000000000000;
      await validators.commitStake({ from: CANDIDATE, value: toBN(amount) });

      assert.equal((await validators.getAccountableSelfStake(CANDIDATE)).toString(), amount);
    });

    it('should get all stake', async () => {
      const amount = 1000;
      await validators.commitStake({ from: CANDIDATE, value: toBN(amount) });

      await qVault.deposit({ from: DEFAULT, value: toBN(500) });
      await qVault.delegateStake([CANDIDATE], [toBN(250)], { from: DEFAULT });

      const totalStake = 1250;
      assert.equal((await validators.getValidatorTotalStake(CANDIDATE)).toString(), toBN(totalStake).toString());
    });
  });

  describe('validatorsInfos slashing', () => {
    let proposalID;
    let endTime;

    beforeEach('init proposal', async () => {
      const amount = 100000;
      await validators.commitStake({ from: CANDIDATE, value: amount });
      await validators.enterShortList({ from: CANDIDATE });

      const result = await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      proposalID = Number(result.logs[0].args._id);
      const proposal = await validatorsSlashingVoting.proposals.call(proposalID);
      endTime = Number(proposal.base.params.votingEndTime);
    });

    it('should add id to slashing list', async () => {
      const result = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(result), '["0"]');
    });

    it('purgeSlashing work correctly', async () => {
      await setTime(endTime + 1);
      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });

      const proposalIdsBeforePurge = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(proposalIdsBeforePurge), '["0","1"]');

      await validators.purgePendingSlashings(CANDIDATE);
      const proposalIdsAfterPurge = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(proposalIdsAfterPurge), '["1"]');
    });

    it('purgeSlashing does not do unnecessary purging', async () => {
      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });

      const proposalIdsBeforePurge = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(proposalIdsBeforePurge), '["0","1"]');

      await validators.purgePendingSlashings(CANDIDATE);

      const proposalIdsAfterPurge = await validators.getSlashingProposalIds(CANDIDATE);
      assert.equal(JSON.stringify(proposalIdsAfterPurge), '["0","1"]');
    });

    it('calculate should ignore slashing after end time', async () => {
      await setTime(endTime + 1);

      const amountToWithdraw = 50000;
      await validators.announceWithdrawal(amountToWithdraw, { from: CANDIDATE });

      const withdrawal = await validators.validatorsInfos(CANDIDATE);
      await setTime(Number(withdrawal.endTime) + 1);

      const beforeWithdrawal = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      await validators.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE });
      const afterWithdrawal = toBN(await validators.getAccountableSelfStake(CANDIDATE));
      assert.equal(beforeWithdrawal.toString(), afterWithdrawal.toString());
    });

    it('should fail to withdraw when remaining will be not enough for slashing', async () => {
      const amountToWithdraw = 50000;
      await validators.announceWithdrawal(amountToWithdraw, { from: CANDIDATE });
      const withdrawal = await validators.validatorsInfos(CANDIDATE);

      await setTime(Number(withdrawal.endTime) + 1);

      await truffleAssert.reverts(
        validators.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE }),
        '[QEC-005009]-The withdrawal is blocked by pending slashing proposals.'
      );
    });

    it('should not be possible to withdraw if balance became insufficient because of slashing', async () => {
      const amountToWithdraw = toBN(80000);
      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '100000');

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });
      const proposalID = 0;
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      await validators.announceWithdrawal(amountToWithdraw, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '40000');

      const withdrawal = await validators.validatorsInfos.call(CANDIDATE);
      await setTime((await getCurrentBlockTime()) + Number(withdrawal.endTime) + 1);
      await truffleAssert.reverts(
        validators.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE }),
        '[QEC-005008]-Cannot withdraw more than the announced amount.'
      );

      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '40000');
    });

    it('should be possible to withdraw less than announced if balance became insufficient because of slashing', async () => {
      const announceToWithdraw = toBN(80000);
      const amountToWithdraw = toBN(20000);
      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '100000');

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT2 });
      const proposalID = 0;
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      await validators.announceWithdrawal(announceToWithdraw, { from: CANDIDATE });

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '40000');

      await validators.withdraw(amountToWithdraw, CANDIDATE, { from: CANDIDATE });

      assert.equal((await validators.getSelfStake(CANDIDATE)).toString(), '20000');
    });
  });

  describe('applySlashing', () => {
    const commitStakeAmount = Q.times(100);
    const lockedAmount = Q.times(100);
    let amountToSlash = Q.times(20);
    const announceWithdrawalAmount = Q.times(40);

    beforeEach('setup', async () => {
      await validators.commitStake({ from: CANDIDATE, value: commitStakeAmount });
      await validators.enterShortList({ from: CANDIDATE });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: CANDIDATE });
      assert.equal(toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(), lockedAmount.toString());

      await validators.announceWithdrawal(announceWithdrawalAmount, { from: CANDIDATE });

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });

      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceWithdrawalAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('QDEV-2371 should correctly handle withdrawal during slashing', async () => {
      const stake = Q.times(100);
      await validators.commitStake({ from: VALIDATOR1, value: stake });
      await validators.enterShortList({ from: VALIDATOR1 });

      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        stake.toString(),
        'unexpected initial stake'
      );

      await validatorsSlashingVoting.createProposal(remark, VALIDATOR1, DECIMAL.times(0.2), { from: ROOT });
      const proposalID = 0;
      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        stake.toString(),
        'unexpected stake after slashing proposal'
      );

      await validators.announceWithdrawal(Q.times(85), { from: VALIDATOR1 });
      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        Q.times(15).toString(),
        'unexpected stake after withdrawal announcement'
      );

      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });
      const voteEndTime = await getVoteTimeHelper(validatorsSlashingVoting, proposalID);

      const minimumBalance = await validators.getMinimumBalance(VALIDATOR1, voteEndTime + 2);
      console.assert(minimumBalance, 0);

      await setTime((await getCurrentBlockTime()) + voteEndTime);
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal(
        (await validators.getAccountableTotalStake(VALIDATOR1)).toString(),
        '0',
        'should be zero after slashing execution'
      );
      assert.isFalse(await validators.isInShortList(VALIDATOR1), 'should drop from short list');

      await validators.commitStake({ from: VALIDATOR1, value: Q.times(10) });
      assert.equal((await validators.getAccountableTotalStake(VALIDATOR1)).toString(), Q.times(10).toString());

      await validators.enterShortList({ from: VALIDATOR1 });
      assert.isTrue(await validators.isInShortList(VALIDATOR1), 'should be in short list');
    });

    it('should successfully apply slashing with amount greater than stake', async () => {
      percentage = getPercentageFormat(100);

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const proposalID = 0;

      await web3.eth.sendTransaction({ to: ROOT, from: CANDIDATE, value: 1000 });

      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal(toBN(await validators.getSelfStake(CANDIDATE)).toString(), 0);

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });

    it('should successfully applied slashing with needed unlock amount < pending unlock amount', async () => {
      amountToSlash = Q.times(30);
      percentage = getPercentageFormat(30);

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const proposalID = 0;
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(amountToSlash).toString()
      );

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        lockedAmount.minus(announceWithdrawalAmount).toString()
      );

      const neededUnlockAmount = amountToSlash.minus(commitStakeAmount.minus(lockedAmount));
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceWithdrawalAmount.minus(neededUnlockAmount).toString()
      );
    });

    it('should successfully applied slashing with needed unlock amount > pending unlock amount', async () => {
      amountToSlash = Q.times(90);
      percentage = getPercentageFormat(90);

      await validatorsSlashingVoting.createProposal(remark, CANDIDATE, percentage, { from: ROOT });
      const proposalID = 0;
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT });
      await validatorsSlashingVoting.voteFor(proposalID, { from: ROOT2 });

      await setTime((await getCurrentBlockTime()) + (await getVoteTimeHelper(validatorsSlashingVoting, proposalID)));
      await validatorsSlashingVoting.execute(proposalID, { from: ROOT });

      assert.equal(
        toBN(await validators.getSelfStake(CANDIDATE)).toString(),
        commitStakeAmount.minus(amountToSlash).toString()
      );

      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const neededUnlockAmount = amountToSlash.minus(commitStakeAmount.minus(lockedAmount));

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(neededUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);
    });
  });

  describe('getAccountableTotalStake', () => {
    let cases;
    let amountCases;

    before(async () => {
      await epqfiParams.setUint('governed.EPQFI.stakeDelegationFactor', DECIMAL.times(10), { from: PARAMETERS_VOTING });

      cases = [
        {
          validator: VALIDATOR1,
          delegator: VALIDATOR1,
        },
        {
          validator: VALIDATOR2,
          delegator: CANDIDATE,
        },
      ];
      amountCases = [
        {
          own: Q,
          delegated: Q.times(900),
          accountable: Q.times(10),
        },
        {
          own: Q.times(100),
          delegated: Q.times(900),
          accountable: Q.times(1000),
        },
        {
          own: Q.times(200),
          delegated: Q.times(900),
          accountable: Q.times(1100),
        },
      ];
    });

    it('should commitStake, deposit and delegateStake correctly', async () => {
      for (let i = 0; i < cases.length; i++) {
        const validator = cases[i].validator;
        const delegator = cases[i].delegator;

        for (let j = 0; j < amountCases.length; j++) {
          const own = amountCases[j].own;
          const delegated = amountCases[j].delegated;
          const accountable = amountCases[j].accountable;

          await validators.commitStake({ from: validator, value: own });
          await qVault.deposit({ from: delegator, value: delegated });
          await qVault.delegateStake([validator], [delegated], { from: delegator });

          assert.equal((await validators.getAccountableTotalStake(validator)).toString(), accountable.toString());

          await reverter.revert();
        }
      }
    });

    it('should sort properly', async () => {
      await epqfiParams.setUint('governed.EPQFI.stakeDelegationFactor', DECIMAL.times(10), { from: PARAMETERS_VOTING });

      const selfStake1 = toBN(1);
      const delegatedStake1 = toBN(100);
      const selfStake2 = toBN(2);
      const delegatedStake2 = toBN(50);

      await validators.commitStake({ from: VALIDATOR1, value: selfStake1 });
      await validators.commitStake({ from: VALIDATOR2, value: selfStake2 });
      await qVault.deposit({ from: ROOT, value: delegatedStake1 });
      await qVault.deposit({ from: ROOT2, value: delegatedStake2 });
      await qVault.delegateStake([VALIDATOR1], [delegatedStake1], { from: ROOT });
      await qVault.delegateStake([VALIDATOR2], [delegatedStake2], { from: ROOT2 });

      await validators.enterShortList({ from: VALIDATOR1 });
      await validators.enterShortList({ from: VALIDATOR2 });

      await validators.makeSnapshot();
      const shortList = await validators.getValidatorsList();
      assert.equal(shortList.length, 2);
      assert.equal(shortList[0], VALIDATOR2);
      assert.equal(shortList[1], VALIDATOR1);
    });
  });
});
