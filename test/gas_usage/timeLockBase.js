const { accounts } = require('../helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('../helpers/reverter');
const BigNumber = require('bignumber.js');
const makeProxy = require('../helpers/makeProxy');
const { toBN, getPercentageFormat } = require('../helpers/defiHelper');
const assertEqualBN = require('../helpers/assertEqualBN');

const { artifacts } = require('hardhat');
const { getCurrentBlockTime, setNextBlockTime } = require('../helpers/hardhatTimeTraveller');
const Validators = artifacts.require('./Validators.sol');
const Constitution = artifacts.require(`Constitution`);
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Parameters = artifacts.require('./Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const AddressStorage = artifacts.require('AddressStorage');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const QHolderRewardPool = artifacts.require('QHolderRewardPool');
const QVault = artifacts.require('QVault');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const ValidatorsSlashingVoting = artifacts.require(`ValidatorsSlashingVoting`);
const ValidatorSlashingEscrow = artifacts.require('./ValidatorSlashingEscrow');
const Roots = artifacts.require(`Roots`);
const DefaultAllocationProxy = artifacts.require('DefaultAllocationProxy');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting');
const ValidationRewardPools = artifacts.require('ValidationRewardPools');
const Vesting = artifacts.require('./Vesting');

describe.skip('ATimeLockBase.sol', () => {
  const reverter = new Reverter();

  const proposalExecutionP = 2592000;

  const votingPeriod = toBN(1000);
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const Q = require('../helpers/defiHelper').Q;

  const CR_UPDATE_MINIMUM_BASE = Q.multipliedBy(1000);

  const hour = toBN(3600);
  const day = toBN(24 * hour);

  let validators;
  let constitutionParameters;
  let addressStorageStakesSorted;
  let addressStorageStakes;
  let validatorsSlashingVoting;
  let qVault;
  let epqfiParams;
  let qHolderRewardPool;
  let rootNodes;
  let votingWeightProxy;
  let generalUpdateVote;
  let registry;
  let validationRewardPools;
  let vesting;
  let constitution;

  const checkInvariant = async (currentLockInfo, userAddress) => {
    const totalLockedAmount = toBN(currentLockInfo.lockedAmount).plus(currentLockInfo.pendingUnlockAmount);
    const validatorStake = toBN(await validators.getSelfStake(userAddress));
    assert.isTrue(validatorStake.gte(totalLockedAmount));
  };

  let CANDIDATE;
  let PARAMETERS_VOTING;
  let ROOT;
  let ROOT2;
  let VALIDATOR1;

  before(async () => {
    CANDIDATE = await accounts(1);
    PARAMETERS_VOTING = await accounts(6);
    ROOT = await accounts(3);
    ROOT2 = await accounts(4);
    VALIDATOR1 = await accounts(7);

    BigNumber.config({ EXPONENTIAL_AT: 1e9 });

    const parametersInitialList = [
      'constitution.maxNValidators',
      'constitution.maxNStandbyValidators',
      'constitution.cliqueEpochLength',
    ];
    const parametersValues = [20, 20, 1];

    registry = await ContractRegistry.new();
    await registry.initialize(
      [await accounts(0)],
      ['governance.constitution.parametersVoting', 'governance.experts.EPQFI.parametersVoting'],
      [PARAMETERS_VOTING, PARAMETERS_VOTING]
    );

    constitutionParameters = await makeProxy(registry.address, Parameters);
    await constitutionParameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    for (let i = 0; i < parametersInitialList.length; ++i) {
      await constitutionParameters.setUint(parametersInitialList[i], parametersValues[i], { from: PARAMETERS_VOTING });
    }
    await constitutionParameters.setUint('constitution.voting.valSlashingVP', 86400, { from: PARAMETERS_VOTING });
    const number = getPercentageFormat(60); // it's 60% from 10 ** 27
    await constitutionParameters.setUint('constitution.voting.valSlashingQRM', number, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.voting.valSlashingRMAJ', 51, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.voting.valSlashingOBJP', 86400, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.valSlashingAppealP', 86400, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.valWithdrawP', 10, { from: PARAMETERS_VOTING });
    await constitutionParameters.setUint('constitution.proposalExecutionP', proposalExecutionP, {
      from: PARAMETERS_VOTING,
    }); // month

    await constitutionParameters.setUint('constitution.voting.changeQnotConstVP', votingPeriod, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitutionParameters.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, {
      from: PARAMETERS_VOTING,
    });

    await registry.setAddress('governance.constitution.parameters', constitutionParameters.address);

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    rootNodes = await makeProxy(registry.address, Roots);
    await rootNodes.initialize(registry.address, [ROOT, ROOT2]);
    await registry.setAddress('governance.rootNodes', rootNodes.address);

    const defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(registry.address, [], []);
    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocationProxy.address);

    validatorsSlashingVoting = await makeProxy(registry.address, ValidatorsSlashingVoting);
    await validatorsSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingVoting', validatorsSlashingVoting.address);

    const validatorSlashingEscrow = await makeProxy(registry.address, ValidatorSlashingEscrow);
    await validatorSlashingEscrow.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingEscrow', validatorSlashingEscrow.address);

    qVault = await makeProxy(registry.address, QVault);
    const crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['governance.rootNodes', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    vesting = await makeProxy(registry.address, Vesting);
    await vesting.initialize(registry.address);
    await registry.setAddress('tokeneconomics.vesting', vesting.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await constitution.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING });
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('deposit several times and purge', () => {
    const depositStakeAmount = Q.times(10);
    const maxTimlocksNumber = toBN(10);
    const step = 1;

    let startReleaseDate = hour;
    let endReleaseDate = startReleaseDate.plus(hour);

    beforeEach('setup', async () => {
      const currentTime = toBN(await getCurrentBlockTime());

      let lockInfo = await validators.getLockInfo({ from: CANDIDATE });
      let totalLockedAmount = toBN(lockInfo.lockedAmount).plus(lockInfo.pendingUnlockAmount);
      let minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(await validators.getSelfStake(CANDIDATE), 0);
      assertEqualBN(totalLockedAmount, 0);
      assertEqualBN(minimumBalance, 0);

      for (let timelocksNumber = step; timelocksNumber <= maxTimlocksNumber; timelocksNumber += step) {
        startReleaseDate = hour
          .times(timelocksNumber)
          .dividedBy(step)
          .plus(await getCurrentBlockTime());
        endReleaseDate = startReleaseDate.plus(hour).plus(await getCurrentBlockTime());

        await validators.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
          from: VALIDATOR1,
          value: depositStakeAmount,
        });
      }

      lockInfo = await validators.getLockInfo({ from: CANDIDATE });
      totalLockedAmount = toBN(lockInfo.lockedAmount).plus(lockInfo.pendingUnlockAmount);
      minimumBalance = await validators.getMinimumBalance(CANDIDATE, await getCurrentBlockTime());
      assertEqualBN(depositStakeAmount.times(maxTimlocksNumber), await validators.getSelfStake(CANDIDATE));
      assertEqualBN(depositStakeAmount.times(maxTimlocksNumber), totalLockedAmount);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber));
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should deposit several times and purge 0 timelocks', async () => {
      const purgeTimelocksNumber = toBN(0);
      const currentTime = hour.times(purgeTimelocksNumber + 1).plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 1 timelocks', async () => {
      const purgeTimelocksNumber = toBN(1);

      const currentTime = hour
        .times(2)
        .plus((await getCurrentBlockTime()) + 37)
        .toNumber();
      console.log(currentTime);
      console.log(endReleaseDate.toNumber());
      await setNextBlockTime(currentTime);

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 2 timelocks', async () => {
      const purgeTimelocksNumber = toBN(2);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 3 timelocks', async () => {
      const purgeTimelocksNumber = toBN(3);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 4 timelocks', async () => {
      const purgeTimelocksNumber = toBN(4);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 5 timelocks', async () => {
      const purgeTimelocksNumber = toBN(5);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 6 timelocks', async () => {
      const purgeTimelocksNumber = toBN(6);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 7 timelocks', async () => {
      const purgeTimelocksNumber = toBN(7);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 8 timelocks', async () => {
      const purgeTimelocksNumber = toBN(8);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 9 timelocks', async () => {
      const purgeTimelocksNumber = toBN(9);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });

    it('should deposit several times and purge 10 timelocks', async () => {
      const purgeTimelocksNumber = toBN(10);
      const currentTime = hour.plus(await getCurrentBlockTime());
      await setNextBlockTime(currentTime.toNumber());

      const startTime = getTime();
      const gasConsumption = await validators.purgeTimeLocks.estimateGas(CANDIDATE);
      const endTime = getTime();
      const timePassed = (endTime - startTime) / 1000;

      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(minimumBalance, depositStakeAmount.times(maxTimlocksNumber.minus(purgeTimelocksNumber)));

      console.log('Pr num\tGas usage\tTime passed\tDeposited amount');
      console.log(`${purgeTimelocksNumber}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`);
    });
  });

  describe('validators: deposit several times', () => {
    const depositStakeAmount = Q.times(100);
    const maxTimlocksNumber = 10;
    const step = 1;

    let startReleaseDate;
    let endReleaseDate;

    beforeEach('setup', async () => {
      const currentTime = toBN(await getCurrentBlockTime()).plus(1);
      await setNextBlockTime(currentTime.toNumber());

      startReleaseDate = day.plus(await getCurrentBlockTime());
      endReleaseDate = startReleaseDate.plus(day);

      const lockInfo = await validators.getLockInfo({ from: CANDIDATE });
      const totalLockedAmount = toBN(lockInfo.lockedAmount).plus(lockInfo.pendingUnlockAmount);
      const minimumBalance = await validators.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(await validators.getSelfStake(CANDIDATE), 0);
      assertEqualBN(totalLockedAmount, 0);
      assertEqualBN(minimumBalance, 0);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await validators.getLockInfo({ from: CANDIDATE });
      await checkInvariant(currentLockInfo, CANDIDATE);
    });

    it('should deposit several times', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(firstCurrentTime);

      console.log('TL num\tGas usage\tTime passed\tDeposited amount');
      let prevTimelocksNumber = 0;
      for (
        let timelocksNumber = 0;
        timelocksNumber <= maxTimlocksNumber;
        prevTimelocksNumber = timelocksNumber, timelocksNumber += step
      ) {
        const leftTimelocksNumber = timelocksNumber - prevTimelocksNumber;

        for (let i = 1; i <= leftTimelocksNumber; i++) {
          await validators.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
            from: VALIDATOR1,
            value: depositStakeAmount,
          });
        }

        const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.minus(hour).toNumber();
        await setNextBlockTime(secondCurrentTime);

        const lockInfo = await validators.getLockInfo({ from: CANDIDATE });
        const minimumBalance = await validators.getMinimumBalance(CANDIDATE, secondCurrentTime);
        assertEqualBN(depositStakeAmount.times(timelocksNumber), await validators.getSelfStake(CANDIDATE));
        assertEqualBN(depositStakeAmount.times(timelocksNumber), lockInfo.lockedAmount);
        assertEqualBN(minimumBalance, depositStakeAmount.times(timelocksNumber));

        const startTime = getTime();
        const gasConsumption = await validators.getMinimumBalance.estimateGas(CANDIDATE, secondCurrentTime);
        const endTime = getTime();
        const timePassed = (endTime - startTime) / 1000;
        console.log(
          `${timelocksNumber.toString()}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`
        );
      }
    });
  });

  describe('rootNodes: deposit several times', () => {
    const depositStakeAmount = Q.times(100);
    const maxTimlocksNumber = 10;
    const step = 1;

    const startReleaseDate = day;
    const endReleaseDate = startReleaseDate.plus(day);

    beforeEach('setup', async () => {
      const currentTime = toBN(await getCurrentBlockTime()).plus(1);
      await setNextBlockTime(currentTime.toNumber());

      const lockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
      const totalLockedAmount = toBN(lockInfo.lockedAmount).plus(lockInfo.pendingUnlockAmount);
      const minimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, currentTime);
      assertEqualBN(await rootNodes.getRootNodeStake(CANDIDATE), 0);
      assertEqualBN(totalLockedAmount, 0);
      assertEqualBN(minimumBalance, 0);
    });

    it('should deposit several times', async () => {
      const firstCurrentTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(firstCurrentTime);

      console.log('TL num\tGas usage\tTime passed\tDeposited amount');
      let prevTimelocksNumber = 0;
      for (
        let timelocksNumber = 0;
        timelocksNumber <= maxTimlocksNumber;
        prevTimelocksNumber = timelocksNumber, timelocksNumber += step
      ) {
        const leftTimelocksNumber = timelocksNumber - prevTimelocksNumber;

        for (let i = 1; i <= leftTimelocksNumber; i++) {
          await rootNodes.depositOnBehalfOf(CANDIDATE, startReleaseDate, endReleaseDate, {
            from: VALIDATOR1,
            value: depositStakeAmount,
          });
        }

        const secondCurrentTime = (await getCurrentBlockTime()) + startReleaseDate.minus(hour).toNumber();
        await setNextBlockTime(secondCurrentTime);

        const lockInfo = await rootNodes.getLockInfo({ from: CANDIDATE });
        const minimumBalance = await rootNodes.getMinimumBalance(CANDIDATE, secondCurrentTime);
        assertEqualBN(depositStakeAmount.times(timelocksNumber), await rootNodes.getRootNodeStake(CANDIDATE));
        assertEqualBN(depositStakeAmount.times(timelocksNumber), lockInfo.lockedAmount);
        assertEqualBN(minimumBalance, depositStakeAmount.times(timelocksNumber));

        const startTime = getTime();
        const gasConsumption = await rootNodes.getMinimumBalance.estimateGas(CANDIDATE, secondCurrentTime);
        const endTime = getTime();
        const timePassed = (endTime - startTime) / 1000;
        console.log(
          `${timelocksNumber.toString()}:\t${gasConsumption.toString()}\t\t${timePassed}\t\t${minimumBalance}`
        );
      }
    });
  });
});

function getTime() {
  return new Date();
}
