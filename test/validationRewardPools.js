const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const ContractRegistry = artifacts.require('./ContractRegistry');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Validators = artifacts.require('Validators');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const Constitution = artifacts.require(`Constitution`);
const ValidationRewardPools = artifacts.require('./ValidationRewardPools');
const QVault = artifacts.require('QVaultMock');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('ValidationRewardPools', () => {
  const reverter = new Reverter();

  const Q = require('./helpers/defiHelper').Q;
  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const proposalExecutionP = 2592000;

  const CR_UPDATE_MINIMUM_BASE = Q;

  async function checkInvariantReservedClaims(userAddresses) {
    // NOTE: userAddress must contain ALL delegator address. Otherwise, the invariant cannot hold
    const delegationPromises = userAddresses.map((user) => qVault.getDelegationsList(user));
    const nestedDelegations = await Promise.all(delegationPromises);

    // aggregate claims
    const claims = {};
    nestedDelegations.forEach((dList) => {
      dList.forEach((d) => {
        if (!claims[d.validator]) {
          claims[d.validator] = toBN(d.claimableReward);
        } else {
          claims[d.validator] = claims[d.validator].plus(toBN(d.claimableReward));
        }
      });
    });

    // compare with reserved amount
    for (const validator in claims) {
      // eslint-disable-line guard-for-in
      const claimsOfValidator = claims[validator].toNumber();
      const poolInfo = await validationRewardPools.getPoolInfo.call(validator);
      assert.isAtLeast(
        Number(poolInfo.reservedForClaims),
        claimsOfValidator,
        `Not enough Q reserved for claims against ${validator}`
      );
    }
  }

  let OWNER;
  let USER;
  let VALIDATOR1;
  let PARAMETERS_VOTING;

  let registry;
  let validationRewardPools;
  let addressStorageStakes;
  let addressStorageStakesSorted;
  let validators;
  let crKeeperFactory;
  let epqfiParams;
  let constitution;
  let qVault;
  let votingWeightProxy;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER = await accounts(2);
    VALIDATOR1 = await accounts(3);
    PARAMETERS_VOTING = await accounts(5);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    const qfiUintKeys = [
      'expertPanels.QFI.tokeneconomics.QholderInterestRate',
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
      'governed.EPQFI.Q_rewardPoolInterest',
    ];
    const qfiUintParams = [getPercentageFormat(0.01), 3, 1000, getPercentageFormat(1000), getPercentageFormat(0.01)];

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(registry.address, qfiUintKeys, qfiUintParams, [], [], [], [], [], []);
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING });

    await constitution.setUint('constitution.maxNValidators', 20, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.maxNStandbyValidators', 20, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.constitution.parameters', constitution.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    addressStorageStakes = await AddressStorageStakes.new();
    addressStorageStakesSorted = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(registry.address, addressStorageStakesSorted.address, addressStorageStakes.address);
    await addressStorageStakes.transferOwnership(validators.address);
    await addressStorageStakesSorted.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    await validators.commitStake({ from: VALIDATOR1, value: Q });
    await validators.enterShortList({ from: VALIDATOR1 });

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    await qVault.deposit({ from: USER, value: Q });
    await qVault.delegateStake([VALIDATOR1], [Q.dividedBy(2)], { from: USER });
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('increase()', () => {
    it('should increase balance of validator', async () => {
      const amount = 150;
      await validationRewardPools.increase(VALIDATOR1, { value: amount });
      assert.equal(amount, await validationRewardPools.getBalance(VALIDATOR1));
    });

    it('should get exception, send zero value', async () => {
      const amount = 0;
      await truffleAssert.reverts(
        validationRewardPools.increase(VALIDATOR1, { value: amount }),
        '[QEC-016000]-Invalid value to increase the pool balance.'
      );
      assert.equal(amount, await validationRewardPools.getBalance(VALIDATOR1));
    });
  });

  describe('updateValidatorsCompoundRate()', () => {
    it('should successfully update the compound rate', async () => {
      await qVault.deposit({ from: USER, value: Q.multipliedBy(2) });
      await qVault.delegateStake([VALIDATOR1], [Q], { from: USER });
      await validationRewardPools.increase(VALIDATOR1, { value: Q });

      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      await validationRewardPools.increase(VALIDATOR1, { value: Q.times(0.5) });
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      const expectedCompoundRate = toBN('2500000000000000000');
      assert.equal(
        toBN(await validationRewardPools.getCompoundRate(VALIDATOR1)).toString(),
        expectedCompoundRate.toString()
      );
    });

    it('should reserve correct amount for later claims', async () => {
      await validationRewardPools.increase(VALIDATOR1, { value: Q });

      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);

      await qVault.delegateStake([VALIDATOR1], [Q.dividedBy(2)], { from: USER });
      await validationRewardPools.increase(VALIDATOR1, { value: Q });
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
    });

    it('should not change the compound rate, if there is nothing to distribute', async () => {
      await validationRewardPools.increase(VALIDATOR1, { value: Q });
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      const rateBefore = await validationRewardPools.getCompoundRate(VALIDATOR1);

      // update cr without new balance to distribute
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      const rateAfter = await validationRewardPools.getCompoundRate(VALIDATOR1);

      assert.equal(
        rateBefore.toString(),
        rateAfter.toString(),
        'Compound rate must not change if nothing could be distributed.'
      );
    });

    it('should not change the compound rate, if validator has no delegated stake', async () => {
      await validationRewardPools.increase(VALIDATOR1, { value: Q });
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      const rateBefore = await validationRewardPools.getCompoundRate(VALIDATOR1);

      // withdraw all stake from validator1, fill pool, then update CR again
      await qVault.delegateStake([VALIDATOR1], [0], { from: USER });
      await validationRewardPools.increase(VALIDATOR1, { value: Q });
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      const rateAfter = await validationRewardPools.getCompoundRate(VALIDATOR1);

      assert.equal(
        rateBefore.toString(),
        rateAfter.toString(),
        'Compound rate must not change if validator does not have delegations.'
      );
    });

    it('should not update the compound rate, if denormalized Aggregated stake is less than minimum base', async () => {
      await qVault.deposit({ from: USER, value: Q.dividedBy(2) });
      await qVault.delegateStake([VALIDATOR1], [Q.dividedBy(4)], { from: USER });

      await validationRewardPools.increase(VALIDATOR1, { value: Q.dividedBy(8) });

      const oldRate = await validationRewardPools.getCompoundRate(VALIDATOR1);
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      const newRate = await validationRewardPools.getCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);
      assert.equal(oldRate.toString(), newRate.toString());
    });
  });

  it('should revert if the caller is not QPB', async () => {
    await validationRewardPools.increase(VALIDATOR1, { value: Q });

    assert.equal(Q.toString(), toBN(await validationRewardPools.getBalance(VALIDATOR1)).toString());
    assert.equal(Q.toString(), toBN(await web3.eth.getBalance(validationRewardPools.address)).toString());

    await truffleAssert.reverts(
      validationRewardPools.requestRewardTransfer(VALIDATOR1, 0),
      '[QEC-016001]-Permission denied - only the QVault contract has access.'
    );
  });

  describe('getPoolInfo()', () => {
    it('should successfully get the pool info', async () => {
      await qVault.deposit({ from: USER, value: Q.multipliedBy(2) });
      await qVault.delegateStake([VALIDATOR1], [Q], { from: USER });
      await validationRewardPools.increase(VALIDATOR1, { value: Q.times(2) });

      const qsv = Q;
      await validationRewardPools.setDelegatorsShare(qsv, { from: VALIDATOR1 });

      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await checkInvariantReservedClaims([USER]);

      const poolInfo = await validationRewardPools.getPoolInfo.call(VALIDATOR1);

      const expectedAggregatedNormalizedStake = '1000000000000000000000000000';
      const expectedDelegatedStake = '3000000000000000000';
      const expectedCompoundRate = '3000000000000000000';
      const expectedReservedForClaims = '2000000000000000000';

      assert.equal(poolInfo.poolBalance, Q.times(2).toString());
      assert.equal(poolInfo.reservedForClaims, expectedReservedForClaims);
      assert.equal(poolInfo.aggregatedNormalizedStake, expectedAggregatedNormalizedStake);
      assert.equal(poolInfo.delegatedStake, expectedDelegatedStake);
      assert.equal(poolInfo.compoundRate, expectedCompoundRate);
      assert.equal(poolInfo.delegatorsShare, qsv.toString());
    });
  });

  describe('setDelegatorsShare()', () => {
    it("should emit event after Delegator's Share changing", async () => {
      await validationRewardPools.increase(VALIDATOR1, { value: Q });

      const qsvs = [50, 50, 100];
      for (let i = 0; i < qsvs.length; i++) {
        const result = await validationRewardPools.setDelegatorsShare(qsvs[i], { from: VALIDATOR1 });
        const shouldHaveChanged = i === 0 || qsvs[i] !== qsvs[i - 1];
        if (shouldHaveChanged) {
          assert.equal(result.logs.length, 1);
          assert.equal(result.logs[0].event, 'DelegatorsShareChanged');
          assert.equal(result.logs[0].args._validatorAddress, VALIDATOR1);
          assert.equal(result.logs[0].args._newDelegatorsShare.toString(), qsvs[i].toString());
        } else {
          assert.equal(result.logs.length, 0);
        }
      }
    });
  });
});
