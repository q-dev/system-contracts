const { accounts } = require('./helpers/utils');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const { toBN } = require('./helpers/defiHelper');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const AddressStorageStakes = artifacts.require('AddressStorageStakes');

describe('AddressStorageStakes', () => {
  const reverter = new Reverter();

  const stake = toBN(1000000000000000000);

  let SOMEBODY;
  let SOMEONE;
  let USER1;
  let USER2;
  let USER3;

  let storage;

  before('setup', async () => {
    SOMEBODY = await accounts(1);
    SOMEONE = await accounts(2);
    USER1 = await accounts(7);
    USER2 = await accounts(8);
    USER3 = await accounts(9);

    storage = await AddressStorageStakes.new();
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('happyPath()', () => {
    it('should add one and remove one', async () => {
      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await storage.add(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.sub(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), 0);

      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());
    });

    it('should be correct listIndex', async () => {
      assert.equal(await storage.size(), 0);

      await storage.add(USER1, stake);
      assert.equal((await storage.addrStake(USER1)).stake.toString(), stake);
      await storage.add(USER2, stake);
      assert.equal((await storage.addrStake(USER2)).stake.toString(), stake);
      await storage.add(USER3, stake);
      assert.equal((await storage.addrStake(USER3)).stake.toString(), stake);

      assert.equal(await storage.size(), 3);

      assert.equal((await storage.addrStake(USER1)).listIndex, 1);
      assert.equal((await storage.addrStake(USER2)).listIndex, 2);
      assert.equal((await storage.addrStake(USER3)).listIndex, 3);
    });

    it('should be correct listIndex after removing element', async () => {
      assert.equal(await storage.size(), 0);

      await storage.add(USER1, stake);
      assert.equal((await storage.addrStake(USER1)).stake.toString(), stake);
      await storage.add(USER2, stake);
      assert.equal((await storage.addrStake(USER2)).stake.toString(), stake);
      await storage.add(USER3, stake);
      assert.equal((await storage.addrStake(USER3)).stake.toString(), stake);

      assert.equal(await storage.size(), 3);

      assert.equal((await storage.addrStake(USER1)).listIndex, 1);
      assert.equal((await storage.addrStake(USER2)).listIndex, 2);
      assert.equal((await storage.addrStake(USER3)).listIndex, 3);

      await storage.sub(USER1, stake);

      assert.equal(await storage.size(), 2);
      assert.equal((await storage.addrStake(USER3)).listIndex, 1);
      assert.equal((await storage.addrStake(USER2)).listIndex, 2);
    });

    it('should be removed from the list after the function sub', async () => {
      assert.equal(await storage.size(), 0);

      await storage.add(USER1, stake);
      assert.equal((await storage.addrStake(USER1)).stake.toString(), stake);
      await storage.add(USER2, stake);
      assert.equal((await storage.addrStake(USER2)).stake.toString(), stake);
      await storage.add(USER3, stake);
      assert.equal((await storage.addrStake(USER3)).stake.toString(), stake);

      assert.equal(await storage.size(), 3);

      await storage.sub(USER1, stake);

      assert.equal(await storage.size(), 2);
      assert.equal((await storage.getAddresses()).toString(), [USER3, USER2].toString());
    });

    it('should add two and remove one', async () => {
      await storage.add(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.add(SOMEONE, stake);
      assert.equal((await storage.addrStake(SOMEONE)).stake.toString(), stake);

      assert.equal(await storage.size(), 2);
      assert.equal(await storage.contains(SOMEONE), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY, SOMEONE].toString());

      await storage.sub(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), 0);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal(await storage.contains(SOMEONE), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEONE].toString());
    });

    it('should add one and increase stake', async () => {
      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await storage.add(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.add(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake * 2);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());
    });

    it('should add one and decrease stake', async () => {
      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await storage.add(SOMEBODY, stake);
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.sub(SOMEBODY, toBN(stake / 2));
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), stake / 2);

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());
    });
  });

  describe('setDelegated', () => {
    it('should remove entry if total stake becomes zero', async () => {
      const delegatedStake = stake;
      await storage.setDelegated(SOMEBODY, delegatedStake);
      let data = await storage.addrStake(SOMEBODY);
      assert.equal(data.delegatedStake.toString(), delegatedStake.toString());
      assert.equal(data.stake.toString(), '0');

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.setDelegated(SOMEBODY, '0');
      data = await storage.addrStake(SOMEBODY);
      assert.equal(data.stake.toString(), '0');
      assert.equal(data.delegatedStake.toString(), '0');

      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());
    });
  });

  describe('fails()', () => {
    it('should fail on sub cause empty list', async () => {
      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await truffleAssert.reverts(
        storage.sub(SOMEBODY, stake),
        '[QEC-036000]-The address does not exist, failed to decrease the stake.'
      );
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), 0);

      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());
    });

    it('should fail on sub cause not enough stake', async () => {
      assert.equal(await storage.size(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await storage.add(SOMEBODY, toBN(stake / 2));
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), toBN(stake / 2));

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await truffleAssert.reverts(
        storage.sub(SOMEBODY, stake),
        '[QEC-036002]-The stake to remove is greater than the available stake.'
      );
      assert.equal((await storage.addrStake(SOMEBODY)).stake.toString(), toBN(stake / 2));

      assert.equal(await storage.size(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());
    });
  });
});
