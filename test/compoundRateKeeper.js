const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const { toBN } = require('./helpers/defiHelper');
const truffleAssert = require('truffle-assertions');

const { setTime } = require('./helpers/hardhatTimeTraveller');

const DECIMAL = require('./helpers/defiHelper').DECIMAL;

const { artifacts } = require('hardhat');
const CompoundRateKeeper = artifacts.require('./contracts/common/CompoundRateKeeper');

describe('CompoundRateKeeper', () => {
  describe('update()', () => {
    it('Should get correct calculation result of compound rates', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.new();
      const creationTime = (await compoundRateKeeper.compoundRate()).lastUpdate.toNumber();

      await setTime(creationTime + 10);

      const interestRate = DECIMAL.multipliedBy(0.01);
      await compoundRateKeeper.update(interestRate);

      const afterUpdate = await compoundRateKeeper.compoundRate();
      const updateTime = afterUpdate.lastUpdate.toNumber();
      const timeIntervals = updateTime - creationTime;
      const expRate = timeIntervals === 10 ? '1104622125411204510' : '1115668346665316555';
      const actualRate = toBN(afterUpdate.rate).decimalPlaces(27);
      assert.equal(toBN(actualRate).toString(), toBN(expRate).toString());
    });

    it('should check auth', async () => {
      const OWNER = await accounts(0);
      const rateKeeper = await CompoundRateKeeper.new({ from: OWNER });

      const NOT_OWNER = await accounts(1);
      await truffleAssert.reverts(rateKeeper.update(DECIMAL.multipliedBy(0.01), { from: NOT_OWNER }), 'Unauth');
    });

    it('update with 0% rate', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.new();
      const before = await compoundRateKeeper.compoundRate();
      const creationTime = before.lastUpdate.toNumber();

      await setTime(creationTime + 100);

      const interestRate = 0;
      await compoundRateKeeper.update(interestRate);

      const after = await compoundRateKeeper.compoundRate();
      const updateTime = after.lastUpdate.toNumber();

      assert.equal(toBN(after.rate).toString(), toBN(before.rate).toString());
      assert.notEqual(updateTime, creationTime);
    });
  });
});
