const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const Aliases = artifacts.require('AccountAliases');

describe('AccountAliases', () => {
  const reverter = new Reverter();

  let aliases;
  let M;
  let A1;
  let A2;
  let E;

  before('setup', async () => {
    M = await accounts(0);
    A1 = await accounts(1);
    A2 = await accounts(2);
    E = await accounts(3);

    aliases = await Aliases.new();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('setAlias', () => {
    it('should set alias for M', async () => {
      await aliases.reserve(M, { from: A1 });
      await aliases.setAlias(A1, 0);
      assert.equal(await aliases.resolve(M, 0), A1);
      await aliases.reserve(M, { from: A2 });
      await aliases.setAlias(A2, 0);
      assert.equal(await aliases.resolve(M, 0), A2);
      await aliases.setAlias(A2, 1);
      assert.equal(await aliases.resolve(M, 1), A2);
    });

    it("shouldn't set taken alias", async () => {
      await truffleAssert.reverts(aliases.setAlias(A1, 0, { from: E }), '[QEC-040000]');
      await aliases.reserve(M, { from: A1 });
      await aliases.setAlias(A1, 0);
      await truffleAssert.reverts(aliases.setAlias(A1, 0, { from: E }));
      await truffleAssert.reverts(aliases.setAlias(A1, 1, { from: E }));
    });
  });

  describe('reserve', () => {
    it.skip('should free taken alias -- DEPRECATED: anyone cannot steal alias', async () => {
      await aliases.setAlias(A1, 0, { from: E });
      await aliases.reserve(M, { from: A1 });
      await aliases.setAlias(A1, 1);
      assert.equal(await aliases.resolve(M, 1), A1);
      assert.equal(await aliases.resolve(E, 0), E);
    });

    it('should resolve identity', async () => {
      await aliases.reserve(M, { from: E });
      assert.equal(await aliases.resolve(M, 0), M);
      assert.equal(await aliases.resolveReverse(E, 0), E);
    });
  });
  describe('resolve', () => {
    it('should resolve alias for several roles', async () => {
      await aliases.reserve(M, { from: A1 });
      await aliases.setAlias(A1, 0);
      await aliases.setAlias(A1, 1);
      assert.equal(await aliases.resolve(M, 0), A1);
      assert.equal(await aliases.resolve(M, 1), A1);
      assert.deepEqual(await aliases.resolveBatch([M, M], [0, 1]), [A1, A1]);
    });

    it.skip('should return identity if not resolving -- DEPRECATED anyone can steal alias', async () => {
      assert.equal(await aliases.resolve(M, 0), M);
      await aliases.setAlias(A1, 0, { from: E });
      await aliases.reserve(M, { from: A1 });
      assert.equal(await aliases.resolve(E, 0), E);
    });
  });

  describe('resolveReverse', () => {
    it('should reverse resolve', async () => {
      await aliases.reserve(M, { from: A1 });
      await aliases.reserve(M, { from: A2 });
      await aliases.setAlias(A1, 0);
      await aliases.setAlias(A2, 1);
      assert.equal(await aliases.resolveReverse(A1, 0), M);
      assert.equal(await aliases.resolveReverse(A2, 1), M);
      assert.deepEqual(await aliases.resolveBatchReverse([A1, A2], [0, 1]), [M, M]);
    });

    it('should return identity if not resolving', async () => {
      await aliases.reserve(M, { from: A2 });
      await aliases.reserve(M, { from: A1 });
      await aliases.setAlias(A1, 0);
      await aliases.setAlias(A2, 1);
      assert.equal(await aliases.resolveReverse(E, 0), E);
    });
  });
});
