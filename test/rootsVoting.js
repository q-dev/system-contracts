'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');
const { approximateAssert, lockWeight, getVetoTimeHelper, getVoteTimeHelper } = require('./helpers/votingHelper');

const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const { assertReverts } = require('./helpers/assertThrows');

const { getCurrentBlockTime, setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const RootsVoting = artifacts.require('RootsVoting');
const Roots = artifacts.require('Roots');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const Parameters = artifacts.require('Constitution');
const ContractRegistry = artifacts.require('ContractRegistry');
const VotingWeightProxy = artifacts.require('VotingWeightProxy');
const QVault = artifacts.require('QVault');
const QHolderRewardPool = artifacts.require('QHolderRewardPool');
const Validators = artifacts.require('Validators');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const ConstitutionVoting = artifacts.require('./ConstitutionVotingMock.sol');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

// proposal status redefined, there's no other way to get
// enum values from the contract
const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

const DelegationStatus = Object.freeze({
  SELF: 0,
  DELEGATED: 1,
  PENDING: 2,
});

describe('RootsVoting', () => {
  const reverter = new Reverter();

  const proposalExecutionP = 2592000;
  const Q = require('./helpers/defiHelper').Q;
  const EMPTY_ADDR = '0x0000000000000000000000000000000000000000';
  const CONSTITUTION_HASH = '0x0bab4bdc7c50e87f51273a1cd95d9588508349797c38432f788970693f40bf87';

  let constitution; // let constHash;
  let roots;
  let rootsVoting;
  let registry;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let validators;
  let votingWeightProxy;
  let qHolderRewardPool;
  let qVault;
  let constitutionVoting;
  let crKeeperFactory;

  let CANDIDATE;
  let ROOT1;
  let ROOT2;
  let NON_EXISTING_ROOT;
  let VOTER1;
  let VOTER2;
  let OWNER;
  let CANDIDATE_2;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    CANDIDATE = await accounts(0);
    ROOT1 = await accounts(1);
    ROOT2 = await accounts(6);
    NON_EXISTING_ROOT = await accounts(2);
    VOTER1 = await accounts(3);
    VOTER2 = await accounts(4);
    OWNER = await accounts(7);
    CANDIDATE_2 = await accounts(8);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);

    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address, { from: OWNER });
    constitution = await makeProxy(registry.address, Parameters);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.maxNRootNodes', 2, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.addOrRemRootVP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.addOrRemRootRNVALP', 86400, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.addOrRemRootQRM', 0, { from: PARAMETERS_VOTING });
    const requiredMajority = getPercentageFormat(50); // it's 51% from 10 ** 27
    await constitution.setUint('constitution.voting.addOrRemRootRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.addOrRemRootSQRM', getPercentageFormat(1), {
      from: PARAMETERS_VOTING,
    });
    const requiredSMajority = getPercentageFormat(80); // it's 80% from 10 ** 27
    await constitution.setUint('constitution.voting.addOrRemRootSMAJ', requiredSMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address, { from: OWNER });

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );
    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address, { from: OWNER });
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address, { from: OWNER });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT1, ROOT2]);

    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });
    rootsVoting = await makeProxy(registry.address, RootsVoting);
    await rootsVoting.initialize(registry.address);
    await registry.setAddress('governance.rootNodes.membershipVoting', rootsVoting.address, { from: OWNER });

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.rootNodes', 'governance.validators'],
      ['governance.rootNodes.membershipVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address, { from: OWNER });

    constitutionVoting = await makeProxy(registry.address, ConstitutionVoting);
    await constitutionVoting.initialize(CONSTITUTION_HASH, registry.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    it('should create a proposal when current set is not full', async () => {
      await constitution.setUint('constitution.maxNRootNodes', 3, { from: PARAMETERS_VOTING });
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, EMPTY_ADDR, {
        from: CANDIDATE,
      });
      truffleAssert.eventEmitted(res, 'ProposalCreated');
    });

    it('should not create a CHANGE proposal when current set is greater than limit', async () => {
      await constitution.setUint('constitution.maxNRootNodes', 1, { from: PARAMETERS_VOTING });
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      const reason = '[QEC-003017]-Maximum number of root nodes is exceeded.';
      await truffleAssert.reverts(
        rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE }),
        reason
      );
    });

    it('should create a proposal when current set is full', async () => {
      await constitution.setUint('constitution.maxNRootNodes', 2, { from: PARAMETERS_VOTING });
      const replaceDest = (await roots.getMembers())[0];

      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, replaceDest, {
        from: CANDIDATE,
      });
      truffleAssert.eventEmitted(res, 'ProposalCreated');
    });

    it('should revert if current set is full and replaceDest is not provided', async () => {
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      await assertReverts(
        rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, EMPTY_ADDR, { from: CANDIDATE })
      );
    });

    it('should revert if current set is full and replaceDest is not a root node', async () => {
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      await assertReverts(
        rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, NON_EXISTING_ROOT, { from: CANDIDATE })
      );
    });

    it('should allow only a self-nomination', async () => {
      const sender = await accounts(1);
      await assertReverts(
        rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, EMPTY_ADDR, { from: sender })
      );
    });

    it('should create a proposal for remove root node without creating vote id', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), EMPTY_ADDR, ROOT2, { from: ROOT2 });
    });

    it('should revert on create a proposal for remove root node, if address not in root list by any VOTER1', async () => {
      await truffleAssert.reverts(
        rootsVoting.createProposal('', web3.utils.fromAscii(''), EMPTY_ADDR, NON_EXISTING_ROOT, {
          from: NON_EXISTING_ROOT,
        }),
        '[QEC-003000]-The root node to be replaced does not exist.'
      );
    });

    it('should throw an exception, local constitution hash is not equal to the current constitution hash.', async () => {
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      await truffleAssert.reverts(
        rootsVoting.createProposal('', web3.utils.fromAscii('some incorrect hash'), ROOT1, ROOT2, { from: ROOT1 }),
        '[QEC-003014]-The given hash does not correspond to the latest onchain constitution hash.'
      );
    });

    it('should correctly create the proposal and should not fail', async () => {
      await registry.setAddress('governance.constitution.parametersVoting', constitutionVoting.address, {
        from: OWNER,
      });
      const currentConstitutionHash = await constitutionVoting.constitutionHash();
      const result = await rootsVoting.createProposal('https://remark.com', currentConstitutionHash, ROOT1, ROOT2, {
        from: ROOT1,
      });
      truffleAssert.eventEmitted(result, 'ProposalCreated');
    });
  });

  describe('vote', () => {
    let proposalID;
    beforeEach('init proposal', async () => {
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should vote for', async () => {
      await lockWeight(qVault, VOTER1, Q);
      const txReceipt = await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      const resWeight = (await rootsVoting.proposals.call(proposalID)).base.counters.weightFor;
      assert.equal(Q, resWeight, 'unexpected resulting weight');

      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'UserVoted');
      assert.equal(txReceipt.logs[0].args._proposalId, 0);
      assert.equal(txReceipt.logs[0].args._votingOption, 1);
      assert.equal(toBN(txReceipt.logs[0].args._amount).toString(), Q.toString());
    });

    it('should vote against', async () => {
      await lockWeight(qVault, VOTER1, Q);
      await rootsVoting.voteAgainst(proposalID, { from: VOTER1 });

      const resWeight = (await rootsVoting.proposals.call(proposalID)).base.counters.weightAgainst;
      assert.equal(Q, resWeight, 'unexpected resulting weight');
    });

    it('should not be possible to vote with zero total voting weight', async () => {
      await truffleAssert.reverts(
        rootsVoting.voteFor(0, { from: VOTER1 }),
        '[QEC-003013]-The total voting weight must be greater than zero.'
      );

      assert.equal((await rootsVoting.proposals(proposalID)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await rootsVoting.proposals(proposalID)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await rootsVoting.hasUserVoted(proposalID, VOTER1));
    });

    it('should not be able to vote for non-existing proposal', async () => {
      const nonExistingID = proposalID + 1;
      assertReverts(rootsVoting.voteFor(nonExistingID, { from: VOTER1 }));
    });

    it('should not be able to vote twice', async () => {
      await lockWeight(qVault, VOTER1, Q);

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await truffleAssert.reverts(
        rootsVoting.voteFor(proposalID, { from: VOTER1 }),
        '[QEC-003005]-The caller has already voted for the proposal.'
      );

      // interesting case study: if I go this instead of line above, everything crashes
      // assertReverts(rootsVoting.voteFor(proposalID, false, { from: VOTER1 }));

      // because of lost 'await', I've fixed it
    });

    it('should not be able to vote with lock expiring before voting end time', async () => {
      await lockWeight(qVault, VOTER1, Q);
      assertReverts(rootsVoting.voteFor(proposalID, { from: VOTER1 }));
    });
  });

  describe('execute()', () => {
    it('should successfully execute a PASSED proposal (rm)', async () => {
      await constitution.setUint('constitution.maxNRootNodes', 3, { from: PARAMETERS_VOTING });
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), EMPTY_ADDR, ROOT1, {
        from: NON_EXISTING_ROOT,
      });
      const proposalID = Number(res.logs[0].args._id);

      const voteWeight = toBN(10 ** 18);
      await lockWeight(qVault, VOTER1, voteWeight);
      await lockWeight(qVault, VOTER2, voteWeight);

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await rootsVoting.voteFor(proposalID, { from: VOTER2 });

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, proposalID);
      await setTime(vetoEndTime);

      let status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status after veto end time');

      await rootsVoting.execute(proposalID);
      status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');

      assert.isFalse(await roots.isMember(ROOT1), 'candidate was not removed from root node list');
    });

    it('should successfully execute a PASSED proposal (add)', async () => {
      const CANDIDATE_2 = await accounts(7);

      await constitution.setUint('constitution.maxNRootNodes', 4, { from: PARAMETERS_VOTING });
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, EMPTY_ADDR, {
        from: CANDIDATE_2,
      });
      const proposalID = Number(res.logs[0].args._id);

      const voteWeight = toBN(10 ** 18);
      await lockWeight(qVault, VOTER1, voteWeight);
      await lockWeight(qVault, VOTER2, voteWeight);

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await rootsVoting.voteFor(proposalID, { from: VOTER2 });

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, proposalID);
      await setTime(vetoEndTime);

      let status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status after veto end time');

      await rootsVoting.execute(proposalID);
      status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');

      assert.isTrue(await roots.isMember(CANDIDATE_2), 'candidate was not added to root nodes set');
    });

    it('should successfully execute a PASSED proposal', async () => {
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });
      const proposalID = Number(res.logs[0].args._id);

      const voteWeight = toBN(10 ** 18);
      await lockWeight(qVault, VOTER1, voteWeight);
      await lockWeight(qVault, VOTER2, voteWeight);

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await rootsVoting.voteFor(proposalID, { from: VOTER2 });

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, proposalID);
      await setTime(vetoEndTime);

      let status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status after veto end time');

      const txReceipt = await rootsVoting.execute(proposalID);
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalID);

      status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');

      assert.isTrue(await roots.isMember(CANDIDATE), 'candidate was not added to root nodes set');
      assert.isFalse(await roots.isMember(ROOT1), 'old root node was not removed from root nodes set');
    });

    it('should not be possible to execute expired proposal', async () => {
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });
      const proposalID = Number(res.logs[0].args._id);

      const voteWeight = toBN(10 ** 18);
      await lockWeight(qVault, VOTER1, voteWeight);
      await lockWeight(qVault, VOTER2, voteWeight);

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await rootsVoting.voteFor(proposalID, { from: VOTER2 });

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, proposalID);
      await setTime(vetoEndTime);

      let status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status after veto end time');

      const proposalVetoEndTime = await getVetoTimeHelper(rootsVoting, 0);
      await setTime(proposalVetoEndTime + proposalExecutionP + 1);

      await truffleAssert.reverts(
        rootsVoting.execute(proposalID),
        '[QEC-003009]-Proposal must be PASSED before excecuting.'
      );

      status = (await rootsVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXPIRED, status, 'unexpected status after proposal is executed');
    });

    it('should only be able to execute a PASSED proposal (mv)', async () => {
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });
      const proposalID = Number(res.logs[0].args._id);
      assertReverts(rootsVoting.execute(proposalID));
    });
  });

  describe('QDEV-630 getters', () => {
    const proposalID = 1;
    it('should successfully return votes weight "FOR"', async () => {
      await rootsVoting.getVotesFor(proposalID, { from: NON_EXISTING_ROOT });
    });

    it('should successfully return votes weight "AGAINST"', async () => {
      await rootsVoting.getVotesAgainst(proposalID, { from: NON_EXISTING_ROOT });
    });
  });

  describe('veto()', () => {
    const proposalID = 0;

    beforeEach(async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });
    });

    it('should veto on proposal', async () => {
      await lockWeight(qVault, VOTER1, Q);
      await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      const voteEndTime = await getVoteTimeHelper(rootsVoting, proposalID);
      await setTime(voteEndTime);

      await rootsVoting.veto(proposalID, { from: ROOT1 });

      const proposal = await rootsVoting.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 1);
      assert.equal((await rootsVoting.getStatus(proposalID)).toString(), 3);

      assert.isTrue(await rootsVoting.hasRootVetoed(proposalID, ROOT1));
    });

    it('should get exception, invalid proposal status', async () => {
      const reason = '[QEC-003010]-Proposal must be ACCEPTED before casting a root node veto.';
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });
      await truffleAssert.reverts(rootsVoting.veto(1, { from: ROOT1 }), reason);
    });

    it('should get exception, overrule is accepted', async () => {
      await constitution.setUint('constitution.voting.addOrRemRootSQRM', 0, { from: PARAMETERS_VOTING });
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });

      await lockWeight(qVault, VOTER1, Q.times(1000));
      await rootsVoting.voteFor(1, { from: VOTER1 });

      const voteEndTime = await getVoteTimeHelper(rootsVoting, 1);
      await setTime(voteEndTime);

      const reason = '[QEC-003015]-Veto is not possible because of super majority vote.';
      await truffleAssert.reverts(rootsVoting.veto(1, { from: ROOT1 }), reason);

      assert.equal(await rootsVoting.getStatus(1), ProposalStatus.ACCEPTED);
    });

    it('should get exception, non exist proposal', async () => {
      const reason = '[QEC-003008]-The proposal does not exist.';
      await truffleAssert.reverts(rootsVoting.veto(100, { from: ROOT1 }), reason);
    });

    it('should get exception, call by non root', async () => {
      const reason = '[QEC-003012]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(rootsVoting.veto(proposalID, { from: await accounts(7) }), reason);
    });
  });

  describe('getStatus', () => {
    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 1000);
      await rootsVoting.voteFor(0, { from: ROOT1 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 1000);
      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status, all roots vetoed proposal', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });
      await lockWeight(qVault, VOTER1, Q);
      await rootsVoting.voteFor(0, { from: VOTER1 });

      const voteEndTime = await getVoteTimeHelper(rootsVoting, 0);
      await setTime(voteEndTime);

      await rootsVoting.veto(0, { from: ROOT1 });
      await rootsVoting.veto(0, { from: ROOT2 });

      const proposal = await rootsVoting.proposals(0);

      assert.equal(proposal.base.counters.vetosCount, 2);
      assert.equal(await rootsVoting.getStatus(0), ProposalStatus.REJECTED);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      await constitution.setUint('constitution.voting.addOrRemRootRMAJ', getPercentageFormat(51), {
        from: PARAMETERS_VOTING,
      });

      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 1000);
      await lockWeight(qVault, ROOT2, 1000);
      await rootsVoting.voteFor(0, { from: ROOT1 });
      await rootsVoting.voteAgainst(0, { from: ROOT2 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 1000);
      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 1000);

      const voteEndTime = await getVoteTimeHelper(rootsVoting, 0);
      await setTime(voteEndTime);

      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await constitution.setUint('constitution.voting.addOrRemRootQRM', getPercentageFormat(2), {
        from: PARAMETERS_VOTING,
      });

      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 100000000001);
      await rootsVoting.voteFor(0, { from: ROOT1 });

      await lockWeight(qVault, ROOT2, 1000);
      await rootsVoting.voteAgainst(0, { from: ROOT2 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 100000000001);

      const voteEndTime = await getVoteTimeHelper(rootsVoting, 0);
      await setTime(voteEndTime);

      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual majority < constitution majority', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.equal((await rootsVoting.proposals(0)).base.counters.weightAgainst.toString(), 0);

      const voteEndTime = await getVoteTimeHelper(rootsVoting, 0);
      await setTime(voteEndTime);

      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 100000000001);
      await rootsVoting.voteFor(0, { from: ROOT1 });

      await lockWeight(qVault, ROOT2, 1000);
      await rootsVoting.voteAgainst(0, { from: ROOT2 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 100000000001);
      assert.equal((await rootsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, 0);
      await setTime(vetoEndTime);

      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED on expired proposal', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 100000000001);
      await rootsVoting.voteFor(0, { from: ROOT1 });

      await lockWeight(qVault, ROOT2, 1000);
      await rootsVoting.voteAgainst(0, { from: ROOT2 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 100000000001);
      assert.equal((await rootsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, 0);
      await setTime(vetoEndTime + proposalExecutionP + 1);

      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a executed proposal', async () => {
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });

      await lockWeight(qVault, ROOT1, 1001);
      await rootsVoting.voteFor(0, { from: ROOT1 });

      await lockWeight(qVault, ROOT2, 1000);
      await rootsVoting.voteAgainst(0, { from: ROOT2 });

      assert.equal((await rootsVoting.proposals(0)).base.counters.weightFor.toString(), 1001);
      assert.equal((await rootsVoting.proposals(0)).base.counters.weightAgainst.toString(), 1000);

      const vetoEndTime = await getVetoTimeHelper(rootsVoting, 0);
      await setTime(vetoEndTime);

      await rootsVoting.execute(0);
      assert.equal((await rootsVoting.getStatus(0)).toString(), ProposalStatus.EXECUTED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const prop = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, {
        from: CANDIDATE,
      });
      const propID = Number(prop.logs[0].args._id);
      const propBase = (await rootsVoting.proposals.call(propID)).base;

      const expectedBase = await rootsVoting.getProposal(propID);

      assert.equal(expectedBase.remark, propBase.remark);
      assert.equal(expectedBase.executed, propBase.executed);
      assert.equal(expectedBase.params.votingEndTime, propBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(rootsVoting.getProposal(1), '[QEC-003008]-The proposal does not exist.');
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      const res = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, { from: CANDIDATE });
      const proposalID = Number(res.logs[0].args._id);

      await lockWeight(qVault, VOTER1, Q);
      await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      const voteEndTime = await getVoteTimeHelper(rootsVoting, proposalID);
      await setTime(voteEndTime);

      const blockNumber = (await rootsVoting.veto(proposalID, { from: ROOT1 })).receipt.blockNumber;
      const propStats = await rootsVoting.getProposalStats(proposalID);
      const prop = await rootsVoting.proposals.call(proposalID);

      assert.equal(prop.base.counters.weightFor, Q);
      assert.equal(prop.base.counters.vetosCount, 1);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const totalWeight = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      assert.isTrue(approximateAssert(totalWeight, blockNumber, propStats.currentQuorum));

      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalWeight);
      assert.equal(currentMajority.toNumber(), propStats.currentMajority);

      const rootsCount = 2;
      const vetoesCount = prop.base.counters.vetosCount;
      const expectedVetoPercentage = toBN(vetoesCount)
        .times(10 ** 27)
        .dividedBy(rootsCount);
      assert.equal(propStats.currentVetoPercentage, expectedVetoPercentage.toNumber());
    });

    it('should return correct voting stats structure before the first vote', async () => {
      const result = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, {
        from: CANDIDATE,
      });
      const proposalID = Number(result.logs[0].args._id);

      const proposal = await rootsVoting.proposals.call(proposalID);
      const propStats = await rootsVoting.getProposalStats(proposalID);

      assert.equal(proposal.base.counters.weightFor, 0);
      assert.equal(proposal.base.counters.vetosCount, 0);

      assert.equal(propStats.requiredMajority, proposal.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, proposal.base.params.requiredQuorum);

      assert.equal(propStats.currentMajority, 0);
      assert.equal(propStats.currentQuorum, 0);
      assert.equal(propStats.currentVetoPercentage, 0);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(rootsVoting.getProposalStats(1), '[QEC-003008]-The proposal does not exist.');
    });
  });

  describe('skipVetoPhase', () => {
    const proposalID = 0;

    beforeEach(async () => {
      await constitution.setUint('constitution.voting.addOrRemRootSQRM', 0, { from: PARAMETERS_VOTING });
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });
    });

    it('should successfully skip veto phase', async () => {
      await lockWeight(qVault, VOTER1, Q);
      await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      const voteEndTime = await getVoteTimeHelper(rootsVoting, proposalID);
      await setTime(voteEndTime);

      await rootsVoting.skipVetoPhase(proposalID);

      assert.equal(await rootsVoting.getStatus(proposalID), ProposalStatus.PASSED);
    });

    it('should get exception, overrule do not applied', async () => {
      const reason = '[QEC-003016]-Veto phase can be skipped only with super majority vote.';
      await truffleAssert.reverts(rootsVoting.skipVetoPhase(proposalID), reason);
    });
  });

  describe('isOverruleApplied', () => {
    const proposalID = 0;

    beforeEach(async () => {
      await constitution.setUint('constitution.voting.addOrRemRootSQRM', 0, { from: PARAMETERS_VOTING });
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });
    });

    it('should return true, overrule applied', async () => {
      await lockWeight(qVault, VOTER1, Q.times(100000));
      await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      assert.isTrue(await rootsVoting.isOverruleApplied(proposalID));
    });

    it('should return false, the proposal did not reach a super-majority', async () => {
      await lockWeight(qVault, VOTER1, Q.times(70000));
      await lockWeight(qVault, VOTER2, Q.times(30000));

      await rootsVoting.voteFor(proposalID, { from: VOTER1 });
      await rootsVoting.voteAgainst(proposalID, { from: VOTER2 });

      assert.isFalse(await rootsVoting.isOverruleApplied(proposalID));
    });

    it('should return false, the proposal did not reach a super-quorum', async () => {
      await constitution.setUint('constitution.voting.addOrRemRootSQRM', getPercentageFormat(5), {
        from: PARAMETERS_VOTING,
      });
      await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE_2, ROOT1, { from: CANDIDATE_2 });

      await lockWeight(qVault, VOTER1, Q.times(90000));
      await lockWeight(qVault, VOTER2, Q.times(10000));

      await rootsVoting.voteFor(1, { from: VOTER1 });
      await rootsVoting.voteAgainst(1, { from: VOTER2 });

      assert.isFalse(await rootsVoting.isOverruleApplied(1));
    });
  });

  describe('getVotingWeightInfo()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, {
        from: CANDIDATE,
      });
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully return the voting weight info', async () => {
      const lockedWeightForVOTER1 = toBN(60000);
      await lockWeight(qVault, VOTER1, lockedWeightForVOTER1);
      await rootsVoting.voteFor(proposalID, { from: VOTER1 });

      const votingWeightInfo = await rootsVoting.getVotingWeightInfo(proposalID, { from: VOTER1 });

      assert.equal(votingWeightInfo.base.ownWeight, lockedWeightForVOTER1.toString());
      assert.equal(votingWeightInfo.base.votingAgent, VOTER1);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.SELF);
      assert.equal(votingWeightInfo.base.lockedUntil, await votingWeightProxy.getLockedUntil(VOTER1));
      assert.equal(votingWeightInfo.hasAlreadyVoted, true);
      assert.equal(votingWeightInfo.canVote, false);
    });

    it('should return PENDING status', async () => {
      const lockedWeightForVOTER1 = toBN(60000);
      await lockWeight(qVault, VOTER1, lockedWeightForVOTER1);
      await votingWeightProxy.announceNewVotingAgent(VOTER2, { from: VOTER1 });

      const votingWeightInfo = await rootsVoting.getVotingWeightInfo(proposalID, { from: VOTER1 });

      assert.equal(votingWeightInfo.base.votingAgent, VOTER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.PENDING);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(VOTER1)).toString());
    });

    it('should return DELEGATED status', async () => {
      const lockedWeightForVOTER1 = toBN(60000);
      await lockWeight(qVault, VOTER1, lockedWeightForVOTER1);

      await votingWeightProxy.announceNewVotingAgent(VOTER2, { from: VOTER1 });
      const delegationInfo = await votingWeightProxy.delegationInfos(VOTER1);
      await setTime((await getCurrentBlockTime()) + toBN(delegationInfo.votingAgentPassOverTime).plus(1).toNumber());
      await votingWeightProxy.setNewVotingAgent({ from: VOTER1 });

      const result = await rootsVoting.createProposal('', web3.utils.fromAscii(''), CANDIDATE, ROOT1, {
        from: CANDIDATE,
      });
      const proposalID = Number(result.logs[0].args._id);
      await rootsVoting.voteFor(proposalID, { from: VOTER2 });

      const votingWeightInfo = await rootsVoting.getVotingWeightInfo(proposalID, { from: VOTER1 });

      assert.equal(votingWeightInfo.base.votingAgent, VOTER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.DELEGATED);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(VOTER2)).toString());
    });

    it('should revert if the slashing proposal does not exist', async () => {
      await truffleAssert.reverts(
        rootsVoting.getVotingWeightInfo(10001, { from: VOTER1 }),
        '[QEC-003008]-The proposal does not exist.'
      );
    });
  });
});
