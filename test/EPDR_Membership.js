const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const truffleAssert = require('truffle-assertions');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');

const { artifacts } = require('hardhat');
const Experts = artifacts.require('EPDR_Membership.sol');
const Parameters = artifacts.require('EPDR_Parameters.sol');
const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('Experts', () => {
  const reverter = new Reverter();

  let DEFAULT;
  let EXPERTS_VOTING;
  let INITIAL_EXPERT;
  let EXPERT1;
  let PARAMETERS_VOTING;

  let constitution;
  let experts;

  before('setup', async () => {
    DEFAULT = await accounts(0);
    EXPERTS_VOTING = await accounts(1);
    INITIAL_EXPERT = await accounts(2);
    EXPERT1 = await accounts(3);
    PARAMETERS_VOTING = await accounts(4);

    const registry = await ContractRegistry.new();
    await registry.initialize(
      [DEFAULT],
      ['governance.experts.EPDR.parametersVoting', 'governance.experts.EPDR.membershipVoting'],
      [PARAMETERS_VOTING, EXPERTS_VOTING]
    );

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    constitution = await makeProxy(registry.address, Parameters);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);
    await constitution.setUint('constitution.EPDR.maxNExperts', 100, { from: PARAMETERS_VOTING });

    await registry.setAddress('governance.constitution.parameters', constitution.address);

    experts = await makeProxy(registry.address, Experts);
    await experts.initialize(registry.address, []);
    await experts.addMember(INITIAL_EXPERT, { from: EXPERTS_VOTING });
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('constructor', () => {
    it('should contain initial expert list', async () => {
      const members = await experts.getMembers();
      assert.deepEqual([INITIAL_EXPERT], members, 'unexpected initial expert node members');
    });
  });

  describe('addMember', () => {
    it('should add new member', async () => {
      await experts.addMember(EXPERT1, { from: EXPERTS_VOTING });
      await assert.isTrue(await experts.isMember(EXPERT1), 'expected to contain new expert node');
    });

    it('should be able to add new member only from expertsVoting', async () => {
      const CALLER = await accounts(9);
      await truffleAssert.reverts(experts.addMember(EXPERT1, { from: CALLER }));
    });
  });

  describe('swapMember', () => {
    it('should change expert', async () => {
      await experts.swapMember(INITIAL_EXPERT, EXPERT1, { from: EXPERTS_VOTING });
      assert.isTrue(await experts.isMember(EXPERT1));
      assert.isFalse(await experts.isMember(INITIAL_EXPERT));
    });

    it('should be able to change expert only from expertsVoting', async () => {
      await truffleAssert.reverts(experts.swapMember(INITIAL_EXPERT, EXPERT1));
    });
  });

  describe('removeMember', () => {
    it('should delete expert', async () => {
      await experts.removeMember(INITIAL_EXPERT, { from: EXPERTS_VOTING });
      assert.isFalse(await experts.isMember(INITIAL_EXPERT));
    });
  });
});
