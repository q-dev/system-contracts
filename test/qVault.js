const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const assertEqualBN = require('./helpers/assertEqualBN');
const BigNumber = require('bignumber.js');
const createExceptionStorage = require('./helpers/exceptionStorage');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const QVault = artifacts.require('QVaultMock');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const QHolderRewardPool = artifacts.require('QHolderRewardPool');
const ValidationRewardPools = artifacts.require('ValidationRewardPools');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require(`Constitution`);
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const EPQFIParametersVoting = artifacts.require(`EPQFI_ParametersVoting`);
const EPQFIMembership = artifacts.require(`EPQFI_Membership`);
const Validators = artifacts.require('ValidatorsMock');
const Roots = artifacts.require('Roots');
const ValidatorsSlashingVoting = artifacts.require(`ValidatorsSlashingVoting`);
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const GeneralUpdateVoting = artifacts.require('./GeneralUpdateVoting.sol');
const ERC677TransferReceiverMock = artifacts.require('ERC677TransferReceiverMock');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const DECIMAL = require('./helpers/defiHelper').DECIMAL;

describe('QVault', () => {
  BigNumber.config({ EXPONENTIAL_AT: 1e9 });

  const reverter = new Reverter();

  let crKeeperFactory;
  let qVault;
  let qHolderRewardPool;
  let validationRewardPools;
  let registry;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let validators;
  let roots;
  let validatorsSlashingVoting;
  let votingWeightProxy;
  let generalUpdateVote;
  let exceptionStorage;
  let constitution;
  let epqfiParams;
  let epqfiParametersVoting;
  let epqfiMembership;

  const Q = require('./helpers/defiHelper').Q;
  const DECIMAL = require('./helpers/defiHelper').DECIMAL;
  const RATE = toBN(0.00000000302226);
  const qi = getPercentageFormat(RATE);

  const CR_UPDATE_MINIMUM_BASE = Q;

  const votingPeriod = 1000;
  const requiredQuorum = 0;
  const requiredMajority = getPercentageFormat(50);
  const vetoPeriod = votingPeriod;
  const proposalExecutionP = 2592000;
  const qRewardPoolInterest = getPercentageFormat(2);

  let DEFAULT;
  let SOMEBODY;
  let VALIDATOR1;
  let VALIDATOR2;
  let VALIDATOR3;
  let VALIDATOR4;
  let VALIDATOR5;
  let EXPERT;
  let ROOT;
  let PARAMETERS_VOTING;

  before('setup', async () => {
    DEFAULT = await accounts(0);
    SOMEBODY = await accounts(1);
    VALIDATOR1 = await accounts(2);
    VALIDATOR2 = await accounts(3);
    VALIDATOR3 = await accounts(4);
    VALIDATOR4 = await accounts(5);
    VALIDATOR5 = await accounts(6);
    EXPERT = await accounts(7);
    ROOT = await accounts(8);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([DEFAULT], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    epqfiParametersVoting = await makeProxy(registry.address, EPQFIParametersVoting);
    epqfiParametersVoting.initialize(registry.address);
    await registry.setAddress('governance.experts.EPQFI.parametersVoting', epqfiParametersVoting.address);

    epqfiMembership = await makeProxy(registry.address, EPQFIMembership);
    epqfiMembership.initialize(registry.address, [EXPERT]);
    await registry.setAddress('governance.experts.EPQFI.membership', epqfiMembership.address);

    const qfiUintKeys = [
      'expertPanels.QFI.tokeneconomics.QholderInterestRate',
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.stakeDelegationFactor',
      'governed.EPQFI.Q_rewardPoolInterest',
    ];
    const qfiUintParams = [qi, 3, getPercentageFormat(1000), qRewardPoolInterest];

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(registry.address, qfiUintKeys, qfiUintParams, [], [], [], [], [], []);

    exceptionStorage = await createExceptionStorage();
    await registry.setAddress('common.exceptionStorage', exceptionStorage.address);

    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);
    const parametersInitialList = ['constitution.maxNValidators', 'constitution.maxNStandbyValidators'];
    const parametersValues = [20, 20];

    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.voting.EPQFI.changeParamVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.EPQFI.changeParamRNVALP', requiredQuorum, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.EPQFI.changeParamRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.EPQFI.changeParamQRM', vetoPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.changeQnotConstRMAJ', requiredMajority, {
      from: PARAMETERS_VOTING,
    });
    await constitution.setUint('constitution.voting.changeQnotConstRNVALP', vetoPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month

    await registry.setAddress('governance.constitution.parameters', constitution.address);

    for (let i = 0; i < parametersInitialList.length; ++i) {
      await constitution.setUint(parametersInitialList[i], parametersValues[i], { from: PARAMETERS_VOTING });
    }

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);
    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);

    validatorsSlashingVoting = await makeProxy(registry.address, ValidatorsSlashingVoting);
    await validatorsSlashingVoting.initialize(registry.address);
    await registry.setAddress('governance.validators.slashingVoting', validatorsSlashingVoting.address);

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();

    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT], { from: DEFAULT });
    await registry.setAddress('governance.rootNodes', roots.address);

    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    qVault = await makeProxy(registry.address, QVault);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    await validators.commitStake({ from: VALIDATOR1, value: Q });
    await validators.enterShortList({ from: VALIDATOR1 });

    await validators.commitStake({ from: VALIDATOR2, value: Q });
    await validators.enterShortList({ from: VALIDATOR2 });

    await validators.commitStake({ from: VALIDATOR3, value: Q });
    await validators.enterShortList({ from: VALIDATOR3 });

    await validators.commitStake({ from: VALIDATOR4, value: Q });
    await validators.enterShortList({ from: VALIDATOR4 });

    await validators.commitStake({ from: DEFAULT, value: Q });
    await validators.enterShortList({ from: DEFAULT });

    await validators.commitStake({ from: SOMEBODY, value: Q });
    await validators.enterShortList({ from: SOMEBODY });

    generalUpdateVote = await makeProxy(registry.address, GeneralUpdateVoting);
    await generalUpdateVote.initialize(registry.address);
    await registry.setAddress('governance.generalUpdateVoting', generalUpdateVote.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('deposit()', () => {
    const DEPOSIT = Q;

    it('should be possible to deposit for the first time 1 eth from SOMEBODY', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());
      const lastClaimStartTime = toBN(await compoundRateKeeper.getLastUpdate());
      await setNextBlockTime((await getCurrentBlockTime()) + lastClaimStartTime.toNumber());

      const result = await qVault.deposit({ from: SOMEBODY, value: DEPOSIT });

      const balanceAfterDeposit = toBN(await qVault.balanceOf(SOMEBODY));

      assert.equal(balanceAfterDeposit.toString(), DEPOSIT.toString());
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserDeposited');
      assert.equal(result.logs[0].args._newDepositAmount.toString(), DEPOSIT.toString());
      assert.equal(result.logs[0].args._newBalance.toString(), DEPOSIT);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should be possible to deposit for the second time 1 eth from SOMEBODY', async () => {
      let result = await qVault.deposit({ from: SOMEBODY, value: DEPOSIT });

      const balanceAfterDeposit = toBN(await qVault.balanceOf(SOMEBODY));

      assert.equal(balanceAfterDeposit.toString(), DEPOSIT.toString());
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserDeposited');
      assert.equal(result.logs[0].args._newDepositAmount.toString(), DEPOSIT.toString());
      assert.equal(result.logs[0].args._newBalance.toString(), DEPOSIT);

      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());

      const creationTime = (await compoundRateKeeper.getLastUpdate()).toNumber();
      await setNextBlockTime((await getCurrentBlockTime()) + toBN(creationTime).toNumber());

      result = await qVault.deposit({ from: SOMEBODY, value: DEPOSIT });

      const balanceAfterSecondDeposit = toBN(await qVault.balanceOf(SOMEBODY));

      const expectedBalance = balanceAfterDeposit.plus(DEPOSIT).plus(0);
      assert.equal(balanceAfterSecondDeposit.toString(), expectedBalance.toString());
      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserDeposited');
      assert.equal(result.logs[0].args._newDepositAmount.toString(), DEPOSIT.toString());
      assert.equal(result.logs[0].args._newBalance.toString(), expectedBalance);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should not be possible to deposit 0 from SOMEBODY', async () => {
      await truffleAssert.reverts(
        qVault.deposit({ from: SOMEBODY, value: 0 }),
        '[QEC-017000]-Deposit amount must not be zero.'
      ); // ');

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('QDEV-3166 should deposit correctly several times to different accounts when compound rate > 1', async () => {
      // init cr > 1
      await setNextBlockTime((await getCurrentBlockTime()) + 1000);
      await qVault.updateCompoundRate();

      // Deposit to QVault balance to protect from invariance violation by multiple uproundings on different accounts
      await qVault.depositFromPool({ from: DEFAULT, value: Q });

      const operationPerAddress = async (a) => {
        await qVault.depositTo(a, { from: SOMEBODY, value: 11 });
      };

      const addresses = [];
      const numberOfAddresses = 10;
      for (let i = 0; i < numberOfAddresses; i++) {
        const newAccount = web3.eth.accounts.create();
        addresses.push(newAccount.address);
      }

      const deposits = addresses.map(operationPerAddress);
      await Promise.all(deposits);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
    });
  });

  describe('withdraw', () => {
    const deposit = Q.times(100);
    const delegatedAmount = Q.times(40);
    const lockedAmount = Q.times(60);
    let pendingUnlockTime;

    beforeEach('setup', async () => {
      const poolBalance = Q.times(20);
      await qHolderRewardPool.send(poolBalance);

      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.lock(lockedAmount, { from: SOMEBODY });

      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: SOMEBODY });

      await qVault.announceUnlock(lockedAmount, { from: SOMEBODY });

      pendingUnlockTime = toBN(votingPeriod).plus(5).toNumber();

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should successfully withdraw when available balance > amount', async () => {
      await qVault.delegateStake([VALIDATOR1], [delegatedAmount], { from: SOMEBODY });
      assert.equal(toBN(await qVault.testTotalDelegatedStake(SOMEBODY)).toString(), delegatedAmount.toString());

      let currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      let currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.toString());

      const withdrawnAmount = Q.times(30);
      await qVault.withdraw(withdrawnAmount, { from: SOMEBODY });

      currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.minus(withdrawnAmount).toString());

      currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should successfully withdraw when available balance < amount and unlocking is possible', async () => {
      await qVault.delegateStake([VALIDATOR1], [delegatedAmount], { from: SOMEBODY });
      assert.equal(toBN(await qVault.testTotalDelegatedStake(SOMEBODY)).toString(), delegatedAmount.toString());

      let currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      let currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.toString());

      const withdrawnAmount = Q.times(50);

      await setNextBlockTime((await getCurrentBlockTime()) + pendingUnlockTime);

      await qVault.withdraw(withdrawnAmount, { from: SOMEBODY });

      currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.minus(withdrawnAmount).toString());

      const neededUnlockAmount = withdrawnAmount.minus(deposit.minus(lockedAmount));
      currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        lockedAmount.minus(neededUnlockAmount).toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should successfully withdraw without announce unlock', async () => {
      let currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      let currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.toString());

      const additionalLock = delegatedAmount;
      await qVault.lock(additionalLock, { from: SOMEBODY });
      const withdrawnAmount = lockedAmount.plus(additionalLock);

      await setNextBlockTime((await getCurrentBlockTime()) + pendingUnlockTime);

      await qVault.withdraw(withdrawnAmount, { from: SOMEBODY });

      currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.minus(withdrawnAmount).toString());

      currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to unlock needed amount before the period expires', async () => {
      await qVault.delegateStake([VALIDATOR1], [delegatedAmount], { from: SOMEBODY });
      assert.equal(toBN(await qVault.testTotalDelegatedStake(SOMEBODY)).toString(), delegatedAmount.toString());

      let currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      let currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.toString());

      const withdrawnAmount = Q.times(50);
      // -Not enough time has elapsed since the announcement of the unlock.';
      const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';
      await truffleAssert.reverts(qVault.withdraw(withdrawnAmount, { from: SOMEBODY }), reason);

      currentBalance = toBN(await qVault.balanceOf(SOMEBODY));
      assert.equal(currentBalance.toString(), deposit.toString());

      currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), lockedAmount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, available balance is less than the amount', async () => {
      await qVault.delegateStake([VALIDATOR1], [delegatedAmount.times(2)], { from: SOMEBODY });

      assert.equal(
        toBN(await qVault.testTotalDelegatedStake(SOMEBODY)).toString(),
        delegatedAmount.times(2).toString()
      );

      const reason = '[QEC-017014]-Insufficient funds to cover all delegations.'; // ';
      await truffleAssert.reverts(qVault.withdraw(deposit.dividedBy(4), { from: SOMEBODY }), reason);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, deposit does not exist', async () => {
      await truffleAssert.reverts(
        qVault.withdraw(100000, { from: DEFAULT }),
        '[QEC-017021]-Insufficient balance for withdrawal'
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.isTrue(await checkInvariant(currentLockInfo, qVault, SOMEBODY));
    });
  });

  describe('getLockInfo', () => {
    const deposit = toBN(10).pow(18);
    const amount = deposit.dividedBy(4);
    const lockedAmount = deposit.minus(amount);

    beforeEach('setup', async () => {
      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.lock(lockedAmount, { from: SOMEBODY });

      await qVault.announceUnlock(amount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(amount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());
    });

    it('should get correct information', async () => {
      const currentLockInfo = await qVault.getLockInfo({ from: SOMEBODY });

      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(amount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), amount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, token lock source is not contained in the list', async () => {
      const votingWeightProxy2 = await makeProxy(registry.address, VotingWeightProxy);
      await votingWeightProxy2.initialize(registry.address, ['governance.validators'], []);
      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy2.address);

      const reason = '[QEC-028001]-Unknown token lock source.'; // ';
      await truffleAssert.reverts(qVault.getLockInfo({ from: SOMEBODY }), reason);

      await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.isTrue(await checkInvariant(currentLockInfo, qVault, SOMEBODY));
    });
  });

  describe('lock', () => {
    const deposit = toBN(10).pow(18);
    const amount = deposit.dividedBy(4);

    beforeEach('initial deposit', async () => {
      await qVault.deposit({ from: SOMEBODY, value: deposit });
    });

    it('should lock amount', async () => {
      await qVault.lock(amount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(amount.toString(), toBN(currentLockInfo.lockedAmount).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should increase locked amount', async () => {
      await qVault.lock(amount, { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(amount.toString(), toBN(currentLockInfo.lockedAmount).toString());

      await qVault.lock(amount, { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(amount.times(2).toString(), toBN(currentLockInfo.lockedAmount).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to lock amount greater than available balance', async () => {
      await qVault.lock(amount, { from: SOMEBODY });

      const reason = '[QEC-017016]-The lock amount must not exceed the available balance.'; // ';
      await truffleAssert.reverts(qVault.lock(amount.times(4), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), amount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to lock zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await truffleAssert.reverts(qVault.lock(0, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.isTrue(await checkInvariant(currentLockInfo, qVault, SOMEBODY));
    });
  });

  describe('announceUnlock', () => {
    const deposit = toBN(10).pow(18);
    const announceUnlockAmount = deposit.dividedBy(2);
    const amount = deposit.dividedBy(4);

    beforeEach('setup', async () => {
      await qVault.deposit({ from: SOMEBODY, value: deposit.plus(amount) });
      await qVault.lock(deposit, { from: SOMEBODY });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
    });

    it('Should announce unlock successfully', async () => {
      await qVault.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should increase pending unlock amount', async () => {
      await qVault.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      const secondAnnouncedUnlockedAmount = announceUnlockAmount.plus(amount);
      await qVault.announceUnlock(secondAnnouncedUnlockedAmount, { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);

      const totalAnnounceUnlockedAmount = secondAnnouncedUnlockedAmount;
      assert.equal(
        toBN(currentLockInfo.lockedAmount).toString(),
        deposit.minus(totalAnnounceUnlockedAmount).toString()
      );
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), totalAnnounceUnlockedAmount.toString());
      assert.equal(toBN(currentLockInfo.lockedUntil).toString(), toBN(currentLockInfo.pendingUnlockTime).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to announce unlock amount greater than locked amount', async () => {
      const reason = '[QEC-028003]-Cannot unlock more than is currently locked.';
      await truffleAssert.reverts(qVault.announceUnlock(deposit.plus(1), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should successfully announce unlock zero amount', async () => {
      await qVault.announceUnlock(0, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), deposit.toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), 0);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.isTrue(await checkInvariant(currentLockInfo, qVault, SOMEBODY));
    });
  });

  describe('unlock', () => {
    const deposit = toBN(10).pow(18).times(2);
    const lockedAmount = deposit.dividedBy(2);
    const announceUnlockAmount = lockedAmount.dividedBy(2);
    const unlockAmount = announceUnlockAmount.dividedBy(2);

    beforeEach('setup', async () => {
      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.lock(lockedAmount, { from: SOMEBODY });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: SOMEBODY });

      await qVault.announceUnlock(announceUnlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), lockedAmount.minus(announceUnlockAmount).toString());
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());
    });

    it('Should unlock successfully', async () => {
      await setNextBlockTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await qVault.unlock(unlockAmount, { from: SOMEBODY });

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceUnlockAmount.minus(unlockAmount).toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should unlock successfully multiple times', async () => {
      await qVault.announceUnlock(announceUnlockAmount.times(2), { from: SOMEBODY });

      let currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.lockedAmount).toString(), 0);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.times(2).toString());

      await setNextBlockTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await qVault.unlock(unlockAmount, { from: SOMEBODY });
      await qVault.unlock(unlockAmount.times(2), { from: SOMEBODY });

      currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      const totalUnlockAmount = unlockAmount.times(3);
      assert.equal(
        toBN(currentLockInfo.pendingUnlockAmount).toString(),
        announceUnlockAmount.times(2).minus(totalUnlockAmount).toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to unlock before the expiration of the pending unlock time', async () => {
      const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';
      await truffleAssert.reverts(qVault.unlock(unlockAmount, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to unlock amount greater than pending unlock amount', async () => {
      const reason = '[QEC-028005]-Smart unlock not possible, tokens are still locked by recent voting.';
      await setNextBlockTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());

      await generalUpdateVote.createProposal('lorem ipsum');
      await generalUpdateVote.voteFor(1, { from: SOMEBODY });

      await truffleAssert.reverts(qVault.unlock(unlockAmount.times(4), { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should get exception, try to unlock zero amount', async () => {
      const reason = '[QEC-028002]-Invalid amount value, amount cannot be zero.';
      await setNextBlockTime((await getCurrentBlockTime()) + toBN(votingPeriod).plus(10).toNumber());
      await truffleAssert.reverts(qVault.unlock(0, { from: SOMEBODY }), reason);

      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.equal(toBN(currentLockInfo.pendingUnlockAmount).toString(), announceUnlockAmount.toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    afterEach('check invariant', async () => {
      const currentLockInfo = await votingWeightProxy.getLockInfo(qVault.address, SOMEBODY);
      assert.isTrue(await checkInvariant(currentLockInfo, qVault, SOMEBODY));
    });
  });

  describe('delegateStake', () => {
    beforeEach(async () => {
      await increasePoolBalances(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3], Q);
    });

    it('should deposit', async () => {
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      const delegationAmount = toBN(50);
      await qVault.delegateStake([VALIDATOR1], [delegationAmount], { from: SOMEBODY });

      const compoundRate = await validationRewardPools.getCompoundRate(VALIDATOR1);
      const normalized = Math.ceil(delegationAmount.times(DECIMAL).div(compoundRate));

      assert.equal(
        (await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR1)).toString(),
        delegationAmount.toString()
      );
      assert.equal(
        (await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR1)).toString(),
        normalized.toString()
      );
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR1)).aggregatedNormalizedStake).toString(),
        normalized.toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('QDEV-2222 should deposit correctly several times when compound rate > 1', async () => {
      // init cr
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      await setNextBlockTime((await getCurrentBlockTime()) + 30 * 24 * 3600);
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      await qVault.deposit({ from: SOMEBODY, value: Q });
      await qVault.delegateStake([VALIDATOR1], [Q.times(0.5)], { from: SOMEBODY });
      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await qVault.delegateStake([VALIDATOR1], [Q.times(0.2)], { from: SOMEBODY });
      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
    });

    it('should allow to claim when full balance is staked (QDEV-2557)', async () => {
      // set q holder reward cr and validator cr > 1
      await setNextBlockTime((await getCurrentBlockTime()) + 100);
      await qVault.updateCompoundRate();
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      // deposit and delegate
      await qVault.deposit({ from: SOMEBODY, value: Q });
      const balance = toBN(await qVault.balanceOf(SOMEBODY));
      await qVault.delegateStake([VALIDATOR1], [balance], { from: SOMEBODY });

      // set claimable reward > 0 and claim
      await setNextBlockTime((await getCurrentBlockTime()) + 200);
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);
      await qVault.claimStakeDelegatorReward({ from: SOMEBODY });

      const newBalance = toBN(await qVault.balanceOf(SOMEBODY));
      const newDelegations = await qVault.getDelegationsList(SOMEBODY);
      const actualStake = toBN(newDelegations[0].actualStake);

      assert.isTrue(
        newBalance.gte(actualStake),
        `The actual stake of ${actualStake} exceeds the user balance of ${newBalance}`
      );
    });

    it('should allow to delegate full balance (QDEV-2557)', async () => {
      // set q holder reward cr and validator cr > 1
      await setNextBlockTime((await getCurrentBlockTime()) + 100);
      await qVault.updateCompoundRate();
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      // deposit and delegate
      await qVault.deposit({ from: SOMEBODY, value: Q.times(2) });
      await qVault.delegateStake([VALIDATOR1], [Q], { from: SOMEBODY });

      // set claimable reward > 0
      await setNextBlockTime((await getCurrentBlockTime()) + 200);
      await validationRewardPools.updateValidatorsCompoundRate(VALIDATOR1);

      const delegations = await qVault.getDelegationsList(SOMEBODY);
      assert.equal(delegations.length, 1);
      assert.notEqual(delegations[0].claimableReward, '0');

      // calculate delegatable rest and delegate this
      const balance = toBN(await qVault.balanceOf(SOMEBODY));
      const alreadyDelegated = toBN(delegations[0].actualStake);
      const availableToDelegate = balance.minus(alreadyDelegated);

      await qVault.delegateStake([VALIDATOR2], [availableToDelegate], { from: SOMEBODY });
    });

    it('should deposit many stakes', async () => {
      const delegationAmount = toBN(50);
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      await qVault.delegateStake(
        [VALIDATOR1, VALIDATOR2, VALIDATOR3],
        [delegationAmount, delegationAmount, delegationAmount],
        { from: SOMEBODY }
      );

      const compoundRate1 = await validationRewardPools.getCompoundRate(VALIDATOR1);
      const normalized1 = Math.ceil(delegationAmount.times(DECIMAL).div(compoundRate1));
      const compoundRate2 = await validationRewardPools.getCompoundRate(VALIDATOR2);
      const normalized2 = Math.ceil(delegationAmount.times(DECIMAL).div(compoundRate2));
      const compoundRate3 = await validationRewardPools.getCompoundRate(VALIDATOR3);
      const normalized3 = Math.ceil(delegationAmount.times(DECIMAL).div(compoundRate3));

      assert.equal(
        (await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR1)).toString(),
        delegationAmount.toString()
      );
      assert.equal(
        (await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR1)).toString(),
        normalized1.toString()
      );
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR1)).aggregatedNormalizedStake).toString(),
        normalized1.toString()
      );

      assert.equal(
        (await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR2)).toString(),
        delegationAmount.toString()
      );
      assert.equal(
        (await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR2)).toString(),
        normalized2.toString()
      );
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR2)).aggregatedNormalizedStake).toString(),
        normalized2.toString()
      );

      assert.equal(
        (await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR3)).toString(),
        delegationAmount.toString()
      );
      assert.equal(
        (await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR3)).toString(),
        normalized3.toString()
      );
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR3)).aggregatedNormalizedStake).toString(),
        normalized3.toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should update stakes', async () => {
      await qVault.deposit({ from: SOMEBODY, value: Q.times(1000) });

      const firstValidators = [VALIDATOR1, VALIDATOR2, VALIDATOR3];
      const firstStakes = [Q.times(50), Q.times(50), Q.times(50)];
      await qVault.delegateStake(firstValidators, firstStakes, { from: SOMEBODY });

      const secondValidators = [VALIDATOR1, VALIDATOR2];
      const secondStakes = [Q.times(100), Q.times(150)];
      await qVault.delegateStake(secondValidators, secondStakes, { from: SOMEBODY });

      const delegationAmounts = [secondStakes[0], secondStakes[1], firstStakes[2]];
      const resultValidators = firstValidators;

      for (let i = 0; i < resultValidators.length; i++) {
        const compoundRate = await validationRewardPools.getCompoundRate(resultValidators[i]);
        const normalized = toBN(
          delegationAmounts[i].multipliedBy(DECIMAL).div(compoundRate).toFixed(0, BigNumber.ROUND_CEIL)
        );

        assert.equal(
          (await qVault.testValidatorsActualStakes(SOMEBODY, resultValidators[i])).toString(),
          delegationAmounts[i].toString()
        );
        assert.equal(
          (await qVault.testValidatorsNormalizedStakes(SOMEBODY, resultValidators[i])).toString(),
          normalized.toString()
        );
        assert.equal(
          toBN(
            (await validationRewardPools.validatorsProperties(resultValidators[i])).aggregatedNormalizedStake
          ).toString(),
          normalized.toString()
        );
      }

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should not remove stakes that do not changed', async () => {
      await qVault.deposit({ from: SOMEBODY, value: Q.times(1000) });
      await qVault.delegateStake([VALIDATOR1, VALIDATOR2, VALIDATOR3], [Q.times(50), Q.times(50), Q.times(50)], {
        from: SOMEBODY,
      });

      await qVault.delegateStake([VALIDATOR1, VALIDATOR2], [Q.times(100), Q.times(150)], { from: SOMEBODY });

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR1)).toString(), Q.times(100).toString());

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR2)).toString(), Q.times(150).toString());

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR3)).toString(), Q.times(50).toString());

      const listOfdelegatedTo = await qVault.testListOfDelegatedTo(SOMEBODY);

      assert.equal(listOfdelegatedTo.length, 3);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should delete stakes', async () => {
      const delegationAmount = toBN(50);
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      await qVault.delegateStake(
        [VALIDATOR1, VALIDATOR2, VALIDATOR3],
        [delegationAmount, delegationAmount, delegationAmount],
        { from: SOMEBODY }
      );

      await qVault.delegateStake([VALIDATOR1, VALIDATOR2], [toBN(0), toBN(0)], { from: SOMEBODY });

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR1)).toString(), toBN(0).toString());
      assert.equal((await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR1)).toString(), toBN(0).toString());
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR1)).aggregatedNormalizedStake).toString(),
        toBN(0).toString()
      );

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR2)).toString(), toBN(0).toString());
      assert.equal((await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR2)).toString(), toBN(0).toString());
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR2)).aggregatedNormalizedStake).toString(),
        toBN(0).toString()
      );

      const compoundRate3 = await validationRewardPools.getCompoundRate(VALIDATOR3);
      const normalized3 = Math.ceil(delegationAmount.times(DECIMAL).div(compoundRate3));

      assert.equal(
        (await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR3)).toString(),
        delegationAmount.toString()
      );
      assert.equal(
        (await qVault.testValidatorsNormalizedStakes(SOMEBODY, VALIDATOR3)).toString(),
        normalized3.toString()
      );
      assert.equal(
        toBN((await validationRewardPools.validatorsProperties(VALIDATOR3)).aggregatedNormalizedStake).toString(),
        normalized3.toString()
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should delete and update stakes', async () => {
      await qVault.deposit({ from: SOMEBODY, value: Q.times(1000) });
      await qVault.delegateStake([VALIDATOR1, VALIDATOR2, VALIDATOR3], [Q.times(50), Q.times(50), Q.times(50)], {
        from: SOMEBODY,
      });

      await qVault.delegateStake(
        [VALIDATOR1, VALIDATOR2, VALIDATOR3, VALIDATOR4],
        [Q.times(0), Q.times(150), Q.times(50), Q.times(50)],
        { from: SOMEBODY }
      );

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR1)).toString(), Q.times(0).toString());

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR2)).toString(), Q.times(150).toString());

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR3)).toString(), Q.times(50).toString());

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR4)).toString(), Q.times(50).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('QDEV-2449 balance invariant must be satisfied', async () => {
      const depositAmount = Q.times(100);
      const amountToWithdraw = Q.times(10);

      const poolBalance = Q.times(200000);
      await qHolderRewardPool.send(poolBalance);

      await qVault.updateCompoundRate();
      await qVault.deposit({ from: SOMEBODY, value: depositAmount });
      await qVault.withdraw(amountToWithdraw, { from: SOMEBODY });

      await setNextBlockTime((await getCurrentBlockTime()) + 100);

      await qVault.updateCompoundRate();
      await qVault.deposit({ from: SOMEBODY, value: depositAmount });
      await qVault.withdraw(amountToWithdraw, { from: SOMEBODY });
    });

    it('should fail if validator does not have a CompoundRateKeeper', async () => {
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });

      const reason = '[QEC-016007]-CompoundRateKeeper not initialized for given validator.';
      await truffleAssert.reverts(qVault.delegateStake([VALIDATOR5], [toBN(50)], { from: SOMEBODY }), reason);

      assert.equal((await qVault.testValidatorsActualStakes(SOMEBODY, VALIDATOR5)).toString(), toBN(0).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await truffleAssert.reverts(updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR5]), reason);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should fail when too many delegators', async () => {
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      await truffleAssert.reverts(
        qVault.delegateStake([SOMEBODY, DEFAULT, await accounts(3), await accounts(4)], [50, 20, 10, 30], {
          from: SOMEBODY,
        }),
        '[QEC-017013]-The limit of candidates for the delegation has been exceeded, stake delegation failed.' // ',
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should fail when number of params mismatch', async () => {
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      // ';
      const reason = '[QEC-017012]-The number of candidates and stakes should be the same, stake delegation failed.';
      await truffleAssert.reverts(qVault.delegateStake([SOMEBODY], [50, 20], { from: SOMEBODY }), reason);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should fail when delegating more than have', async () => {
      await qVault.deposit({ from: SOMEBODY, value: toBN(1000000000000000000) });
      await truffleAssert.reverts(
        qVault.delegateStake([SOMEBODY], [toBN(2000000000000000000)], { from: SOMEBODY }),
        '[QEC-017014]-Insufficient funds to cover all delegations.' // ',
      );

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });
  });

  describe('getDelegationsList', () => {
    beforeEach(async () => {
      await increasePoolBalances(validationRewardPools, [VALIDATOR1, VALIDATOR2, VALIDATOR3], Q);
    });

    it('should get list of delegations', async () => {
      const delegatedTo = [VALIDATOR1, VALIDATOR2, VALIDATOR3];
      await qVault.deposit({ from: SOMEBODY, value: Q.times(500) });

      const stakesToDelegate = [Q.times(50), Q.times(100), Q.times(150)];
      await qVault.delegateStake(delegatedTo, stakesToDelegate, { from: SOMEBODY });

      const actualStakes = [
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[1])),
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[2])),
      ];

      const normalizedStakes = [
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[1])),
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[2])),
      ];

      const rates = [
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[0])),
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[1])),
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[2])),
      ];

      const lastClaims = [
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[0])),
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[1])),
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[2])),
      ];

      const list = await qVault.getDelegationsList.call(SOMEBODY);

      assert.equal(list.length, delegatedTo.length);
      for (let i = 0; i < list.length; i++) {
        assert.equal(list[i].validator.toString(), delegatedTo[i].toString());
        assert.equal(toBN(list[i].actualStake).toString(), actualStakes[i].toString());
        assert.equal(toBN(list[i].normalizedStake).toString(), normalizedStakes[i].toString());

        assert.equal(toBN(list[i].compoundRate).toString(), rates[i].toString());
        assert.equal(toBN(list[i].latestUpdateOfCompoundRate).toString(), lastClaims[i].toString());

        assert.equal(
          toBN(list[i].idealStake).toString(),
          rates[i].multipliedBy(normalizedStakes[i]).div(DECIMAL).toFixed(0).toString()
        );
        assert.equal(toBN(list[i].claimableReward).toString(), toBN(0).toString()); // check for zero
      }

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, delegatedTo);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should get list of delegations after rate update', async () => {
      const delegatedTo = [VALIDATOR1, VALIDATOR2, VALIDATOR3];
      await qVault.deposit({ from: SOMEBODY, value: Q.times(500) });

      const stakesToDelegate = [Q.times(50), Q.times(100), Q.times(150)];
      await qVault.delegateStake(delegatedTo, stakesToDelegate, { from: SOMEBODY });

      await updateValidatorsCompoundRate(validationRewardPools, delegatedTo);

      const actualStakes = [
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[1])),
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[2])),
      ];

      const normalizedStakes = [
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[1])),
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[2])),
      ];

      const rates = [
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[0])),
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[1])),
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[2])),
      ];

      const lastClaims = [
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[0])),
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[1])),
        toBN(await validationRewardPools.getLastUpdateOfCompoundRate.call(delegatedTo[2])),
      ];

      const list = await qVault.getDelegationsList(SOMEBODY);

      const expectedIdealStakes = ['51000000000000000000', '101000000000000000000', '150999999999999999900'];
      const expectedClaimableRewards = ['1000000000000000000', '1000000000000000000', '999999999999999900'];

      assert.equal(list.length, delegatedTo.length);
      for (let i = 0; i < list.length; i++) {
        assert.equal(list[i].validator.toString(), delegatedTo[i].toString());
        assert.equal(toBN(list[i].actualStake).toString(), actualStakes[i].toString());
        assert.equal(toBN(list[i].normalizedStake).toString(), normalizedStakes[i].toString());

        assert.equal(toBN(list[i].compoundRate).toString(), rates[i].toString());
        assert.equal(toBN(list[i].latestUpdateOfCompoundRate).toString(), lastClaims[i].toString());

        assert.equal(toBN(list[i].idealStake).toString(), expectedIdealStakes[i]);
        assert.equal(toBN(list[i].claimableReward).toString(), expectedClaimableRewards[i]);
      }

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY]);

      await updateValidatorsCompoundRate(validationRewardPools, delegatedTo);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });
  });

  describe('claimStakeDelegatorReward()', () => {
    const DEPOSIT = Q;
    const stakesToDelegate = [toBN(600000000000000000), toBN(400000000000000000)];

    it('should be possible to claim', async () => {
      const delegatedTo = [VALIDATOR1, VALIDATOR2];
      await reserveValidatorsAdditionalFunds(validationRewardPools, Q, delegatedTo);

      await qVault.deposit({ from: SOMEBODY, value: toBN(DEPOSIT) });
      await qVault.delegateStake(delegatedTo, stakesToDelegate, { from: SOMEBODY });

      const poolsBalances = [Q, Q];
      await validationRewardPools.increase(delegatedTo[0], { value: poolsBalances[0] });
      await validationRewardPools.increase(delegatedTo[1], { value: poolsBalances[1] });

      const balanceBeforeClaim = toBN(await qVault.balanceOf(SOMEBODY));
      const actualStakes = [
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[1])),
      ];
      const normalizedStakes = [
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[0])),
        toBN(await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[1])),
      ];

      await updateValidatorsCompoundRate(validationRewardPools, delegatedTo);

      const rates = [
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[0])),
        toBN(await validationRewardPools.getCompoundRate.call(delegatedTo[1])),
      ];

      await qVault.claimStakeDelegatorReward({ from: SOMEBODY });

      const balanceAfterClaim = toBN(await qVault.balanceOf(SOMEBODY));

      let expectedReward = toBN(0);

      for (let i = 0; i < delegatedTo.length; i++) {
        const expectedNewActualStake = rates[i].multipliedBy(normalizedStakes[i]).div(DECIMAL);
        const delegatorReward = expectedNewActualStake.minus(actualStakes[i]).toFixed(0, BigNumber.ROUND_DOWN);
        expectedReward = expectedReward.plus(delegatorReward);

        const newActualStake = await qVault.testValidatorsActualStakes(SOMEBODY, delegatedTo[i]);
        assert.equal(
          newActualStake.toString(),
          stakesToDelegate[i].plus(delegatorReward),
          `Wrong reward for validator ${delegatedTo[i]}`
        );

        assert.equal(
          (await qVault.testValidatorsNormalizedStakes(SOMEBODY, delegatedTo[i])).toString(),
          normalizedStakes[i].toString(),
          `Wrong normalized stake for validator ${delegatedTo[i]}`
        );
        assert.equal(
          toBN((await validationRewardPools.validatorsProperties(delegatedTo[i])).aggregatedNormalizedStake).toString(),
          normalizedStakes[i].toString(),
          `Wrong aggregated normalized stake for validator ${delegatedTo[i]}`
        );
      }

      const actualReward = balanceAfterClaim.minus(balanceBeforeClaim);

      assert.equal(actualReward.toString(), expectedReward.toString(), `wrong overall reward`);

      await updateValidatorsCompoundRate(validationRewardPools, delegatedTo);

      await updateValidatorsCompoundRate(validationRewardPools, [VALIDATOR1, VALIDATOR2]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should be possible to claim if the deposit does not exist', async () => {
      await qVault.claimStakeDelegatorReward({ from: SOMEBODY });
      await qVault.deposit({ value: Q.times(1), from: SOMEBODY });
      await qVault.delegateStake([VALIDATOR1], [Q.times(1)], { from: SOMEBODY });
      await qVault.claimStakeDelegatorReward({ from: SOMEBODY });

      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });
  });

  describe('updateCompoundRate', () => {
    const deposit = toBN(10000);

    it('should revert with msg', async () => {
      const poolBalance = Q.times(10000);
      await qHolderRewardPool.send(poolBalance);

      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.deposit({ from: VALIDATOR1, value: deposit });
      await setNextBlockTime((await getCurrentBlockTime()) + 2500);
      await truffleAssert.reverts(qVault.updateCompoundRate(), '[QEC-015001]-Pool balance is not enough for request.');
    });

    it('should be possible to update compound rate', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());

      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.deposit({ from: VALIDATOR1, value: deposit });

      const poolBalance = Q.times(10000);
      await qHolderRewardPool.send(poolBalance);

      const oldRate = toBN(await compoundRateKeeper.getCurrentRate());
      const oldQVaultBalance = toBN(await web3.eth.getBalance(qVault.address));
      const normalizedBalance = toBN(await qVault.aggregatedNormalizedBalance());

      const lastClaimStartTime = toBN(await compoundRateKeeper.getLastUpdate()).toNumber();
      await setNextBlockTime((await getCurrentBlockTime()) + lastClaimStartTime + 50);
      await qVault.updateCompoundRate();

      const newRate = toBN(await compoundRateKeeper.getCurrentRate());
      const newQVaultBalance = toBN(await web3.eth.getBalance(qVault.address));

      const accruedReward = normalizedBalance.times(newRate.minus(oldRate)).div(DECIMAL).integerValue();

      assert.closeTo(accruedReward.toNumber(), newQVaultBalance.minus(oldQVaultBalance).toNumber(), 1);

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('should not be possible to update compound rate if reward is not enough', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());

      await qVault.deposit({ from: SOMEBODY, value: deposit });
      await qVault.deposit({ from: VALIDATOR1, value: deposit });

      const poolBalance = Q.times(0);
      await qHolderRewardPool.send(poolBalance);

      const oldRate = toBN(await compoundRateKeeper.getCurrentRate());
      const oldQVaultBalance = toBN(await web3.eth.getBalance(qVault.address));
      const normalizedBalance = toBN(await qVault.aggregatedNormalizedBalance());

      const lastClaimStartTime = toBN(await compoundRateKeeper.getLastUpdate()).toNumber();
      await setNextBlockTime((await getCurrentBlockTime()) + lastClaimStartTime);
      await truffleAssert.reverts(qVault.updateCompoundRate());

      const newRate = toBN(await compoundRateKeeper.getCurrentRate());
      const newQVaultBalance = toBN(await web3.eth.getBalance(qVault.address));

      const accruedReward = normalizedBalance.times(newRate.minus(oldRate));

      assert.equal(accruedReward.toString(), newQVaultBalance.minus(oldQVaultBalance).toString());

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });

    it('QDEV-3244 should not fail during multiple updating a compound rate with different interest rate', async () => {
      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());
      let currTime = toBN(await compoundRateKeeper.getLastUpdate());

      await qVault.deposit({ from: VALIDATOR1, value: deposit });
      const poolBalance = Q.times(100);
      await qHolderRewardPool.send(poolBalance);

      for (let i = 0; i < 2; i++) {
        currTime = currTime.plus(1);
        await setNextBlockTime((await getCurrentBlockTime()) + currTime.toNumber());

        await qVault.updateCompoundRate();

        const percentage = 2 * (i % 2) + 0.01;
        currTime = await changeInterestRate(epqfiParametersVoting, EXPERT, votingPeriod, percentage, currTime);
      }

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
      await checkReservedClaimsAmount(qVault, validationRewardPools, [SOMEBODY]);
    });
  });

  describe('getBalanceDetails()', () => {
    it('should be possible to get balance details', async () => {
      const deposit = toBN(10000000);
      await qVault.deposit({ from: SOMEBODY, value: deposit });

      const poolBalance = Q.times(10000);
      await qHolderRewardPool.send(poolBalance);

      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());
      const lastUpdate = toBN(await compoundRateKeeper.getLastUpdate());

      await setNextBlockTime((await getCurrentBlockTime()) + lastUpdate.plus(10).toNumber());

      await qVault.updateCompoundRate();

      const balanceDetails = await qVault.getBalanceDetails({ from: SOMEBODY });

      assert.equal(balanceDetails.currentBalance, await qVault.balanceOf(SOMEBODY));
      assert.equal(balanceDetails.normalizedBalance, await qVault.getNormalizedBalance(SOMEBODY));
      assert.equal(balanceDetails.compoundRate, await compoundRateKeeper.getCurrentRate());
      assert.equal(balanceDetails.lastUpdateOfCompoundRate, await compoundRateKeeper.getLastUpdate());
      assert.equal(balanceDetails.interestRate, qRewardPoolInterest.toString());
    });
  });

  describe('ERC20 metadata functions', () => {
    it('should get erc20 metadata', async () => {
      const name = await qVault.name();
      const decimals = await qVault.decimals();
      const symbol = await qVault.symbol();
      assert.equal(name, 'Vault Q');
      assert.equal(decimals, 18);
      assert.equal(symbol, 'VQ');
    });
  });

  describe('transfer()', () => {
    let lastClaimStartTime;
    beforeEach(async () => {
      const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());
      lastClaimStartTime = toBN(await compoundRateKeeper.getLastUpdate());
    });

    it('should be correct transfer', async () => {
      await setNextBlockTime((await getCurrentBlockTime()) + lastClaimStartTime.toNumber());
      await qVault.deposit({ from: SOMEBODY, value: Q.times(100) });
      const transferAmount = Q.times(30);

      const totalSupplyBefore = toBN(await qVault.totalSupply());
      const validatorBalanceBefore = toBN(await qVault.balanceOf(VALIDATOR1));
      const somebodyBalanceBefore = toBN(await qVault.balanceOf(SOMEBODY));
      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
      const tx = await qVault.transfer(VALIDATOR1, transferAmount, { from: SOMEBODY });
      truffleAssert.eventEmitted(tx, 'Transfer');
      const currentSomebodyBalance = toBN(await qVault.balanceOf(SOMEBODY));
      const currentValidatorBalance = toBN(await qVault.balanceOf(VALIDATOR1));
      const totalSupply = toBN(await qVault.totalSupply());
      assert.equal(totalSupplyBefore.toString(), totalSupply.toString(), 'check totalSupply');
      assert.equal(
        validatorBalanceBefore.plus(somebodyBalanceBefore).toString(),
        currentValidatorBalance.plus(currentSomebodyBalance).toString(),
        'checkSum'
      );
      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
    });
    it('should be reverted transfer', async () => {
      await qVault.deposit({ from: SOMEBODY, value: Q.times(100) });
      const transferAmount = Q.times(101);
      await truffleAssert.reverts(qVault.transfer(VALIDATOR1, transferAmount, { from: VALIDATOR2 }), '[QEC-017021]');

      await checkInvariantNormalizedBalance(qVault, [SOMEBODY, VALIDATOR1]);
    });
  });

  describe('allowances', () => {
    it('should be approved', async () => {
      await qVault.approve(VALIDATOR1, Q.times(20), { from: SOMEBODY });
      let allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR1));
      assert.equal(allowed.toString(), Q.times(20).toString(), 'must be approved 20 Q ');

      await qVault.approve(VALIDATOR2, Q.times(30), { from: SOMEBODY });
      allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR2));
      assert.equal(allowed.toString(), Q.times(30).toString(), 'must be approved 30 Q ');

      await qVault.approve(VALIDATOR1, Q.times(20), { from: SOMEBODY });
      allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR1));
      assert.equal(allowed.toString(), Q.times(20).toString(), 'must be approved 20 Q yet');

      await qVault.increaseAllowance(VALIDATOR1, Q.times(10), { from: SOMEBODY });
      allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR1));
      assert.equal(allowed.toString(), Q.times(30).toString(), 'must be approved 30 Q ');

      await qVault.decreaseAllowance(VALIDATOR2, Q.times(10), { from: SOMEBODY });
      allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR2));
      assert.equal(allowed.toString(), Q.times(20).toString(), 'must be approved 20 Q ');

      truffleAssert.reverts(
        qVault.decreaseAllowance(VALIDATOR2, Q.times(10), { from: SOMEBODY }),
        '[QEC-017020]-The allowance is too low for this operation.' // ',
      );
    });

    it('should be spent', async () => {
      await qVault.deposit({ from: SOMEBODY, value: Q.times(100) });

      await qVault.approve(VALIDATOR1, Q.times(20), { from: SOMEBODY });
      const allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR1));
      assert.equal(allowed.toString(), Q.times(20).toString(), 'must be approved 20 Q ');

      await qVault.transferFrom(SOMEBODY, DEFAULT, Q.times(15), { from: VALIDATOR1 });
      assert.equal(
        (await qVault.allowance(SOMEBODY, VALIDATOR1)).toString(),
        Q.times(5).toString(),
        'allowance should be 5Q'
      );
      truffleAssert.reverts(
        qVault.transferFrom(SOMEBODY, DEFAULT, Q.times(6), { from: VALIDATOR1 }),
        '[QEC-017020]-The allowance is too low for this operation.'
      ); // ');
      assert.equal((await qVault.balanceOf(DEFAULT)).toString(), Q.times(15).toString(), 'DEFAULT should get 15 Q');
      assert.equal((await qVault.balanceOf(SOMEBODY)).toString(), Q.times(85).toString(), 'SOMEBODY should get 85 Q');
    });

    it('QDEV-2964 lockInfo issue. Should successfully transfer via transferFrom ', async () => {
      const depositAmount = Q.times(100);
      const lockAmount = depositAmount.times(2);
      const allowanceAmount = Q.times(20);
      const transferAmount = Q.times(15);

      await qVault.deposit({ from: SOMEBODY, value: depositAmount });
      await qVault.approve(VALIDATOR1, allowanceAmount, { from: SOMEBODY });
      const allowed = toBN(await qVault.allowance(SOMEBODY, VALIDATOR1));
      assert.equal(allowed.toString(), allowanceAmount.toString(), 'must be approved 20 Q ');

      await qVault.deposit({ from: VALIDATOR1, value: depositAmount.times(2) });
      await qVault.lock(lockAmount, { from: VALIDATOR1 });
      await qVault.transferFrom(SOMEBODY, DEFAULT, transferAmount, { from: VALIDATOR1 });
    });
  });

  describe('transferAndCall', () => {
    it('should be transfer and data received', async () => {
      const SOMEDATA = web3.utils.asciiToHex('TestData');
      const reciever = await makeProxy(registry.address, ERC677TransferReceiverMock);
      await qVault.deposit({ from: SOMEBODY, value: Q.times(10) });
      const tx = await qVault.transferAndCall(reciever.address, Q.times(5), SOMEDATA, { from: SOMEBODY });
      truffleAssert.eventEmitted(tx, 'Transfer', (ev) => {
        return ev.data === SOMEDATA;
      });
      const gotData = await reciever.getLastData();
      assert.equal(gotData, SOMEDATA);
    });
  });

  describe('depositTo', () => {
    it('should be deposited some value', async () => {
      await qVault.depositTo(VALIDATOR1, { value: Q.times(100), from: SOMEBODY });
      assert.equal(
        (await qVault.balanceOf(VALIDATOR1)).toString(),
        Q.times(100).toString(),
        'should be deposited 100 Q to validator'
      );
    });
  });

  describe('withdrawTo', () => {
    it('should be withdrawn to VALIDATOR1', async () => {
      const oldBalance = toBN(await web3.eth.getBalance(VALIDATOR1));
      await qVault.deposit({ value: Q.times(100), from: SOMEBODY });
      await qVault.withdrawTo(VALIDATOR1, Q.times(100), { from: SOMEBODY });
      assert.equal(
        toBN(await web3.eth.getBalance(VALIDATOR1)).toString(),
        oldBalance.plus(Q.times(100)).toString(),
        'should be got 100 Q by validator'
      );
    });
  });

  describe('QDEV-2757 TimeLock', () => {
    const depositStakeAmount = Q.times(10);
    const depositOnBehalfStakeAmount = Q.times(10);
    const maxTimelocksNumber = toBN(10);
    let releaseStart;
    let releaseEnd;

    beforeEach('setup', async () => {
      releaseStart = toBN((await getCurrentBlockTime()) + 1000);
      releaseEnd = toBN((await getCurrentBlockTime()) + 2000);
      await qVault.deposit({ from: VALIDATOR1, value: depositStakeAmount });
      await qVault.depositOnBehalfOf(VALIDATOR1, releaseStart, releaseEnd, {
        from: VALIDATOR1,
        value: depositOnBehalfStakeAmount,
      });
    });

    it('should be withdrawn', async () => {
      await qVault.withdraw(Q.times(10), { from: VALIDATOR1 });
    });

    it('should be reverted when withdraw immediately', async () => {
      const reason = '[QEC-017022]';
      await truffleAssert.reverts(
        qVault.withdraw(Q.times(15), { from: VALIDATOR1 }),
        reason,
        'withdraw must failed cause not enough time passed'
      );

      await setNextBlockTime((await getCurrentBlockTime()) + 1500);
      await qVault.withdraw(Q.times(15), { from: VALIDATOR1 });
      const balance = await qVault.balanceOf(VALIDATOR1);
      assertEqualBN(balance, Q.times(5));
    });

    it('should be locked with voting', async () => {
      await qVault.lock(Q.times(5), { from: VALIDATOR1 });
      await generalUpdateVote.createProposal('');
      await generalUpdateVote.voteFor(0, { from: VALIDATOR1 });

      await truffleAssert.reverts(qVault.withdraw(Q.times(15), { from: VALIDATOR1 }));
      await setNextBlockTime((await getCurrentBlockTime()) + 1500);
      await qVault.withdraw(Q.times(15), { from: VALIDATOR1 });
    });

    it('should be locked with delegated', async () => {
      await qVault.deposit({ value: Q.times(10), from: SOMEBODY });
      await qVault.delegateStake([VALIDATOR1], [Q.times(5)], { from: SOMEBODY });

      await truffleAssert.reverts(qVault.withdraw(Q.times(15), { from: VALIDATOR1 }));
      await setNextBlockTime((await getCurrentBlockTime()) + 1500);
      await qVault.withdraw(Q.times(15), { from: VALIDATOR1 });
    });

    it('should get exception, try to deposit less than minimum amount', async () => {
      const wrongDepositAmount = Q.times(10).minus(1);

      const reason = '[QEC-032002]-Given amount is below deposit minimum.';
      const releaseStart_ = toBN((await getCurrentBlockTime()) + 1000);
      await truffleAssert.reverts(
        qVault.depositOnBehalfOf(VALIDATOR1, releaseStart_, toBN((await getCurrentBlockTime()) + 2000), {
          from: VALIDATOR1,
          value: wrongDepositAmount,
        }),
        reason
      );

      const lockInfo = await qVault.getLockInfo({ from: VALIDATOR1 });
      const minimumBalance = await qVault.getMinimumBalance(VALIDATOR1, releaseStart);
      assertEqualBN(await qVault.balanceOf(VALIDATOR1), depositStakeAmount.plus(depositOnBehalfStakeAmount));
      assertEqualBN(lockInfo.lockedAmount, 0);
      assertEqualBN(minimumBalance, depositStakeAmount);
    });

    it('should get exception, try to deposit maxTimelocksNumber + 1 times', async () => {
      for (let i = 1; i < maxTimelocksNumber; i++) {
        qVault.depositOnBehalfOf(
          VALIDATOR1,
          toBN((await getCurrentBlockTime()) + 1000),
          toBN((await getCurrentBlockTime()) + 2000),
          { from: VALIDATOR1, value: depositStakeAmount }
        );
      }

      const reason = '[QEC-032003]-Failed to deposit amount, too many timelocks.';
      await truffleAssert.reverts(
        qVault.depositOnBehalfOf(
          VALIDATOR1,
          toBN((await getCurrentBlockTime()) + 1000),
          toBN((await getCurrentBlockTime()) + 2000),
          { from: VALIDATOR1, value: depositStakeAmount }
        ),
        reason
      );

      const firstLockInfo = await qVault.getLockInfo({ from: VALIDATOR1 });
      const minimumBalance = await qVault.getMinimumBalance(VALIDATOR1, releaseStart);
      assertEqualBN(
        await qVault.balanceOf(VALIDATOR1),
        depositOnBehalfStakeAmount.times(maxTimelocksNumber).plus(depositStakeAmount)
      );
      assertEqualBN(firstLockInfo.lockedAmount, 0);
      assertEqualBN(minimumBalance, depositOnBehalfStakeAmount.times(maxTimelocksNumber));
    });
    it('should get exception, try to deposit for too long', async () => {
      await truffleAssert.reverts(
        qVault.depositOnBehalfOf(
          VALIDATOR1,
          toBN((await getCurrentBlockTime()) + 1000),
          toBN((await getCurrentBlockTime()) + 64_060_588_861),
          { from: VALIDATOR1, value: depositStakeAmount }
        ),
        '[QEC-032005]'
      );
    });
  });

  describe('QDEV-3168 Prevent multiple votes by delegation', () => {
    const depositStakeAmount = Q.times(1000);
    const lockAmount = Q.times(600);
    const selfAmount = Q.times(2);

    beforeEach(async () => {
      await qVault.deposit({ from: VALIDATOR1, value: depositStakeAmount });
      await generalUpdateVote.createProposal('');
    });

    it('there should be no reuse of tokens for voting', async () => {
      await qVault.lock(lockAmount, { from: VALIDATOR1 });

      await votingWeightProxy.announceNewVotingAgent(VALIDATOR2, { from: VALIDATOR1 });
      await votingWeightProxy.setNewVotingAgent({ from: VALIDATOR1 });

      await generalUpdateVote.voteFor(0, { from: VALIDATOR2 });

      assert.equal(await generalUpdateVote.hasUserVoted(0, VALIDATOR2), true);
      assert.equal(
        toBN((await generalUpdateVote.proposals(0)).counters.weightFor).toString(),
        lockAmount.plus(selfAmount).toString()
      );

      await qVault.announceUnlock(lockAmount, { from: VALIDATOR1 });

      const reason = '[QEC-028004]-Not enough time has elapsed since the announcement of the unlock.';
      await truffleAssert.reverts(qVault.unlock(lockAmount, { from: VALIDATOR1 }), reason);
    });
  });
});

async function checkInvariant(currentLockInfo, qVault, userAddress) {
  const totalLockedAmount = toBN(currentLockInfo.lockedAmount).plus(currentLockInfo.pendingUnlockAmount);
  const currentBalance = toBN(await qVault.balanceOf(userAddress));
  return currentBalance.gte(totalLockedAmount);
}

async function checkInvariantNormalizedBalance(qVault, userAddresses) {
  const currentNormalizedBalance = toBN(await qVault.aggregatedNormalizedBalance());

  const compoundRateKeeper = await CompoundRateKeeper.at(await qVault.compoundRateKeeper());
  const compoundRate = toBN(await compoundRateKeeper.getCurrentRate());

  const calculatedBalance = currentNormalizedBalance.times(compoundRate).idiv(DECIMAL).toFixed();

  let countedBalance = toBN(0);
  for (let i = 0; i < userAddresses.length; i++) {
    const balance = toBN(await qVault.balanceOf(userAddresses[i]));
    countedBalance = countedBalance.plus(balance);
  }

  const actualContractBalance = toBN(await web3.eth.getBalance(qVault.address));

  assert.isAtLeast(
    +actualContractBalance,
    +calculatedBalance,
    `Insufficient contract balance to cover entire QVault holdings`
  );

  assert.isAtLeast(+calculatedBalance, +countedBalance, `QVault holdings are less than the sum of individual balances`);
}

async function checkReservedClaimsAmount(qVault, validationPools, delegators) {
  for (let j = 0; j < delegators.length; j++) {
    const delegationsList = await qVault.getDelegationsList.call(delegators[j]);

    for (let i = 0; i < delegationsList.length; i++) {
      const validator = delegationsList[i].validator;
      const currentRate = await validationPools.getCompoundRate.call(validator);
      const claimableReward = toBN(
        toBN(delegationsList[i].normalizedStake).multipliedBy(currentRate).dividedBy(DECIMAL).toFixed(0)
      ).minus(delegationsList[i].actualStake);

      const validatorInfo = await validationPools.validatorsProperties(validator);
      const reservedForClaims = toBN(validatorInfo.reservedForClaim).toNumber();
      assert.isAtLeast(
        reservedForClaims,
        claimableReward.toNumber(),
        `Not enough Q reserved for claims against ${validator}`
      );
    }
  }
}

async function updateValidatorsCompoundRate(validationRewardPools, delegatedTo) {
  for (let i = 0; i < delegatedTo.length; i++) {
    await validationRewardPools.updateValidatorsCompoundRate(delegatedTo[i]);
  }
}

async function reserveValidatorsAdditionalFunds(validationRewardPools, amount, delegatedTo) {
  for (let i = 0; i < delegatedTo.length; i++) {
    await validationRewardPools.reserveAdditionalFunds(delegatedTo[i], { value: amount });
  }
}

async function increasePoolBalances(validationRewardPools, delegatedTo, amount) {
  for (let i = 0; i < delegatedTo.length; i++) {
    await validationRewardPools.increase(delegatedTo[i], { value: amount });
  }
}

async function changeInterestRate(paramVoting, expert, votingPeriod, percentage, currTime = 1) {
  await setNextBlockTime((await getCurrentBlockTime()) + Number(currTime));
  const proposalId = (await paramVoting.proposalCount.call()).toString();
  await paramVoting.createUintProposal('', 'governed.EPQFI.Q_rewardPoolInterest', getPercentageFormat(percentage), {
    from: expert,
  });
  await paramVoting.voteFor(proposalId, { from: expert });
  currTime = currTime.plus(votingPeriod).plus(10);
  await setNextBlockTime((await getCurrentBlockTime()) + currTime.toNumber());
  await paramVoting.execute(proposalId);

  return currTime;
}
