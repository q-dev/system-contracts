const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN } = require('./helpers/defiHelper');

const { setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ContractRegistry = artifacts.require('./ContractRegistry');
const SystemReserve = artifacts.require('./SystemReserve');
const SystemReserveHelper = artifacts.require('./SystemReserveHelper');
const DefaultAllocationProxy = artifacts.require('./DefaultAllocationProxy');
const Roots = artifacts.require('./Roots');
const EPQFImembership = artifacts.require('./EPQFI_Membership');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const Parameters = artifacts.require('./EPQFI_Parameters');

describe('SystemReserve', () => {
  const reverter = new Reverter();

  const Q = require('./helpers/defiHelper').Q;

  let registry;
  let systemReserve;
  let systemReserveHelper;
  let defaultAllocationProxy;
  let roots;
  let epqfiMembership;
  let epqfiParameters;

  let AUCTION;
  let NOT_OWNER;
  let ROOT;

  before('setup', async () => {
    AUCTION = await accounts(0);
    NOT_OWNER = await accounts(1);
    ROOT = await accounts(2);

    registry = await ContractRegistry.new();
    await registry.initialize([AUCTION], ['defi.QUSD.systemDebtAuction'], [AUCTION]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    const parametersInitialList = ['governed.EPQFI.reserveCoolDownThreshold', 'governed.EPQFI.reserveCoolDownP'];
    const parametersValues = [toBN(10000 * 10 ** 18), toBN(604800)];

    epqfiParameters = await makeProxy(registry.address, Parameters);
    await epqfiParameters.initialize(registry.address, parametersInitialList, parametersValues, [], [], [], [], [], []);

    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParameters.address);

    systemReserve = await makeProxy(registry.address, SystemReserve);
    await systemReserve.initialize(registry.address, ['defi.QUSD.systemDebtAuction']);

    systemReserveHelper = await makeProxy(registry.address, SystemReserveHelper);
    await systemReserveHelper.initialize(registry.address);

    defaultAllocationProxy = await makeProxy(registry.address, DefaultAllocationProxy);
    await defaultAllocationProxy.initialize(registry.address, [], []);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT]);
    epqfiMembership = await makeProxy(registry.address, EPQFImembership);
    await epqfiMembership.initialize(registry.address, []);

    await registry.setAddress('tokeneconomics.defaultAllocationProxy', defaultAllocationProxy.address);
    await registry.setAddress('governance.rootNodes', roots.address);
    await registry.setAddress('governance.experts.EPQFI.membership', epqfiMembership.address);
    await registry.setAddress('tokeneconomics.systemReserve', systemReserve.address);
    await registry.setAddress('system.reserve.helper', systemReserveHelper.address);
    await registry.setAddress('defi.QUSD.systemDebtAuction', AUCTION);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('getEligibleContractKeys()', () => {
    it('should get array with owner', async () => {
      assert.deepEqual(['defi.QUSD.systemDebtAuction'], await systemReserve.getEligibleContractKeys());
    });

    it('should deploy contract and get initial list', async () => {
      const initialKeys = ['governance.validators', 'tokeneconomics.qVault', 'governance.rootNodes'];
      const sReserve = await makeProxy(registry.address, SystemReserve);
      await sReserve.initialize(registry.address, initialKeys);
      assert.deepEqual(initialKeys, await sReserve.getEligibleContractKeys());
    });

    it('should add QUER only once', async () => {
      await systemReserve.addQEURStableCoin();
      assert.deepEqual(await systemReserve.getEligibleContractKeys(), [
        'defi.QUSD.systemDebtAuction',
        'defi.QEUR.systemDebtAuction',
      ]);

      await truffleAssert.reverts(
        systemReserve.addQEURStableCoin(),
        '[QEC-018005]-The QEUR stable coin has already been added.'
      );
    });
  });

  describe('withdraw()', () => {
    it('should get money and withdraw it on test contract', async () => {
      // Because we are using helper contract to interact with system reserve
      await registry.setAddress('defi.QUSD.systemDebtAuction', systemReserveHelper.address);

      const amount = 150;

      await systemReserve.send(amount);
      assert.equal(amount, await web3.eth.getBalance(systemReserve.address));

      await systemReserveHelper.transfer(amount);
      assert.equal(await web3.eth.getBalance(systemReserve.address), 0);
      assert.equal(await web3.eth.getBalance(systemReserveHelper.address), 0);
      assert.equal(amount, await web3.eth.getBalance(defaultAllocationProxy.address));
    });

    it('should get exception, caller is not owner', async () => {
      await truffleAssert.reverts(
        systemReserve.withdraw(0, { from: NOT_OWNER }),
        '[QEC-018000]-Permission denied - only eligible contracts have access.'
      );
    });

    it('should return false, try to call withdraw with zero balance', async () => {
      assert.isFalse(await systemReserve.withdraw.call(1));
    });

    it('should return false, try to call withdraw with amount more than balance', async () => {
      const amount = 100;
      await systemReserve.send(amount);

      assert.equal(amount, await web3.eth.getBalance(systemReserve.address));
      assert.isFalse(await systemReserve.withdraw.call(amount + 1));
    });

    it('should get exception, try to call withdraw with amount more than limit', async () => {
      await systemReserve.send(Q.multipliedBy(11000));
      // const reason = '[QEC-018003]-Insufficient funds available for withdrawal.';
      await truffleAssert.reverts(systemReserve.withdraw(Q.multipliedBy(10001)));
    });

    it('should get exception, try to call while system reserve paused', async () => {
      await systemReserve.setPauseState(true, { from: ROOT });
      const reason = '[QEC-018002]-The system reserve contract is paused..';
      await truffleAssert.reverts(systemReserve.withdraw(0), reason);
    });

    it('should get exception, try to call withdraw before the cool down phase', async () => {
      const reserveBalance = Q.multipliedBy(20000);
      await systemReserve.send(reserveBalance);
      assert.equal(await reserveBalance.toNumber(), await web3.eth.getBalance(systemReserve.address));

      const requestAmount = Q.multipliedBy(6000);
      await systemReserve.withdraw(requestAmount);

      const currentAvailableAmount = Q.multipliedBy(4000);
      assert.equal(await currentAvailableAmount.toNumber(), await systemReserve.getAvailableAmount());

      const reason = '[QEC-018003]-Insufficient funds available for withdrawal.';
      await truffleAssert.reverts(systemReserve.withdraw(requestAmount), reason);

      assert.equal(await currentAvailableAmount.toNumber(), await systemReserve.getAvailableAmount());

      await systemReserve.withdraw(currentAvailableAmount);
      assert.equal(await systemReserve.getAvailableAmount(), 0);

      await setTime(toBN(await systemReserve.getCoolDownPhase()).toNumber());
      await systemReserve.withdraw(requestAmount);
      assert.equal(await currentAvailableAmount.toNumber(), await systemReserve.getAvailableAmount());

      const finalReserveBalance = Q.multipliedBy(4000);
      assert.equal(await finalReserveBalance.toNumber(), await web3.eth.getBalance(systemReserve.address));
    });
  });

  describe('updateCoolDownPhase()', () => {
    const reserveBalance = Q.multipliedBy(20000);

    beforeEach('setup', async () => {
      await systemReserve.send(reserveBalance);
      assert.equal(reserveBalance.toNumber(), await web3.eth.getBalance(systemReserve.address));
    });

    it('should update phase', async () => {
      const availableAmount = Q.multipliedBy(10000);
      assert.equal(availableAmount.toNumber(), await systemReserve.getAvailableAmount());

      const requestAmount = Q.multipliedBy(7000);
      await systemReserve.withdraw(requestAmount);

      assert.equal(reserveBalance - requestAmount, await web3.eth.getBalance(systemReserve.address));
      assert.equal(availableAmount - requestAmount, await systemReserve.getAvailableAmount());

      await setTime(toBN(await systemReserve.getCoolDownPhase()).toNumber());
      await systemReserve.withdraw(0);
      assert.equal(availableAmount.toNumber(), await systemReserve.getAvailableAmount());
    });

    it('should not update phase, week period has not passed', async () => {
      const availableAmount = Q.multipliedBy(10000);
      assert.equal(availableAmount.toNumber(), await systemReserve.getAvailableAmount());

      const requestAmount = Q.multipliedBy(7000);
      await truffleAssert.passes(await systemReserve.withdraw(requestAmount));

      const remainingAmount = availableAmount.minus(requestAmount);
      assert.equal(remainingAmount.toFixed(), toBN(await systemReserve.getAvailableAmount()).toFixed());

      await systemReserve.withdraw(0);
      assert.equal(remainingAmount.toFixed(), toBN(await systemReserve.getAvailableAmount()).toFixed());
    }).retries(3);
  });

  describe('changePauseState', () => {
    it('should change system state', async () => {
      assert.isFalse(await systemReserve.getSystemPaused());

      await systemReserve.setPauseState(true, { from: ROOT });
      assert.isTrue(await systemReserve.getSystemPaused());

      await systemReserve.setPauseState(false, { from: ROOT });
      assert.isFalse(await systemReserve.getSystemPaused());
    });

    it('should get exception, try to call change from non root or expert', async () => {
      const reason = '[QEC-018001]-Permission denied - only root nodes and EPQFI experts have access.';
      await truffleAssert.reverts(systemReserve.setPauseState(false), reason);
    });
  });
});
