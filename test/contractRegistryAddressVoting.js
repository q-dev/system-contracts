const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const BigNumber = require('bignumber.js');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const ContractRegistryAddressVoting = artifacts.require('ContractRegistryAddressVoting');
const ContractRegistry = artifacts.require('ContractRegistry');
const Roots = artifacts.require('Roots');
const Constitution = artifacts.require('Constitution');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('ContractRegistryAddressVoting', () => {
  const reverter = new Reverter();

  let DEFAULT;
  let SOMEBODY;
  let ROOT1;
  let ROOT2;
  let ROOT3;
  let ROOT4;

  let contractRegistryAddressVoting;
  let registry;
  let roots;

  before('setup', async () => {
    DEFAULT = await accounts(0);
    SOMEBODY = await accounts(1);
    ROOT1 = await accounts(2);
    ROOT2 = await accounts(3);
    ROOT3 = await accounts(4);
    ROOT4 = await accounts(5);

    registry = await ContractRegistry.new();
    contractRegistryAddressVoting = await ContractRegistryAddressVoting.new();
    await registry.initialize([DEFAULT, contractRegistryAddressVoting.address], [], []);
    await contractRegistryAddressVoting.initialize(registry.address);
    const constitution = await Constitution.new();
    const addressStorageFactory = await AddressStorageFactory.new();
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    await constitution.initialize(
      registry.address,
      ['constitution.proposalExecutionP', 'constitution.voting.emgQUpdateRMAJ'],
      ['3600', '500000000000000000000000000'],
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.constitution.parameters', constitution.address);
    roots = await Roots.new();
    await roots.initialize(registry.address, [ROOT1, ROOT2, ROOT3, ROOT4]);
    await registry.setAddress('governance.rootNodes', roots.address);
    await registry.setAddress('governance.rootNodes.membershipVoting', DEFAULT);
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    it('should revert if not owner', async () => {
      await truffleAssert.reverts(
        contractRegistryAddressVoting.createProposal('SOMEBODY', SOMEBODY, { from: SOMEBODY })
      );
    });

    it('should create proposal', async () => {
      await contractRegistryAddressVoting.createProposal('SOMEBODY', SOMEBODY);
      const count = await contractRegistryAddressVoting.proposalsCount();
      assert.equal(count.toString(), '1');

      const proposal = await contractRegistryAddressVoting.getProposal(0);
      assert.equal(proposal.key, 'SOMEBODY');
      assert.equal(proposal.proxy, SOMEBODY);
    });
  });

  describe('approve', () => {
    beforeEach('setup for approve', async () => {
      await contractRegistryAddressVoting.createProposal('SOMEBODY', SOMEBODY);
    });

    it('should revert if not rootNode', async () => {
      await truffleAssert.reverts(contractRegistryAddressVoting.approve(0, { from: SOMEBODY }), '[QEC-039000]');
      await contractRegistryAddressVoting.approve(0, { from: ROOT1 });
    });

    it('should approve once', async () => {
      const voteCountBefore = bn(await contractRegistryAddressVoting.voteCount(0));
      await contractRegistryAddressVoting.approve(0, { from: ROOT1 });

      await truffleAssert.reverts(contractRegistryAddressVoting.approve(0, { from: ROOT1 }), '[QEC-039004]');
      const voteCountAfter = bn(await contractRegistryAddressVoting.voteCount(0));

      assert.equal(voteCountBefore.plus(1).toString(), voteCountAfter.toString());
      assert.isFalse((await contractRegistryAddressVoting.getProposal(0)).executed);
    });

    it('should change address after 3of4 votes', async () => {
      await contractRegistryAddressVoting.approve(0, { from: ROOT1 });
      await contractRegistryAddressVoting.approve(0, { from: ROOT2 });
      assert.isFalse((await contractRegistryAddressVoting.getProposal(0)).executed);

      let gotAddress = await registry.getAddress('SOMEBODY');
      assert.notEqual(gotAddress, SOMEBODY);

      await contractRegistryAddressVoting.approve(0, { from: ROOT3 });
      assert.isTrue((await contractRegistryAddressVoting.getProposal(0)).executed);

      gotAddress = await registry.getAddress('SOMEBODY');
      assert.equal(gotAddress, SOMEBODY);
    });

    it('should change address after root4 leave', async () => {
      await contractRegistryAddressVoting.approve(0, { from: ROOT1 });
      await contractRegistryAddressVoting.approve(0, { from: ROOT2 });
      await roots.removeMember(ROOT4);
      await contractRegistryAddressVoting.approve(0, { from: ROOT2 });

      assert.isTrue((await contractRegistryAddressVoting.getProposal(0)).executed);
    });
  });
});

function bn(num) {
  return new BigNumber(num);
}
