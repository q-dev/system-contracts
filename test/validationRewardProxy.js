'use strict';
const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat } = require('./helpers/defiHelper');

const { artifacts } = require('hardhat');
const ValidationRewardProxy = artifacts.require('./ValidationRewardProxy');
const Validators = artifacts.require('./Validators');
const Parameters = artifacts.require('./Constitution');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const ValidationRewardPools = artifacts.require('./ValidationRewardPools');
const EPQFIParameters = artifacts.require(`EPQFI_Parameters`);
const QVault = artifacts.require('QVaultMock');
const VotingWeightProxy = artifacts.require('./VotingWeightProxy');
const PushPayments = artifacts.require('PushPayments');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

describe('ValidationRewardProxy', () => {
  const reverter = new Reverter();

  const Q = require('./helpers/defiHelper').Q;
  const bigQ = require('./helpers/defiHelper').Q;
  const DECIMAL = require('./helpers/defiHelper').DECIMAL;

  const CR_UPDATE_MINIMUM_BASE = bigQ.multipliedBy(1000);

  let OWNER;
  let PARAMETERS_VOTING;

  let constitution;
  let registry;
  let validators;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let validationRewardProxy;
  let validationRewardPools;
  let epqfiParams;
  let crKeeperFactory;
  let qVault;
  let votingWeightProxy;
  let pushPayments;

  before('setup', async () => {
    OWNER = await accounts(0);
    PARAMETERS_VOTING = await accounts(1);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    const uintKeys = [
      'constitution.maxNValidators',
      'constitution.maxNStandbyValidators',
      'constitution.cliqueEpochLength',
    ];
    const uintParams = [1, 1, 1];

    constitution = await makeProxy(registry.address, Parameters);
    await constitution.initialize(registry.address, uintKeys, uintParams, [], [], [], [], [], []);
    await registry.setAddress('governance.constitution.parameters', constitution.address);

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();
    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );
    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);

    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    validationRewardPools = await makeProxy(registry.address, ValidationRewardPools);
    await validationRewardPools.initialize(registry.address, CR_UPDATE_MINIMUM_BASE);

    await registry.setAddress('tokeneconomics.validationRewardPools', validationRewardPools.address);
    await registry.setAddress('governance.validators', validators.address, { from: OWNER });

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['tokeneconomics.qVault', 'governance.validators'],
      ['governance.generalUpdateVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    const QFIparametersInitialList = [
      'governed.EPQFI.maximumDelegationTargets',
      'governed.EPQFI.Q_rewardPoolMaxClaimP',
      'governed.EPQFI.stakeDelegationFactor',
    ];
    const QFIparametersValues = [2, 1000, getPercentageFormat(1000)]; // factor 10 times

    epqfiParams = await makeProxy(registry.address, EPQFIParameters);
    await epqfiParams.initialize(
      registry.address,
      QFIparametersInitialList,
      QFIparametersValues,
      [],
      [],
      [],
      [],
      [],
      []
    );
    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParams.address);

    qVault = await makeProxy(registry.address, QVault);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);

    validationRewardProxy = await makeProxy(registry.address, ValidationRewardProxy);
    await validationRewardProxy.initialize(registry.address);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('allocate', () => {
    it('should pay active validators', async () => {
      const VALIDATOR = await accounts(1);
      const STANDBY = await accounts(2);
      const STAKER = await accounts(3);

      const validatorCollateral = 3 * Q;
      await validators.commitStake({ value: toBN(validatorCollateral), from: VALIDATOR });
      await validators.enterShortList({ from: VALIDATOR });

      const standbyCollateral = 2 * Q;
      await validators.commitStake({ value: toBN(standbyCollateral), from: STANDBY });
      await validators.enterShortList({ from: STANDBY });

      await validators.commitStake({ value: toBN(standbyCollateral / 2), from: STAKER });

      await validators.makeSnapshot();

      // fund validationRewardProxy
      const balance = Q;
      await web3.eth.sendTransaction({
        from: OWNER,
        to: validationRewardProxy.address,
        value: Q,
      });

      const validatorBalanceBefore = toBN(await web3.eth.getBalance(VALIDATOR));
      const standbyBalanceBefore = toBN(await web3.eth.getBalance(STANDBY));
      const stakerBalanceBefore = toBN(await web3.eth.getBalance(STAKER));

      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      // validator
      const beneficiariesStake = toBN(validatorCollateral).plus(standbyCollateral);
      const expectedValidatorReward = toBN(balance).multipliedBy(validatorCollateral).div(beneficiariesStake);

      const validatorBalanceAfter = toBN(await web3.eth.getBalance(VALIDATOR));
      const validatorReward = toBN(validatorBalanceAfter).minus(validatorBalanceBefore);
      assert.equal(validatorReward.toString(), expectedValidatorReward.toString(), 'unexpected validators reward');

      // standby
      const expectedStandByReward = toBN(balance).multipliedBy(toBN(standbyCollateral)).div(toBN(beneficiariesStake));

      const standbyBalanceAfter = toBN(await web3.eth.getBalance(STANDBY));
      const standbyReward = toBN(standbyBalanceAfter).minus(standbyBalanceBefore);
      assert.equal(standbyReward.toString(), expectedStandByReward.toString(), 'unexpected standby reward');

      // ordinary staker: assert balance wasn't changed
      const stakerBalanceAfter = toBN(await web3.eth.getBalance(STAKER));
      assert.equal(
        stakerBalanceAfter.toString(),
        stakerBalanceBefore.toString(),
        'balance of ordinary staker from long list should not be changed'
      );
    });

    it('should pay 100% to delegator', async () => {
      // setup validators
      const amount = toBN(Q);
      const VALIDATOR = await accounts(1);

      await validators.commitStake({ value: amount, from: VALIDATOR });
      await validationRewardPools.setDelegatorsShare(toBN(10 ** 27), { from: VALIDATOR }); // 100% from decimal
      await validators.enterShortList({ from: VALIDATOR });

      await validators.makeSnapshot();

      // setup proxy
      await validationRewardProxy.send(amount);
      assert.equal(amount, await web3.eth.getBalance(validationRewardProxy.address));

      const balanceBefore = toBN(await web3.eth.getBalance(VALIDATOR));
      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(await web3.eth.getBalance(validationRewardProxy.address), 0);
      assert.equal(balanceBefore.toString(), toBN(await web3.eth.getBalance(VALIDATOR)));
      assert.equal(amount, (await validationRewardPools.getBalance(VALIDATOR)).toString());
    });

    it('should pay 100% to validator', async () => {
      // setup validators
      const amount = toBN(Q);
      const VALIDATOR = await accounts(1);

      await validators.commitStake({ value: amount, from: VALIDATOR });
      await validators.enterShortList({ from: VALIDATOR });

      await validators.makeSnapshot();

      // setup proxy
      await validationRewardProxy.send(amount);
      assert.equal(amount, await web3.eth.getBalance(validationRewardProxy.address));

      const balanceBefore = toBN(await web3.eth.getBalance(VALIDATOR));
      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(await web3.eth.getBalance(validationRewardProxy.address), 0);
      assert.equal(balanceBefore.plus(Q).toString(), toBN(await web3.eth.getBalance(VALIDATOR)));
      assert.equal((await validationRewardPools.getBalance(VALIDATOR)).toString(), 0);
    });

    it('should correctly split reward between validator and delegator', async () => {
      // setup validators
      const VALIDATOR = await accounts(1);
      await validators.commitStake({ value: toBN(Q), from: VALIDATOR });
      await validators.enterShortList({ from: VALIDATOR });
      await validators.makeSnapshot();

      const qsv = DECIMAL.multipliedBy(0.1);
      await validationRewardPools.setDelegatorsShare(qsv, { from: VALIDATOR });

      // setup proxy
      const totalReward = toBN(Q);
      await validationRewardProxy.send(totalReward);
      assert.equal(totalReward.toString(), toBN(await web3.eth.getBalance(validationRewardProxy.address)).toString());

      const validatorBalanceBefore = toBN(await web3.eth.getBalance(VALIDATOR));
      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      const expectedDelegatorReward = totalReward.multipliedBy(qsv).div(DECIMAL);
      assert.equal(
        expectedDelegatorReward.toString(),
        await web3.eth.getBalance(validationRewardPools.address),
        'unexpected delegator reward'
      );

      const expectedValidatorReward = totalReward.minus(expectedDelegatorReward);
      const validatorReward = toBN(await web3.eth.getBalance(VALIDATOR)).minus(validatorBalanceBefore);
      assert.equal(expectedValidatorReward.toString(), validatorReward.toString(), 'unexpected validator reward');
    });

    it('validator reward depends on accountable stake', async () => {
      const val1 = await accounts(7);
      const val2 = await accounts(8);
      const staker1 = await accounts(5);
      const staker2 = await accounts(6);
      const selfStake1 = bigQ.times(10);
      const delegatedStake1 = bigQ.times(1000);
      const selfStake2 = bigQ.times(20);
      const delegatedStake2 = bigQ.times(500);

      await validators.commitStake({ from: val1, value: selfStake1 });
      await validators.commitStake({ from: val2, value: selfStake2 });
      await validationRewardPools.setDelegatorsShare(toBN(0), { from: val1 }); // 0% from decimal
      await validationRewardPools.setDelegatorsShare(toBN(0), { from: val2 }); // 0% from decimal
      await qVault.deposit({ from: staker1, value: delegatedStake1 });
      await qVault.deposit({ from: staker2, value: delegatedStake2 });
      await qVault.delegateStake([val1], [delegatedStake1], { from: staker1 });
      await qVault.delegateStake([val2], [delegatedStake2], { from: staker2 });

      await validators.enterShortList({ from: val1 });
      await validators.enterShortList({ from: val2 });

      await validators.makeSnapshot();

      const amount = bigQ.times(50);

      // setup proxy
      await validationRewardProxy.send(amount);
      assert.equal(amount, await web3.eth.getBalance(validationRewardProxy.address));

      const balanceBefore1 = toBN(await web3.eth.getBalance(val1));
      const balanceBefore2 = toBN(await web3.eth.getBalance(val2));

      const balanceDiffBefore = balanceBefore1.minus(balanceBefore2);
      assert.isTrue(balanceDiffBefore > 0);
      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      const balanceAfter1 = toBN(await web3.eth.getBalance(val1));
      const balanceAfter2 = toBN(await web3.eth.getBalance(val2));

      const balanceDiffAfter = balanceAfter2.minus(balanceAfter1);
      assert.isTrue(balanceDiffAfter > 0);
    });

    // to work the following config is required
    // "big-funds-private-network"

    it('should pay active validators big reward', async () => {
      const VALIDATOR = await accounts(1);
      const STANDBY = await accounts(2);
      const STAKER = await accounts(3);

      const validatorCollateral = 3 * Q;
      await validators.commitStake({ value: toBN(validatorCollateral), from: VALIDATOR });
      await validators.enterShortList({ from: VALIDATOR });

      const standbyCollateral = 2 * Q;
      await validators.commitStake({ value: toBN(standbyCollateral), from: STANDBY });
      await validators.enterShortList({ from: STANDBY });

      await validators.commitStake({ value: toBN(standbyCollateral / 2), from: STAKER });

      await validators.makeSnapshot();

      // fund validationRewardProxy
      const balance = toBN(toBN(99) * Q);
      await web3.eth.sendTransaction({
        from: OWNER,
        to: validationRewardProxy.address,
        value: balance,
      });

      const validatorBalanceBefore = await web3.eth.getBalance(VALIDATOR);

      const resultTx = await validationRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      // validator
      const beneficiariesStake = validatorCollateral + standbyCollateral;
      const expectedValidatorReward = balance.multipliedBy(toBN(validatorCollateral)).div(beneficiariesStake);

      const validatorBalanceAfter = await web3.eth.getBalance(VALIDATOR);
      const validatorReward = toBN(validatorBalanceAfter).minus(toBN(validatorBalanceBefore));
      assert.equal(validatorReward.toString(), expectedValidatorReward.toString(), 'unexpected validators reward');
    });
  });
});
