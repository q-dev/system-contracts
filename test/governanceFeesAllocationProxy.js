const { assert } = require('chai');
const { artifacts } = require('hardhat');

const truffleAssert = require('truffle-assertions');

const { accounts } = require('./helpers/utils.js');

const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { setBalance } = require('./helpers/utils');

const GovernanceFeesAllocationProxy = artifacts.require('GovernanceFeesAllocationProxy');

const ContractRegistry = artifacts.require('ContractRegistry');

describe('GovernanceFeesAllocationProxy', () => {
  const reverter = new Reverter();

  let contractRegistry;
  let governanceFeesAllocationProxy;

  const Q = require('./helpers/defiHelper').Q;

  let OWNER;
  let ROOT_NODE_REWARD_PROXY = '0x4E4C176352Eb66A922b16Ad8CB804f23af00E156';
  let Q_HOLDER_REWARD_PROXY = '0x75D0b5E9fCeCaAF0Bea708A669c1A56e12b82eda';
  let VALIDATION_REWARD_PROXY = '0xb2c6AC0709d45F85B6a4669B153caDE6Ef2941b7';

  before(async () => {
    OWNER = await accounts(0);

    await setBalance(ROOT_NODE_REWARD_PROXY, 0n);
    await setBalance(Q_HOLDER_REWARD_PROXY, 0n);
    await setBalance(VALIDATION_REWARD_PROXY, 0n);

    contractRegistry = await ContractRegistry.new();
    await contractRegistry.initialize([OWNER], [], []);

    await contractRegistry.setAddress('tokeneconomics.rootNodeRewardProxy', ROOT_NODE_REWARD_PROXY);
    await contractRegistry.setAddress('tokeneconomics.validationRewardProxy', VALIDATION_REWARD_PROXY);
    await contractRegistry.setAddress('tokeneconomics.qHolderRewardProxy', Q_HOLDER_REWARD_PROXY);

    governanceFeesAllocationProxy = await makeProxy(contractRegistry.address, GovernanceFeesAllocationProxy);
    await governanceFeesAllocationProxy.__GovernanceFeesAllocationProxy_init(contractRegistry.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('access', async () => {
    it('should not be initialized twice', async () => {
      await truffleAssert.reverts(
        governanceFeesAllocationProxy.__GovernanceFeesAllocationProxy_init(contractRegistry.address),
        'Initializable: contract is already initialized'
      );
    });
  });

  describe('allocate', () => {
    it('should correctly distribute balance amongst beneficiaries', async () => {
      await governanceFeesAllocationProxy.send(Q);

      const transactionReceipt = await governanceFeesAllocationProxy.allocate();
      assert.equal(transactionReceipt.receipt.logs[0].event, 'Allocated');

      assert.equal(
        (await web3.eth.getBalance(ROOT_NODE_REWARD_PROXY)).toString(),
        Q.times(0.4).toString() // 1.0 * 0.4 = 0.4
      );

      assert.equal(
        (await web3.eth.getBalance(Q_HOLDER_REWARD_PROXY)).toString(),
        Q.times(0.4).toString() // 1.0 * 0.4 = 0.4
      );

      assert.equal(
        (await web3.eth.getBalance(VALIDATION_REWARD_PROXY)).toString(),
        Q.times(0.2).toString() // 1.0 * 0.2 = 0.2
      );
    });

    it('should handle zero balance allocation correctly', async () => {
      assert.equal((await web3.eth.getBalance(governanceFeesAllocationProxy.address)).toString(), '0');

      const transactionReceipt = await governanceFeesAllocationProxy.allocate();
      assert.equal(transactionReceipt.receipt.logs[0].event, 'Allocated');
      assert.equal(transactionReceipt.receipt.logs[0].args.amount.toString(), '0');
    });

    it('should handle remainder in allocation correctly', async () => {
      await governanceFeesAllocationProxy.send('123');

      const transactionReceipt = await governanceFeesAllocationProxy.allocate();

      const totalAllocated = transactionReceipt.receipt.logs[0].args.amount;
      assert.equal(totalAllocated.toString(), '122');

      assert.equal(
        (await web3.eth.getBalance(ROOT_NODE_REWARD_PROXY)).toString(),
        '49' // 123 * 0.4 = 49.2
      );

      assert.equal(
        (await web3.eth.getBalance(Q_HOLDER_REWARD_PROXY)).toString(),
        '49' // 123 * 0.4 = 49.2
      );

      assert.equal(
        (await web3.eth.getBalance(VALIDATION_REWARD_PROXY)).toString(),
        '24' // 123 * 0.2 = 24.6
      );

      assert.equal(
        (await web3.eth.getBalance(governanceFeesAllocationProxy.address)).toString(),
        '1' // 123 - 122 = 1
      );
    });

    it('should revert if reward transfer fails', async () => {
      await contractRegistry.setAddress('tokeneconomics.rootNodeRewardProxy', contractRegistry.address);

      await governanceFeesAllocationProxy.send(Q);

      await truffleAssert.reverts(
        governanceFeesAllocationProxy.allocate(),
        '[QEC-011001]-Failed to transfer the amount, allocation failed.'
      );
    });
  });
});
