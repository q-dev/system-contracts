const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');
const { toBN } = require('./helpers/defiHelper');

const { artifacts } = require('hardhat');
const ContractRegistry = artifacts.require('./ContractRegistry');
const SystemReserve = artifacts.require('./SystemReserve');
const QHolderRewardProxy = artifacts.require('./QHolderRewardProxy');
const Parameters = artifacts.require('./EPQFI_Parameters');
const QHolderRewardPool = artifacts.require('./QHolderRewardPool');

describe('qHolderRewardProxy', () => {
  const reverter = new Reverter();

  let registry;
  let systemReserve;
  let qHolderRewardProxy;
  let parameters;
  let qHolderRewardPool;
  let epqfiParameters;

  const qReserveShare = toBN(toBN(10 ** 25) * 5); // 5% from decimal

  let OWNER;

  before('setup', async () => {
    OWNER = await accounts(0);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], [], []);
    parameters = await makeProxy(registry.address, Parameters);
    await parameters.initialize(
      registry.address,
      ['governed.EPQFI.Q_reserveShare'],
      [qReserveShare],
      [],
      [],
      [],
      [],
      [],
      []
    );

    const parametersInitialList = ['governed.EPQFI.reserveCoolDownThreshold', 'governed.EPQFI.reserveCoolDownP'];
    const parametersValues = [toBN(10000 * 10 ** 18), toBN(604800)];

    epqfiParameters = await makeProxy(registry.address, Parameters);
    await epqfiParameters.initialize(registry.address, parametersInitialList, parametersValues, [], [], [], [], [], []);

    await registry.setAddress('governance.experts.EPQFI.parameters', epqfiParameters.address);

    systemReserve = await makeProxy(registry.address, SystemReserve);
    await systemReserve.initialize(registry.address, []);

    qHolderRewardProxy = await makeProxy(registry.address, QHolderRewardProxy);
    await qHolderRewardProxy.initialize(registry.address);

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('governance.experts.EPQFI.parameters', parameters.address);
    await registry.setAddress('tokeneconomics.systemReserve', systemReserve.address);
    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('', () => {
    it('should get money, triger send function and send to beneficiaries', async () => {
      const amount = 100;
      const resultTx = await qHolderRewardProxy.send(amount);
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(await web3.eth.getBalance(qHolderRewardProxy.address), 0);

      // systemReserve gets only 5% from contract balance
      assert.equal(await web3.eth.getBalance(systemReserve.address), 5);

      // holderRewardPool
      assert.equal(await web3.eth.getBalance(qHolderRewardPool.address), 95);
    });

    it('should send with rounding handling for first receiver', async () => {
      const amount = 101;
      const resultTx = await qHolderRewardProxy.send(amount);
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(await web3.eth.getBalance(qHolderRewardProxy.address), 0);
      assert.equal(await web3.eth.getBalance(systemReserve.address), 5);
      assert.equal(await web3.eth.getBalance(qHolderRewardPool.address), 96);
    });
  });
});
