const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const ContractRegistry = artifacts.require('./ContractRegistry');

describe('ContractRegistry', () => {
  const reverter = new Reverter();

  let registry;

  let DEFAULT;
  let SOMEBODY;
  let SOMEONE;

  before(async () => {
    DEFAULT = await accounts(0);
    SOMEBODY = await accounts(1);
    SOMEONE = await accounts(2);

    // make default is maintainer
    registry = await ContractRegistry.new();
    await registry.initialize([DEFAULT], [], []);
    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('setAddress()/setAddresses()', () => {
    it('should set address', async () => {
      const key = 'key';
      const address = '0x060dD4e83d587B35F2D5094D6C142817F7e8de97';

      await registry.setAddress(key, address);
      const contracts = await registry.getContracts();

      assert.equal(await registry.contains(key), true);
      assert.equal(key, contracts[0].key);
      assert.equal(address, contracts[0].addr);
      assert.equal(contracts.length, 1);
    });

    it('should set address and set new instead of old', async () => {
      const key = 'oldKey';
      const oldAddress = '0x060dD4e83d587B35F2D5094D6C142817F7e8de97';
      const newAddress = '0x407d84C08CCa6469f2B07de79f2E19D308A23a4B';

      await registry.setAddress(key, oldAddress);
      let contracts = await registry.getContracts();
      assert.equal(await registry.contains(key), true);
      assert.equal(key, contracts[0].key);
      assert.equal(oldAddress, contracts[0].addr);
      assert.equal(contracts.length, 1);

      await registry.setAddress(key, newAddress);
      assert.equal(await registry.contains(key), true);
      contracts = await registry.getContracts();
      assert.equal(key, contracts[0].key);
      assert.equal(newAddress, contracts[0].addr);
      assert.equal(contracts.length, 1);
    });

    it('should set arrays and keys', async () => {
      const keys = ['key1', 'key2', 'key3'];
      const addresses = [
        '0x060dD4e83d587B35F2D5094D6C142817F7e8de97',
        '0x407d84C08CCa6469f2B07de79f2E19D308A23a4B',
        '0xAA0eD8A3DBbD8C6bcB9F7520C020E46ac2B511ED',
      ];

      await registry.setAddresses(keys, addresses);
      const contracts = await registry.getContracts();
      for (let i = 0; i < contracts.length; ++i) {
        assert.equal(keys[i], contracts[i].key);
        assert.equal(addresses[i], contracts[i].addr);
        assert.equal(await registry.contains(keys[i]), true);
      }
      assert.equal(contracts.length, 3);
    });

    it('should get exception, pass nil address', async () => {
      const key = 'key';
      const nilAddress = '0x0000000000000000000000000000000000000000';
      const reason = 'Invalid address value, failed to set the address.';
      await truffleAssert.reverts(registry.setAddress(key, nilAddress), reason);

      await truffleAssert.reverts(registry.setAddresses([key], [nilAddress]), reason);
    });

    it('should get exception, not equal length', async () => {
      const keys = ['key1', 'key2'];
      const addresses = ['0x407d84C08CCa6469f2B07de79f2E19D308A23a4B'];
      const reason = 'The number of keys and addresses should be the same, failed to set addresses.';
      await truffleAssert.reverts(registry.setAddresses(keys, addresses), reason);
    });
  });

  describe('removeKey()/removeKeys()', () => {
    it('should set and remove an address', async () => {
      const key = 'key';
      const address = '0x060dD4e83d587B35F2D5094D6C142817F7e8de97';

      await registry.setAddress(key, address);
      let contracts = await registry.getContracts();

      assert.equal(key, contracts[0].key);
      assert.equal(address, contracts[0].addr);
      assert.equal(contracts.length, 1);
      assert.equal(await registry.contains(key), true);

      await registry.removeKey(key);
      contracts = await registry.getContracts();
      assert.equal(contracts.length, 0);
      assert.equal(await registry.contains(key), false);
    });

    it('should set addresses and keys, then remove all of them', async () => {
      const keys = ['key1', 'key2', 'key3'];
      const addresses = [
        '0x060dD4e83d587B35F2D5094D6C142817F7e8de97',
        '0x407d84C08CCa6469f2B07de79f2E19D308A23a4B',
        '0xAA0eD8A3DBbD8C6bcB9F7520C020E46ac2B511ED',
      ];

      await registry.setAddresses(keys, addresses);
      let contracts = await registry.getContracts();
      for (let i = 0; i < contracts.length; ++i) {
        assert.equal(keys[i], contracts[i].key);
        assert.equal(addresses[i], contracts[i].addr);
        assert.equal(await registry.contains(keys[i]), true);
      }
      assert.equal(contracts.length, 3);

      await registry.removeKeys(keys);
      contracts = await registry.getContracts();
      assert.equal(contracts.length, 0);
      for (let i = 0; i < contracts.length; ++i) {
        assert.equal(await registry.contains(keys[i]), false);
      }
    });

    it('should add key and address, remove and add again', async () => {
      const key = 'key';
      const address1 = '0x060dD4e83d587B35F2D5094D6C142817F7e8de97';
      const address2 = '0xAA0eD8A3DBbD8C6bcB9F7520C020E46ac2B511ED';

      await registry.setAddress(key, address1);
      let contracts = await registry.getContracts();

      assert.equal(key, contracts[0].key);
      assert.equal(address1, contracts[0].addr);
      assert.equal(contracts.length, 1);
      assert.equal(await registry.contains(key), true);

      await registry.removeKey(key);
      contracts = await registry.getContracts();
      assert.equal(contracts.length, 0);
      assert.equal(await registry.contains(key), false);

      await registry.setAddress(key, address2);
      contracts = await registry.getContracts();

      assert.equal(key, contracts[0].key);
      assert.equal(address2, contracts[0].addr);
      assert.equal(contracts.length, 1);
      assert.equal(await registry.contains(key), true);
    });
  });

  describe('onlyMaintainer()', () => {
    const reason = 'Permission denied - only maintainers have access.';
    let notMaintainer;

    before(async () => {
      notMaintainer = await accounts(3);
    });

    it('should get exception, set address/es by not maintainer', async () => {
      await truffleAssert.reverts(registry.setAddresses([], [], { from: notMaintainer }), reason);

      await truffleAssert.reverts(
        registry.setAddress('', '0x407d84C08CCa6469f2B07de79f2E19D308A23a4B', { from: notMaintainer }),
        reason
      );
    });

    it("should get exception, somebody try to add someone in maintainers list, but he's not maintainer", async () => {
      await truffleAssert.reverts(registry.setMaintainer(SOMEONE, { from: SOMEBODY }), reason);
    });
  });

  describe('getMaintainers()', () => {
    it('should get the list of maintainers', async () => {
      const expectedMaintainers = [DEFAULT];
      const actualMaintainers = await registry.getMaintainers();

      assert.equal(expectedMaintainers.toString(), actualMaintainers.toString());
    });
  });

  describe('setMaintainer()/leaveMaintainers()', () => {
    it('maintainer should add somebody to maintainer list and somebody should leave', async () => {
      await registry.setMaintainer(SOMEBODY);
      let actualMaintainers = await registry.getMaintainers();
      assert.equal(actualMaintainers.includes(SOMEBODY), true, 'SOMEBODY should be a maintainer');

      await registry.leaveMaintainers({ from: SOMEBODY });
      actualMaintainers = await registry.getMaintainers();
      assert.equal(actualMaintainers.includes(SOMEBODY), false, 'SOMEBODY should not be a maintainer');
    });

    it('default should leave from list', async () => {
      await truffleAssert.reverts(
        registry.leaveMaintainers(),
        '[QEC-034002]-Cannot leave the maintainers list, you are the only maintainer.'
      );
      await registry.setMaintainer(SOMEBODY);
      await registry.leaveMaintainers();
    });

    it("should get exception, somebody leave from list, but he's not maintainer", async () => {
      const reason = '[QEC-035000]-Failed to remove the address from the address storage.';
      await truffleAssert.reverts(registry.leaveMaintainers({ from: SOMEBODY }), reason);
    });

    it('should get exception, add somebody to maintainer list 2 times', async () => {
      const reason =
        'The address has already been added to the storage,' + ' failed to add the address to the address storage.';
      await registry.setMaintainer(SOMEBODY);
      await truffleAssert.reverts(registry.setMaintainer(SOMEBODY), reason);
    });
  });
});
