const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');

const { artifacts } = require('hardhat');
const AddressStorage = artifacts.require('AddressStorage');

describe('AddressStorage', () => {
  const reverter = new Reverter();

  let addressStorage;
  let A;
  let B;
  let C;

  before('setup', async () => {
    A = await accounts(0);
    B = await accounts(1);
    C = await accounts(2);

    addressStorage = await AddressStorage.new();
    await addressStorage.initialize([]);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('double entry', () => {
    it('should contains only A', async () => {
      const storage = await AddressStorage.new();
      await storage.initialize([A, A]);

      const size = await storage.size();
      assert.equal(size, 1);
      const addrs = await storage.getAddresses();
      assert.equal(addrs.length, 1);
    });
  });

  describe('adding', () => {
    it('should add A, B and C', async () => {
      await addressStorage.mustAdd(A);

      await addressStorage.mustAdd(B);

      await addressStorage.mustAdd(C);
      const size = await addressStorage.size();
      assert.equal(size, 3);
    });
  });

  describe('removing', () => {
    it('should add A and remove it', async () => {
      await addressStorage.mustAdd(A);
      await addressStorage.mustRemove(A);
      const size = await addressStorage.size();
      assert.equal(size, 0);
    });

    it('should add A, B and remove B', async () => {
      await addressStorage.mustAdd(A);
      await addressStorage.mustAdd(B);
      await addressStorage.mustRemove(B);
      const size = await addressStorage.size();

      assert.equal(size, 1);
      assert.isFalse(await addressStorage.contains(B));
      assert.isTrue(await addressStorage.contains(A));
    });

    it('should remove A, B and C', async () => {
      await addressStorage.mustAdd(A);
      await addressStorage.mustAdd(B);
      await addressStorage.mustAdd(C);

      await addressStorage.mustRemove(C);

      let size = await addressStorage.size();
      assert.equal(size, 2);

      await addressStorage.mustRemove(A);

      size = await addressStorage.size();
      assert.equal(size, 1);

      await addressStorage.mustRemove(B);

      size = await addressStorage.size();
      assert.equal(size, 0);
    });
  });

  describe.skip('clear', () => {
    it('gas usage analysis', async () => {
      const addressNumbers = [0, 1, 10, 50, 100, 200, 500];
      // eslint-disable-next-line guard-for-in
      for (const addressNumber of addressNumbers) {
        console.log('addressNumber: ', addressNumber);
        for (let i = 1; i <= addressNumber; i++) {
          const newAddress = '0x' + i.toString().padStart(40, '0');
          await addressStorage.mustAdd(newAddress);
        }

        const result = await addressStorage.clear();
        console.log('\tgasUsed: ', result.receipt.gasUsed);
      }
    });
  });
});
