const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');

const { getCurrentBlockTime, setTime, setNextBlockTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const EmergencyUpdateVoting = artifacts.require('./EmergencyUpdateVoting');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Constitution = artifacts.require('Constitution');
const Roots = artifacts.require('Roots');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

describe('EmergencyUpdateVoting', () => {
  const reverter = new Reverter();

  const votingPeriod = 1000;
  const requiredMajority = getPercentageFormat(50); // it's 50% from 10 ** 27
  const requiredQuorum = 0;
  const remark = 'https://ethereum.org';
  const proposalExecutionP = 2592000;

  let roots;
  let registry;
  let constitution;
  let emergencyUpdateVoting;

  let OWNER;
  let ROOT;
  let ROOT2;
  let ROOT3;
  let NON_EXISTING_ROOT;
  let PARAMETERS_VOTING;

  before(async () => {
    OWNER = await accounts(0);
    ROOT = await accounts(1);
    ROOT2 = await accounts(6);
    ROOT3 = await accounts(3);
    NON_EXISTING_ROOT = await accounts(2);
    PARAMETERS_VOTING = await accounts(9);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING], {
      from: OWNER,
    });

    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);
    constitution = await makeProxy(registry.address, Constitution);
    await constitution.initialize(registry.address, [], [], [], [], [], [], [], []);

    await constitution.setUint('constitution.voting.emgQUpdateVP', votingPeriod, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.emgQUpdateQRM', requiredQuorum, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.voting.emgQUpdateRMAJ', requiredMajority, { from: PARAMETERS_VOTING });
    await constitution.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month

    await registry.setAddress('governance.constitution.parameters', constitution.address, { from: OWNER });

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [ROOT, ROOT2, ROOT3]);
    await registry.setAddress('governance.rootNodes', roots.address, { from: OWNER });

    emergencyUpdateVoting = await makeProxy(registry.address, EmergencyUpdateVoting);
    await emergencyUpdateVoting.initialize(registry.address);
    await registry.setAddress('governance.emergencyUpdateVoting', emergencyUpdateVoting.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('createProposal', () => {
    const proposalID = 0;

    it('should create a proposal with existing root node', async () => {
      const result = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });

      assert.equal((await emergencyUpdateVoting.proposalCount()).toString(), 1);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).remark.toString(), remark);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, remark);
    });

    it('should create a proposal with the correct values', async () => {
      const votingStartTime = (await getCurrentBlockTime()) + 1;
      await setNextBlockTime(votingStartTime);
      await emergencyUpdateVoting.createProposal(remark, { from: ROOT });

      const resultBase = await emergencyUpdateVoting.proposals(proposalID);
      assert.equal((await emergencyUpdateVoting.proposalCount()).toString(), 1);
      assert.equal(resultBase.params.votingStartTime, votingStartTime);
      assert.equal(resultBase.params.votingEndTime, votingPeriod + votingStartTime);
      assert.equal(resultBase.params.requiredMajority, requiredMajority.toNumber());
      assert.equal(resultBase.params.requiredQuorum, requiredQuorum);
    }).retries(3);

    it('should get exception, caller is not a root node', async () => {
      const reason = '[QEC-030004]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(emergencyUpdateVoting.createProposal(remark, { from: NON_EXISTING_ROOT }), reason);

      assert.equal((await emergencyUpdateVoting.proposalCount()).toString(), 0);
    });
  });

  describe('vote', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const res = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should be possible to vote for an existing pending proposal', async () => {
      const result = await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 1);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 0);
      assert.isTrue(await emergencyUpdateVoting.voted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
    });

    it('should be possible to vote against an existing pending proposal', async () => {
      const result = await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 0);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);
      assert.isTrue(await emergencyUpdateVoting.voted(0, ROOT));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 2);
    });

    it('should not be possible to vote if already voted', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 1);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 0);
      assert.isTrue(await emergencyUpdateVoting.voted(0, ROOT));

      const reason = '[QEC-030002]-The caller has already voted for the proposal.';
      await truffleAssert.reverts(emergencyUpdateVoting.voteFor(proposalID, { from: ROOT }), reason);

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 1);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 0);
      assert.isTrue(await emergencyUpdateVoting.voted(0, ROOT));
    });

    it('should not be possible to vote if sender not a root node', async () => {
      const reason = '[QEC-030004]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(emergencyUpdateVoting.voteFor(proposalID, { from: NON_EXISTING_ROOT }), reason);

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 0);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 0);
    });

    it('should not be able to vote for non-existing proposal', async () => {
      const nonExistingID = proposalID + 1;

      const reason = '[QEC-030005]-Proposal does not exist.';
      await truffleAssert.reverts(emergencyUpdateVoting.voteAgainst(nonExistingID, { from: ROOT }), reason);

      assert.equal((await emergencyUpdateVoting.proposals(nonExistingID)).counters.weightAgainst.toString(), 0);
      assert.equal((await emergencyUpdateVoting.proposals(nonExistingID)).counters.weightFor.toString(), 0);
      assert.isFalse(await emergencyUpdateVoting.voted(1, ROOT));
    });
  });

  describe('test getters', () => {
    let proposalID;

    beforeEach('init proposal', async () => {
      const res = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
    });

    it('should successfully return votes weight "FOR"', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      assert.equal(await emergencyUpdateVoting.getVotesFor(proposalID, { from: NON_EXISTING_ROOT }), 1);
    });

    it('should successfully return votes weight "AGAINST"', async () => {
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT });
      assert.equal(await emergencyUpdateVoting.getVotesAgainst(proposalID, { from: NON_EXISTING_ROOT }), 1);
    });
  });

  describe('execute', () => {
    let proposalID;
    let endTime;

    beforeEach('init proposal', async () => {
      const res = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
      const prop = await emergencyUpdateVoting.proposals.call(proposalID);
      endTime = Number(prop.params.votingEndTime);
    });

    it('should successfully execute a PASSED proposal from QTH', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT2 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);

      await setTime(endTime);

      let status = (await emergencyUpdateVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      const txReceipt = await emergencyUpdateVoting.execute(proposalID, { from: NON_EXISTING_ROOT });
      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalID);

      status = (await emergencyUpdateVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXECUTED, status, 'unexpected status after proposal is executed');
    });

    it('should not be possible to execute EXPIRED proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT2 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);

      await setTime(endTime);

      let status = (await emergencyUpdateVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.PASSED, status, 'unexpected status');

      await setTime(endTime + proposalExecutionP + 1);

      const reason = '[QEC-030003]-Proposal must be PASSED before excecuting.';
      await truffleAssert.reverts(emergencyUpdateVoting.execute(proposalID, { from: ROOT }), reason);
      status = (await emergencyUpdateVoting.getStatus(proposalID)).toNumber();
      assert.equal(ProposalStatus.EXPIRED, status, 'unexpected status after proposal is consumed');
    });

    it('should only be able to execute a PASSED proposal', async () => {
      const reason = '[QEC-030003]-Proposal must be PASSED before excecuting.';
      await truffleAssert.reverts(emergencyUpdateVoting.execute(proposalID, { from: ROOT }), reason);

      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });
  });

  describe('getStatus', () => {
    let proposalID;
    let endTime;
    beforeEach('init proposal', async () => {
      const res = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      proposalID = Number(res.logs[0].args._id);
      const prop = await emergencyUpdateVoting.proposals.call(proposalID);
      endTime = Number(prop.params.votingEndTime);
    });

    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await emergencyUpdateVoting.getStatus(1)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 1);
      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of voting results proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT2 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT3 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 1);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 2);

      await setTime(endTime);

      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await constitution.setUint('constitution.voting.emgQUpdateQRM', getPercentageFormat(80), {
        from: PARAMETERS_VOTING,
      });

      const result = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      const proposalID = Number(result.logs[0].args._id);
      const prop = await emergencyUpdateVoting.proposals.call(proposalID);
      const endTime = Number(prop.params.votingEndTime);

      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT2 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 0);

      await setTime(endTime);

      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
    });

    it('should return PASSED status on a passed because of voting results proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT3 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);

      await setTime(endTime);

      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXECUTED status on a consumed proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT3 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);

      await setTime(endTime);

      await emergencyUpdateVoting.execute(proposalID, { from: ROOT });
      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.EXECUTED);
    });

    it('should return EXPIRED on expired proposal', async () => {
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT });
      await emergencyUpdateVoting.voteFor(proposalID, { from: ROOT3 });
      await emergencyUpdateVoting.voteAgainst(proposalID, { from: ROOT2 });

      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightFor.toString(), 2);
      assert.equal((await emergencyUpdateVoting.proposals(proposalID)).counters.weightAgainst.toString(), 1);

      await setTime(endTime + proposalExecutionP + 1);

      assert.equal((await emergencyUpdateVoting.getStatus(proposalID)).toString(), ProposalStatus.EXPIRED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const firstProp = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      const firstPropID = Number(firstProp.logs[0].args._id);
      const firstPropBase = await emergencyUpdateVoting.proposals.call(firstPropID);

      let expectedBase = await emergencyUpdateVoting.getProposal(firstPropID);

      assert.equal(expectedBase.remark, firstPropBase.remark);
      assert.equal(expectedBase.executed, firstPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, firstPropBase.params.votingEndTime);

      const secondProp = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      const secondPropID = Number(secondProp.logs[0].args._id);
      const secondPropBase = await emergencyUpdateVoting.proposals.call(secondPropID);

      expectedBase = await emergencyUpdateVoting.getProposal(secondPropID);

      assert.equal(expectedBase.remark, secondPropBase.remark);
      assert.equal(expectedBase.executed, secondPropBase.executed);
      assert.equal(expectedBase.params.votingEndTime, secondPropBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      const reason = '[QEC-030005]-Proposal does not exist.';
      await truffleAssert.reverts(emergencyUpdateVoting.getProposal(1), reason);
    });
  });

  describe('getProposalStats', () => {
    it('should return correct voting stats structure', async () => {
      const result = await emergencyUpdateVoting.createProposal(remark, { from: ROOT });
      const propID = Number(result.logs[0].args._id);

      await emergencyUpdateVoting.voteFor(propID, { from: ROOT2 });
      await emergencyUpdateVoting.voteAgainst(propID, { from: ROOT3 });

      const propStats = await emergencyUpdateVoting.getProposalStats(propID);
      const prop = await emergencyUpdateVoting.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.params.requiredQuorum);

      const votesCount = await toBN(prop.counters.weightFor).plus(prop.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.counters.weightFor, votesCount);
      const rootsCount = 3;
      const currentQuorum = calculatePercentage(votesCount, rootsCount);

      assert.equal(prop.counters.weightFor, 1);
      assert.equal(votesCount.toNumber(), 2);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);
      assert.equal(currentQuorum.toNumber(), propStats.currentQuorum);
    });

    it('should get exception, proposal does not exist', async () => {
      const reason = '[QEC-030005]-Proposal does not exist.';
      await truffleAssert.reverts(emergencyUpdateVoting.getProposalStats(1), reason);
    });
  });
});
