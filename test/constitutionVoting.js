const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const truffleAssert = require('truffle-assertions');
const makeProxy = require('./helpers/makeProxy');
const { toBN, getPercentageFormat, calculatePercentage } = require('./helpers/defiHelper');

const { approximateAssert, lockWeight, getVetoTimeHelper, getVoteTimeHelper } = require('./helpers/votingHelper');

const { getCurrentBlockTime, setTime } = require('./helpers/hardhatTimeTraveller');

const { artifacts } = require('hardhat');
const ConstitutionVote = artifacts.require('./ConstitutionVotingMock.sol');
const QVault = artifacts.require('./QVault');
const QHolderRewardPool = artifacts.require('./QHolderRewardPool');
const Parameters = artifacts.require('./Constitution');
const ContractRegistry = artifacts.require('./ContractRegistry');
const Roots = artifacts.require('./Roots');
const AddressStorageStakes = artifacts.require('./AddressStorageStakes');
const AddressStorageStakesSorted = artifacts.require('./AddressStorageStakesSorted');
const Validators = artifacts.require('Validators');
const VotingWeightProxy = artifacts.require('VotingWeightProxy');
const CompoundRateKeeperFactory = artifacts.require('CompoundRateKeeperFactory');
const CompoundRateKeeper = artifacts.require('CompoundRateKeeper');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');

const ProposalStatus = Object.freeze({
  NONE: 0,
  PENDING: 1,
  REJECTED: 2,
  ACCEPTED: 3,
  PASSED: 4,
  EXECUTED: 5,
  OBSOLETE: 6,
  EXPIRED: 7,
});

const Classification = Object.freeze({
  BASIC: 0,
  FUNDAMENTAL: 1,
  DETAILED: 2,
});

const ParameterType = Object.freeze({
  NONE: 0,
  ADDRESS: 1,
  UINT: 2,
  STRING: 3,
  BYTE32: 4,
  BOOL: 5,
});

const DelegationStatus = Object.freeze({
  SELF: 0,
  DELEGATED: 1,
  PENDING: 2,
});

const CONSTITUTION_DECIMAL = toBN(10 ** 27);
const CONSTITUTION_HASH = '0x0BAB4BDC7C50E87F51273A1CD95D9588508349797C38432F788970693F40BF87'.toLowerCase();
const NEW_CONSTITUTION_HASH = '0xc81ff8689878486c77098faba9d872fd6b0ab442fa97d9c76ff94c5c56d6a6a9'.toLowerCase();
const EMPTY_BYTES32_HASH = '0x0000000000000000000000000000000000000000000000000000000000000000';

describe('ConstitutionVoting', () => {
  const reverter = new Reverter();

  const LINK = 'https://example.com';

  const basicQSectionVP = 600;
  const fundQSectionVP = 900;
  const detailedQSectionVP = 1200;
  const proposalExecutionP = 2592000;
  const basicQSectionQIDVTP = basicQSectionVP;
  const fundQSectionQIDVTP = fundQSectionVP;
  const detailedQSectionQIDVTP = detailedQSectionVP;

  const Q = require('./helpers/defiHelper').Q;

  let OWNER;
  let USER1;
  let USER2;
  let PARAMETERS_VOTING;

  let constitutionVote;
  let crKeeperFactory;
  let qVault;
  let qHolderRewardPool;
  let parameters;
  let registry;
  let roots;
  let validators;
  let AddressStorageStakesDeployed;
  let AddressStorageStakesSortedDepl;
  let votingWeightProxy;

  before('setup', async () => {
    OWNER = await accounts(0);
    USER1 = await accounts(1);
    USER2 = await accounts(3);
    PARAMETERS_VOTING = await accounts(2);

    registry = await ContractRegistry.new();
    await registry.initialize([OWNER], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);

    qHolderRewardPool = await makeProxy(registry.address, QHolderRewardPool);
    await qHolderRewardPool.initialize(registry.address);

    await registry.setAddress('tokeneconomics.qHolderRewardPool', qHolderRewardPool.address);

    qVault = await makeProxy(registry.address, QVault);
    crKeeperFactory = await makeProxy(registry.address, CompoundRateKeeperFactory);
    await crKeeperFactory.initialize((await CompoundRateKeeper.new()).address);
    await registry.setAddress('common.factory.crKeeper', crKeeperFactory.address);
    await qVault.initialize(registry.address);
    await registry.setAddress('tokeneconomics.qVault', qVault.address);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    parameters = await makeProxy(registry.address, Parameters);
    await parameters.initialize(registry.address, [], [], [], [], [], [], [], []);

    await parameters.setUint('constitution.expertPanels.maxNExperts', 100, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.expertPanels.changeParamVP', 100, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.expertPanels.changeParamRNVALPRD', 100, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.expertPanels.changeParamRMAJ', 0, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.maxNRootNodes', 3, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootVP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootRNVALP', 86400, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootQRM', 0, { from: PARAMETERS_VOTING });
    await parameters.setUint('constitution.voting.addOrRemRootRMAJ', getPercentageFormat(51), {
      from: PARAMETERS_VOTING,
    });

    await parameters.setUint('constitution.voting.basicQSectionVP', basicQSectionVP, { from: PARAMETERS_VOTING }); // in seconds
    await parameters.setUint('constitution.voting.fundQSectionVP', fundQSectionVP, { from: PARAMETERS_VOTING }); // in seconds
    await parameters.setUint('constitution.voting.detailedQSectionVP', detailedQSectionVP, { from: PARAMETERS_VOTING }); // in seconds
    await parameters.setUint('constitution.voting.basicQSectionRNVALP', basicQSectionQIDVTP, {
      from: PARAMETERS_VOTING,
    }); // in seconds
    await parameters.setUint('constitution.voting.fundQSectionRNVALP', fundQSectionQIDVTP, { from: PARAMETERS_VOTING }); // in seconds
    await parameters.setUint('constitution.voting.detailedQSectionRNVALP', detailedQSectionQIDVTP, {
      from: PARAMETERS_VOTING,
    }); // in seconds
    await parameters.setUint('constitution.voting.basicQSectionQRM', getPercentageFormat(0.0000000000001), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.fundQSectionQRM', getPercentageFormat(0.0000000000003), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.detailedQSectionQRM', getPercentageFormat(0.0000000000004), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.basicQSectionRMAJ', getPercentageFormat(51), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.fundQSectionRMAJ', getPercentageFormat(30), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.voting.detailedQSectionRMAJ', getPercentageFormat(53), {
      from: PARAMETERS_VOTING,
    });
    await parameters.setUint('constitution.proposalExecutionP', proposalExecutionP, { from: PARAMETERS_VOTING }); // month
    await registry.setAddress('governance.constitution.parameters', parameters.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [OWNER, USER1]);
    await registry.setAddress('governance.rootNodes', roots.address);

    AddressStorageStakesDeployed = await AddressStorageStakes.new();
    AddressStorageStakesSortedDepl = await AddressStorageStakesSorted.new();

    validators = await makeProxy(registry.address, Validators);
    await validators.initialize(
      registry.address,
      AddressStorageStakesSortedDepl.address,
      AddressStorageStakesDeployed.address
    );

    await AddressStorageStakesDeployed.transferOwnership(validators.address);
    await AddressStorageStakesSortedDepl.transferOwnership(validators.address);
    await registry.setAddress('governance.validators', validators.address);

    constitutionVote = await makeProxy(registry.address, ConstitutionVote);
    await constitutionVote.initialize(CONSTITUTION_HASH, registry.address);
    await registry.setAddress('governance.constitution.parametersVoting', constitutionVote.address);

    votingWeightProxy = await makeProxy(registry.address, VotingWeightProxy);
    await votingWeightProxy.initialize(
      registry.address,
      ['governance.validators', 'governance.rootNodes', 'tokeneconomics.qVault'],
      ['governance.constitution.parametersVoting']
    );
    await registry.setAddress('governance.votingWeightProxy', votingWeightProxy.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('constructor', () => {
    it('Should pass the test with valid classification', async () => {
      assert.equal(await constitutionVote.constitutionHash(), CONSTITUTION_HASH, 'Constitution hash set correct');
    });
  });

  describe('veto', () => {
    const proposalID = 0;
    let voteEndTime;

    const winnerBalance = toBN(10 ** 18);

    beforeEach(async () => {
      const param1 = await constitutionVote.createStrParam('1', '1');
      await constitutionVote.createProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, [param1]);

      await qVault.deposit({ value: winnerBalance, from: OWNER });
      await qVault.lock(winnerBalance, { from: OWNER });

      await constitutionVote.voteFor(proposalID);
    });

    it('should veto on proposal', async () => {
      voteEndTime = await getVoteTimeHelper(constitutionVote, proposalID);
      await setTime(voteEndTime);

      assert.equal((await constitutionVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);
      await constitutionVote.veto(proposalID);
      await constitutionVote.veto(proposalID, { from: USER1 });

      const proposal = await constitutionVote.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 2);
      assert.equal((await constitutionVote.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);

      assert.isTrue(await constitutionVote.hasRootVetoed(proposalID, OWNER));
    });

    it('should get exception, sender already put veto on this proposal', async () => {
      const reason = '[QEC-001007]-The caller has already vetoed the proposal.';
      const ROOTS_VOTING = await accounts(4);
      const RANDOM_ADDRESS = '0x7e6d16fc71e1298F0c00712843d9F31758aa5392';

      await registry.setAddress('governance.rootNodes.membershipVoting', ROOTS_VOTING);
      await roots.addMember(RANDOM_ADDRESS, { from: ROOTS_VOTING });

      voteEndTime = await getVoteTimeHelper(constitutionVote, proposalID);
      await setTime(voteEndTime);

      assert.equal((await constitutionVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);

      await constitutionVote.veto(proposalID);

      const proposal = await constitutionVote.proposals(proposalID);

      assert.equal(proposal.base.counters.vetosCount, 1);
      assert.equal((await constitutionVote.getStatus(proposalID)).toString(), ProposalStatus.ACCEPTED);

      assert.isTrue(await constitutionVote.hasRootVetoed(proposalID, OWNER));

      await truffleAssert.reverts(constitutionVote.veto(proposalID), reason);
    });

    it('should get exception, invalid proposal status', async () => {
      const param1 = await constitutionVote.createStrParam('1', '1');
      const reason = '[QEC-001006]-Proposal must be ACCEPTED before casting a root node veto.';
      await constitutionVote.createProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, [param1]);

      await truffleAssert.reverts(constitutionVote.veto(1), reason);
    });

    it('should get rejected status, all roots vetoed proposal', async () => {
      voteEndTime = await getVoteTimeHelper(constitutionVote, proposalID);
      await setTime(voteEndTime);

      await constitutionVote.veto(proposalID);
      await constitutionVote.veto(proposalID, { from: USER1 });

      const proposal = await constitutionVote.proposals(proposalID);

      assert.equal((await constitutionVote.getStatus(proposalID)).toString(), ProposalStatus.REJECTED);
      assert.equal(proposal.base.counters.vetosCount, 2);
    });

    it('should get exception, non exist proposal', async () => {
      const reason = '[QEC-001011]-The proposal does not exist.';
      await truffleAssert.reverts(constitutionVote.veto(137), reason);
    });

    it('should get exception, call by non root', async () => {
      const reason = '[QEC-001012]-Permission denied - only root nodes have access.';
      await truffleAssert.reverts(constitutionVote.veto(proposalID, { from: await accounts(7) }), reason);
    });
  });

  describe('createProposal', () => {
    it('should successfully create the proposal with parameters of all types', async () => {
      const param1 = await constitutionVote.createAddrParam('addr', OWNER);
      const param2 = await constitutionVote.createUintParam('uint', 100);
      const param3 = await constitutionVote.createStrParam('str', 'str');
      const param4 = await constitutionVote.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await constitutionVote.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      const result = await constitutionVote.createProposal('test', Classification.BASIC, CONSTITUTION_HASH, params);

      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await constitutionVote.getParam(0, i), params[i]);
      }

      assert.equal((await constitutionVote.proposals(0)).parametersSize, paramCount);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._proposal.remark, 'test');
      assert.equal(result.logs[0].args._id, 0);
    });

    it('should successfully create the proposal with many parameters of the same type', async () => {
      const param1 = await constitutionVote.createStrParam('one', '1');
      const param2 = await constitutionVote.createStrParam('two', '2');
      const param3 = await constitutionVote.createStrParam('three', '3');
      const param4 = await constitutionVote.createStrParam('four', '4');
      const param5 = await constitutionVote.createStrParam('five', '5');
      const params = [param1, param2, param3, param4, param5];

      await constitutionVote.createProposal('test', Classification.BASIC, CONSTITUTION_HASH, params);

      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await constitutionVote.getParam(0, i), params[i]);
      }

      assert.equal((await constitutionVote.proposals(0)).parametersSize, paramCount);
    });

    it('should successfully create the proposal with a different number of parameters of different types', async () => {
      const param1 = await constitutionVote.createAddrParam('addr', OWNER);
      const param2 = await constitutionVote.createAddrParam('addr2', USER1);
      const param3 = await constitutionVote.createAddrParam('addr3', USER2);
      const param4 = await constitutionVote.createUintParam('uint', 100);
      const param5 = await constitutionVote.createUintParam('uint2', 666);
      const param6 = await constitutionVote.createStrParam('str', 'str');
      const param7 = await constitutionVote.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param8 = await constitutionVote.createBoolParam('true', true);
      const param9 = await constitutionVote.createBoolParam('false', false);

      const params = [param1, param2, param3, param4, param5, param6, param7, param8, param9];

      await constitutionVote.createProposal('test', Classification.BASIC, CONSTITUTION_HASH, params);

      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      const paramCount = 9;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await constitutionVote.getParam(0, i), params[i]);
      }

      assert.equal((await constitutionVote.proposals(0)).parametersSize, paramCount);
    });

    it('should be possible to createProposal FUNDAMENTAL', async () => {
      const proposalCounter = 0;
      const param = await constitutionVote.createStrParam('test', 'test');
      const result = await constitutionVote.createProposal(LINK, Classification.FUNDAMENTAL, CONSTITUTION_HASH, [
        param,
      ]);

      assert.equal((await constitutionVote.proposalCounter()).toNumber(), 1);
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), Classification.FUNDAMENTAL);
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK);
      assert.equal((await constitutionVote.proposals(0)).newConstitutionHash.toString(), CONSTITUTION_HASH);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });

    it('should be possible to createProposal DETAILED', async () => {
      const proposalCounter = 0;
      const param = await constitutionVote.createStrParam('test', 'test');
      const result = await constitutionVote.createProposal(LINK, Classification.DETAILED, CONSTITUTION_HASH, [param]);

      assert.equal((await constitutionVote.proposalCounter()).toNumber(), 1);
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), Classification.DETAILED);
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK);
      assert.equal((await constitutionVote.proposals(0)).newConstitutionHash.toString(), CONSTITUTION_HASH);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });

    it('should be possible to createProposal with empty parameters array', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createProposal(LINK, Classification.DETAILED, CONSTITUTION_HASH, []);

      assert.equal((await constitutionVote.proposalCounter()).toNumber(), 1);
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), Classification.DETAILED);
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK);
      assert.equal((await constitutionVote.proposals(0)).newConstitutionHash.toString(), CONSTITUTION_HASH);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('createAddrProposal', () => {
    it('should be possible to createProposal with address type parameter', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createAddrProposal(
        LINK,
        Classification.BASIC,
        CONSTITUTION_HASH,
        'test',
        OWNER
      );

      assert.equal(
        (await constitutionVote.proposalCounter()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), 0, 'set correct classification');
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK, 'set correct url');
      assert.equal(
        (await constitutionVote.proposals(0)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );

      const param = await constitutionVote.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.ADDRESS);
      assert.equal(param.addrValue, OWNER);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('createBoolProposal', () => {
    it('should be possible to createProposal with boolean type parameter', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createBoolProposal(
        LINK,
        Classification.BASIC,
        CONSTITUTION_HASH,
        'test',
        true
      );

      assert.equal(
        (await constitutionVote.proposalCounter()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), 0, 'set correct classification');
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK, 'set correct url');
      assert.equal(
        (await constitutionVote.proposals(0)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );
      assert.equal(
        (await constitutionVote.proposals(proposalCounter)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );

      const param = await constitutionVote.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.BOOL);
      assert.equal(param.boolValue, true);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('createBytesProposal', () => {
    it('should be possible to createProposal with bytes32 type parameter', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createBytesProposal(
        LINK,
        Classification.BASIC,
        CONSTITUTION_HASH,
        'test',
        NEW_CONSTITUTION_HASH
      );

      assert.equal(
        (await constitutionVote.proposalCounter()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), 0, 'set correct classification');
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK, 'set correct url');
      assert.equal(
        (await constitutionVote.proposals(0)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );

      const param = await constitutionVote.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.BYTE32);
      assert.equal(param.bytes32Value, NEW_CONSTITUTION_HASH);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('createStrProposal', () => {
    it('should be possible to createProposal with string type parameter', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createStrProposal(
        LINK,
        Classification.BASIC,
        CONSTITUTION_HASH,
        'test',
        'hello world'
      );

      assert.equal(
        (await constitutionVote.proposalCounter()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), 0, 'set correct classification');
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK, 'set correct url');
      assert.equal(
        (await constitutionVote.proposals(0)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );
      assert.equal(
        (await constitutionVote.proposals(proposalCounter)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );

      const param = await constitutionVote.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.STRING);
      assert.equal(param.strValue, 'hello world');

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('createUintProposal', () => {
    it('should be possible to createProposal with uint32 type parameter', async () => {
      const proposalCounter = 0;
      const result = await constitutionVote.createUintProposal(
        LINK,
        Classification.BASIC,
        CONSTITUTION_HASH,
        'test',
        123
      );

      assert.equal(
        (await constitutionVote.proposalCounter()).toNumber(),
        proposalCounter + 1,
        'Proposal counter incremented'
      );
      assert.equal((await constitutionVote.proposals(0)).classification.toString(), 0, 'set correct classification');
      assert.equal((await constitutionVote.proposals(0)).base.remark.toString(), LINK, 'set correct url');
      assert.equal(
        (await constitutionVote.proposals(0)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );
      assert.equal(
        (await constitutionVote.proposals(proposalCounter)).newConstitutionHash.toString(),
        CONSTITUTION_HASH,
        'set correct hash'
      );

      const param = await constitutionVote.getParam(0, 0);
      assert.equal(param.paramKey, 'test');
      assert.equal(param.paramType, ParameterType.UINT);
      assert.equal(param.uintValue, 123);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalCreated');
      assert.equal(result.logs[0].args._id, proposalCounter);
    });
  });

  describe('vote', () => {
    const depositAmount = 1000;
    const lockAmount = depositAmount;

    beforeEach('vote setup', async () => {
      await qVault.deposit({ value: depositAmount });
      await qVault.deposit({ value: depositAmount, from: USER1 });
      await qVault.lock(lockAmount, { from: OWNER });
      await qVault.lock(lockAmount, { from: USER1 });

      const param = await constitutionVote.createStrParam('test', 'test');
      await constitutionVote.createProposal(LINK, Classification.BASIC, CONSTITUTION_HASH, [param]);
    });

    it('should be possible to vote for an existing pending proposal with valid amount and expiration', async () => {
      const result = await constitutionVote.voteFor(0);

      assert.equal((await constitutionVote.proposals(0)).base.counters.weightFor.toString(), 1000);
      assert.isTrue(await constitutionVote.hasUserVoted(0, OWNER));

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'UserVoted');
      assert.equal(result.logs[0].args._proposalId, 0);
      assert.equal(result.logs[0].args._votingOption, 1);
      assert.equal(result.logs[0].args._amount, 1000);
    });

    it('should not be possible to vote for not pending proposal', async () => {
      await truffleAssert.reverts(constitutionVote.voteAgainst(1), '[QEC-001011]-The proposal does not exist.');

      assert.equal((await constitutionVote.proposals(1)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await constitutionVote.proposals(1)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await constitutionVote.hasUserVoted(1, OWNER));
    });

    it('should not be possible to vote with zero total voting weight', async () => {
      await truffleAssert.reverts(
        constitutionVote.voteFor(0, { from: USER2 }),
        '[QEC-001013]-The total voting weight must be greater than zero.'
      );

      assert.equal((await constitutionVote.proposals(0)).base.counters.weightAgainst.toString(), 0);
      assert.equal((await constitutionVote.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isFalse(await constitutionVote.hasUserVoted(0, USER2));
    });

    it('should not be possible to vote if already voted', async () => {
      await constitutionVote.voteAgainst(0);

      assert.equal((await constitutionVote.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.equal((await constitutionVote.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await constitutionVote.hasUserVoted(0, OWNER));

      await truffleAssert.reverts(
        constitutionVote.voteAgainst(0),
        '[QEC-001009]-The caller has already voted for the proposal.'
      );

      assert.equal((await constitutionVote.proposals(0)).base.counters.weightAgainst.toString(), 1000);
      assert.equal((await constitutionVote.proposals(0)).base.counters.weightFor.toString(), 0);
      assert.isTrue(await constitutionVote.hasUserVoted(0, OWNER));
    });

    it('QDEV-2954 should have status passed, when weight FOR < AGAINST and RMAJ is acceptable', async () => {
      const voteFor = Q.times(4);
      const voteAgainst = Q.times(6);
      const depositForAmount = voteFor.minus(depositAmount);
      const depositAgainstAmount = voteAgainst.minus(depositAmount);

      await qVault.deposit({ value: depositForAmount, from: OWNER });
      await qVault.deposit({ value: depositAgainstAmount, from: USER1 });
      await qVault.lock(depositForAmount, { from: OWNER });
      await qVault.lock(depositAgainstAmount, { from: USER1 });

      const param = await constitutionVote.createStrParam('test1', 'test1');
      await constitutionVote.createProposal(LINK, Classification.FUNDAMENTAL, CONSTITUTION_HASH, [param]);

      const proposalId = 1;

      const voteForResult = await constitutionVote.voteFor(proposalId, { from: OWNER });

      assert.equal(
        (await constitutionVote.proposals(proposalId)).base.counters.weightFor.toString(),
        voteFor.toString()
      );
      assert.isTrue(await constitutionVote.hasUserVoted(proposalId, OWNER));

      assert.equal(voteForResult.logs.length, 1);
      assert.equal(voteForResult.logs[0].event, 'UserVoted');
      assert.equal(voteForResult.logs[0].args._proposalId, proposalId);
      assert.equal(voteForResult.logs[0].args._votingOption, 1);
      assert.equal(voteForResult.logs[0].args._amount, voteFor.toString());

      const voteAgainstResult = await constitutionVote.voteAgainst(proposalId, { from: USER1 });

      assert.equal(
        (await constitutionVote.proposals(proposalId)).base.counters.weightAgainst.toString(),
        voteAgainst.toString()
      );
      assert.isTrue(await constitutionVote.hasUserVoted(proposalId, USER1));

      assert.equal(voteAgainstResult.logs.length, 1);
      assert.equal(voteAgainstResult.logs[0].event, 'UserVoted');
      assert.equal(voteAgainstResult.logs[0].args._proposalId, proposalId);
      assert.equal(voteAgainstResult.logs[0].args._votingOption, 2);
      assert.equal(voteAgainstResult.logs[0].args._amount, voteAgainst.toString());

      const vetoEndTime = await getVetoTimeHelper(constitutionVote, proposalId);
      await setTime(vetoEndTime);

      assert.equal((await constitutionVote.getStatus(proposalId)).toString(), ProposalStatus.PASSED);

      const txReceipt = await constitutionVote.execute(proposalId);

      assert.equal((await constitutionVote.getStatus(proposalId)).toString(), ProposalStatus.EXECUTED);

      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, proposalId);
    });
  });

  describe('getStatus', () => {
    let voteEndTime;
    let vetoEndTime;
    const winnerBalance = toBN(10 ** 18);
    const loserBalance = 999;

    beforeEach('status setup', async () => {
      await qVault.deposit({ value: winnerBalance, from: OWNER });
      await qVault.lock(winnerBalance, { from: OWNER });

      await qVault.deposit({ value: loserBalance, from: USER1 });
      await qVault.lock(loserBalance, { from: USER1 });

      const param = await constitutionVote.createStrParam('test', 'test');
      await constitutionVote.createProposal(LINK, Classification.BASIC, CONSTITUTION_HASH, [param]);
    });

    it('should get status NONE if proposal does not exists', async () => {
      assert.equal((await constitutionVote.getStatus(1)).toString(), ProposalStatus.NONE);
    });

    it('should return PENDING status on a pending proposal', async () => {
      await constitutionVote.voteFor(0);

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.PENDING);
    });

    it('should return REJECTED status on a rejected because of votingParams results proposal', async () => {
      await constitutionVote.voteAgainst(0, { from: USER1 });
      assert.equal((await constitutionVote.proposals(0)).base.counters.weightAgainst.toString(), loserBalance);

      voteEndTime = await getVoteTimeHelper(constitutionVote, 0);
      await setTime(voteEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return REJECTED because actual quorum < constitution quorum', async () => {
      await constitutionVote.voteAgainst(0, { from: USER1 });
      assert.equal(toBN((await constitutionVote.proposals(0)).base.counters.weightAgainst).toString(), loserBalance);

      voteEndTime = await getVoteTimeHelper(constitutionVote, 0);
      await setTime(voteEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.REJECTED);
    });

    it('should return ACCEPTED status on a accepted because of votingParams results proposal', async () => {
      await constitutionVote.voteFor(0, { from: USER1 });
      await constitutionVote.voteFor(0, { from: OWNER });

      voteEndTime = await getVoteTimeHelper(constitutionVote, 0);
      await setTime(voteEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.ACCEPTED);
    });

    it('should return PASSED status on a passed because of votingParams results proposal', async () => {
      await constitutionVote.voteFor(0);
      await constitutionVote.voteAgainst(0, { from: USER1 });

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.PASSED);
    });

    it('should return EXPIRED status on a expired proposal', async () => {
      await constitutionVote.voteFor(0);
      await constitutionVote.voteAgainst(0, { from: USER1 });

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime + proposalExecutionP + 1);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });

    it('should return EXECUTED status on a consumed proposal', async () => {
      await constitutionVote.voteFor(0);
      await constitutionVote.voteAgainst(0, { from: USER1 });

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime);
      await constitutionVote.execute(0);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXECUTED);
    });
  });

  describe('constitutionHash and getConstitutionHash', () => {
    let vetoEndTime;

    it('Should return constructor hash', async () => {
      assert.equal((await constitutionVote.constitutionHash()).toString(), CONSTITUTION_HASH, 'Hash is equal');
    });

    it('Should return new constitution hash after execute proposal', async () => {
      const winnerBalance = toBN(10 ** 18);
      await qVault.deposit({ value: winnerBalance, from: OWNER });
      await qVault.lock(winnerBalance, { from: OWNER });

      const param = await constitutionVote.createStrParam('test', 'test');
      await constitutionVote.createProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, [param]);

      await constitutionVote.voteFor(0);

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime);

      await constitutionVote.execute(0);
      assert.equal((await constitutionVote.constitutionHash()).toString(), NEW_CONSTITUTION_HASH, 'Hash is equal');
    });

    it('Should return empty constitution hash for not existent proposal', async () => {
      assert.equal((await constitutionVote.getConstitutionHash(0)).toString(), EMPTY_BYTES32_HASH, 'Hash is equal');
    });

    it('Should return constitution hash by proposal id', async () => {
      const param = await constitutionVote.createStrParam('test', 'test');
      await constitutionVote.createProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, [param]);
      assert.equal((await constitutionVote.getConstitutionHash(0)).toString(), NEW_CONSTITUTION_HASH, 'Hash is equal');
    });
  });

  describe('execute', () => {
    const winnerBalance = toBN(10).pow(18);
    const loserBalance = toBN(1000);
    beforeEach('status setup', async () => {
      await qVault.deposit({ value: winnerBalance, from: OWNER });
      await qVault.lock(winnerBalance, { from: OWNER });

      await qVault.deposit({ value: loserBalance, from: USER1 });
      await qVault.lock(loserBalance, { from: USER1 });
    });

    it('should be possible to execute passed proposal', async () => {
      await constitutionVote.createUintProposal('test', Classification.BASIC, NEW_CONSTITUTION_HASH, 'test', 111);

      await constitutionVote.voteFor(0, { from: OWNER });

      const vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.PASSED);

      const txReceipt = await constitutionVote.execute(0);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(txReceipt.logs.length, 1);
      assert.equal(txReceipt.logs[0].event, 'ProposalExecuted');
      assert.equal(txReceipt.logs[0].args._proposalId, 0);

      assert.equal((await parameters.getUint('test')).toString(), 111);
    });

    it('should set correct parameters of different types', async () => {
      const param1 = await constitutionVote.createAddrParam('addr', OWNER);
      const param2 = await constitutionVote.createUintParam('uint', 100);
      const param3 = await constitutionVote.createStrParam('str', 'str');
      const param4 = await constitutionVote.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await constitutionVote.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      await constitutionVote.createProposal('test', Classification.BASIC, NEW_CONSTITUTION_HASH, params);

      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await constitutionVote.getParam(0, i), params[i]);
      }

      assert.equal((await constitutionVote.proposals(0)).parametersSize, paramCount);

      await constitutionVote.voteFor(0);

      const vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.PASSED);

      const result = await constitutionVote.execute(0);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalExecuted');
      assert.equal(result.logs[0].args._proposalId, 0);

      assert.equal((await parameters.getAddr('addr')).toString(), OWNER);
      assert.equal((await parameters.getUint('uint')).toString(), 100);
      assert.equal((await parameters.getString('str')).toString(), 'str');
      assert.equal(web3.utils.toAscii(await parameters.getBytes32('bytes')).replace(/\0/g, ''), 'bytes');
      assert.equal(await parameters.getBool('true'), true);
    });

    it('should set correct parameters of the same type', async () => {
      const param1 = await constitutionVote.createStrParam('1', '1');
      const param2 = await constitutionVote.createStrParam('2', '2');
      const param3 = await constitutionVote.createStrParam('3', '3');
      const param4 = await constitutionVote.createStrParam('4', '4');
      const param5 = await constitutionVote.createStrParam('5', '5');
      const params = [param1, param2, param3, param4, param5];

      await constitutionVote.createProposal('test', Classification.BASIC, NEW_CONSTITUTION_HASH, params);

      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      const paramCount = 5;
      for (let i = 0; i < paramCount; i++) {
        assert.deepEqual(await constitutionVote.getParam(0, i), params[i]);
      }

      assert.equal((await constitutionVote.proposals(0)).parametersSize, paramCount);
      assert.equal((await constitutionVote.proposalCounter()).toString(), 1);

      await constitutionVote.voteFor(0);
      const vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);

      await setTime(vetoEndTime);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.PASSED);

      const result = await constitutionVote.execute(0);

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXECUTED);

      assert.equal(result.logs.length, 1);
      assert.equal(result.logs[0].event, 'ProposalExecuted');
      assert.equal(result.logs[0].args._proposalId, 0);

      for (let i = 1; i <= paramCount; i++) {
        assert.equal(await parameters.getString(i.toString()), i);
      }
    });

    it('should not be possible to execute not passed proposal', async () => {
      await constitutionVote.createUintProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, 'test', 123);

      await constitutionVote.voteFor(0);

      await truffleAssert.reverts(
        constitutionVote.execute(0),
        '[QEC-001004]-Proposal must be PASSED before excecuting.'
      );
    });

    it('should not be possible to execute obsolete proposal', async () => {
      const paramValue = 123;
      const paramValue1 = 1234;

      await constitutionVote.createUintProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, 'test', paramValue);

      await constitutionVote.voteFor(0, { from: OWNER });

      await constitutionVote.createUintProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, 'test', paramValue1);

      await constitutionVote.voteFor(1, { from: OWNER });

      const vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);
      await setTime(vetoEndTime + basicQSectionVP + 1);
      await constitutionVote.execute(1);

      await truffleAssert.reverts(
        constitutionVote.execute(0),
        '[QEC-001004]-Proposal must be PASSED before excecuting.'
      );

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.OBSOLETE);
      assert.equal((await constitutionVote.getStatus(1)).toString(), ProposalStatus.EXECUTED);

      assert.equal((await parameters.getUint('test')).toString(), paramValue1, `Test value ${paramValue1}`);
    });

    it('should not be possible to execute expired proposal', async () => {
      const paramValue = 123;

      await constitutionVote.createUintProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, 'test', paramValue);

      await constitutionVote.voteFor(0, { from: OWNER });
      await constitutionVote.voteAgainst(0, { from: USER1 });

      assert.equal(
        toBN((await constitutionVote.proposals(0)).base.counters.weightFor).toString(),
        winnerBalance.toString()
      );

      const vetoEndTime = await getVetoTimeHelper(constitutionVote, 0);

      await setTime(vetoEndTime + proposalExecutionP + 1);
      await truffleAssert.reverts(
        constitutionVote.execute(0),
        '[QEC-001004]-Proposal must be PASSED before excecuting.'
      );

      assert.equal((await constitutionVote.getStatus(0)).toString(), ProposalStatus.EXPIRED);
    });
  });

  describe('getProposal', () => {
    it('should return correct proposal base', async () => {
      const prop = await constitutionVote.createUintProposal(
        LINK,
        Classification.BASIC,
        NEW_CONSTITUTION_HASH,
        'test',
        123
      );
      const propID = Number(prop.logs[0].args._id);
      const propBase = (await constitutionVote.proposals.call(propID)).base;

      const expectedBase = await constitutionVote.getProposal(propID);

      assert.equal(expectedBase.remark, propBase.remark);
      assert.equal(expectedBase.executed, propBase.executed);
      assert.equal(expectedBase.params.votingEndTime, propBase.params.votingEndTime);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(constitutionVote.getProposal(1), '[QEC-001011]-The proposal does not exist.');
    });
  });

  describe('getParametersArr', () => {
    it('should return correct parameters array', async () => {
      const param1 = await constitutionVote.createAddrParam('addr', OWNER);
      const param2 = await constitutionVote.createUintParam('uint', 100);
      const param3 = await constitutionVote.createStrParam('str', 'str');
      const param4 = await constitutionVote.createBytes32Param('bytes', web3.utils.fromAscii('bytes'));
      const param5 = await constitutionVote.createBoolParam('true', true);
      const params = [param1, param2, param3, param4, param5];

      await constitutionVote.createProposal('test', Classification.BASIC, CONSTITUTION_HASH, params);

      assert.deepEqual(await constitutionVote.getParametersArr(0), params);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(constitutionVote.getParametersArr(1), '[QEC-001011]-The proposal does not exist.');
    });
  });

  describe('getProposalStats', () => {
    const winnerBalance = toBN(10 ** 18);
    const loserBalance = 999;

    before('status setup', async () => {
      await qVault.deposit({ value: winnerBalance, from: OWNER });
      await qVault.lock(winnerBalance, { from: OWNER });

      await qVault.deposit({ value: loserBalance, from: USER2 });
      await qVault.lock(loserBalance, { from: USER2 });
    });

    it('should return correct voting stats structure', async () => {
      const param = await constitutionVote.createStrParam('test', 'test');
      const result = await constitutionVote.createProposal(LINK, Classification.BASIC, CONSTITUTION_HASH, [param]);
      const propID = Number(result.logs[0].args._id);

      await constitutionVote.voteFor(0, { from: USER2 });
      await constitutionVote.voteFor(0, { from: OWNER });

      await setTime((await getCurrentBlockTime()) + basicQSectionVP);
      let status = await constitutionVote.getStatus(0);
      if (status === ProposalStatus.PENDING) {
        // give an extra time unit to switch to accepted
        await setTime((await getCurrentBlockTime()) + 100);
        status = await constitutionVote.getStatus(0);
      }
      assert.equal(status, ProposalStatus.ACCEPTED);

      const blockNumber = (await constitutionVote.veto(0, { from: USER1 })).receipt.blockNumber;
      const propStats = await constitutionVote.getProposalStats(propID);
      const prop = await constitutionVote.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const totalWeight = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalWeight);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);

      assert.isTrue(approximateAssert(totalWeight, blockNumber, propStats.currentQuorum));

      const vetoesCount = prop.base.counters.vetosCount;
      const expectedVetoPercentage = toBN(vetoesCount).times(CONSTITUTION_DECIMAL).dividedBy(2);
      assert.equal(propStats.currentVetoPercentage, expectedVetoPercentage.toNumber());
    });

    it('should return correct voting stats structure before the first vote', async () => {
      const param = await constitutionVote.createStrParam('test', 'test');
      const result = await constitutionVote.createProposal(LINK, Classification.BASIC, CONSTITUTION_HASH, [param]);
      const propID = Number(result.logs[0].args._id);

      const propStats = await constitutionVote.getProposalStats(propID);
      const prop = await constitutionVote.proposals.call(propID);

      assert.equal(propStats.requiredMajority, prop.base.params.requiredMajority);
      assert.equal(propStats.requiredQuorum, prop.base.params.requiredQuorum);

      const totalWeight = toBN(prop.base.counters.weightFor).plus(prop.base.counters.weightAgainst);
      const currentMajority = calculatePercentage(prop.base.counters.weightFor, totalWeight);

      assert.equal(currentMajority.toNumber(), propStats.currentMajority);
      assert.equal(propStats.currentQuorum, 0);
      assert.equal(propStats.currentVetoPercentage, 0);
    });

    it('should get exception, proposal does not exist', async () => {
      await truffleAssert.reverts(constitutionVote.getProposalStats(1), '[QEC-001011]-The proposal does not exist.');
    });
  });

  describe('getVotingWeightInfo()', () => {
    let proposalID;

    beforeEach(async () => {
      const result = await constitutionVote.createUintProposal(
        LINK,
        Classification.BASIC,
        NEW_CONSTITUTION_HASH,
        'test',
        123
      );
      proposalID = Number(result.logs[0].args._id);
    });

    it('should successfully return the voting weight info', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await constitutionVote.voteFor(proposalID, { from: USER1 });

      const votingWeightInfo = await constitutionVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.ownWeight, lockedWeightForUser.toString());
      assert.equal(votingWeightInfo.base.votingAgent, USER1);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.SELF);
      assert.equal(votingWeightInfo.base.lockedUntil, await votingWeightProxy.getLockedUntil(USER1));
      assert.equal(votingWeightInfo.hasAlreadyVoted, true);
      assert.equal(votingWeightInfo.canVote, false);
    });

    it('should return PENDING status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);
      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });

      const votingWeightInfo = await constitutionVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.PENDING);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER1)).toString());
    });

    it('should return DELEGATED status', async () => {
      const lockedWeightForUser = toBN(60000);
      await lockWeight(qVault, USER1, lockedWeightForUser);

      await votingWeightProxy.announceNewVotingAgent(USER2, { from: USER1 });
      const delegationInfo = await votingWeightProxy.delegationInfos(USER1);

      await setTime((await getCurrentBlockTime()) + toBN(delegationInfo.votingAgentPassOverTime).plus(1).toNumber());

      await votingWeightProxy.setNewVotingAgent({ from: USER1 });

      const param1 = await constitutionVote.createStrParam('1', '1');
      const result = await constitutionVote.createProposal(LINK, Classification.BASIC, NEW_CONSTITUTION_HASH, [param1]);
      proposalID = Number(result.logs[0].args._id);
      await constitutionVote.voteFor(proposalID, { from: USER2 });

      const votingWeightInfo = await constitutionVote.getVotingWeightInfo(proposalID, { from: USER1 });

      assert.equal(votingWeightInfo.base.votingAgent, USER2);
      assert.equal(votingWeightInfo.base.delegationStatus, DelegationStatus.DELEGATED);
      assert.equal(votingWeightInfo.base.lockedUntil, (await votingWeightProxy.getLockedUntil(USER2)).toString());
    });

    it('should revert if the slashing proposal does not exist', async () => {
      await truffleAssert.reverts(
        constitutionVote.getVotingWeightInfo(10001, { from: USER1 }),
        '[QEC-001011]-The proposal does not exist.'
      );
    });
  });
});
