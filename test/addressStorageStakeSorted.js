const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const { toBN } = require('./helpers/defiHelper');
const truffleAssert = require('truffle-assertions');

const { artifacts } = require('hardhat');
const AddressStorageStakesSorted = artifacts.require('AddressStorageStakesSorted');

describe('AddressStorageStakesSorted', () => {
  const reverter = new Reverter();

  const HEAD = '0x'.padEnd(42, '0');
  const TAIL = '0x' + '1'.padStart(40, '0');

  const ONE = toBN(1e18);

  let storage;

  before('setup', async () => {
    storage = await AddressStorageStakesSorted.new();

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('addAddr', () => {
    it('should add an address', async () => {
      const SOMEBODY = await accounts(0);
      // check initial state
      assert.equal(await storage.listSize(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());

      await storage.addAddr(SOMEBODY, ONE);
      assert.equal((await storage.addrStake(SOMEBODY)).toString(), ONE);

      assert.equal(await storage.listSize(), 1);
      assert.equal(await storage.contains(SOMEBODY), true);
      assert.equal((await storage.getAddresses()).toString(), [SOMEBODY].toString());

      await storage.removeLast();
      assert.equal((await storage.addrStake(SOMEBODY)).toString(), 0);

      assert.equal(await storage.listSize(), 0);
      assert.equal(await storage.contains(SOMEBODY), false);
      assert.equal((await storage.getAddresses()).toString(), [].toString());
    });

    it('should add in correct place', async () => {
      await storage.addAddr(await accounts(0), ONE);
      await storage.addAddr(await accounts(1), ONE.div(2));
      await storage.addAddr(await accounts(2), ONE.times(2));

      assert.deepEqual(await storage.getAddresses(), [await accounts(2), await accounts(0), await accounts(1)]);

      // this time, the highest value first, then the lowest
      await storage.addAddr(await accounts(4), ONE.times(3));
      await storage.addAddr(await accounts(5), ONE.div(4));

      // and one in the middle
      await storage.addAddr(await accounts(6), ONE.times(1.1));
      assert.deepEqual(await storage.getAddresses(), [
        await accounts(4),
        await accounts(2),
        await accounts(6),
        await accounts(0),
        await accounts(1),
        await accounts(5),
      ]);
    });

    it('should fail add to add the same addr twice', async () => {
      const acc0 = await accounts(0);
      await storage.addAddr(acc0, ONE);

      const reason =
        '[QEC-037000]-The address has already been added to the storage, ' +
        'failed to add to the address stakes sorted storage.';
      // shouldn't be able to add no matter what the stake is
      // first, try to add with the same stake
      await truffleAssert.reverts(storage.addAddr(acc0, ONE), reason);

      // try adding with a different stake
      await truffleAssert.reverts(storage.addAddr(acc0, ONE.times(2)), reason);
    });

    it('should not add with empty stake', async () => {
      await truffleAssert.reverts(
        storage.addAddr(await accounts(0), toBN(0)),
        '[QEC-037001]-Invalid stake, failed to add the address to the storage.'
      );
    });

    it('should not add address of HEAD', async () => {
      const initialSize = 3;
      for (let i = 0; i < initialSize; i++) {
        await storage.addAddr(await accounts(i), ONE.multipliedBy(i + 1).plus(1));
      }
      await truffleAssert.reverts(
        storage.addAddr(HEAD, ONE.minus(1)),
        '[QEC-037005]-The address should not be HEAD or TAIL.'
      );

      const largeStake = ONE.multipliedBy(99);
      await storage.addAddr(await accounts(initialSize), largeStake);

      const neededAddress = await accounts(initialSize);
      const addresses = await storage.getAddresses();
      let prevStake = largeStake.plus(1);
      for (let i = 0; i < addresses.length; i++) {
        const currStake = toBN(await storage.addrStake(addresses[i]));
        assert.equal(currStake.lte(prevStake), true, 'Addresses are not sorted by stake');
        prevStake = currStake;
      }
      assert.equal(addresses.includes(neededAddress), true);
      assert.equal(await storage.contains(neededAddress), true);
      assert.equal(addresses.length, initialSize + 1);
    });

    it('should not add address of TAIL', async () => {
      const initialSize = 3;
      for (let i = 0; i < initialSize; i++) {
        await storage.addAddr(await accounts(i), ONE.multipliedBy(i).plus(1));
      }
      await truffleAssert.reverts(storage.addAddr(TAIL, ONE), '[QEC-037005]-The address should not be HEAD or TAIL.');

      const neededAddress = await accounts(0);
      const addresses = await storage.getAddresses();
      assert.equal(addresses.includes(neededAddress), true);
      assert.equal(await storage.contains(neededAddress), true);
      assert.equal(addresses.length, initialSize);
    });
  });

  describe('updateStake', () => {
    let TOP;
    let MIDDLE;
    let BOTTOM;

    before('setup', async () => {
      TOP = await accounts(0);
      MIDDLE = await accounts(1);
      BOTTOM = await accounts(2);
    });

    beforeEach(async () => {
      await storage.addAddr(TOP, ONE.times(3));
      await storage.addAddr(MIDDLE, ONE.times(2));
      await storage.addAddr(BOTTOM, ONE);

      assert.deepEqual(await storage.getAddresses(), [TOP, MIDDLE, BOTTOM], 'unexpected order');
    });

    it('should go to the bottom', async () => {
      await storage.updateStake(TOP, ONE.minus(1));
      assert.deepEqual(await storage.getAddresses(), [MIDDLE, BOTTOM, TOP]);
    });

    it('should go to the top', async () => {
      await storage.updateStake(BOTTOM, ONE.times(4));
      assert.deepEqual(await storage.getAddresses(), [BOTTOM, TOP, MIDDLE]);
    });

    it('should go to the middle', async () => {
      await storage.updateStake(BOTTOM, ONE.times(2.5));
      assert.deepEqual(await storage.getAddresses(), [TOP, BOTTOM, MIDDLE]);
    });

    it('should be removed after updating to zero stake', async () => {
      await storage.updateStake(MIDDLE, toBN(0));
      assert.deepEqual(await storage.getAddresses(), [TOP, BOTTOM]);
    });

    it('should not update stake for non-existing address', async () => {
      truffleAssert.reverts(
        storage.updateStake(await accounts(9), ONE),
        '[QEC-037002]-The address is not in the storage, failed to update the stake.'
      );
    });
  });

  describe('removeLast', () => {
    it('should remove last by stake', async () => {
      await storage.addAddr(await accounts(0), ONE);
      await storage.addAddr(await accounts(1), ONE.div(2));
      await storage.addAddr(await accounts(2), ONE.times(2));

      await storage.removeLast();
      assert.deepEqual(await storage.getAddresses(), [await accounts(2), await accounts(0)]);
    });
    it('should work correctly for one element', async () => {
      await storage.addAddr(await accounts(0), ONE);
      await storage.removeLast();

      assert.deepEqual(await storage.getAddresses(), []);
    });

    it('should not remove when already empty', async () => {
      truffleAssert.reverts(storage.removeLast(), '[QEC-037003]-The list is empty, failed to remove the address.');
    });
  });
});
