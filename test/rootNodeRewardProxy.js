const { accounts } = require('./helpers/utils.js');
const { assert } = require('chai');
const Reverter = require('./helpers/reverter');
const makeProxy = require('./helpers/makeProxy');

const { artifacts } = require('hardhat');
const RootNodeRewardProxy = artifacts.require('./RootNodeRewardProxy');
const Roots = artifacts.require('./Roots');

const ContractRegistry = artifacts.require('./ContractRegistry');
const AddressStorageFactory = artifacts.require('AddressStorageFactory');
const AddressStorage = artifacts.require('AddressStorage');
const PushPayments = artifacts.require('PushPayments');

describe('RootNodeRewardProxy', () => {
  const reverter = new Reverter();

  let roots;
  let rootNodeRewardProxy;
  let pushPayments;
  let registry;

  const Q = require('./helpers/defiHelper').Q;

  let DEFAULT;
  let PARAMETERS_VOTING;
  let USER1;
  let USER2;

  before(async () => {
    DEFAULT = await accounts(0);
    PARAMETERS_VOTING = await accounts(1);
    USER1 = await accounts(2);
    USER2 = await accounts(3);

    registry = await ContractRegistry.new();
    await registry.initialize([DEFAULT], ['governance.constitution.parametersVoting'], [PARAMETERS_VOTING]);
    const addressStorageFactory = await makeProxy(registry.address, AddressStorageFactory);
    await addressStorageFactory.initialize((await AddressStorage.new()).address);
    await registry.setAddress('common.factory.addressStorage', addressStorageFactory.address);

    pushPayments = await makeProxy(registry.address, PushPayments);
    await registry.setAddress('tokeneconomics.pushPayments', pushPayments.address);

    roots = await makeProxy(registry.address, Roots);
    await roots.initialize(registry.address, [USER1, USER2]);
    await registry.setAddress('governance.rootNodes', roots.address);

    rootNodeRewardProxy = await makeProxy(registry.address, RootNodeRewardProxy);
    await rootNodeRewardProxy.initialize(registry.address);
    await registry.setAddress('tokeneconomics.rootNodeRewardProxy', rootNodeRewardProxy.address);

    await reverter.snapshot();
  });

  afterEach('revert', async () => {
    await reverter.revert();
  });

  describe('allocate event', () => {
    it('should correctly distribute balance amongst roots', async () => {
      const amount = Q;
      await rootNodeRewardProxy.send(amount);
      const initialUser1Balance = await web3.eth.getBalance(USER1);
      const initialUser2Balance = await web3.eth.getBalance(USER2);

      const balance = await web3.eth.getBalance(rootNodeRewardProxy.address);
      assert.equal(balance, amount, 'unexpected initial balance');

      const resultTx = await rootNodeRewardProxy.allocate();
      assert.equal(resultTx.receipt.logs[0].event, 'Allocated');

      assert.equal(
        (await web3.eth.getBalance(USER1)).toString(),
        Q.times(0.5).plus(initialUser1Balance).toString() // 1.0/2.0
      );

      assert.equal(
        (await web3.eth.getBalance(USER2)).toString(),
        Q.times(0.5).plus(initialUser2Balance).toString() // 1.0/2.0
      );
    });
  });
});
