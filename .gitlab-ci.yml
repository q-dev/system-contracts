image: node:18.17.1

cache:
  paths:
    - node_modules/
    - .npm
    - docs/

# We tell GitLab to install all the packages
# before running anything.
# Docker images come with npm preinstalled
.before_script_template:
  before_script:
    - git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf https://gitlab.com/
    - go env -w GOPRIVATE=gitlab.com/${CI_PROJECT_NAMESPACE}
    - apt-get update -qq && apt-get install

stages:
  - build
  - test
  - analyze
  - docs
  - release

compile:
  stage: build
  only:
    - master
    - dev
    - tags
    - merge_requests
  before_script:
    - npm config set cache .npm --global
    - npm install
  script:
    - npm run compile
  artifacts:
    paths:
      - artifacts

run_linters:
  stage: analyze
  needs: []
  only:
    - master
    - dev
    - tags
    - merge_requests
  before_script:
    - npm config set cache .npm --global
    - npm install
  script:
    - npm run lint
    - ./scripts/checkTODO.sh

test_hardhat:
  stage: test
  needs: [compile]
  only:
    - master
    - dev
    - tags
    - merge_requests
  before_script:
    - npm config set cache .npm --global
    - npm install
    - node_modules/.bin/mocha --version
  script:
    - npx hardhat test
  artifacts:
    paths:
      - artifacts

abi_job:
  stage: analyze
  needs: [compile]
  only:
    - dev
    - merge_requests
  before_script:
    - npm config set cache .npm --global
    - npm install
    - npx hardhat compile
    - ./scripts/compare-abi.sh -s master_snapshot
    - cd .. && mkdir tmp && cd tmp && git clone https://gitlab.com/q-dev/system-contracts.git && cd system-contracts
    - npm install
    - npx hardhat compile
    - ./scripts/compare-abi.sh -s pulled_master
    - cp ./abi/pulled_master.json ../../system-contracts/abi
    - cd ../../system-contracts
  script:
    - ./scripts/compare-abi.sh --snp-snp ./abi/pulled_master.json ./abi/master_snapshot.json
  artifacts:
    name: abi
    paths:
      - abi
    expire_in: 1 year

extract_abi:
  stage: docs
  needs: [compile]
  only:
    - master
    - tags
  before_script:
    - npm config set cache .npm --global
    - npm install
    - npm run compile
  script:
    - ./scripts/compare-abi.sh -s
  artifacts:
    name: abi
    paths:
      - abi

extract_bytecode:
  stage: docs
  needs: [compile]
  only:
    - master
    - tags
  before_script:
    - npm config set cache .npm --global
    - npm install
    - npm run compile
  script:
    - ./scripts/bytecode.sh
  artifacts:
    name: bytecodes
    paths:
      - bytecodes

extract_storage_layout:
  stage: docs
  needs: [compile]
  only:
    - master
    - tags
  before_script:
    - npm config set cache .npm --global
    - npm install
    - npm run compile
  script:
    - npx hardhat storage:save
  artifacts:
    name: storage_snapshots
    paths:
      - storage_snapshots

pages:
  stage: docs
  before_script:
    - npm config set cache .npm --global
  script:
    - mkdir public/
    - cp -R ./assets/* public/
    - cp ./scripts/docgen.sh ../
    - ../docgen.sh master latest
    - ../docgen.sh mainnet mainnet
    - ../docgen.sh testnet testnet
    - ../docgen.sh devnet devnet
  artifacts:
    paths:
      - docs
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
    - if: '$CI_COMMIT_TAG == "mainnet"'
    - if: '$CI_COMMIT_TAG == "devnet"'
    - if: '$CI_COMMIT_TAG == "testnet"'

publish:
  stage: release
  needs: []
  only: [tags]
  before_script:
    - npm config set _authToken $NPM_ACCESS_TOKEN
  script:
    - cp ./LICENSE ./contracts/LICENSE
    - npm publish --access public ./contracts

push_bindings:
  stage: release
  image: golang:1.20
  needs: [compile]
  only:
    - tags
    - master
  before_script:
    - git clone https://gitlab.com/q-dev/q-client.git
    - mkdir -p build/contracts
    - shopt -s globstar
    - cp artifacts/@dlsl/**/*.json build/contracts/
    - cp artifacts/@opengsn/**/*.json build/contracts/
    - cp artifacts/@openzeppelin/**/*.json build/contracts/
    - cp artifacts/contracts/**/*.json build/contracts/
    - rm -rf  build/contracts/**.dbg.json
    - cd q-client
    - env GOBIN=/tmp go install ./cmd/abigen
    - cd ..
    - ./scripts/generate-bindings.sh
  script:
    - git clone https://$USER:$PASSWORD@gitlab.com/q-dev/system-contract-bindings-go.git
    - cd system-contract-bindings-go
    - cp -rf ../generated .
    - git config --global user.email "our@email.com"
    - git config --global user.name "Gitlab Runner"
    - git add .
    - ../scripts/push-bindings.sh $USER $PASSWORD
