import "@nomicfoundation/hardhat-ethers";

import "@typechain/hardhat";

import "@nomiclabs/hardhat-web3";
import "@nomiclabs/hardhat-truffle5";

import "@solarity/hardhat-markup";
import "@solarity/hardhat-migrate";
import "@solarity/hardhat-smart-compare";

import "hardhat-gas-reporter";
import "hardhat-abi-exporter";
import "hardhat-contract-sizer";

import "solidity-coverage";

import "tsconfig-paths/register";

import { HardhatUserConfig } from "hardhat/config";

import * as dotenv from "dotenv";
dotenv.config();

// const gasPrice = Number(process.env.GAS_PRICE?process.env.GAS_PRICE:50_000_000_000);
const accounts = process.env.PRIVATE_KEY
  ? [process.env.PRIVATE_KEY]
  : undefined;

const config: HardhatUserConfig = {
  networks: {
    hardhat: {
      initialDate: "1970-01-01T00:00:00Z",
      gasPrice: 1,
      accounts: {
        accountsBalance: "1000000000000000000000000000000",
      },
      hardfork: "berlin",
    },
    localhost: {
      url: "http://127.0.0.1:8545",
      initialDate: "1970-01-01T00:00:00Z",
      gas: 6721975,
      gasPrice: 1,
      hardfork: "berlin",
    },
    devnet: {
      url: "https://rpc.qdevnet.org/",
      accounts,
    },
    testnet: {
      url: "https://rpc.qtestnet.org/",
      accounts,
    },
    mainnet: {
      url: "https://rpc.q.org",
      accounts,
    },
    sepolia: {
      url: `https://sepolia.infura.io/v3/${process.env.INFURA_KEY}`,
      accounts,
    },
    nightly: {
      url: `${process.env.NIGHTLY_RPC_URL}`,
      accounts,
    },
  },
  etherscan: {
    apiKey: {
      devnet: "abc",
      testnet: "abc",
      mainnet: "abc",
      nightly: "abc",
      sepolia: `${process.env.ETHERSCAN_KEY}`,
    },
    customChains: [
      {
        network: "devnet",
        chainId: 35442,
        urls: {
          apiURL: "https://explorer.qdevnet.org/api",
          browserURL: "https://explorer.qdevnet.org",
        },
      },
      {
        network: "testnet",
        chainId: 35443,
        urls: {
          apiURL: "https://explorer.qtestnet.org/api",
          browserURL: "https://explorer.qtestnet.org",
        },
      },
      {
        network: "mainnet",
        chainId: 35441,
        urls: {
          apiURL: "https://explorer.q.org/api",
          browserURL: "https://explorer.q.org",
        },
      },
      {
        network: `${process.env.NIGHTLY_NETWORK}`,
        chainId: Number(process.env.NIGHTLY_CHAIN_ID),
        urls: {
          apiURL: `${process.env.NIGHTLY_API_URL}`,
          browserURL: `${process.env.NIGHTLY_BROWSER_URL}`,
        },
      },
    ],
  },
  migrate: {
    pathToMigrations: "./deploy/",
  },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 50,
      },
      evmVersion: "berlin",
      outputSelection: {
        "*": {
          "*": ["storageLayout"],
        },
      },
    },
  },
  markup: {
    outdir: "./docs",
    skipFiles: ["contracts/defi/token/IERC677.sol"],
  },
  mocha: {
    // timeout: 1000000,
  },
  typechain: {
    outDir: `generated-types`,
    target: "ethers-v6",
    alwaysGenerateOverloads: true,
    discriminateTypes: true,
  },
  contractSizer: {
    alphaSort: false,
    disambiguatePaths: false,
    runOnCompile: true,
    strict: false,
  },
  gasReporter: {
    currency: "USD",
    gasPrice: 50,
    enabled: false,
    coinmarketcap: `${process.env.COINMARKETCAP_KEY}`,
  },
  abiExporter: {
    clear: true,
    flat: true,
    format: "json",
  },
};

export default config;
