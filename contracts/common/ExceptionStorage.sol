// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

contract ExceptionStorage is Ownable {
    mapping(uint32 => string) public exceptions;

    constructor() {}

    function setException(uint32 key, string memory note) public onlyOwner {
        exceptions[key] = note;
    }

    function setExceptions(uint32[] memory keys, string[] memory notes) public onlyOwner {
        require(keys.length == notes.length, "array lengthes must be equal");
        for (uint256 i = 0; i < keys.length; i++) {
            setException(keys[i], notes[i]);
        }
    }

    function getException(uint32 key) public view returns (string memory note) {
        note = exceptions[key];
        if (bytes(note).length == 0) {
            note = "Error with unknown code";
        }
    }

    function throwException(uint32 key) public view {
        revert(getException(key));
    }
}
