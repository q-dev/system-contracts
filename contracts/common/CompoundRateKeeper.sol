// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@dlsl/dev-modules/libs/math/DSMath.sol";

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../common/Globals.sol";

contract CompoundRateKeeper is Initializable {
    struct CompoundRate {
        uint256 rate;
        uint256 lastUpdate;
    }

    CompoundRate public compoundRate;

    address internal _owner;

    /**
     * @notice Restricts only the owner can interact
     */
    modifier onlyOwner() {
        require(msg.sender == _owner, "Unauthorized");
        _;
    }

    constructor() {
        compoundRate.rate = getInitialCompoundRate();
        compoundRate.lastUpdate = block.timestamp;

        _owner = msg.sender;
    }

    function initialize() external initializer {
        compoundRate.rate = getInitialCompoundRate();
        compoundRate.lastUpdate = block.timestamp;

        _owner = msg.sender;
    }

    /**
     * @notice Updates Compound rate
     * @param interestRate_ Current Interest rate
     * @return new Compound rate
     */
    function update(uint256 interestRate_) external onlyOwner returns (uint256) {
        if (block.timestamp <= compoundRate.lastUpdate) return compoundRate.rate;

        uint256 decimal_ = getDecimal();
        uint256 period_ = block.timestamp - compoundRate.lastUpdate;
        uint256 newRate_ = (compoundRate.rate * DSMath.rpow(interestRate_ + decimal_, period_, decimal_)) / decimal_;

        compoundRate.lastUpdate = block.timestamp;
        compoundRate.rate = newRate_;

        return newRate_;
    }

    /**
     * @notice Updates Compound rate using distributable amount
     * @param oldAmount_ old amount: denormalized
     * @param distributable_ available amount: denormalized
     * @return new Compound rate
     */
    function updateWithDistributableAmount(uint256 oldAmount_, uint256 distributable_)
        external
        onlyOwner
        returns (uint256)
    {
        if (distributable_ == 0 || oldAmount_ == 0) return compoundRate.rate;

        uint256 newRate_ = ((oldAmount_ + distributable_) * compoundRate.rate) / oldAmount_;

        if (newRate_ != compoundRate.rate) {
            compoundRate.rate = newRate_;
            compoundRate.lastUpdate = block.timestamp;
        }

        return newRate_;
    }

    /**
     * @notice Returns Compound rate
     * @return Current Compound rate
     */
    function getCurrentRate() external view returns (uint256) {
        return compoundRate.rate;
    }

    /**
     * @notice Returns last update date
     * @return last update date: timestamp
     */
    function getLastUpdate() external view returns (uint256) {
        return compoundRate.lastUpdate;
    }

    /**
     * @notice Transfer ownership to another account
     * @param to_ New owner
     * Redone here instead of inheriting from Ownable
     * in order to keep minimal contract size
     */
    function transferOwnership(address to_) public onlyOwner {
        _owner = to_;
    }

    /**
     * @notice Normalize target amount
     * @param targetAmount_ Denormalized amount to normalize
     * @return Normalized amount
     */
    function normalizeAmount(uint256 targetAmount_) public view returns (uint256) {
        uint256 normalizedAmount_ = (targetAmount_ * getDecimal()) / compoundRate.rate;
        uint256 actualLookAhead_ = denormalizeAmount(normalizedAmount_);

        if (actualLookAhead_ < targetAmount_) {
            // try to compensate for rounding errors
            uint256 normalizedLookAhead_ = normalizedAmount_ + 1;
            actualLookAhead_ = denormalizeAmount(normalizedLookAhead_);

            if (actualLookAhead_ <= targetAmount_) {
                normalizedAmount_ = normalizedLookAhead_;
            }
        }

        return normalizedAmount_;
    }

    /**
     * @notice Denormalize target amount
     * @param normalizedAmount_ Normalized amount to denormalize
     * @return Denormalize amount
     */
    function denormalizeAmount(uint256 normalizedAmount_) public view returns (uint256) {
        return (normalizedAmount_ * compoundRate.rate) / getDecimal();
    }
}
