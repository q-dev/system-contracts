// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

function getDecimal() pure returns (uint256) {
    return 10**27;
}

function getInitialCompoundRate() pure returns (uint256) {
    return 10**18;
}

function getTotalQInExistence(uint256 _blockNumber) pure returns (uint256) {
    return _blockNumber * Q_REWARD_PER_BLOCK + INITIAL_Q_AMOUNT;
}

string constant RKEY__ADDRESS_STORAGE_FACTORY = "common.factory.addressStorage";
string constant RKEY__CR_KEEPER_FACTORY = "common.factory.crKeeper";
string constant RKEY__EPDR_PARAMETERS = "governance.experts.EPDR.parameters";
string constant RKEY__EPRS_PARAMETERS = "governance.experts.EPRS.parameters";
string constant RKEY__EPDR_PARAMETERS_VOTING = "governance.experts.EPDR.parametersVoting";
string constant RKEY__EPRS_PARAMETERS_VOTING = "governance.experts.EPRS.parametersVoting";
string constant RKEY__EPDR_MEMBERSHIP = "governance.experts.EPDR.membership";
string constant RKEY__EPRS_MEMBERSHIP = "governance.experts.EPRS.membership";
string constant RKEY__EPQFI_PARAMETERS = "governance.experts.EPQFI.parameters";
string constant RKEY__EPQFI_MEMBERSHIP = "governance.experts.EPQFI.membership";
string constant RKEY__EPQFI_PARAMETERS_VOTING = "governance.experts.EPQFI.parametersVoting";
string constant RKEY__CONSTITUTION_PARAMETERS = "governance.constitution.parameters";
string constant RKEY__CONSTITUTION_PARAMETERS_VOTING = "governance.constitution.parametersVoting";
string constant RKEY__ROOT_NODES = "governance.rootNodes";
string constant RKEY__ROOT_NODES_MEMBERSHIP_VOTING = "governance.rootNodes.membershipVoting";
string constant RKEY__ROOT_NODES_SLASHING_VOTING = "governance.rootNodes.slashingVoting";
string constant RKEY__ROOT_NODES_SLASHING_ESCROW = "governance.rootNodes.slashingEscrow";
string constant RKEY__VALIDATORS = "governance.validators";
string constant RKEY__VALIDATORS_SLASHING_VOTING = "governance.validators.slashingVoting";
string constant RKEY__VALIDATORS_SLASHING_ESCROW = "governance.validators.slashingEscrow";
string constant RKEY__VOTING_WEIGHT_PROXY = "governance.votingWeightProxy";
string constant RKEY__VALIDATION_REWARDS_POOL = "tokeneconomics.validationRewardPools";
string constant RKEY__PUSH_PAYMENTS = "tokeneconomics.pushPayments";
string constant RKEY__QVAULT = "tokeneconomics.qVault";
string constant RKEY__QHOLDER_REWARD_POOL = "tokeneconomics.qHolderRewardPool";
string constant RKEY__SYSTEM_RESERVE = "tokeneconomics.systemReserve";
string constant RKEY__DEFAULT_ALLOCATION_PROXY = "tokeneconomics.defaultAllocationProxy";
string constant RKEY__ROOT_NODE_REWARD_PROXY = "tokeneconomics.rootNodeRewardProxy";
string constant RKEY__Q_HOLDER_REWARD_PROXY = "tokeneconomics.qHolderRewardProxy";
string constant RKEY__VALIDATION_REWARD_PROXY = "tokeneconomics.validationRewardProxy";
string constant RKEY__WITHDRAW_ADDRESSES = "tokeneconomics.withdrawAddresses";

uint256 constant INITIAL_Q_AMOUNT = 1000000000 ether;
uint256 constant Q_REWARD_PER_BLOCK = 1.5 ether;
