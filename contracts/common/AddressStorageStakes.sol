// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

contract AddressStorageStakes is Ownable {
    struct Data {
        uint256 listIndex;
        uint256 stake;
        uint256 delegatedStake;
    }

    mapping(address => Data) public addrStake;
    address[] public addrList;

    event AddressAdded(address indexed _addr);
    event AddressRemoved(address indexed _addr);
    event StakeIncreased(address indexed _addr, uint256 _stake);
    event StakeDecreased(address indexed _addr, uint256 _stake);
    event DelegatedStakeUpdated(address indexed _addr, uint256 _stake);

    /**
     * @notice increase stake for existed addresses
     *         and to add new one with passed stake
     * @param _addr address whose stake will be increased
     * @param _stake amount of increasing
     */
    function add(address _addr, uint256 _stake) external onlyOwner returns (bool) {
        if (_stake == 0) return false;
        addrStake[_addr].stake += _stake;

        emit StakeIncreased(_addr, _stake);
        _assertListIntegrity(_addr);

        return true;
    }

    /**
     * @notice decreases stake.
     *         address will be removed if stake is zero
     * @param _addr address whose stake will be decreased
     * @param _stake amount of decreasing
     */
    function sub(address _addr, uint256 _stake) external onlyOwner returns (bool) {
        Data storage data = addrStake[_addr];
        require(data.listIndex != 0, "[QEC-036000]-The address does not exist, failed to decrease the stake.");
        require(data.stake >= _stake, "[QEC-036002]-The stake to remove is greater than the available stake.");

        data.stake -= _stake;

        emit StakeDecreased(_addr, _stake);
        _assertListIntegrity(_addr);

        return true;
    }

    /**
     * @notice Set or update delegated stake
     * @param _addr address of the validator
     * @param _delegatedStake new value of total delegated stake
     */
    function setDelegated(address _addr, uint256 _delegatedStake) external onlyOwner returns (bool) {
        if (addrStake[_addr].listIndex == 0 && _delegatedStake == 0) {
            return true; // not in list and nothing to add -> just exist
        }

        addrStake[_addr].delegatedStake = _delegatedStake;
        emit DelegatedStakeUpdated(_addr, _delegatedStake);

        _assertListIntegrity(_addr);

        return true;
    }

    /**
     * @notice Returns count of addresses
     * @return count of addresses
     */
    function size() external view returns (uint256) {
        return addrList.length;
    }

    /**
     * @return array of stored addresses
     */
    function getAddresses() external view returns (address[] memory) {
        return addrList;
    }

    /**
     * @notice checks availability of address
     * @param _addr address to check
     * @return true if address in storage
     */
    function contains(address _addr) public view returns (bool) {
        return addrStake[_addr].stake > 0 || addrStake[_addr].delegatedStake > 0;
    }

    function _addAddr(address _addr) private {
        if (addrStake[_addr].listIndex == 0) {
            addrList.push(_addr);
            emit AddressAdded(_addr);

            addrStake[_addr].listIndex = addrList.length;

            _checkEntry(_addr);
        }
    }

    function _removeAddr(address _addr) private {
        uint256 listIndex = addrStake[_addr].listIndex - 1;
        uint256 lastListIndex = addrList.length - 1;
        if (listIndex != lastListIndex) {
            addrStake[addrList[lastListIndex]].listIndex = listIndex + 1;
            addrList[listIndex] = addrList[lastListIndex];
        }
        addrList.pop();

        delete addrStake[_addr];

        _checkEntry(_addr); //checking removed
        emit AddressRemoved(_addr);
        if (listIndex != lastListIndex) {
            _checkEntry(addrList[listIndex]); // checking swapped
        }
    }

    function _assertListIntegrity(address _addr) private {
        if (!contains(_addr)) {
            _removeAddr(_addr);
        } else {
            _addAddr(_addr);
        }
    }

    function _checkEntry(address _addr) private view {
        uint256 _id = addrStake[_addr].listIndex;
        assert(_id <= addrList.length); //, "Map contains wrong id"); //
        if (contains(_addr)) {
            assert(_id > 0); //, "index start with 1, so included elements must have index > 0");
            assert(addrStake[_addr].stake > 0 || addrStake[_addr].delegatedStake > 0); //, "addr should have stake";
        } else {
            assert(_id == 0); //, "non included elements must have index 0")//;
            // addr should haven't any stake
            assert(addrStake[_addr].stake == 0 && addrStake[_addr].delegatedStake == 0);
        }
    }
}
