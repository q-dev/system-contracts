// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title AddressStorageStakesSorted
 * @dev Priority queue on top of linked list
 * build for validators short list.
 */
contract AddressStorageStakesSorted is Ownable {
    mapping(address => uint256) public addrStake;
    mapping(address => address) public linkedList;

    uint256 public listSize;

    address constant TAIL = address(1);
    address constant HEAD = address(0);

    event AddressAdded(address indexed _addr);
    event AddressRemoved(address indexed _addr);
    event StakeUpdated(address indexed _addr, uint256 _stake);

    modifier shouldNotBeHeadOrTail(address _addr) {
        require(_addr != HEAD && _addr != TAIL, "[QEC-037005]-The address should not be HEAD or TAIL.");
        _;
    }

    constructor() {
        linkedList[TAIL] = HEAD;
    }

    /**
     * @notice Adds a new address with a given stake to the list.
     * @param _addr new address
     * @param _stake given stake
     */
    function addAddr(address _addr, uint256 _stake) public onlyOwner shouldNotBeHeadOrTail(_addr) returns (bool) {
        require(
            !contains(_addr),
            "[QEC-037000]-The address has already been added to the storage, "
            "failed to add to the address stakes sorted storage."
        );
        require(_stake > 0, "[QEC-037001]-Invalid stake, failed to add the address to the storage.");

        address _prev = getPrevByStake(_stake);
        bool _isOnTop = linkedList[_prev] == HEAD;
        if (!_isOnTop) {
            linkedList[_addr] = linkedList[_prev];
        }

        linkedList[_prev] = _addr;
        addrStake[_addr] = _stake;
        listSize++;

        emit AddressAdded(_addr);
        _checkEntry(_addr);
        return true;
    }

    /**
     * @notice Updates stake of addr and puts it into a new position
     *         or removes if _stake is zero.
     * @param _addr whose stake is updated
     * @param _stake new stake amount
     */
    function updateStake(address _addr, uint256 _stake) public onlyOwner returns (bool) {
        require(contains(_addr), "[QEC-037002]-The address is not in the storage, failed to update the stake.");

        // remove and re-insert at new position
        linkedList[getPrevByAddr(_addr)] = linkedList[_addr];
        delete linkedList[_addr];

        if (_stake == 0) {
            listSize--;
            delete addrStake[_addr];

            emit AddressRemoved(_addr);
            return true;
        }

        addrStake[_addr] = _stake;
        emit StakeUpdated(_addr, _stake);

        address _prev = getPrevByStake(_stake);
        bool _isOnTop = linkedList[_prev] == HEAD;
        if (!_isOnTop) {
            linkedList[_addr] = linkedList[_prev];
        }
        linkedList[_prev] = _addr;
        _checkEntry(_addr);
        return true;
    }

    /**
     * @notice Removes address with the lowest stake.
     */
    function removeLast() public onlyOwner returns (address) {
        require(listSize > 0, "[QEC-037003]-The list is empty, failed to remove the address.");
        address last = linkedList[TAIL];
        linkedList[TAIL] = linkedList[last];
        delete linkedList[last];
        delete addrStake[last];
        listSize--;
        emit AddressRemoved(last);
        _checkEntry(last);
        return last;
    }

    /**
     * @notice checks availability of address
     * @param _addr address to check
     * @return true if address in storage
     */
    function contains(address _addr) public view returns (bool) {
        return addrStake[_addr] > 0;
    }

    /**
     * @notice returns addresses sorted by stake desc.
     */
    function getAddresses() public view returns (address[] memory) {
        address currentAddr = TAIL;
        address[] memory result = new address[](listSize);
        for (uint256 i = listSize; i > 0; i--) {
            currentAddr = linkedList[currentAddr];
            result[i - 1] = currentAddr;
        }

        return result;
    }

    function getPrevByStake(uint256 _stake) private view returns (address) {
        address _addr = TAIL;
        while (addrStake[linkedList[_addr]] < _stake && linkedList[_addr] != HEAD) {
            _addr = linkedList[_addr];
        }

        return _addr;
    }

    function getPrevByAddr(address _addr) private view returns (address) {
        require(contains(_addr), "[QEC-037004]-The address is not in the storage, failed to get the previous address.");
        address _current = TAIL;
        while (linkedList[_current] != _addr) {
            _current = linkedList[_current];
        }

        return _current;
    }

    function _checkEntry(address _addr) private view {
        if (contains(_addr)) {
            assert(addrStake[_addr] > 0);
        } else {
            assert(addrStake[_addr] == 0);
        }
        address next = linkedList[_addr];
        if (next != HEAD) {
            assert(addrStake[next] >= addrStake[_addr]);
        }
    }
}
