// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/proxy/ERC1967/ERC1967Proxy.sol";

import "./CompoundRateKeeper.sol";

/**
 * @title CompoundRateFactory.
 * @dev Use this in order to reduce contract size.
 */
contract CompoundRateKeeperFactory is Initializable {
    address implementation;

    constructor() {}

    function initialize(address _impl) external initializer {
        implementation = _impl;
    }

    /**
     * @notice Create new Compound rate keeper
     * @return Compound rate keeper
     */
    function create() public returns (CompoundRateKeeper) {
        CompoundRateKeeper t = CompoundRateKeeper(address(new ERC1967Proxy(implementation, "")));
        t.initialize();
        t.transferOwnership(msg.sender);
        return t;
    }
}
