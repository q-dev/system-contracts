// SPDX-License-Identifier: LGPL-3.0-or-later
// solium-disable linebreak-style
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/proxy/ERC1967/ERC1967Proxy.sol";

import "./AddressStorage.sol";

/*
 * @notice Contract for deploying AddressStorage
 */
contract AddressStorageFactory is Initializable {
    address implementation;

    function initialize(address _impl) external virtual initializer {
        implementation = _impl;
    }

    /*
     * @notice method gor creating AddressStorage
     *          msg.sender is owner of storage
     * @param _addrList  initing array;
     */
    function create(address[] memory _addrList) external returns (AddressStorage) {
        AddressStorage s = AddressStorage(address(new ERC1967Proxy(implementation, "")));
        s.initialize(_addrList);
        s.transferOwnership(msg.sender);
        return s;
    }
}
