// SPDX-License-Identifier: LGPL-3.0-or-later
// solium-disable linebreak-style
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

contract StringStorage is Ownable {
    struct Index {
        uint256 id;
    }

    mapping(string => Index) internal map;
    string[] internal strings;

    constructor(string[] memory _strings) {
        for (uint256 i = 0; i < _strings.length; i++) {
            strings.push(_strings[i]);
            map[_strings[i]].id = strings.length;
        }
    }

    /**
     * @notice remove string if it is in storage, reverts if string is not in storage
     * @param _string string to remove
     */
    function mustRemove(string calldata _string) external onlyOwner {
        require(remove(_string), "[QEC-038000]-Failed to remove the string from the string storage.");
    }

    /**
     * @notice Returns count of strings
     * @return count of strings
     */
    function size() external view returns (uint256) {
        return uint256(strings.length);
    }

    /**
     * @return array of stored strings
     */
    function getStrings() external view returns (string[] memory) {
        return strings;
    }

    /**
     * @notice add string if it is in storage, reverts if string in storage
     * @param _string string to add
     */
    function mustAdd(string calldata _string) public onlyOwner {
        require(
            add(_string),
            "[QEC-038001]-The string has already been added to the storage, "
            "failed to add the string to the string storage."
        );
    }

    /**
     * @notice add string if it is not in storage
     * @param _string string to add
     * @return true if string was added
     */
    function add(string calldata _string) public onlyOwner returns (bool) {
        if (contains(_string)) {
            return false;
        }
        _add(_string);
        return true;
    }

    /**
     * @notice remove string if it is in storage
     * @param _string string to remove
     * @return true if string was removed
     */
    function remove(string calldata _string) public onlyOwner returns (bool) {
        if (!contains(_string)) {
            return false;
        }
        _remove(_string);
        return true;
    }

    /**
     * @notice remove all strings
     * @return true if all strings were removed
     */
    function clear() public onlyOwner returns (bool) {
        for (uint256 i = 0; i < strings.length; i++) {
            delete map[strings[i]];
        }
        delete strings;
        return true;
    }

    /**
     * @notice checks availability of string
     * @param _string string to check
     * @return true if string in storage
     */
    function contains(string memory _string) public view returns (bool) {
        return map[_string].id > 0;
    }

    function _add(string calldata _string) private {
        strings.push(_string);
        map[_string].id = strings.length;

        _checkEntry(_string);
    }

    function _remove(string memory _string) private {
        Index memory index = map[_string];

        // Move an last element of array into the vacated key slot.
        uint256 _lastListID = strings.length - 1;
        string memory _lastListString = strings[_lastListID];
        if (_lastListID != index.id - 1) {
            map[_lastListString].id = index.id;
            strings[index.id - 1] = _lastListString;
        }
        strings.pop();
        delete map[_string];

        _checkEntry(_string);

        if (!_equals(_lastListString, _string)) _checkEntry(_lastListString);
    }

    function _checkEntry(string memory _string) private view {
        uint256 _id = map[_string].id;
        assert(_id <= strings.length); // "Map contains wrong id"
        if (contains(_string))
            assert(_id > 0); // "index start with 1, so included elements must have index > 0"
        else assert(_id == 0); // non included elements must have index 0
    }

    function _equals(string memory _firstString, string memory _secondString) private pure returns (bool) {
        return (keccak256(abi.encodePacked((_firstString))) == keccak256(abi.encodePacked((_secondString))));
    }
}
