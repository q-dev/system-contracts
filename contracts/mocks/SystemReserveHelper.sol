// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../tokeneconomics/SystemReserve.sol";
import "../interfaces/IContractRegistry.sol";

contract SystemReserveHelper {
    IContractRegistry registry;

    constructor() {}

    receive() external payable {}

    function initialize(address _registry) external {
        registry = IContractRegistry(_registry);
    }

    function transfer(uint256 _amount) external returns (bool) {
        require(
            SystemReserve(payable(registry.mustGetAddress(RKEY__SYSTEM_RESERVE))).withdraw(_amount),
            "Failed to request the withdrawal."
        );

        (bool _success, ) = payable(registry.mustGetAddress(RKEY__DEFAULT_ALLOCATION_PROXY)).call{
            value: address(this).balance
        }("");
        require(_success, "Failed to transfer withdrawal amount, failed to request the withdrawal.");
        return true;
    }
}
