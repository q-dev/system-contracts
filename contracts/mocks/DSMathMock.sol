// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity ^0.8.0;

import "@dlsl/dev-modules/libs/math/DSMath.sol";

contract DSMathMock {
    function rpow(
        uint256 x,
        uint256 n,
        uint256 b
    ) external pure returns (uint256) {
        return DSMath.rpow(x, n, b);
    }
}
