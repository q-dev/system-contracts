// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../tokeneconomics/PushPayments.sol";

contract GreedyReceiverMock {
    bool greedMode;

    event Recieved(uint256 value);

    receive() external payable {
        uint256 t;
        if (greedMode) {
            for (uint256 i = 0; i < 3000; i++) {
                t += 1230;
            }
        }
        emit Recieved(msg.value);
    }

    function changeMode(bool setingValue) external {
        greedMode = setingValue;
    }

    // withdraws some q from _address
    function doWithdraw(address _withdrawble) external {
        PushPayments(_withdrawble).withdraw();
    }
}
