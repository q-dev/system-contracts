// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../../defi/token/StableCoin.sol";

contract StableCoinMock is StableCoin {
    constructor(
        address _registry,
        string memory _name,
        string memory _symbol,
        string[] memory _eligibleContracts,
        address forwarder
    ) StableCoin(_registry, _name, _symbol, _eligibleContracts, forwarder) {
        _mint(_msgSender(), 10000000000000000000000);
    }
}
