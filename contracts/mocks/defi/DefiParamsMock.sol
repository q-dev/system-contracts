// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity ^0.8.0;

import "../../defi/DefiParams.sol";

contract DefiParamsMock {
    function concatenate(
        string memory col_,
        string memory stc_,
        string memory ending_
    ) external pure returns (string memory) {
        return DefiParams.concatenate(col_, stc_, ending_);
    }

    function concatenate(string memory param_, string memory ending_) external pure returns (string memory) {
        return DefiParams.concatenate(param_, ending_);
    }

    function concatenate(string memory ending_) external pure returns (string memory) {
        return DefiParams.concatenate(ending_);
    }

    function stableCoinAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.stableCoinAddress(stc_);
    }

    function liquidationAuctionAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.liquidationAuctionAddress(stc_);
    }

    function systemBalanceAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.systemBalanceAddress(stc_);
    }

    function borrowingAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.borrowingAddress(stc_);
    }

    function savingAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.savingAddress(stc_);
    }

    function compoundRateKeeperAddress(string memory stc_) external pure returns (string memory) {
        return DefiParams.compoundRateKeeperAddress(stc_);
    }

    function oracle(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.oracle(col_, stc_);
    }

    function ceiling(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.ceiling(col_, stc_);
    }

    function step(string memory stc_) external pure returns (string memory) {
        return DefiParams.step(stc_);
    }

    function collateralizationRatio(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.collateralizationRatio(col_, stc_);
    }

    function liquidationRatio(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.liquidationRatio(col_, stc_);
    }

    function liquidationFee(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.liquidationFee(col_, stc_);
    }

    function interestRate(string memory col_, string memory stc_) external pure returns (string memory) {
        return DefiParams.interestRate(col_, stc_);
    }

    function systemSurplusAuction(string memory stc_) external pure returns (string memory) {
        return DefiParams.systemSurplusAuction(stc_);
    }

    function systemDebtAuction(string memory stc_) external pure returns (string memory) {
        return DefiParams.systemDebtAuction(stc_);
    }

    function surplusThreshold(string memory stc_) external pure returns (string memory) {
        return DefiParams.surplusThreshold(stc_);
    }

    function surplusLot(string memory stc_) external pure returns (string memory) {
        return DefiParams.surplusLot(stc_);
    }

    function savingRate(string memory stc_) external pure returns (string memory) {
        return DefiParams.savingRate(stc_);
    }

    function debtThreshold(string memory stc_) external pure returns (string memory) {
        return DefiParams.debtThreshold(stc_);
    }

    function contractAddress(string memory col_) external pure returns (string memory) {
        return DefiParams.contractAddress(col_);
    }
}
