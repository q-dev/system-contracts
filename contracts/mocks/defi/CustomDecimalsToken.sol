// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title CustomDecimalsCoin
 *
 * @dev Example of erc20 with custom decimals for testing in DeFi.
 * Warning! Only for testing.
 */
contract CustomDecimalsCoin is ERC20 {
    uint8 internal _decimals;

    constructor(
        string memory name_,
        string memory symbol_,
        uint8 decimals_
    ) ERC20(name_, symbol_) {
        _decimals = decimals_;
        _mint(msg.sender, 10000000000000000000000000000000);
    }

    function mint(address recipient_, uint256 amount_) external returns (bool) {
        _mint(recipient_, amount_);

        return true;
    }

    function decimals() public view override returns (uint8) {
        return _decimals;
    }
}
