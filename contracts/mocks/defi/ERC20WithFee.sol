// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title ERC20WithFee
 *
 * Fees are calculated in basis points (1/100 of a percent).
 * Ref: https://www.investopedia.com/terms/b/basispoint.asp
 */
contract ERC20WithFee is ERC20 {
    uint256 public constant ONE_HUNDRED = 10000;

    uint256 private _fee;
    uint8 private _decimals;

    constructor(
        string memory name_,
        string memory symbol_,
        uint8 decimals_,
        uint256 fee_
    ) ERC20(name_, symbol_) {
        _fee = fee_;
        _decimals = decimals_;

        _mint(msg.sender, 10000000000000000000000000000000);
    }

    function mint(address to_, uint256 amount_) public {
        _mint(to_, amount_);
    }

    function burn(address to_, uint256 amount_) public {
        _burn(to_, amount_);
    }

    function decimals() public view override returns (uint8) {
        return _decimals;
    }

    function _transfer(
        address sender_,
        address recipient_,
        uint256 amount_
    ) internal override {
        uint256 feeAmount_ = _getFeeAmount(amount_);
        uint256 amountAfterFee_ = amount_ - feeAmount_;

        super._transfer(sender_, recipient_, amountAfterFee_);
        super._transfer(sender_, address(this), feeAmount_);
    }

    function _getFeeAmount(uint256 amount_) internal view returns (uint256) {
        return (amount_ * _fee) / ONE_HUNDRED;
    }
}
