// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../../defi/SystemBalance.sol";

contract SystemBalanceMock is SystemBalance {
    constructor() {}

    function setDebt(uint256 debt_) external {
        _debt = debt_;
    }
}
