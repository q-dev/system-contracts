// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../../defi/token/IERC677.sol";

contract ERC677TransferReceiverMock is IERC677TransferReceiver {
    bytes internal _lastData;

    event TokenFallback(address from, uint256 amount, bytes data);

    function tokenFallback(
        address from_,
        uint256 amount_,
        bytes calldata data_
    ) external override returns (bool) {
        _lastData = data_;

        emit TokenFallback(from_, amount_, data_);

        return true;
    }

    function getLastData() public view returns (bytes memory) {
        return _lastData;
    }
}
