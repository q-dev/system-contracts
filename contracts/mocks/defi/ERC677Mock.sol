// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

import "../../defi/token/ERC677.sol";

contract ERC677Mock is ERC677, ERC20Burnable {
    constructor(string memory name_, string memory symbol_) ERC677(name_, symbol_) {
        _mint(msg.sender, 10000000000000000000000000000000);
    }
}
