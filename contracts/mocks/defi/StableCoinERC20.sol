// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

/**
 * @title ERC20
 *
 * Regular ERC20 token with additional features:
 * - minting and burning
 * - total supply cap
 */
contract StableCoinERC20 is ERC20Upgradeable, OwnableUpgradeable {
    uint256 public totalSupplyCap;

    uint8 internal _decimals;

    function __StableCoinERC20_init(
        string memory name_,
        string memory symbol_,
        uint8 decimals_,
        uint256 totalSupplyCap_
    ) external initializer {
        __Ownable_init();
        __ERC20_init(name_, symbol_);

        _decimals = decimals_;

        totalSupplyCap = totalSupplyCap_;
    }

    function mintTo(address account_, uint256 amount_) external onlyOwner {
        require(
            totalSupplyCap == 0 || totalSupply() + amount_ <= totalSupplyCap,
            "ERC20: The total supply capacity exceeded, minting is not allowed."
        );

        _mint(account_, amount_);
    }

    function burnFrom(address account_, uint256 amount_) external {
        if (account_ != msg.sender) {
            _spendAllowance(account_, msg.sender, amount_);
        }

        _burn(account_, amount_);
    }

    function decimals() public view override returns (uint8) {
        return _decimals;
    }

    function _spendAllowance(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual {
        uint256 currentAllowance = allowance(owner, spender);
        if (currentAllowance != type(uint256).max) {
            require(currentAllowance >= amount, "ERC20: insufficient allowance");
            unchecked {
                _approve(owner, spender, currentAllowance - amount);
            }
        }
    }
}
