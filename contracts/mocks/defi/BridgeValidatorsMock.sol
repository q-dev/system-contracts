// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../../defi/IBridgeValidators.sol";

contract BridgeValidatorsMock is IBridgeValidators {
    mapping(address => uint256) indexMap;

    address[] public validators;
    uint256 public override requiredSignatures = 3;

    event ValidatorAdded(address indexed validator);
    event ValidatorRemoved(address indexed validator);

    function setValidators(address[] memory newAddresses_) external {
        for (uint256 i; i < validators.length; i++) {
            delete indexMap[validators[i]];
        }

        validators = newAddresses_;

        for (uint256 i; i < validators.length; i++) {
            indexMap[validators[i]] = i + 1;
        }
    }

    function addValidator(address validator_) external override {
        require(!isValidator(validator_), "BridgeValidatorsMock: address is already validator");

        validators.push(validator_);
        indexMap[validator_] = validators.length;

        emit ValidatorAdded(validator_);
    }

    function removeValidator(address validator_) external override {
        require(isValidator(validator_), "BridgeValidatorsMock: address not validator");
        require(validators.length > requiredSignatures, "BridgeValidatorsMock: min count of validators reached");

        address last_ = validators[validators.length - 1];
        uint256 validatorIndex_ = indexMap[validator_];

        validators[validatorIndex_ - 1] = last_;
        indexMap[last_] = validatorIndex_;

        delete indexMap[validator_];
        validators.pop();

        emit ValidatorRemoved(validator_);
    }

    function validatorList() external view override returns (address[] memory) {
        return validators;
    }

    function isValidator(address validator_) public view override returns (bool) {
        return indexMap[validator_] > 0;
    }

    function validatorCount() public view override returns (uint256) {
        return validators.length;
    }
}
