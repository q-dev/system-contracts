// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "../../defi/oracles/FxPriceFeedSM.sol";

contract FxPriceFeedSMMock is FxPriceFeedSM {
    uint8 internal _decimals;

    constructor(address _priceFeed) FxPriceFeedSM(_priceFeed) {}

    function getCurrentEra() external view returns (uint256) {
        return _getPrevEra(block.timestamp);
    }

    function getPrevEra(uint256 _timestamp) external pure returns (uint256) {
        return _getPrevEra(_timestamp);
    }
}
