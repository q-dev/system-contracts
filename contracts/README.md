
# Q System Contracts


## Installing 

```sh
$ npm i @q-dev/contracts
```

## Usage

```solidity
pragma solidity 0.8.9;

import "@q-dev/contracts/ContractRegistry.sol";

contract Example{
    address reg = 0xc3E589056Ece16BCB88c6f9318e9a7343b663522;
    function getQUSD() public view returns (address _addr){
        _addr = ContractRegistry(reg).getAddress("defi.QUSD.coin");
    }
}
```

## Documentation

There you may find usage examples and descriptions of all methods:

https://q-dev.gitlab.io/system-contracts/

## License
LGPL 3.0: https://gitlab.com/q-dev/system-contracts/-/blob/master/LICENSE
