// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IUpgradable.sol";
import "./common/AddressStorage.sol";
import "./common/StringStorage.sol";
import "./interfaces/IContractRegistry.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

contract ContractRegistry is Initializable, IContractRegistry {
    mapping(string => address) private registry;
    StringStorage private keyStorage;
    AddressStorage private maintainers;

    modifier _onlyMaintainer() {
        require(maintainers.contains(msg.sender), "[QEC-034004]-Permission denied - only maintainers have access.");
        _;
    }

    modifier _shouldHaveSameSize(uint256 _firstSize, uint256 _secondSize) {
        require(
            _firstSize == _secondSize,
            "[QEC-034001]-The number of keys and addresses should be the same, failed to set addresses."
        );
        _;
    }

    function initialize(
        address[] memory _maintainersList,
        string[] memory _keys,
        address[] memory _addresses
    ) external initializer _shouldHaveSameSize(_keys.length, _addresses.length) {
        maintainers = new AddressStorage();
        maintainers.initialize(_maintainersList);
        keyStorage = new StringStorage(_keys);
        for (uint256 i = 0; i < _addresses.length; i++) {
            registry[_keys[i]] = _addresses[i];
        }
    }

    function upgradeContract(address _proxy, address _newImplementation) external _onlyMaintainer {
        IUpgradable(_proxy).upgradeTo(_newImplementation);
    }

    function getImplementation(address _proxy) external _onlyMaintainer returns (address) {
        return IUpgradable(_proxy).implementation();
    }

    function setAddress(string calldata _key, address _addr) external _onlyMaintainer {
        _setAddress(_key, _addr);
    }

    function setMaintainer(address _maintainer) external _onlyMaintainer {
        maintainers.mustAdd(_maintainer);
    }

    function leaveMaintainers() external {
        if (maintainers.contains(msg.sender))
            require(
                maintainers.size() > 1,
                "[QEC-034002]-Cannot leave the maintainers list, you are the only maintainer."
            );
        maintainers.mustRemove(msg.sender);
    }

    function removeKey(string calldata _key) external _onlyMaintainer {
        _removeKey(_key);
    }

    function removeKeys(string[] memory _keys) external _onlyMaintainer {
        for (uint256 i = 0; i < _keys.length; i++) {
            _removeKey(_keys[i]);
        }
    }

    // getAddress is used by Q-client (do not remove it, Viktor!!!)
    function getAddress(string calldata _key) external view returns (address) {
        return registry[_key];
    }

    function mustGetAddress(string calldata _key) external view returns (address) {
        address addr = registry[_key];
        require(
            addr != address(0),
            string(abi.encodePacked("[QEC-034003]-The ", _key, " key does not exist, failed to get the address."))
        );
        return addr;
    }

    function getContracts() external view returns (Pair[] memory) {
        string[] memory _keys = keyStorage.getStrings();
        Pair[] memory ctrs = new Pair[](_keys.length);
        for (uint8 i = 0; i < _keys.length; i++) {
            ctrs[i] = Pair(_keys[i], registry[_keys[i]]);
        }

        return ctrs;
    }

    /**
     * @notice gets a list of contract registry maintainers
     * @return address [] array maintainers` addresses
     */
    function getMaintainers() external view returns (address[] memory) {
        return maintainers.getAddresses();
    }

    function setAddresses(string[] memory _keys, address[] memory _addresses)
        public
        _onlyMaintainer
        _shouldHaveSameSize(_keys.length, _addresses.length)
    {
        for (uint256 i = 0; i < _keys.length; ++i) {
            _setAddress(_keys[i], _addresses[i]);
        }
    }

    function contains(string memory _key) public view returns (bool) {
        return registry[_key] != address(0);
    }

    function _setAddress(string memory _key, address _addr) private {
        require(_addr != address(0), "[QEC-034000]-Invalid address value, failed to set the address.");

        if (!contains(_key)) {
            keyStorage.add(_key);
        }

        registry[_key] = _addr;
    }

    function _removeKey(string memory _key) private {
        if (contains(_key)) {
            keyStorage.remove(_key);
            registry[_key] = address(0);
        }
    }
}
