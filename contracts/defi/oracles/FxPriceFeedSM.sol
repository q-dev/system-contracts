// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/Pausable.sol";

import "./IFxPriceFeedSM.sol";

/**
 * @title FxPriceFeedSM contract
 * @notice This contract implements the price feed security module, which implements interface IFxPriceFeed,
 * where the exchangeRate function it returns a delayed price.
 */
contract FxPriceFeedSM is IFxPriceFeedSM, Ownable, Pausable {
    // Update period interval in seconds (4096 seconds)
    uint256 public constant UPDATE_PERIOD = 1 hours + 8 minutes + 16 seconds;

    // Address of the price feed contract
    IFxPriceFeed public priceFeed;

    // Time of the last update
    uint256 internal _lastUpdate;

    // Current exchange rate value
    uint256 internal _currentExchangeRate;

    // Next exchange rate value
    uint256 internal _nextExchangeRate;

    // Flag for price validity (initially false)
    bool internal _priceValidFlag;

    /**
     * @notice event to notify about update of the price feed contract
     * @param newPriceFeedAddress an address of the new price feed contract
     */
    event PriceFeedUpdated(address newPriceFeedAddress);

    /**
     * @notice event to notify about update of the price validity flag
     * @param newValue a value of the new price validity flag
     */
    event PriceValidFlagUpdated(bool newValue);

    /**
     * @notice event to notify about update of the current exchange rate value
     * @param newExchangeRate a value of the new exchange rate
     * @param nextExchangeRate a value of the next exchange rate
     * @param currentEraTs a current era timestamp value
     */
    event ExchangeRateUpdated(uint256 newExchangeRate, uint256 nextExchangeRate, uint256 currentEraTs);

    constructor(address priceFeed_) {
        require(priceFeed_ != address(0), "[QEC-043000]-Zero price feed address.");

        priceFeed = IFxPriceFeed(priceFeed_);
    }

    /**
     * @notice Function to update the contract address of the price feed
     * @dev Only contract owner can call this function
     * @param newPriceFeedAddr_ an address of the new price feed contract
     */
    function updatePriceFeed(address newPriceFeedAddr_) external onlyOwner {
        require(newPriceFeedAddr_ != address(0), "[QEC-043001]-Zero new price feed address.");

        priceFeed = IFxPriceFeed(newPriceFeedAddr_);

        emit PriceFeedUpdated(newPriceFeedAddr_);
    }

    /**
     * @notice Function to update the price validity flag value
     * @dev Only contract owner can call this function
     * @param newValue_ a new value of the price validity flag
     */
    function setPriceValidFlag(bool newValue_) external onlyOwner {
        require(_priceValidFlag != newValue_, "[QEC-043002]-Incorrect new value.");

        _priceValidFlag = newValue_;

        emit PriceValidFlagUpdated(newValue_);
    }

    /**
     * @notice Function for pausing price updates
     * @dev Only contract owner can call this function
     */
    function pause() external onlyOwner {
        _pause();
    }

    /**
     * @notice Function for unpausing price updates
     * @dev Only contract owner can call this function
     */
    function unpause() external onlyOwner {
        _unpause();
    }

    /**
     * @notice Function to update the current exchange rate
     */
    function updateExchangeRate() external whenNotPaused {
        require(_getPrevEra(block.timestamp) > _lastUpdate, "[QEC-043003]-Time for the update has not been reached.");

        _lastUpdate = block.timestamp;

        if (_priceValidFlag) {
            _currentExchangeRate = _nextExchangeRate;
        }

        _nextExchangeRate = priceFeed.exchangeRate();

        emit ExchangeRateUpdated(_currentExchangeRate, _nextExchangeRate, _lastUpdate);
    }

    /**
     * @inheritdoc IFxPriceFeed
     */
    function pair() external view override returns (string memory) {
        return priceFeed.pair();
    }

    /**
     * @inheritdoc IFxPriceFeed
     */
    function baseTokenAddr() external view override returns (address) {
        return priceFeed.baseTokenAddr();
    }

    /**
     * @inheritdoc IFxPriceFeed
     */
    function decimalPlaces() external view override returns (uint256) {
        return priceFeed.decimalPlaces();
    }

    /**
     * @inheritdoc IFxPriceFeed
     */
    function updateTime() external view override returns (uint256) {
        return _lastUpdate;
    }

    /**
     * @notice Function to get the delayed exchange rate
     * @return delayed exchange rate
     */
    function exchangeRate() external view override returns (uint256) {
        return _currentExchangeRate;
    }

    /**
     * @inheritdoc IFxPriceFeedSM
     */
    function nextExchangeRate() external view returns (uint256) {
        return _nextExchangeRate;
    }

    /**
     * @inheritdoc IFxPriceFeedSM
     */
    function isPriceValid() external view override returns (bool) {
        return _priceValidFlag;
    }

    function _getPrevEra(uint256 _timestamp) internal pure returns (uint256) {
        return _timestamp & ~(UPDATE_PERIOD - 1);
    }
}
