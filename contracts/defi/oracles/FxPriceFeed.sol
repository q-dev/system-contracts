// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

import "./IFxPriceFeed.sol";

import "../../common/AddressStorage.sol";

/**
 * @title FxPriceFeed
 * @notice Gets the current price of currencies using exchange rate data.
 * Allows only designated maintainers to update the exchange rate.
 */
contract FxPriceFeed is IMutableFxPriceFeed, Ownable {
    AddressStorage private _maintainers;

    // The name of the currency pair.
    string public override pair;

    // The address of the base token.
    address public immutable override baseTokenAddr;

    // The address of the quote token.
    address public immutable quoteTokenAddr;

    // The number of decimal places in the exchange rate.
    uint256 public immutable override decimalPlaces;

    // The last time the exchange rate was changed.
    uint256 public override updateTime;

    // The last time the exchange rate was priced.
    uint256 public pricingTime;

    // The exchange rate, representing the number of wei of quoteToken in exchange for 1 full baseToken.
    uint256 public override exchangeRate;

    // The maximum time tolerance for future pricingTime.
    uint256 public futurePricingTolerance;

    /**
     * @notice Restricts access to only maintainers.
     */
    modifier onlyMaintainer() {
        require(_maintainers.contains(msg.sender), "[QEC-019000]-Permission denied - only maintainers have access.");
        _;
    }

    constructor(
        string memory pair_,
        uint256 decimal_,
        address[] memory maintainersList_,
        address baseTokenAddr_,
        address quoteTokenAddr_
    ) {
        require(bytes(pair_).length != 0, "[QEC-019001]-Invalid pair, the initialization failed.");
        require(decimal_ > 0, "[QEC-019002]-Invalid decimal, the initialization failed.");
        require(baseTokenAddr_ != address(0), "[QEC-019003]-Invalid base token address, the initialization failed.");
        require(quoteTokenAddr_ != address(0), "[QEC-019004]-Invalid quote token address, the initialization failed.");
        require(
            _isContract(baseTokenAddr_),
            "[QEC-019005]-The base token address is not a contract, the initialization failed."
        );
        require(
            _isContract(quoteTokenAddr_),
            "[QEC-019006]-The quote token address is not a contract, the initialization failed."
        );

        _maintainers = new AddressStorage();
        _maintainers.initialize(maintainersList_);

        pair = pair_;
        decimalPlaces = decimal_;
        baseTokenAddr = baseTokenAddr_;
        quoteTokenAddr = quoteTokenAddr_;
        futurePricingTolerance = 30 minutes;
    }

    /**
     * @notice Adds a new maintainer address to the list of maintainers.
     * @param maintainer_ address to be added as a maintainer.
     */
    function setMaintainer(address maintainer_) external onlyMaintainer {
        _maintainers.mustAdd(maintainer_);
    }

    /**
     * @notice Allows a maintainer to remove themselves from the maintainers list.
     */
    function leaveMaintainers() external {
        if (_maintainers.contains(msg.sender)) {
            require(
                _maintainers.size() > 1,
                "[QEC-019008]-Cannot leave the maintainers list, you are the only maintainer."
            );
        }

        _maintainers.mustRemove(msg.sender);
    }

    /**
     * @inheritdoc IMutableFxPriceFeed
     */
    function setExchangeRate(uint256 exchangeRate_, uint256 pricingTime_)
        external
        override
        onlyMaintainer
        returns (bool)
    {
        require(exchangeRate_ > 0, "[QEC-019007]-Invalid exchange rate, failed to set the exchange rate.");
        require(pricingTime_ > pricingTime, "[QEC-019009]-Pricing time is less or equal current.");
        require(pricingTime_ <= block.timestamp + futurePricingTolerance, "[QEC-019010]-Pricing time in future.");

        exchangeRate = exchangeRate_;
        updateTime = block.timestamp;
        pricingTime = pricingTime_;

        return true;
    }

    /**
     * @notice Sets the maximum time tolerance for future pricingTime.
     * @param tolerance_ new future pricing tolerance
     */
    function setFuturePricingTolerance(uint256 tolerance_) external onlyMaintainer {
        futurePricingTolerance = tolerance_;
    }

    /**
     * @notice Returns the list of maintainer addresses.
     */
    function getMaintainers() external view returns (address[] memory) {
        return _maintainers.getAddresses();
    }

    /**
     * @notice Checks if the given address is a contract.
     * @param address_ address to be checked.
     */
    function _isContract(address address_) private view returns (bool) {
        uint32 size_;
        assembly {
            size_ := extcodesize(address_)
        }

        return size_ > 0;
    }
}
