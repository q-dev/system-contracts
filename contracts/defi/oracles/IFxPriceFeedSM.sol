// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IFxPriceFeed.sol";

/**
 * @title FxPriceFeedSM interface
 * @notice This interface is for the price feed security module, which implements interface IFxPriceFeed,
 * but in the exchangeRate function it returns a delayed price
 */
interface IFxPriceFeedSM is IFxPriceFeed {
    /**
     * @notice Function to get the next exchange rate value
     * @return next exchange rate value
     */
    function nextExchangeRate() external view returns (uint256);

    /**
     * @notice Function to get information about whether the exchange rate is valid or not
     * @return true if exchange rate valid, false - otherwise
     */
    function isPriceValid() external view returns (bool);
}
