// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

import "./IFxPriceFeed.sol";

import "../../common/Median.sol";

/**
 * @title FxPriceFeedMedianizer
 * @notice This contract collects price data from multiple subfeeds, processes it, and provides a medianized price.
 * The use of multiple subfeeds and medianization reduces the impact of outlier values and potential manipulation.
 * It adheres to the IFxPriceFeed interface and includes ownership capabilities provided by the Ownable contract.
 */
contract FxPriceFeedMedianizer is IFxPriceFeed, Ownable {
    using EnumerableSet for EnumerableSet.AddressSet;

    /**
     * @notice Represents a round of price feed data collection.
     * Used to store aggregated data from various price subfeeds for each round.
     *
     * @param rates Array of price responses from the subfeeds.
     * @param roundRate The finalized, aggregated price for the round. It's set to zero if no price was determined for the round.
     * @param startedAt Timestamp indicating when this round began.
     * @param submitted Mapping to track which subfeeds have submitted their price for the current round.
     */
    struct RoundInfo {
        int256[] rates;
        uint256 roundRate;
        uint256 startedAt;
        mapping(address => bool) submitted;
    }

    /// @notice Name of the currency pair being represented, e.g., "QBTC_QUSD".
    string public override pair;

    /// @notice Address of the base token for which the price is being fetched.
    address public immutable override baseTokenAddr;

    /// @notice Number of decimal places used in the exchange rate.
    uint256 public immutable override decimalPlaces;

    /// @notice Timestamp of the last time the exchange rate was updated.
    uint256 public override updateTime;

    /// @notice Represents the price of one unit of the base token in terms of the quote currency.
    uint256 public override exchangeRate;

    /// @notice Identifier for the current round of data aggregation.
    uint256 public roundId;

    /// @notice Timestamp indicating when the current round began.
    uint256 public roundStarted;

    /// @notice Maximum duration (in seconds) a round can be active.
    uint256 public roundTime;

    /// @notice Minimum number of valid submissions required to calculate and accept a new price.
    uint256 public minSubmissionsCount;

    /// @notice Maximum number of submissions allowed in one round. Once reached, the round concludes.
    uint256 public maxSubmissionsCount;

    /// @notice Minimum allowable value a subfeed can submit as a price.
    int256 public minRateValue;

    /// @notice Indicates whether a round is currently active or not.
    bool public isOpenRound;

    /// @dev Internal set of addresses representing the subfeeds contributing to the price data.
    EnumerableSet.AddressSet internal _subFeeds;

    /// @dev Mapping that stores information about each round of data aggregation.
    mapping(uint256 => RoundInfo) internal _rounds;

    /**
     * @notice Emitted when a subfeed submits a new exchange rate for a round.
     * @param roundId Identifier of the round to which the rate was submitted.
     * @param subFeed Address of the subfeed that submitted the rate.
     * @param rate The rate that was submitted by the subfeed.
     */
    event ExchangeRateSubmitted(uint256 indexed roundId, address indexed subFeed, int256 rate);

    /**
     * @notice Emitted when the aggregate exchange rate for a round is updated.
     * @param roundId Identifier of the round where the exchange rate update took place.
     * @param newRate The updated aggregate rate for the specified round.
     */
    event ExchangeRateUpdated(uint256 indexed roundId, uint256 newRate);

    /**
     * @notice Emitted when a new subfeed is added to the list of price subfeeds.
     * @param newSubFeed Address of the newly added subfeed.
     */
    event SubFeedAdded(address newSubFeed);

    /**
     * @notice Emitted when a subfeed is removed from the list of price subfeeds.
     * @param removedSubFeed Address of the subfeed that was removed.
     */
    event SubFeedRemoved(address removedSubFeed);

    /**
     * @notice Emitted when the minimum submission count is updated.
     * @param newMinCount The updated count value indicating the new minimum number of submissions required.
     */
    event MinSubmissionsCountSet(uint256 newMinCount);

    /**
     * @notice Emitted when the maximum submission count is updated.
     * @param newMaxCount The updated count value indicating the new maximum number of submissions allowed.
     */
    event MaxSubmissionsCountSet(uint256 newMaxCount);

    /**
     * @notice Emitted when the minimum allowable rate value is updated.
     * @param rate The updated value indicating the new minimum rate a subfeed can submit.
     */
    event MinRateValueSet(int256 rate);

    /**
     * @notice Emitted when a data collection round concludes.
     * @param roundId Identifier of the round that was closed.
     */
    event RoundClosed(uint256 roundId);

    /**
     * @notice Modifier to ensure a function is only callable by registered subfeeds.
     */
    modifier onlySubFeed() {
        require(_subFeeds.contains(msg.sender), "[QEC-041000]-Permission denied - only subFeeds have access.");
        _;
    }

    constructor(
        string memory pair_,
        uint256 decimal_,
        address[] memory subFeedsList_,
        address baseTokenAddr_,
        uint256 roundTime_,
        uint256 minSubmissionsCount_,
        uint256 maxSubmissionsCount_
    ) {
        require(bytes(pair_).length != 0, "[QEC-041001]-Invalid pair, the initialization failed.");
        require(decimal_ > 0, "[QEC-041002]-Invalid decimal, the initialization failed.");
        require(
            Address.isContract(baseTokenAddr_),
            "[QEC-041003]-The base token address is not a contract, the initialization failed."
        );

        for (uint256 i = 0; i < subFeedsList_.length; i++) {
            _subFeeds.add(subFeedsList_[i]);
        }

        pair = pair_;
        decimalPlaces = decimal_;
        baseTokenAddr = baseTokenAddr_;
        roundTime = roundTime_;
        minSubmissionsCount = minSubmissionsCount_;
        maxSubmissionsCount = maxSubmissionsCount_;
    }

    /**
     * @notice Submits a new rate for the current round or initializes a new round with the provided rate.
     * If the current round has expired, it's closed before accepting the new rate. If the number
     * of rates in the current round meets or exceeds the thresholds, the median price is computed
     * or the round is closed as appropriate.
     * @param rate_ The rate value to be submitted for the current round.
     */
    function submit(int256 rate_) external onlySubFeed {
        require(rate_ >= minRateValue, "[QEC-041004]-Submitted rate is less than minRateValue.");

        if (isOpenRound && block.timestamp > roundTime + roundStarted) {
            _closeRound();
        }

        uint256 currentRoundId_ = roundId;
        RoundInfo storage round = _rounds[currentRoundId_];

        require(!round.submitted[msg.sender], "[QEC-041005]-Already submitted in this round.");

        if (!isOpenRound) {
            roundStarted = block.timestamp;
            isOpenRound = true;
            round.startedAt = block.timestamp;
        }

        round.rates.push(rate_);
        round.submitted[msg.sender] = true;

        emit ExchangeRateSubmitted(currentRoundId_, msg.sender, rate_);

        uint256 roundRateLength_ = round.rates.length;
        if (roundRateLength_ >= minSubmissionsCount) {
            _compute(currentRoundId_);
        }

        if (roundRateLength_ >= maxSubmissionsCount) {
            _closeRound();
        }
    }

    /**
     * @notice Adds a new address to the list of authorized subfeeds.
     * @param feed_ Address to be added as a subfeed.
     */
    function addSubFeed(address feed_) external onlyOwner {
        _subFeeds.add(feed_);

        emit SubFeedAdded(feed_);
    }

    /**
     * @notice Updates the duration of a data collection round.
     * @param time_ Duration (in seconds) to set as the round duration.
     */
    function setRoundTime(uint256 time_) external onlyOwner {
        roundTime = time_;
    }

    /**
     * @notice Updates the minimum allowable rate value that subfeeds can submit.
     * @param rate_ Rate value to set as the new minimum submission value.
     */
    function setMinRateValue(int256 rate_) external onlyOwner {
        minRateValue = rate_;

        emit MinRateValueSet(rate_);
    }

    /**
     * @notice Updates the minimum number of submissions required to determine a new rate.
     * @param minCount_ The count to set as the new minimum number of submissions.
     */
    function setMinSubmissionsCount(uint256 minCount_) external onlyOwner {
        minSubmissionsCount = minCount_;

        emit MinSubmissionsCountSet(minCount_);
    }

    /**
     * @notice Updates the maximum number of submissions allowed in a single round.
     * @param maxCount_ The count to set as the new maximum number of submissions.
     */
    function setMaxSubmissionsCount(uint256 maxCount_) external onlyOwner {
        maxSubmissionsCount = maxCount_;

        emit MaxSubmissionsCountSet(maxCount_);
    }

    /**
     * @notice Removes an address from the list of authorized subfeeds.
     * @param feed_ Address of the subfeed to be removed.
     * @dev Ensures the number of remaining subfeeds doesn't fall below the minimum required for rate determination.
     */
    function removeSubFeed(address feed_) external onlyOwner {
        require(_subFeeds.length() > minSubmissionsCount, "[QEC-041006]-Count of subFeeds is minimal.");

        _subFeeds.remove(feed_);

        emit SubFeedRemoved(feed_);
    }

    /**
     * @notice Fetches a list of addresses representing active subfeeds.
     */
    function getSubFeeds() external view returns (address[] memory) {
        return _subFeeds.values();
    }

    /**
     * @notice Retrieves detailed information for a specified round of data collection.
     * @param roundId_ Identifier for the desired round.
     * @return Details of the specified round.
     */
    function getRoundInfo(uint256 roundId_)
        external
        view
        returns (
            int256[] memory,
            uint256,
            uint256
        )
    {
        RoundInfo storage round_ = _rounds[roundId_];

        return (round_.rates, round_.roundRate, round_.startedAt);
    }

    /**
     * @notice Checks if a specific subfeed has submitted its rate for a given round.
     * @param roundId_ Identifier of the round to check.
     * @param subFeed_ Address of the subfeed to verify.
     * @return True if the subfeed has submitted its rate for the specified round, otherwise false.
     */
    function isSubFeedSubmitted(uint256 roundId_, address subFeed_) external view returns (bool) {
        return _rounds[roundId_].submitted[subFeed_];
    }

    /**
     * @notice Fetches the current exchange rate and its last update timestamp.
     */
    function getRateAndTime() external view returns (uint256, uint256) {
        return (exchangeRate, updateTime);
    }

    function _compute(uint256 roundId_) private {
        uint256 newRate_ = uint256(Median.calculateInplace(_rounds[roundId_].rates));

        exchangeRate = newRate_;
        updateTime = block.timestamp;

        _rounds[roundId_].roundRate = newRate_;

        emit ExchangeRateUpdated(roundId_, newRate_);
    }

    function _closeRound() private {
        isOpenRound = false;

        emit RoundClosed(roundId++);
    }
}
