// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "./IBridgeValidators.sol";
import "../interfaces/IContractRegistry.sol";
import "../governance/validators/Validators.sol";

contract TokenBridgeAdminProxy is OwnableUpgradeable {
    IContractRegistry private registry;
    IBridgeValidators private bridgeValidators;

    event ValidatorAdded(address indexed _validator);
    event ValidatorRemoved(address indexed _validator);

    constructor() {}

    fallback() external onlyOwner {
        address addr = address(bridgeValidators);

        assembly {
            calldatacopy(0, 0, calldatasize())
            let result := call(gas(), addr, 0, 0, calldatasize(), 0, 0)
            returndatacopy(0, 0, returndatasize())
            switch result
            case 0 {
                revert(0, returndatasize())
            }
            default {
                return(0, returndatasize())
            }
        }
    }

    function initialize(address _registry, address _bridgeValidators) external initializer {
        registry = IContractRegistry(_registry);
        bridgeValidators = IBridgeValidators(_bridgeValidators);
        __Ownable_init();
    }

    function updateTokenbridgeValidators() external {
        address[] memory _newValidatorsList = _getNewValidatorsList();
        _updateTokenbridgeValidators(_newValidatorsList);
    }

    function _updateTokenbridgeValidators(address[] memory _newValidatorsList) private {
        address[] memory _currentBridgeValidators = bridgeValidators.validatorList();

        for (uint256 i = 0; i < _currentBridgeValidators.length; i++) {
            address _currentValidator = _currentBridgeValidators[i];

            bool found;
            for (uint256 j = 0; j < _newValidatorsList.length; j++) {
                if (_currentValidator == _newValidatorsList[j]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                bridgeValidators.removeValidator(_currentValidator);
            }
        }

        for (uint256 i = 0; i < _newValidatorsList.length; i++) {
            address _currentValidator = _newValidatorsList[i];

            if (!bridgeValidators.isValidator(_currentValidator)) {
                bridgeValidators.addValidator(_currentValidator);
            }
        }
    }

    function _getNewValidatorsList() private view returns (address[] memory) {
        address[] memory _currentSnapshot = Validators(registry.mustGetAddress(RKEY__VALIDATORS)).getValidatorsList();

        uint256 _newLength = _currentSnapshot.length;

        uint256 _maxCount = IParameters(registry.getAddress(RKEY__EPDR_PARAMETERS)).getUint(
            "governed.EPDR.maxTBvalidators"
        );

        if (_maxCount < _currentSnapshot.length) {
            _newLength = _maxCount;
        }

        address[] memory _newValidatorsList = new address[](_newLength);
        for (uint256 i = 0; i < _newLength; i++) {
            _newValidatorsList[i] = _currentSnapshot[i];
        }

        return _newValidatorsList;
    }
}
