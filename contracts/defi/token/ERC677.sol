// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "./IERC677.sol";

/**
 * @title ERC677 contract
 * @notice Contract that implements IERC677 interface
 */
contract ERC677 is IERC677, ERC20 {
    constructor(string memory name_, string memory symbol_) ERC20(name_, symbol_) {}

    /**
     * @notice transfers valuable and calls data
     * @param to_ address to transfer tokens
     * @param value_ amount to transfer
     * @param data_ calldata with which
     * @return boolean to specify if operations are successful
     */
    function transferAndCall(
        address to_,
        uint256 value_,
        bytes memory data_
    ) public override returns (bool) {
        bool result = super.transfer(to_, value_);
        if (!result) return false;

        emit Transfer(_msgSender(), to_, value_, data_);

        IERC677TransferReceiver receiver = IERC677TransferReceiver(to_);
        receiver.tokenFallback(_msgSender(), value_, data_);

        // IMPORTANT: the ERC-677 specification does not say
        // anything about the use of the receiver contract's
        // tokenFallback method return value. Given
        // its return type matches with this method's return
        // type, returning it could be a possibility.
        // We here take the more conservative approach and
        // ignore the return value, returning true
        // to signal a successful transfer despite tokenFallback's
        // return value -- fact being tokens are transferred
        // in any case.
        return true;
    }
}
