// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@opengsn/contracts/src/BaseRelayRecipient.sol";

import "./ERC677.sol";

import "../DefiParams.sol";

import "../../interfaces/IContractRegistry.sol";

/**
 * @title Stable Coin
 * @notice Holds the logic for stable coin tokens
 */
contract StableCoin is ERC677, ERC20Burnable, BaseRelayRecipient {
    using DefiParams for string;

    IContractRegistry private _registry;
    string[] private _eligibleContractKeys;

    /**
     * @notice Restricts callers only to eligible contracts
     */
    modifier onlyEligibleContract() {
        bool isFound_;

        for (uint256 i = 0; i < _eligibleContractKeys.length; ++i) {
            if (_registry.mustGetAddress(_eligibleContractKeys[i]) == _msgSender()) {
                isFound_ = true;
                break;
            }
        }

        require(isFound_, "[QEC-020000]-Permission denied - only the eligible contracts have access.");
        _;
    }

    /**
     * @notice Creates initial storage state
     * @param registry_ Token recipient address
     * @param name_ name for IERC20 token
     * @param symbol_ symbol for IERC20 token
     * @param eligibleContracts_ List of eligible contracts
     * @param forwarder_ GSN forwarder address
     */
    constructor(
        address registry_,
        string memory name_,
        string memory symbol_,
        string[] memory eligibleContracts_,
        address forwarder_
    ) ERC677(name_, symbol_) {
        _setTrustedForwarder(forwarder_);

        _registry = IContractRegistry(registry_);
        _eligibleContractKeys = eligibleContracts_;
    }

    /**
     * @notice Creates the transferred amount of tokens to the user
     * @dev For system contracts only
     * @param recipient_ Token recipient address
     * @param amount_ Number of tokens
     * @return true if everything went well
     */
    function mint(address recipient_, uint256 amount_) external onlyEligibleContract returns (bool) {
        _mint(recipient_, amount_);

        return true;
    }

    /**
     * @notice Returns the version of the recipient
     * @return recipient version
     */
    function versionRecipient() external pure override returns (string memory) {
        return "2.2.0";
    }

    function _msgSender() internal view override(Context, BaseRelayRecipient) returns (address) {
        return BaseRelayRecipient._msgSender();
    }

    function _msgData() internal view override(Context, BaseRelayRecipient) returns (bytes memory) {
        return BaseRelayRecipient._msgData();
    }
}
