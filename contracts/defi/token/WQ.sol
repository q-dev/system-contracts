// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "../../interfaces/defi/IWQ.sol";

contract WQ is IWQ, ERC20 {
    constructor() ERC20("Wrapped Q", "WQ") {}

    receive() external payable {
        _depositTo(msg.sender);
    }

    function deposit() external payable override {
        _depositTo(msg.sender);
    }

    function depositTo(address recipient_) external payable override {
        _depositTo(recipient_);
    }

    function withdraw(uint256 amount_) external override {
        _withdrawTo(msg.sender, amount_);
    }

    function withdrawTo(address recipient_, uint256 amount_) external override {
        _withdrawTo(recipient_, amount_);
    }

    function _depositTo(address recipient_) internal {
        require(msg.value != 0, "WQ: Zero deposit amount.");

        _mint(recipient_, msg.value);
    }

    function _withdrawTo(address recipient_, uint256 amount_) internal {
        require(amount_ != 0, "WQ: Zero withdraw amount.");

        _burn(msg.sender, amount_);

        (bool success_, ) = recipient_.call{value: amount_}("");
        require(success_, "WQ: Failed to transfer.");
    }
}
