// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IERC677TransferReceiver {
    function tokenFallback(
        address from_,
        uint256 amount_,
        bytes calldata data_
    ) external returns (bool);
}

/**
 * @title IERC677 interface
 */
abstract contract IERC677 is IERC20 {
    /**
     * @notice Event to notify about transfer
     * @param from Address funds taken from
     * @param to Address funds transferred to
     * @param value Amount of funds
     * @return data call data
     */
    event Transfer(address indexed from, address indexed to, uint256 value, bytes data);

    /**
     * @notice Transfer tokens to the recipient
     * @param to_ Address of the person to whom the funds will be transferred
     * @param value_ The amount of funds to be transferred
     * @param data_ Date of the transfer
     * @return true if everything went well
     */
    function transferAndCall(
        address to_,
        uint256 value_,
        bytes memory data_
    ) public virtual returns (bool);
}
