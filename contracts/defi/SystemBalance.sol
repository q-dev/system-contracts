// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./DefiParams.sol";
import "./SystemDebtAuction.sol";

import "../governance/IParameters.sol";

import "../interfaces/defi/IStableCoin.sol";
import "../interfaces/IContractRegistry.sol";

contract SystemBalance is Initializable {
    using DefiParams for string;

    struct SystemBalanceDetails {
        bool isDebtAuctionPossible;
        bool isSurplusAuctionPossible;
        uint256 currentDebt;
        uint256 debtThreshold;
        uint256 currentSurplus;
        uint256 surplusThreshold;
    }

    IContractRegistry private _registry;
    string private _stc;

    uint256 internal _debt;

    /**
     * @notice Restricts callers only to system surplus auction contracts
     */
    modifier onlySurplusAuction() {
        require(
            msg.sender == _registry.mustGetAddress(_stc.systemSurplusAuction()),
            "[QEC-023000]-Permission denied - only the SystemSurplusAuction contract has access."
        );
        _;
    }

    /**
     * @notice Restricts callers only to saving contracts
     */
    modifier onlySaving() {
        require(
            msg.sender == _registry.mustGetAddress(_stc.savingAddress()),
            "[QEC-023001]-Permission denied - only the Saving contract has access."
        );
        _;
    }

    /**
     * @notice Restricts callers only to liquidation auction and saving contracts
     */
    modifier onlyLiquidationAuctionOrSaving() {
        bool isLiquidationAuction_ = (msg.sender == _registry.mustGetAddress(_stc.liquidationAuctionAddress()));
        bool isSaving_ = (msg.sender == _registry.mustGetAddress(_stc.savingAddress()));

        require(
            (isLiquidationAuction_ || isSaving_),
            "[QEC-023002]-Permission denied - only the Liquidation auction and Saving contracts have access."
        );
        _;
    }

    constructor() {}

    function initialize(address registry_, string memory stc_) external virtual initializer {
        _registry = IContractRegistry(registry_);
        _stc = stc_;
    }

    /**
     * @notice increasing debt
     * @return true if the debt increasing was successful
     */
    function increaseDebt(uint256 debtAmount_) external onlyLiquidationAuctionOrSaving returns (bool) {
        _debt += debtAmount_;

        return true;
    }

    /**
     * @notice Surplus auction amount transferring
     * @return true if the surplus auction amount transferring was successful
     */
    function transferSurplusAuctionAmount(uint256 lot_) external onlySurplusAuction returns (bool) {
        return _transferToSystemContract(msg.sender, lot_);
    }

    /**
     * @notice Accrued interest amount transferring
     * @return true if the accrued interest amount transferring was successful
     */
    function transferAccruedInterestAmount(uint256 amount_) external onlySaving returns (bool) {
        return _transferToSystemContract(msg.sender, amount_);
    }

    /**
     * @notice perform netting: burn available surplus amount
     * @return true if the netting performing was successful
     */
    function performNetting() external returns (bool) {
        uint256 surplus_ = getSurplus();
        uint256 min_;

        if (surplus_ > _debt) {
            min_ = _debt;
        } else {
            min_ = surplus_;
        }

        if (min_ == 0) {
            return false;
        }

        _debt -= min_;

        IStableCoin(_getStableCoinAddress()).burn(min_);

        return true;
    }

    /**
     * @notice Returns debt
     * @return Debt
     */
    function getDebt() external view returns (uint256) {
        return _debt;
    }

    /**
     * @notice Returns balance in STC without debt
     * @return STC balance without debt
     */
    function getBalance() external view returns (int256) {
        unchecked {
            return int256(getSurplus() - _debt);
        }
    }

    /**
     * @notice Returns detailed information about the system balance
     * @return struct with system balance details
     */
    function getBalanceDetails() external view returns (SystemBalanceDetails memory) {
        IParameters params_ = IParameters(_registry.mustGetAddress(RKEY__EPDR_PARAMETERS));

        SystemBalanceDetails memory balanceDetails_;
        balanceDetails_.currentDebt = _debt;
        balanceDetails_.currentSurplus = getSurplus();
        balanceDetails_.debtThreshold = params_.getUint(_stc.debtThreshold());
        balanceDetails_.surplusThreshold = params_.getUint(_stc.surplusThreshold());

        if (balanceDetails_.currentDebt >= balanceDetails_.currentSurplus) {
            uint256 debtAfterNetting_ = balanceDetails_.currentDebt - balanceDetails_.currentSurplus;

            balanceDetails_.isDebtAuctionPossible =
                debtAfterNetting_ > balanceDetails_.debtThreshold &&
                !SystemDebtAuction(payable(_registry.mustGetAddress(_stc.systemDebtAuction()))).hasActiveAuction();
        } else {
            uint256 surplusAfterNetting_ = balanceDetails_.currentSurplus - balanceDetails_.currentDebt;
            uint256 lotSize_ = params_.getUint(_stc.surplusLot());

            balanceDetails_.isSurplusAuctionPossible =
                surplusAfterNetting_ >= balanceDetails_.surplusThreshold &&
                surplusAfterNetting_ >= lotSize_;
        }

        return balanceDetails_;
    }

    /**
     * @notice Returns balance in STC
     * @return STC balance
     */
    function getSurplus() public view returns (uint256) {
        return IStableCoin(_getStableCoinAddress()).balanceOf(address(this));
    }

    function _transferToSystemContract(address recipient_, uint256 amount_) internal returns (bool) {
        return IStableCoin(_getStableCoinAddress()).transfer(recipient_, amount_);
    }

    function _getStableCoinAddress() internal view returns (address) {
        return _registry.mustGetAddress(_stc.stableCoinAddress());
    }
}
