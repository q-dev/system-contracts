// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./DefiParams.sol";
import "./BorrowingCore.sol";
import "./SystemBalance.sol";

import "../common/Globals.sol";

import "../governance/IParameters.sol";

import "../interfaces/IContractRegistry.sol";

/**
 * @notice Manages the auctioning of collateral assets in case of borrowing vault liquidation.
 * The contract is tied to one synthetic asset (a.k.a stable coin).
 * Users bid an amount of the synthetic asset to win the collateral asset from the liquidated vault.
 * Liquidation auctions are identified by the same combination of user and vault ID as the liquidated vault.
 */
contract LiquidationAuction is Initializable {
    using DefiParams for string;

    enum AuctionStatus {
        NONE,
        ACTIVE,
        CLOSED
    }

    struct AuctionInfo {
        AuctionStatus status;
        address bidder;
        uint256 highestBid;
        uint256 endTime;
    }

    struct DefiContracts {
        BorrowingCore borrowingCore;
        SystemBalance systemBalance;
    }

    mapping(address => mapping(uint256 => AuctionInfo)) public auctions;

    IContractRegistry private _registry;
    string private _stc;

    event AuctionStarted(address indexed user, uint256 vaultId, address indexed bidder, uint256 bid);
    event Bid(address indexed user, uint256 indexed vaultId, address indexed bidder, uint256 bid);
    event Executed(address indexed user, uint256 vaultId, AuctionInfo info);

    constructor() {}

    /**
     * @notice Post constructor initialization to support upgradability via proxy deployment.
     * @param registry_ Address of registry contract
     * @param stc_ The symbol of the synthetic asset (a.k.a. stable coin).
     */
    function initialize(address registry_, string memory stc_) external initializer {
        _registry = IContractRegistry(registry_);
        _stc = stc_;
    }

    /**
     * @notice Starts an auction.
     * @param user_ The owner of the liquidated vault
     * @param vaultId_ The user specific vault id of the liquidated vault
     * @param bid_ The initial bid.
     */
    function startAuction(
        address user_,
        uint256 vaultId_,
        uint256 bid_
    ) external returns (bool) {
        IParameters params_ = IParameters(_getEPDRParametersAddress());
        BorrowingCore borrowingCore_ = BorrowingCore(_registry.mustGetAddress(_stc.borrowingAddress()));

        (, , , , bool isLiquidated_, ) = borrowingCore_.userVaults(user_, vaultId_);

        AuctionInfo memory auction_ = auctions[user_][vaultId_];

        require(
            auction_.status == AuctionStatus.NONE,
            "[QEC-024000]-A liquidation auction for this borrowing vault has already been started."
        );

        if (!isLiquidated_) {
            borrowingCore_.liquidate(user_, vaultId_);
        }

        _getStableCoin().transferFrom(msg.sender, address(this), bid_);

        auction_.status = AuctionStatus.ACTIVE;
        auction_.bidder = msg.sender;
        auction_.highestBid = bid_;
        auction_.endTime = block.timestamp + params_.getUint("governed.EPDR.liquidationAuctionP");
        auctions[user_][vaultId_] = auction_;

        emit AuctionStarted(user_, vaultId_, msg.sender, bid_);

        return true;
    }

    /**
     * @notice Places a bid for the given auction.
     * Returns funds to the previous highest bidder.
     * @param user_ The owner of the liquidated vault
     * @param vaultId_ The user specific vault id of the liquidated vault
     * @param bid_ The caller's raising bid
     */
    function bid(
        address user_,
        uint256 vaultId_,
        uint256 bid_
    ) external returns (bool) {
        AuctionInfo memory auction_ = auctions[user_][vaultId_];

        require(auction_.status == AuctionStatus.ACTIVE, "[QEC-024001]-The auction is not active, failed to bid.");
        require(
            auction_.endTime >= block.timestamp,
            "[QEC-024002]-The auction for this vault is finished, failed to bid."
        );
        require(
            bid_ >= _getRaisingBid(user_, vaultId_),
            "[QEC-024003]-The bid amount must exceed the highest bid by the minimum increment percentage or more."
        );

        IERC677 stcInstance_ = _getStableCoin();
        stcInstance_.transfer(auction_.bidder, auction_.highestBid);
        stcInstance_.transferFrom(msg.sender, address(this), bid_);

        auction_.bidder = msg.sender;
        auction_.highestBid = bid_;

        auctions[user_][vaultId_] = auction_;

        emit Bid(user_, vaultId_, msg.sender, bid_);

        return true;
    }

    /**
     * @notice Executes a finalized auction.
     * Pays back the open debt (creates system debt if necessary).
     * Transfers the vault collateral to the highest bidder.
     * Transfers liquidation fee as system surplus.
     * Any remainder of synthetic asset is sent to the liquidated vault owner.
     * @param user_ The owner of the liquidated vault
     * @param vaultId_ The user specific vault id of the liquidated vault
     */
    function execute(address user_, uint256 vaultId_) external returns (bool) {
        AuctionInfo memory auction_ = auctions[user_][vaultId_];
        DefiContracts memory defiContracts_;

        defiContracts_.borrowingCore = BorrowingCore(_registry.mustGetAddress(_stc.borrowingAddress()));
        defiContracts_.systemBalance = SystemBalance(_registry.mustGetAddress(_stc.systemBalanceAddress()));

        require(
            block.timestamp > auction_.endTime,
            "[QEC-024004]-The auction is not finished yet, failed to execute the liquidation."
        );
        require(auction_.status == AuctionStatus.ACTIVE, "[QEC-024005]-The auction has already been executed.");

        IERC677 stableCoin_ = _getStableCoin();
        uint256 oldBalance_ = stableCoin_.balanceOf(address(this));

        (string memory col_, , , uint256 mintedAmount_, , uint256 liquidationFullDebt_) = defiContracts_
            .borrowingCore
            .userVaults(user_, vaultId_);

        uint256 liquidationFee_ = (liquidationFullDebt_ *
            (IParameters(_getEPDRParametersAddress()).getUint(col_.liquidationFee(_stc)))) / getDecimal();

        uint256 amountToClear_ = liquidationFullDebt_;

        if (auction_.highestBid > liquidationFullDebt_ + liquidationFee_) {
            stableCoin_.transfer(user_, auction_.highestBid - liquidationFullDebt_ - liquidationFee_);
            stableCoin_.transfer(address(defiContracts_.systemBalance), liquidationFee_);
        } else if (auction_.highestBid > liquidationFullDebt_) {
            stableCoin_.transfer(address(defiContracts_.systemBalance), auction_.highestBid - liquidationFullDebt_);
        } else {
            amountToClear_ = auction_.highestBid;

            if (auction_.highestBid < mintedAmount_) {
                defiContracts_.systemBalance.increaseDebt(mintedAmount_ - auction_.highestBid);
            }
        }

        _closeAuction(user_, vaultId_);

        stableCoin_.approve(address(defiContracts_.borrowingCore), amountToClear_);
        defiContracts_.borrowingCore.clearVault(user_, vaultId_, amountToClear_, auction_.bidder);

        _checkExecute(oldBalance_, stableCoin_.balanceOf(address(this)), user_, vaultId_);

        emit Executed(user_, vaultId_, auction_);

        return true;
    }

    /**
     * @notice Retrieves the minimum next bid to exceed the current highest bid.
     * @param user_ The owner of the liquidated vault
     * @param vaultId_ The user specific vault id of the liquidated vault
     */
    function getRaisingBid(address user_, uint256 vaultId_) public view returns (uint256) {
        require(auctions[user_][vaultId_].status == AuctionStatus.ACTIVE, "[QEC-024006]-The auction is not active.");

        return _getRaisingBid(user_, vaultId_);
    }

    function _closeAuction(address user_, uint256 vaultId_) private {
        auctions[user_][vaultId_].status = AuctionStatus.CLOSED;
    }

    function _checkExecute(
        uint256 oldBalance_,
        uint256 newBalance_,
        address user_,
        uint256 vaultId_
    ) private view {
        uint256 bid_ = auctions[user_][vaultId_].highestBid;

        assert(oldBalance_ - bid_ == newBalance_);
    }

    function _getStableCoin() private view returns (IERC677) {
        return IERC677(_registry.mustGetAddress(_stc.stableCoinAddress()));
    }

    function _getRaisingBid(address user_, uint256 vaultId_) private view returns (uint256) {
        uint256 minimumIncrement_ = IParameters(_getEPDRParametersAddress()).getUint(
            "governed.EPDR.auctionMinIncrement"
        );

        uint256 highestBid_ = auctions[user_][vaultId_].highestBid;
        uint256 raisingBid_ = (highestBid_ * (minimumIncrement_)) / getDecimal() + highestBid_;

        if (raisingBid_ == highestBid_) {
            raisingBid_++;
        }

        return raisingBid_;
    }

    function _getEPDRParametersAddress() private view returns (address) {
        return _registry.mustGetAddress(RKEY__EPDR_PARAMETERS);
    }
}
