// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./DefiParams.sol";
import "./SystemBalance.sol";

import "../interfaces/defi/IStableCoin.sol";
import "../interfaces/IContractRegistry.sol";

import "../governance/IParameters.sol";

import "../tokeneconomics/PushPayments.sol";

/**
 * @notice Manages the auctioning of system surplus of a synthetic asset.
 * The contract is tied to one synthetic asset (a.k.a stable coin).
 * Users bid an amount of Q to win the auctioned lot of the synthetic asset.
 */
contract SystemSurplusAuction is Initializable {
    using DefiParams for string;

    struct AuctionInfo {
        bool isExecuted;
        address bidder;
        uint256 highestBid;
        uint256 lot;
        uint256 endTime;
    }

    IContractRegistry private _registry;
    string private _stc;

    uint256 public auctionsCount;
    mapping(uint256 => AuctionInfo) public auctions;

    event AuctionStarted(uint256 _auctionId, address indexed _bidder, uint256 _bid);
    event Bid(uint256 indexed _auctionId, address indexed _bidder, uint256 _bid);
    event Executed(uint256 _auctionId, address indexed _bidder, AuctionInfo _info);

    /**
     * @notice Guard for the existence of a given auction
     * @param auctionId_ The auction id
     */
    modifier shouldExist(uint256 auctionId_) {
        require(auctionId_ < auctionsCount, "[QEC-025004]-The auction does not exist.");
        _;
    }

    constructor() {}

    /**
     * @notice Post constructor initialization to support upgradability via proxy deployment.
     * @param registry_ Address of registry contract
     * @param stc_ The symbol of the synthetic asset (a.k.a. stable coin).
     */
    function initialize(address registry_, string memory stc_) external initializer {
        _registry = IContractRegistry(registry_);
        _stc = stc_;
    }

    /**
     * @notice Starts an auction with an initial bid of Q
     */
    function startAuction() external payable returns (uint256) {
        require(msg.value != 0, "[QEC-025000]-Invalid bid amount, failed start the auction.");

        SystemBalance systemBalance_ = SystemBalance(_registry.mustGetAddress(_stc.systemBalanceAddress()));

        systemBalance_.performNetting();

        IParameters params_ = IParameters(_getEPDRParametersAddress());

        uint256 surplusLot_ = params_.getUint(_stc.surplusLot());
        uint256 surplusThreshold_ = params_.getUint(_stc.surplusThreshold());

        uint256 surplus_ = systemBalance_.getSurplus();

        require(
            surplus_ >= surplusThreshold_,
            "[QEC-025002]-Surplus after netting is below surplus auction threshold."
        );

        require(surplus_ >= surplusLot_, "[QEC-025003]-Not enough surplus to fill the auction lot.");

        systemBalance_.transferSurplusAuctionAmount(surplusLot_);

        uint256 id_ = auctionsCount;
        auctionsCount++;

        AuctionInfo memory auction_;
        auction_.bidder = msg.sender;
        auction_.highestBid = msg.value;
        auction_.lot = surplusLot_;
        auction_.endTime = block.timestamp + params_.getUint("governed.EPDR.surplusAuctionP");
        auctions[id_] = auction_;

        emit AuctionStarted(id_, msg.sender, msg.value);

        return id_;
    }

    /**
     * @notice Places a bid for the given auction.
     * Returns funds to the previous highest bidder via the PushPayments contract.
     * @param auctionId_ The id of the auction to bid on
     */
    function bid(uint256 auctionId_) public payable shouldExist(auctionId_) returns (bool) {
        AuctionInfo storage auction_ = auctions[auctionId_];

        require(auction_.endTime >= block.timestamp, "[QEC-025005]-The auction is finished, failed to bid.");

        require(
            msg.value >= _getRaisingBid(auctionId_),
            "[QEC-025006]-The bid amount must exceed the highest bid by the minimum increment percentage or more."
        );

        PushPayments pushPayments_ = PushPayments(_registry.mustGetAddress(RKEY__PUSH_PAYMENTS));

        uint256 previousHighestBid_ = auction_.highestBid;
        address previousBidder_ = auction_.bidder;

        auction_.bidder = msg.sender;
        auction_.highestBid = msg.value;

        pushPayments_.safeTransferTo{value: previousHighestBid_}(previousBidder_);

        emit Bid(auctionId_, msg.sender, msg.value);

        return true;
    }

    /**
     * @notice Executes the given auction.
     * Transfers the auction lot of synthetic asset to the highest bidder.
     * Transfers the acquired Q as system surplus
     * @param auctionId_ The id of the auction to execute
     */
    function execute(uint256 auctionId_) public shouldExist(auctionId_) returns (bool) {
        AuctionInfo storage auction_ = auctions[auctionId_];

        require(auction_.endTime < block.timestamp, "[QEC-025008]-The auction is not finished.");
        require(!auction_.isExecuted, "[QEC-025009]-The auction has already been executed.");

        IStableCoin stableCoin_ = IStableCoin(_registry.mustGetAddress(_stc.stableCoinAddress()));
        stableCoin_.transfer(auction_.bidder, auction_.lot);

        auction_.isExecuted = true;

        (bool success_, ) = payable(_registry.mustGetAddress(RKEY__DEFAULT_ALLOCATION_PROXY)).call{
            value: auction_.highestBid
        }("");
        require(success_, "QEC-025010-Failed to transfer the highest bid for community distribution.");

        emit Executed(auctionId_, auction_.bidder, auction_);

        return true;
    }

    /**
     * @notice Retrieves the minimum next bid to exceed the current highest bid.
     * @param auctionId_ The id of the auction of interest
     */
    function getRaisingBid(uint256 auctionId_) public view shouldExist(auctionId_) returns (uint256) {
        return _getRaisingBid(auctionId_);
    }

    function _getRaisingBid(uint256 auctionId_) private view returns (uint256) {
        uint256 minimumIncrement_ = IParameters(_getEPDRParametersAddress()).getUint(
            "governed.EPDR.auctionMinIncrement"
        );

        uint256 highestBid_ = auctions[auctionId_].highestBid;
        uint256 raisingBid_ = (highestBid_ * minimumIncrement_) / getDecimal() + (highestBid_);

        if (raisingBid_ == highestBid_) {
            raisingBid_++;
        }

        return raisingBid_;
    }

    function _getEPDRParametersAddress() private view returns (address) {
        return _registry.mustGetAddress(RKEY__EPDR_PARAMETERS);
    }
}
