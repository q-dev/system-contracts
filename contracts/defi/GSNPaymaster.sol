// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@opengsn/contracts/src/BasePaymaster.sol";

struct ClientInfo {
    bool inWhitelist;
}

struct ContextInfo {
    uint256 timestamp;
}

contract GSNPaymaster is BasePaymaster {
    address public stc;
    mapping(address => ClientInfo) public clients;

    event PreRelayed(ContextInfo);
    event PostRelayed(ContextInfo);

    event AddressAddedToWhitelist(address indexed client);
    event AddressDroppedFromWhitelist(address indexed client);

    constructor(address _stc) {
        stc = _stc;
    }

    function preRelayedCall(
        GsnTypes.RelayRequest calldata relayRequest,
        bytes calldata signature,
        bytes calldata approvalData,
        uint256 maxPossibleGas
    ) external virtual override returns (bytes memory context, bool) {
        require(clients[relayRequest.request.from].inWhitelist, "Sender is not in Whitelist");
        require(stc == relayRequest.request.to, "Wrong target");
        _verifyForwarder(relayRequest);
        (signature, approvalData, maxPossibleGas);
        ContextInfo memory _context = ContextInfo(block.timestamp);
        emit PreRelayed(_context);
        return (abi.encode(_context), false);
    }

    function postRelayedCall(
        bytes calldata context,
        bool success,
        uint256 gasUseWithoutPost,
        GsnTypes.RelayData calldata relayData
    ) external virtual override {
        (context, success, gasUseWithoutPost, relayData);
        emit PostRelayed(abi.decode(context, (ContextInfo)));
    }

    function versionPaymaster() external pure virtual override returns (string memory) {
        return "2.2.0";
    }

    /**
     * @notice add clients to Whitelist
     *         emit AddressAddedToWhitelist if client added
     * @param _clients array of clients' addresses
     */
    function addToWhitelist(address[] memory _clients) public onlyOwner {
        for (uint256 i = 0; i < _clients.length; i++) {
            if (!clients[_clients[i]].inWhitelist) {
                clients[_clients[i]].inWhitelist = true;
                emit AddressAddedToWhitelist(_clients[i]);
            }
        }
    }

    /**
     * @notice drop clients from Whitelist
     *         emit AddressDroppedFromWhitelist if client dropped
     * @param _clients array of clients' addresses
     */
    function dropFromWhitelist(address[] memory _clients) public onlyOwner {
        for (uint256 i = 0; i < _clients.length; i++) {
            if (clients[_clients[i]].inWhitelist) {
                clients[_clients[i]].inWhitelist = false;
                emit AddressDroppedFromWhitelist(_clients[i]);
            }
        }
    }
}
