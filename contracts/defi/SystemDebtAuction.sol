// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./DefiParams.sol";
import "./SystemBalance.sol";
import "./token/IERC677.sol";

import "../common/Globals.sol";

import "./oracles/IFxPriceFeed.sol";

import "../governance/IParameters.sol";

import "../interfaces/IContractRegistry.sol";

import "../tokeneconomics/SystemReserve.sol";
import "../tokeneconomics/PushPayments.sol";

/**
 * @notice Manages the auctioning of Q from the system reserve to reduce system debt.
 * The contract is tied to one synthetic asset (a.k.a stable coin).
 * Users bid an amount of the synthetic asset to win the auctioned lot of Q.
 * There can only be one system debt auction at a time.
 */
contract SystemDebtAuction is Initializable {
    using DefiParams for string;

    enum AuctionStatus {
        NONE,
        ACTIVE,
        CLOSED
    }

    struct AuctionInfo {
        AuctionStatus status;
        address bidder;
        uint256 highestBid;
        uint256 endTime;
        uint256 reserveLot;
    }

    IContractRegistry private _registry;
    string private _stc;

    mapping(uint256 => AuctionInfo) public auctions;
    uint256 public currentAuctionId;

    event AuctionStarted(uint256 indexed auctionId, address indexed bidder, uint256 bid);
    event Bid(uint256 indexed auctionId, address indexed bidder, uint256 bid);
    event Executed(uint256 indexed auctionId, address indexed bidder, AuctionInfo info);

    constructor() {}

    /**
     * @notice Accepts incoming Q transfers without additional logic.
     */
    receive() external payable {}

    /**
     * @notice Post constructor initialization to support upgradability via proxy deployment.
     * @param registry_ Address of registry contract
     * @param stc_ The symbol of the synthetic asset (a.k.a. stable coin).
     */
    function initialize(address registry_, string memory stc_) external initializer {
        _registry = IContractRegistry(registry_);
        _stc = stc_;
    }

    /**
     * @notice Starts an auction.
     * @param bid_ The initial bid.
     */
    function startAuction(uint256 bid_) external returns (bool) {
        IParameters params_ = IParameters(_getEPDRParametersAddress());
        SystemBalance systemBalance_ = SystemBalance(_registry.mustGetAddress(_stc.systemBalanceAddress()));
        SystemReserve systemReserve_ = SystemReserve(payable(_registry.mustGetAddress(RKEY__SYSTEM_RESERVE)));

        uint256 auctionId_ = currentAuctionId;
        AuctionInfo memory auction_ = auctions[auctionId_];

        require(
            auction_.status != AuctionStatus.ACTIVE,
            "[QEC-026000]-Only one system debt auction can run at a time."
        );

        systemBalance_.performNetting();

        require(
            systemBalance_.getDebt() > params_.getUint(_stc.debtThreshold()),
            "[QEC-026001]-System debt is below auction threshold."
        );

        _getStableCoin().transferFrom(msg.sender, address(this), bid_);

        uint256 reserveLot_ = params_.getUint("governed.EPDR.reserveLot");

        require(
            systemReserve_.withdraw(reserveLot_),
            "[QEC-026002]-Failed to withdraw from the SystemReserve contract, failed to start the auction."
        );

        auction_.status = AuctionStatus.ACTIVE;
        auction_.bidder = msg.sender;
        auction_.highestBid = bid_;
        auction_.reserveLot = reserveLot_;
        auction_.endTime = block.timestamp + params_.getUint("governed.EPDR.debtAuctionP");
        auctions[auctionId_] = auction_;

        emit AuctionStarted(auctionId_, msg.sender, bid_);

        _checkBalances(auctionId_);

        return true;
    }

    /**
     * @notice Places a bid for the current auction.
     * Returns funds to the previous highest bidder.
     * @param bid_ The caller's raising bid.
     */
    function bid(uint256 bid_) external returns (bool) {
        uint256 auctionId_ = currentAuctionId;

        AuctionInfo memory auction_ = auctions[auctionId_];

        require(auction_.status == AuctionStatus.ACTIVE, "[QEC-026003]-The auction is not active, failed to bid.");
        require(auction_.endTime >= block.timestamp, "[QEC-026004]-The auction is finished, failed to bid.");
        require(
            bid_ >= getRaisingBid(auctionId_),
            "[QEC-026005]-The bid amount must exceed the highest bid by the minimum increment percentage or more."
        );

        IERC677 stableCoin_ = _getStableCoin();

        stableCoin_.transfer(auction_.bidder, auction_.highestBid);
        stableCoin_.transferFrom(msg.sender, address(this), bid_);

        auction_.bidder = msg.sender;
        auction_.highestBid = bid_;
        auctions[auctionId_] = auction_;

        emit Bid(auctionId_, msg.sender, bid_);

        _checkBalances(auctionId_);

        return true;
    }

    /**
     * @notice Executes the current auction.
     * Transfers the auction lot of Q to the highest bidder.
     * Transfers the acquired synthetic asset as system surplus.
     * Via netting, this can be burnt to reduce the system debt.
     */
    function execute() external returns (bool) {
        uint256 auctionId_ = currentAuctionId;

        AuctionInfo memory auction_ = auctions[currentAuctionId];
        SystemBalance _systemBalance = SystemBalance(_registry.mustGetAddress(_stc.systemBalanceAddress()));

        IERC677 _stableCoin = _getStableCoin();

        require(block.timestamp > auction_.endTime, "[QEC-026006]-The auction is not finished.");
        require(auction_.status == AuctionStatus.ACTIVE, "[QEC-026007]-The auction is not active, execution failed.");

        _checkBalances(auctionId_);

        _stableCoin.transfer(address(_systemBalance), auction_.highestBid);

        auctions[currentAuctionId].status = AuctionStatus.CLOSED;

        currentAuctionId++;

        PushPayments _pushPayments = PushPayments(_registry.mustGetAddress(RKEY__PUSH_PAYMENTS));

        _pushPayments.safeTransferTo{value: auction_.reserveLot}(auction_.bidder);

        emit Executed(auctionId_, auction_.bidder, auction_);

        return true;
    }

    /**
     * @notice Retrieves the minimum next bid to exceed the current highest bid.
     * @param auctionId_ the auction id of interest (usually `currentAuctionId`)
     */
    function getRaisingBid(uint256 auctionId_) public view returns (uint256) {
        require(auctionId_ <= currentAuctionId, "[QEC-026009]-The auction does not exist.");

        return _getRaisingBid(auctionId_);
    }

    /**
     * @notice Indicates, whether a system debt auction is currently active.
     * @return true if there's an active auction
     */
    function hasActiveAuction() public view returns (bool) {
        return auctions[currentAuctionId].status == AuctionStatus.ACTIVE;
    }

    function _checkBalances(uint256 auctionId_) private view {
        if (auctions[auctionId_].status == AuctionStatus.ACTIVE) {
            IERC677 stableCoin_ = _getStableCoin();

            assert(stableCoin_.balanceOf(address(this)) >= auctions[auctionId_].highestBid);
            assert(address(this).balance >= auctions[auctionId_].reserveLot);
        }
    }

    function _getStableCoin() private view returns (IERC677) {
        return IERC677(_registry.mustGetAddress(_stc.stableCoinAddress()));
    }

    function _getRaisingBid(uint256 auctionId_) private view returns (uint256) {
        uint256 minimumIncrement_ = IParameters(_getEPDRParametersAddress()).getUint(
            "governed.EPDR.auctionMinIncrement"
        );

        uint256 highestBid_ = auctions[auctionId_].highestBid;
        uint256 raisingBid_ = (highestBid_ * minimumIncrement_) / getDecimal() + (highestBid_);

        if (raisingBid_ == highestBid_) {
            raisingBid_++;
        }

        return raisingBid_;
    }

    function _getEPDRParametersAddress() private view returns (address) {
        return _registry.mustGetAddress(RKEY__EPDR_PARAMETERS);
    }
}
