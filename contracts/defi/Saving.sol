// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "./DefiParams.sol";
import "./SystemBalance.sol";
import "./BorrowingCore.sol";

import "../interfaces/defi/IStableCoin.sol";
import "../interfaces/IContractRegistry.sol";

import "../governance/IParameters.sol";
import "../governance/constitution/Constitution.sol";

import "../common/Globals.sol";
import "../common/CompoundRateKeeper.sol";

/**
 * @title Saving
 * @notice Used to earn interest on holdings by STC holders
 */
contract Saving is Initializable {
    using DefiParams for string;

    struct BalanceDetails {
        uint256 currentBalance;
        uint256 normalizedBalance;
        uint256 compoundRate;
        uint256 lastUpdateOfCompoundRate;
        uint256 interestRate;
    }

    IContractRegistry internal _registry;
    string internal _stc;

    CompoundRateKeeper public compoundRateKeeper;
    uint256 public aggregatedNormalizedCapital;

    mapping(address => uint256) public normalizedCapitals;

    event UserDeposited(address indexed user, uint256 depositAmount);
    event UserWithdrawn(address indexed user, uint256 withdrawAmount);

    constructor() {}

    function initialize(address registry_, string memory stc_) external initializer {
        _registry = IContractRegistry(registry_);
        _stc = stc_;

        compoundRateKeeper = CompoundRateKeeperFactory(_registry.mustGetAddress(RKEY__CR_KEEPER_FACTORY)).create();
    }

    /**
     * @notice Deposits STC to saving balance of the user
     * @param amount_ The amount of STC the user wants to deposit
     * @return true if everything went well
     */
    function deposit(uint256 amount_) external returns (bool) {
        require(amount_ > 0, "[QEC-022000]-Deposit amount must not be zero.");

        IStableCoin(_registry.mustGetAddress(_stc.stableCoinAddress())).transferFrom(
            msg.sender,
            address(this),
            amount_
        );

        uint256 newNormalizedCapital_ = compoundRateKeeper.normalizeAmount(getBalance() + amount_);

        aggregatedNormalizedCapital =
            aggregatedNormalizedCapital +
            (newNormalizedCapital_ - normalizedCapitals[msg.sender]);

        normalizedCapitals[msg.sender] = newNormalizedCapital_;

        emit UserDeposited(msg.sender, amount_);

        _checkBalance();

        return true;
    }

    /**
     * @notice Withdraws STC from the saving balance of the user
     * @param amount_ The amount of STC the user wants to withdraw
     * @return true if everything went well
     */
    function withdraw(uint256 amount_) external returns (bool) {
        uint256 userBalance_ = getBalance();

        require(userBalance_ != 0, "[QEC-022001]-The caller does not have any balance to withdraw.");

        uint256 withdrawAmount_ = amount_;

        if (userBalance_ < withdrawAmount_) {
            withdrawAmount_ = userBalance_;
        }

        uint256 newNormalizedCapital_ = compoundRateKeeper.normalizeAmount(userBalance_ - withdrawAmount_);

        aggregatedNormalizedCapital =
            aggregatedNormalizedCapital -
            (normalizedCapitals[msg.sender] - newNormalizedCapital_);

        normalizedCapitals[msg.sender] = newNormalizedCapital_;

        IStableCoin(_registry.mustGetAddress(_stc.stableCoinAddress())).transfer(msg.sender, withdrawAmount_);

        emit UserWithdrawn(msg.sender, withdrawAmount_);

        _checkBalance();

        return true;
    }

    /**
     * @notice Calculates and updates the compound rate
     * @return New compound rate
     */
    function updateCompoundRate() external returns (uint256) {
        CompoundRateKeeper compoundRateKeeper_ = compoundRateKeeper;
        string memory stc_ = _stc;

        uint256 savingRate_ = IParameters(_getEPDRParametersAddress()).getUint(DefiParams.savingRate(stc_));

        uint256 oldAggregatedDenormalizedCapital_ = compoundRateKeeper_.denormalizeAmount(aggregatedNormalizedCapital);
        uint256 newRate_ = compoundRateKeeper_.update(savingRate_);
        uint256 newAggregatedDenormalizedCapital_ = compoundRateKeeper_.denormalizeAmount(aggregatedNormalizedCapital);

        SystemBalance systemBalance_ = SystemBalance(_registry.mustGetAddress(stc_.systemBalanceAddress()));

        uint256 surplus_ = systemBalance_.getSurplus();
        uint256 accruedSavings_ = newAggregatedDenormalizedCapital_ - oldAggregatedDenormalizedCapital_;

        if (surplus_ < accruedSavings_) {
            BorrowingCore.AggregatedTotalsInfo memory totalsInfo_ = BorrowingCore(
                _registry.mustGetAddress(stc_.borrowingAddress())
            ).getAggregatedTotals();

            uint256 missingAmount_ = accruedSavings_ - surplus_;

            require(
                systemBalance_.getDebt() + missingAmount_ <= totalsInfo_.owedBorrowingFees,
                "[QEC-022002]-System debt exceeds owed borrowing fees, failed to update compound rate."
            );

            IStableCoin(_registry.mustGetAddress(stc_.stableCoinAddress())).mint(
                address(systemBalance_),
                missingAmount_
            );

            systemBalance_.increaseDebt(missingAmount_);
        }

        systemBalance_.transferAccruedInterestAmount(accruedSavings_);

        _checkBalance();

        return newRate_;
    }

    /**
     * @notice Returns detailed information about the user's balance
     * @return BalanceDetails struct
     */
    function getBalanceDetails() external view returns (BalanceDetails memory) {
        BalanceDetails memory balanceDetails_;
        CompoundRateKeeper compoundRateKeeper_ = compoundRateKeeper;

        balanceDetails_.compoundRate = compoundRateKeeper_.getCurrentRate();
        balanceDetails_.lastUpdateOfCompoundRate = compoundRateKeeper_.getLastUpdate();
        balanceDetails_.currentBalance = getBalance();
        balanceDetails_.normalizedBalance = normalizedCapitals[msg.sender];
        balanceDetails_.interestRate = IParameters(_getEPDRParametersAddress()).getUint(DefiParams.savingRate(_stc));

        return balanceDetails_;
    }

    /**
     * @notice Returns the saving balance of the user
     * @return Amount of the user balance
     */
    function getBalance() public view returns (uint256) {
        return compoundRateKeeper.denormalizeAmount(normalizedCapitals[msg.sender]);
    }

    function _checkBalance() private view {
        uint256 aggregatedDenormalizedCapital_ = compoundRateKeeper.denormalizeAmount(aggregatedNormalizedCapital);
        uint256 balance_ = IStableCoin(_registry.mustGetAddress(_stc.stableCoinAddress())).balanceOf(address(this));

        assert(balance_ >= aggregatedDenormalizedCapital_);
    }

    function _getEPDRParametersAddress() private view returns (address) {
        return _registry.mustGetAddress(RKEY__EPDR_PARAMETERS);
    }
}
