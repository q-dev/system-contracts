// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

/**
 * @notice This interface is needed for correct interaction with the BridgeValidators contract,
 * which is located in TokenBridge
 */
interface IBridgeValidators {
    function addValidator(address _validator) external;

    function removeValidator(address _validator) external;

    function validatorList() external view returns (address[] calldata);

    function validatorCount() external view returns (uint256);

    function requiredSignatures() external view returns (uint256);

    function isValidator(address _validator) external view returns (bool);
}
