// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IBridgeValidators.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract ForeignChainTokenBridgeAdminProxy is OwnableUpgradeable {
    IBridgeValidators bridgeValidators;

    event ValidatorAdded(address indexed _validator);
    event ValidatorRemoved(address indexed _validator);

    constructor() {}

    fallback() external onlyOwner {
        address addr = address(bridgeValidators);

        assembly {
            calldatacopy(0, 0, calldatasize())
            let result := call(gas(), addr, 0, 0, calldatasize(), 0, 0)
            returndatacopy(0, 0, returndatasize())
            switch result
            case 0 {
                revert(0, returndatasize())
            }
            default {
                return(0, returndatasize())
            }
        }
    }

    function initialize(address _bridgeValidators) external initializer {
        bridgeValidators = IBridgeValidators(_bridgeValidators);
        __Ownable_init();
    }

    function updateTokenbridgeValidators(address[] memory _newValidatorsList) external onlyOwner {
        address[] memory _currentBridgeValidators = bridgeValidators.validatorList();

        for (uint256 i = 0; i < _currentBridgeValidators.length; i++) {
            address _currentValidator = _currentBridgeValidators[i];
            bool found;
            for (uint256 j = 0; j < _newValidatorsList.length; j++) {
                if (_currentValidator == _newValidatorsList[j]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                bridgeValidators.removeValidator(_currentValidator);
            }
        }

        for (uint256 i = 0; i < _newValidatorsList.length; i++) {
            address _currentValidator = _newValidatorsList[i];

            if (!bridgeValidators.isValidator(_currentValidator)) {
                bridgeValidators.addValidator(_currentValidator);
            }
        }
    }
}
