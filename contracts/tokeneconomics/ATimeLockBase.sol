// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

struct TimeLockEntry {
    uint256 amount;
    uint256 releaseStart;
    uint256 releaseEnd;
}

abstract contract ATimeLockBase {
    mapping(address => TimeLockEntry[]) timeLocks;

    uint256 constant minimumLockAmount = 10 ether;
    uint256 constant maximumTimelocksNumber = 10;
    uint256 constant MAX_TIME_LOCK_END = 64_060_588_861; // Jan 1, 4000

    event NewTimeLock(address indexed _account, TimeLockEntry _timelock);
    event Purged(address indexed _account, TimeLockEntry _timelock);

    /*
     * @notice deposit native tokens with timelocking
     * @param _account address of account
     * @param _releaseStart timestamp of timelock start
     * @param _releaseEnd   timestamp of timelock ending
     */
    function depositOnBehalfOf(
        address _account,
        uint256 _releaseStart,
        uint256 _releaseEnd
    ) external payable {
        require(_releaseStart < _releaseEnd, "[QEC-032001]-Time lock release end must be after release start.");
        require(msg.value >= minimumLockAmount, "[QEC-032002]-Given amount is below deposit minimum.");
        require(_releaseEnd < MAX_TIME_LOCK_END, "[QEC-032005]-Release end is too high.");

        _purgeTimeLocks(_account);

        require(
            timeLocks[_account].length < maximumTimelocksNumber,
            "[QEC-032003]-Failed to deposit amount, too many timelocks."
        );

        if (_releaseEnd > block.timestamp) {
            TimeLockEntry memory _timelock;
            _timelock.amount = msg.value;
            _timelock.releaseStart = _releaseStart;
            _timelock.releaseEnd = _releaseEnd;
            timeLocks[_account].push(_timelock);
            emit NewTimeLock(_account, _timelock);
        }
        _onTimeLockedDeposit(_account, msg.value);
    }

    /*
     * @notice remove expired timelocks from a given account
     * @param _account address of account which timelocks to be purged
     */
    function purgeTimeLocks(address _account) external {
        _purgeTimeLocks(_account);
    }

    /*
     * @notice array of _account's timelocks getter
     * @return  array of TimeLockEntry
     */
    function getTimeLocks(address _account) external view returns (TimeLockEntry[] memory) {
        return timeLocks[_account];
    }

    /*
     * @notice locked amount getter
     * @param _account address of account which locked amount to be calculated
     * @param _timestamp moment when timelocked amount to be calculated
     * @return amount that timelocked at _timestamp moment
     */
    function getMinimumBalance(address _account, uint256 _timestamp) public view returns (uint256) {
        if (timeLocks[_account].length == 0) {
            return 0;
        }
        uint256 minBalance;
        for (uint256 i = 0; i < timeLocks[_account].length; i++) {
            TimeLockEntry memory _timelock = timeLocks[_account][i];
            minBalance += _getLockBalance(_timelock, _timestamp);
        }
        return minBalance;
    }

    function _onTimeLockedDeposit(address _account, uint256 _amount) internal virtual;

    function _purgeTimeLocks(address _account) private {
        TimeLockEntry[] memory _userTimeLocks = timeLocks[_account];
        delete timeLocks[_account];

        for (uint256 i = 0; i < _userTimeLocks.length; i++) {
            if (block.timestamp < _userTimeLocks[i].releaseEnd) {
                timeLocks[_account].push(_userTimeLocks[i]);
            } else {
                emit Purged(_account, _userTimeLocks[i]);
            }
        }
    }

    function _getLockBalance(TimeLockEntry memory _timelock, uint256 _timestamp) private pure returns (uint256) {
        if (_timestamp < _timelock.releaseStart) {
            return _timelock.amount;
        } else if (_timestamp >= _timelock.releaseEnd) {
            return 0;
        }
        uint256 remainingPeriod = _timelock.releaseEnd - _timestamp;
        uint256 originalPeriod = _timelock.releaseEnd - _timelock.releaseStart;

        return (_timelock.amount * remainingPeriod) / (originalPeriod);
    }
}
