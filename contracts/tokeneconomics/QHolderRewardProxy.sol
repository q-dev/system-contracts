// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../governance/IParameters.sol";
import "../interfaces/IContractRegistry.sol";
import "../common/Globals.sol";

/**
 * @title Q Holder Reward Proxy
 * @notice Performs allocation of rewards for Q holders
 */
contract QHolderRewardProxy is Initializable {
    IContractRegistry private registry;

    /**
     * @notice event to notify about allocation of rewards for Q holders
     * @param amount that was transferred to SystemReserve and QHolderRewardPool contracts.
     */
    event Allocated(uint256 amount);

    constructor() {}

    receive() external payable {
        allocate();
    }

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice performs default allocation procedure
     */
    function allocate() public {
        IParameters parameters = IParameters(registry.mustGetAddress(RKEY__EPQFI_PARAMETERS));

        uint256 transferredAmount = address(this).balance;
        (bool success, ) = payable(registry.mustGetAddress(RKEY__SYSTEM_RESERVE)).call{
            value: (parameters.getUint("governed.EPQFI.Q_reserveShare") * (address(this).balance)) / getDecimal()
        }("");
        require(success, "[QEC-014000]-Failed to transfer the reserve share to the SystemReserve contract.");

        (success, ) = payable(registry.mustGetAddress(RKEY__QHOLDER_REWARD_POOL)).call{value: address(this).balance}(
            ""
        );
        require(
            success,
            "[QEC-014001]-Failed to transfer the Q token holder reward to the QHolderRewardPool contract."
        );

        emit Allocated(transferredAmount);
    }
}
