// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import {PaymentStretchingBuckets} from "./PaymentStretchingBuckets.sol";

/**
 * @title DefaultAllocationThrottle
 * @notice Extends the functionality of the PaymentStretchingBuckets contract with Linear Release feature.
 */
contract DefaultAllocationThrottle is PaymentStretchingBuckets {
    uint256 public constant PRECISION = 10**27;

    uint256 public initialCurrentBucketBalance;

    event CurrentBucketSwept(uint256 newAllocatableBalance);

    /**
     * @notice Function for changing the recipient address.
     */
    function setNewRecipient() external {
        recipient = 0x2fe3180D143665fFD0513b87CB4D0787c2444eBd;
    }

    /**
     * @inheritdoc PaymentStretchingBuckets
     *
     * @notice This function overrides the original `getReleasedAmount` from `PaymentStretchingBuckets`.
     *
     * It handles a situation if the current bucket might already have a partial release.
     */
    function getReleasedAmount() public view override returns (uint256) {
        return super.getReleasedAmount() + _calculateUnreleasedAmount();
    }

    /**
     * @inheritdoc PaymentStretchingBuckets
     *
     * @notice Overrides `_stretchPayment` from `PaymentStretchingBuckets` to ensure:
     *   1. Correct updating of `firstNonEmptyBucketIndex` when the first bucket is initially empty.
     *      a. The initialization process for `initialCurrentBucketBalance` is detailed in `_updateInitialCurrentBucketBalance`.
     *   2. Updates to `initialCurrentBucketBalance` occur when the `firstNonEmptyBucketIndex` is 0.
     */
    function _stretchPayment(uint256 depositedAmount_, uint256 numberOfBuckets_) internal override {
        super._stretchPayment(depositedAmount_, numberOfBuckets_);

        if (bucketBalances[firstNonEmptyBucketIndex % MAX_BUCKET_COUNT] == 0) {
            firstNonEmptyBucketIndex = getCurrentBucketIndex() + 1;

            _updateInitialCurrentBucketBalance();
        }
    }

    /**
     * @inheritdoc PaymentStretchingBuckets
     *
     * @notice Overrides `_sweepOldBuckets` from `PaymentStretchingBuckets` to ensure:
     *   1. The `initialCurrentBucketBalance` is updated before updating the `firstNonEmptyBucketIndex`.
     *   2. Sweeping of the current bucket is performed after calling the base contract's `_sweepOldBuckets` method.
     *   3. The current bucket is swept regardless of its balance status, which may include the case where enough
     *      time has passed and the bucket has a non-zero balance.
     */
    function _sweepOldBuckets() internal override {
        _updateInitialCurrentBucketBalance();

        super._sweepOldBuckets();

        _sweepCurrentBucket();
    }

    /**
     * @inheritdoc PaymentStretchingBuckets
     *
     * @notice This function overrides `_getStartingBucketIndexToStretch` from `PaymentStretchingBuckets`.
     * To prevent immediate allocation upon deposit, distribution starts from the bucket subsequent
     * to the first non-empty one.
     */
    function _getStartingBucketIndexToStretch() internal view override returns (uint256) {
        return firstNonEmptyBucketIndex + 1;
    }

    /**
     * @notice Sweeps a portion of the balance from the current bucket.
     *
     * This function computes the unreleased amount in the current bucket using `_calculateUnreleasedAmount`.
     * It then updates the `allocatableBalance` by adding this computed amount. The balance of
     * the current bucket, identified using `getCurrentBucketIndex() % MAX_BUCKET_COUNT`, is reduced by the same amount.
     * Finally, the function emits a `CurrentBucketSwept` event with the updated `allocatableBalance`.
     */
    function _sweepCurrentBucket() private {
        uint256 partiallyReleasedAmount_ = _calculateUnreleasedAmount();

        allocatableBalance += partiallyReleasedAmount_;
        bucketBalances[getCurrentBucketIndex() % MAX_BUCKET_COUNT] -= partiallyReleasedAmount_;

        emit CurrentBucketSwept(allocatableBalance);
    }

    /**
     * @notice Updates the `initialCurrentBucketBalance` based on the current bucket index.
     *
     * This function handles two scenarios:
     *   1. The common case where the current bucket index is greater than `firstNonEmptyBucketIndex`.
     *      In this scenario, the balance of the current bucket is directly set as `initialCurrentBucketBalance`.
     *
     *   2. The edge case where all buckets are empty, typically encountered during deposit operations.
     *      In such a case, according to contract logic, the balance should be added to the bucket next to the current one.
     *      Therefore, `initialCurrentBucketBalance` is set to the balance of the next bucket (currentBucketIndex + 1).
     *      Refer to the `_calculateUnreleasedAmount` function for more details on edge case handling.
     */
    function _updateInitialCurrentBucketBalance() private {
        uint256 currentBucketIndex_ = getCurrentBucketIndex();

        if (currentBucketIndex_ > firstNonEmptyBucketIndex) {
            initialCurrentBucketBalance = bucketBalances[currentBucketIndex_ % MAX_BUCKET_COUNT];
        } else if (bucketBalances[currentBucketIndex_ % MAX_BUCKET_COUNT] == 0) {
            initialCurrentBucketBalance = bucketBalances[(currentBucketIndex_ + 1) % MAX_BUCKET_COUNT];
        }
    }

    /**
     * @notice Returns the initial balance based on the current bucket index.
     *
     * This function handles two cases:
     *   1. If the current bucket index is different from `firstNonEmptyBucketIndex`,
     *      it returns the balance of the current bucket.
     *   2. If the current bucket index is the same as `firstNonEmptyBucketIndex`,
     *      it returns the precomputed `initialCurrentBucketBalance`.
     *
     * This approach ensures that the correct initial balance is returned based on the relative positioning
     * of the current bucket index to the `firstNonEmptyBucketIndex`.
     */
    function _getInitialCurrentBucketBalance() private view returns (uint256) {
        uint256 currentBucketIndex_ = getCurrentBucketIndex();

        if (currentBucketIndex_ != firstNonEmptyBucketIndex) {
            return bucketBalances[currentBucketIndex_ % MAX_BUCKET_COUNT];
        }

        return initialCurrentBucketBalance;
    }

    /**
     * @notice Calculates the unreleased amount of tokens from the current bucket.
     *
     * This function determines the unreleased amount by considering the time elapsed in the current time unit
     * relative to the entire time unit and the initial balance of the bucket. It calculates the expected amount
     * to be released based on this proportion and then subtracts any amount already released to find the unreleased portion.
     *
     * Edge cases:
     *   1. If the initial balance of the current bucket (`initialBalance_`) is 0, or if the remaining balance
     *      (`remainingBalance_`) is 0, the function returns 0, indicating no tokens are pending release.
     *   2. If the percentage of the time unit elapsed (`percentageElapsed_`) is 0, it also returns 0, implying
     *      that the time unit has just begun, and no tokens are due for release yet.
     *
     * @return The amount of tokens that are due for release but haven't been released yet.
     */
    function _calculateUnreleasedAmount() private view returns (uint256) {
        uint256 initialBalance_ = _getInitialCurrentBucketBalance();
        uint256 remainingBalance_ = bucketBalances[getCurrentBucketIndex() % MAX_BUCKET_COUNT];

        if (initialBalance_ == 0 || remainingBalance_ == 0) {
            return 0;
        }

        uint256 currentTimestamp_ = block.timestamp;
        uint256 timeUnit_ = timeUnit;

        uint256 percentageElapsed_ = ((currentTimestamp_ % timeUnit_) * PRECISION) / timeUnit_;

        if (percentageElapsed_ == 0) {
            return 0;
        }

        uint256 shouldBeReleasedAmount_ = (initialBalance_ * percentageElapsed_) / PRECISION;
        uint256 alreadyReleasedAmount_ = initialBalance_ - remainingBalance_;

        return shouldBeReleasedAmount_ - alreadyReleasedAmount_;
    }
}
