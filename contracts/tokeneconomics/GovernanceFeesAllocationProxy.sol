// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../common/Globals.sol";

import "../interfaces/IContractRegistry.sol";

import "../governance/IParameters.sol";

/**
 * @title Governance Fees Allocation Proxy
 * @notice This contract is responsible for the allocation of governance fees to predefined beneficiaries.
 * It allows for the distribution of rewards based on specified shares.
 */
contract GovernanceFeesAllocationProxy is Initializable {
    IContractRegistry private registry;

    /**
     * @notice Array of registry keys representing the beneficiaries.
     */
    string[] public beneficiaries;

    /**
     * @notice Array of share percentages corresponding to each beneficiary.
     * @dev These percentages determine the proportion of total allocatable balance each beneficiary receives.
     */
    uint256[] public sharePercentages;

    /**
     * @notice Emitted when rewards are allocated to beneficiaries.
     * @param amount The total amount of rewards distributed to the beneficiaries.
     */
    event Allocated(uint256 amount);

    receive() external payable {}

    /**
     * @notice Initializes the contract with a set of predefined beneficiaries and their corresponding shares.
     * @param contractRegistry_ The address of the contract registry.
     */
    function __GovernanceFeesAllocationProxy_init(address contractRegistry_) external initializer {
        registry = IContractRegistry(contractRegistry_);

        beneficiaries.push(RKEY__ROOT_NODE_REWARD_PROXY);
        sharePercentages.push((getDecimal() * 40) / 100);

        beneficiaries.push(RKEY__Q_HOLDER_REWARD_PROXY);
        sharePercentages.push((getDecimal() * 40) / 100);

        beneficiaries.push(RKEY__VALIDATION_REWARD_PROXY);
        sharePercentages.push((getDecimal() * 20) / 100);
    }

    /**
     * @notice Allocates the contract's balance to the beneficiaries based on their share percentages.
     */
    function allocate() external {
        string[] memory beneficiaries_ = beneficiaries;
        uint256[] memory shareValues_ = sharePercentages;

        uint256 totalWeight_ = getDecimal();
        uint256 totalAllocatableBalance_ = address(this).balance;

        uint256 totalAllocation_;

        for (uint256 i = 0; i < beneficiaries_.length; ++i) {
            uint256 amount_ = (totalAllocatableBalance_ * shareValues_[i]) / totalWeight_;
            totalAllocation_ += amount_;

            address payable beneficiary_ = payable(registry.mustGetAddress(beneficiaries_[i]));

            (bool success, ) = beneficiary_.call{value: amount_}("");
            require(success, "[QEC-011001]-Failed to transfer the amount, allocation failed.");
        }

        emit Allocated(totalAllocation_);
    }
}
