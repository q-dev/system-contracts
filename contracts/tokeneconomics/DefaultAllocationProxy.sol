// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../common/Globals.sol";
import "../interfaces/IContractRegistry.sol";
import "../governance/IParameters.sol";

/**
 * @title Default Allocation Proxy
 * @notice Performs allocation of rewards
 */
contract DefaultAllocationProxy is Initializable {
    IContractRegistry private registry;

    /**
     * @dev Registry keys of beneficiaries.
     */
    string[] public beneficiaries;

    /**
     * @dev Constitution param names of beneficiaries' shares.
     */
    string[] public shares;

    /**
     * @notice event to notify about allocation of rewards
     * @param amount that was transferred to beneficiaries
     */
    event Allocated(uint256 amount);

    constructor() {}

    /* solium-disable-next-line */
    receive() external payable {}

    function initialize(
        address _registry,
        string[] memory _beneficiaries,
        string[] memory _shares
    ) external initializer {
        require(
            _beneficiaries.length == _shares.length,
            "[QEC-011000]-Number of beneficiaries should be the same as number of shares."
        );
        beneficiaries = _beneficiaries;
        shares = _shares;

        registry = IContractRegistry(_registry);
    }

    /**
     * @notice performs default allocation procedure
     */
    function allocate() external {
        IParameters _parameters = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        string[] memory _shareParamNames = shares;
        uint256[] memory _shareValues = new uint256[](_shareParamNames.length);
        uint256 _totalWeight;

        for (uint256 i = 0; i < _shareParamNames.length; i++) {
            _shareValues[i] = _parameters.getUint(_shareParamNames[i]);
            _totalWeight += _shareValues[i];
        }

        string[] memory _beneficiaries = beneficiaries;
        address payable beneficiary;
        uint256 _totalAllocation;
        uint256 balance = address(this).balance;

        for (uint256 i = 0; i < _beneficiaries.length; i++) {
            uint256 amount = (balance * _shareValues[i]) / _totalWeight;
            _totalAllocation += amount;

            beneficiary = payable(registry.mustGetAddress(_beneficiaries[i]));
            (bool success, ) = beneficiary.call{value: amount}("");
            require(success, "[QEC-011001]-Failed to transfer the amount, allocation failed.");
        }

        emit Allocated(_totalAllocation);
    }
}
