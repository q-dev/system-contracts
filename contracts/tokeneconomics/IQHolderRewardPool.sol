// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

/**
 * @title QHolderRewardPool interface
 * @notice You can use this interface to create your own QHolderRewardPool contracts
 */
interface IQHolderRewardPool {
    /**
     * @notice Reward withdrawal request
     * @dev should be called from QVault contract only
     * @param _amount Amount of funds requested
     * and 0 if false
     * @return current reward, or will fail if balance < _rewardAmount
     */
    function requestRewardTransfer(uint256 _amount) external returns (uint256);
}
