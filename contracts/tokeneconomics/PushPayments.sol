// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

/**
 * @notice Supportive contract for secure push payments
 */
contract PushPayments is Initializable {
    mapping(address => uint256) balances;

    constructor() {}

    /**
     * @notice Tries a push payment with maximum gas stipend (30000 gas)
     *         If the payment fails, the amount and can be pulled by the recipient.
     * @dev the maximum of 30000 gas is considered future proof for EOA recipients
     *      and for contracts with limited fallback logic (like emit event)
     * @param _account receiver.
     * @return the success of the operation.
     */
    function safeTransferTo(address _account) external payable returns (bool) {
        bool _success;
        assembly {
            _success := call(30000, _account, callvalue(), 0, 0, 0, 0)
        }
        if (!_success) {
            _depositTo(_account);
        }

        return _success;
    }

    /**
     * @notice Allows to pull the entire balance that was aggregated via failed `safeTransferTo`
     */
    function withdraw() external {
        address _account = msg.sender;
        uint256 _amount = balanceOf(_account);
        require(_amount > 0, "[QEC-033001]-The caller does not have any balance to withdraw.");

        balances[_account] = 0;
        (bool _success, ) = payable(_account).call{value: _amount}("");
        require(_success, "[QEC-033002]-Transfer to the withdrawal caller failed.");
    }

    /**
     * @notice Get the entire balance that was aggregated via failed `safeTransferTo` the given address
     * @param _account given address
     * @return the entire balance that was aggregated via failed `safeTransferTo` the given address
     */
    function balanceOf(address _account) public view returns (uint256) {
        return balances[_account];
    }

    function _depositTo(address _account) internal {
        balances[_account] += msg.value;
    }
}
