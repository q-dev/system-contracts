// SPDX-License-Identifier: LGPL-3.0-or-later
// solium-disable linebreak-style
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

contract WithdrawAddresses is OwnableUpgradeable {
    struct AccountData {
        address addr;
        bool isFinalized;
    }
    mapping(address => AccountData) public map;

    function initialize() external initializer {
        __Ownable_init();
    }

    function change(address _alias) external {
        AccountData storage account = map[msg.sender];
        require(!account.isFinalized, "[QEC-042000]-Account is finalized.");
        account.addr = _alias;
    }

    function finalize(address _main, address _alias) external onlyOwner {
        AccountData storage _account = map[_main];
        require(_account.addr == _alias, "[QEC-042001]-Finalizing alias not correspond to mapping's value.");
        _account.isFinalized = true;
    }

    function resolve(address _main) external view returns (address) {
        AccountData storage _account = map[_main];
        if (_account.isFinalized) return _account.addr;
        else return _main;
    }
}
