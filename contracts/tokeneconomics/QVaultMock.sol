// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./QVault.sol";

contract QVaultMock is QVault {
    constructor() {}

    function testListOfDelegatedTo(address _delegator) public view returns (address[] memory) {
        return accounts[_delegator].delegationInfo.delegatedTo.getAddresses();
    }

    function testTotalDelegatedStake(address _delegator) public view returns (uint256) {
        return accounts[_delegator].delegationInfo.totalDelegatedStake;
    }

    function testValidatorsActualStakes(address _delegator, address _validator) public view returns (uint256) {
        return accounts[_delegator].delegationInfo.delegatedAmounts[_validator].actualStake;
    }

    function testValidatorsNormalizedStakes(address _delegator, address _validator) public view returns (uint256) {
        return accounts[_delegator].delegationInfo.delegatedAmounts[_validator].normalizedStake;
    }
}
