// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../governance/validators/Validators.sol";
import "../common/FullMath.sol";
import "../interfaces/IContractRegistry.sol";
import "./ValidationRewardPools.sol";
import "../tokeneconomics/PushPayments.sol";

/**
 * @title Validation Reward Proxy
 * @notice Performs allocation of rewards for validators
 */
contract ValidationRewardProxy is Initializable {
    using FullMath for uint256;

    struct PayInformation {
        uint256 balance;
        uint256 nBeneficiaries;
        uint256 totalStake;
        uint256 qsv;
        uint256 delegatorReward;
        uint256 validatorReward;
    }

    IContractRegistry private registry;

    /**
     * @notice event to notify about allocation of rewards for validators
     * @param amount that was transferred to validators
     */
    event Allocated(uint256 amount);

    constructor() {}

    receive() external payable {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice performs default allocation procedure
     */
    function allocate() external {
        PayInformation memory p;

        p.balance = address(this).balance;
        if (p.balance == 0) {
            // everyone will receive nothing anyway
            return;
        }

        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));

        p.nBeneficiaries =
            constitution.getUint("constitution.maxNValidators") +
            constitution.getUint("constitution.maxNStandbyValidators");

        Validators _validators = Validators(registry.mustGetAddress(RKEY__VALIDATORS));
        Validators.ValidatorAmount[] memory shortList = _validators.getValidatorShortList();

        // list may not be full
        if (shortList.length < p.nBeneficiaries) {
            p.nBeneficiaries = shortList.length;
        }

        for (uint256 i = 0; i < p.nBeneficiaries; ++i) {
            p.totalStake = p.totalStake + shortList[i].amount;
        }

        ValidationRewardPools _validationRewardPools = ValidationRewardPools(
            payable(registry.mustGetAddress(RKEY__VALIDATION_REWARDS_POOL))
        );

        PushPayments pushPayments = PushPayments(registry.mustGetAddress(RKEY__PUSH_PAYMENTS));

        uint256 decimal = getDecimal();
        uint256 _totalAllocation;
        for (uint256 i = 0; i < p.nBeneficiaries; ++i) {
            p.qsv = _validationRewardPools.getDelegatorsShare(shortList[i].validator);

            p.delegatorReward = p.balance.mulDiv(shortList[i].amount, p.totalStake).mulDiv(p.qsv, decimal);
            if (p.delegatorReward != 0) {
                _validationRewardPools.increase{value: p.delegatorReward}(shortList[i].validator);
                _totalAllocation += p.delegatorReward;
            }

            p.validatorReward = p.balance.mulDiv(shortList[i].amount, p.totalStake).mulDiv(decimal - p.qsv, decimal);
            if (p.validatorReward > 0) {
                pushPayments.safeTransferTo{value: p.validatorReward}(shortList[i].validator);
                _totalAllocation += p.validatorReward;
            }
        }

        emit Allocated(_totalAllocation);
    }
}
