// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "@dlsl/dev-modules/libs/data-structures/memory/Vector.sol";

/**
 * @title PaymentStretchingBuckets
 * @notice This contract is used to stretch payments over time.
 *
 * There are situations where large payments should be directed to the Q stakeholders. This is easily done by
 * transferring the amount to the default allocation proxy. However, it is desirable to stretch the payment over a
 * longer period of time, such that (i) temporary coincidences, e.g. of the validator ranking, have less effect on the
 * distribution of these payments and (ii) “accrual accounting-like” functionality is enabled.
 */
contract PaymentStretchingBuckets is Initializable {
    using Vector for Vector.UintVector;

    /**
     * @notice The maximum number of buckets that can be used to stretch payments.
     */
    uint256 public constant MAX_BUCKET_COUNT = 255;

    /**
     * @notice The address to which the payments are directed.
     */
    address public recipient;

    /**
     * @notice The amount that is available for allocation from past buckets.
     */
    uint256 public allocatableBalance;

    /**
     * @notice The period of time in seconds that represents a single bucket.
     */
    uint256 public timeUnit;

    /**
     * @notice The number of buckets that are created by default when a payment is made and numberOfBuckets is not specified.
     */
    uint256 public defaultBucketCount;

    /**
     * @notice The index of the earliest bucket that is not empty.
     */
    uint256 public firstNonEmptyBucketIndex;

    uint256[MAX_BUCKET_COUNT] public bucketBalances;

    event BucketsSwept(uint256 newAllocatableBalance);

    event Allocated(address indexed recipient, uint256 allocatedAmount);

    event PaymentReceived(
        address indexed sender,
        uint256 startingBucketIndex,
        uint256 numberOfBuckets,
        uint256 depositedAmount
    );

    constructor() initializer {}

    receive() external payable {
        deposit(defaultBucketCount);
    }

    /**
     * @notice Initializes the contract.
     * @param recipient_ The address to which the payments are directed.
     * @param timeUnit_ The period of time in seconds that represents a single bucket.
     * @param defaultBucketCount_ The number of buckets that are created by default when a payment is made and
     * numberOfBuckets is not specified.
     *
     * Sets the firstNonEmptyBucketIndex to the current bucket index.
     *
     * Requirements:
     *
     * - `defaultBucketCount_` must be less than or equal to MAX_BUCKET_COUNT.
     * - `timeUnit_` must be greater than zero.
     * - `recipient_` cannot be zero.
     */
    function PaymentStretchingBuckets__init(
        address recipient_,
        uint256 timeUnit_,
        uint256 defaultBucketCount_
    ) external initializer {
        require(defaultBucketCount_ <= MAX_BUCKET_COUNT, "[QEC-044000]-Default bucket count is too high.");
        require(timeUnit_ > 0, "[QEC-044001]-Allocation period cannot be zero.");
        require(recipient_ != address(0), "[QEC-044002]-Recipient cannot be zero.");

        recipient = recipient_;
        timeUnit = timeUnit_;
        defaultBucketCount = defaultBucketCount_;
        firstNonEmptyBucketIndex = getCurrentBucketIndex();
    }

    /**
     * @notice Deposits the amount to the contract.
     * @param numberOfBuckets_ The number of buckets to stretch the payment over.
     *
     * Requirements:
     *
     * - `numberOfBuckets_` must be greater than zero.
     * - `numberOfBuckets_` must be less than or equal to MAX_BUCKET_COUNT.
     * - `msg.value` must be greater than zero.
     */
    function deposit(uint256 numberOfBuckets_) public payable {
        require(numberOfBuckets_ > 0, "[QEC-044005]-Number of buckets cannot be zero.");
        require(
            numberOfBuckets_ < MAX_BUCKET_COUNT,
            "[QEC-044006]-Number of buckets cannot be greater than bucket limit."
        );

        uint256 depositedAmount_ = msg.value;

        require(depositedAmount_ > 0, "[QEC-044003]-Amount cannot be zero.");

        _stretchPayment(depositedAmount_, numberOfBuckets_);
    }

    /**
     * @notice Allocates all available amount to the recipient.
     */
    function allocate() external {
        _sweepOldBuckets();

        allocate(allocatableBalance);
    }

    /**
     * @notice Allocates the specified amount to the recipient.
     * @param amountToAllocate_ The amount to allocate.
     *
     * Requirements:
     *
     * - `allocatableAmount_` must be greater than or equal to `amountToAllocate_`.
     * - Allocation must succeed.
     */
    function allocate(uint256 amountToAllocate_) public {
        _sweepOldBuckets();

        uint256 allocatableAmount_ = allocatableBalance;

        require(
            allocatableAmount_ >= amountToAllocate_,
            "[QEC-044007]-Allocatable amount is less than requested amount."
        );

        allocatableBalance = allocatableAmount_ - amountToAllocate_;

        (bool success_, ) = recipient.call{value: amountToAllocate_}("");
        require(success_, "[QEC-044004]-Allocation failed");

        if (bucketBalances[firstNonEmptyBucketIndex % MAX_BUCKET_COUNT] == 0) {
            firstNonEmptyBucketIndex += 1;
        }

        emit Allocated(recipient, amountToAllocate_);
    }

    /**
     * @notice Returns the index of the current bucket.
     */
    function getCurrentBucketIndex() public view returns (uint256) {
        return block.timestamp / timeUnit;
    }

    /**
     * @notice Returns the amount that is available for allocation.
     */
    function getReleasedAmount() public view virtual returns (uint256) {
        uint256 currentBucketIndex_ = getCurrentBucketIndex();
        uint256 firstNonEmptyBucketIndex_ = firstNonEmptyBucketIndex;

        uint256 releasedAmount_ = allocatableBalance;

        if (currentBucketIndex_ == firstNonEmptyBucketIndex_) {
            return releasedAmount_;
        }

        uint256 bucketsLimit_ = MAX_BUCKET_COUNT;
        uint256 upperLimit_ = firstNonEmptyBucketIndex_ + bucketsLimit_;
        upperLimit_ = upperLimit_ > currentBucketIndex_ ? currentBucketIndex_ : upperLimit_;

        for (uint256 i = firstNonEmptyBucketIndex_; i < upperLimit_; ++i) {
            releasedAmount_ += bucketBalances[i % bucketsLimit_];
        }

        return releasedAmount_;
    }

    /**
     * @notice Returns the buckets in prettified form.
     */
    function getNormalizedBuckets() external view returns (uint256[] memory) {
        Vector.UintVector memory vector_ = Vector.newUint();

        uint256 bucketsLimit_ = MAX_BUCKET_COUNT;
        uint256 firstNonEmptyBucketIndex_ = firstNonEmptyBucketIndex;

        for (uint256 i = 0; i < bucketsLimit_; ++i) {
            uint256 currentBucketIndex_ = (firstNonEmptyBucketIndex_ + i) % bucketsLimit_;

            uint256 bucketBalance_ = bucketBalances[currentBucketIndex_];

            if (bucketBalance_ == 0) {
                break;
            }

            vector_.push(bucketBalance_);
        }

        return vector_.toArray();
    }

    /**
     * @notice Stretches the payment over the specified number of buckets.
     * @param depositedAmount_ The amount that was deposited.
     * @param numberOfBuckets_ The number of buckets to stretch the payment over.
     *
     * Before stretching the payment, the buckets that are available for allocation are cleaned up.
     *
     * Handles the rounding error by adding the remainder to the last bucket.
     */
    function _stretchPayment(uint256 depositedAmount_, uint256 numberOfBuckets_) internal virtual {
        _sweepOldBuckets();

        uint256 stretchedAmount_ = depositedAmount_ / numberOfBuckets_;
        uint256 startingBucketIndex_ = _getStartingBucketIndexToStretch();
        uint256 bucketsLimit_ = MAX_BUCKET_COUNT;

        for (uint256 i = 0; i < numberOfBuckets_ - 1; ++i) {
            uint256 currentBucketIndex_ = (startingBucketIndex_ + i) % bucketsLimit_;

            bucketBalances[currentBucketIndex_] += stretchedAmount_;
        }

        uint256 lastBucketIndex_ = (startingBucketIndex_ + numberOfBuckets_ - 1) % bucketsLimit_;
        bucketBalances[lastBucketIndex_] += depositedAmount_ - stretchedAmount_ * (numberOfBuckets_ - 1);

        emit PaymentReceived(msg.sender, startingBucketIndex_, numberOfBuckets_, depositedAmount_);
    }

    /**
     * @notice Cleans up the buckets that are available for allocation, updates the earliest bucket index and
     * updates the allocatableBalance.
     */
    function _sweepOldBuckets() internal virtual {
        uint256 currentBucketIndex_ = getCurrentBucketIndex();
        uint256 firstNonEmptyBucketIndex_ = firstNonEmptyBucketIndex;

        if (currentBucketIndex_ == firstNonEmptyBucketIndex_) {
            return;
        }

        firstNonEmptyBucketIndex = currentBucketIndex_;

        uint256 bucketsLimit_ = MAX_BUCKET_COUNT;
        uint256 amountToAllocate_ = allocatableBalance;

        for (uint256 i = firstNonEmptyBucketIndex_; i < currentBucketIndex_; ++i) {
            uint256 bucketIndex_ = i % bucketsLimit_;
            uint256 bucketBalance_ = bucketBalances[bucketIndex_];

            if (bucketBalance_ == 0) {
                break;
            }

            amountToAllocate_ += bucketBalance_;

            bucketBalances[bucketIndex_] = 0;
        }

        allocatableBalance = amountToAllocate_;

        emit BucketsSwept(amountToAllocate_);
    }

    /**
     * @notice Determines the starting bucket index for stretching the payment.
     */
    function _getStartingBucketIndexToStretch() internal view virtual returns (uint256) {
        return firstNonEmptyBucketIndex;
    }
}
