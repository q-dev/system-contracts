// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

struct VotingLockInfo {
    uint256 lockedAmount;
    uint256 lockedUntil;
    uint256 pendingUnlockAmount;
    uint256 pendingUnlockTime;
}

/**
 * @title TokenLock interface
 * @notice You can use this interface to implement the token lock property in your contracts
 */
interface ITokenLock {
    /**
     * @notice lock is used to lock a specific number of user tokens
     * @param _amount The amount of locked tokens
     */
    function lock(uint256 _amount) external;

    /**
     * @notice announceUnlock is used to announce the unlock of a certain number of user tokens
     * @param _amount The amount of tokens to announce the unlock
     */
    function announceUnlock(uint256 _amount) external;

    /**
     * @notice unlock is used to unlock locked user tokens
     * @param _amount The amount of tokens to unlock
     */
    function unlock(uint256 _amount) external;

    /**
     * @notice getLockInfo is used to get information about the user's locked tokens
     *
     * @return VotingLockInfo structure object with information about the user's locked tokens
     */
    function getLockInfo() external view returns (VotingLockInfo calldata);
}
