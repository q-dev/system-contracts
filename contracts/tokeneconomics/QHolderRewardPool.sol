// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IQHolderRewardPool.sol";
import "./QVault.sol";
import "../governance/IParameters.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IContractRegistry.sol";

contract QHolderRewardPool is IQHolderRewardPool, Initializable {
    IContractRegistry private registry;

    event RewardTransferedToQVault(uint256 _amount);

    /**
     * @notice Restricts only QVault can interact
     */
    modifier onlyQVault() {
        require(
            msg.sender == _getQVaultAddress(),
            "[QEC-015000]-Permission denied - only the QVault contract has access."
        );
        _;
    }

    constructor() {}

    /* solium-disable-next-line */
    receive() external payable {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Reward withdrawal request
     * @dev should be called from QVault contract only
     * @param _rewardAmount Amount of funds requested
     * and 0 if false
     * @return current reward, or will fail if balance < _rewardAmount
     */
    function requestRewardTransfer(uint256 _rewardAmount) external override onlyQVault returns (uint256) {
        require(_rewardAmount <= address(this).balance, "[QEC-015001]-Pool balance is not enough for request.");
        QVault(_getQVaultAddress()).depositFromPool{value: _rewardAmount}();

        emit RewardTransferedToQVault(_rewardAmount);
        return _rewardAmount;
    }

    function _getQVaultAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__QVAULT);
    }
}
