// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IContractRegistry.sol";
import "../governance/validators/Validators.sol";
import "./QVault.sol";
import "../common/Globals.sol";
import "../common/CompoundRateKeeper.sol";
import "../common/CompoundRateKeeperFactory.sol";

/**
 * @title Validation Reward Pools
 * @notice Used for keeping validators rewards
 */
contract ValidationRewardPools is Initializable {
    struct PoolInfo {
        uint256 poolBalance;
        uint256 reservedForClaims;
        uint256 aggregatedNormalizedStake;
        uint256 delegatedStake;
        uint256 compoundRate;
        uint256 lastUpdateOfCompoundRate;
        uint256 delegatorsShare;
    }

    struct ValidatorProperties {
        uint256 balance;
        uint256 reservedForClaim;
        uint256 delegatorsShare;
        uint256 aggregatedNormalizedStake;
        CompoundRateKeeper compoundRate;
    }

    IContractRegistry registry;

    mapping(address => ValidatorProperties) public validatorsProperties;

    uint256 private crUpdateMinimumBase;

    event RewardTransferedToQVault(address indexed _claimerAddress, uint256 _rewardAmount);
    event UpdateRate(ValidatorProperties _v, uint256 _oldRate, uint256 _newRate, uint256 _inc);
    event DelegatorsShareChanged(address indexed _validatorAddress, uint256 _newDelegatorsShare);

    /**
     * @notice Restricts only the QVault contract can interact
     */
    modifier onlyQVault() {
        require(
            msg.sender == _getQVaultAddress(),
            "[QEC-016001]-Permission denied - only the QVault contract has access."
        );
        _;
    }

    constructor() {}

    function initialize(address _registry, uint256 _crUpdateMinimumBase) external initializer {
        registry = IContractRegistry(_registry);
        crUpdateMinimumBase = _crUpdateMinimumBase;
    }

    /**
     * @notice For usage between contracts.
     */
    function addCompoundRateKeeper(address _validator) external {
        if (validatorsProperties[_validator].compoundRate == CompoundRateKeeper(address(0))) {
            validatorsProperties[_validator].compoundRate = _getRateKeeperFactory().create();
        }
    }

    /**
     * @notice Increases validator's pool balance
     * @param _validator Validator's address
     */
    function increase(address _validator) external payable {
        require(msg.value != 0, "[QEC-016000]-Invalid value to increase the pool balance.");
        validatorsProperties[_validator].balance += (msg.value);
    }

    /**
     * @notice Transfers validator's reward to the QVault
     * @param _validator Validator's address
     * @param _rewardAmount Claimable reward
     * @return true if everything went well
     */
    function requestRewardTransfer(address _validator, uint256 _rewardAmount) external onlyQVault returns (bool) {
        ValidatorProperties storage validatorProperties = validatorsProperties[_validator];

        validatorProperties.balance -= _rewardAmount;
        validatorProperties.reservedForClaim -= _rewardAmount;

        QVault(_getQVaultAddress()).depositFromPool{value: _rewardAmount}();

        emit RewardTransferedToQVault(_validator, _rewardAmount);
        return true;
    }

    /**
     * @notice Updates compound rate for validator
     * @param _validator address to update rate for
     * @return new compound rate
     */
    function updateValidatorsCompoundRate(address _validator) external returns (uint256) {
        uint256 _newRate = _updateCompoundRate(_validator);
        Validators(_getValidatorsAddress()).refreshDelegatedStake(_validator);

        return _newRate;
    }

    /**
     * @notice Sets new delegators share value
     * @param _newDelegatorsShare New delegators share value
     */
    function setDelegatorsShare(uint256 _newDelegatorsShare) external {
        require(
            Validators(_getValidatorsAddress()).isInLongList(msg.sender),
            "[QEC-016005]-The caller is not a validator, failed to set delegators share."
        );

        require(
            0 <= _newDelegatorsShare && _newDelegatorsShare <= getDecimal(),
            "[QEC-016003]-Invalid delegator share parameter."
        );

        uint256 _oldDelegatorsShare = validatorsProperties[msg.sender].delegatorsShare;
        if (_oldDelegatorsShare != _newDelegatorsShare) {
            validatorsProperties[msg.sender].delegatorsShare = _newDelegatorsShare;
            emit DelegatorsShareChanged(msg.sender, _newDelegatorsShare);
        }
    }

    /**
     * @notice Increases validator's pool balance and reserved for claim amount
     * @param _validator Validator's address
     */
    function reserveAdditionalFunds(address _validator) external payable {
        require(msg.value != 0, "[QEC-016000]-Invalid value to increase the pool balance.");
        validatorsProperties[_validator].balance += msg.value;
        validatorsProperties[_validator].reservedForClaim += msg.value;
    }

    /**
     * @notice Returns the user's delegators share
     * @param _addr User's address
     * @return Delegators share
     */
    function getDelegatorsShare(address _addr) external view returns (uint256) {
        return validatorsProperties[_addr].delegatorsShare;
    }

    /**
     * @notice Returns the validator's pool balance
     * @param _validator Validator's address
     * @return Pool balance
     */
    function getBalance(address _validator) external view returns (uint256) {
        return validatorsProperties[_validator].balance;
    }

    /**
     * @notice For usage between contracts.
     */
    function addAggregatedNormalizedStake(address _validator, uint256 _stake) public onlyQVault {
        validatorsProperties[_validator].aggregatedNormalizedStake += _stake;
    }

    /**
     * @notice For usage between contracts.
     */
    function subAggregatedNormalizedStake(address _validator, uint256 _stake) public onlyQVault {
        validatorsProperties[_validator].aggregatedNormalizedStake -= _stake;
    }

    /**
     * @notice Returns detailed information about the validator's pool
     * @param _validator Validator's address
     * @return PoolInfo struct
     */
    function getPoolInfo(address _validator) public view returns (PoolInfo memory) {
        ValidatorProperties memory _validatorProperties = validatorsProperties[_validator];
        address _rateKeeperAddress = _getRateKeeperAddress(_validator);
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_rateKeeperAddress);

        PoolInfo memory _poolInfo;

        _poolInfo.lastUpdateOfCompoundRate = _rateKeeper.getLastUpdate();
        _poolInfo.compoundRate = _rateKeeper.getCurrentRate();
        _poolInfo.delegatedStake = Validators(_getValidatorsAddress()).getValidatorDelegatedStake(_validator);
        _poolInfo.aggregatedNormalizedStake = _validatorProperties.aggregatedNormalizedStake;
        _poolInfo.delegatorsShare = _validatorProperties.delegatorsShare;
        _poolInfo.poolBalance = _validatorProperties.balance;
        _poolInfo.reservedForClaims = _validatorProperties.reservedForClaim;

        return _poolInfo;
    }

    /**
     * @notice Returns the validator's compound rate
     * @param _validator Validator's address
     * @return Compound rate
     */
    function getCompoundRate(address _validator) public view returns (uint256) {
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_getRateKeeperAddress(_validator));
        uint256 _currentRate = _rateKeeper.getCurrentRate();
        return _currentRate;
    }

    /**
     * @notice Retrieves delegate stake balance
     * @param _validator address to get balance for
     * @return balance for validator
     */
    function getDelegatedStake(address _validator) public view returns (uint256) {
        return getDenormalizedAmount(_validator, validatorsProperties[_validator].aggregatedNormalizedStake);
    }

    /**
     * @notice Returns the time of the last update of the validator's compound rate
     * @param _validator Validator's address
     * @return Time of the last update
     */
    function getLastUpdateOfCompoundRate(address _validator) public view returns (uint256) {
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_getRateKeeperAddress(_validator));
        uint256 _lastUpdate = _rateKeeper.getLastUpdate();
        return _lastUpdate;
    }

    /**
     * @notice Returns normalized amount
     * @param _validator Validator's address
     * @param _targetAmount amount to normalize
     * @return Normalized amount
     */
    function getNormalizedAmount(address _validator, uint256 _targetAmount) public view returns (uint256) {
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_getRateKeeperAddress(_validator));
        return _rateKeeper.normalizeAmount(_targetAmount);
    }

    /**
     * @notice Returns denormalized amount
     * @param _validator Validator's address
     * @param _normalizedAmount amount to denormalize
     * @return Denormalized amount
     */
    function getDenormalizedAmount(address _validator, uint256 _normalizedAmount) public view returns (uint256) {
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_getRateKeeperAddress(_validator));
        return _rateKeeper.denormalizeAmount(_normalizedAmount);
    }

    function _updateCompoundRate(address _validator) private returns (uint256) {
        ValidatorProperties memory _validatorProperties = validatorsProperties[_validator];
        CompoundRateKeeper _rateKeeper = CompoundRateKeeper(_getRateKeeperAddress(_validator));

        uint256 _oldRate = _rateKeeper.getCurrentRate();
        uint256 _oldDelegatedStake = _rateKeeper.denormalizeAmount(_validatorProperties.aggregatedNormalizedStake);
        uint256 available = _validatorProperties.balance - _validatorProperties.reservedForClaim;
        if (available <= 1 || _oldDelegatedStake < crUpdateMinimumBase) {
            return _oldRate;
        }
        uint256 _newRate = _rateKeeper.updateWithDistributableAmount(_oldDelegatedStake, available);
        uint256 _newDelegatedStake = _rateKeeper.denormalizeAmount(_validatorProperties.aggregatedNormalizedStake);
        uint256 _actualIncrease = _newDelegatedStake - _oldDelegatedStake;

        if (_validatorProperties.aggregatedNormalizedStake > 0 && _actualIncrease > 0) {
            uint256 _reservedForClaims = _actualIncrease + _validatorProperties.reservedForClaim;

            require(
                _reservedForClaims <= _validatorProperties.balance,
                "[QEC-016002]-There is not enough pool balance, failed to update the compound rate."
            );

            validatorsProperties[_validator].reservedForClaim = _reservedForClaims;
        }

        emit UpdateRate(validatorsProperties[_validator], _oldRate, _newRate, _actualIncrease);
        return _newRate;
    }

    function _getRateKeeperFactory() private view returns (CompoundRateKeeperFactory) {
        return CompoundRateKeeperFactory(registry.mustGetAddress(RKEY__CR_KEEPER_FACTORY));
    }

    function _getRateKeeperAddress(address _validator) private view returns (address) {
        address _rateKeeperAddress = address(validatorsProperties[_validator].compoundRate);

        require(
            _rateKeeperAddress != address(0),
            "[QEC-016007]-CompoundRateKeeper not initialized for given validator."
        );

        return _rateKeeperAddress;
    }

    function _getValidatorsAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VALIDATORS);
    }

    function _getQVaultAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__QVAULT);
    }
}
