// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../governance/rootNodes/Roots.sol";
import "../interfaces/IContractRegistry.sol";
import "../tokeneconomics/PushPayments.sol";

/**
 * @title RootNodeReward Proxy
 * @notice Performs allocation of rewards for root nodes
 */
contract RootNodeRewardProxy is Initializable {
    IContractRegistry private registry;

    /**
     * @notice event to notify about allocation of rewards for root nodes
     * @notice amount that was transferred to Root Nodes
     */
    event Allocated(uint256 amount);

    constructor() {}

    receive() external payable {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice performs default allocation procedure
     */
    function allocate() external {
        address[] memory members = IRoots(registry.mustGetAddress(RKEY__ROOT_NODES)).getMembers();
        uint256 amount = address(this).balance / members.length;

        PushPayments pushPayments = PushPayments(registry.mustGetAddress(RKEY__PUSH_PAYMENTS));

        address payable toRoot;
        uint256 _totalAllocation;

        for (uint64 i = 0; i < members.length; ++i) {
            toRoot = payable(members[i]);
            pushPayments.safeTransferTo{value: amount}(toRoot);
            _totalAllocation += amount;
        }

        emit Allocated(_totalAllocation);
    }
}
