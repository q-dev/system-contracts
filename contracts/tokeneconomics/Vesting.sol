// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./ATimeLockBase.sol";
import "./ITokenLock.sol";
import "../interfaces/IContractRegistry.sol";
import "../interfaces/IVotingWeightProxy.sol";
import "../common/Globals.sol";

contract Vesting is Initializable, ITokenLock, ATimeLockBase {
    mapping(address => uint256) balances;

    IContractRegistry private registry;

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice lock is used to increase the user's current locked tokens
     * @dev For token lock, the corresponding proxy contract method is used.
     * When trying to lock an amount more than is currently available, we get an error 031004
     * @param _amount The amount of locked tokens
     */
    function lock(uint256 _amount) external override {
        _lock(msg.sender, _amount);
    }

    /**
     * @notice Withdraws Q from vesting
     * @param _amount amount of Q to withdraw
     */
    function withdraw(uint256 _amount) external {
        address _account = msg.sender;
        _subFromBalance(_account, _amount);

        (bool _success, ) = payable(_account).call{value: _amount}("");
        require(_success, "[QEC-031003]-Failed to withdraw from vesting.");
    }

    /**
     * @notice announceUnlock is used to announce unlocking of locked tokens
     * @dev To announce the unlocking of tokens, the corresponding proxy contract method is used
     * @param _amount The amount of tokens for which unlocking will be announced
     */
    function announceUnlock(uint256 _amount) external override {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.announceUnlock(msg.sender, _amount, 0);
    }

    /**
     * @notice unlock is used for the final unlocking of tokens announced for unlocking
     * @dev To unlock tokens, the corresponding method of the proxy contract is used
     * @param _amount Amount of tokens to be unlocked
     */
    function unlock(uint256 _amount) external override {
        _unlock(msg.sender, _amount);
    }

    /**
     * @notice getLockInfo is used to get information about the user's locked tokens
     * @dev We receive error 028001 if this contract is not the source of the token lock
     * @return information about the current lock of tokens as an object of the VotingLockInfo structure
     */
    function getLockInfo() external view override returns (VotingLockInfo memory) {
        return _getLockInfo();
    }

    /**
     * @notice Retrieves user balance
     * @param _account address to get user balance for
     * @return user balance
     */
    function balanceOf(address _account) public view returns (uint256) {
        return balances[_account];
    }

    /**
     * @notice Retrieves withdrawable user balance
     * @param _account address to get withdrawable user balance for
     * @return withdrawable user balance
     */
    function withdrawableBalanceOf(address _account) public view returns (uint256) {
        VotingLockInfo memory _votingLockInfo = _getLockInfo(_account);
        uint256 _minimumBalance = getMinimumBalance(_account, block.timestamp);

        if (_votingLockInfo.lockedAmount > _minimumBalance) {
            return balanceOf(_account) - _votingLockInfo.lockedAmount;
        }

        return balanceOf(_account) - _minimumBalance;
    }

    function _onTimeLockedDeposit(address _account, uint256 _amount) internal override {
        balances[_account] += _amount;
        _lock(_account, _amount);
    }

    function _lock(address _owner, uint256 _amount) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        VotingLockInfo memory _votingLockInfo = _getLockInfo(_owner);
        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;

        require(
            balanceOf(_owner) - _totalLockedAmount >= _amount,
            "[QEC-031004]-The lock amount must not exceed the available balance."
        );

        _votingWeightProxy.lock(_owner, _amount);
    }

    function _unlock(address _owner, uint256 _amount) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.unlock(_owner, _amount);
    }

    function _subFromBalance(address _owner, uint256 _amount) private returns (bool) {
        if (_amount == 0) return false;

        require(balanceOf(_owner) >= _amount, "[QEC-031001]-Insufficient balance for vesting withdrawal.");

        require(
            balanceOf(_owner) - _amount >= getMinimumBalance(_owner, block.timestamp),
            "[QEC-031002]-Vesting withdrawal prevented by timelock(s)."
        );

        VotingLockInfo memory _votingLockInfo = _getLockInfo(_owner);

        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;
        uint256 _nonLockedAmount = balanceOf(_owner) - _totalLockedAmount;

        if (_nonLockedAmount < _amount) {
            _unlock(_owner, _amount - _nonLockedAmount);
        }

        balances[_owner] -= _amount;

        return true;
    }

    function _getLockInfo() private view returns (VotingLockInfo memory) {
        return _getLockInfo(msg.sender);
    }

    function _getLockInfo(address _account) private view returns (VotingLockInfo memory) {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        return _votingWeightProxy.getLockInfo(address(this), _account);
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }
}
