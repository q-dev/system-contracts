// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../common/Globals.sol";

import "../governance/IPanel.sol";
import "../governance/IParameters.sol";

import "../interfaces/IContractRegistry.sol";

contract SystemReserve is Initializable {
    IContractRegistry private _registry;

    string[] private _eligibleContractKeys;

    bool private _systemPaused;

    uint256 private _coolDownPhase;
    uint256 private _availableAmount;

    modifier onlyEligibleContract() {
        bool isFound_;

        uint256 eligibleContractKeysLength_ = _eligibleContractKeys.length;
        for (uint256 i = 0; i < eligibleContractKeysLength_; ++i) {
            if (_registry.mustGetAddress(_eligibleContractKeys[i]) == msg.sender) {
                isFound_ = true;

                break;
            }
        }
        require(isFound_, "[QEC-018000]-Permission denied - only eligible contracts have access.");
        _;
    }

    modifier onlyEPQFIorRoot() {
        require(
            IExperts(_registry.mustGetAddress(RKEY__EPQFI_MEMBERSHIP)).isMember(msg.sender) ||
                IRoots(_registry.mustGetAddress(RKEY__ROOT_NODES)).isMember(msg.sender),
            "[QEC-018001]-Permission denied - only root nodes and EPQFI experts have access."
        );
        _;
    }

    modifier onlyNotPausedState() {
        require(!_systemPaused, "[QEC-018002]-The system reserve contract is paused..");
        _;
    }

    constructor() {}

    receive() external payable {}

    function initialize(address registry_, string[] memory keys_) external initializer {
        _registry = IContractRegistry(registry_);
        _eligibleContractKeys = keys_;

        _coolDownPhase = block.timestamp + _getWindowTime();
        _availableAmount = _getThreshold();
    }

    function addQEURStableCoin() external {
        require(_eligibleContractKeys.length == 1, "[QEC-018005]-The QEUR stable coin has already been added.");

        _eligibleContractKeys.push("defi.QEUR.systemDebtAuction");
    }

    function setPauseState(bool state_) external onlyEPQFIorRoot {
        _systemPaused = state_;
    }

    function withdraw(uint256 amount_) external onlyEligibleContract onlyNotPausedState returns (bool) {
        _updateCoolDownPhase();

        if (address(this).balance < amount_ || amount_ == 0) {
            return false;
        }

        require(_availableAmount >= amount_, "[QEC-018003]-Insufficient funds available for withdrawal.");

        _availableAmount -= amount_;

        (bool success_, ) = payable(msg.sender).call{value: amount_}("");
        require(success_, "[QEC-018004]-Transfer of the withdrawal amount failed.");

        return true;
    }

    function getEligibleContractKeys() external view returns (string[] memory) {
        return _eligibleContractKeys;
    }

    function getSystemPaused() external view returns (bool) {
        return _systemPaused;
    }

    function getCoolDownPhase() external view returns (uint256) {
        return _coolDownPhase;
    }

    function getAvailableAmount() external view returns (uint256) {
        return _availableAmount;
    }

    function _updateCoolDownPhase() private {
        if (_coolDownPhase < block.timestamp) {
            _coolDownPhase = block.timestamp + _getWindowTime();

            _availableAmount = _getThreshold();
        }
    }

    function _getWindowTime() private view returns (uint256) {
        return IParameters(_getEPDRParametersAddress()).getUint("governed.EPQFI.reserveCoolDownP");
    }

    function _getThreshold() private view returns (uint256) {
        return IParameters(_getEPDRParametersAddress()).getUint("governed.EPQFI.reserveCoolDownThreshold");
    }

    function _getEPDRParametersAddress() private view returns (address) {
        return _registry.mustGetAddress(RKEY__EPQFI_PARAMETERS);
    }
}
