// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IContractRegistry.sol";
import "../interfaces/IVotingWeightProxy.sol";
import "./IVoting.sol";

/**
 * @title Voting weight proxy contract
 * @notice VotingWeightProxy is used to store and record information
 * about locked tokens of users for all sources of token locks
 */
contract VotingWeightProxy is Initializable, IVotingWeightProxy {
    IContractRegistry private registry;

    string[] private tokenLockSourcesKeys;
    string[] private votingContractsKeys;

    // TLS => User address => Voting lock info
    mapping(address => mapping(address => VotingLockInfo)) public lockInfos;

    // user address => user's locked until need
    mapping(address => uint256) lockedUntil;

    // User address => Voting delegation info
    mapping(address => VotingDelegationInfo) public delegationInfos;

    event LockedAmountChanged(address _tokenLockSource, address indexed _who, uint256 _newLockedAmount);
    event PendingUnlockAmountChanged(address _tokenLockSource, address indexed _who, uint256 _newPendingUnlockAmount);
    event PendingUnlockChanged(
        address _tokenLockSource,
        address indexed _who,
        uint256 _newPendingUnlockAmount,
        uint256 _newPendingUnlockTime
    );
    event NewVotingAgentAnnounced(address indexed _who, address _prevVotingAgent, address _newVotingAgent);
    event VotingAgentChanged(address indexed _who, address indexed _votingAgent, uint256 _delegatedAmount);

    modifier onlyEligibleContracts() {
        for (uint256 i = 0; i < tokenLockSourcesKeys.length; ++i) {
            if (registry.mustGetAddress(tokenLockSourcesKeys[i]) == msg.sender) {
                _;
                return;
            }
        }
        revert("[QEC-028007]-Permission denied - only the token lock sources have access.");
    }

    modifier onlyVotingContracts() {
        for (uint256 i = 0; i < votingContractsKeys.length; ++i) {
            if (registry.mustGetAddress(votingContractsKeys[i]) == msg.sender) {
                _;
                return;
            }
        }
        revert("[QEC-028010]-Permission denied - only voting contracts have access.");
    }

    constructor() {}

    function initialize(
        address _registry,
        string[] memory _tokenLockSourcesKeys,
        string[] memory _votingContractsKeys
    ) external initializer {
        registry = IContractRegistry(_registry);
        tokenLockSourcesKeys = _tokenLockSourcesKeys;
        votingContractsKeys = _votingContractsKeys;
    }

    /**
     * @notice lock is used to increase the currently locked tokens for a specific user
     *
     * @dev When trying to lock zero tokens, we get error 028002.
     * msg.sender is the source address of the token lock
     *
     * @param _who The address of the user who will have an increased number of locked tokens
     *
     * @param _amount The amount of locked tokens
     */
    function lock(address _who, uint256 _amount) external onlyEligibleContracts {
        require(_amount > 0, "[QEC-028002]-Invalid amount value, amount cannot be zero.");

        VotingLockInfo memory _votingLockInfo = lockInfos[msg.sender][_who];
        _votingLockInfo.lockedAmount += _amount;

        lockInfos[msg.sender][_who] = _votingLockInfo;

        emit LockedAmountChanged(msg.sender, _who, _votingLockInfo.lockedAmount);

        VotingDelegationInfo storage delegationInfo = delegationInfos[_who];

        if (delegationInfo.votingAgent == address(0)) {
            delegationInfo.votingAgent = _who;
        }

        if (!delegationInfo.isPending) {
            delegationInfos[delegationInfo.votingAgent].receivedWeight += _amount;
        }
    }

    /**
     * @notice announceUnlock is used to announce the unlock of locked coins.
     * When an unlock is announced, the information about the current expected amount
     * of the unlock and the unlock timeout is changed
     *
     * @dev When trying to announce an unlock for an amount more than
     * the user has in locked tokens, we will receive an error 028003.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which a certain amount of tokens will be announced to unlock
     *
     * @param _amount The amount of tokens for which unlocking will be announced
     *
     * @param _newUnlockTime new time for unlocking
     */
    function announceUnlock(
        address _who,
        uint256 _amount,
        uint256 _newUnlockTime
    ) external onlyEligibleContracts {
        VotingLockInfo storage votingLockInfo = lockInfos[msg.sender][_who];

        if (_amount < votingLockInfo.pendingUnlockAmount) {
            uint256 amountDifference = votingLockInfo.pendingUnlockAmount - _amount;
            _increaseLockedAmount(_who, amountDifference);
        } else {
            uint256 amountDifference = _amount - votingLockInfo.pendingUnlockAmount;
            _reduceLockedAmount(_who, amountDifference);
        }
        votingLockInfo.pendingUnlockAmount = _amount;

        uint256 _maxLockedUntil = _getMaxLockedUntil(_who);

        if (_newUnlockTime < _maxLockedUntil) {
            _newUnlockTime = _maxLockedUntil;
        }
        votingLockInfo.pendingUnlockTime = _newUnlockTime;

        emit PendingUnlockChanged(
            msg.sender,
            _who,
            votingLockInfo.pendingUnlockAmount,
            votingLockInfo.pendingUnlockTime
        );
    }

    /**
     * @notice unlock is used to unlock a certain amount of locked tokens
     * that were previously announced to be unlocked (or the amount, the lockedUntil of which has passed)
     *
     * @dev When trying to unlock zero tokens, we get error 028002.
     * If you try to unlock before the unlock time comes, we get error 028004.
     * When trying to unlock more than the user announced to unlock and lockedUntil time hasn't passed,
     * we will receive error 028005.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which a certain number of tokens will be unlocked
     *
     * @param _amount Amount of tokens to be unlocked
     */
    function unlock(address _who, uint256 _amount) external onlyEligibleContracts {
        require(_amount > 0, "[QEC-028002]-Invalid amount value, amount cannot be zero.");

        VotingLockInfo storage votingLockInfo = lockInfos[msg.sender][_who];
        require(
            votingLockInfo.pendingUnlockTime < block.timestamp,
            "[QEC-028004]-Not enough time has elapsed since the announcement of the unlock."
        );

        uint256 _totalLockedAmount = votingLockInfo.lockedAmount + votingLockInfo.pendingUnlockAmount;
        require(_totalLockedAmount >= _amount, "[QEC-028006]-Available locked amount less than unlock amount.");

        bool _hadPendingUnlock = votingLockInfo.pendingUnlockAmount > 0;

        if (votingLockInfo.pendingUnlockAmount >= _amount) {
            votingLockInfo.pendingUnlockAmount = votingLockInfo.pendingUnlockAmount - _amount;
        } else {
            require(
                _getMaxLockedUntil(_who) < block.timestamp,
                "[QEC-028005]-Smart unlock not possible, tokens are still locked by recent voting."
            );
            uint256 _smartUnlockAmount = _amount - votingLockInfo.pendingUnlockAmount;
            votingLockInfo.pendingUnlockAmount = 0;
            _reduceLockedAmount(_who, _smartUnlockAmount);
        }

        if (_hadPendingUnlock) {
            emit PendingUnlockAmountChanged(msg.sender, _who, votingLockInfo.pendingUnlockAmount);
        }
    }

    /**
     * @notice forceUnlock is used to force unlock locked tokens.
     * Unlocking occurs bypassing the required time before unlocking.
     * If there are not enough tokens announced for unlocking, then locked tokens will be taken.
     *
     * @dev When trying to lock zero tokens, we get error 028002.
     * When trying to forcefully unlock the token amount that exceeds
     * the amount of locked tokens, we will receive an error 028006.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which forced unlocking will be performed
     *
     * @param _amount The amount of tokens for forced unlocking
     */
    function forceUnlock(address _who, uint256 _amount) external onlyEligibleContracts {
        require(_amount > 0, "[QEC-028002]-Invalid amount value, amount cannot be zero.");

        VotingLockInfo storage votingLockInfo = lockInfos[msg.sender][_who];
        uint256 _totalLockedAmount = votingLockInfo.lockedAmount + votingLockInfo.pendingUnlockAmount;

        require(_totalLockedAmount >= _amount, "[QEC-028006]-Cannot enforce to unlock more than is currently locked.");

        bool _hadPendingUnlock = votingLockInfo.pendingUnlockAmount > 0;

        if (votingLockInfo.pendingUnlockAmount >= _amount) {
            votingLockInfo.pendingUnlockAmount = votingLockInfo.pendingUnlockAmount - _amount;
        } else {
            uint256 _forcedUnlockAmount = _amount - votingLockInfo.pendingUnlockAmount;
            votingLockInfo.pendingUnlockAmount = 0;
            _reduceLockedAmount(_who, _forcedUnlockAmount);
        }

        if (_hadPendingUnlock) {
            emit PendingUnlockAmountChanged(msg.sender, _who, votingLockInfo.pendingUnlockAmount);
        }
    }

    /**
     * @notice announceNewVoting
     * Agent is used to announce a new Voting Agent.
     * All delegated weight is removed from the old Voting Agent if it was.
     *
     * @param _newAgent New Voting Agent Address
     */
    function announceNewVotingAgent(address _newAgent) external {
        require(_newAgent != address(0), "[028011]-New voting agent cannot be a zero address.");

        VotingDelegationInfo storage delegationInfo = delegationInfos[msg.sender];
        address _prevVotingAgent = delegationInfo.votingAgent;

        if (!delegationInfo.isPending) {
            delegationInfos[_prevVotingAgent].receivedWeight =
                delegationInfos[_prevVotingAgent].receivedWeight -
                getLockedAmount(msg.sender);

            delegationInfo.votingAgentPassOverTime = getLockedUntil(_prevVotingAgent);
            delegationInfo.isPending = true;
        }

        delegationInfo.votingAgent = _newAgent;
        emit NewVotingAgentAnnounced(msg.sender, _prevVotingAgent, _newAgent);
    }

    /**
     * @notice setNewVotingAgent is used to set a pending voting agent.
     * The voting agent must be advertised before calling this method.
     */
    function setNewVotingAgent() external {
        VotingDelegationInfo memory _delegationInfo = delegationInfos[msg.sender];
        require(
            _delegationInfo.isPending,
            "[QEC-028008]-Changing the voting agent has to be announced, failed to set new voting agent."
        );
        require(
            _delegationInfo.votingAgentPassOverTime <= block.timestamp,
            "[QEC-028009]-Cannot change voting agent before passover time is reached."
        );

        _delegationInfo.isPending = false;
        delegationInfos[msg.sender] = _delegationInfo;

        uint256 _weightToAdd = getLockedAmount(msg.sender);
        address _votingAgent = _delegationInfo.votingAgent;

        delegationInfos[_votingAgent].receivedWeight += _weightToAdd;

        emit VotingAgentChanged(msg.sender, _votingAgent, _weightToAdd);
    }

    /**
     * @notice extendLocking is used by voting contracts during vote function execution.
     *
     * @dev Voting contracts extend lockedUntil by the voting end time.
     * If _lockNeededUntil less than lockedUntil - nothing changes
     *
     * @param _who User address for which a certain number of tokens will be unlocked
     * @param _lockNeededUntil Time until which tokens should be locked
     * @return total weight of tokens for the voting
     */
    function extendLocking(address _who, uint256 _lockNeededUntil) external onlyVotingContracts returns (uint256) {
        lockedUntil[_who] = _max(lockedUntil[_who], _lockNeededUntil);
        return getVotingWeight(_who, _lockNeededUntil);
    }

    /**
     * @notice getLockInfo is used to get token locking information
     * for a specific token lock source address and user address
     *
     * @dev We receive error 028001 if the address of the token lock source is passed to the method,
     * the key of which is not in the list of token lock sources
     *
     * @param _tokenLockSource The address of the contract in which the lock of tokens can be produced
     *
     * @param _who User address for which token lock information will be returned
     * @return information about the current lock of tokens as an object of the VotingLockInfo structure
     */
    function getLockInfo(address _tokenLockSource, address _who) external view returns (VotingLockInfo memory) {
        bool _isFound;

        for (uint256 i = 0; i < tokenLockSourcesKeys.length; ++i) {
            if (registry.mustGetAddress(tokenLockSourcesKeys[i]) == _tokenLockSource) {
                _isFound = true;
                break;
            }
        }

        string memory _reason = "[QEC-028001]-Unknown token lock source.";
        require(_isFound, _reason);
        VotingLockInfo memory _info = lockInfos[_tokenLockSource][_who];
        _info.lockedUntil = _getMaxLockedUntil(_who);

        return _info;
    }

    /**
     * @notice Returns base information about the user's voting rights
     * @param _user User's address
     * @param _votingEndTime End time of the voting period for the proposal
     * @return BaseVotingWeightInfo struct
     */
    function getBaseVotingWeightInfo(address _user, uint256 _votingEndTime)
        external
        view
        returns (BaseVotingWeightInfo memory)
    {
        VotingDelegationInfo memory _delegationInfo = delegationInfos[_user];
        BaseVotingWeightInfo memory _baseInfo;

        _baseInfo.ownWeight = getVotingWeight(_user, _votingEndTime);

        _baseInfo.votingAgent = _delegationInfo.votingAgent;
        _baseInfo.lockedUntil = getLockedUntil(_baseInfo.votingAgent);

        if (_delegationInfo.isPending) {
            _baseInfo.delegationStatus = DelegationStatus.PENDING;
        } else if (_baseInfo.votingAgent == _user) {
            _baseInfo.delegationStatus = DelegationStatus.SELF;
        } else {
            _baseInfo.delegationStatus = DelegationStatus.DELEGATED;
        }

        return _baseInfo;
    }

    /**
     * @notice Returns locking time, until which stakes are locked for given `_user`
     * @param _user given user
     * @return time, until which stakes are locked
     */
    function getLockedUntil(address _user) public view returns (uint256) {
        return lockedUntil[_user];
    }

    /**
     * @notice getVotingWeight is used to get the current voting weight for a specific user
     *
     * @param _who User address for which the current voting weight will be returned
     *
     * @param _lockNeededUntil Time until which tokens should be locked
     * @return total weight of tokens for voting
     */
    function getVotingWeight(address _who, uint256 _lockNeededUntil) public view returns (uint256) {
        VotingLockInfo memory _votingLockInfo;
        VotingDelegationInfo memory _delegationInfo = delegationInfos[_who];
        uint256 _totalVotingWeight = 0;

        if (_who == _delegationInfo.votingAgent && !_delegationInfo.isPending) {
            for (uint256 i = 0; i < tokenLockSourcesKeys.length; ++i) {
                _votingLockInfo = lockInfos[registry.mustGetAddress(tokenLockSourcesKeys[i])][_who];

                if (_votingLockInfo.pendingUnlockTime > _lockNeededUntil) {
                    _totalVotingWeight += _votingLockInfo.pendingUnlockAmount;
                }
            }
        }

        _totalVotingWeight += _delegationInfo.receivedWeight;

        return _totalVotingWeight;
    }

    /**
     * @notice getLockedAmount is used to get the total number of locked tokens
     * from all current sources of token lock for a specific user
     *
     * @param _who User address for which the total amount of locked tokens will be returned
     * @return total number of locked tokens in all sources of the token lock
     */
    function getLockedAmount(address _who) public view returns (uint256) {
        uint256 _totalLocked = 0;

        for (uint256 i = 0; i < tokenLockSourcesKeys.length; ++i) {
            _totalLocked += lockInfos[registry.mustGetAddress(tokenLockSourcesKeys[i])][_who].lockedAmount;
        }

        return _totalLocked;
    }

    function _reduceLockedAmount(address _who, uint256 _amount) private onlyEligibleContracts {
        VotingLockInfo storage votingLockInfo = lockInfos[msg.sender][_who];
        require(votingLockInfo.lockedAmount >= _amount, "[QEC-028003]-Cannot unlock more than is currently locked.");

        votingLockInfo.lockedAmount -= _amount;
        emit LockedAmountChanged(msg.sender, _who, votingLockInfo.lockedAmount);

        VotingDelegationInfo memory _delegationInfo = delegationInfos[_who];
        if (!_delegationInfo.isPending) {
            delegationInfos[_delegationInfo.votingAgent].receivedWeight -= _amount;
        }
    }

    function _increaseLockedAmount(address _who, uint256 _amount) private onlyEligibleContracts {
        VotingLockInfo storage votingLockInfo = lockInfos[msg.sender][_who];

        votingLockInfo.lockedAmount += _amount;
        emit LockedAmountChanged(msg.sender, _who, votingLockInfo.lockedAmount);

        VotingDelegationInfo memory _delegationInfo = delegationInfos[_who];
        if (!_delegationInfo.isPending) {
            delegationInfos[_delegationInfo.votingAgent].receivedWeight += _amount;
        }
    }

    function _getMaxLockedUntil(address _who) private view returns (uint256) {
        VotingDelegationInfo storage delegationInfo = delegationInfos[_who];

        return
            _max(
                lockedUntil[_who],
                _max(lockedUntil[delegationInfo.votingAgent], delegationInfo.votingAgentPassOverTime)
            );
    }

    function _max(uint256 a, uint256 b) private pure returns (uint256) {
        return (a > b ? a : b);
    }
}
