// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../interfaces/IContractRegistry.sol";
import "../governance/rootNodes/Roots.sol";
import "../governance/IParameters.sol";
import "../common/Globals.sol";

abstract contract ARootNodeApprovalVoting {
    struct AProposal {
        bool executed;
        uint256 votingStartTime;
        uint256 votingExpiredTime;
        uint256 requiredMajority;
        bytes callData;
    }
    struct ProposalStats {
        uint256 requiredMajority;
        uint256 currentMajority;
    }

    enum ProposalStatus {
        NONE,
        PENDING,
        EXPIRED,
        EXECUTED
    }

    IContractRegistry registry;

    // id => rootNodeAddress => voted
    mapping(uint256 => mapping(address => bool)) public voted;

    // id => vote count
    mapping(uint256 => uint256) public voteCount;

    uint256 public proposalsCount;

    // id => proposal
    mapping(uint256 => AProposal) internal proposals;

    event RootNodeApproved(uint256 _proposalId, address _rootNode);
    event ProposalCreated(uint256 _proposalId);
    event ProposalExecuted(uint256 _proposalId);

    modifier onlyRoot(address _a) {
        require(_getRoots().isMember(_a), "[QEC-039000]-Permission denied - only root nodes have access.");
        _;
    }
    modifier onlyInitedProposal(uint256 _id) {
        require(_id < proposalsCount, "[QEC-039005]-Proposal havn't inited.");
        _;
    }

    /*
     * @notice returns status of proposal
     * @param _id id of proposal
     */
    function getStatus(uint256 _id) external view returns (ProposalStatus) {
        if (_id >= proposalsCount) {
            return ProposalStatus.NONE;
        }
        AProposal memory _proposal = proposals[_id];
        if (_proposal.executed) {
            return ProposalStatus.EXECUTED;
        }
        if (block.timestamp < _proposal.votingExpiredTime) {
            return ProposalStatus.PENDING;
        } else {
            return ProposalStatus.EXPIRED;
        }
    }

    /*
     * @notice returns stats of proposal(currentMajority, requiredMajority)
     * @param _id id of proposal
     */
    function getProposalStats(uint256 _id) external view returns (ProposalStats memory _stats) {
        _stats.currentMajority = _currentMajority(_id);
        _stats.requiredMajority = proposals[_id].requiredMajority;
    }

    /*
     * @notice approve for proposal and execute it if vote count > requiredMajority
     * @param _id id of proposal
     */
    function approve(uint256 _id) public onlyInitedProposal(_id) onlyRoot(msg.sender) {
        AProposal memory _proposal = proposals[_id];

        require(!_proposal.executed, "[QEC-039001]-Already executed.");
        require(block.timestamp < _proposal.votingExpiredTime, "[QEC-039002]-Voting time has expired.");

        bool _voted;
        bool _executed;
        if (!voted[_id][msg.sender]) {
            voted[_id][msg.sender] = true;
            voteCount[_id]++;
            emit RootNodeApproved(_id, msg.sender);
            _voted = true;
        }
        _executed = _execute(_id);
        require(_voted || _executed, "[QEC-039004]-Sender has already voted");
    }

    function createProposal(bytes memory callData) internal returns (uint256) {
        uint256 _id = proposalsCount++;
        uint256 _executionP = _getConstitution().getUint("constitution.proposalExecutionP");
        AProposal storage _newProposal = proposals[_id];
        _newProposal.callData = callData;
        _newProposal.requiredMajority = _getConstitution().getUint("constitution.voting.emgQUpdateRMAJ");
        _newProposal.votingExpiredTime = (_newProposal.votingStartTime = block.timestamp) + _executionP;
        emit ProposalCreated(_id);
        return _id;
    }

    function onExecute(bytes memory _callData) internal virtual;

    function _getRoots() internal view returns (IRoots) {
        return IRoots(registry.mustGetAddress(RKEY__ROOT_NODES));
    }

    function _getConstitution() internal view returns (IParameters) {
        return IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
    }

    function _execute(uint256 _id) private returns (bool) {
        AProposal storage _proposal = proposals[_id];
        if (_currentMajority(_id) > _proposal.requiredMajority) {
            _proposal.executed = true;
            onExecute(_proposal.callData);
            emit ProposalExecuted(_id);
        }
        return _proposal.executed;
    }

    function _currentMajority(uint256 _id) private view returns (uint256) {
        uint256 _rootCount = _getRoots().getSize();
        return (voteCount[_id] * getDecimal()) / _rootCount;
    }
}
