// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IVoting.sol";
import "./IParameters.sol";
import "./IPanel.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IContractRegistry.sol";
import "../common/Globals.sol";

contract EmergencyUpdateVoting is IVoting, Initializable {
    IContractRegistry private registry;

    mapping(uint256 => BaseProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public voted;

    uint256 public proposalCount;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption);
    event ProposalExecuted(uint256 indexed _proposalId);

    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-030004]-Permission denied - only root nodes have access."
        );
        _;
    }

    modifier shouldExist(uint256 _id) {
        require(getStatus(_id) != ProposalStatus.NONE, "[QEC-030005]-Proposal does not exist.");
        _;
    }

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Creates new emergency proposal
     * @param _remark proposal remark
     * @return Proposal id
     */
    function createProposal(string memory _remark) external onlyRoot returns (uint256) {
        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        BaseProposal memory _prop;
        _prop.remark = _remark;
        _prop.params.votingStartTime = block.timestamp;
        _prop.params.votingEndTime = block.timestamp + constitution.getUint("constitution.voting.emgQUpdateVP");
        _prop.params.requiredQuorum = constitution.getUint("constitution.voting.emgQUpdateQRM");
        _prop.params.requiredMajority = constitution.getUint("constitution.voting.emgQUpdateRMAJ");
        _prop.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        proposals[proposalCount] = _prop;

        emit ProposalCreated(proposalCount, _prop);

        return proposalCount++;
    }

    /**
     * @notice Executes the proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override {
        require(
            getStatus(_proposalId) == ProposalStatus.PASSED,
            "[QEC-030003]-Proposal must be PASSED before excecuting."
        );

        proposals[_proposalId].executed = true;
        emit ProposalExecuted(_proposalId);
    }

    /**
     * @notice Votes for the proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Votes against the proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Returns the weight of votes for given proposal
     * @param _proposalId proposal id
     * @return the weight of votes for given proposal
     */
    function getVotesFor(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].counters.weightFor;
    }

    /**
     * @notice Returns the weight of votes against given proposal
     * @param _proposalId proposal id
     * @return the weight of votes against given proposal
     */
    function getVotesAgainst(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].counters.weightAgainst;
    }

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId];
    }

    /**
     * @notice Returns the current proposal stats for the given id
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        BaseProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.params.requiredQuorum;
        _stats.requiredMajority = _prop.params.requiredMajority;

        uint256 _votesCount = _prop.counters.weightFor + _prop.counters.weightAgainst;
        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        uint256 _currentQuorum = _calculatePercentage(_votesCount, _rootsCount);
        _stats.currentQuorum = _currentQuorum;

        uint256 _currentMajority = _calculatePercentage(_prop.counters.weightFor, _votesCount);
        _stats.currentMajority = _currentMajority;
        return _stats;
    }

    /**
     * @notice Returns status of specific proposal
     * @param _proposalId proposal id
     * @return Proposal status
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        BaseProposal memory _prop = proposals[_proposalId];

        if (_prop.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        uint256 _votesCount = _prop.counters.weightFor + _prop.counters.weightAgainst;
        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        uint256 _actualQuorum = _calculatePercentage(_votesCount, _rootsCount);

        if (_actualQuorum < _prop.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        uint256 _actualMajority = _calculatePercentage(_prop.counters.weightFor, _votesCount);

        if (_actualMajority <= _prop.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp > _prop.params.votingEndTime + _prop.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _vote(uint256 _proposalId, VotingOption _votingOption) private {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-030000]-Voting is only possible on PENDING proposals."
        );
        require(!voted[_proposalId][msg.sender], "[QEC-030002]-The caller has already voted for the proposal.");

        if (_votingOption == VotingOption.FOR) {
            proposals[_proposalId].counters.weightFor++;
        } else {
            proposals[_proposalId].counters.weightAgainst++;
        }

        voted[_proposalId][msg.sender] = true;

        emit UserVoted(_proposalId, _votingOption);
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _calculatePercentage(uint256 _part, uint256 _amount) private pure returns (uint256) {
        if (_amount == 0) {
            return 0;
        }
        return (_part * getDecimal()) / _amount;
    }
}
