// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IParameters.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../interfaces/IContractRegistry.sol";

abstract contract AParameters is IMutableParameters, Initializable {
    struct ValueAddr {
        address val;
        uint256 ind;
    }

    struct ValueUint {
        uint256 val;
        uint256 ind;
    }

    struct ValueString {
        string val;
        uint256 ind;
    }

    struct ValueBytes32 {
        bytes32 val;
        uint256 ind;
    }

    struct ValueBool {
        bool val;
        uint256 ind;
    }

    mapping(string => ValueUint) internal uintMap;
    mapping(string => ValueAddr) internal addrMap;
    mapping(string => ValueString) internal stringMap;
    mapping(string => ValueBytes32) internal bytes32Map;
    mapping(string => ValueBool) internal boolMap;

    string[] internal addrKeys;
    string[] internal uintKeys;
    string[] internal stringKeys;
    string[] internal boolKeys;
    string[] internal bytes32Keys;

    string ownerKey;
    IContractRegistry internal registry;

    modifier onlyContractOwner() {
        require(
            msg.sender == registry.mustGetAddress(ownerKey),
            "[QEC-009000]-Permission denied - only the owner has access."
        );
        _;
    }

    constructor() {}

    /**
     * @notice Removes uint256 parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeUint(string calldata _key) external override onlyContractOwner {
        ValueUint memory value = uintMap[_key];
        require(value.ind != 0, _concatErrorMessage(_key));
        // addr not exist
        require(
            value.ind <= uintKeys.length,
            "[QEC-009001]-Invalid index value, failed to remove the uint parameter from the list."
        );
        // invalid index value

        // Move an last element of array into the vacated key slot.
        uint256 _keyID = value.ind - 1;
        uint256 _lastID = uintKeys.length - 1;

        uintMap[uintKeys[_lastID]].ind = _keyID + 1;
        uintKeys[_keyID] = uintKeys[_lastID];
        uintKeys.pop();
        delete uintMap[_key];
    }

    /**
     * @notice Removes address parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeAddr(string calldata _key) external override onlyContractOwner {
        ValueAddr memory value = addrMap[_key];
        require(value.ind != 0, _concatErrorMessage(_key));
        // addr not exist
        require(
            value.ind <= addrKeys.length,
            "[QEC-009002]-Invalid index value, failed to remove the address from the list."
        );
        // invalid index value

        // Move an last element of array into the vacated key slot.
        uint256 _keyID = value.ind - 1;
        uint256 _lastID = addrKeys.length - 1;
        addrMap[addrKeys[_lastID]].ind = _keyID + 1;
        addrKeys[_keyID] = addrKeys[_lastID];

        addrKeys.pop();
        delete addrMap[_key];
    }

    /**
     * @notice Removes string parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeString(string calldata _key) external override onlyContractOwner {
        ValueString memory value = stringMap[_key];
        require(value.ind != 0, _concatErrorMessage(_key));
        // addr not exist
        require(
            value.ind <= stringKeys.length,
            "[QEC-009003]-Invalid index value, failed to remove the string parameter from the list."
        );
        // invalid index value

        // Move an last element of array into the vacated key slot.
        uint256 _keyID = value.ind - 1;
        uint256 _lastID = stringKeys.length - 1;
        stringMap[stringKeys[_lastID]].ind = _keyID + 1;
        stringKeys[_keyID] = stringKeys[_lastID];

        stringKeys.pop();
        delete stringMap[_key];
    }

    /**
     * @notice Removes byte32 parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeBytes32(string calldata _key) external override onlyContractOwner {
        ValueBytes32 memory value = bytes32Map[_key];
        require(value.ind != 0, _concatErrorMessage(_key));
        // bytes32 not exist
        require(
            value.ind <= bytes32Keys.length,
            "[QEC-009004]-Invalid index value, failed to remove the bytes32 parameter from the list."
        );
        // invalid index value

        // Move an last element of array into the vacated key slot.
        uint256 _keyID = value.ind - 1;
        uint256 _lastID = bytes32Keys.length - 1;
        bytes32Map[bytes32Keys[_lastID]].ind = _keyID + 1;
        bytes32Keys[_keyID] = bytes32Keys[_lastID];

        bytes32Keys.pop();
        delete bytes32Map[_key];
    }

    /**
     * @notice Removes bool parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeBool(string calldata _key) external override onlyContractOwner {
        ValueBool memory value = boolMap[_key];
        require(value.ind != 0, _concatErrorMessage(_key));
        // bool not exist
        require(
            value.ind <= boolKeys.length,
            "[QEC-009005]-Invalid index value, failed to remove the boolean parameter from the list."
        );
        // invalid index value

        // Move an last element of array into the vacated key slot.
        uint256 _keyID = value.ind - 1;
        uint256 _lastID = boolKeys.length - 1;
        boolMap[boolKeys[_lastID]].ind = _keyID + 1;
        boolKeys[_keyID] = boolKeys[_lastID];

        boolKeys.pop();
        delete boolMap[_key];
    }

    /**
     * @notice Sets byte32 by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setBytes32(string memory _key, bytes32 _val) external override onlyContractOwner {
        _setBytes32(_key, _val);
    }

    /**
     * @notice Returns an array of address keys
     * @return array of address keys
     */
    function getAddrKeys() external view override returns (string[] memory) {
        return addrKeys;
    }

    /**
     * @notice Returns an array of uint256 keys
     * @return array of uint256 keys
     */
    function getUintKeys() external view override returns (string[] memory) {
        return uintKeys;
    }

    /**
     * @notice Returns an array of string keys
     * @return array of string keys
     */
    function getStringKeys() external view override returns (string[] memory) {
        return stringKeys;
    }

    /**
     * @notice Returns an array of byte32 keys
     * @return array of byte32 keys
     */
    function getBytes32Keys() external view override returns (string[] memory) {
        return bytes32Keys;
    }

    /**
     * @notice Returns an array of bool keys
     * @return array of bool keys
     */
    function getBoolKeys() external view override returns (string[] memory) {
        return boolKeys;
    }

    /**
     * @notice Returns the address from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return address value
     */
    function getAddr(string calldata _key) external view override returns (address) {
        ValueAddr memory value = addrMap[_key];
        require(value.ind > 0, _concatErrorMessage(_key));
        return value.val;
    }

    /**
     * @notice Returns uint256 from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return decimal number
     */
    function getUint(string calldata _key) external view override returns (uint256) {
        ValueUint memory value = uintMap[_key];
        require(value.ind > 0, _concatErrorMessage(_key));
        return value.val;
    }

    /**
     * @notice Returns a string from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return string value
     */
    function getString(string calldata _key) external view override returns (string memory) {
        ValueString memory value = stringMap[_key];
        require(value.ind > 0, _concatErrorMessage(_key));
        return value.val;
    }

    /**
     * @notice Returns byte32 from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return byte32 value
     */
    function getBytes32(string calldata _key) external view override returns (bytes32) {
        ValueBytes32 memory value = bytes32Map[_key];
        require(value.ind > 0, _concatErrorMessage(_key));
        return value.val;
    }

    /**
     * @notice Returns bool from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return bool value
     */
    function getBool(string calldata _key) external view override returns (bool) {
        ValueBool memory value = boolMap[_key];
        require(value.ind > 0, _concatErrorMessage(_key));
        return value.val;
    }

    function initialize(
        address _registry,
        string[] memory _uintKeys,
        uint256[] memory _uintVals,
        string[] memory _addrKeys,
        address[] memory _addrVals,
        string[] memory _strKeys,
        string[] memory _strVals,
        string[] memory _boolKeys,
        bool[] memory _boolVals
    ) public virtual initializer {
        registry = IContractRegistry(_registry);

        for (uint256 i = 0; i < _uintKeys.length; i++) {
            _setUint(_uintKeys[i], _uintVals[i]);
        }

        for (uint256 i = 0; i < _addrKeys.length; i++) {
            _setAddr(_addrKeys[i], _addrVals[i]);
        }

        for (uint256 i = 0; i < _strKeys.length; i++) {
            _setString(_strKeys[i], _strVals[i]);
        }

        for (uint256 i = 0; i < _boolKeys.length; i++) {
            _setBool(_boolKeys[i], _boolVals[i]);
        }
    }

    /**
     * @notice Sets the address by key, if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setAddr(string memory _key, address _val) public virtual override onlyContractOwner {
        _setAddr(_key, _val);
    }

    /**
     * @notice Sets uint256 by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setUint(string memory _key, uint256 _val) public override onlyContractOwner {
        _setUint(_key, _val);
    }

    /**
     * @notice Sets the string by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setString(string memory _key, string memory _val) public override onlyContractOwner {
        _setString(_key, _val);
    }

    /**
     * @notice Sets bool by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setBool(string memory _key, bool _val) public override onlyContractOwner {
        _setBool(_key, _val);
    }

    function _setAddr(string memory _key, address _val) internal {
        require(_val != address(0x0), "[QEC-009006]-Invalid address, failed to set the address.");
        ValueAddr memory value = addrMap[_key];
        if (value.ind == 0) {
            addrKeys.push(_key);
            value.ind = addrKeys.length;
        }

        value.val = _val;
        addrMap[_key] = value;
    }

    function _setUint(string memory _key, uint256 _val) internal {
        ValueUint memory value = uintMap[_key];
        if (value.ind == 0) {
            uintKeys.push(_key);
            value.ind = uintKeys.length;
        }

        value.val = _val;
        uintMap[_key] = value;
    }

    function _setString(string memory _key, string memory _val) internal {
        ValueString memory value = stringMap[_key];
        if (value.ind == 0) {
            stringKeys.push(_key);
            value.ind = stringKeys.length;
        }

        value.val = _val;
        stringMap[_key] = value;
    }

    function _setBytes32(string memory _key, bytes32 _val) internal {
        ValueBytes32 memory value = bytes32Map[_key];
        if (value.ind == 0) {
            bytes32Keys.push(_key);
            value.ind = bytes32Keys.length;
        }

        value.val = _val;
        bytes32Map[_key] = value;
    }

    function _setBool(string memory _key, bool _val) internal {
        ValueBool memory value = boolMap[_key];
        if (value.ind == 0) {
            boolKeys.push(_key);
            value.ind = boolKeys.length;
        }

        value.val = _val;
        boolMap[_key] = value;
    }

    function _concatErrorMessage(string memory _key) internal pure returns (string memory) {
        return string(abi.encodePacked("[QEC-009007]-The parameter '", _key, "' does not exist."));
    }
}
