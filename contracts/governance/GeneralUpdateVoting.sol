// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./IVoting.sol";
import "./IParameters.sol";
import "./IPanel.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../tokeneconomics/ITokenLock.sol";
import "../interfaces/IContractRegistry.sol";
import "../common/Globals.sol";
import "../interfaces/IVotingWeightProxy.sol";

contract GeneralUpdateVoting is IQthVoting, Initializable {
    IContractRegistry private registry;

    mapping(uint256 => BaseProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public hasUserVoted;
    mapping(uint256 => mapping(address => bool)) public hasRootVetoed;

    uint256 public proposalCount;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption, uint256 _amount);
    event ProposalExecuted(uint256 indexed _proposalId);

    modifier shouldExist(uint256 _proposalId) {
        require(getStatus(_proposalId) != ProposalStatus.NONE, "[QEC-029006]-Proposal does not exist.");
        _;
    }

    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-029007]-Permission denied - only root nodes have access."
        );
        _;
    }

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Creates new general proposal
     * @param _remark proposal remark
     * @return Proposal id
     */
    function createProposal(string memory _remark) external returns (uint256) {
        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        BaseProposal memory _prop;
        _prop.remark = _remark;
        _prop.params.votingStartTime = block.timestamp;
        _prop.params.votingEndTime = block.timestamp + constitution.getUint("constitution.voting.changeQnotConstVP");
        _prop.params.requiredQuorum = constitution.getUint("constitution.voting.changeQnotConstQRM");
        _prop.params.requiredMajority = constitution.getUint("constitution.voting.changeQnotConstRMAJ");
        _prop.params.vetoEndTime =
            _prop.params.votingEndTime +
            constitution.getUint("constitution.voting.changeQnotConstRNVALP");
        _prop.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        proposals[proposalCount] = _prop;

        emit ProposalCreated(proposalCount, _prop);

        return proposalCount++;
    }

    /**
     * @notice Executes the proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override {
        require(
            getStatus(_proposalId) == ProposalStatus.PASSED,
            "[QEC-029000]-Proposal must be PASSED before excecuting."
        );

        proposals[_proposalId].executed = true;
        emit ProposalExecuted(_proposalId);
    }

    /**
     * @notice Votes for the proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Votes against the proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Veto from the passed id
     * @param _proposalId Proposal id
     */
    function veto(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        require(
            getStatus(_proposalId) == ProposalStatus.ACCEPTED,
            "[QEC-029004]-Proposal status must be accepted, veto failed."
        );
        require(
            !hasRootVetoed[_proposalId][msg.sender],
            "[QEC-029005]-The sender has already vetoed this proposal, veto failed."
        );

        hasRootVetoed[_proposalId][msg.sender] = true;

        ++proposals[_proposalId].counters.vetosCount;
    }

    /**
     * @notice Returns vote count for specific proposal
     * @param _proposalId proposal id
     * @return vote count for specific proposal
     */
    function getVotesFor(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].counters.weightFor;
    }

    /**
     * @notice Returns vote count against specific proposal
     * @param _proposalId proposal id
     * @return vote count against specific proposal
     */
    function getVotesAgainst(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].counters.weightAgainst;
    }

    /**
     * @notice Returns the number of users who voted for the veto
     * @param _proposalId Proposal id
     * @return number of vetoes
     */
    function getVetosNumber(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return proposals[_proposalId].counters.vetosCount;
    }

    /**
     * @notice Returns the percentage of those who voted for the veto from all roots
     * @param _proposalId Proposal id
     * @return vetos percentage
     */
    function getVetosPercentage(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return _getVetosPercentageNumber(_proposalId);
    }

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId];
    }

    /**
     * @notice Returns the current proposal stats for the given id
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        BaseProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.params.requiredQuorum;
        _stats.requiredMajority = _prop.params.requiredMajority;

        uint256 _votesCount = _prop.counters.weightFor + _prop.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _currentQuorum = _calculatePercentage(_votesCount, _Qamount);
        _stats.currentQuorum = _currentQuorum;

        uint256 _currentMajority = _calculatePercentage(_prop.counters.weightFor, _votesCount);
        _stats.currentMajority = _currentMajority;

        _stats.currentVetoPercentage = _getVetosPercentageNumber(_proposalId);

        return _stats;
    }

    /**
     * @notice Returns information about the user's voting rights by the proposal id
     * @param _proposalId Proposal id
     * @return VotingWeightInfo struct
     */
    function getVotingWeightInfo(uint256 _proposalId)
        public
        view
        override
        shouldExist(_proposalId)
        returns (VotingWeightInfo memory)
    {
        IVotingWeightProxy _proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        VotingWeightInfo memory _info;
        _info.base = _proxy.getBaseVotingWeightInfo(msg.sender, proposals[_proposalId].params.votingEndTime);
        _info.hasAlreadyVoted = hasUserVoted[_proposalId][msg.sender];
        _info.canVote = _info.base.ownWeight > 0 && !_info.hasAlreadyVoted;

        return _info;
    }

    /**
     * @notice Returns status of specific proposal
     * @param _proposalId proposal id
     * @return Proposal status
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        BaseProposal memory _prop = proposals[_proposalId];
        if (_prop.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        uint256 _totalWeight = _prop.counters.weightFor + _prop.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _Qamount);
        if (_actualQuorum < _prop.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        uint256 _actualMajority = _calculatePercentage(_prop.counters.weightFor, _totalWeight);
        if (_actualMajority <= _prop.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (_getVetosPercentageNumber(_proposalId) > getDecimal() / 2) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp < _prop.params.vetoEndTime) {
            return ProposalStatus.ACCEPTED;
        }

        if (block.timestamp > _prop.params.vetoEndTime + _prop.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _vote(uint256 _proposalId, VotingOption _votingOption) private {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-029001]-Voting is only possible on PENDING proposals."
        );
        require(!hasUserVoted[_proposalId][msg.sender], "[QEC-029002]-The caller has already voted for the proposal.");

        hasUserVoted[_proposalId][msg.sender] = true;

        BaseProposal storage _prop = proposals[_proposalId];
        IVotingWeightProxy proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        uint256 _totalLockedQ = proxy.extendLocking(msg.sender, _prop.params.votingEndTime);
        require(_totalLockedQ > 0, "[QEC-029003]-The total voting weight must be greater than zero, failed to vote.");

        if (_votingOption == VotingOption.FOR) {
            _prop.counters.weightFor += _totalLockedQ;
        } else {
            _prop.counters.weightAgainst += _totalLockedQ;
        }

        emit UserVoted(_proposalId, _votingOption, _totalLockedQ);
    }

    function _getVetosPercentageNumber(uint256 _proposalId) private view returns (uint256) {
        return (proposals[_proposalId].counters.vetosCount * getDecimal()) / (IRoots(_getRootNodesAddress()).getSize());
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }

    function _calculatePercentage(uint256 _part, uint256 _amount) private pure returns (uint256) {
        if (_amount == 0) {
            return 0;
        }
        return (_part * getDecimal()) / _amount;
    }
}
