// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IParameters.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../IPanel.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../governance/constitution/ConstitutionVoting.sol";
import "../../interfaces/IVotingWeightProxy.sol";

/** @title Root nodes voting */
contract RootsVoting is IMembershipVoting, Initializable {
    struct RootsProposal {
        BaseProposal base;
        address candidate;
        address replaceDest;
    }

    IContractRegistry private registry;

    mapping(uint256 => RootsProposal) public proposals;
    uint256 proposalsLength;

    // keep track of already voted accounts
    mapping(uint256 => mapping(address => bool)) public hasUserVoted;
    mapping(uint256 => mapping(address => bool)) public hasRootVetoed;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption, uint256 _amount);
    event ProposalExecuted(uint256 indexed _proposalId);

    modifier shouldExist(uint256 _proposalId) {
        // zero status value is invalid for existing proposal
        require(getStatus(_proposalId) != ProposalStatus.NONE, "[QEC-003008]-The proposal does not exist.");
        _;
    }

    modifier shouldBePassed(uint256 _proposalId) {
        // zero status value is invalid for existing proposal
        require(
            getStatus(_proposalId) == ProposalStatus.PASSED,
            "[QEC-003009]-Proposal must be PASSED before excecuting."
        );
        _;
    }

    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-003012]-Permission denied - only root nodes have access."
        );
        _;
    }

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Applies changes for specified proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override shouldExist(_proposalId) shouldBePassed(_proposalId) {
        RootsProposal memory _prop = proposals[_proposalId];
        ProposalType _proposalType = _getProposalType(_prop.candidate, _prop.replaceDest);
        if (_proposalType == ProposalType.ADD) {
            IRoots(_getRootNodesAddress()).addMember(_prop.candidate);
        } else if (_proposalType == ProposalType.REMOVE) {
            IRoots(_getRootNodesAddress()).removeMember(_prop.replaceDest);
        } else if (_proposalType == ProposalType.CHANGE) {
            IRoots(_getRootNodesAddress()).swapMember(_prop.replaceDest, _prop.candidate);
        }

        proposals[_proposalId].base.executed = true;
        emit ProposalExecuted(_proposalId);
    }

    /**
     * @dev create new root node proposal
     * @param _remark is an optional string to store in proposal
     * @param _constitutionHash is an expected constitutionHash that stored in constitutionVoting
     * @param _candidate of proposal
     * @param _replaceDest root to replace in current set (required, if the current set of root nodes is full)
     * @return proposal id
     */
    function createProposal(
        string memory _remark,
        bytes32 _constitutionHash,
        address _candidate,
        address _replaceDest
    ) external returns (uint256) {
        ProposalType _proposalType = _getProposalType(_candidate, _replaceDest);
        IRoots _roots = IRoots(_getRootNodesAddress());
        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        uint256 _nRootNodes = _roots.getSize();
        if (ProposalType.REMOVE == _proposalType) {
            require(_roots.isMember(_replaceDest), "[QEC-003000]-The root node to be replaced does not exist.");

            if (_replaceDest == msg.sender) {
                uint8 _nullId = 0;
                _roots.removeMember(msg.sender);

                return _nullId;
            }
        } else {
            require(_candidate == msg.sender, "[QEC-003001]-Root nodes have to propose themselves.");

            uint256 _rootNodesLimit = constitution.getUint("constitution.maxNRootNodes");
            if (ProposalType.CHANGE == _proposalType) {
                require(_roots.isMember(_replaceDest), "[QEC-003000]-The root node to be replaced does not exist.");

                require(_nRootNodes <= _rootNodesLimit, "[QEC-003017]-Maximum number of root nodes is exceeded.");
            } else if (ProposalType.ADD == _proposalType) {
                require(_nRootNodes < _rootNodesLimit, "[QEC-003002]-Maximum number of root nodes is reached.");
            }
        }

        if (_constitutionHash != "") {
            ConstitutionVoting _constitutionVoting = ConstitutionVoting(
                registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS_VOTING)
            );
            require(
                _constitutionHash == _constitutionVoting.constitutionHash(),
                "[QEC-003014]-The given hash does not correspond to the latest onchain constitution hash."
            );
        }

        RootsProposal memory _prop;
        _prop.candidate = _candidate;
        _prop.base.remark = _remark;
        _prop.replaceDest = _replaceDest;
        _prop.base.params.votingEndTime = block.timestamp + constitution.getUint("constitution.voting.addOrRemRootVP");
        _prop.base.params.vetoEndTime =
            _prop.base.params.votingEndTime +
            constitution.getUint("constitution.voting.addOrRemRootRNVALP");
        _prop.base.params.requiredQuorum = constitution.getUint("constitution.voting.addOrRemRootQRM");
        _prop.base.params.requiredMajority = constitution.getUint("constitution.voting.addOrRemRootRMAJ");
        _prop.base.params.requiredSQuorum = constitution.getUint("constitution.voting.addOrRemRootSQRM");
        _prop.base.params.requiredSMajority = constitution.getUint("constitution.voting.addOrRemRootSMAJ");
        _prop.base.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        uint256 _id = proposalsLength++;
        proposals[_id] = _prop;

        emit ProposalCreated(_id, _prop.base);

        return _id;
    }

    /**
     * @dev vote for the proposal
     * @param _proposalId - id of the proposal
     */
    function voteFor(uint256 _proposalId) external override {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @dev vote against the proposal
     * @param _proposalId - id of the proposal
     */
    function voteAgainst(uint256 _proposalId) external override {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice veto a proposal
     * @param _proposalId - id of the proposal
     */
    function veto(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        require(
            getStatus(_proposalId) == ProposalStatus.ACCEPTED,
            "[QEC-003010]-Proposal must be ACCEPTED before casting a root node veto."
        );
        require(!hasRootVetoed[_proposalId][msg.sender], "[QEC-003011]-The caller has already vetoed the proposal.");
        require(!isOverruleApplied(_proposalId), "[QEC-003015]-Veto is not possible because of super majority vote.");

        hasRootVetoed[_proposalId][msg.sender] = true;

        ++proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Skips veto time for given proposal if Overrule is applied.
     * @dev Checks whether Overrule is applied via `isOverruleApplied` method.
     * @param _proposalId proposal id
     */
    function skipVetoPhase(uint256 _proposalId) external shouldExist(_proposalId) {
        require(
            isOverruleApplied(_proposalId),
            "[QEC-003016]-Veto phase can be skipped only with super majority vote."
        );

        proposals[_proposalId].base.params.vetoEndTime = block.timestamp;
    }

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId].base;
    }

    /**
     * @notice Returns the current proposal stats for the given id
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        RootsProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.base.params.requiredQuorum;
        _stats.requiredMajority = _prop.base.params.requiredMajority;

        uint256 _totalWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);

        _stats.currentQuorum = _calculatePercentage(_totalWeight, _Qamount);
        _stats.currentMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);
        _stats.currentVetoPercentage = getVetosPercentage(_proposalId);

        return _stats;
    }

    /**
     * @notice Returns the number of users who voted for the veto
     * @param _proposalId Proposal id
     * @return number of vetoes
     */
    function getVetosNumber(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @dev count voting results "YES"
     * @param _proposalId - id of the proposal
     * @return the weight of votes for given proposal
     */
    function getVotesFor(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.counters.weightFor;
    }

    /**
     * @dev count voting results "NO"
     * @param _proposalId - id of the proposal
     * @return the weight of votes against given proposal
     */
    function getVotesAgainst(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.counters.weightAgainst;
    }

    /**
     * @notice Gets percent for vetos number
     * @param _proposalId Proposal id
     * @return percent of vetos
     */
    function getVetosPercentage(uint256 _proposalId) public view override returns (uint256) {
        return
            (proposals[_proposalId].base.counters.vetosCount * getDecimal()) / IRoots(_getRootNodesAddress()).getSize();
    }

    /**
     * @notice Returns information about the user's voting rights by the proposal id
     * @param _proposalId Proposal id
     * @return VotingWeightInfo struct
     */
    function getVotingWeightInfo(uint256 _proposalId)
        public
        view
        override
        shouldExist(_proposalId)
        returns (VotingWeightInfo memory)
    {
        IVotingWeightProxy _proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        VotingWeightInfo memory _info;
        _info.base = _proxy.getBaseVotingWeightInfo(msg.sender, proposals[_proposalId].base.params.votingEndTime);
        _info.hasAlreadyVoted = hasUserVoted[_proposalId][msg.sender];
        _info.canVote = _info.base.ownWeight > 0 && !_info.hasAlreadyVoted;

        return _info;
    }

    /**
     * @notice checks whether Overrule is applied
     * @param _proposalId proposal id
     * @return result of check whether Overrule is applied
     */
    function isOverruleApplied(uint256 _proposalId) public view shouldExist(_proposalId) returns (bool) {
        RootsProposal memory _prop = proposals[_proposalId];

        uint256 _totalWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _Qamount);
        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);

        return
            _actualMajority > _prop.base.params.requiredSMajority && _actualQuorum > _prop.base.params.requiredSQuorum;
    }

    /**
     * @notice Returns the status of the given proposal
     * @param _proposalId Proposal id
     * @return status of proposal
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        RootsProposal memory _prop = proposals[_proposalId];
        if (_prop.base.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.base.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.base.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        if (getVetosPercentage(_proposalId) > getDecimal() / 2) {
            return ProposalStatus.REJECTED;
        }

        uint256 _totalWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _Qamount);
        if (_actualQuorum < _prop.base.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);
        if (_actualMajority <= _prop.base.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp < _prop.base.params.vetoEndTime) {
            return ProposalStatus.ACCEPTED;
        }

        if (block.timestamp > _prop.base.params.vetoEndTime + _prop.base.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _vote(uint256 _proposalId, VotingOption option) private shouldExist(_proposalId) returns (bool) {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-003003]-Voting is only possible on PENDING proposals."
        );
        require(!hasUserVoted[_proposalId][msg.sender], "[QEC-003005]-The caller has already voted for the proposal.");

        IVotingWeightProxy proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        uint256 _totalLockedQ = proxy.extendLocking(msg.sender, proposals[_proposalId].base.params.votingEndTime);
        require(_totalLockedQ > 0, "[QEC-003013]-The total voting weight must be greater than zero.");

        if (option == VotingOption.FOR) {
            proposals[_proposalId].base.counters.weightFor += _totalLockedQ;
        } else {
            proposals[_proposalId].base.counters.weightAgainst += _totalLockedQ;
        }

        hasUserVoted[_proposalId][msg.sender] = true;
        emit UserVoted(_proposalId, option, _totalLockedQ);

        return true;
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _getProposalType(address _candidate, address _replaceDest) private pure returns (ProposalType) {
        ProposalType _proposalType = ProposalType.NONE;
        if (_candidate != address(0) && _replaceDest != address(0)) {
            _proposalType = ProposalType.CHANGE;
        } else if (_candidate == address(0) && _replaceDest != address(0)) {
            _proposalType = ProposalType.REMOVE;
        } else if (_candidate != address(0) && _replaceDest == address(0)) {
            _proposalType = ProposalType.ADD;
        } else {
            revert("[QEC-003007]-Invalid proposal parameters, failed to derive the proposal type.");
        }
        return _proposalType;
    }

    function _calculatePercentage(uint256 part, uint256 amount) private pure returns (uint256) {
        if (amount == 0) {
            return 0;
        }
        return (part * getDecimal()) / amount;
    }
}
