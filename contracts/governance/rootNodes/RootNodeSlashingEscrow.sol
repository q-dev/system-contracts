// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../ASlashingEscrow.sol";

/**
 * @title Root Nodes Slashing Escrow
 * @notice Used to object the root node slashing
 */
contract RootNodeSlashingEscrow is ASlashingEscrow {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ASlashingEscrow.initialize(_registry);

        associatedContractKey = RKEY__ROOT_NODES;
        slashingVotingContractKey = RKEY__ROOT_NODES_SLASHING_VOTING;
        slashingOBJP = "constitution.voting.rootSlashingOBJP";
        slashingVP = "constitution.voting.rootSlashingVP";
        slashingAppealP = "constitution.rootSlashingAppealP";
        slashingReward = "constitution.rootSlashingReward";
    }
}
