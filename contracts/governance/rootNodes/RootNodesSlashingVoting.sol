// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IParameters.sol";
import "../IVoting.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../common/Globals.sol";
import "../../interfaces/IVotingWeightProxy.sol";
import "../IPanel.sol";

/**
 * @title Root Nodes Slashing Voting
 * @notice Used to vote for slashing for root nodes
 */
contract RootNodesSlashingVoting is IQthVoting, ISlashingVoting, Initializable {
    IContractRegistry private registry;

    mapping(uint256 => SlashingProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public hasUserVoted;
    mapping(uint256 => mapping(address => bool)) public hasRootVetoed;

    uint256 public proposalCount;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption, uint256 _amount);
    event ProposalExecuted(uint256 indexed _proposalId, address _candidate, uint256 _amountToSlash);

    /**
     * @notice Restricts only root node can interact
     */
    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-004004]-Permission denied - only root nodes have access."
        );
        _;
    }

    /**
     * @notice Restricts only for existing proposals
     * @param _proposalId Proposal id
     */
    modifier shouldExist(uint256 _proposalId) {
        require(getStatus(_proposalId) != ProposalStatus.NONE, "[QEC-004005]-The slashing proposal does not exist.");
        _;
    }

    /**
     * @notice Restricts only for not existing proposals
     * @param _root Proposer
     * @param _target Slashing target
     */
    modifier shouldBeUnique(address _root, address _target) {
        uint256[] memory proposalIds = IRoots(_getRootNodesAddress()).getSlashingProposalIds(_target);
        for (uint256 i = 0; i < proposalIds.length; i++) {
            require(
                _root != proposals[proposalIds[i]].proposer,
                "[QEC-004012]-The slashing proposal with same creator and victim exists."
            );
        }
        _;
    }

    /**
     * @notice Restricts only for passed proposals
     * @param _proposalId Proposal id
     */
    modifier shouldBePassed(uint256 _proposalId) {
        require(getStatus(_proposalId) == ProposalStatus.PASSED, "[QEC-004006]-The slashing proposal has not passed.");
        _;
    }

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Creates proposal
     * @param _remark Some message with details (may be link)
     * @param _candidate address of candidate for slashing
     * @param _percentage percentage of stake to slash
     * @return id of new proposal
     */
    function createProposal(
        string memory _remark,
        address _candidate,
        uint256 _percentage
    ) external onlyRoot shouldBeUnique(msg.sender, _candidate) returns (uint256) {
        require(
            0 < _percentage && _percentage <= getDecimal(),
            "[QEC-004008]-Invalid percentage parameter, slashing proposal creation failed."
        );

        IRoots roots = IRoots(_getRootNodesAddress());
        require(roots.getRootNodeStake(_candidate) > 0, "[QEC-004000]-The candidate does not have any stake to slash.");

        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));

        SlashingProposal memory _prop;
        _prop.base.remark = _remark;
        _prop.candidate = _candidate;
        _prop.proposer = msg.sender;
        _prop.amountToSlash = _calculateAmountToSlash(_percentage, roots.getRootNodeStake(_candidate));
        _prop.base.params.votingStartTime = block.timestamp;
        _prop.base.params.votingEndTime = block.timestamp + constitution.getUint("constitution.voting.rootSlashingVP");
        _prop.base.params.vetoEndTime =
            _prop.base.params.votingEndTime +
            constitution.getUint("constitution.voting.rootSlashingRNVALP");
        _prop.base.params.requiredQuorum = constitution.getUint("constitution.voting.rootSlashingQRM");
        _prop.base.params.requiredMajority = constitution.getUint("constitution.voting.rootSlashingRMAJ");
        _prop.base.params.requiredSQuorum = constitution.getUint("constitution.voting.rootSlashingSQRM");
        _prop.base.params.requiredSMajority = constitution.getUint("constitution.voting.rootSlashingSMAJ");
        _prop.base.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");
        proposals[proposalCount] = (_prop);

        roots.addSlashingProposal(_candidate, proposalCount);

        emit ProposalCreated(proposalCount, _prop.base);

        uint256 _proposalId = proposalCount;
        proposalCount++;

        return _proposalId;
    }

    /**
     * @notice Give a vote for the specified proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Give a vote against the specified proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Applies changes for specified proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override onlyRoot shouldBePassed(_proposalId) {
        SlashingProposal memory _prop = proposals[_proposalId];
        proposals[_proposalId].base.executed = IRoots(_getRootNodesAddress()).applySlashing(
            _proposalId,
            _prop.candidate,
            _prop.amountToSlash
        );

        emit ProposalExecuted(_proposalId, _prop.candidate, _prop.amountToSlash);
    }

    /**
     * @notice Objects the proposal
     * @param _proposalId Proposal id
     */
    function veto(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        require(
            getStatus(_proposalId) == ProposalStatus.ACCEPTED,
            "[QEC-004002]-Proposal must be ACCEPTED before casting a root node veto."
        );
        require(!hasRootVetoed[_proposalId][msg.sender], "[QEC-004009]-The caller has already vetoed the proposal.");
        require(!isOverruleApplied(_proposalId), "[QEC-004010]-Veto is not possible because of super majority vote.");

        hasRootVetoed[_proposalId][msg.sender] = true;

        ++proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Skips the veto phase
     * @param _proposalId Proposal id
     */
    function skipVetoPhase(uint256 _proposalId) external shouldExist(_proposalId) {
        require(
            isOverruleApplied(_proposalId),
            "[QEC-004011]-Veto phase can be skipped only with super majority vote."
        );

        proposals[_proposalId].base.params.vetoEndTime = block.timestamp;
    }

    /**
     * @notice Returns the base structure for the proposal of given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId].base;
    }

    /**
     * @notice Gets slashing victim from the proposal
     * @param _proposalId Proposal id
     * @return address of slashing victim
     */
    function getSlashingVictim(uint256 _proposalId) external view override shouldExist(_proposalId) returns (address) {
        return proposals[_proposalId].candidate;
    }

    /**
     * @notice Gets slashing proposer from the proposal
     * @param _proposalId Proposal id
     * @return address of slashing proposer
     */
    function getSlashingProposer(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (address)
    {
        return proposals[_proposalId].proposer;
    }

    /**
     * @notice Gets statistic for the proposal
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        SlashingProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.base.params.requiredQuorum;
        _stats.requiredMajority = _prop.base.params.requiredMajority;

        uint256 _totalVotingWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _currentQuorum = _calculatePercentage(_totalVotingWeight, _Qamount);
        _stats.currentQuorum = _currentQuorum;
        _stats.currentVetoPercentage = getVetosPercentage(_proposalId);
        _stats.currentMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalVotingWeight);

        return _stats;
    }

    /**
     * @notice Specifies is proposal pending
     * @param _proposalId Proposal id
     * @return boolean, true for pending
     */
    function slashingAffectsWithdrawal(uint256 _proposalId) external view returns (bool) {
        ProposalStatus _status = getStatus(_proposalId);
        return (_status != ProposalStatus.REJECTED &&
            _status != ProposalStatus.EXECUTED &&
            _status != ProposalStatus.EXPIRED);
    }

    /**
     * @notice Specifies is proposal start time
     * @param _proposalId Proposal id
     * @return unix TimeStamp start time
     */
    function getProposalStartTime(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.params.votingStartTime;
    }

    /**
     * @notice Gets count for vetos number
     * @param _proposalId Proposal id
     * @return number of vetos
     */
    function getVetosNumber(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Gets percent for vetos number
     * @param _proposalId Proposal id
     * @return percent of vetos
     */
    function getVetosPercentage(uint256 _proposalId) public view override returns (uint256) {
        return
            (proposals[_proposalId].base.counters.vetosCount * getDecimal()) /
            (IRoots(_getRootNodesAddress()).getSize());
    }

    /**
     * @notice Checks if overrule applies
     * @param _proposalId Proposal id
     * @return true if the super quorum and super majority are reached
     */
    function isOverruleApplied(uint256 _proposalId) public view shouldExist(_proposalId) returns (bool) {
        SlashingProposal memory _prop = proposals[_proposalId];

        uint256 _totalWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _Qamount);
        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);

        return
            _actualMajority > _prop.base.params.requiredSMajority && _actualQuorum > _prop.base.params.requiredSQuorum;
    }

    /**
     * @notice Returns information about the user's voting rights by the proposal id
     * @param _proposalId Proposal id
     * @return VotingWeightInfo struct
     */
    function getVotingWeightInfo(uint256 _proposalId)
        public
        view
        override
        shouldExist(_proposalId)
        returns (VotingWeightInfo memory)
    {
        IVotingWeightProxy _proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        VotingWeightInfo memory _info;
        _info.base = _proxy.getBaseVotingWeightInfo(msg.sender, proposals[_proposalId].base.params.votingEndTime);
        _info.hasAlreadyVoted = hasUserVoted[_proposalId][msg.sender];
        _info.canVote = _info.base.ownWeight > 0 && !_info.hasAlreadyVoted;

        return _info;
    }

    /**
     * @notice Returns the status of the given proposal
     * @param _proposalId Proposal id
     * @return status of proposal
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        SlashingProposal memory _prop = proposals[_proposalId];

        if (_prop.base.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.base.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.base.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        if (getVetosPercentage(_proposalId) > getDecimal() / 2) {
            return ProposalStatus.REJECTED;
        }

        uint256 _totalWeight = _prop.base.counters.weightFor + (_prop.base.counters.weightAgainst);
        uint256 _qAmount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _qAmount);
        if (_actualQuorum < _prop.base.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);
        if (_actualMajority <= _prop.base.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp < _prop.base.params.vetoEndTime) {
            return ProposalStatus.ACCEPTED;
        }

        if (block.timestamp > _prop.base.params.votingEndTime + _prop.base.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    /**
     * @notice Adds voting weight of sender to the weight of related voting option
     * @param _proposalId Proposal id
     * @param _votingOption Can be FOR or AGAINST
     * @return true if everything went well
     */
    function _vote(uint256 _proposalId, VotingOption _votingOption) private returns (bool) {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-004001]-Voting is only possible on PENDING proposals."
        );
        require(!hasUserVoted[_proposalId][msg.sender], "[QEC-004003]-The caller has already voted for the proposal.");

        IVotingWeightProxy _proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        SlashingProposal memory _proposal = proposals[_proposalId];
        uint256 _totalLockedQ = _proxy.extendLocking(msg.sender, _proposal.base.params.votingEndTime);
        require(_totalLockedQ > 0, "[QEC-004007]-The total voting weight must be greater than zero.");

        if (_votingOption == VotingOption.FOR) {
            proposals[_proposalId].base.counters.weightFor += _totalLockedQ;
        } else {
            proposals[_proposalId].base.counters.weightAgainst += _totalLockedQ;
        }

        hasUserVoted[_proposalId][msg.sender] = true;

        emit UserVoted(_proposalId, _votingOption, _totalLockedQ);

        return true;
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _calculatePercentage(uint256 _part, uint256 _amount) private pure returns (uint256) {
        if (_amount == 0) {
            return 0;
        }
        return (_part * getDecimal()) / _amount;
    }

    function _calculateAmountToSlash(uint256 _percentage, uint256 _stake) private pure returns (uint256) {
        return (_percentage * (_stake)) / getDecimal();
    }
}
