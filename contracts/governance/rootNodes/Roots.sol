// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IParameters.sol";
import "../IPanel.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../../tokeneconomics/ITokenLock.sol";
import "../../interfaces/IVotingWeightProxy.sol";
import "./RootNodesSlashingVoting.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../common/AddressStorageFactory.sol";
import "../ASlashingEscrow.sol";
import "../../tokeneconomics/ATimeLockBase.sol";
import "../../tokeneconomics/WithdrawAddresses.sol";

contract Roots is IRoots, Initializable, ATimeLockBase {
    struct Stake {
        address root;
        uint256 value;
    }

    struct Withdrawal {
        uint256 endTime;
        uint256 amount;
    }

    mapping(address => uint256) private rootNodesStakes;
    mapping(address => uint256) private indexerOfStakes;
    mapping(address => Withdrawal) public withdrawals;
    mapping(address => uint256[]) public pendingSlashingProposals;

    AddressStorage roots;
    IContractRegistry private registry;
    Stake[] stakes;

    event CommittedStake(address indexed _root, uint256 _amount);
    event WithdrawalAnnounced(address indexed _root, uint256 _amount);
    event Withdrawn(address indexed _root, address indexed _paidTo, uint256 _amount);
    event DepositedStake(address indexed _sender, address indexed _root, uint256 _amount);

    modifier onlyRootsVoting() {
        require(
            msg.sender == registry.mustGetAddress(RKEY__ROOT_NODES_MEMBERSHIP_VOTING),
            "[QEC-002008]-Permission denied - only the RootNodesMembershipVoting contract has access."
        );
        _;
    }

    modifier onlyRootNodesSlashingVoting() {
        require(
            msg.sender == _getRootNodesSlahingVotingAddress(),
            "[QEC-002009]-Permission denied - only the RootNodesSlashingVoting contract has access."
        );
        _;
    }

    constructor() {}

    function initialize(address _registry, address[] memory _roots) external initializer {
        registry = IContractRegistry(_registry);
        roots = AddressStorageFactory(registry.mustGetAddress(RKEY__ADDRESS_STORAGE_FACTORY)).create(_roots);
    }

    /**
     * @notice Adds a new member to the list
     * @param _rootAddress Member to add
     */
    function addMember(address _rootAddress) external override onlyRootsVoting {
        _addRoot(_rootAddress);
    }

    /**
     * @notice Removes existing member
     * @param _rootAddress Member to be deleted
     */
    function removeMember(address _rootAddress) external override onlyRootsVoting {
        roots.remove(_rootAddress);
    }

    /**
     * @notice Modifies an existing root
     * @param _rootAddress Member to be changed
     * @param _newRootAddress New member address
     */
    function swapMember(address _rootAddress, address _newRootAddress) external override onlyRootsVoting {
        roots.remove(_rootAddress);
        _addRoot(_newRootAddress);
    }

    /**
     * @notice Adds sent amount to root balance and locks it for voting.
     */
    function commitStake() external payable {
        _addStake(msg.sender, msg.value);
        emit CommittedStake(msg.sender, rootNodesStakes[msg.sender]);
    }

    /**
     * @notice announceWithdrawal is used to announce withdrawal tokens from the root account
     *
     * @dev Announce withdrawal amount must be more than zero and less than stake amount.
     *
     * @param _amount The amount of tokens to announcing withdrawal
     */
    function announceWithdrawal(uint256 _amount) external {
        require(
            rootNodesStakes[msg.sender] >= _amount,
            "[QEC-002001]-Announced withdrawal must not exceed current stake."
        );

        uint256 withdrawalTime = block.timestamp +
            IParameters(_getConstitutionParametersAddress()).getUint("constitution.rootWithdrawP");

        _announceUnlock(_amount, withdrawalTime);

        withdrawals[msg.sender] = Withdrawal(withdrawalTime, _amount);

        emit WithdrawalAnnounced(msg.sender, _amount);
    }

    /**
     * @notice applySlashing is used to slash a specific amount on a specific root
     *
     * @param _proposalId proposal id
     * @param _amountToSlash Amount of slashing tokens
     * @param _root Root address from which slashing will be performed
     * @return succes of the operation
     */
    function applySlashing(
        uint256 _proposalId,
        address _root,
        uint256 _amountToSlash
    ) external onlyRootNodesSlashingVoting returns (bool) {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        uint256 _totalAmountToSlash = _amountToSlash;
        uint256 _stake = rootNodesStakes[_root];
        if (_stake < _amountToSlash) {
            _totalAmountToSlash = _stake;
        }

        VotingLockInfo memory _votingLockInfo = _votingWeightProxy.getLockInfo(address(this), _root);

        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;
        uint256 _remainingStake = _stake - _totalLockedAmount;

        if (_remainingStake < _totalAmountToSlash) {
            _votingWeightProxy.forceUnlock(_root, _totalAmountToSlash - (_remainingStake));
        }

        uint256 indexOfStake = indexerOfStakes[_root];
        rootNodesStakes[_root] = _stake - _totalAmountToSlash;
        stakes[indexOfStake].value -= _totalAmountToSlash;
        _popStake(_root);
        _checkInvariant();

        ASlashingEscrow _rootNodeSlashingEscrow = ASlashingEscrow(
            payable(registry.mustGetAddress(RKEY__ROOT_NODES_SLASHING_ESCROW))
        );

        return _rootNodeSlashingEscrow.open{value: _totalAmountToSlash}(_proposalId);
    }

    /**
     * @notice withdraw is used to withdraw tokens from the root account to the user's address
     *
     * @dev Withdrawal of the announced amount is impossible if this amount is locked.
     * When trying to withdraw the amount, there may be an attempt to unblock the missing difference.
     *
     * @param _amount The amount of tokens that the user wants to withdraw
     * @param _payTo The address to which the amount will be withdrawn
     * @return success of the operation
     */
    function withdraw(uint256 _amount, address payable _payTo) external returns (bool) {
        require(
            withdrawals[msg.sender].endTime <= block.timestamp,
            "[QEC-002003]-Not enough time has elapsed since the announcement of the withdrawal."
        );

        require(
            withdrawals[msg.sender].amount >= _amount,
            "[QEC-002004]-Cannot withdraw more than the announced amount."
        );

        uint256 _pendingSlashingAmount = _calculatePendingSlashingsAmount(msg.sender);
        uint256 _minimalBalance = getMinimumBalance(msg.sender, block.timestamp);

        require(
            rootNodesStakes[msg.sender] - _amount >= _minimalBalance,
            "[QEC-002013]-Withdrawal prevented by timelock(s)."
        );

        require(
            rootNodesStakes[msg.sender] - _amount >= _pendingSlashingAmount,
            "[QEC-002005]-The withdrawal is blocked by pending slashing proposals."
        );

        _unlock(_amount);

        rootNodesStakes[msg.sender] -= _amount;
        stakes[indexerOfStakes[msg.sender]].value -= _amount;
        _popStake(msg.sender);
        withdrawals[msg.sender].amount -= _amount;

        address _withdrawAddresses = registry.getAddress(RKEY__WITHDRAW_ADDRESSES);
        address _withdrawAddress = WithdrawAddresses(_withdrawAddresses).resolve(msg.sender);
        if (_withdrawAddress != msg.sender) {
            require(
                _payTo == _withdrawAddress || _payTo == msg.sender,
                "[QEC-002014]-Your withdrawals are limited to a specific withdraw address."
            );
            _payTo = payable(_withdrawAddress);
        }
        (bool success, ) = _payTo.call{value: _amount}("");
        require(success, "[QEC-002006]-Transfer of the withdrawal amount failed.");
        emit Withdrawn(msg.sender, _payTo, _amount);

        _checkInvariant();

        return true;
    }

    /**
     * @notice For usage between contracts.
     */
    function addSlashingProposal(address _root, uint256 _slashingProposalId) external onlyRootNodesSlashingVoting {
        pendingSlashingProposals[_root].push(_slashingProposalId);
    }

    /**
     * @notice Returns slashing proposal ids of given `_root`.
     * You should call `purgePendingSlashings` method before calling this one
     * in order to work truly pending proposals.
     * @param _root given root
     * @return slashing proposal ids
     */
    function getSlashingProposalIds(address _root) external view returns (uint256[] memory) {
        return pendingSlashingProposals[_root];
    }

    /**
     * @notice Returns the current list of members
     * @return current list of members
     */
    function getMembers() external view override returns (address[] memory) {
        return roots.getAddresses();
    }

    /**
     * @notice Checks if the user is in the list
     * @param _exp User to check
     * @return true if the check passed, false if not
     */
    function isMember(address _exp) external view override returns (bool) {
        return roots.contains(_exp);
    }

    /**
     * @notice Returns the current size of members list
     * @return list size
     */
    function getSize() external view override returns (uint256) {
        return roots.size();
    }

    /**
     * @notice Returns the maximum number of members in a list
     * @return maximum number of members in the list
     */
    function getLimit() external view override returns (uint256) {
        return _getLimit();
    }

    /**
     * @notice Returns stake of given `_root`
     * @param _root given root
     * @return stake of given root
     */
    function getRootNodeStake(address _root) external view returns (uint256) {
        return rootNodesStakes[_root];
    }

    /**
     * @notice Returns stakes of all roots
     * @return all roots stakes
     */
    function getStakes() external view returns (Stake[] memory) {
        return stakes;
    }

    /**
     * @notice getLockInfo is used to get information about the user's locked tokens
     *
     * @dev We receive error 028001 if this contract is not the source of the token lock
     *
     * @return information about the current lock of tokens as an object of the VotingLockInfo structure
     */
    function getLockInfo() external view returns (VotingLockInfo memory) {
        return _getLockInfo();
    }

    /**
     * @notice Purges all pending slashing proposals.
     * It reduces list of pending proposals, removing all non-pending proposals.
     * @param _root root whose proposals we will check.
     */
    function purgePendingSlashings(address _root) public {
        RootNodesSlashingVoting _rootsSlashingVoting = RootNodesSlashingVoting(_getRootNodesSlahingVotingAddress());
        uint256[] memory _pendingProposals = pendingSlashingProposals[_root];
        pendingSlashingProposals[_root] = new uint256[](0);

        for (uint256 i = 0; i < _pendingProposals.length; i++) {
            uint256 _currentId = _pendingProposals[i];

            if (_rootsSlashingVoting.slashingAffectsWithdrawal(_currentId)) {
                pendingSlashingProposals[_root].push(_currentId);
            }
        }
    }

    function _onTimeLockedDeposit(address _account, uint256 _amount) internal override {
        _addStake(_account, _amount);
        emit DepositedStake(msg.sender, _account, rootNodesStakes[_account]);
    }

    function _lock(uint256 _amount) private {
        _lock(_amount, msg.sender);
    }

    function _addRoot(address _rootAddress) private {
        require(roots.size() < _getLimit(), "[QEC-002000]-Maximum number of root nodes is reached.");

        if (!roots.contains(_rootAddress)) {
            roots.add(_rootAddress);
        }
    }

    function _lock(uint256 _amount, address _account) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        VotingLockInfo memory _votingLockInfo = _getLockInfo(_account);
        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;

        require(
            rootNodesStakes[_account] - _totalLockedAmount >= _amount,
            "[QEC-002011]-Total locked amount must not exceed stake."
        );

        _votingWeightProxy.lock(_account, _amount);

        _checkInvariant(_account);
    }

    function _announceUnlock(uint256 _amount, uint256 _newUnlockTime) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.announceUnlock(msg.sender, _amount, _newUnlockTime);

        _checkInvariant();
    }

    function _addStake(address _account, uint256 _amount) private {
        require(_amount > 0, "[QEC-002012]-Additional stake must not be zero.");

        if (rootNodesStakes[_account] == 0) {
            rootNodesStakes[_account] = _amount;
            stakes.push(Stake(_account, _amount));
            indexerOfStakes[_account] = stakes.length - 1;
        } else {
            rootNodesStakes[_account] += _amount;
            stakes[indexerOfStakes[_account]].value = rootNodesStakes[_account];
        }

        _lock(_amount, _account);
    }

    function _popStake(address _account) private {
        if (rootNodesStakes[_account] == 0) {
            uint256 lastIndex = stakes.length - 1;
            uint256 toDeleteIndex = indexerOfStakes[_account];
            stakes[toDeleteIndex] = stakes[lastIndex];
            indexerOfStakes[stakes[lastIndex].root] = toDeleteIndex;
            stakes.pop();
        }
    }

    function _unlock(uint256 _amount) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.unlock(msg.sender, _amount);

        _checkInvariant();
    }

    function _calculatePendingSlashingsAmount(address _root) private returns (uint256) {
        RootNodesSlashingVoting rootsSlashingVoting = RootNodesSlashingVoting(_getRootNodesSlahingVotingAddress());
        uint256 _originalEndTime = withdrawals[_root].endTime;
        uint256 _pendingSlashingAmount = 0;

        purgePendingSlashings(_root);

        uint256[] memory _pendingProposals = pendingSlashingProposals[_root];

        for (uint256 i = 0; i < _pendingProposals.length; i++) {
            uint256 _currentId = _pendingProposals[i];

            if (rootsSlashingVoting.getProposalStartTime(_currentId) < _originalEndTime) {
                (, , , uint256 _amountToSlash) = rootsSlashingVoting.proposals(_currentId);
                _pendingSlashingAmount += _amountToSlash;
            }
        }
        return _pendingSlashingAmount;
    }

    function _getLimit() private view returns (uint256) {
        return IParameters(_getConstitutionParametersAddress()).getUint("constitution.maxNRootNodes");
    }

    function _getLockInfo(address _account) private view returns (VotingLockInfo memory) {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        return _votingWeightProxy.getLockInfo(address(this), _account);
    }

    function _getLockInfo() private view returns (VotingLockInfo memory) {
        return _getLockInfo(msg.sender);
    }

    function _checkInvariant(address _account) private view {
        VotingLockInfo memory _votingLockInfo = _getLockInfo(_account);
        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;

        assert(rootNodesStakes[_account] >= _totalLockedAmount);
    }

    function _checkInvariant() private view {
        _checkInvariant(msg.sender);
    }

    function _getRootNodesSlahingVotingAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES_SLASHING_VOTING);
    }

    function _getConstitutionParametersAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS);
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }
}
