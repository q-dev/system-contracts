// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../interfaces/IContractRegistry.sol";
import "../common/AddressStorage.sol";
import "./IParameters.sol";
import "./IVoting.sol";
import "../common/Globals.sol";
import "./IPanel.sol";
import "../tokeneconomics/PushPayments.sol";

/**
 * @title Slashing Escrow
 * @notice Abstract contract to object slashing
 */
abstract contract ASlashingEscrow is Initializable {
    enum ArbitrationStatus {
        NONE,
        OPEN,
        ACCEPTED,
        PENDING,
        DECIDED,
        EXECUTED
    }

    struct ArbitrationParams {
        uint256 slashedAmount;
        uint256 objectionEndTime;
        uint256 appealEndTime;
    }

    struct Decision {
        bool notAppealed;
        address proposer;
        AddressStorage confirmers;
        string externalReference;
        uint256 percentage;
        uint256 confirmationCount;
        uint256 endDate;
        bytes32 hash;
    }

    struct DecisionStats {
        uint256 confirmationCount;
        uint256 requiredConfirmations;
        uint256 currentConfirmationPercentage;
    }

    struct ArbitrationInfo {
        string remark;
        string proposerRemark;
        ArbitrationParams params;
        Decision decision;
        bool executed;
        bool appealConfirmed;
    }

    IContractRegistry internal registry;

    string public associatedContractKey;
    string public slashingVotingContractKey;
    string public slashingOBJP;
    string public slashingAppealP;
    string public slashingVP;
    string public slashingReward;

    uint256 public aggregatedEscrowHoldings;

    mapping(uint256 => ArbitrationInfo) public arbitrationInfos;

    /**
     * @notice Triggered when the arbitration status switches to DECIDED
     * @param _id Proposal id
     */
    event ArbitrationDecided(uint256 _id);
    event RemarkUpdated(uint256 indexed _id, string _remark);
    event ProposerRemarkUpdated(uint256 indexed _id, string _proposerRemark, bool _appealConfirmed);

    /**
     * @notice Restricts only the associated contract can interact
     */
    modifier onlyAssociatedContract() {
        require(
            msg.sender == registry.mustGetAddress(associatedContractKey),
            "[QEC-027013]-Permission denied - only the associated contract has access."
        );
        _;
    }

    /**
     * @notice Restricts only the slashing victim can interact
     * @param _id Proposal id
     */
    modifier onlySlashingVictim(uint256 _id) {
        require(
            msg.sender == ISlashingVoting(registry.mustGetAddress(slashingVotingContractKey)).getSlashingVictim(_id),
            "[QEC-027014]-Permission denied - only the slashing target has access."
        );
        _;
    }

    /**
     * @notice Restricts only root node can interact
     */
    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-027015]-Permission denied - only the root node has access."
        );
        _;
    }

    /**
     * @notice Restricts only the decision proposer can interact
     * @param _id Proposal id
     */
    modifier onlyDecisionProposer(uint256 _id) {
        require(
            arbitrationInfos[_id].decision.proposer == msg.sender,
            "[QEC-027016]-Permission denied - only the decision proposer has access."
        );
        _;
    }

    /**
     * @notice Restricts only for existing proposals
     * @param _id Proposal id
     */
    modifier shouldExist(uint256 _id) {
        require(_getStatus(_id) != ArbitrationStatus.NONE, "[QEC-027017]-The arbitration does not exist.");
        _;
    }

    /**
     * @notice Restricts only for pending arbitration
     * @param _id Proposal id
     */
    modifier shouldBePending(uint256 _id) {
        require(_getStatus(_id) == ArbitrationStatus.PENDING, "[QEC-027018]-The given arbitration is not pending.");
        _;
    }

    /**
     * @notice Restricts only for open decision
     * @param _id Proposal id
     */
    modifier shouldBeOpenDecision(uint256 _id) {
        require(
            arbitrationInfos[_id].decision.endDate > block.timestamp,
            "[QEC-027019]-The arbitration is not open for decision."
        );
        _;
    }

    constructor() {}

    /**
     * @notice Opens the arbitration
     * @param _id Proposal id
     * @return true if everything went well
     */
    function open(uint256 _id) external payable onlyAssociatedContract returns (bool) {
        require(_getStatus(_id) == ArbitrationStatus.NONE, "[QEC-027000]-The arbitration already exists.");

        ArbitrationInfo memory _arbitrationInfo;
        _arbitrationInfo.params.objectionEndTime =
            block.timestamp +
            IParameters(_getConstitutionParametersAddress()).getUint(slashingOBJP);
        _arbitrationInfo.params.slashedAmount = msg.value;

        arbitrationInfos[_id] = _arbitrationInfo;

        aggregatedEscrowHoldings = aggregatedEscrowHoldings + _arbitrationInfo.params.slashedAmount;
        _checkEscrowHoldingsInvariant();

        return true;
    }

    /**
     * @notice Sets a remark for arbitration information
     * @param _id Proposal id
     * @param _remark Remark for arbitration information
     * @return true if everything went well
     */
    function setRemark(uint256 _id, string memory _remark)
        external
        shouldExist(_id)
        shouldBePending(_id)
        onlySlashingVictim(_id)
        returns (bool)
    {
        arbitrationInfos[_id].remark = _remark;
        emit RemarkUpdated(_id, _remark);

        return true;
    }

    /**
     * @notice Objects slashing
     * @param _id Proposal id
     * @param _remark Remark for arbitration information
     * @return appeal end time
     */
    function castObjection(uint256 _id, string memory _remark)
        external
        shouldExist(_id)
        onlySlashingVictim(_id)
        returns (uint256)
    {
        require(
            _getStatus(_id) == ArbitrationStatus.OPEN,
            "[QEC-027001]-The given arbitration is not open, objection failed."
        );

        uint256 _appealEndTime = block.timestamp +
            IParameters(_getConstitutionParametersAddress()).getUint(slashingAppealP);
        arbitrationInfos[_id].params.appealEndTime = _appealEndTime;
        arbitrationInfos[_id].remark = _remark;
        emit RemarkUpdated(_id, _remark);

        return _appealEndTime;
    }

    /**
     * @notice A function that allows the slashing victim to confirm the appeal
     * @param _id Proposal id
     * @param _proposerRemark Remark for arbitration information
     * @param _appealConfirmed Flag that indicates whether the appeal is confirmed
     */
    function setProposerRemark(
        uint256 _id,
        string memory _proposerRemark,
        bool _appealConfirmed
    ) external shouldExist(_id) shouldBePending(_id) {
        require(
            msg.sender == ISlashingVoting(registry.mustGetAddress(slashingVotingContractKey)).getSlashingProposer(_id),
            "[QEC-027020]-Permission denied - only the original proposer has access."
        );
        arbitrationInfos[_id].proposerRemark = _proposerRemark;
        arbitrationInfos[_id].appealConfirmed = _appealConfirmed;

        emit ProposerRemarkUpdated(_id, _proposerRemark, _appealConfirmed);
    }

    /**
     * @notice Proposes the decision about slashing
     * @param _id Proposal id
     * @param _percentage Percentage of slashing amount
     * @param _notAppealed Flag means that appeal can be filed
     * @param _reference External reference
     * @return true if everything went well
     */
    function proposeDecision(
        uint256 _id,
        uint256 _percentage,
        bool _notAppealed,
        string memory _reference
    ) external onlyRoot shouldExist(_id) shouldBePending(_id) returns (bool) {
        ArbitrationInfo storage _arbitrationInfo = arbitrationInfos[_id];

        if (_arbitrationInfo.decision.proposer != address(0)) {
            require(
                _arbitrationInfo.decision.endDate < block.timestamp,
                "[QEC-027002]-The current decision period is not over."
            );
            require(
                _arbitrationInfo.decision.proposer != msg.sender,
                "[QEC-027003]-The caller has already proposed the previous decision."
            );
        }

        uint256 _decimal = getDecimal();

        require(
            _percentage >= 0 && _percentage <= _decimal,
            "[QEC-027004]-Invalid percentage value, failed to propose a decision."
        );

        if (_notAppealed) {
            require(
                _arbitrationInfo.params.appealEndTime < block.timestamp,
                "[QEC-027005]-The appeal period is not over."
            );
            require(
                _percentage == _decimal,
                "[QEC-027006]-If the no appeal was submitted, the slashing percentage should be 100%."
            );
        }

        _arbitrationInfo.decision.proposer = msg.sender;
        _arbitrationInfo.decision.confirmationCount = 0;
        _arbitrationInfo.decision.percentage = _percentage;
        _arbitrationInfo.decision.notAppealed = _notAppealed;
        _arbitrationInfo.decision.externalReference = _reference;
        _arbitrationInfo.decision.endDate =
            block.timestamp +
            IParameters(_getConstitutionParametersAddress()).getUint(slashingVP);
        _arbitrationInfo.decision.hash = _decisionHash(_arbitrationInfo.decision);

        if (_arbitrationInfo.decision.confirmers == AddressStorage(address(0))) {
            _arbitrationInfo.decision.confirmers = new AddressStorage();
            _arbitrationInfo.decision.confirmers.initialize(new address[](0));
        } else {
            _arbitrationInfo.decision.confirmers.clear();
        }

        _confirmDecision(_id);
        return true;
    }

    /**
     * @notice Confirms a decision about slashing
     * @param _id Proposal id
     * @param _hash keccak256 of next Decision's feilds: notAppealed, proposer, externalReference, percentage, endDate
     * @return true if everything went well
     */
    function confirmDecision(uint256 _id, bytes32 _hash)
        external
        onlyRoot
        shouldExist(_id)
        shouldBeOpenDecision(_id)
        returns (bool)
    {
        bool _r = _confirmDecision(_id);
        require(arbitrationInfos[_id].decision.hash == _hash, "[QEC-027021]-Hash of decision isn't valid.");
        return _r;
    }

    /**
     * @notice Recalls a proposed decision about slashing
     * @param _id Proposal id
     * @return true if everything went well
     */
    function recallProposedDecision(uint256 _id)
        external
        shouldExist(_id)
        shouldBePending(_id)
        onlyDecisionProposer(_id)
        shouldBeOpenDecision(_id)
        returns (bool)
    {
        arbitrationInfos[_id].decision.endDate = block.timestamp;

        return true;
    }

    /**
     * @notice Executes slashing decision
     * @param _id Proposal id
     * @return true if everything went well
     */
    function execute(uint256 _id) external shouldExist(_id) returns (bool) {
        ArbitrationStatus _status = _getStatus(_id);
        require(
            _status == ArbitrationStatus.DECIDED || _status == ArbitrationStatus.ACCEPTED,
            "[QEC-027009]-The arbitration is still undecided. decision."
        );

        arbitrationInfos[_id].executed = true;

        ArbitrationInfo memory _arbitrationInfo = arbitrationInfos[_id];
        ISlashingVoting _slashingVoting = ISlashingVoting(registry.mustGetAddress(slashingVotingContractKey));

        uint256 _justifiedAmount = _arbitrationInfo.params.slashedAmount;
        if (_status == ArbitrationStatus.DECIDED) {
            _justifiedAmount = (_justifiedAmount * _arbitrationInfo.decision.percentage) / getDecimal();
        }

        bool _success = false;
        PushPayments _pushPayments = PushPayments(registry.mustGetAddress(RKEY__PUSH_PAYMENTS));

        // When we transfer the unjustified slashing amount back to the slashing victim
        if (_arbitrationInfo.params.slashedAmount > _justifiedAmount) {
            _pushPayments.safeTransferTo{value: _arbitrationInfo.params.slashedAmount - (_justifiedAmount)}(
                _slashingVoting.getSlashingVictim(_id)
            );
            aggregatedEscrowHoldings -= _arbitrationInfo.params.slashedAmount - _justifiedAmount;
        }

        // When we don't have a justified amount
        if (_justifiedAmount == 0) {
            return true;
        }

        IParameters _parameters = IParameters(_getConstitutionParametersAddress());
        uint256 _reward = (_justifiedAmount * _parameters.getUint(slashingReward)) / getDecimal();

        _pushPayments.safeTransferTo{value: _reward}(_slashingVoting.getSlashingProposer(_id));
        aggregatedEscrowHoldings -= _reward;

        (_success, ) = payable(registry.mustGetAddress(RKEY__DEFAULT_ALLOCATION_PROXY)).call{
            value: _justifiedAmount - _reward
        }("");
        aggregatedEscrowHoldings -= _justifiedAmount - _reward;

        require(_success, "[QEC-027012]-Failed to transfer the slashing amount for community distribution.");

        _checkEscrowHoldingsInvariant();

        return true;
    }

    /**
     * @notice Returns status of the arbitration for the given id
     * @param _id Proposal id
     * @return status from the ArbitrationStatus enum
     */
    function getStatus(uint256 _id) external view returns (ArbitrationStatus) {
        return _getStatus(_id);
    }

    /**
     * @notice Returns the proposal base structure for the given id
     * @param _id Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _id) external view returns (ISlashingVoting.BaseProposal memory) {
        return ISlashingVoting(registry.mustGetAddress(slashingVotingContractKey)).getProposal(_id);
    }

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual initializer {
        registry = IContractRegistry(_registry);
    }

    function hasAlreadyConfirmedDecision(uint256 _id, address confirmer) public view returns (bool) {
        return arbitrationInfos[_id].decision.confirmers.contains(confirmer);
    }

    /**
     * @notice Returns stats of the arbitration for the given proposal id
     * @param _id Proposal id
     * @return DecisionStats structure
     */
    function getDecisionStats(uint256 _id) public view shouldExist(_id) returns (DecisionStats memory) {
        ArbitrationInfo memory _arbitrationInfo = arbitrationInfos[_id];
        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        DecisionStats memory _stats;

        _stats.confirmationCount = _arbitrationInfo.decision.confirmationCount;
        _stats.currentConfirmationPercentage =
            (_arbitrationInfo.decision.confirmationCount * getDecimal()) /
            _rootsCount;
        _stats.requiredConfirmations = _rootsCount / 2 + 1;

        return _stats;
    }

    /**
     * @notice Confirms a decision about slashing
     * @param _id Proposal id
     * @return true if everything went well
     */
    function _confirmDecision(uint256 _id) private returns (bool) {
        require(_getStatus(_id) != ArbitrationStatus.DECIDED, "[QEC-027007]-This arbitration is already decided.");
        require(!hasAlreadyConfirmedDecision(_id, msg.sender), "[QEC-027008]-The caller has already confirmed.");
        Decision storage _decision = arbitrationInfos[_id].decision;

        _decision.confirmers.add(msg.sender);
        _decision.confirmationCount = _decision.confirmers.size();

        if (_getStatus(_id) == ArbitrationStatus.DECIDED) {
            emit ArbitrationDecided(_id);
        }

        return true;
    }

    function _decisionHash(Decision storage _decision) private view returns (bytes32) {
        return
            keccak256(
                abi.encode(
                    _decision.notAppealed,
                    _decision.proposer,
                    _decision.externalReference,
                    _decision.percentage,
                    _decision.endDate
                )
            );
    }

    /**
     * @notice Returns status of the arbitration for the given proposal id
     * @param _id Proposal id
     * @return status from the ArbitrationStatus enum
     */
    function _getStatus(uint256 _id) private view returns (ArbitrationStatus) {
        ArbitrationInfo memory _arbitrationInfo = arbitrationInfos[_id];

        if (_arbitrationInfo.params.objectionEndTime == 0) {
            return ArbitrationStatus.NONE;
        }

        if (_arbitrationInfo.executed) {
            return ArbitrationStatus.EXECUTED;
        }

        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        uint256 _decimal = getDecimal();

        if ((_arbitrationInfo.decision.confirmationCount * _decimal) / _rootsCount > _decimal / 2) {
            return ArbitrationStatus.DECIDED;
        }

        if (_arbitrationInfo.params.appealEndTime != 0) {
            return ArbitrationStatus.PENDING;
        }

        if (_arbitrationInfo.params.objectionEndTime > block.timestamp) {
            return ArbitrationStatus.OPEN;
        }

        return ArbitrationStatus.ACCEPTED;
    }

    function _checkEscrowHoldingsInvariant() private view {
        assert(address(this).balance >= aggregatedEscrowHoldings);
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _getConstitutionParametersAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS);
    }
}
