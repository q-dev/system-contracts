// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

contract AccountAliases {
    mapping(address => mapping(uint256 => address)) public aliases;
    mapping(address => address) public reverseAliases;

    event AliasUpdated(address indexed _main, address indexed _alias, uint256 indexed role);
    event Reserved(address indexed _main, address indexed _alias);

    /**
     * @notice Creates or updates alias
     * @param _alias address for aliasing
     * @param _role role of alias
     */
    function setAlias(address _alias, uint256 _role) external {
        require(reverseAliases[_alias] != address(0), "[QEC-040000]-Alias is not reserved.");
        require(reverseAliases[_alias] == msg.sender, "[QEC-040002]-Alias is reserved for another account.");
        aliases[msg.sender][_role] = _alias;
        reverseAliases[_alias] = msg.sender;
        emit AliasUpdated(msg.sender, _alias, _role);
    }

    /**
     * @notice reserves alias
     * @param _newOwner new owner of alias
     */
    function reserve(address _newOwner) external {
        reverseAliases[msg.sender] = _newOwner;
        emit Reserved(_newOwner, msg.sender);
    }

    /**
     * @notice resolve owners of aliases
     * @param _alias array of alias' addresses
     * @param _role array of roles(corresponding to _alias)
     * @return _main array of owners of aliases
     */
    function resolveBatchReverse(address[] memory _alias, uint256[] memory _role)
        external
        view
        returns (address[] memory _main)
    {
        _checkLength(_alias.length, _role.length);
        _main = new address[](_alias.length);
        for (uint256 i = 0; i < _alias.length; i++) {
            _main[i] = resolveReverse(_alias[i], _role[i]);
        }
    }

    /**
     * @notice resolve aliases for main
     * @param _main array of owners of aliases
     * @param _role array of roles(corresponding to _main)
     * @return _alias array of alias' addresses
     */
    function resolveBatch(address[] memory _main, uint256[] memory _role)
        external
        view
        returns (address[] memory _alias)
    {
        _checkLength(_main.length, _role.length);
        _alias = new address[](_main.length);
        for (uint256 i = 0; i < _main.length; i++) {
            _alias[i] = resolve(_main[i], _role[i]);
        }
    }

    /**
     * @notice resolve alias for main
     *         return main address if alias not setuped
     * @param _main owner
     * @param _role alias' role
     * @return _alias setuped alias
     */
    function resolve(address _main, uint256 _role) public view returns (address _alias) {
        _alias = aliases[_main][_role];
        if (reverseAliases[_alias] != _main) _alias = _main;
    }

    /**
     * @notice resolve owner of alias
     *         return alias address if alias not setuped
     * @param _alias address of alias
     * @param _role alias' role
     * @return _main owner of alias
     */
    function resolveReverse(address _alias, uint256 _role) public view returns (address _main) {
        _main = reverseAliases[_alias];
        if (aliases[_main][_role] != _alias) _main = _alias;
    }

    /// @dev for checking correspondence of arrays
    function _checkLength(uint256 _aLen, uint256 _rolesLen) internal pure {
        require(_aLen == _rolesLen, "[QEC-040001]-Length of addresses and role arrays must be equal.");
    }
}
