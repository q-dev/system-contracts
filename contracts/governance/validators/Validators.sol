// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IParameters.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "../../tokeneconomics/ITokenLock.sol";
import "./ValidatorsSlashingVoting.sol";
import "../../interfaces/IVotingWeightProxy.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../common/AddressStorageStakesSorted.sol";
import "../../common/AddressStorageStakes.sol";
import "../ASlashingEscrow.sol";
import "../../tokeneconomics/ValidationRewardPools.sol";
import "../../common/Globals.sol";
import "../../tokeneconomics/ATimeLockBase.sol";
import "../../tokeneconomics/WithdrawAddresses.sol";

// Validators implements logic of DPOS
contract Validators is Initializable, ATimeLockBase {
    struct ValidatorAmount {
        address validator;
        uint256 amount;
    }

    struct Withdrawal {
        uint256 endTime;
        uint256 amount;
    }

    struct ValidatorInfo {
        Withdrawal withdrawal;
        uint256[] pendingSlashingProposals;
    }

    IContractRegistry private registry;
    // validatorsAddressesArray (long list)
    AddressStorageStakes public longList;
    // short list (sorted)
    AddressStorageStakesSorted shortList;

    address[] snapshot;
    uint256 snapshottingBlock;

    mapping(address => ValidatorInfo) public validatorsInfos;

    event ValidatorEnteredShortList(address indexed _validator);
    event ValidatorDroppedFromShortList(address indexed _validator);

    /**
     * @notice Restricts only the ValidatorsSlashingVoting contract can interact
     */
    modifier onlyValidatorsSlashingVoting() {
        require(
            msg.sender == _getSlashingVotingAddress(),
            "[QEC-005013]-Permission denied - only the ValidatorsSlashingVoting contract has access."
        );
        _;
    }

    constructor() {}

    function initialize(
        address _registry,
        address _AddressStorageStakesSorted,
        address _AddressStorageStakes
    ) external initializer {
        registry = IContractRegistry(_registry);
        shortList = AddressStorageStakesSorted(_AddressStorageStakesSorted);
        longList = AddressStorageStakes(_AddressStorageStakes);
    }

    /**
     * @notice Commits a stake of Q as collateral.
     */
    function commitStake() external payable {
        _addStake(msg.sender, msg.value);
    }

    /**
     * @notice Adds a validator to the shortlist
     * @return true if validator has been added to shortlist
     */
    function enterShortList() external returns (bool) {
        AddressStorageStakesSorted _shortList = shortList;
        require(!_shortList.contains(msg.sender), "[QEC-005003]-The validator is already on the short list.");

        uint256 _maxLen = getShortListMaxLength();
        uint256 _senderAccountableTotalStake = getAccountableTotalStake(msg.sender);

        require(
            _maxLen != _shortList.listSize() || _senderAccountableTotalStake > _getLowestAccountableTotalStake(),
            "[QEC-005017]-Not enough stake to enter the short list."
        );

        _shortList.addAddr(msg.sender, _senderAccountableTotalStake);
        uint256 _actualSize = _shortList.listSize();

        while (_maxLen < _actualSize) {
            address _droppedValidator = _shortList.removeLast();
            if (msg.sender != _droppedValidator) {
                emit ValidatorDroppedFromShortList(_droppedValidator);
            }

            _actualSize = _shortList.listSize();
        }

        if (!_shortList.contains(msg.sender)) {
            return false;
        }

        emit ValidatorEnteredShortList(msg.sender);
        return true;
    }

    /**
     * @notice Starts the target time window with duration length valWithdrawP for an desired amount to withdraw
     * @param _amount Withdrawal amount
     */
    function announceWithdrawal(uint256 _amount) external {
        uint256 withdrawalTime = block.timestamp +
            IParameters(_getConstitutionParametersAddress()).getUint("constitution.valWithdrawP");

        AddressStorageStakes _longList = longList;
        (, uint256 _stake, ) = _longList.addrStake(msg.sender);
        require(_stake >= _amount, "[QEC-005005]-Announced withdrawal must not exceed current stake.");

        _announceUnlock(_amount, withdrawalTime);

        validatorsInfos[msg.sender].withdrawal = Withdrawal(withdrawalTime, _amount);

        if (shortList.contains(msg.sender)) {
            shortList.updateStake(msg.sender, getAccountableTotalStake(msg.sender));
        }
    }

    /**
     * @notice withdraw is used to withdraw tokens from the validators account to the user's address
     *
     * @dev Withdrawal of the announced amount is impossible if this amount is locked.
     * When trying to withdraw the amount, there may be an attempt to unlock the missing difference.
     *
     * @param _amount The amount of tokens that the user wants to withdraw
     * @param _payTo The address to which the amount will be withdrawn
     *
     * @return true if the withdrawal of tokens was successful
     */
    function withdraw(uint256 _amount, address payable _payTo) external returns (bool) {
        Withdrawal memory _currentWithdrawal = validatorsInfos[msg.sender].withdrawal;
        require(
            _currentWithdrawal.endTime <= block.timestamp,
            "[QEC-005007]-Not enough time has elapsed since the announcement of the withdrawal."
        );
        require(_currentWithdrawal.amount >= _amount, "[QEC-005008]-Cannot withdraw more than the announced amount.");

        uint256 _pendingSlashingAmount = _calculatePendingSlashingsAmount(msg.sender, _currentWithdrawal.endTime);
        uint256 _validatorStake = getSelfStake(msg.sender);
        uint256 _minimumBalance = getMinimumBalance(msg.sender, block.timestamp);

        require(_validatorStake - _amount >= _minimumBalance, "[QEC-005015]-Withdrawal prevented by timelock(s).");

        require(
            _validatorStake - _amount >= _pendingSlashingAmount,
            "[QEC-005009]-The withdrawal is blocked by pending slashing proposals."
        );

        _unlock(_amount);

        _currentWithdrawal.amount = _currentWithdrawal.amount - _amount;

        validatorsInfos[msg.sender].withdrawal = _currentWithdrawal;

        longList.sub(msg.sender, _amount);

        AddressStorageStakesSorted _shortList = shortList;
        if (_shortList.contains(msg.sender)) {
            _shortList.updateStake(msg.sender, getAccountableTotalStake(msg.sender));
        }

        address _withdrawAddresses = registry.getAddress(RKEY__WITHDRAW_ADDRESSES);
        address _withdrawAddress = WithdrawAddresses(_withdrawAddresses).resolve(msg.sender);
        if (_withdrawAddress != msg.sender) {
            require(
                _payTo == _withdrawAddress || _payTo == msg.sender,
                "[QEC-005016]-Your withdrawals are limited to a specific withdraw address."
            );
            _payTo = payable(_withdrawAddress);
        }
        (bool success, ) = _payTo.call{value: _amount}("");
        require(success, "[QEC-005010]-Transfer of the withdrawal amount failed.");

        _checkInvariant();

        return true;
    }

    /**
     * @notice Updates validators list
     * @return true if the function succeeded, false otherwise
     */
    function makeSnapshot() external returns (bool) {
        uint256 epochLength = IParameters(_getConstitutionParametersAddress()).getUint(
            "constitution.cliqueEpochLength"
        );

        require(
            block.number + 1 >= epochLength + snapshottingBlock,
            "[QEC-005011]-Not enough time has elapsed since the last epoch, failed to make the snapshot."
        );

        snapshot = shortList.getAddresses();
        snapshottingBlock += epochLength;

        return true;
    }

    /**
     * @notice applySlashing is used to slash a specific amount on a specific validator
     *
     * @param _proposalId Id of the proposal
     * @param _validator Validator address from which slashing will be performed
     * @param _amountToSlash Amount of slashing tokens
     * @return success of the operation.
     */
    function applySlashing(
        uint256 _proposalId,
        address _validator,
        uint256 _amountToSlash
    ) external onlyValidatorsSlashingVoting returns (bool) {
        uint256 _totalAmountToSlash = _amountToSlash;
        uint256 _stake = getSelfStake(_validator);

        if (_stake < _amountToSlash) {
            _totalAmountToSlash = _stake;
        }

        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        VotingLockInfo memory _votingLockInfo = _votingWeightProxy.getLockInfo(address(this), _validator);

        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;
        uint256 _remainingStake = _stake - _totalLockedAmount;

        if (_remainingStake < _totalAmountToSlash) {
            _votingWeightProxy.forceUnlock(_validator, _totalAmountToSlash - _remainingStake);
        }

        if (longList.contains(_validator)) {
            longList.sub(_validator, _totalAmountToSlash);
        }

        _stake = getSelfStake(_validator);

        if (_stake < validatorsInfos[_validator].withdrawal.amount) {
            validatorsInfos[_validator].withdrawal.amount = _stake;
        }

        if (shortList.contains(_validator)) {
            shortList.updateStake(_validator, getAccountableTotalStake(_validator));
        }

        ASlashingEscrow _validatorSlashingEscrow = ASlashingEscrow(
            payable(registry.mustGetAddress(RKEY__VALIDATORS_SLASHING_ESCROW))
        );

        return _validatorSlashingEscrow.open{value: _totalAmountToSlash}(_proposalId);
    }

    /**
     * @notice Adds slashing proposal to the list of pending slashing proposals
     * @param _validator Validator's address
     * @param _slashingProposalId Validator's address
     */
    function addSlashingProposal(address _validator, uint256 _slashingProposalId)
        external
        onlyValidatorsSlashingVoting
    {
        validatorsInfos[_validator].pendingSlashingProposals.push(_slashingProposalId);
    }

    /**
     * @notice Sums up all pending slashings until _endTime
     * @param _validator Validator's address
     * @param _endTime Time until which proposals will be taken into account
     * @return Sum of all pending slashings
     */
    function calculatePendingSlashingsAmount(address _validator, uint256 _endTime) external returns (uint256) {
        return _calculatePendingSlashingsAmount(_validator, _endTime);
    }

    /**
     * @notice Retrieves information about the current shortlist
     * @return an array of structures that contain the address of the validator and its amount
     */
    function getValidatorShortList() external view returns (ValidatorAmount[] memory) {
        AddressStorageStakesSorted _shortList = shortList;
        address[] memory addresses = _shortList.getAddresses();

        ValidatorAmount[] memory validatorAmount = new ValidatorAmount[](addresses.length);
        for (uint64 i = 0; i < addresses.length; ++i) {
            validatorAmount[i] = ValidatorAmount(
                addresses[i],
                _shortList.addrStake(addresses[i]) // accountableTotalStake already in shortList
            );
        }

        return validatorAmount;
    }

    /**
     * @notice Retrieves user's delegated stake from a validator contract
     * @param _addr user address
     * @return user's delegated stake
     */
    function getValidatorDelegatedStake(address _addr) external view returns (uint256) {
        (, , uint256 delegatedStake) = longList.addrStake(_addr);
        return delegatedStake;
    }

    /**
     * @notice Сhecks if the user has committed stake
     * @param _userAddr User's address
     * @return true if user has committed stake
     */
    function isInLongList(address _userAddr) external view returns (bool) {
        return longList.contains(_userAddr);
    }

    /**
     * @notice Checks if validator exists
     * @param _validator Validator's address
     * @return true if validator exists
     */
    function isInShortList(address _validator) external view returns (bool) {
        return shortList.contains(_validator);
    }

    /**
     * @notice Returns the list of validator's slashing proposals Ids
     * @param _validator Validator's address
     * @return Array of Ids
     */
    function getSlashingProposalIds(address _validator) external view returns (uint256[] memory) {
        return validatorsInfos[_validator].pendingSlashingProposals;
    }

    /**
     * @notice getLockInfo is used to get information about the user's locked tokens
     *
     * @dev We receive error 028001 if this contract is not the source of the token lock
     *
     * @return information about the current lock of tokens as an object of the VotingLockInfo structure
     */
    function getLockInfo() external view returns (VotingLockInfo memory) {
        return _getLockInfo();
    }

    /**
     * @notice Returns list of validators
     * @return Array with validator addresses
     */
    function getValidatorsList() external view returns (address[] memory) {
        return snapshot;
    }

    /**
     * @notice Returns the user's total stake to the validator contract
     * @param _addr user address
     * @return user's total stake
     */
    function getValidatorTotalStake(address _addr) external view returns (uint256) {
        (, uint256 stake, uint256 delegatedStake) = longList.addrStake(_addr);
        return stake + delegatedStake - validatorsInfos[_addr].withdrawal.amount;
    }

    /**
     * @notice Purges pending slashings
     * @param _validator Validator's address
     */
    function purgePendingSlashings(address _validator) public {
        ValidatorsSlashingVoting _validatorsSlashingVoting = ValidatorsSlashingVoting(_getSlashingVotingAddress());

        uint256[] memory _pendingProposals = validatorsInfos[_validator].pendingSlashingProposals;
        validatorsInfos[_validator].pendingSlashingProposals = new uint256[](0);

        for (uint256 i = 0; i < _pendingProposals.length; i++) {
            uint256 _currentId = _pendingProposals[i];

            if (_validatorsSlashingVoting.slashingAffectsWithdrawal(_currentId)) {
                validatorsInfos[_validator].pendingSlashingProposals.push(_currentId);
            }
        }
    }

    /**
     * @notice For usage between contracts.
     * @param _delegateTo user, whose delegation info will be updated
     */
    function refreshDelegatedStake(address _delegateTo) public {
        longList.setDelegated(
            _delegateTo,
            ValidationRewardPools(registry.mustGetAddress(RKEY__VALIDATION_REWARDS_POOL)).getDelegatedStake(_delegateTo)
        );

        AddressStorageStakesSorted _shortList = shortList;
        if (_shortList.contains(_delegateTo)) {
            _shortList.updateStake(_delegateTo, getAccountableTotalStake(_delegateTo));
        }
    }

    /**
     * @notice Retrieves information about the maximum size of the shortlist
     * @return the maximum size of the shortlist
     */
    function getShortListMaxLength() public view returns (uint256) {
        IParameters _constitutionParams = IParameters(_getConstitutionParametersAddress());
        uint256 aValidators = _constitutionParams.getUint("constitution.maxNValidators");
        uint256 sValidators = _constitutionParams.getUint("constitution.maxNStandbyValidators");
        uint256 bValidators = sValidators;

        return aValidators + sValidators + bValidators;
    }

    /**
     * @notice Retrieves the user's accountable self stake from a validator contract
     * @param _addr user address
     * @return user's accountable self stake
     */
    function getAccountableSelfStake(address _addr) public view returns (uint256) {
        (, uint256 stake, ) = longList.addrStake(_addr);
        return stake - validatorsInfos[_addr].withdrawal.amount;
    }

    /**
     * @notice Retrieves user's self stake in the validator contract
     * @param _addr user address
     * @return user's own stake
     */
    function getSelfStake(address _addr) public view returns (uint256) {
        (, uint256 stake, ) = longList.addrStake(_addr);
        return stake;
    }

    /**
     * @notice Retrieves user's accountable total stake from a validator contract
     * @param _addr user address
     * @return user's accountable total stake
     */
    function getAccountableTotalStake(address _addr) public view returns (uint256) {
        (, uint256 _stake, uint256 _delegatedStake) = longList.addrStake(_addr);
        _stake = _stake - validatorsInfos[_addr].withdrawal.amount;

        // limit delegated stake
        uint256 _factor = IParameters(registry.mustGetAddress(RKEY__EPQFI_PARAMETERS)).getUint(
            "governed.EPQFI.stakeDelegationFactor"
        );
        uint256 _maxDelegatedStake = (_stake * _factor) / getDecimal();

        // according to the spec: min of total and factor*delegated
        uint256 _totalStake = _stake + _delegatedStake;
        if (_totalStake < _maxDelegatedStake) {
            return _totalStake;
        }

        return _maxDelegatedStake;
    }

    function _onTimeLockedDeposit(address _account, uint256 _amount) internal override {
        _addStake(_account, _amount);
    }

    function _lock(uint256 _amount, address _account) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        VotingLockInfo memory _votingLockInfo = _getLockInfo(_account);
        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;

        require(
            getSelfStake(_account) - _totalLockedAmount >= _amount,
            "[QEC-005014]-Total locked amount must not exceed stake."
        );

        _votingWeightProxy.lock(_account, _amount);

        _checkInvariant(_account);
    }

    function _lock(uint256 _amount) private {
        _lock(_amount, msg.sender);
    }

    function _announceUnlock(uint256 _amount, uint256 _newUnlockTime) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.announceUnlock(msg.sender, _amount, _newUnlockTime);

        _checkInvariant();
    }

    function _unlock(uint256 _amount) private {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        _votingWeightProxy.unlock(msg.sender, _amount);

        _checkInvariant();
    }

    function _addStake(address _account, uint256 _amount) private {
        require(_amount > 0, "[QEC-005000]-Additional stake must not be zero.");
        longList.add(_account, _amount);

        AddressStorageStakesSorted _shortList = shortList;
        if (_shortList.contains(_account)) {
            _shortList.updateStake(_account, getAccountableTotalStake(_account));
        }

        ValidationRewardPools(registry.mustGetAddress(RKEY__VALIDATION_REWARDS_POOL)).addCompoundRateKeeper(_account);

        _lock(_amount, _account);
    }

    /**
     * @notice Sums up all pending slashings until _endTime
     * @param _validator Validator's address
     * @param _endTime Time until which proposals will be taken into account
     * @return Sum of all pending slashings
     */
    function _calculatePendingSlashingsAmount(address _validator, uint256 _endTime) private returns (uint256) {
        ValidatorsSlashingVoting _validatorsSlashingVoting = ValidatorsSlashingVoting(_getSlashingVotingAddress());

        purgePendingSlashings(_validator);

        uint256[] memory _pendingProposals = validatorsInfos[_validator].pendingSlashingProposals;

        uint256 _pendingSlashingAmount = 0;
        for (uint256 i = 0; i < _pendingProposals.length; i++) {
            uint256 _currentId = _pendingProposals[i];

            if (_validatorsSlashingVoting.getProposalStartTime(_currentId) < _endTime) {
                (, , , uint256 _amountToSlash) = _validatorsSlashingVoting.proposals(_currentId);

                _pendingSlashingAmount = _pendingSlashingAmount + _amountToSlash;
            }
        }
        return _pendingSlashingAmount;
    }

    function _checkInvariant(address _account) private view {
        VotingLockInfo memory _votingLockInfo = _getLockInfo(_account);
        uint256 _totalLockedAmount = _votingLockInfo.lockedAmount + _votingLockInfo.pendingUnlockAmount;

        assert(getSelfStake(_account) >= _totalLockedAmount);
    }

    function _checkInvariant() private view {
        _checkInvariant(msg.sender);
    }

    function _getLockInfo(address _account) private view returns (VotingLockInfo memory) {
        IVotingWeightProxy _votingWeightProxy = IVotingWeightProxy(_getVotingWeightProxyAddress());

        return _votingWeightProxy.getLockInfo(address(this), _account);
    }

    function _getLockInfo() private view returns (VotingLockInfo memory) {
        return _getLockInfo(msg.sender);
    }

    // Getters for Global parameters

    function _getSlashingVotingAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VALIDATORS_SLASHING_VOTING);
    }

    function _getConstitutionParametersAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS);
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }

    /**
     * @notice address(1) `TAIL` in AddressStorageStakesSorted points to the last item in the sorted linked list, which
     * starts with the account with the highest stake and ends with the account with the lowest stake, so it
     * @return the account with the lowest stake in the short list
     */
    function _getLowestStakeAccount() private view returns (address) {
        return shortList.linkedList(address(1));
    }

    function _getLowestAccountableTotalStake() private view returns (uint256) {
        return getAccountableTotalStake(_getLowestStakeAccount());
    }
}
