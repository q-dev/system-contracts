// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../ASlashingEscrow.sol";

/**
 * @title Validator Slashing Escrow
 * @notice Used to object the validator slashing
 */
contract ValidatorSlashingEscrow is ASlashingEscrow {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ASlashingEscrow.initialize(_registry);

        associatedContractKey = RKEY__VALIDATORS;
        slashingVotingContractKey = RKEY__VALIDATORS_SLASHING_VOTING;
        slashingOBJP = "constitution.voting.valSlashingOBJP";
        slashingVP = "constitution.voting.valSlashingVP";
        slashingAppealP = "constitution.valSlashingAppealP";
        slashingReward = "constitution.valSlashingReward";
    }
}
