// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IParameters.sol";
import "../IVoting.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./Validators.sol";
import "../rootNodes/Roots.sol";
import "../../interfaces/IContractRegistry.sol";

/**
 * @title Validators Slashing Voting
 * @notice Used to vote for slashing for validator
 */
contract ValidatorsSlashingVoting is ISlashingVoting, Initializable {
    IContractRegistry private registry;

    mapping(uint256 => SlashingProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public voted;

    uint256 public proposalCount;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption);
    event ProposalExecuted(uint256 indexed _proposalId, address _candidate, uint256 _amountToSlash);

    /**
     * @notice Restricts only root node can interact
     */
    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-006004]-Permission denied - only root nodes have access."
        );
        _;
    }

    /**
     * @notice Restricts only for existing proposals
     * @param _proposalId Proposal id
     */
    modifier shouldExist(uint256 _proposalId) {
        require(getStatus(_proposalId) != ProposalStatus.NONE, "[QEC-006005]-The slashing proposal does not exist.");
        _;
    }

    /**
     * @notice Restricts only for not existing proposals
     * @param _root Proposer
     * @param _target Slashing target
     */
    modifier shouldBeUnique(address _root, address _target) {
        uint256[] memory proposalIds = Validators(_getValidatorsAddress()).getSlashingProposalIds(_target);
        for (uint256 i = 0; i < proposalIds.length; i++) {
            require(
                _root != proposals[proposalIds[i]].proposer,
                "[QEC-006007]-The slashing proposal with same creator and victim exists."
            );
        }
        _;
    }

    /**
     * @notice Restricts only for passed proposals
     * @param _proposalId Proposal id
     */
    modifier shouldBePassed(uint256 _proposalId) {
        require(getStatus(_proposalId) == ProposalStatus.PASSED, "[QEC-006006]-The slashing proposal has not passed.");
        _;
    }

    constructor() {}

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Creates proposal for voting
     * @param _remark Some message with details (may be link)
     * @param _candidate address of candidate for slashing
     * @param _percentage percentage of stake to slash
     * @return id of new proposal
     */
    function createProposal(
        string memory _remark,
        address _candidate,
        uint256 _percentage
    ) external onlyRoot shouldBeUnique(msg.sender, _candidate) returns (uint256) {
        require(
            0 < _percentage && _percentage <= getDecimal(),
            "[QEC-006002]-Invalid percentage parameter, slashing proposal creation failed."
        );

        Validators _validators = Validators(_getValidatorsAddress());
        require(
            _validators.getSelfStake(_candidate) > 0,
            "[QEC-006000]-The candidate does not have any stake to slash."
        );

        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        SlashingProposal memory _prop;
        _prop.base.remark = _remark;
        _prop.candidate = _candidate;
        _prop.proposer = msg.sender;
        _prop.amountToSlash = _calculateAmountToSlash(_percentage, _validators.getSelfStake(_candidate));
        _prop.base.params.votingStartTime = block.timestamp;
        _prop.base.params.votingEndTime = block.timestamp + constitution.getUint("constitution.voting.valSlashingVP");
        _prop.base.params.requiredQuorum = constitution.getUint("constitution.voting.valSlashingQRM");
        _prop.base.params.requiredMajority = constitution.getUint("constitution.voting.valSlashingRMAJ");
        _prop.base.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        uint256 _id = proposalCount;
        proposals[_id] = (_prop);

        _validators.addSlashingProposal(_candidate, _id);

        emit ProposalCreated(_id, _prop.base);

        proposalCount++;

        return _id;
    }

    /**
     * @notice Give a vote for the specified proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Give a vote against the specified proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Applies changes for specified proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override onlyRoot shouldBePassed(_proposalId) {
        SlashingProposal memory _prop = proposals[_proposalId];
        Validators validators = Validators(_getValidatorsAddress());

        proposals[_proposalId].base.executed = validators.applySlashing(
            _proposalId,
            _prop.candidate,
            _prop.amountToSlash
        );

        emit ProposalExecuted(_proposalId, _prop.candidate, _prop.amountToSlash);
    }

    /**
     * @notice Returns the base structure for the proposal of given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId].base;
    }

    /**
     * @notice Gets slashing victim from the proposal
     * @param _proposalId Proposal id
     * @return address of slashing victim
     */
    function getSlashingVictim(uint256 _proposalId) external view override shouldExist(_proposalId) returns (address) {
        return proposals[_proposalId].candidate;
    }

    /**
     * @notice Gets slashing proposer from the proposal
     * @param _proposalId Proposal id
     * @return address of slashing proposer
     */
    function getSlashingProposer(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (address)
    {
        return proposals[_proposalId].proposer;
    }

    /**
     * @notice Gets statistic for the proposal
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        SlashingProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.base.params.requiredQuorum;
        _stats.requiredMajority = _prop.base.params.requiredMajority;

        uint256 _votesCount = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        uint256 _currentQuorum = _calculatePercentage(_votesCount, _rootsCount);
        _stats.currentQuorum = _currentQuorum;

        uint256 _currentMajority = _calculatePercentage(_prop.base.counters.weightFor, _votesCount);
        _stats.currentMajority = _currentMajority;
        return _stats;
    }

    /**
     * @notice Specifies if is proposal pending
     * @param _proposalId Proposal id
     * @return boolean, true for pending
     */
    function slashingAffectsWithdrawal(uint256 _proposalId) external view returns (bool) {
        ProposalStatus _status = getStatus(_proposalId);
        return (_status != ProposalStatus.REJECTED &&
            _status != ProposalStatus.EXECUTED &&
            _status != ProposalStatus.EXPIRED);
    }

    /**
     * @notice Specifies the proposal`s start time
     * @param _proposalId Proposal id
     * @return unix TimeStamp start time
     */
    function getProposalStartTime(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.params.votingStartTime;
    }

    /**
     * @notice Returns the status of the given proposal
     * @param _id Proposal id
     * @return status of proposal
     */
    function getStatus(uint256 _id) public view returns (ProposalStatus) {
        SlashingProposal memory _prop = proposals[_id];

        if (_prop.base.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.base.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.base.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        uint256 _votesCount = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _votesCount);

        if (_actualMajority <= _prop.base.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        uint256 _rootsCount = IRoots(_getRootNodesAddress()).getSize();
        uint256 _actualQuorum = _calculatePercentage(_votesCount, _rootsCount);

        if (_actualQuorum < _prop.base.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp > _prop.base.params.votingEndTime + _prop.base.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _vote(uint256 _id, VotingOption _votingOption) private returns (bool) {
        require(getStatus(_id) == ProposalStatus.PENDING, "[QEC-006001]-Voting is only possible on PENDING proposals.");
        require(!voted[_id][msg.sender], "[QEC-006003]-The caller has already voted for the proposal.");

        if (_votingOption == VotingOption.FOR) {
            proposals[_id].base.counters.weightFor++;
        } else {
            proposals[_id].base.counters.weightAgainst++;
        }

        voted[_id][msg.sender] = true;

        emit UserVoted(_id, _votingOption);
        return true;
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _getValidatorsAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VALIDATORS);
    }

    function _calculatePercentage(uint256 _part, uint256 _amount) private pure returns (uint256) {
        if (_amount == 0) {
            return 0;
        }
        return (_part * getDecimal()) / _amount;
    }

    function _calculateAmountToSlash(uint256 _percentage, uint256 _stake) private pure returns (uint256) {
        return (_percentage * _stake) / getDecimal();
    }
}
