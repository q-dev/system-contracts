// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./Validators.sol";

contract ValidatorsMock is Validators {
    constructor() {}

    function setSnapshot(address[] memory _newSnapshot) public {
        snapshot = _newSnapshot;
    }
}
