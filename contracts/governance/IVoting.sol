// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

struct MembershipProposalDetails {
    address addressToAdd;
    address addressToRemove;
}

enum DelegationStatus {
    SELF,
    DELEGATED,
    PENDING
}

struct BaseVotingWeightInfo {
    uint256 ownWeight;
    address votingAgent;
    DelegationStatus delegationStatus;
    uint256 lockedUntil;
}

/**
 * @title Voting interface
 * @notice You can use this interface to create a basic voting contract
 */
interface IVoting {
    enum ProposalStatus {
        NONE,
        PENDING,
        REJECTED,
        ACCEPTED,
        PASSED,
        EXECUTED,
        OBSOLETE,
        EXPIRED
    }
    enum VotingOption {
        NONE,
        FOR,
        AGAINST
    }

    struct VotingParams {
        uint256 votingStartTime;
        uint256 votingEndTime;
        uint256 vetoEndTime;
        uint256 proposalExecutionP;
        uint256 requiredQuorum;
        uint256 requiredMajority;
        uint256 requiredSMajority;
        uint256 requiredSQuorum;
    }

    struct VotingCounters {
        uint256 weightFor;
        uint256 weightAgainst;
        uint256 vetosCount;
    }

    struct VotingStats {
        uint256 requiredQuorum;
        uint256 currentQuorum;
        uint256 requiredMajority;
        uint256 currentMajority;
        uint256 currentVetoPercentage;
    }

    struct BaseProposal {
        string remark;
        VotingParams params;
        VotingCounters counters;
        bool executed;
    }

    event ProposalCreated(uint256 _id, BaseProposal _proposal);

    /**
     * @notice Executes the proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external;

    /**
     * @notice Votes for the proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external;

    /**
     * @notice Votes against the proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external;

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId) external view returns (BaseProposal calldata);

    /**
     * @notice Returns the current proposal stats for the given id
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId) external view returns (VotingStats calldata);
}

/**
 * @title Vetoable interface
 * @notice You can use this interface to add veto functionality to your contract
 */
interface IVetoable {
    event VetoOccurred(uint256 indexed id, address indexed sender);

    /**
     * @notice Veto from the passed id
     * @param _proposalId Proposal id
     */
    function veto(uint256 _proposalId) external;

    /**
     * @notice Returns the number of users who voted for the veto
     * @param _proposalId Proposal id
     * @return number of vetoes
     */
    function getVetosNumber(uint256 _proposalId) external view returns (uint256);

    /**
     * @notice Returns the percentage of those who voted for the veto from all roots
     * @param _proposalId Proposal id
     * @return vetos percentage
     */
    function getVetosPercentage(uint256 _proposalId) external view returns (uint256);
}

/**
 * @title QthVoting interface
 * @notice You can use this interface to create an unequal weight voting contract
 */
interface IQthVoting is IVoting, IVetoable {
    struct VotingWeightInfo {
        bool hasAlreadyVoted;
        bool canVote;
        BaseVotingWeightInfo base;
    }

    event QuorumReached(uint256 id);

    /**
     * @notice Returns voting information by id of pending proposal
     * @param _proposalId Proposal id
     * @return VotingWeightInfo struct
     */
    function getVotingWeightInfo(uint256 _proposalId) external view returns (VotingWeightInfo calldata);
}

/**
 * @title ParametersVoting interface
 * @notice You can use this interface to create a parameter voting contract
 */
interface IParametersVoting is IVoting, IVetoable {
    enum ParameterType {
        NONE,
        ADDRESS,
        UINT,
        STRING,
        BYTES32,
        BOOL
    }

    /**
     * @notice Structure that holds parameter value
     */
    struct ParameterInfo {
        string paramKey;
        ParameterType paramType;
        address addrValue;
        bool boolValue;
        bytes32 bytes32Value;
        string strValue;
        uint256 uintValue;
    }
}

/**
 * @title ExpertVoting interface
 * @notice Alias for the expert voting contract
 */
interface IMembershipVoting is IQthVoting {
    enum ProposalType {
        NONE,
        ADD,
        REMOVE,
        CHANGE
    }
}

/**
 *  @title ConstitutionVoting interface
 *  @notice Alias for the constitution voting contract
 */
interface IConstitutionVoting is IQthVoting, IParametersVoting {

}

/**
 * @title SlashingVoting interface
 * @notice Alias for the slashing voting contracts
 */
interface ISlashingVoting is IVoting {
    struct SlashingProposal {
        BaseProposal base;
        address proposer;
        address candidate;
        uint256 amountToSlash;
    }

    /**
     * @notice Returns address of slashing victim for the given id
     * @param _proposalId Proposal id
     * @return address of slashing victim
     */
    function getSlashingVictim(uint256 _proposalId) external view returns (address);

    /**
     * @notice Returns address of slashing proposer for the given id
     * @param _proposalId Proposal id
     * @return address slashing proposer
     */
    function getSlashingProposer(uint256 _proposalId) external view returns (address);
}
