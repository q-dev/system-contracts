// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

/**
 * @title Parameters interface
 * @notice You can use this interface to create your own parameters contracts
 */
interface IParameters {
    /**
     * @notice Returns an array of address keys
     * @return array of address keys
     */
    function getAddrKeys() external view returns (string[] memory);

    /**
     * @notice Returns an array of uint256 keys
     * @return array of uint256 keys
     */
    function getUintKeys() external view returns (string[] memory);

    /**
     * @notice Returns an array of string keys
     * @return array of string keys
     */
    function getStringKeys() external view returns (string[] memory);

    /**
     * @notice Returns an array of byte32 keys
     * @return array of byte32 keys
     */
    function getBytes32Keys() external view returns (string[] memory);

    /**
     * @notice Returns an array of bool keys
     * @return array of bool keys
     */
    function getBoolKeys() external view returns (string[] memory);

    /**
     * @notice Returns the address from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return address value
     */
    function getAddr(string calldata _key) external view returns (address);

    /**
     * @notice Returns uint256 from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return decimal number
     */
    function getUint(string calldata _key) external view returns (uint256);

    /**
     * @notice Returns a string from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return string value
     */
    function getString(string calldata _key) external view returns (string memory);

    /**
     * @notice Returns byte32 from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return byte32 value
     */
    function getBytes32(string calldata _key) external view returns (bytes32);

    /**
     * @notice Returns bool from the list by the given key, if it is in the list
     * @param _key Parameter key
     * @return bool value
     */
    function getBool(string calldata _key) external view returns (bool);
}

/**
 * @title Parameters interface
 * @notice You can use this interface to create your own mutable parameters contracts
 */
interface IMutableParameters is IParameters {
    /**
     * @notice Removes uint256 parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeUint(string calldata _key) external;

    /**
     * @notice Removes address parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeAddr(string calldata _key) external;

    /**
     * @notice Removes string parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeString(string calldata _key) external;

    /**
     * @notice Removes byte32 parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeBytes32(string calldata _key) external;

    /**
     * @notice Removes bool parameter from parameters list
     * @param _key Parameter key to be removed
     */
    function removeBool(string calldata _key) external;

    /**
     * @notice Sets the address by key, if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setAddr(string memory _key, address _val) external;

    /**
     * @notice Sets uint256 by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setUint(string memory _key, uint256 _val) external;

    /**
     * @notice Sets the string by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setString(string memory _key, string memory _val) external;

    /**
     * @notice Sets byte32 by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setBytes32(string memory _key, bytes32 _val) external;

    /**
     * @notice Sets bool by key if the parameter exists, otherwise creates a new parameter
     * @param _key The parameter key to be set
     * @param _val New parameter value
     */
    function setBool(string memory _key, bool _val) external;
}
