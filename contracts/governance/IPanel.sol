// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

/**
 * @title Experts interface
 * @notice You can use this interface to create your own interface with panel properties
 */
interface IPanel {
    /**
     * @notice Checks if the user is in the list
     * @param _exp User to check
     * @return true if the check passed, false if not
     */
    function isMember(address _exp) external view returns (bool);

    /**
     * @notice Returns the current list of members
     * @return current list of members
     */
    function getMembers() external view returns (address[] calldata);

    /**
     * @notice Returns the maximum number of members in a list
     * @return maximum number of members in the list
     */
    function getLimit() external view returns (uint256);

    /**
     * @notice Returns the current size of members list
     * @return list size
     */
    function getSize() external view returns (uint256);
}

/**
 * @title Experts interface
 * @notice You can use this interface to create your own interface with mutable panel properties
 */
interface IMutablePanel is IPanel {
    /**
     * @notice Adds a new member to the list
     * @param _exp Member to add
     */
    function addMember(address _exp) external;

    /**
     * @notice Removes existing member
     * @param _exp Member to be deleted
     */
    function removeMember(address _exp) external;

    /**
     * @notice Modifies an existing root
     * @param _oldExp Member to be changed
     * @param _newExp New member address
     */
    function swapMember(address _oldExp, address _newExp) external;
}

/**
 * @title Experts interface
 * @notice Alias for the experts contract
 */
interface IExperts is IMutablePanel {

}

/**
 * @title Roots interface
 * @notice Alias for the roots contract
 */
interface IRoots is IMutablePanel {
    function addSlashingProposal(address _root, uint256 _slashingProposalId) external;

    function applySlashing(
        uint256 _proposalId,
        address _root,
        uint256 _amountToSlash
    ) external returns (bool);

    function getRootNodeStake(address _root) external view returns (uint256);

    function getSlashingProposalIds(address _root) external view returns (uint256[] memory);
}
