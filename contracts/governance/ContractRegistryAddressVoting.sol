// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "./ARootNodeApprovalVoting.sol";

contract ContractRegistryAddressVoting is ARootNodeApprovalVoting, OwnableUpgradeable {
    struct SetKeyProposal {
        bool executed;
        uint256 votingStartTime;
        uint256 votingExpiredTime;
        string key;
        address proxy;
    }

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
        __Ownable_init();
    }

    /*
     * @notice create proposal about contract upgrading
     * @param _key contract's key
     * @param _proxy  address of contract's proxy
     * @return id of created proposal
     */
    function createProposal(string calldata _key, address _proxy) external onlyOwner returns (uint256) {
        return createProposal(abi.encode(_key, _proxy));
    }

    /*
     * @notice view for proposals
     * @param _id id of proposal
     * @return proposal
     */
    function getProposal(uint256 _id) external view onlyInitedProposal(_id) returns (SetKeyProposal memory proposal) {
        AProposal storage _proposal = proposals[_id];
        proposal.executed = _proposal.executed;
        proposal.votingStartTime = _proposal.votingStartTime;
        proposal.votingExpiredTime = _proposal.votingExpiredTime;

        (proposal.key, proposal.proxy) = abi.decode(_proposal.callData, (string, address));
    }

    function onExecute(bytes memory _callData) internal override {
        (string memory _key, address _proxy) = abi.decode(_callData, (string, address));
        registry.setAddress(_key, _proxy);
    }
}
