// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "./ARootNodeApprovalVoting.sol";

contract ContractRegistryUpgradeVoting is ARootNodeApprovalVoting, OwnableUpgradeable {
    struct UpgradeProposal {
        bool executed;
        uint256 votingStartTime;
        uint256 votingExpiredTime;
        address proxy;
        address implementation;
    }

    function initialize(address _registry) external initializer {
        registry = IContractRegistry(_registry);
        __Ownable_init();
    }

    /*
     * @notice create proposal about contract upgrading
     * @param _proxy  address of contract's proxy
     * @param _implementation address of new implementation
     * @return id of created proposal
     */
    function createProposal(address _proxy, address _implementation) external onlyOwner returns (uint256) {
        return createProposal(abi.encode(_proxy, _implementation));
    }

    /*
     * @notice view for proposals
     * @param _id id of proposal
     * @return proposal
     */
    function getProposal(uint256 _id) external view onlyInitedProposal(_id) returns (UpgradeProposal memory proposal) {
        AProposal storage _proposal = proposals[_id];
        proposal.executed = _proposal.executed;
        proposal.votingStartTime = _proposal.votingStartTime;
        proposal.votingExpiredTime = _proposal.votingExpiredTime;

        (proposal.proxy, proposal.implementation) = abi.decode(_proposal.callData, (address, address));
    }

    function onExecute(bytes memory _callData) internal override {
        (address _proxy, address _impl) = abi.decode(_callData, (address, address));
        registry.upgradeContract(_proxy, _impl);
    }
}
