// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../AParameters.sol";
import "../../common/Globals.sol";

contract EPRS_Parameters is AParameters {
    constructor() {}

    function initialize(
        address _registry,
        string[] memory _uintKeys,
        uint256[] memory _uintVals,
        string[] memory _addrKeys,
        address[] memory _addrVals,
        string[] memory _strKeys,
        string[] memory _strVals,
        string[] memory _boolKeys,
        bool[] memory _boolVals
    ) public virtual override {
        AParameters.initialize(
            _registry,
            _uintKeys,
            _uintVals,
            _addrKeys,
            _addrVals,
            _strKeys,
            _strVals,
            _boolKeys,
            _boolVals
        );

        ownerKey = RKEY__EPRS_PARAMETERS_VOTING;
    }
}
