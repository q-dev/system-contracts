// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsMembership.sol";

contract EPRS_Membership is ExpertsMembership {
    constructor() {}

    function initialize(address _registry, address[] memory _experts) public virtual override {
        ExpertsMembership.initialize(_registry, _experts);

        expertLimitKey = "constitution.EPRS.maxNExperts";
        expertVotingKey = "governance.experts.EPRS.membershipVoting";
    }
}
