// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

import "../IVoting.sol";
import "../IParameters.sol";
import "../IPanel.sol";
import "./AExpertsMembership.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../common/Globals.sol";

/**
 * @title Experts Parameters Voting
 * @notice Abstract contract to vote for EP parameters
 */
abstract contract ExpertsParametersVoting is IParametersVoting, Initializable {
    struct ExpertsParametersProposal {
        BaseProposal base;
        mapping(uint256 => ParameterInfo) parameters;
        uint256 parametersSize;
    }

    mapping(uint256 => ExpertsParametersProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public hasUserVoted;
    mapping(uint256 => mapping(address => bool)) public hasRootVetoed;

    uint256 public proposalCount;

    string public expertsParametersKey;
    string public expertsMembershipKey;
    string public votingPeriodKey;
    string public vetoPeriodKey;
    string public majorityKey;
    string public quorumKey;
    IContractRegistry internal registry;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption);
    event ProposalExecuted(uint256 indexed _proposalId);

    /**
     * @notice Restricts only root node can interact
     */
    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-010001]-Permission denied - only root nodes have access."
        );
        _;
    }

    /**
     * @notice Restricts only for existing proposals
     * @param _proposalId Proposal id
     */
    modifier shouldExist(uint256 _proposalId) {
        require(getStatus(_proposalId) != ProposalStatus.NONE, "[QEC-010002]-The proposal does not exist.");
        _;
    }

    /**
     * @notice Restricts only experts can interact
     */
    modifier onlyExpert() {
        require(
            ExpertsMembership(registry.mustGetAddress(expertsMembershipKey)).isMember(msg.sender),
            "[QEC-010000]-Permission denied - only experts have access."
        );
        _;
    }

    constructor() {}

    /**
     * @notice Creates address proposal
     * @param _remark Some message with details (may be link)
     * @param _parameterKey the key of the parameter to change
     * @param _addrValue new parameter value
     * @return id of new proposal
     */
    function createAddrProposal(
        string memory _remark,
        string memory _parameterKey,
        address _addrValue
    ) external onlyExpert returns (uint256) {
        ParameterInfo memory _param;
        _param.paramKey = _parameterKey;
        _param.paramType = ParameterType.ADDRESS;
        _param.addrValue = _addrValue;

        ParameterInfo[] memory _parametersArr = new ParameterInfo[](1);
        _parametersArr[0] = _param;

        return createProposal(_remark, _parametersArr);
    }

    /**
     * @notice Creates uint proposal
     * @param _remark Some message with details (may be link)
     * @param _parameterKey the key of the parameter to change
     * @param _uintValue new parameter value
     * @return id of new proposal
     */
    function createUintProposal(
        string memory _remark,
        string memory _parameterKey,
        uint256 _uintValue
    ) external onlyExpert returns (uint256) {
        ParameterInfo memory _param;
        _param.paramKey = _parameterKey;
        _param.paramType = ParameterType.UINT;
        _param.uintValue = _uintValue;

        ParameterInfo[] memory _parametersArr = new ParameterInfo[](1);
        _parametersArr[0] = _param;

        return createProposal(_remark, _parametersArr);
    }

    // /**
    // * @notice Creates asset uint proposal
    // * @param _remark Some message with details (may be link)
    // * @param _parameterKey the key of the parameter to change
    // * @param _uintValue new parameter value
    // * @param _assetCode code of the asset
    // * @return id of new proposal
    // */
    // function createAssetUintProposal(
    //     string memory _remark,
    //     string memory _parameterKey,
    //     uint256 _uintValue,
    //     string memory _assetCode
    // )
    // external onlyExpert() returns (uint256)
    // {
    //     string[] memory _parameterKeyArr = new string[](1);
    //     _parameterKeyArr[0] = _parameterKey;
    //     uint256[] memory _assetUintArr = new uint256[](1);
    //     _assetUintArr[0] = _uintValue;
    //     string[] memory _assetCodeArr = new string[](1);
    //     _assetCodeArr[0] = _assetCode;

    //     return createProposal(_remark, _parameterKeyArr, _assetUintArr, _assetCodeArr,
    //         new address[](0), new uint256[](0), new string[](0), new bytes32[](0), new bool[](0));
    // }

    /**
     * @notice Creates string proposal
     * @param _remark Some message with details (may be link)
     * @param _parameterKey the key of the parameter to change
     * @param _strValue new parameter value
     * @return id of new proposal
     */
    function createStrProposal(
        string memory _remark,
        string memory _parameterKey,
        string memory _strValue
    ) external onlyExpert returns (uint256) {
        ParameterInfo memory _param;
        _param.paramKey = _parameterKey;
        _param.paramType = ParameterType.STRING;
        _param.strValue = _strValue;

        ParameterInfo[] memory _parametersArr = new ParameterInfo[](1);
        _parametersArr[0] = _param;

        return createProposal(_remark, _parametersArr);
    }

    /**
     * @notice Creates bytes proposal
     * @param _remark Some message with details (may be link)
     * @param _parameterKey the key of the parameter to change
     * @param _bytes32Value new parameter value
     * @return id of new proposal
     */
    function createBytesProposal(
        string memory _remark,
        string memory _parameterKey,
        bytes32 _bytes32Value
    ) external onlyExpert returns (uint256) {
        ParameterInfo memory _param;
        _param.paramKey = _parameterKey;
        _param.paramType = ParameterType.BYTES32;
        _param.bytes32Value = _bytes32Value;

        ParameterInfo[] memory _parametersArr = new ParameterInfo[](1);
        _parametersArr[0] = _param;

        return createProposal(_remark, _parametersArr);
    }

    /**
     * @notice Creates boolean proposal
     * @param _remark Some message with details (may be link)
     * @param _parameterKey the key of the parameter to change
     * @param _boolValue new parameter value
     * @return id of new proposal
     */
    function createBoolProposal(
        string memory _remark,
        string memory _parameterKey,
        bool _boolValue
    ) external onlyExpert returns (uint256) {
        ParameterInfo memory _param;
        _param.paramKey = _parameterKey;
        _param.paramType = ParameterType.BOOL;
        _param.boolValue = _boolValue;

        ParameterInfo[] memory _parametersArr = new ParameterInfo[](1);
        _parametersArr[0] = _param;

        return createProposal(_remark, _parametersArr);
    }

    /**
     * @notice Give a vote for the specified proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override onlyExpert {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Give a vote against the specified proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override onlyExpert {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Root gives a veto for the specified proposal
     * @param _proposalId Proposal id
     */
    function veto(uint256 _proposalId) external override onlyRoot shouldExist(_proposalId) {
        require(!hasRootVetoed[_proposalId][msg.sender], "[QEC-010014]-The caller has already vetoed the proposal.");
        require(
            getStatus(_proposalId) == ProposalStatus.ACCEPTED,
            "[QEC-010015]-Proposal must be ACCEPTED before casting a root node veto."
        );
        hasRootVetoed[_proposalId][msg.sender] = true;

        ++proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Applies changes for specified proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override {
        require(
            getStatus(_proposalId) == ProposalStatus.PASSED,
            "[QEC-010010]-Proposal must be PASSED before excecuting."
        );

        proposals[_proposalId].base.executed = true;

        require(
            _applyProposedChanges(_proposalId),
            "[QEC-010011]-Failed to apply changes to the parameters, the execution failed."
        );

        emit ProposalExecuted(_proposalId);
    }

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId].base;
    }

    /**
     * @notice Gets statistic for the proposal
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        ExpertsParametersProposal storage _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredMajority = _prop.base.params.requiredMajority;
        _stats.requiredQuorum = _prop.base.params.requiredQuorum;

        uint256 _votesCount = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _expertsCount = ExpertsMembership(registry.mustGetAddress(expertsMembershipKey)).getSize();
        _stats.currentQuorum = _calculatePercentage(_votesCount, _expertsCount);

        _stats.currentMajority = _calculatePercentage(_prop.base.counters.weightFor, _votesCount);
        _stats.currentVetoPercentage = _getVetosPercentageNumber(_proposalId);

        return _stats;
    }

    /**
     * @notice Gets an array of parameters from a specific proposal
     * @param _proposalId Proposal id
     * @return array of parameter structures
     */
    function getParametersArr(uint256 _proposalId)
        external
        view
        shouldExist(_proposalId)
        returns (ParameterInfo[] memory)
    {
        ExpertsParametersProposal storage prop = proposals[_proposalId];

        uint256 _arraySize = prop.parametersSize;
        ParameterInfo[] memory _parametersArr = new ParameterInfo[](_arraySize);

        for (uint256 i = 0; i < _arraySize; i++) {
            _parametersArr[i] = prop.parameters[i];
        }

        return _parametersArr;
    }

    /**
     * @notice Gets count for vetos number
     * @param _proposalId Proposal id
     * @return number of vetos
     */
    function getVetosNumber(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Gets percent for vetos number
     * @param _proposalId Proposal id
     * @return percent of vetos
     */
    function getVetosPercentage(uint256 _proposalId) external view override shouldExist(_proposalId) returns (uint256) {
        return _getVetosPercentageNumber(_proposalId);
    }

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Creates a new offer that can contain multiple parameters. The new proposal is atomic.
     *
     * @param _remark Some message with details (may be link)
     * @param _parametersArr Some comments
     * @return id of new proposal
     */
    function createProposal(string memory _remark, ParameterInfo[] memory _parametersArr)
        public
        onlyExpert
        returns (uint256)
    {
        uint256 _parametersArrLength = _parametersArr.length;
        require(_parametersArrLength > 0, "[QEC-010016]-Proposal must contain at least one expert parameter.");

        ExpertsParametersProposal storage prop = proposals[proposalCount];
        prop.parametersSize = _parametersArrLength;

        for (uint256 i = 0; i < _parametersArrLength; i++) {
            prop.parameters[i] = _parametersArr[i];
        }

        IParameters constitution = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS));
        uint256 _endDate = block.timestamp + constitution.getUint(votingPeriodKey);
        uint256 _vetoEndDate = _endDate + constitution.getUint(vetoPeriodKey);

        prop.base.remark = _remark;
        prop.base.params.requiredMajority = constitution.getUint(majorityKey);
        prop.base.params.requiredQuorum = constitution.getUint(quorumKey);
        prop.base.params.votingEndTime = _endDate;
        prop.base.params.vetoEndTime = _vetoEndDate;
        prop.base.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        uint256 _id = proposalCount;
        emit ProposalCreated(_id, proposals[_id].base);

        proposalCount++;

        return _id;
    }

    /**
     * @notice Returns the status of the given proposal
     * @param _proposalId Proposal id
     * @return status of proposal
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        ExpertsParametersProposal storage prop = proposals[_proposalId];

        if (prop.base.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (prop.base.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < prop.base.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        uint256 _votesCount = prop.base.counters.weightFor + prop.base.counters.weightAgainst;
        uint256 _expertsCount = ExpertsMembership(registry.mustGetAddress(expertsMembershipKey)).getSize();
        uint256 _actualQuorum = _calculatePercentage(_votesCount, _expertsCount);

        if (_actualQuorum < prop.base.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        // votes percantage for compare with required maority
        uint256 _actualMajority = _calculatePercentage(prop.base.counters.weightFor, _votesCount);
        if (_actualMajority <= prop.base.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (_getVetosPercentageNumber(_proposalId) > getDecimal() / 2) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp < prop.base.params.vetoEndTime) {
            return ProposalStatus.ACCEPTED;
        }

        if (block.timestamp > prop.base.params.vetoEndTime + prop.base.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _vote(uint256 _proposalId, VotingOption _votingOption) private {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-010012]-Voting is only possible on PENDING proposals."
        );

        require(!hasUserVoted[_proposalId][msg.sender], "[QEC-010013]-The caller has already voted for the proposal.");

        hasUserVoted[_proposalId][msg.sender] = true;

        if (_votingOption == VotingOption.FOR) {
            proposals[_proposalId].base.counters.weightFor++;
        } else {
            proposals[_proposalId].base.counters.weightAgainst++;
        }

        emit UserVoted(_proposalId, _votingOption);
    }

    function _applyProposedChanges(uint256 _proposalId) private returns (bool) {
        IMutableParameters expertParameters = IMutableParameters(registry.mustGetAddress(expertsParametersKey));

        ExpertsParametersProposal storage prop = proposals[_proposalId];
        ParameterInfo memory _param;
        ParameterType _type;
        string memory _key;

        for (uint256 i = 0; i < prop.parametersSize; i++) {
            _param = prop.parameters[i];
            _type = _param.paramType;
            _key = _param.paramKey;

            if (_type == ParameterType.ADDRESS) {
                expertParameters.setAddr(_key, _param.addrValue);
            } else if (_type == ParameterType.UINT) {
                expertParameters.setUint(_key, _param.uintValue);
            } else if (_type == ParameterType.STRING) {
                expertParameters.setString(_key, _param.strValue);
            } else if (_type == ParameterType.BYTES32) {
                expertParameters.setBytes32(_key, _param.bytes32Value);
            } else if (_type == ParameterType.BOOL) {
                expertParameters.setBool(_key, _param.boolValue);
            }
        }

        return true;
    }

    function _getVetosPercentageNumber(uint256 _proposalId) private view returns (uint256) {
        return
            (proposals[_proposalId].base.counters.vetosCount * getDecimal()) / IRoots(_getRootNodesAddress()).getSize();
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _calculatePercentage(uint256 part, uint256 amount) private pure returns (uint256) {
        if (amount == 0) {
            return 0;
        }
        return (part * getDecimal()) / amount;
    }
}
