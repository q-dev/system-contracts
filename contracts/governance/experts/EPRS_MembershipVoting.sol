// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsMembershipVoting.sol";

/**
 * @title EPRS Membership Voting
 * @notice Used to vote for membership in DR expert panel
 */
contract EPRS_MembershipVoting is ExpertsMembershipVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsMembershipVoting.initialize(_registry);

        expertKey = RKEY__EPRS_MEMBERSHIP;
        votingQuorumKey = "constitution.voting.EPRS.addOrRemExpertQRM";
        votingPeriodKey = "constitution.voting.EPRS.addOrRemExpertVP";
        majorityKey = "constitution.voting.EPRS.addOrRemExpertRMAJ";
        vetoPeriodKey = "constitution.voting.EPRS.addOrRemExpertRNVALP";
    }
}
