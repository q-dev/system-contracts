// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsParametersVoting.sol";

/**
 * @title EPQFI Parameters Voting
 * @notice Used to vote for parameters governed by QFI expert panel
 */
contract EPQFI_ParametersVoting is ExpertsParametersVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsParametersVoting.initialize(_registry);

        expertsParametersKey = RKEY__EPQFI_PARAMETERS;
        expertsMembershipKey = RKEY__EPQFI_MEMBERSHIP;
        votingPeriodKey = "constitution.voting.EPQFI.changeParamVP";
        vetoPeriodKey = "constitution.voting.EPQFI.changeParamRNVALP";
        majorityKey = "constitution.voting.EPQFI.changeParamRMAJ";
        quorumKey = "constitution.voting.EPQFI.changeParamQRM";
    }
}
