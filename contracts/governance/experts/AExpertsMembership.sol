// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "../IPanel.sol";
import "../IParameters.sol";
import "../../common/AddressStorageFactory.sol";
import "../../interfaces/IContractRegistry.sol";
import "../../common/Globals.sol";

abstract contract ExpertsMembership is IExperts, OwnableUpgradeable {
    AddressStorage public experts;
    IContractRegistry private registry;

    string public expertLimitKey;
    string public expertVotingKey;

    /**
     * @notice Restricts only the ExpertsVoting contract can interact
     */
    modifier onlyExpertsVoting() {
        require(
            msg.sender == registry.mustGetAddress(expertVotingKey),
            "[QEC-007002]-Permission denied - only experts from this panel have access."
        );
        _;
    }

    constructor() {}

    /**
     * @notice Adds an expert to the expert list
     * @param _exp Expert to add to the list
     */
    function addMember(address _exp) external override onlyExpertsVoting {
        AddressStorage _experts = experts;
        require(_experts.size() < getLimit(), "[QEC-007000]-Maximum number of experts is reached.");
        _experts.add(_exp);
    }

    /**
     * @notice Removes an expert from the expert list
     * @param _exp Expert to remove from the list
     */
    function removeMember(address _exp) external override onlyExpertsVoting {
        experts.remove(_exp);
    }

    /**
     * @notice Replace one expert with another
     * @param _oldExp Expert to remove from the list
     * @param _newExp Expert to add to the list
     */
    function swapMember(address _oldExp, address _newExp) external override onlyExpertsVoting {
        AddressStorage _experts = experts;
        _experts.remove(_oldExp);
        _experts.add(_newExp);
    }

    /**
     * @notice Returns expert membership checking result
     * @param _exp Address for expert membersih checking
     * @return true if given address is an expert
     */
    function isMember(address _exp) external view override returns (bool) {
        return experts.contains(_exp);
    }

    /**
     * @notice Returns expert list
     * @return List of expert addresses
     */
    function getMembers() external view override returns (address[] memory) {
        return experts.getAddresses();
    }

    /**
     * @notice Returns expert list size
     * @return Size of the expert list
     */
    function getSize() external view override returns (uint256) {
        return experts.size();
    }

    function initialize(address _registry, address[] memory _addrList) public virtual initializer {
        registry = IContractRegistry(_registry);
        experts = AddressStorageFactory(registry.mustGetAddress(RKEY__ADDRESS_STORAGE_FACTORY)).create(_addrList);
        __Ownable_init();
    }

    /**
     * @notice Returns expert list limit size
     * @return Limit size of the expert list
     */
    function getLimit() public view override returns (uint256) {
        uint256 limit = IParameters(registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS)).getUint(expertLimitKey);
        require(limit > 0, "[QEC-007001]-Expert limit parameter is invalid or does not exist.");
        return limit;
    }
}
