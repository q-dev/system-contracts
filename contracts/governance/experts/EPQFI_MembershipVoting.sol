// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsMembershipVoting.sol";

/**
 * @title EPQFI Membership Voting
 * @notice Used to vote for membership in QFI expert panel
 */
contract EPQFI_MembershipVoting is ExpertsMembershipVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsMembershipVoting.initialize(_registry);

        expertKey = RKEY__EPQFI_MEMBERSHIP;
        votingQuorumKey = "constitution.voting.EPQFI.addOrRemExpertQRM";
        votingPeriodKey = "constitution.voting.EPQFI.addOrRemExpertVP";
        majorityKey = "constitution.voting.EPQFI.addOrRemExpertRMAJ";
        vetoPeriodKey = "constitution.voting.EPQFI.addOrRemExpertRNVALP";
    }
}
