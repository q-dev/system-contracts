// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../IPanel.sol";
import "../IVoting.sol";
import "../IParameters.sol";
import "../../tokeneconomics/QVault.sol";
import "../../common/Globals.sol";
import "../../interfaces/IVotingWeightProxy.sol";

abstract contract ExpertsMembershipVoting is IMembershipVoting, Initializable {
    struct ExpertsMembershipProposal {
        BaseProposal base;
        MembershipProposalDetails proposalDetails;
    }

    mapping(uint256 => ExpertsMembershipProposal) public proposals;
    mapping(uint256 => mapping(address => bool)) public hasUserVoted;
    mapping(uint256 => mapping(address => bool)) public hasRootVetoed;

    uint256 public proposalCount;

    IContractRegistry internal registry;

    string public expertKey;
    string public votingQuorumKey;
    string public votingPeriodKey;
    string public majorityKey;
    string public vetoPeriodKey;

    event UserVoted(uint256 indexed _proposalId, VotingOption _votingOption, uint256 _amount);
    event ProposalExecuted(uint256 indexed _proposalId);

    /**
     * @notice Restricts only root node can interact
     */
    modifier onlyRoot() {
        require(
            IRoots(_getRootNodesAddress()).isMember(msg.sender),
            "[QEC-008010]-Permission denied - only root nodes have access."
        );
        _;
    }

    /**
     * @notice Restricts only for existing proposals
     * @param _id Proposal id
     */
    modifier shouldExist(uint256 _id) {
        require(getStatus(_id) != ProposalStatus.NONE, "[QEC-008011]-The proposal does not exist.");
        _;
    }

    constructor() {}

    /**
     * @notice Create proposal for expert changing
     * @param _remark is an optional string to store in proposal
     * @param _newExpert Expert to add
     * @param _expertToBeChanged Expert to remove
     * @return id of new proposal
     */
    function createChangeExpertProposal(
        string memory _remark,
        address _newExpert,
        address _expertToBeChanged
    ) external returns (uint256) {
        return _createProposal(_remark, _newExpert, _expertToBeChanged);
    }

    /**
     * @notice Create proposal for expert adding
     * @param _remark is an optional string to store in proposal
     * @param _newExpert Expert to add
     * @return id of new proposal
     */
    function createAddExpertProposal(string memory _remark, address _newExpert) external returns (uint256) {
        return _createProposal(_remark, _newExpert, address(0));
    }

    /**
     * @notice Create proposal for expert adding
     * @param _remark is an optional string to store in proposal
     * @param _toRemoveExpert Expert to remove
     * @return id of new proposal
     */
    function createRemoveExpertProposal(string memory _remark, address _toRemoveExpert) external returns (uint256) {
        return _createProposal(_remark, address(0), _toRemoveExpert);
    }

    /**
     * @notice Give a vote for the specified proposal
     * @param _proposalId Proposal id
     */
    function voteFor(uint256 _proposalId) external override {
        _vote(_proposalId, VotingOption.FOR);
    }

    /**
     * @notice Give a vote against the specified proposal
     * @param _proposalId Proposal id
     */
    function voteAgainst(uint256 _proposalId) external override {
        _vote(_proposalId, VotingOption.AGAINST);
    }

    /**
     * @notice Applies changes for specified proposal
     * @param _proposalId Proposal id
     */
    function execute(uint256 _proposalId) external override {
        require(
            getStatus(_proposalId) == ProposalStatus.PASSED,
            "[QEC-008006]-Proposal must be PASSED before excecuting."
        );
        proposals[_proposalId].base.executed = true;

        IExperts experts = IExperts(registry.mustGetAddress(expertKey));
        ExpertsMembershipProposal memory _prop = proposals[_proposalId];
        ProposalType _proposalType = _getProposalType(
            _prop.proposalDetails.addressToAdd,
            _prop.proposalDetails.addressToRemove
        );

        if (_proposalType == ProposalType.REMOVE) {
            experts.removeMember(_prop.proposalDetails.addressToRemove);
        }

        if (_proposalType == ProposalType.CHANGE) {
            experts.swapMember(
                _prop.proposalDetails.addressToRemove,
                proposals[_proposalId].proposalDetails.addressToAdd
            );
        }

        if (_proposalType == ProposalType.ADD) {
            experts.addMember(_prop.proposalDetails.addressToAdd);
        }

        emit ProposalExecuted(_proposalId);
    }

    /**
     * @notice Root gives a veto for the specified proposal
     * @param _proposalId Proposal id
     */
    function veto(uint256 _proposalId) external override onlyRoot {
        require(
            getStatus(_proposalId) == ProposalStatus.ACCEPTED,
            "[QEC-008008]-Proposal must be ACCEPTED before casting a root node veto."
        );
        require(!hasRootVetoed[_proposalId][msg.sender], "[QEC-008009]-The caller has already vetoed the proposal.");
        hasRootVetoed[_proposalId][msg.sender] = true;

        ++proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @notice Gets count for vetos number
     * @param _proposalId Proposal id
     * @return number of vetos
     */
    function getVetosNumber(uint256 _proposalId) external view override returns (uint256) {
        return proposals[_proposalId].base.counters.vetosCount;
    }

    /**
     * @dev count voting results "YES"
     * @param _proposalId - id of the proposal
     * @return vote count for specific proposal
     */
    function getVotesFor(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.counters.weightFor;
    }

    /**
     * @dev count voting results "NO"
     * @param _proposalId - id of the proposal
     * @return vote count against specific proposal
     */
    function getVotesAgainst(uint256 _proposalId) external view returns (uint256) {
        return proposals[_proposalId].base.counters.weightAgainst;
    }

    /**
     * @notice Returns the base structure for the given id
     * @param _proposalId Proposal id
     * @return structure of type BaseProposal
     */
    function getProposal(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (BaseProposal memory)
    {
        return proposals[_proposalId].base;
    }

    /**
     * @notice Gets statistic for the proposal
     * @param _proposalId Proposal id
     * @return structure of type VotingStats
     */
    function getProposalStats(uint256 _proposalId)
        external
        view
        override
        shouldExist(_proposalId)
        returns (VotingStats memory)
    {
        ExpertsMembershipProposal memory _prop = proposals[_proposalId];
        VotingStats memory _stats;

        _stats.requiredQuorum = _prop.base.params.requiredQuorum;
        _stats.requiredMajority = _prop.base.params.requiredMajority;

        uint256 _totalWeight = _prop.base.counters.weightFor + _prop.base.counters.weightAgainst;
        uint256 _Qamount = getTotalQInExistence(block.number);

        _stats.currentQuorum = _calculatePercentage(_totalWeight, _Qamount);
        _stats.currentMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);
        _stats.currentVetoPercentage = this.getVetosPercentage(_proposalId);

        return _stats;
    }

    function initialize(address _registry) public virtual initializer {
        registry = IContractRegistry(_registry);
    }

    /**
     * @notice Returns information about the user's voting rights by the proposal id
     * @param _proposalId Proposal id
     * @return VotingWeightInfo struct
     */
    function getVotingWeightInfo(uint256 _proposalId)
        public
        view
        override
        shouldExist(_proposalId)
        returns (VotingWeightInfo memory)
    {
        IVotingWeightProxy _proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        VotingWeightInfo memory _info;
        _info.base = _proxy.getBaseVotingWeightInfo(msg.sender, proposals[_proposalId].base.params.votingEndTime);
        _info.hasAlreadyVoted = hasUserVoted[_proposalId][msg.sender];
        _info.canVote = _info.base.ownWeight > 0 && !_info.hasAlreadyVoted;

        return _info;
    }

    /**
     * @notice Returns the percentage of those who voted for the veto from all roots
     * @param _proposalId Proposal id
     * @return vetos percentage
     */
    function getVetosPercentage(uint256 _proposalId) public view override returns (uint256) {
        IRoots roots = IRoots(_getRootNodesAddress());

        if (proposals[_proposalId].base.counters.vetosCount > 0) {
            return (proposals[_proposalId].base.counters.vetosCount * getDecimal()) / roots.getSize();
        }

        return 0;
    }

    /**
     * @notice Returns the status of the given proposal
     * @param _proposalId Proposal id
     * @return status of proposal
     */
    function getStatus(uint256 _proposalId) public view returns (ProposalStatus) {
        ExpertsMembershipProposal memory _prop = proposals[_proposalId];
        if (_prop.base.params.votingEndTime == 0) {
            return ProposalStatus.NONE;
        }

        if (_prop.base.executed) {
            return ProposalStatus.EXECUTED;
        }

        if (block.timestamp < _prop.base.params.votingEndTime) {
            return ProposalStatus.PENDING;
        }

        if (getVetosPercentage(_proposalId) > getDecimal() / 2) {
            return ProposalStatus.REJECTED;
        }

        uint256 _totalWeight = _prop.base.counters.weightFor + (_prop.base.counters.weightAgainst);
        uint256 _Qamount = getTotalQInExistence(block.number);
        uint256 _actualQuorum = _calculatePercentage(_totalWeight, _Qamount);
        if (_actualQuorum < _prop.base.params.requiredQuorum) {
            return ProposalStatus.REJECTED;
        }

        // votes percantage for compare with required majority
        uint256 _actualMajority = _calculatePercentage(_prop.base.counters.weightFor, _totalWeight);
        if (_actualMajority <= _prop.base.params.requiredMajority) {
            return ProposalStatus.REJECTED;
        }

        if (block.timestamp < proposals[_proposalId].base.params.vetoEndTime) {
            return ProposalStatus.ACCEPTED;
        }

        if (block.timestamp > _prop.base.params.vetoEndTime + _prop.base.params.proposalExecutionP) {
            return ProposalStatus.EXPIRED;
        }

        return ProposalStatus.PASSED;
    }

    function _createProposal(
        string memory _remark,
        address _newExpert,
        address _expertToBeChanged
    ) private returns (uint256) {
        ProposalType _proposalType = _getProposalType(_newExpert, _expertToBeChanged);

        IExperts experts = IExperts(registry.mustGetAddress(expertKey));
        if (ProposalType.REMOVE == _proposalType) {
            // check that toDelete is an expert
            require(
                experts.isMember(_expertToBeChanged),
                "[QEC-008000]-The expert to be removed does not exist, proposal creation failed."
            );

            if (_expertToBeChanged == msg.sender) {
                experts.removeMember(msg.sender);

                return 0;
            }
        } else {
            // check that candidate isn't expert
            require(
                !experts.isMember(_newExpert),
                "[QEC-008001]-The proposed new expert is already member of this panel."
            );

            uint256 _expertsLimit = experts.getLimit();
            if (ProposalType.CHANGE == _proposalType) {
                require(experts.isMember(_expertToBeChanged), "[QEC-008002]-The expert to be replaced does not exist.");

                require(
                    experts.getSize() <= _expertsLimit,
                    "[QEC-008013]-Maximum number of experts in this panel is exceeded."
                );
            } else if (ProposalType.ADD == _proposalType) {
                require(
                    experts.getSize() < _expertsLimit,
                    "[QEC-008014]-Maximum number of experts in this panel is reached."
                );
            }
        }

        uint256 _endDate = block.timestamp + IParameters(_getConstitutionParametersAddress()).getUint(votingPeriodKey);
        uint256 _id = _proposalCreation(_remark, _endDate, _newExpert, _expertToBeChanged);

        emit ProposalCreated(_id, proposals[_id].base);

        return _id;
    }

    function _proposalCreation(
        string memory _remark,
        uint256 _endDate,
        address _newExpert,
        address _toChangeExpert
    ) private returns (uint256) {
        uint256 _id = proposalCount;
        IParameters constitution = IParameters(_getConstitutionParametersAddress());

        ExpertsMembershipProposal memory _prop;
        _prop.proposalDetails.addressToAdd = _newExpert;
        _prop.proposalDetails.addressToRemove = _toChangeExpert;
        _prop.base.remark = _remark;
        _prop.base.params.votingEndTime = _endDate;
        _prop.base.params.requiredQuorum = constitution.getUint(votingQuorumKey);
        _prop.base.params.requiredMajority = constitution.getUint(majorityKey);
        _prop.base.params.vetoEndTime = _endDate + constitution.getUint(vetoPeriodKey);
        _prop.base.params.proposalExecutionP = constitution.getUint("constitution.proposalExecutionP");

        proposalCount++;
        proposals[_id] = _prop;

        return _id;
    }

    function _vote(uint256 _proposalId, VotingOption _votingOption) private returns (bool) {
        require(
            getStatus(_proposalId) == ProposalStatus.PENDING,
            "[QEC-008003]-Voting is only possible on PENDING proposals."
        );

        require(!hasUserVoted[_proposalId][msg.sender], "[QEC-008004]-The caller has already voted for the proposal.");
        hasUserVoted[_proposalId][msg.sender] = true;

        IVotingWeightProxy proxy = IVotingWeightProxy(_getVotingWeightProxyAddress());
        uint256 _totalLockedQ = proxy.extendLocking(msg.sender, proposals[_proposalId].base.params.votingEndTime);
        require(_totalLockedQ > 0, "[QEC-008012]-The total voting weight must be greater than zero.");

        if (_votingOption == VotingOption.FOR) {
            proposals[_proposalId].base.counters.weightFor += _totalLockedQ;
        } else {
            proposals[_proposalId].base.counters.weightAgainst += _totalLockedQ;
        }

        emit UserVoted(_proposalId, _votingOption, _totalLockedQ);
        return true;
    }

    function _getRootNodesAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__ROOT_NODES);
    }

    function _getConstitutionParametersAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__CONSTITUTION_PARAMETERS);
    }

    function _getVotingWeightProxyAddress() private view returns (address) {
        return registry.mustGetAddress(RKEY__VOTING_WEIGHT_PROXY);
    }

    function _calculatePercentage(uint256 part, uint256 amount) private pure returns (uint256) {
        if (amount == 0) {
            return 0;
        }
        return (part * getDecimal()) / amount;
    }

    function _getProposalType(address _newExpert, address _expertToBeChanged) private pure returns (ProposalType) {
        ProposalType _proposalType = ProposalType.NONE;
        if (_newExpert != address(0) && _expertToBeChanged != address(0)) {
            _proposalType = ProposalType.CHANGE;
        } else if (_newExpert == address(0) && _expertToBeChanged != address(0)) {
            _proposalType = ProposalType.REMOVE;
        } else if (_newExpert != address(0) && _expertToBeChanged == address(0)) {
            _proposalType = ProposalType.ADD;
        } else {
            revert("[QEC-008007]-Invalid proposal parameters, failed to derive the proposal type.");
        }
        return _proposalType;
    }
}
