// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsParametersVoting.sol";

/**
 * @title EPRS Parameters Voting
 * @notice Used to vote for parameters governed by DR expert panel
 */
contract EPRS_ParametersVoting is ExpertsParametersVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsParametersVoting.initialize(_registry);

        expertsParametersKey = RKEY__EPRS_PARAMETERS;
        expertsMembershipKey = RKEY__EPRS_MEMBERSHIP;
        votingPeriodKey = "constitution.voting.EPRS.changeParamVP";
        vetoPeriodKey = "constitution.voting.EPRS.changeParamRNVALP";
        majorityKey = "constitution.voting.EPRS.changeParamRMAJ";
        quorumKey = "constitution.voting.EPRS.changeParamQRM";
    }
}
