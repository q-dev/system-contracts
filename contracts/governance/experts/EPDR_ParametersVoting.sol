// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsParametersVoting.sol";

/**
 * @title EPDR Parameters Voting
 * @notice Used to vote for parameters governed by DR expert panel
 */
contract EPDR_ParametersVoting is ExpertsParametersVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsParametersVoting.initialize(_registry);

        expertsParametersKey = RKEY__EPDR_PARAMETERS;
        expertsMembershipKey = RKEY__EPDR_MEMBERSHIP;
        votingPeriodKey = "constitution.voting.EPDR.changeParamVP";
        vetoPeriodKey = "constitution.voting.EPDR.changeParamRNVALP";
        majorityKey = "constitution.voting.EPDR.changeParamRMAJ";
        quorumKey = "constitution.voting.EPDR.changeParamQRM";
    }
}
