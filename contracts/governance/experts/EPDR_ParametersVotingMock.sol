// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./EPDR_ParametersVoting.sol";

contract EPDR_ParametersVotingMock is EPDR_ParametersVoting {
    constructor() {}

    function getParam(uint256 _proposalId, uint256 _indexOfParam) public view returns (ParameterInfo memory) {
        return proposals[_proposalId].parameters[_indexOfParam];
    }

    function createAddrParam(string memory _key, address _addrValue) public pure returns (ParameterInfo memory _param) {
        _param.paramKey = _key;
        _param.paramType = ParameterType.ADDRESS;
        _param.addrValue = _addrValue;
    }

    function createUintParam(string memory _key, uint256 _uintValue) public pure returns (ParameterInfo memory _param) {
        _param.paramKey = _key;
        _param.paramType = ParameterType.UINT;
        _param.uintValue = _uintValue;
    }

    function createStrParam(string memory _key, string memory _strValue)
        public
        pure
        returns (ParameterInfo memory _param)
    {
        _param.paramKey = _key;
        _param.paramType = ParameterType.STRING;
        _param.strValue = _strValue;
    }

    function createBytes32Param(string memory _key, bytes32 _bytes32Value)
        public
        pure
        returns (ParameterInfo memory _param)
    {
        _param.paramKey = _key;
        _param.paramType = ParameterType.BYTES32;
        _param.bytes32Value = _bytes32Value;
    }

    function createBoolParam(string memory _key, bool _boolValue) public pure returns (ParameterInfo memory _param) {
        _param.paramKey = _key;
        _param.paramType = ParameterType.BOOL;
        _param.boolValue = _boolValue;
    }
}
