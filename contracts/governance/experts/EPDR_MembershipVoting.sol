// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "./AExpertsMembershipVoting.sol";

/**
 * @title EPDR Membership Voting
 * @notice Used to vote for membership in DR expert panel
 */
contract EPDR_MembershipVoting is ExpertsMembershipVoting {
    constructor() {}

    /**
     * @notice Initializes the contract in the beginning
     * @param _registry address of registry contract
     */
    function initialize(address _registry) public virtual override {
        ExpertsMembershipVoting.initialize(_registry);

        expertKey = RKEY__EPDR_MEMBERSHIP;
        votingQuorumKey = "constitution.voting.EPDR.addOrRemExpertQRM";
        votingPeriodKey = "constitution.voting.EPDR.addOrRemExpertVP";
        majorityKey = "constitution.voting.EPDR.addOrRemExpertRMAJ";
        vetoPeriodKey = "constitution.voting.EPDR.addOrRemExpertRNVALP";
    }
}
