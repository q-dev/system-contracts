// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../AParameters.sol";
import "../../common/Globals.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract EPDR_Parameters is AParameters {
    constructor() {}

    function initialize(
        address _registry,
        string[] memory _uintKeys,
        uint256[] memory _uintVals,
        string[] memory _addrKeys,
        address[] memory _addrVals,
        string[] memory _strKeys,
        string[] memory _strVals,
        string[] memory _boolKeys,
        bool[] memory _boolVals
    ) public virtual override {
        AParameters.initialize(
            _registry,
            _uintKeys,
            _uintVals,
            _addrKeys,
            _addrVals,
            _strKeys,
            _strVals,
            _boolKeys,
            _boolVals
        );

        ownerKey = RKEY__EPDR_PARAMETERS_VOTING;
    }

    function setAddr(string memory _key, address _val) public override onlyContractOwner {
        require(isMutableAddress(_key), "Immutable address");
        _setAddr(_key, _val);
    }

    /// @notice checks if param is muttable
    /// @dev if address is ERC20 returns ("governed.EPDR.{ERC20.symbol}_address" == _key)
    ///
    function isMutableAddress(string memory _key) public view returns (bool) {
        address _a = addrMap[_key].val;
        if (_a == address(0) || _a.code.length == 0) return true;
        address _borrowing = registry.mustGetAddress("defi.QUSD.borrowing");
        (bool _s, ) = _a.staticcall(abi.encodePacked(IERC20Metadata.symbol.selector));
        if (!_s) return true;

        uint256 _balance = IERC20(_a).balanceOf(_borrowing);
        uint256 keylen = bytes(_key).length; // ascii len;
        if (keylen < 14) return true; // check before slising
        if (
            keccak256(abi.encodePacked("governed.EPDR.")) != keccak256(getSlice(0, 14, _key)) ||
            keccak256(abi.encodePacked("_address")) != keccak256(getSlice(keylen - 8, keylen, _key))
        ) return true;
        return _balance == 0;
    }

    function getSlice(
        uint256 begin,
        uint256 end,
        string memory text
    ) internal pure returns (bytes memory a) {
        a = new bytes(end - begin);
        for (uint256 i = begin; i < end; i++) {
            a[i - begin] = bytes(text)[i];
        }
    }
}
