// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";

interface IStableCoin is IERC20Metadata {
    /**
     * @notice Creates the transferred amount of tokens to the user
     * @dev For system contracts only
     * @param recipient_ Token recipient address
     * @param amount_ Number of tokens
     * @return true if everything went well
     */
    function mint(address recipient_, uint256 amount_) external returns (bool);

    /**
     * @notice Transfer tokens to the recipient
     * @param to_ Address of the person to whom the funds will be transferred
     * @param value_ The amount of funds to be transferred
     * @param data_ Date of the transfer
     * @return true if everything went well
     */
    function transferAndCall(
        address to_,
        uint256 value_,
        bytes memory data_
    ) external returns (bool);

    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
    function burn(uint256 amount_) external;

    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account_, uint256 amount_) external;
}
