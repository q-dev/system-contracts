// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

import "../tokeneconomics/ITokenLock.sol";
import "../governance/IVoting.sol";

/**
 * @title Voting weight proxy contract
 * @notice VotingWeightProxy is used to store and record information
 * about locked tokens of users for all sources of token locks
 */
interface IVotingWeightProxy {
    struct VotingDelegationInfo {
        uint256 receivedWeight;
        address votingAgent;
        bool isPending;
        uint256 votingAgentPassOverTime;
    }

    /**
     * @notice lock is used to increase the currently locked tokens for a specific user
     *
     * @dev When trying to lock zero tokens, we get error 028002.
     * msg.sender is the source address of the token lock
     *
     * @param _who The address of the user who will have an increased number of locked tokens
     *
     * @param _amount The amount of locked tokens
     */
    function lock(address _who, uint256 _amount) external;

    /**
     * @notice announceUnlock is used to announce the unlock of locked coins.
     * When an unlock is announced, the information about the current expected amount
     * of the unlock and the unlock timeout is changed
     *
     * @dev When trying to announce an unlock for an amount more than
     * the user has in locked tokens, we will receive an error 028003.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which a certain amount of tokens will be announced to unlock
     *
     * @param _amount The amount of tokens for which unlocking will be announced
     *
     * @param _newUnlockTime new time for unlocking
     */
    function announceUnlock(
        address _who,
        uint256 _amount,
        uint256 _newUnlockTime
    ) external;

    /**
     * @notice unlock is used to unlock a certain amount of locked tokens
     * that were previously announced to be unlocked (or the amount, the lockedUntil of which has passed)
     *
     * @dev When trying to unlock zero tokens, we get error 028002.
     * If you try to unlock before the unlock time comes, we get error 028004.
     * When trying to unlock more than the user announced to unlock and lockedUntil time hasn't passed,
     * we will receive error 028005.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which a certain number of tokens will be unlocked
     *
     * @param _amount Amount of tokens to be unlocked
     */
    function unlock(address _who, uint256 _amount) external;

    /**
     * @notice forceUnlock is used to force unlock locked tokens.
     * Unlocking occurs bypassing the required time before unlocking.
     * If there are not enough tokens announced for unlocking, then locked tokens will be taken.
     *
     * @dev When trying to lock zero tokens, we get error 028002.
     * When trying to forcefully unlock the token amount that exceeds
     * the amount of locked tokens, we will receive an error 028006.
     * msg.sender is the source address of the token lock
     *
     * @param _who User address for which forced unlocking will be performed
     *
     * @param _amount The amount of tokens for forced unlocking
     */
    function forceUnlock(address _who, uint256 _amount) external;

    /**
     * @notice announceNewVoting
     * Agent is used to announce a new Voting Agent.
     * All delegated weight is removed from the old Voting Agent if it was.
     *
     * @param _newAgent New Voting Agent Address
     */
    function announceNewVotingAgent(address _newAgent) external;

    /**
     * @notice setNewVotingAgent is used to set a pending voting agent.
     * The voting agent must be advertised before calling this method.
     */
    function setNewVotingAgent() external;

    /**
     * @notice extendLocking is used by voting contracts during vote function execution.
     *
     * @dev Voting contracts extend lockedUntil by the voting end time.
     * If _lockNeededUntil less than lockedUntil - nothing changes
     *
     * @param _who User address for which a certain number of tokens will be unlocked
     * @param _lockNeededUntil Time until which tokens should be locked
     * @return total weight of tokens for the voting
     */
    function extendLocking(address _who, uint256 _lockNeededUntil) external returns (uint256);

    /**
     * @notice getLockInfo is used to get token locking information
     * for a specific token lock source address and user address
     *
     * @dev We receive error 028001 if the address of the token lock source is passed to the method,
     * the key of which is not in the list of token lock sources
     *
     * @param _tokenLockSource The address of the contract in which the lock of tokens can be produced
     *
     * @param _who User address for which token lock information will be returned
     * @return information about the current lock of tokens as an object of the VotingLockInfo structure
     */
    function getLockInfo(address _tokenLockSource, address _who) external view returns (VotingLockInfo memory);

    /**
     * @notice Returns base information about the user's voting rights
     * @param _user User's address
     * @param _votingEndTime End time of the voting period for the proposal
     * @return BaseVotingWeightInfo struct
     */
    function getBaseVotingWeightInfo(address _user, uint256 _votingEndTime)
        external
        view
        returns (BaseVotingWeightInfo memory);

    /**
     * @notice Returns locking time, until which stakes are locked for given `_user`
     * @param _user given user
     * @return time, until which stakes are locked
     */
    function getLockedUntil(address _user) external view returns (uint256);

    /**
     * @notice getVotingWeight is used to get the current voting weight for a specific user
     *
     * @param _who User address for which the current voting weight will be returned
     *
     * @param _lockNeededUntil Time until which tokens should be locked
     * @return total weight of tokens for voting
     */
    function getVotingWeight(address _who, uint256 _lockNeededUntil) external view returns (uint256);

    /**
     * @notice getLockedAmount is used to get the total number of locked tokens
     * from all current sources of token lock for a specific user
     *
     * @param _who User address for which the total amount of locked tokens will be returned
     * @return total number of locked tokens in all sources of the token lock
     */
    function getLockedAmount(address _who) external view returns (uint256);
}
