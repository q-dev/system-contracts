// SPDX-License-Identifier: LGPL-3.0-or-later
pragma solidity 0.8.9;

/**
 * @title Upgradable interface
 * @notice You can use this interface to create your own upgradable contracts
 */
interface IUpgradable {
    /**
     * @notice Updates the current implementation of the contract
     * @param _newImplementation New contract implementation
     */
    function upgradeTo(address _newImplementation) external;

    /**
     * @return The address of the implementation of the contract.
     */
    function implementation() external returns (address);
}
