import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  TokenBridgeAdminProxy__factory,
  Ownable__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

import { getConfigJson } from "@/deploy/config/config-parser";

export = async (deployer: Deployer) => {
  const config = getConfigJson();

  const tba = await deployer.deploy(TokenBridgeAdminProxy__factory);

  const registry = await deployer.deployed(ContractRegistry__factory);
  const registryAddress: string = await (registry as any).address;

  console.info(`Using Contract Registry at ${registry} as Upgrader`);

  const tbaProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await tba.getAddress(), registryAddress, "0x"],
    { name: "TokenBridgeAdminProxy" }
  );

  const bridgeValidatorAddr = config.bridge_validators;

  const ownable = await deployer.deployed(
    Ownable__factory,
    bridgeValidatorAddr
  );

  await ownable.transferOwnership((await tbaProxy.getAddress()) as any);

  const tokenBridgeAdminProxy = await deployer.deployed(
    TokenBridgeAdminProxy__factory,
    await tbaProxy.getAddress()
  );

  await tokenBridgeAdminProxy.initialize(registryAddress, bridgeValidatorAddr);

  await tokenBridgeAdminProxy.transferOwnership(config.epdr_safe);

  Reporter.reportContracts(
    ["[Home] Token Bridge Admin Proxy", await tbaProxy.getAddress()],
    ["[Home] Token Bridge Admin Proxy Implementation", await tba.getAddress()]
  );
};
