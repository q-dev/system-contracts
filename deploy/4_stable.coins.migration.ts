import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import { StableCoinERC20__factory } from "@ethers-v6";

import { getConfigJson } from "@/deploy/config/config-parser";

export = async (deployer: Deployer) => {
  const config = getConfigJson();

  const contractOwner = config.foreign_epdr_safe;

  const dai = await deployer.deploy(StableCoinERC20__factory);

  await dai.__StableCoinERC20_init(
    "DAI Stablecoin",
    "DAI",
    "18",
    "1000000000000000000000000000"
  );

  await dai.transferOwnership(contractOwner);

  const usdc = await deployer.deploy(StableCoinERC20__factory);

  await usdc.__StableCoinERC20_init("USD Coin", "USDC", "6", "1000000000000");

  await usdc.transferOwnership(contractOwner);

  const wbtc = await deployer.deploy(StableCoinERC20__factory);

  await wbtc.__StableCoinERC20_init(
    "Wrapped BTC",
    "WBTC",
    "8",
    "1000000000000"
  );

  await wbtc.transferOwnership(contractOwner);

  const vnxau = await deployer.deploy(StableCoinERC20__factory);

  await vnxau.__StableCoinERC20_init(
    "VNX AU",
    "VNXAU",
    "18",
    "1000000000000000000000000000"
  );

  await vnxau.transferOwnership(contractOwner);

  Reporter.reportContracts(
    ["DAI Stablecoin", await dai.getAddress()],
    ["USD Coin", await usdc.getAddress()],
    ["Wrapped BTC", await wbtc.getAddress()],
    ["VNX AU", await vnxau.getAddress()]
  );
};
