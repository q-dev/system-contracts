import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import { FxPriceFeedMedianizer__factory } from "@ethers-v6";

export async function deployFxPriceFeedMedianizer(
  config: any,
  deployer: Deployer,
  pair: string,
  decimal: string,
  subFeedsList: string[],
  baseTokenAddr: string,
  roundTime: string,
  minSubmissionsCount: string,
  maxSubmissionsCount: string,
  suffix = "_oracle",
  expertPanel = "epdr"
) {
  if (!baseTokenAddr) {
    console.log(`baseTokenAddr is not defined for ${pair}`);

    return;
  }

  const fxPriceFeedMedianizer = await deployer.deploy(
    FxPriceFeedMedianizer__factory,
    [
      pair,
      decimal,
      subFeedsList,
      baseTokenAddr,
      roundTime,
      minSubmissionsCount,
      maxSubmissionsCount,
    ],
    { name: `FxPriceFeedMedianizer_${pair}` }
  );

  await fxPriceFeedMedianizer.transferOwnership(
    config.safe[`home_${expertPanel}`]
  );

  Reporter.reportContracts([
    `fxPriceFeedMedianizer for ${pair}`,
    await fxPriceFeedMedianizer.getAddress(),
  ]);

  config.system_contracts[`${expertPanel}_path`].governed[
    `${expertPanel.toUpperCase()}`
  ][`${pair}${suffix}`] = {
    type: "address",
    value: await fxPriceFeedMedianizer.getAddress(),
  };
}
