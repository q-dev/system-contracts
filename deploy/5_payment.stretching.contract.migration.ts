import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import {
  PaymentStretchingBuckets__factory,
  DefaultAllocationThrottle__factory,
  TransparentUpgradeableProxy__factory,
} from "@ethers-v6";

import { getConfigJson } from "@/deploy/config/config-parser";

export = async (deployer: Deployer) => {
  const config = getConfigJson();

  const recipient =
    config.system_contracts.payment_stretching_contract.recipient;
  const allocationPeriod =
    config.system_contracts.payment_stretching_contract.time_unit;
  const defaultBucketCount =
    config.system_contracts.payment_stretching_contract.default_bucket_count;
  const proxyContractAdmin = config.safe.home_registry_maintainers;

  const defaultAllocationThrottle = await deployer.deploy(
    DefaultAllocationThrottle__factory
  );

  const initCalldata =
    PaymentStretchingBuckets__factory.createInterface().encodeFunctionData(
      "PaymentStretchingBuckets__init",
      [recipient, allocationPeriod, defaultBucketCount]
    );

  const proxyContract = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [
      await defaultAllocationThrottle.getAddress(),
      proxyContractAdmin,
      initCalldata,
    ],
    { name: "DefaultAllocationThrottleProxy" }
  );

  Reporter.reportContracts(
    [
      "DefaultAllocationThrottle implementation",
      await defaultAllocationThrottle.getAddress(),
    ],
    ["DefaultAllocationThrottle", await proxyContract.getAddress()]
  );
};
