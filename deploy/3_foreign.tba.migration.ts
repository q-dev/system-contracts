import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  ForeignChainTokenBridgeAdminProxy__factory,
  ProxyAdmin__factory,
  Ownable__factory,
} from "@ethers-v6";

import { getConfigJson } from "@/deploy/config/config-parser";

export = async (deployer: Deployer) => {
  const config = getConfigJson();

  const foreignEPDRSafe = config.foreign_epdr_safe;

  console.info(`Using EPDR Safe at ${foreignEPDRSafe} as Upgrader`);

  const ftba = await deployer.deploy(
    ForeignChainTokenBridgeAdminProxy__factory
  );

  const proxyAdmin = await deployer.deploy(ProxyAdmin__factory);

  const ftbaProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ftba.getAddress(), await proxyAdmin.getAddress(), "0x"],
    { name: "ForeignTokenBridgeAdminProxy" }
  );

  const foreignBridgeValidatorAddr = config.foreign_bridge_validators;

  const ownable = await deployer.deployed(
    Ownable__factory,
    foreignBridgeValidatorAddr
  );

  await ownable.transferOwnership((await ftbaProxy.getAddress()) as any);

  await proxyAdmin.transferOwnership(config.foreign_epdr_safe);

  const foreignTokenBridgeAdminProxy = await deployer.deployed(
    ForeignChainTokenBridgeAdminProxy__factory,
    await ftbaProxy.getAddress()
  );

  await foreignTokenBridgeAdminProxy.initialize(foreignBridgeValidatorAddr);

  await foreignTokenBridgeAdminProxy.transferOwnership(
    config.foreign_epdr_safe
  );

  Reporter.reportContracts(
    ["[Foreign] Token Bridge Admin Proxy", await ftbaProxy.getAddress()],
    [
      "[Foreign] Token Bridge Admin Proxy Implementation",
      await ftba.getAddress(),
    ]
  );
};
