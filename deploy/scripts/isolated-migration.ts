import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import { WBTC__factory } from "@ethers-v6";

export const isolatedMigration = async (config: any, deployer: Deployer) => {
  const wbtc = await deployer.deploy(WBTC__factory);

  Reporter.reportContracts(["WBTC", await wbtc.getAddress()]);

  config.system_contracts.epdr_path.governed.EPDR.QBTC_address.value =
    await wbtc.getAddress();
};
