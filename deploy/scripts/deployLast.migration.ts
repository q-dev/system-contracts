import { Deployer } from "@solarity/hardhat-migrate";

import {
  ContractRegistry__factory,
  SystemReserve__factory,
  TransparentUpgradeableProxy__factory,
} from "@ethers-v6";

export const deployLast = async (
  registryAddress: string,
  deployer: Deployer
) => {
  const systemReserveImpl = await deployer.deploy(SystemReserve__factory);

  const systemReserveProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [systemReserveImpl.getAddress(), registryAddress, "0x"],
    { name: "SystemReserveProxy" }
  );

  const systemReserve = await deployer.deployed(
    SystemReserve__factory,
    await systemReserveProxy.getAddress()
  );

  const keys = ["defi.QUSD.systemDebtAuction"];

  await systemReserve.initialize(registryAddress, keys);

  const systemReserveKey = "tokeneconomics.systemReserve";

  const registry = await deployer.deployed(
    ContractRegistry__factory,
    registryAddress
  );
  await registry.setAddresses(
    [systemReserveKey],
    [await systemReserve.getAddress()]
  );

  return [systemReserve];
};
