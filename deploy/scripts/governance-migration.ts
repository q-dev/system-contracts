import { Deployer } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  Constitution__factory,
  ConstitutionVoting__factory,
  Roots__factory,
  RootsVoting__factory,
  RootNodesSlashingVoting__factory,
  RootNodeSlashingEscrow__factory,
  Validators__factory,
  ValidatorsSlashingVoting__factory,
  ValidatorSlashingEscrow__factory,
  EmergencyUpdateVoting__factory,
  GeneralUpdateVoting__factory,
  VotingWeightProxy__factory,
  ContractRegistryUpgradeVoting__factory,
  ContractRegistryAddressVoting__factory,
  AccountAliases__factory,
  AddressStorageStakes__factory,
  AddressStorageStakesSorted__factory,
  WithdrawAddresses__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

import {
  getAddressKeysAndValues,
  getBoolKeysAndValues,
  getStringKeysAndValues,
  getUintKeysAndValues,
  validateHexString,
} from "@/deploy/config/config-parser";

export const deployGovernance = async (
  config: any,
  registryAddress: string,
  deployer: Deployer
) => {
  const constitutionImpl = await deployer.deploy(Constitution__factory);
  const constitutionVotingImpl = await deployer.deploy(
    ConstitutionVoting__factory
  );
  const rootsImpl = await deployer.deploy(Roots__factory);
  const rootsVotingImpl = await deployer.deploy(RootsVoting__factory);
  const rootNodesSlashingVotingImpl = await deployer.deploy(
    RootNodesSlashingVoting__factory
  );
  const rootNodeSlashingEscrowImpl = await deployer.deploy(
    RootNodeSlashingEscrow__factory
  );
  const validatorsImpl = await deployer.deploy(Validators__factory);
  const validatorsSlashingVotingImpl = await deployer.deploy(
    ValidatorsSlashingVoting__factory
  );
  const validatorSlashingEscrowImpl = await deployer.deploy(
    ValidatorSlashingEscrow__factory
  );
  const emergencyUpdateVotingImpl = await deployer.deploy(
    EmergencyUpdateVoting__factory
  );
  const generalUpdateVotingImpl = await deployer.deploy(
    GeneralUpdateVoting__factory
  );
  const votingWeightProxyImpl = await deployer.deploy(
    VotingWeightProxy__factory
  );
  const contractRegistryUpgradeVotingImpl = await deployer.deploy(
    ContractRegistryUpgradeVoting__factory
  );
  const contractRegistryAddressVotingImpl = await deployer.deploy(
    ContractRegistryAddressVoting__factory
  );
  const accountAliasesImpl = await deployer.deploy(AccountAliases__factory);
  const withdrawAddressesImpl = await deployer.deploy(
    WithdrawAddresses__factory
  );

  const constitutionProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await constitutionImpl.getAddress(), registryAddress, "0x"],
    { name: "ConstitutionProxy" }
  );

  const constitution = await deployer.deployed(
    Constitution__factory,
    await constitutionProxy.getAddress()
  );

  const [uintKeys, uintValues] = getUintKeysAndValues(
    config.system_contracts.constitution.constitution,
    "constitution."
  );
  const [addressKeys, addressValues] = getAddressKeysAndValues(
    config.system_contracts.constitution.constitution,
    "constitution."
  );
  const [stringKeys, stringValues] = getStringKeysAndValues(
    config.system_contracts.constitution.constitution,
    "constitution."
  );
  const [boolKeys, boolValues] = getBoolKeysAndValues(
    config.system_contracts.constitution.constitution,
    "constitution."
  );

  const deployerAccount = await (await deployer.getSigner()).getAddress();

  const registry = await deployer.deployed(
    ContractRegistry__factory,
    registryAddress
  );

  await registry.setAddress(
    "governance.constitution.parametersVoting",
    deployerAccount
  );

  await constitution.initialize(
    registryAddress,
    uintKeys.slice(0, 60),
    uintValues.slice(0, 60),
    addressKeys,
    addressValues,
    stringKeys,
    stringValues,
    boolKeys,
    boolValues
  );

  for (let i = 60; i < uintKeys.length; i++) {
    await constitution.setUint(uintKeys[i], uintValues[i]);
  }

  const constitutionVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await constitutionVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "ConstitutionVotingProxy" }
  );

  const constitutionVoting = await deployer.deployed(
    ConstitutionVoting__factory,
    await constitutionVotingProxy.getAddress()
  );

  const constitutionHash = validateHexString(
    config.system_contracts.constitution.meta.info.hash
  );

  await constitutionVoting.initialize(constitutionHash, registryAddress);

  const rootsProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await rootsImpl.getAddress(), registryAddress, "0x"],
    { name: "RootsProxy" }
  );

  const roots = await deployer.deployed(
    Roots__factory,
    await rootsProxy.getAddress()
  );

  const rootNodes =
    config.system_contracts.panel_members.panelMembership.rootNodes;

  await roots.initialize(registryAddress, rootNodes);

  const rootsVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await rootsVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "RootsVotingProxy" }
  );

  const rootsVoting = await deployer.deployed(
    RootsVoting__factory,
    await rootsVotingProxy.getAddress()
  );

  await rootsVoting.initialize(registryAddress);

  const rootNodesSlashingVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await rootNodesSlashingVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "RootNodesSlashingVotingProxy" }
  );
  const rootNodesSlashingVoting = await deployer.deployed(
    RootNodesSlashingVoting__factory,
    await rootNodesSlashingVotingProxy.getAddress()
  );

  await rootNodesSlashingVoting.initialize(registryAddress);

  const rootNodeSlashingEscrowProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await rootNodeSlashingEscrowImpl.getAddress(), registryAddress, "0x"],
    { name: "RootNodeSlashingEscrowProxy" }
  );
  const rootNodeSlashingEscrow = await deployer.deployed(
    RootNodeSlashingEscrow__factory,
    await rootNodeSlashingEscrowProxy.getAddress()
  );

  await rootNodeSlashingEscrow.initialize(registryAddress);

  const validatorProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await validatorsImpl.getAddress(), registryAddress, "0x"],
    { name: "ValidatorProxy" }
  );
  const validators = await deployer.deployed(
    Validators__factory,
    await validatorProxy.getAddress()
  );

  const addressStorageStakes = await deployer.deploy(
    AddressStorageStakes__factory
  );
  const addressStorageStakesSorted = await deployer.deploy(
    AddressStorageStakesSorted__factory
  );

  await addressStorageStakes.transferOwnership(await validators.getAddress());

  await addressStorageStakesSorted.transferOwnership(
    await validators.getAddress()
  );

  await validators.initialize(
    registryAddress,
    await addressStorageStakesSorted.getAddress(),
    await addressStorageStakes.getAddress()
  );

  const validatorsSlashingVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await validatorsSlashingVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "ValidatorsSlashingVotingProxy" }
  );
  const validatorsSlashingVoting = await deployer.deployed(
    ValidatorsSlashingVoting__factory,
    await validatorsSlashingVotingProxy.getAddress()
  );

  await validatorsSlashingVoting.initialize(registryAddress);

  const validatorSlashingEscrowProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await validatorSlashingEscrowImpl.getAddress(), registryAddress, "0x"],
    { name: "ValidatorSlashingEscrowProxy" }
  );
  const validatorSlashingEscrow = await deployer.deployed(
    ValidatorSlashingEscrow__factory,
    await validatorSlashingEscrowProxy.getAddress()
  );

  await validatorSlashingEscrow.initialize(registryAddress);

  const emergencyUpdateVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await emergencyUpdateVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EmergencyUpdateVotingProxy" }
  );
  const emergencyUpdateVoting = await deployer.deployed(
    EmergencyUpdateVoting__factory,
    await emergencyUpdateVotingProxy.getAddress()
  );

  await emergencyUpdateVoting.initialize(registryAddress);

  const generalUpdateVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await generalUpdateVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "GeneralUpdateVotingProxy" }
  );
  const generalUpdateVoting = await deployer.deployed(
    GeneralUpdateVoting__factory,
    await generalUpdateVotingProxy.getAddress()
  );

  await generalUpdateVoting.initialize(registryAddress);

  const votingWeightProxyProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await votingWeightProxyImpl.getAddress(), registryAddress, "0x"],
    { name: "VotingWeightProxyProxy" }
  );
  const votingWeightProxy = await deployer.deployed(
    VotingWeightProxy__factory,
    await votingWeightProxyProxy.getAddress()
  );

  const tokenLockSourcesKeys = [
    "governance.rootNodes",
    "tokeneconomics.qVault",
    "governance.validators",
    "tokeneconomics.vesting",
  ];

  const votingContractsKeys = [
    "governance.experts.EPDR.membershipVoting",
    "governance.experts.EPRS.membershipVoting",
    "governance.experts.EPQFI.membershipVoting",
    "governance.rootNodes.membershipVoting",
    "governance.rootNodes.slashingVoting",
    "governance.constitution.parametersVoting",
    "governance.generalUpdateVoting",
  ];

  await votingWeightProxy.initialize(
    registryAddress,
    tokenLockSourcesKeys,
    votingContractsKeys
  );

  const contractRegistryUpgradeVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [
      await contractRegistryUpgradeVotingImpl.getAddress(),
      registryAddress,
      "0x",
    ],
    { name: "ContractRegistryUpgradeVotingProxy" }
  );
  const contractRegistryUpgradeVoting = await deployer.deployed(
    ContractRegistryUpgradeVoting__factory,
    await contractRegistryUpgradeVotingProxy.getAddress()
  );

  await contractRegistryUpgradeVoting.initialize(registryAddress);

  await contractRegistryUpgradeVoting.transferOwnership(registryAddress);

  await registry.setMaintainer(
    await contractRegistryUpgradeVoting.getAddress()
  );

  const contractRegistryAddressVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [
      await contractRegistryAddressVotingImpl.getAddress(),
      registryAddress,
      "0x",
    ],
    { name: "ContractRegistryAddressVotingProxy" }
  );
  const contractRegistryAddressVoting = await deployer.deployed(
    ContractRegistryAddressVoting__factory,
    await contractRegistryAddressVotingProxy.getAddress()
  );

  await contractRegistryAddressVoting.initialize(registryAddress);

  await contractRegistryAddressVoting.transferOwnership(registryAddress);

  await registry.setMaintainer(
    await contractRegistryAddressVoting.getAddress()
  );

  const accountAliasesProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await accountAliasesImpl.getAddress(), registryAddress, "0x"],
    { name: "AccountAliasesProxy" }
  );
  const accountAliases = await deployer.deployed(
    AccountAliases__factory,
    await accountAliasesProxy.getAddress()
  );

  const withdrawAddressesProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await withdrawAddressesImpl.getAddress(), registryAddress, "0x"],
    { name: "WithdrawAddressesProxy" }
  );
  const withdrawAddresses = await deployer.deployed(
    WithdrawAddresses__factory,
    await withdrawAddressesProxy.getAddress()
  );

  await withdrawAddresses.initialize();

  await withdrawAddresses.transferOwnership(config.safe.home_epdr);

  const constitutionKey = "governance.constitution.parameters";
  const parametersVotingKey = "governance.constitution.parametersVoting";
  const rootNodesKey = "governance.rootNodes";
  const rootMembershipVotingKey = "governance.rootNodes.membershipVoting";
  const rootSlashingVotingKey = "governance.rootNodes.slashingVoting";
  const rootSlashingEscrowKey = "governance.rootNodes.slashingEscrow";
  const validatorKey = "governance.validators";
  const validatorSlashingVotingKey = "governance.validators.slashingVoting";
  const validatorSlashingEscrowKey = "governance.validators.slashingEscrow";
  const emergencyUpdateVotingKey = "governance.emergencyUpdateVoting";
  const generalUpdateVotingKey = "governance.generalUpdateVoting";
  const votingWeightProxyKey = "governance.votingWeightProxy";
  const contractRegistryVotingKey = "governance.upgrade.contractRegistryVoting";
  const contractRegistryAddressVotingKey =
    await "governance.getAddress().contractRegistryVoting";
  const accountAliasesKey = "governance.accountAliases";
  const withdrawAddressesKey = "tokeneconomics.withdrawAddresses";

  await registry.setAddresses(
    [
      constitutionKey,
      parametersVotingKey,
      rootNodesKey,
      rootMembershipVotingKey,
      rootSlashingVotingKey,
      rootSlashingEscrowKey,
      validatorKey,
      validatorSlashingVotingKey,
      validatorSlashingEscrowKey,
      emergencyUpdateVotingKey,
      generalUpdateVotingKey,
      votingWeightProxyKey,
      contractRegistryVotingKey,
      contractRegistryAddressVotingKey,
      accountAliasesKey,
      withdrawAddressesKey,
    ],
    [
      await constitution.getAddress(),
      await constitutionVoting.getAddress(),
      await roots.getAddress(),
      await rootsVoting.getAddress(),
      await rootNodesSlashingVoting.getAddress(),
      await rootNodeSlashingEscrow.getAddress(),
      await validators.getAddress(),
      await validatorsSlashingVoting.getAddress(),
      await validatorSlashingEscrow.getAddress(),
      await emergencyUpdateVoting.getAddress(),
      await generalUpdateVoting.getAddress(),
      await votingWeightProxy.getAddress(),
      await contractRegistryUpgradeVoting.getAddress(),
      await contractRegistryAddressVoting.getAddress(),
      await accountAliases.getAddress(),
      await withdrawAddresses.getAddress(),
    ]
  );

  return [
    constitution,
    constitutionVoting,
    roots,
    rootsVoting,
    rootNodesSlashingVoting,
    rootNodeSlashingEscrow,
    validators,
    validatorsSlashingVoting,
    validatorSlashingEscrow,
    emergencyUpdateVoting,
    generalUpdateVoting,
    votingWeightProxy,
    contractRegistryUpgradeVoting,
    contractRegistryAddressVoting,
    accountAliases,
    withdrawAddresses,
  ];
};
