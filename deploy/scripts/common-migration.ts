import { Deployer } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  DefaultAllocationProxy__factory,
  CompoundRateKeeperFactory__factory,
  CompoundRateKeeper__factory,
  AddressStorageFactory__factory,
  AddressStorage__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

export async function deployCommon(
  registryAddress: string,
  deployer: Deployer
) {
  const defaultAllocationProxyImpl = await deployer.deploy(
    DefaultAllocationProxy__factory
  );
  const crKeeperFactoryImpl = await deployer.deploy(
    CompoundRateKeeperFactory__factory
  );
  const addressStorageFactoryImpl = await deployer.deploy(
    AddressStorageFactory__factory
  );

  const defaultAllocationProxyProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await defaultAllocationProxyImpl.getAddress(), registryAddress, "0x"],
    { name: "DefaultAllocationProxy" }
  );
  const defaultAllocationProxy = await deployer.deployed(
    DefaultAllocationProxy__factory,
    await defaultAllocationProxyProxy.getAddress()
  );

  const crKeeperFactoryProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await crKeeperFactoryImpl.getAddress(), registryAddress, "0x"],
    { name: "CompoundRateKeeperFactory" }
  );
  const crKeeperFactory = await deployer.deployed(
    CompoundRateKeeperFactory__factory,
    await crKeeperFactoryProxy.getAddress()
  );
  const addressStorageFactoryProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await addressStorageFactoryImpl.getAddress(), registryAddress, "0x"],
    { name: "AddressStorageFactory" }
  );

  const addressStorageFactory = await deployer.deployed(
    AddressStorageFactory__factory,
    await addressStorageFactoryProxy.getAddress()
  );

  const beneficiaries = [
    "tokeneconomics.validationRewardProxy",
    "tokeneconomics.rootNodeRewardProxy",
    "tokeneconomics.qHolderRewardProxy",
  ];

  const shares = [
    "constitution.rewardShareValidatorNodes",
    "constitution.rewardShareRootNodes",
    "constitution.rewardShareQTokenHolders",
  ];

  await defaultAllocationProxy.initialize(
    registryAddress,
    beneficiaries,
    shares
  );

  const crKeeperImpl = await deployer.deploy(CompoundRateKeeper__factory);
  await crKeeperFactory.initialize(await crKeeperImpl.getAddress());

  const addressStorageImpl = await deployer.deploy(AddressStorage__factory);
  await addressStorageFactory.initialize(await addressStorageImpl.getAddress());

  const defaultAllocationProxyKey = "tokeneconomics.defaultAllocationProxy";
  const crKeeperFactoryKey = "common.factory.crKeeper";
  const addressStorageFactoryKey = "common.factory.addressStorage";

  const registry = await deployer.deployed(
    ContractRegistry__factory,
    registryAddress
  );
  await registry.setAddresses(
    [defaultAllocationProxyKey, crKeeperFactoryKey, addressStorageFactoryKey],
    [
      await defaultAllocationProxy.getAddress(),
      await crKeeperFactory.getAddress(),
      await addressStorageFactory.getAddress(),
    ]
  );

  return [
    defaultAllocationProxy,
    crKeeperFactory,
    addressStorageFactory,
    crKeeperImpl,
    addressStorageImpl,
  ];
}
