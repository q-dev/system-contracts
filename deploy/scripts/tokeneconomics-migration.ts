import { Deployer } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  ValidationRewardProxy__factory,
  RootNodeRewardProxy__factory,
  QHolderRewardProxy__factory,
  QHolderRewardPool__factory,
  ValidationRewardPools__factory,
  QVault__factory,
  Vesting__factory,
  PushPayments__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

export async function deployTokeneconomics(
  config: any,
  registryAddress: string,
  deployer: Deployer
) {
  const validationRewardProxyImpl = await deployer.deploy(
    ValidationRewardProxy__factory
  );
  const rootNodeRewardProxyImpl = await deployer.deploy(
    RootNodeRewardProxy__factory
  );
  const qHolderRewardProxyImpl = await deployer.deploy(
    QHolderRewardProxy__factory
  );
  const qHolderRewardPoolImpl = await deployer.deploy(
    QHolderRewardPool__factory
  );
  const validationRewardPoolsImpl = await deployer.deploy(
    ValidationRewardPools__factory
  );
  const qVaultImpl = await deployer.deploy(QVault__factory);
  const vestingImpl = await deployer.deploy(Vesting__factory);
  const pushPaymentsImpl = await deployer.deploy(PushPayments__factory);

  const validationRewardProxyProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await validationRewardProxyImpl.getAddress(), registryAddress, "0x"],
    { name: "ValidationRewardProxy" }
  );
  const validationRewardProxy = await deployer.deployed(
    ValidationRewardProxy__factory,
    await validationRewardProxyProxy.getAddress()
  );

  await validationRewardProxy.initialize(registryAddress);

  const rootNodeRewardProxyProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await rootNodeRewardProxyImpl.getAddress(), registryAddress, "0x"],
    { name: "RootNodeRewardProxy" }
  );
  const rootNodeRewardProxy = await deployer.deployed(
    RootNodeRewardProxy__factory,
    await rootNodeRewardProxyProxy.getAddress()
  );

  await rootNodeRewardProxy.initialize(registryAddress);

  const qHolderRewardProxyProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await qHolderRewardProxyImpl.getAddress(), registryAddress, "0x"],
    { name: "QHolderRewardProxy" }
  );
  const qHolderRewardProxy = await deployer.deployed(
    QHolderRewardProxy__factory,
    await qHolderRewardProxyProxy.getAddress()
  );

  await qHolderRewardProxy.initialize(registryAddress);

  const qHolderRewardPoolProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await qHolderRewardPoolImpl.getAddress(), registryAddress, "0x"],
    { name: "QHolderRewardPool" }
  );
  const qHolderRewardPool = await deployer.deployed(
    QHolderRewardPool__factory,
    await qHolderRewardPoolProxy.getAddress()
  );

  await qHolderRewardPool.initialize(registryAddress);

  const validationRewardPoolsProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await validationRewardPoolsImpl.getAddress(), registryAddress, "0x"],
    { name: "ValidationRewardPools" }
  );
  const validationRewardPools = await deployer.deployed(
    ValidationRewardPools__factory,
    await validationRewardPoolsProxy.getAddress()
  );

  await validationRewardPools.initialize(
    registryAddress,
    config.system_contracts.cr_update_minimum_base
  );

  const qVaultProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await qVaultImpl.getAddress(), registryAddress, "0x"],
    { name: "QVault" }
  );
  const qVault = await deployer.deployed(
    QVault__factory,
    await qVaultProxy.getAddress()
  );

  await qVault.initialize(registryAddress);

  const vestingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await vestingImpl.getAddress(), registryAddress, "0x"],
    { name: "VestingProxy" }
  );

  const vesting = await deployer.deployed(
    Vesting__factory,
    await vestingProxy.getAddress()
  );

  await vesting.initialize(registryAddress);

  const pushPaymentProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await pushPaymentsImpl.getAddress(), registryAddress, "0x"],
    { name: "PushPaymentsProxy" }
  );

  const pushPayments = await deployer.deployed(
    PushPayments__factory,
    await pushPaymentProxy.getAddress()
  );

  const validationRewardProxyKey = "tokeneconomics.validationRewardProxy";
  const rootNodeRewardProxyKey = "tokeneconomics.rootNodeRewardProxy";
  const qHolderRewardProxyKey = "tokeneconomics.qHolderRewardProxy";
  const qHolderRewardPoolKey = "tokeneconomics.qHolderRewardPool";
  const validationRewardPoolsKey = "tokeneconomics.validationRewardPools";
  const qVaultKey = "tokeneconomics.qVault";
  const vestingKey = "tokeneconomics.vesting";
  const pushPaymentsKey = "tokeneconomics.pushPayments";

  const registry = await deployer.deployed(
    ContractRegistry__factory,
    registryAddress
  );

  await registry.setAddresses(
    [
      validationRewardProxyKey,
      rootNodeRewardProxyKey,
      qHolderRewardProxyKey,
      qHolderRewardPoolKey,
      validationRewardPoolsKey,
      qVaultKey,
      vestingKey,
      pushPaymentsKey,
    ],
    [
      await validationRewardProxy.getAddress(),
      await rootNodeRewardProxy.getAddress(),
      await qHolderRewardProxy.getAddress(),
      await qHolderRewardPool.getAddress(),
      await validationRewardPools.getAddress(),
      await qVault.getAddress(),
      await vesting.getAddress(),
      await pushPayments.getAddress(),
    ]
  );

  return [
    validationRewardProxy,
    rootNodeRewardProxy,
    qHolderRewardProxy,
    qHolderRewardPool,
    validationRewardPools,
    qVault,
    vesting,
    pushPayments,
  ];
}
