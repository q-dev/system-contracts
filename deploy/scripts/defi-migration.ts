import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  BorrowingCore__factory,
  Saving__factory,
  SystemBalance__factory,
  LiquidationAuction__factory,
  SystemSurplusAuction__factory,
  SystemDebtAuction__factory,
  StableCoin__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

import { deployFxPriceFeedMedianizer } from "@/deploy/utils/oracle-deployment";

export async function deployDeFi(
  config: any,
  registryAddress: string,
  deployer: Deployer
) {
  const borrowingCoreImpl = await deployer.deploy(BorrowingCore__factory);
  const savingImpl = await deployer.deploy(Saving__factory);
  const systemBalanceImpl = await deployer.deploy(SystemBalance__factory);
  const liquidationAuctionImpl = await deployer.deploy(
    LiquidationAuction__factory
  );
  const systemSurplusAuctionImpl = await deployer.deploy(
    SystemSurplusAuction__factory
  );
  const systemDebtAuctionImpl = await deployer.deploy(
    SystemDebtAuction__factory
  );

  const stableCoins = config.system_contracts.stable_coins;

  for (const stc of stableCoins) {
    const stableCoin = await deployer.deploy(
      StableCoin__factory,
      [
        registryAddress,
        stc,
        stc,
        [`defi.${stc}.borrowing`, `defi.${stc}.saving`],
        config.gsn.forwarder,
      ],
      { name: `StableCoin_${stc}` }
    );

    const borrowingCoreProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await borrowingCoreImpl.getAddress(), registryAddress, "0x"],
      { name: `BorrowingCore_${stc}Proxy` }
    );
    const borrowingCore = await deployer.deployed(
      BorrowingCore__factory,
      await borrowingCoreProxy.getAddress()
    );

    await borrowingCore.initialize(registryAddress, stc);

    const decimal = "18";
    const subFeedsList =
      config.system_contracts.panel_members.panelMembership
        .priceOracleMaintainers;
    const roundTime = config.system_contracts.oracle.round_time;
    const minSubmissionsCount =
      config.system_contracts.oracle.min_submissions_count;
    const maxSubmissionsCount =
      config.system_contracts.oracle.max_submissions_count;

    await deployFxPriceFeedMedianizer(
      config,
      deployer,
      `QBTC_${stc}`,
      decimal,
      subFeedsList,
      config.system_contracts.epdr_path.governed.EPDR.QBTC_address.value,
      roundTime,
      minSubmissionsCount,
      maxSubmissionsCount
    );

    const savingProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await savingImpl.getAddress(), registryAddress, "0x"],
      { name: `Saving_${stc}Proxy` }
    );

    const saving = await deployer.deployed(
      Saving__factory,
      await savingProxy.getAddress()
    );

    await saving.initialize(registryAddress, stc);

    const systemBalanceProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await systemBalanceImpl.getAddress(), registryAddress, "0x"],
      { name: `SystemBalance_${stc}Proxy` }
    );
    const systemBalance = await deployer.deployed(
      SystemBalance__factory,
      await systemBalanceProxy.getAddress()
    );

    await systemBalance.initialize(registryAddress, stc);

    const liquidationAuctionProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await liquidationAuctionImpl.getAddress(), registryAddress, "0x"],
      { name: `LiquidationAuction_${stc}Proxy` }
    );
    const liquidationAuction = await deployer.deployed(
      LiquidationAuction__factory,
      await liquidationAuctionProxy.getAddress()
    );

    await liquidationAuction.initialize(registryAddress, stc);

    const systemSurplusAuctionProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await systemSurplusAuctionImpl.getAddress(), registryAddress, "0x"],
      { name: `SystemSurplusAuction_${stc}Proxy` }
    );

    const systemSurplusAuction = await deployer.deployed(
      SystemSurplusAuction__factory,
      await systemSurplusAuctionProxy.getAddress()
    );

    await systemSurplusAuction.initialize(registryAddress, stc);

    const systemDebtAuctionProxy = await deployer.deploy(
      TransparentUpgradeableProxy__factory,
      [await systemDebtAuctionImpl.getAddress(), registryAddress, "0x"],
      { name: `SystemDebtAuction_${stc}Proxy` }
    );

    const systemDebtAuction = await deployer.deployed(
      SystemDebtAuction__factory,
      await systemDebtAuctionProxy.getAddress()
    );

    await systemDebtAuction.initialize(registryAddress, stc);

    const stableCoinKey = `defi.${stc}`;
    const borrowingKey = `defi.${stc}.borrowing`;
    const savingKey = `defi.${stc}.saving`;
    const systemBalanceKey = `defi.${stc}.systemBalance`;
    const liquidationAuctionKey = `defi.${stc}.liquidationAuction`;
    const systemSurplusAuctionKey = `defi.${stc}.systemSurplusAuction`;
    const systemDebtAuctionKey = `defi.${stc}.systemDebtAuction`;

    const registry = await deployer.deployed(
      ContractRegistry__factory,
      registryAddress
    );
    await registry.setAddresses(
      [
        stableCoinKey,
        borrowingKey,
        savingKey,
        systemBalanceKey,
        liquidationAuctionKey,
        systemSurplusAuctionKey,
        systemDebtAuctionKey,
      ],
      [
        await stableCoin.getAddress(),
        await borrowingCore.getAddress(),
        await saving.getAddress(),
        await systemBalance.getAddress(),
        await liquidationAuction.getAddress(),
        await systemSurplusAuction.getAddress(),
        await systemDebtAuction.getAddress(),
      ]
    );

    console.info(`Defi contracts for ${stc} deployed successfully!`);
    Reporter.reportContracts(
      ["borrowingCore", await borrowingCore.getAddress()],
      ["saving", await saving.getAddress()],
      ["systemBalance", await systemBalance.getAddress()],
      ["liquidationAuction", await liquidationAuction.getAddress()],
      ["systemSurplusAuction", await systemSurplusAuction.getAddress()],
      ["systemDebtAuction", await systemDebtAuction.getAddress()]
    );
  }
}
