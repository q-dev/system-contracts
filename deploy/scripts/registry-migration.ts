import {
  TransparentUpgradeableProxy__factory,
  ContractRegistry__factory,
} from "@ethers-v6";
import { Deployer } from "@solarity/hardhat-migrate";

export async function deployRegistry(config: any, deployer: Deployer) {
  const homeRegistrySafe = config.safe.home_registry_maintainers;
  const registryMaintainerList =
    config.system_contracts.panel_members.panelMembership.registryMaintainers;

  const contractRegistryImpl = await deployer.deploy(ContractRegistry__factory);

  const contractRegistryProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [(contractRegistryImpl as any).address, homeRegistrySafe, "0x"],
    { name: "ContractRegistryProxy" }
  );

  const contractRegistry = await deployer.deployed(
    ContractRegistry__factory,
    await contractRegistryProxy.getAddress()
  );

  const keys: string[] = [];
  const addresses: string[] = [];

  const deployerAccount = await (await deployer.getSigner()).getAddress();

  registryMaintainerList.push(deployerAccount);

  await contractRegistry.initialize(registryMaintainerList, keys, addresses);

  return contractRegistry;
}
