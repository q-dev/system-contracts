import { Deployer } from "@solarity/hardhat-migrate";

import {
  TransparentUpgradeableProxy__factory,
  EPDR_Membership__factory,
  EPDR_MembershipVoting__factory,
  EPDR_Parameters__factory,
  EPDR_ParametersVoting__factory,
  EPQFI_Membership__factory,
  EPQFI_MembershipVoting__factory,
  EPQFI_Parameters__factory,
  EPQFI_ParametersVoting__factory,
  EPRS_Membership__factory,
  EPRS_MembershipVoting__factory,
  EPRS_Parameters__factory,
  EPRS_ParametersVoting__factory,
  ContractRegistry__factory,
} from "@ethers-v6";

import {
  getAddressKeysAndValues,
  getBoolKeysAndValues,
  getStringKeysAndValues,
  getUintKeysAndValues,
} from "@/deploy/config/config-parser";

import { deployFxPriceFeedMedianizer } from "@/deploy/utils/oracle-deployment";

export const deployExperts = async (
  config: any,
  registryAddress: string,
  deployer: Deployer
) => {
  const ePDR_MembershipImpl = await deployer.deploy(EPDR_Membership__factory);
  const ePDR_MembershipVotingImpl = await deployer.deploy(
    EPDR_MembershipVoting__factory
  );
  const ePDR_ParametersImpl = await deployer.deploy(EPDR_Parameters__factory);
  const ePDR_ParametersVotingImpl = await deployer.deploy(
    EPDR_ParametersVoting__factory
  );

  const ePQFI_MembershipImpl = await deployer.deploy(EPQFI_Membership__factory);
  const ePQFI_MembershipVotingImpl = await deployer.deploy(
    EPQFI_MembershipVoting__factory
  );
  const ePQFI_ParametersImpl = await deployer.deploy(EPQFI_Parameters__factory);
  const ePQFI_ParametersVotingImpl = await deployer.deploy(
    EPQFI_ParametersVoting__factory
  );

  const ePRS_MembershipImpl = await deployer.deploy(EPRS_Membership__factory);
  const ePRS_MembershipVotingImpl = await deployer.deploy(
    EPRS_MembershipVoting__factory
  );
  const ePRS_ParametersImpl = await deployer.deploy(EPRS_Parameters__factory);
  const ePRS_ParametersVotingImpl = await deployer.deploy(
    EPRS_ParametersVoting__factory
  );

  const ePDR_MembershipProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePDR_MembershipImpl.getAddress(), registryAddress, "0x"],
    { name: "EPDR_MembershipProxy" }
  );
  const ePDR_Membership = await deployer.deployed(
    EPDR_Membership__factory,
    await ePDR_MembershipProxy.getAddress()
  );

  const epdr_experts =
    config.system_contracts.panel_members.panelMembership.epdr;

  await ePDR_Membership.initialize(registryAddress, epdr_experts);

  await ePDR_Membership.transferOwnership(registryAddress);

  const ePDR_MembershipVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePDR_MembershipVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPDR_MembershipVotingProxy" }
  );
  const ePDR_MembershipVoting = await deployer.deployed(
    EPDR_MembershipVoting__factory,
    await ePDR_MembershipVotingProxy.getAddress()
  );

  await ePDR_MembershipVoting.initialize(registryAddress);

  const ePDR_ParametersProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePDR_ParametersImpl.getAddress(), registryAddress, "0x"],
    { name: "EPDR_ParametersProxy" }
  );

  const ePDR_Parameters = await deployer.deployed(
    EPDR_Parameters__factory,
    await ePDR_ParametersProxy.getAddress()
  );

  let [uintKeys, uintValues] = getUintKeysAndValues(
    config.system_contracts.epdr_path.governed.EPDR,
    "governed.EPDR."
  );
  let [addressKeys, addressValues] = getAddressKeysAndValues(
    config.system_contracts.epdr_path.governed.EPDR,
    "governed.EPDR."
  );
  let [stringKeys, stringValues] = getStringKeysAndValues(
    config.system_contracts.epdr_path.governed.EPDR,
    "governed.EPDR."
  );
  let [boolKeys, boolValues] = getBoolKeysAndValues(
    config.system_contracts.epdr_path.governed.EPDR,
    "governed.EPDR."
  );

  await ePDR_Parameters.initialize(
    registryAddress,
    uintKeys,
    uintValues,
    addressKeys,
    addressValues,
    stringKeys,
    stringValues,
    boolKeys,
    boolValues
  );

  const ePDR_ParametersVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePDR_ParametersVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPDR_ParametersVotingProxy" }
  );
  const ePDR_ParametersVoting = await deployer.deployed(
    EPDR_ParametersVoting__factory,
    await ePDR_ParametersVotingProxy.getAddress()
  );

  await ePDR_ParametersVoting.initialize(registryAddress);

  const ePQFI_MembershipProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePQFI_MembershipImpl.getAddress(), registryAddress, "0x"],
    { name: "EPQFI_MembershipProxy" }
  );

  const ePQFI_Membership = await deployer.deployed(
    EPQFI_Membership__factory,
    await ePQFI_MembershipProxy.getAddress()
  );

  const epqfi_experts =
    config.system_contracts.panel_members.panelMembership.epqfi;

  await ePQFI_Membership.initialize(registryAddress, epqfi_experts);

  await ePQFI_Membership.transferOwnership(registryAddress);

  const ePQFI_MembershipVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePQFI_MembershipVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPQFI_MembershipVotingProxy" }
  );
  const ePQFI_MembershipVoting = await deployer.deployed(
    EPQFI_MembershipVoting__factory,
    await ePQFI_MembershipVotingProxy.getAddress()
  );
  await ePQFI_MembershipVoting.initialize(registryAddress);

  const ePQFI_ParametersProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePQFI_ParametersImpl.getAddress(), registryAddress, "0x"],
    { name: "EPQFI_ParametersProxy" }
  );
  const ePQFI_Parameters = await deployer.deployed(
    EPQFI_Parameters__factory,
    await ePQFI_ParametersProxy.getAddress()
  );

  const decimal = "18";
  const subFeedsList =
    config.system_contracts.panel_members.panelMembership
      .priceOracleMaintainers;
  const roundTime = config.system_contracts.oracle.round_time;
  const minSubmissionsCount =
    config.system_contracts.oracle.min_submissions_count;
  const maxSubmissionsCount =
    config.system_contracts.oracle.max_submissions_count;

  // TODO: change baseTokenAddr
  await deployFxPriceFeedMedianizer(
    config,
    deployer,
    `Q_QUSD`,
    decimal,
    subFeedsList,
    config.system_contracts.epdr_path.governed.EPDR.QBTC_address.value,
    roundTime,
    minSubmissionsCount,
    maxSubmissionsCount,
    "_source",
    "epqfi"
  );

  [uintKeys, uintValues] = getUintKeysAndValues(
    config.system_contracts.epqfi_path.governed.EPQFI,
    "governed.EPQFI."
  );
  [addressKeys, addressValues] = getAddressKeysAndValues(
    config.system_contracts.epqfi_path.governed.EPQFI,
    "governed.EPQFI."
  );
  [stringKeys, stringValues] = getStringKeysAndValues(
    config.system_contracts.epqfi_path.governed.EPQFI,
    "governed.EPQFI."
  );
  [boolKeys, boolValues] = getBoolKeysAndValues(
    config.system_contracts.epqfi_path.governed.EPQFI,
    "governed.EPQFI."
  );
  await ePQFI_Parameters.initialize(
    registryAddress,
    uintKeys,
    uintValues,
    addressKeys,
    addressValues,
    stringKeys,
    stringValues,
    boolKeys,
    boolValues
  );

  const ePQFI_ParametersVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePQFI_ParametersVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPQFI_ParametersVotingProxy" }
  );
  const ePQFI_ParametersVoting = await deployer.deployed(
    EPQFI_ParametersVoting__factory,
    await ePQFI_ParametersVotingProxy.getAddress()
  );
  await ePQFI_ParametersVoting.initialize(registryAddress);

  const ePRS_MembershipProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePRS_MembershipImpl.getAddress(), registryAddress, "0x"],
    { name: "EPRS_MembershipProxy" }
  );
  const ePRS_Membership = await deployer.deployed(
    EPRS_Membership__factory,
    await ePRS_MembershipProxy.getAddress()
  );

  const eprs_experts =
    config.system_contracts.panel_members.panelMembership.eprs;
  await ePRS_Membership.initialize(registryAddress, eprs_experts);

  await ePRS_Membership.transferOwnership(registryAddress);

  const ePRS_MembershipVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePRS_MembershipVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPRS_MembershipVotingProxy" }
  );

  const ePRS_MembershipVoting = await deployer.deployed(
    EPRS_MembershipVoting__factory,
    await ePRS_MembershipVotingProxy.getAddress()
  );
  await ePRS_MembershipVoting.initialize(registryAddress);

  const ePRS_ParametersProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePRS_ParametersImpl.getAddress(), registryAddress, "0x"],
    { name: "EPRS_ParametersProxy" }
  );

  const ePRS_Parameters = await deployer.deployed(
    EPRS_Parameters__factory,
    await ePRS_ParametersProxy.getAddress()
  );

  [uintKeys, uintValues] = getUintKeysAndValues(
    config.system_contracts.eprs_path.governed.EPRS,
    "governed.EPRS."
  );
  [addressKeys, addressValues] = getAddressKeysAndValues(
    config.system_contracts.eprs_path.governed.EPRS,
    "governed.EPRS."
  );
  [stringKeys, stringValues] = getStringKeysAndValues(
    config.system_contracts.eprs_path.governed.EPRS,
    "governed.EPRS."
  );
  [boolKeys, boolValues] = getBoolKeysAndValues(
    config.system_contracts.eprs_path.governed.EPRS,
    "governed.EPRS."
  );
  await ePRS_Parameters.initialize(
    registryAddress,
    uintKeys,
    uintValues,
    addressKeys,
    addressValues,
    stringKeys,
    stringValues,
    boolKeys,
    boolValues
  );

  const ePRS_ParametersVotingProxy = await deployer.deploy(
    TransparentUpgradeableProxy__factory,
    [await ePRS_ParametersVotingImpl.getAddress(), registryAddress, "0x"],
    { name: "EPRS_ParametersVotingProxy" }
  );
  const ePRS_ParametersVoting = await deployer.deployed(
    EPRS_ParametersVoting__factory,
    await ePRS_ParametersVotingProxy.getAddress()
  );
  await ePRS_ParametersVoting.initialize(registryAddress);

  const epdrMembershipKey = "governance.experts.EPDR.membership";
  const epdrMembershipVotingKey = "governance.experts.EPDR.membershipVoting";
  const epdrParametersKey = "governance.experts.EPDR.parameters";
  const epdrParametersVotingKey = "governance.experts.EPDR.parametersVoting";
  const epqfiMembershipKey = "governance.experts.EPQFI.membership";
  const epqfiMembershipVotingKey = "governance.experts.EPQFI.membershipVoting";
  const epqfiParametersKey = "governance.experts.EPQFI.parameters";
  const epqfiParametersVotingKey = "governance.experts.EPQFI.parametersVoting";
  const eprsMembershipKey = "governance.experts.EPRS.membership";
  const eprsMembershipVotingKey = "governance.experts.EPRS.membershipVoting";
  const eprsParametersKey = "governance.experts.EPRS.parameters";
  const eprsParametersVotingKey = "governance.experts.EPRS.parametersVoting";

  const registry = await deployer.deployed(
    ContractRegistry__factory,
    registryAddress
  );

  await registry.setAddresses(
    [
      epdrMembershipKey,
      epdrMembershipVotingKey,
      epdrParametersKey,
      epdrParametersVotingKey,
      epqfiMembershipKey,
      epqfiMembershipVotingKey,
      epqfiParametersKey,
      epqfiParametersVotingKey,
      eprsMembershipKey,
      eprsMembershipVotingKey,
      eprsParametersKey,
      eprsParametersVotingKey,
    ],
    [
      await ePDR_Membership.getAddress(),
      await ePDR_MembershipVoting.getAddress(),
      await ePDR_Parameters.getAddress(),
      await ePDR_ParametersVoting.getAddress(),
      await ePQFI_Membership.getAddress(),
      await ePQFI_MembershipVoting.getAddress(),
      await ePQFI_Parameters.getAddress(),
      await ePQFI_ParametersVoting.getAddress(),
      await ePRS_Membership.getAddress(),
      await ePRS_MembershipVoting.getAddress(),
      await ePRS_Parameters.getAddress(),
      await ePRS_ParametersVoting.getAddress(),
    ]
  );

  return [
    ePDR_Membership,
    ePDR_MembershipVoting,
    ePDR_Parameters,
    ePDR_ParametersVoting,
    ePQFI_Membership,
    ePQFI_MembershipVoting,
    ePQFI_Parameters,
    ePQFI_ParametersVoting,
    ePRS_Membership,
    ePRS_MembershipVoting,
    ePRS_Parameters,
    ePRS_ParametersVoting,
  ];
};
