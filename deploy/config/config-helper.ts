import { web3 } from "hardhat";

import { getYamlConfig, saveYamlConfig } from "@/deploy/config/yaml-helper";

export const saveContractByKey = async (
  path: string,
  key: string,
  address: string,
  configPath = process.env.CONFIG_FILE_PATH
) => {
  const config = getYamlConfig(configPath);

  let currentObject = config;
  const pathSegments = path.split(".");

  for (let i = 0; i < pathSegments.length; i++) {
    const segment = pathSegments[i];

    if (!currentObject[segment]) {
      currentObject[segment] = {};
    }

    currentObject = currentObject[segment];
  }

  // Assign the address to the final key
  currentObject[key] = address;

  saveYamlConfig(config, configPath);
};

export const getContractAddressByKey = async (path: string, key: string) => {
  const config = getYamlConfig();

  const pathSegments = path.split(".");
  let currentObject = config;

  for (let i = 0; i < pathSegments.length; i++) {
    const segment = pathSegments[i];

    if (!currentObject[segment]) {
      throw new Error(`Config file under path ${path} does not exist`);
    }

    currentObject = currentObject[segment];
  }

  const address = currentObject[key];

  if (await isContractExist(address)) {
    return address;
  }

  return null;
};

export async function setKeyPrefix(key: string) {
  if (await isHomeChain()) {
    key = `home_${key}`;
  } else {
    key = `foreign_${key}`;
  }

  return key;
}

export const isHomeChain = async () => {
  const chainId = await getChainId();
  const nightlyChainId =
    process.env.NIGHTLY_CHAIN_ID !== undefined
      ? process.env.NIGHTLY_CHAIN_ID
      : -1;

  return (
    chainId === 35441 ||
    chainId === 35442 ||
    chainId === 35443 ||
    Number(nightlyChainId) === Number(chainId)
  );
};

export const getChainId = async () => {
  return web3.eth.getChainId();
};

export const isContractExist = async (address: string) => {
  return (await web3.eth.getCode(address)) !== "0x";
};
