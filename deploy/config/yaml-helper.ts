import fs from "fs";

// @ts-ignore
import * as yaml from "yaml";

export const getYamlConfig = (configPath = process.env.CONFIG_FILE_PATH) => {
  if (configPath === undefined) {
    throw new Error("CONFIG_FILE_PATH is not defined");
  }

  return yaml.parse(fs.readFileSync(configPath, "utf8"));
};

export const saveYamlConfig = (
  config: any,
  configPath = process.env.CONFIG_FILE_PATH
) => {
  if (configPath === undefined) {
    throw new Error("CONFIG_FILE_PATH is not defined");
  }

  const doc = yaml.parseDocument(fs.readFileSync(configPath, "utf8"));

  function applyChanges(yamlNode: any, jsonNode: any) {
    for (let key in jsonNode) {
      if (typeof jsonNode[key] === "object" && !Array.isArray(jsonNode[key])) {
        applyChanges(yamlNode.get(key), jsonNode[key]);
      } else {
        yamlNode.set(key, jsonNode[key]);
      }
    }
  }

  applyChanges(doc.contents, config);

  fs.writeFileSync(configPath, doc.toString());
};
