import fs from "fs";
import toml from "toml";
import path from "path";

// @ts-ignore
import yaml from "js-yaml";

import BigNumber from "bignumber.js";

export const getConfigJson = () => {
  const configPath = process.env.CONFIG_FILE_PATH;

  if (configPath === undefined) {
    throw new Error("CONFIG_FILE_PATH is not defined");
  }

  if (!fs.existsSync(configPath)) {
    throw new Error(`Config file under path ${configPath} does not exist`);
  }

  const rawConfig = loadYamlFile(configPath);

  const processConfig = (obj: any, basePath: string) => {
    for (const [key, value] of Object.entries(obj)) {
      if (
        typeof value === "object" &&
        value !== null &&
        !Array.isArray(value)
      ) {
        processConfig(value, basePath);
      } else {
        const filePath = path.join(basePath, String(value));

        if (!fs.existsSync(filePath) || fs.statSync(filePath).isDirectory()) {
          continue;
        }

        switch (getFileExtension(filePath)) {
          case "toml":
            obj[key] = loadTomlFile(filePath);
            break;
          case "yaml":
            obj[key] = loadYamlFile(filePath);
            break;
          case "json":
            obj[key] = JSON.parse(fs.readFileSync(filePath, "utf8"));
            break;
          default:
            throw new Error(
              `Config file under path ${filePath} is not a supported file type`
            );
        }
      }
    }
  };

  processConfig(rawConfig, path.dirname(configPath));

  return rawConfig;
};

const getFileExtension = (filePath: string) => {
  return path.extname(filePath).slice(1);
};

function loadYamlFile(yamlFilePath: string) {
  validateExtension(yamlFilePath, "yaml");

  const fileContents = fs.readFileSync(yamlFilePath, "utf8");

  return yaml.load(fileContents, {});
}

function loadTomlFile(tomlFilePath: string) {
  validateExtension(tomlFilePath, "toml");

  const fileContents = fs.readFileSync(tomlFilePath, "utf8");

  const parsedResult = toml.parse(fileContents);

  return flattenObject(parsedResult);
}

function validateExtension(filePath: string, extension: string) {
  const fileExtension = getFileExtension(filePath);

  if (fileExtension !== extension) {
    throw new Error(
      `Config file under path ${filePath} is not a ${extension} file`
    );
  }
}

function flattenObject(rawObject: any, parentKey = "", parentObject = {}) {
  Object.entries(rawObject).forEach(([key, value]) => {
    // Construct the new key
    let newKey = parentKey ? `${parentKey}.${key}` : key;

    if (typeof value === "object" && value !== null && !Array.isArray(value)) {
      // If it's a nested object, recursively flatten
      flattenObject(value, newKey, parentObject);
    } else {
      // Split the key by dots and create nested structure
      let keyParts = newKey.split(".");
      let lastKeyIndex = keyParts.length - 1;
      let currentObject: any = parentObject;

      // Navigate through/create nested objects
      keyParts.forEach((part, index) => {
        if (index === lastKeyIndex) {
          currentObject[part] = value; // Set the value at the deepest level
        } else {
          if (!currentObject[part]) currentObject[part] = {}; // Create nested object if it doesn't exist
          currentObject = currentObject[part]; // Navigate to the next level
        }
      });
    }
  });

  return parentObject;
}

export function validateHexString(hexStr: string) {
  // Add '0x' prefix if missing
  if (!hexStr.startsWith("0x")) {
    hexStr = "0x" + hexStr;
  }

  // Check if the string is a valid hexadecimal number
  if (!/^0x[0-9a-fA-F]+$/.test(hexStr)) {
    throw new Error("The string contains invalid characters.");
  }

  // Check if the string represents exactly 32 bytes
  if (hexStr.length !== 2 + 32 * 2) {
    // '0x' prefix + 32 bytes * 2 hex digits per byte
    throw new Error("The string does not represent exactly 32 bytes.");
  }

  return hexStr;
}

// Helper function to flatten and filter the object recursively
const flattenAndFilter = (
  obj: any,
  types: any,
  transformFn: any = null,
  parentKey = ""
) => {
  let result = {};

  for (const [key, value] of Object.entries(obj)) {
    const currentKey = parentKey ? `${parentKey}.${key}` : key;

    if (
      typeof value === "object" &&
      value !== null &&
      !Array.isArray(value) &&
      !(value as any).type
    ) {
      // Recursively process nested objects
      Object.assign(
        result,
        flattenAndFilter(value, types, transformFn, currentKey)
      );
    } else if ((value as any).type && types.includes((value as any).type)) {
      // Process and add values based on type
      const processedValue = transformFn
        ? (transformFn as any)((value as any).type, (value as any).value)
        : (value as any).value;
      (result as any)[currentKey] = {
        ...(value as any),
        value: processedValue,
      };
    }
  }

  return result;
};

// Example usage for uint and ufraction types
export const getUintKeysAndValues = (flattened: string, prefix = "") => {
  const filteredValues = flattenAndFilter(
    flattened,
    ["uint", "ufraction"],
    (type: string, value: any) => {
      return type === "ufraction" ? handleUFraction(value) : value;
    }
  );

  const keys = Object.keys(filteredValues).map((key) => prefix + key);
  const values = Object.values(filteredValues).map((entry: any) => entry.value);

  return [keys, values];
};

function handleUFraction(valueRaw: string) {
  return BigNumber(valueRaw).times(BigNumber(10).exponentiatedBy(27)).toFixed();
}

function isDecimal(value: number): boolean {
  return value % 1 !== 0;
}

export const getAddressKeysAndValues = (rawObject: any, prefix = "") => {
  const filteredValues = flattenAndFilter(rawObject, ["address"]);

  const keys = Object.keys(filteredValues).map((key) => prefix + key);
  const values = Object.values(filteredValues).map((entry: any) => entry.value);

  return [keys, values];
};

export const getStringKeysAndValues = (rawObject: any, prefix = "") => {
  const filteredValues = flattenAndFilter(rawObject, ["string"]);

  const keys = Object.keys(filteredValues).map((key) => prefix + key);
  const values = Object.values(filteredValues).map((entry: any) => entry.value);

  return [keys, values];
};

export const getBoolKeysAndValues = (rawObject: any, prefix = "") => {
  const filteredValues = flattenAndFilter(rawObject, ["bool"]);

  const keys = Object.keys(filteredValues).map((key) => prefix + key);
  const values = Object.values(filteredValues).map((entry: any) => entry.value);

  return [keys, values];
};
