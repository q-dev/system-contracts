import { Deployer, Reporter } from "@solarity/hardhat-migrate";

import {
  getConfigJson,
  getUintKeysAndValues,
} from "@/deploy/config/config-parser";
import { isolatedMigration } from "@/deploy/scripts/isolated-migration";
import { deployRegistry } from "@/deploy/scripts/registry-migration";
import { deployCommon } from "@/deploy/scripts/common-migration";
import { deployTokeneconomics } from "@/deploy/scripts/tokeneconomics-migration";
import { deployDeFi } from "@/deploy/scripts/defi-migration";
import { deployGovernance } from "@/deploy/scripts/governance-migration";
import { deployExperts } from "@/deploy/scripts/experts-migration";
import { deployLast } from "@/deploy/scripts/deployLast.migration";

export = async (deployer: Deployer) => {
  const config = getConfigJson();

  // console.log(JSON.stringify(config, null, 2));

  if (config.system_contracts.isolated_migration) {
    await isolatedMigration(config, deployer);
  }

  const registry = await deployRegistry(config, deployer);
  const registryAddress = (registry as any).address;

  const [
    defaultAllocationProxy,
    crKeeperFactory,
    addressStorageFactory,
    crKeeperImpl,
    addressStorageImpl,
  ] = await deployCommon(registryAddress, deployer);

  const [
    validationRewardProxy,
    rootNodeRewardProxy,
    qHolderRewardProxy,
    qHolderRewardPool,
    validationRewardPools,
    qVault,
    vesting,
    pushPayments,
  ] = await deployTokeneconomics(config, registryAddress, deployer);

  await deployDeFi(config, registryAddress, deployer);

  const [
    constitution,
    constitutionVoting,
    rootNodes,
    rootsVoting,
    rootNodesSlashingVoting,
    rootNodeSlashingEscrow,
    validators,
    validatorsSlashingVoting,
    validatorSlashingEscrow,
    emergencyUpdateVoting,
    generalUpdateVoting,
    votingWeightProxy,
    contractRegistryUpgradeVoting,
    contractRegistryAddressVoting,
    accountAliases,
    withdrawAddresses,
  ] = await deployGovernance(config, registryAddress, deployer);

  const [
    ePDRMembership,
    ePDRMembershipVoting,
    ePDRParameters,
    ePDRParametersVoting,
    ePQFIMembership,
    ePQFIMembershipVoting,
    ePQFIParameters,
    ePQFIParametersVoting,
    ePRSMembership,
    ePRSMembershipVoting,
    ePRSParameters,
    ePRSParametersVoting,
  ] = await deployExperts(config, registryAddress, deployer);

  const [systemReserve] = await deployLast(registryAddress, deployer);

  if (config.system_contracts.registry_updates_only_voting) {
    await registry.leaveMaintainers();
  }

  Reporter.reportContracts(
    ["Contract Registry Address", registryAddress],
    ["DefaultAllocationProxy", await defaultAllocationProxy.getAddress()],
    ["CompoundRateKeeperFactory", await crKeeperFactory.getAddress()],
    ["AddressStorageFactory", await addressStorageFactory.getAddress()],
    ["CompoundRateKeeper Implementation", await crKeeperImpl.getAddress()],
    ["AddressStorage Implementation", await addressStorageImpl.getAddress()],
    ["ValidationRewardProxy", await validationRewardProxy.getAddress()],
    ["RootNodeRewardProxy", await rootNodeRewardProxy.getAddress()],
    ["QHolderRewardProxy", await qHolderRewardProxy.getAddress()],
    ["QHolderRewardPool", await qHolderRewardPool.getAddress()],
    ["ValidationRewardPools", await validationRewardPools.getAddress()],
    ["QVault", await qVault.getAddress()],
    ["Vesting", await vesting.getAddress()],
    ["PushPayments", await pushPayments.getAddress()],
    ["Constitution", await constitution.getAddress()],
    ["ConstitutionVoting", await constitutionVoting.getAddress()],
    ["RootNodes", await rootNodes.getAddress()],
    ["RootsVoting", await rootsVoting.getAddress()],
    ["RootNodesSlashingVoting", await rootNodesSlashingVoting.getAddress()],
    ["RootNodeSlashingEscrow", await rootNodeSlashingEscrow.getAddress()],
    ["Validators", await validators.getAddress()],
    ["ValidatorsSlashingVoting", await validatorsSlashingVoting.getAddress()],
    ["ValidatorSlashingEscrow", await validatorSlashingEscrow.getAddress()],
    ["EmergencyUpdateVoting", await emergencyUpdateVoting.getAddress()],
    ["GeneralUpdateVoting", await generalUpdateVoting.getAddress()],
    ["VotingWeightProxy", await votingWeightProxy.getAddress()],
    [
      "ContractRegistryUpgradeVoting",
      await contractRegistryUpgradeVoting.getAddress(),
    ],
    [
      "ContractRegistryAddressVoting",
      await contractRegistryAddressVoting.getAddress(),
    ],
    ["AccountAliases", await accountAliases.getAddress()],
    ["WithdrawAddresses", await withdrawAddresses.getAddress()],
    ["EPDR_Membership", await ePDRMembership.getAddress()],
    ["EPDR_MembershipVoting", await ePDRMembershipVoting.getAddress()],
    ["EPDR_Parameters", await ePDRParameters.getAddress()],
    ["EPDR_ParametersVoting", await ePDRParametersVoting.getAddress()],
    ["EPQFI_Membership", await ePQFIMembership.getAddress()],
    ["EPQFI_MembershipVoting", await ePQFIMembershipVoting.getAddress()],
    ["EPQFI_Parameters", await ePQFIParameters.getAddress()],
    ["EPQFI_ParametersVoting", await ePQFIParametersVoting.getAddress()],
    ["EPRS_Membership", await ePRSMembership.getAddress()],
    ["EPRS_MembershipVoting", await ePRSMembershipVoting.getAddress()],
    ["EPRS_Parameters", await ePRSParameters.getAddress()],
    ["EPRS_ParametersVoting", await ePRSParametersVoting.getAddress()],
    ["SystemReserve", await systemReserve.getAddress()]
  );
};
