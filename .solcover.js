module.exports = {
  skipFiles: [
    'mocks/',
    'interfaces/',
    'defi/oracles/IFxPriceFeed.sol',
    'defi/oracles/IFxPriceFeedSM.sol',
    'defi/token/IERC677.sol',
    'defi/IBridgeValidators.sol',
    'governance/IPanel.sol',
    'governance/IParameters.sol',
    'governance/IVoting.sol',
    'IUpgradable.sol',
    'Helper.sol',
    'governance/validators/ValidatorsMock.sol',
    'tokeneconomics/IQHolderRewardPool.sol',
    'tokeneconomics/ITokenLock.sol',
  ],
  configureYulOptimizer: true,
};
