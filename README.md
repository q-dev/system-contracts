# System Contracts

This repo is used to store solidity smart contracts. Also here must be unit tests for smart contracts.

## Installing 

```sh
$ npm i @q-dev/contracts
```

## Usage

```solidity
pragma solidity 0.8.9;

import "@q-dev/contracts/ContractRegistry.sol";

contract Example{
    address reg = 0xc3E589056Ece16BCB88c6f9318e9a7343b663522;
    function getQUSD() public view returns (address _addr){
        _addr = ContractRegistry(reg).getAddress("defi.QUSD.coin");
    }
}
```

## Generating go code
1. Compile `abigen` if you don't have it yet:

    1.1 Pull recent `q-client`

    1.2. `$ make all`

    1.3. put `abigen` from `build/bin/abigen` of `q-client` to `./bin` of this repo.

2. Generate code

You could generate code with the following command:
```console
$ ./generate.sh
```

**Notes:**
* Check that generated code have imports like `gitlab.com/q-dev/q-client`, not `github.com/ethereum/go-ethereum`

## Extract bytecode from artifacts or networks

**You could use `help` command to see available options for scripts**
```console
$ ./scripts/bytecode.sh --help 
```
Or for script to compare two networks: 
```console
$ ./scripts/compare-bytecode.sh --help
```

To extract bytecode from artifacts you could use following command:
```console
$ ./scripts/bytecode.sh
```

To extract bytecode from all networks:
```console
$ ./scripts/bytecode.sh -c -n
```

To compare bytecode between two networks:
```console
$ ./scripts/compare-bytecode.sh -f mainnet -s testnet 
```

### Options description

#### Common options for compare-bytecode.sh and bytecode.sh
- `-h, --help`: Show help
- `-r, --raw-bytecode`: If this flag is specified, the bytecode will be extracted and saved in raw format (by default in hash format)
- `-m, --compare-with-metadata`: If the flag is specified, the bytecode metadata will be taken into account in the comparison
- `-n, --save-network-bytecodes`: If the flag is specified, the bytecode from the network will be saved in a separate file

#### Unique option for bytecode.sh
- `-c, --compare-networks`: If specified, the bytecode will be compared between all networks

#### Unique options for compare-bytecode.sh
- `-f, --first network`: Name of the network for compare
- `-s, --second network`: Name of the network to compare with

## ABI comparing tool

The `compare-abi.sh` tool will help you compare ABIs across networks, snapshots or contracts.

**You can use the `help` command to see the available options for scripts**.
```console
$ ./scripts/compare-abi.sh --help
```

From there, you will know the available options for ABI comparison.

## Contribute

Code refactoring is currently underway. Code Style agreements are intended to make the code more readable and cleaner.

### Code Style Agreements

1. `internal` or `private` functions are named **with** an underscore before the name.

```solidity
    contract ContractExample {
        function _calcI() internal {}
        function _calcP() private {}
    }
    
```

2. `internal` functions in "internal libs" (without `external` functions) are named **without** an underscore before the name.

```solidity
    library LibExample {
        function calcI() internal {}
        function _calcP() private {}
    }
```

3. `internal` or `private` `storage` variables are named **with** an underscore before the name.

```solidity
    uint256 internal _varA;
    uint256 private _varB;
```

4. Local variables (`memory`, `calldata`, etc.), including function arguments, are named **with** an underscore at the end of the name.

```solidity
    function _calc1(uint256 a_) private pure returns (uint256 c_) {
        uint256 d_ = 5;
        c_ = a_ + d_;
    }

    function _calc2(uint256[] memory arr1_, uint256[] calldata arr2_) private pure returns (uint256 c_) {
        c_ = arr1_[0] + arr2_[0];
    }
```

5. If you want to declare a local `storage` variable, the name of such a variable will have the **same** format as the
   referenced `storage` variable.

```solidity
    contract ContractExample {
        struct Pair {
            uint256 numA;
            uint256 numB;
        }

        Pair[] public pairs;
        Pair[] internal _pairs;
    
        function someNameA() external view returns (uint256) {
            Pair storage pair = pairs[0];
    
            return pair.numA;
        }
    
        function someNameB() external view returns (uint256) {
            Pair storage _pair = _pairs[0];
    
            return _pair.numB;
        }
    }
```

6. `event` parameters are named **without** lower handwriting. The name of the event must refer to an action that has already happened.

```solidity
    event Deposited(address to, uint256 value);
```

7. `const` and `immutable` are named **with** SCREAMING_SNAKE_CASE.

8. Comments in the `contract` can omitted.

9. Write full function comments in NatSpec format in the contract `interface` via multi-line comments.

10. The declaration of `events` and `structs` should be in the contract interface.

11. If there is no `interface`, everything is written in the `contract`.


### Deploying

If the `verify` flag is set, the automatic verification will be enabled.

You can set your own migrations and deploy them to the network you want.

The plugin used for deployment is [@dlsl/hardhat-migrate](https://github.com/distributedlab-solidity-library/hardhat-migrate),
so for more detailed examples of contract deployments, please see the documentation listed in that plugin.

An example of how you can write a migration can be found in the `deploy` folder.

To start the migration, you must first specify an environment variable, such as the one shown below, 
or any other way that you prefer:
```console
$ export PRIVATE_KEY='1a2b....4c5d'
```

Then you can start the migration, and you can specify your own parameters if necessary, other parameters 
will be obtained from the `migrate` section in the `hardhat.config.ts` file:
```console
$ npx hardhat migrate --network devnet --only 2
```

Please see here [Hardhat migrate docs](https://github.com/distributedlab-solidity-library/hardhat-migrate/blob/master/README.md),
for more examples of commands to run migrations.

The migration parameters listed in `hardhat.config.ts` under the `migrate` section.

Also check out this section on how you should name your migrations: [Naming convention](https://github.com/distributedlab-solidity-library/hardhat-migrate/blob/master/README.md#naming-convention)

### Verifying

#### You could manually verify contract
```console
$ npx hardhat verify --network devnet DEPLOYED_CONTRACT_ADDRESS "Constructor argument 1"
```

Other examples of manual contract verification can be found here [@nomiclabs/hardhat-etherscan](https://www.npmjs.com/package/@nomiclabs/hardhat-etherscan)


### pre-commit hook activating
```console
$ npx husky install
```

### prettier
To check and fix codestyle bugs you should run prettier
```console
$ npm run prettier
```

### bytecode size

You may check bytecode size of each contract via
```console
$ npm run contract-size
```

## Tests

You can run tests via
```console
$ npm run test
```

## Documentation

There you may find usage examples and descriptions of all methods:

https://q-dev.gitlab.io/system-contracts/

## License
LGPL 3.0: https://gitlab.com/q-dev/system-contracts/-/blob/master/LICENSE
