import fs from "fs";
import web3 from "web3";

export function cutBytecodeMetaData(bytecode: string) {
  return bytecode.slice(0, -64 - 22).concat(bytecode.slice(-22, 0));
}

export function getBytecodeHash(proxyCode: string, implCode: string) {
  const proxyBytecode = web3.utils.keccak256(cutBytecodeMetaData(proxyCode));
  const proxyBytecodeRaw = web3.utils.keccak256(proxyCode);

  const implBytecode = web3.utils.keccak256(cutBytecodeMetaData(implCode));
  const implBytecodeRaw = web3.utils.keccak256(implCode);
  return [proxyBytecode, proxyBytecodeRaw, implBytecode, implBytecodeRaw];
}

export function getBytecodeRaw(proxyCode: string, implCode: string) {
  const proxyBytecode = cutBytecodeMetaData(proxyCode);
  const implBytecode = cutBytecodeMetaData(implCode);
  return [proxyBytecode, proxyCode, implBytecode, implCode];
}

export function writeToFile(
  dataToWrite: any,
  dest = "",
  fileName = "",
  extension = "",
  isRaw = false,
  rpc = ""
) {
  const raw = isRaw ? ".raw" : ".hash";
  const fileName_ = fileName === "" ? rpc.split("/")[2] + raw : fileName + raw;
  const data_ =
    typeof dataToWrite === "string" ? dataToWrite : convertToCSV(dataToWrite);
  fs.writeFile(dest + fileName_ + extension, data_, function (err) {
    if (err) throw err;
    console.log(`Saved: ${fileName_}!`);
  });
}

export function convertToCSV(arr: any) {
  const array = [Object.keys(arr[0])].concat(arr);

  return array
    .map((it) => {
      return Object.values(it).join(",");
    })
    .join("\n");
}

export const getContractName = (key: string): string => {
  const result = key.split(".").at(-1);

  if (result === undefined) {
    throw new Error("Invalid contract name. Method getContractName");
  }

  return result;
};
