#!/bin/bash

# this is a simple script for code generation
# extend it when needed

# IMPORTANT!
# chances are your abigen doesn't support "truffle-json" option,
# in this case pull recent q-client and recompile it from there.

# Make sure you have abigen in tmp (to install q-client):
# git clone git@gitlab.com:q-dev/q-client.git
# cd q-client
# env GOBIN=/tmp go install ./cmd/abigen
# cd ..
# See instructions in the end of the file

# Uncomment this if you need to compile contracts locally
# compile everything first
# have to use hardhat because of openzeppelin contracts
# npm run compile
#
# mkdir -p build/contracts
# shopt -s globstar
# cp artifacts/@dlsl/**/*.json build/contracts/
# cp artifacts/@opengsn/**/*.json build/contracts/
# cp artifacts/@openzeppelin/**/*.json build/contracts/
# cp artifacts/contracts/**/*.json build/contracts/
# rm -rf  build/contracts/**.dbg.json

EXCLUDE_BASE=IVotingBaseProposal,IVotingVotingCounters,IVotingVotingParams,IVotingVotingStats,IQthVotingVotingWeightInfo
EXCLUDE=${EXCLUDE_BASE},MembershipProposalDetails,ExpertsParametersVotingParameterValue,ExpertsParametersVotingParam,VotingLockInfo,ASlashingEscrowArbitrationParams,ASlashingEscrowDecision,ASlashingEscrowDecisionStats,BaseVotingWeightInfo,ATimeLockBase,TimeLockEntry,IParametersVotingParameterInfo,ARootNodeApprovalVoting,ARootNodeApprovalVotingProposalStats
echo ${EXCLUDE}

rm -rf generated
mkdir -p generated

# registry
/tmp/abigen --pkg generated --truffle-json build/contracts/ContractRegistry.json --type ContractRegistry --out generated/contract_registry.go

# governance
/tmp/abigen --pkg generated --truffle-json build/contracts/GeneralUpdateVoting.json --exc ${EXCLUDE} --type GeneralUpdateVoting --out generated/general_update_voting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EmergencyUpdateVoting.json --exc ${EXCLUDE} --type EmergencyUpdateVoting --out generated/emergency_update_voting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/VotingWeightProxy.json --type VotingWeightProxy --out generated/voting_weight_proxy.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ContractRegistryUpgradeVoting.json --type ContractRegistryUpgradeVoting --out generated/contract_registry_upgradeVoting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ContractRegistryAddressVoting.json --exc ${EXCLUDE} --type ContractRegistryAddressVoting --out generated/contract_address_upgradeVoting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/AccountAliases.json --exc ${EXCLUDE} --type AccountAliases --out generated/account_aliases.go

# constitution
/tmp/abigen --pkg generated --truffle-json build/contracts/Constitution.json --type Constitution --out generated/constitution.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ConstitutionVoting.json --exc ${EXCLUDE} --type ConstitutionVoting --out generated/constitution_voting.go

# storage
/tmp/abigen --pkg generated --truffle-json build/contracts/AddressStorageStakes.json --type AddressStorageStakes --out generated/address_storage_stakes.go
/tmp/abigen --pkg generated --truffle-json build/contracts/AddressStorageStakesSorted.json --type AddressStorageStakesSorted --out generated/address_storage_stakes_sorted.go

# roots
/tmp/abigen --pkg generated --truffle-json build/contracts/Roots.json --exc ${EXCLUDE} --type Roots --out generated/roots.go
/tmp/abigen --pkg generated --truffle-json build/contracts/RootsVoting.json --exc ${EXCLUDE} --type RootsVoting --out generated/roots_voting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/RootNodesSlashingVoting.json --exc ${EXCLUDE} --type RootNodesSlashingVoting --out generated/root_nodes_slashing_voting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/RootNodeSlashingEscrow.json --exc ${EXCLUDE} --type RootNodeSlashingEscrow --out generated/root_slashing_escrow.go

# validators
/tmp/abigen --pkg generated --truffle-json build/contracts/Validators.json --exc ${EXCLUDE} --type Validators --out generated/validators.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ValidatorsSlashingVoting.json --exc ${EXCLUDE} --type ValidatorsSlashingVoting --out generated/validators_slashing_voting.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ValidatorSlashingEscrow.json --exc ${EXCLUDE_BASE} --type ValidatorSlashingEscrow --out generated/validator_slashing_escrow.go

# epdr
/tmp/abigen --pkg generated --truffle-json build/contracts/EPDR_Membership.json --type EPDR_Membership --out generated/epdr_membership.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPDR_MembershipVoting.json  --exc BaseVotingWeightInfo --type EPDR_MembershipVoting --out generated/epdr_membership_voting.go

/tmp/abigen --pkg generated --truffle-json build/contracts/EPDR_Parameters.json --type EPDR_Parameters --out generated/epdr_parameters.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPDR_ParametersVoting.json --exc ${EXCLUDE_BASE} --type EPDR_ParametersVoting --out generated/epdr_parameters_voting.go

# eprs
/tmp/abigen --pkg generated --truffle-json build/contracts/EPRS_Membership.json --type EPRS_Membership --out generated/eprs_membership.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPRS_MembershipVoting.json  --exc ${EXCLUDE} --type EPRS_MembershipVoting --out generated/eprs_membership_voting.go

/tmp/abigen --pkg generated --truffle-json build/contracts/EPRS_Parameters.json --type EPRS_Parameters --out generated/eprs_parameters.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPRS_ParametersVoting.json --exc ${EXCLUDE} --type EPRS_ParametersVoting --out generated/eprs_parameters_voting.go

# epqfi
/tmp/abigen --pkg generated --truffle-json build/contracts/EPQFI_Membership.json --type EPQFI_Membership --out generated/epqfi_membership.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPQFI_MembershipVoting.json --exc ${EXCLUDE} --type EPQFI_MembershipVoting --out generated/epqfi_membership_voting.go

/tmp/abigen --pkg generated --truffle-json build/contracts/EPQFI_Parameters.json --type EPQFI_Parameters --out generated/epqfi_parameters.go
/tmp/abigen --pkg generated --truffle-json build/contracts/EPQFI_ParametersVoting.json --exc ${EXCLUDE} --type EPQFI_ParametersVoting --out generated/epqfi_parameters_voting.go

# tokenomics
/tmp/abigen --pkg generated --truffle-json build/contracts/DefaultAllocationProxy.json --type DefaultAllocationProxy --out generated/default_allocation_proxy.go
/tmp/abigen --pkg generated --truffle-json build/contracts/QVault.json --exc ${EXCLUDE} --type QVault --out generated/q_vault.go
/tmp/abigen --pkg generated --truffle-json build/contracts/SystemReserve.json --type SystemReserve --out generated/system_reserve.go

/tmp/abigen --pkg generated --truffle-json build/contracts/RootNodeRewardProxy.json --type RootNodeRewardProxy --out generated/pay_root_nodes.go

/tmp/abigen --pkg generated --truffle-json build/contracts/ValidationRewardProxy.json --type ValidationRewardProxy --out generated/validation_reward_proxy.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ValidationRewardPools.json --type ValidationRewardPools --out generated/validation_reward_pools.go

/tmp/abigen --pkg generated --truffle-json build/contracts/QHolderRewardPool.json --type QHolderRewardPool --out generated/qholder_reward_pool.go
/tmp/abigen --pkg generated --truffle-json build/contracts/QHolderRewardProxy.json --type QHolderRewardProxy --out generated/qholder_reward_proxy.go

/tmp/abigen --pkg generated --truffle-json build/contracts/PushPayments.json --type PushPayments --out generated/push_payments.go
/tmp/abigen --pkg generated --truffle-json build/contracts/Vesting.json --exc VotingLockInfo --type Vesting --out generated/vesting.go

# DEFI
/tmp/abigen --pkg generated --truffle-json build/contracts/ERC677Mock.json --type ERC677Mock --out generated/erc677mock.go
/tmp/abigen --pkg generated --truffle-json build/contracts/StableCoin.json --type StableCoin --out generated/stable_coin.go
/tmp/abigen --pkg generated --truffle-json build/contracts/FxPriceFeed.json --type FxPriceFeed --out generated/fx_price_feed.go
/tmp/abigen --pkg generated --truffle-json build/contracts/BorrowingCore.json --type BorrowingCore --out generated/borrowing_core.go
/tmp/abigen --pkg generated --truffle-json build/contracts/SystemBalance.json --type SystemBalance --out generated/system_balance.go
/tmp/abigen --pkg generated --truffle-json build/contracts/Saving.json --type Saving --out generated/saving.go
/tmp/abigen --pkg generated --truffle-json build/contracts/LiquidationAuction.json --type LiquidationAuction --out generated/liquidation_auction.go
/tmp/abigen --pkg generated --truffle-json build/contracts/SystemDebtAuction.json --type SystemDebtAuction --out generated/system_debt_auction.go
/tmp/abigen --pkg generated --truffle-json build/contracts/SystemSurplusAuction.json --type SystemSurplusAuction --out generated/system_surplus_auction.go
/tmp/abigen --pkg generated --truffle-json build/contracts/TokenBridgeAdminProxy.json --type TokenBridgeAdminProxy --out generated/token_bridge_admin_proxy.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ForeignChainTokenBridgeAdminProxy.json --type ForeignChainTokenBridgeAdminProxy --out generated/foreign_chain_token_bridge_admin_proxy.go

# common contracts
/tmp/abigen --pkg generated --truffle-json build/contracts/ExceptionStorage.json --type ExceptionStorage --out generated/exceptionStorage.go
/tmp/abigen --pkg generated --truffle-json build/contracts/CompoundRateKeeper.json --type CompoundRateKeeper --out generated/cr_keeper.go
/tmp/abigen --pkg generated --truffle-json build/contracts/CompoundRateKeeperFactory.json --type CompoundRateKeeperFactory --out generated/cr_keeper_factory.go
/tmp/abigen --pkg generated --truffle-json build/contracts/AddressStorage.json --type AddressStorage --out generated/address_storage.go
/tmp/abigen --pkg generated --truffle-json build/contracts/AddressStorageFactory.json --type AddressStorageFactory --out generated/address_storage_factory.go
/tmp/abigen --pkg generated --truffle-json build/contracts/Ownable.json --type Ownable --out generated/ownable.go

# upgradability
/tmp/abigen --pkg generated --truffle-json build/contracts/TransparentUpgradeableProxy.json --type TransparentUpgradeableProxy --out generated/transparent_upgradeable_proxy.go
/tmp/abigen --pkg generated --truffle-json build/contracts/ProxyAdmin.json --type ProxyAdmin --out generated/proxy_admin.go

# Uncomment this if you cloned the repo before
# rm -rf ./q-client
