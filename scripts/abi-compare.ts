import fs from "fs";

import {
  deepCompareMappedABIs,
  deepEqualsAbi,
  getNormalizedSnapshotFromNetwork,
  getNormalizedSnapshotFromProject,
  saveSnapshot,
} from "./abi-utils";

async function compareBetweenNetworks(first: any, second: any) {
  const firstSnp: any = await getNormalizedSnapshotFromNetwork(first);
  const secondSnp: any = await getNormalizedSnapshotFromNetwork(second);
  deepEqualsAbi(firstSnp, secondSnp);
  if (saveABI) {
    saveSnapshot("./", `${first.split("/")[2]}`, firstSnp).catch(console.log);
    saveSnapshot("./", `${second.split("/")[2]}`, secondSnp).catch(console.log);
  }
}

async function compareBetweenNetworkAndSnapshot(network: any, snapshot: any) {
  const firstSnp: any = await getNormalizedSnapshotFromNetwork(network);
  const secondSnp = require(fs.realpathSync(snapshot));
  deepEqualsAbi(firstSnp, secondSnp);
  if (saveABI) {
    saveSnapshot("./", `${network.split("/")[2]}`, firstSnp).catch(console.log);
  }
}

async function compareBetweenNetworkAndProject(network: any) {
  const firstSnp: any = await getNormalizedSnapshotFromNetwork(network);
  const secondSnp: any = await getNormalizedSnapshotFromProject();
  deepEqualsAbi(firstSnp, secondSnp);
  if (saveABI) {
    saveSnapshot("./", `${network.split("/")[2]}`, firstSnp).catch(console.log);
    saveSnapshot("./", `current`, secondSnp).catch(console.log);
  }
}

function compareBetweenTwoSnapshots(first: any, second: any) {
  const firstSnp = require(fs.realpathSync(first));
  const secondSnp = require(fs.realpathSync(second));
  deepEqualsAbi(firstSnp, secondSnp);
}

async function compareBetweenProjectAndSnapshot(snapshot: any) {
  const firstSnp = await getNormalizedSnapshotFromProject();
  const secondSnp = require(fs.realpathSync(snapshot));
  deepEqualsAbi(firstSnp, secondSnp);
}

function compareTwoContracts(first: any, second: any) {
  let contractA;
  try {
    contractA = require(first);
  } catch (e) {
    throw new Error(`Couldn't find contract at path ` + first);
  }

  let contractB;
  try {
    contractB = require(second);
  } catch (e) {
    throw new Error(`Couldn't find contract at path ` + second);
  }

  if (!contractA.abi || !contractB.abi) throw new Error("not an ABI file");

  console.log(
    deepCompareMappedABIs(contractA.abi, contractB.abi)
      ? "✔️ ABIs are identical"
      : "❌ ABIs are not identical"
  );
}

const args = process.argv.slice(4);
const saveABI = process.argv[2] === "true";
const fileNameToSave = process.argv[3];

switch (args[0]) {
  case "net-net":
    compareBetweenNetworks(args[1], args[2]).catch(console.log);
    break;
  case "net-snp":
    compareBetweenNetworkAndSnapshot(args[1], args[3]).catch(console.log);
    break;
  case "net-prj":
    compareBetweenNetworkAndProject(args[1]).catch(console.log);
    break;
  case "snp-snp":
    compareBetweenTwoSnapshots(args[3], args[4]);
    break;
  case "snp-prj":
    compareBetweenProjectAndSnapshot(args[3]).catch(console.log);
    break;
  case "cnt-cnt":
    compareTwoContracts(args[3], args[4]);
    break;
  default:
    if (saveABI) {
      getNormalizedSnapshotFromProject()
        .catch(console.log)
        .then((snp: any) => {
          const filename =
            fileNameToSave === "---" ? "abi-snapshot" : fileNameToSave;
          saveSnapshot("./", filename, snp).catch(console.log);
        });
    }
}
