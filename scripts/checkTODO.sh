#! /bin/bash

RED='\033[1;31m'
YELLOW='\033[1;33m'
NC='\033[0m'

result=$(grep -iRl 'todo' 'contracts/')
status=$?

if [ $status -eq 1 ]
then
    echo TODO test passed!
else
    printf "\t${RED}This contracts contain TODO tasks\n"
    printf "\tsolve it now or create issue in Jira${NC}\n"
    grep -C5 -iR todo contracts
    exit 1
fi

result=$(grep -iRl 'todo' 'test/')
status=$?

if [ ! $status -eq 1 ]
then
    printf "\t ${YELLOW}Tests' TODO: \n"
    while read p; do
        printf " >%s\n" "$p"
    done <<< "$result"
fi
