import Eth from "web3-eth";

import {
  CONTRACT_REGISTRY_ADDRESS,
  IMPL_STORAGE_SLOT,
  whitelist,
  ZERO_ADDRESS,
} from "./constants";

import {
  getBytecodeHash,
  getBytecodeRaw,
  getContractName,
  writeToFile,
} from "./extract-bytecode-utils";

export async function parseBytecodeToCSV(rpcArr: string[], isRaw: boolean) {
  for (const arg of rpcArr) {
    if (whitelist.includes(arg)) {
      const resArr = await getBytecodeFromRPC(arg, isRaw);
      writeToFile(resArr, "./bytecodes/", "", ".csv", isRaw, arg);
    } else {
      console.log("Invalid rpc endpoint!");
    }
  }
}

export async function getBytecodeFromRPC(rpcEndpoint: string, isRaw = false) {
  const Contract = require("web3-eth-contract");
  Contract.setProvider(rpcEndpoint);
  const web3Eth = new Eth(rpcEndpoint);

  const Registry = require("../artifacts/contracts/ContractRegistry.sol/ContractRegistry.json");

  const contract = new Contract(Registry.abi, CONTRACT_REGISTRY_ADDRESS);
  const contractsMap = await contract.methods.getContracts().call();
  const resArr = await getBytecodes(web3Eth, contractsMap, isRaw);

  return resArr
    .filter((item) => {
      return !(item.bytecode === null || item.bytecode === "");
    })
    .sort((a, b) => {
      return getContractName(a.name).localeCompare(getContractName(b.name));
    });
}

async function getBytecodes(web3Eth: Eth, contractsMap: any, isRaw: boolean) {
  const result = [];

  for (const [key, proxyAddress] of contractsMap) {
    const codes = await extractBytecode(web3Eth, proxyAddress);
    const resProxyObj = {
      name: key,
      bytecode: "",
      bytecodeRaw: "",
    };
    const resImplObj = {
      name: key + "-impl",
      bytecode: "",
      bytecodeRaw: "",
    };

    let bytecode;
    if (isRaw) {
      bytecode = getBytecodeRaw(codes[0], codes[1]);
    } else {
      bytecode = getBytecodeHash(codes[0], codes[1]);
    }

    resProxyObj.bytecode = bytecode[0];
    resProxyObj.bytecodeRaw = bytecode[1];
    resImplObj.bytecode = bytecode[2];
    resImplObj.bytecodeRaw = bytecode[3];

    result.push(resProxyObj, resImplObj);
  }

  return result;
}

async function extractBytecode(web3Eth: Eth, proxyAddress: string) {
  const proxyCode = await web3Eth.getCode(proxyAddress);
  const implAddress = await web3Eth.getStorageAt(
    proxyAddress,
    IMPL_STORAGE_SLOT
  );

  if (implAddress === ZERO_ADDRESS) {
    return [proxyCode, ""];
  }
  const implCode = await web3Eth.getCode("0x" + implAddress.slice(-40));
  return [proxyCode, implCode];
}
