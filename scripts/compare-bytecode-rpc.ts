import { whitelist } from "./constants";
import { getContractName, writeToFile } from "./extract-bytecode-utils";
import {
  getBytecodeFromRPC,
  parseBytecodeToCSV,
} from "./extract-bytecodes-rpc";

async function compareBytecodeOfTwoNetworks(
  rpc1: string,
  rpc2: string,
  includeMeta = false
) {
  const arr1 = await getBytecodeFromRPC(rpc1);
  const arr2 = await getBytecodeFromRPC(rpc2);
  let counter1 = 0;
  let counter2 = 0;

  let resultStr = "";

  while (counter1 < arr1.length && counter2 < arr2.length) {
    const first = arr1[counter1];
    const second = arr2[counter2];
    if (first.name !== second.name) {
      if (
        getContractName(first.name).localeCompare(
          getContractName(second.name)
        ) <= 0
      ) {
        counter1++;
        resultStr +=
          "\n" +
          first.name +
          " is new on " +
          rpc1 +
          " in contrast to " +
          rpc2 +
          "\n";
      } else {
        counter2++;
        resultStr +=
          "\n" +
          second.name +
          " is new on " +
          rpc2 +
          " in contrast to " +
          rpc1 +
          "\n";
      }
      continue;
    }

    if (
      first.bytecode !== second.bytecode ||
      (includeMeta && first.bytecodeRaw !== second.bytecodeRaw)
    ) {
      resultStr += "\nDiff!!!\n";
      resultStr += "On " + rpc1 + "\n";
      resultStr += JSON.stringify(first, null, 2);
      resultStr += "\nOn " + rpc2 + "\n";
      resultStr += JSON.stringify(second, null, 2) + "\n";
    }

    counter1++;
    counter2++;
  }

  if (counter1 === arr1.length) {
    resultStr += "\nAll elements for " + rpc1 + " checked\n";
  } else {
    while (counter1 < arr1.length) {
      resultStr +=
        "\n" +
        arr1[counter1].name +
        " is new on " +
        rpc1 +
        " in contrast to " +
        rpc2 +
        "\n";
      counter1++;
    }
    resultStr += "\nAll elements for " + rpc1 + " checked with appending\n";
  }

  if (counter2 === arr2.length) {
    resultStr += "\nAll elements for " + rpc2 + " checked\n";
  } else {
    while (counter2 < arr2.length) {
      resultStr +=
        "\n" +
        arr2[counter2].name +
        " is new on " +
        rpc2 +
        " in contrast to " +
        rpc1 +
        "\n";
      counter2++;
    }
    resultStr += "\nAll elements for " + rpc2 + " checked with appending\n";
  }

  const meta = includeMeta ? ".withMeta" : ".withoutMeta";
  const fileName =
    "Compare_" + rpc1.split("/")[2] + "_and_" + rpc2.split("/")[2] + meta;
  writeToFile(resultStr, "./bytecodes/", fileName, ".txt", isRaw);
}

const args = process.argv.slice(5);
const isRaw = process.argv[2] === "true";
const includeMetadata = process.argv[3] === "true";
const saveNetBytecode = process.argv[4] === "true";

if (whitelist.includes(args[0]) && whitelist.includes(args[1])) {
  compareBytecodeOfTwoNetworks(args[0], args[1], includeMetadata).catch(
    console.log
  );
} else {
  console.log("Invalid rpc endpoint!");
}
if (!saveNetBytecode) {
} else {
  parseBytecodeToCSV([args[0], args[1]], isRaw).catch(console.log);
}
