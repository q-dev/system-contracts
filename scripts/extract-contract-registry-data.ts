import { promises as fs } from "fs";

import { CONTRACT_REGISTRY_ADDRESS, whitelist } from "./constants";

export async function parseContractList() {
  const contractRegistryEntries = require("./contract-registry-entries.ts");

  for (const arg of whitelist) {
    const resArr = await getRegistryContractsFromRPC(arg);
    for (const line of resArr) {
      contractRegistryEntries[line[0]] = "";
    }
  }

  try {
    await fs.writeFile(
      "./_contractRegistryEntries.json",
      JSON.stringify(contractRegistryEntries, null, 2)
    );
    console.log(`Saved: contractRegistryEntries!`);
  } catch (err) {
    // handle the error here
    console.error(err);
  }
}

export async function getRegistryContractsFromRPC(rpcEndpoint: string) {
  const Contract = require("web3-eth-contract");
  Contract.setProvider(rpcEndpoint);

  const Registry = require("../artifacts/contracts/ContractRegistry.sol/ContractRegistry.json");

  const contract = new Contract(Registry.abi, CONTRACT_REGISTRY_ADDRESS);

  return await contract.methods.getContracts().call();
}

// Uncomment the following line and use this script file to create a fresh contract-registry-entries.ts file
// parseContractList().catch(console.error);
