#!/usr/bin/env bash

RAW_BYTECODE=false
COMPARE_NETWORKS=false
INCLUDE_METADATA=false
SAVE_NET_BYTECODE=false

function printHelp {
    echo "Usage: ./bytecode.sh [<flags>]

          Description:
            Extract and compare bytecode from artifacts or networks.
            Files are saved in the "bytecode" directory in the root of the project if the -n flag was specified.

          Flags:
            -h, --help                   Show this help message.
            -r, --raw-bytecode           Extract and save bytecode in raw format (default is hash format).
            -c, --compare-networks       Compare bytecode between all networks. To compare only 2 networks, use "compare-bytecode.sh".
            -m, --compare-with-metadata  Consider bytecode metadata in comparison.
            -n, --save-network-bytecodes Save bytecode from the network in a separate file."
}

function parseArgs {
    while [[ -n "$1" ]]
    do
        case "$1" in
            -h | --help)
                printHelp && exit 0
                ;;
            -r | --raw-bytecode) shift
                RAW_BYTECODE=true
                ;;
            -c | --compare-networks) shift
                COMPARE_NETWORKS=true
                ;;
            -m | --compare-with-metadata) shift
                INCLUDE_METADATA=true
                ;;
            -n | --save-network-bytecodes) shift
                SAVE_NET_BYTECODE=true
                ;;
            *)
                echo "invalid flag: $1" && exit 1
                ;;
        esac
    done
}

parseArgs "$@"

mkdir -p ./bytecodes
mkdir -p ./scripts/hardhat_artifacts
shopt -s globstar
cp ./artifacts/**/*.json ./scripts/hardhat_artifacts/
rm -r ./scripts/hardhat_artifacts/**/*.dbg.json

npx ts-node scripts/extract-bytecode-artifacts.ts ${RAW_BYTECODE}

if [ ${COMPARE_NETWORKS} == true ]
then
  npx ts-node scripts/compare-bytecode-rpc.ts ${RAW_BYTECODE} ${INCLUDE_METADATA} ${SAVE_NET_BYTECODE} https://rpc.q.org http://63.34.190.209:8545
  npx ts-node scripts/compare-bytecode-rpc.ts ${RAW_BYTECODE} ${INCLUDE_METADATA} ${SAVE_NET_BYTECODE} https://rpc.q.org https://rpc.qtestnet.org
  npx ts-node scripts/compare-bytecode-rpc.ts ${RAW_BYTECODE} ${INCLUDE_METADATA} ${SAVE_NET_BYTECODE} https://rpc.qtestnet.org http://63.34.190.209:8545
fi
echo End!

