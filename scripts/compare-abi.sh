
SAVE_ABI=false
FIRST_NETWORK='---'
SECOND_NETWORK='---'
FIRST_SNAPSHOT='---'
SECOND_SNAPSHOT='---'
MODE=''
FILE_NAME='---'

function printHelp {
    echo "Usage: ./compare-abi.sh [<flags>]
          A script to compare ABI between networks, snapshots, or contracts.
          All files are saved in the "abi" folder.

          Available networks:
            mainnet: https://explorer.q.org/api
            testnet: https://explorer.qtestnet.org/api
            devnet : http://54.73.188.73:8080/api

          Flags:
            -h, --help                                      Show this help message
            -s, --save-abi [filename]                       Save the ABI to a separate file with the provided FILE_NAME.
                                                            Use "_" if no file name is desired.
                                                            If this is the only option specified, a project snapshot will be created.
                                                            (Won't save anything in the last three options)
            --net-net [network] [network]                   Compare two network ABIs, e.g: ./compare-abi.sh --net-net mainnet devnet
            --net-snp [network] [path-to-snapshot]          Compare a network ABI with a snapshot ABI, e.g: ./compare-abi.sh --net-snp mainnet ./snapshot
            --net-prj [network]                             Compare a network ABI with the project, e.g: ./compare-abi.sh --net-prj mainnet
            --snp-snp [path-to-snapshot] [path-to-snapshot] Compare two snapshot ABIs, e.g: ./compare-abi.sh --snp-snp ./snapshot1 ./snapshot2
            --snp-prj [path-to-snapshot]                    Compare a snapshot ABI with the project, e.g: ./compare-abi.sh --snp-prj ./snapshot
            --cnt-cnt [path-to-contract] [path-to-contract] Compare two contract ABIs, e.g: ./compare-abi.sh -s _ --cnt-cnt ../artifacts/contracts/ContractRegistry.sol/ContractRegistry.json ../artifacts/contracts/interfaces/IContractRegistry.sol/IContractRegistry.json"
}

parseNetwork () {
    case "$1" in
        mainnet)
            func_result=https://explorer.q.org/api
            ;;
        devnet)
            func_result=http://54.73.188.73:8080/api
            ;;
        testnet)
            func_result=https://explorer.qtestnet.org/api
            ;;
        *)
            echo "invalid network: $1" && exit 1
            ;;
    esac
}


function parseArgs {
    while [[ -n "$1" ]]
    do
        case "$1" in
            -h | --help)
                printHelp && exit 0
                ;;
            -s | --save-abi) shift
                SAVE_ABI=true
                FILE_NAME=$1
                shift
                ;;
            --net-net) shift
                MODE='net-net'
                parseNetwork $1
                FIRST_NETWORK=$func_result
                shift
                parseNetwork $1
                SECOND_NETWORK=$func_result
                shift
                ;;
            --net-snp) shift
                MODE='net-snp'
                parseNetwork $1
                FIRST_NETWORK=$func_result
                shift
                FIRST_SNAPSHOT=$1
                shift
                ;;
            --net-prj) shift
                MODE='net-prj'
                parseNetwork $1
                FIRST_NETWORK=$func_result
                shift
                ;;
            --snp-snp) shift
                MODE='snp-snp'
                FIRST_SNAPSHOT=$1
                shift
                SECOND_SNAPSHOT=$1
                shift
                ;;
            --snp-prj) shift
                MODE='snp-prj'
                FIRST_SNAPSHOT=$1
                shift
                ;;
            --cnt-cnt) shift
                MODE='cnt-cnt'
                FIRST_SNAPSHOT=$1
                shift
                SECOND_SNAPSHOT=$1
                shift
                ;;
            *)
                echo "invalid flag: $1" && exit 1
                ;;
        esac
    done
}

parseArgs "$@"

mkdir -p ./abi

npx ts-node ./scripts/abi-compare.ts ${SAVE_ABI} ${FILE_NAME} ${MODE} ${FIRST_NETWORK} ${SECOND_NETWORK} ${FIRST_SNAPSHOT} ${SECOND_SNAPSHOT}
