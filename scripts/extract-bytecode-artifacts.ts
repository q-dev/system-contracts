import fs from "fs";

import {
  getBytecodeHash,
  getBytecodeRaw,
  getContractName,
  writeToFile,
} from "./extract-bytecode-utils";

parseBytecodeToCSV();

function parseBytecodeToCSV() {
  const isRaw = process.argv[2] === "true";
  const files = fs.readdirSync("./scripts/hardhat_artifacts");

  const resArr = getBytecodes(files, isRaw)
    .filter((item) => {
      return !(item.bytecode === null || item.bytecode === "");
    })
    .sort((a, b) => {
      return getContractName(a.name).localeCompare(getContractName(b.name));
    });

  writeToFile(resArr, "./bytecodes/", "artifacts", ".csv", isRaw);
}

function getBytecodes(arrayOfContractPath: string[], isRaw: boolean) {
  const result = [];
  const dirPath = "./hardhat_artifacts/";

  for (const contractName of arrayOfContractPath) {
    const contract = require(dirPath + contractName);
    if (contract.deployedBytecode === undefined) {
      continue;
    }

    const resProxyObj = {
      name: contract.contractName,
      bytecode: "",
      bytecodeRaw: "",
    };
    const resImplObj = {
      name: contract.contractName + "-impl",
      bytecode: "",
      bytecodeRaw: "",
    };

    let bytecode;
    if (isRaw) {
      bytecode = getBytecodeRaw("", contract.deployedBytecode);
    } else {
      bytecode = getBytecodeHash("", contract.deployedBytecode);
    }

    resImplObj.bytecode = bytecode[2];
    resImplObj.bytecodeRaw = bytecode[3];

    result.push(resProxyObj, resImplObj);
  }

  return result;
}
