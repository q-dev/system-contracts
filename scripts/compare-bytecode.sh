
RAW_BYTECODE=false
INCLUDE_METADATA=false
SAVE_NET_BYTECODE=false
FIRST_NETWORK='---'
SECOND_NETWORK='---'

function printHelp {
    echo "Usage: ./compare-bytecode.sh [<flags>]

          Description:
            Compares bytecode within two networks and saves the results in the "bytecode" directory (if -n is specified).

          Available Networks:
            mainnet --> https://rpc.q.org
            testnet --> https://rpc.qtestnet.org
            devnet  --> http://63.34.190.209:8545

          Flags:
            -h, --help             Show this help message
            -r, --raw-bytecode     Extract and save bytecode in raw format (default: hash format)
            -m, --compare-metadata Compare bytecode with metadata
            -n, --save-network     Save bytecode from the network to a separate file
            -f, --first [network]  Name of the first network to compare (see Available Networks)
            -s, --second [network] Name of the second network to compare with (see Available Networks)"
}

parseNetwork () {
    case "$1" in
        mainnet)
            func_result=https://rpc.q.org
            ;;
        devnet)
            func_result=http://63.34.190.209:8545
            ;;
        testnet)
            func_result=https://rpc.qtestnet.org
            ;;
        *)
            echo "invalid network: $1" && exit 1
            ;;
    esac
}


function parseArgs {
    while [[ -n "$1" ]]
    do
        case "$1" in
            -h | --help)
                printHelp && exit 0
                ;;
            -r | --raw-bytecode) shift
                RAW_BYTECODE=true
                ;;
            -m | --compare-with-metadata) shift
                INCLUDE_METADATA=true
                ;;
            -n | --save-network-bytecodes) shift
                SAVE_NET_BYTECODE=true
                ;;
            -f | --first) shift
                parseNetwork $1
                FIRST_NETWORK=$func_result
                shift
                ;;
            -s | --second) shift
                parseNetwork $1
                SECOND_NETWORK=$func_result
                shift
                ;;
            *)
                echo "invalid flag: $1" && exit 1
                ;;
        esac
    done
}

parseArgs "$@"

if [[ $FIRST_NETWORK == '---' ]] || [[ $SECOND_NETWORK == '---' ]] || [[ $SECOND_NETWORK == $FIRST_NETWORK ]]
then
  echo Networks are not set properly && printHelp && exit 1
fi

mkdir -p ./bytecodes

npx ts-node scripts/compare-bytecode-rpc.ts ${RAW_BYTECODE} ${INCLUDE_METADATA} ${SAVE_NET_BYTECODE} ${FIRST_NETWORK} ${SECOND_NETWORK}
