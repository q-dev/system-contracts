#!/bin/bash

# Run the git push command and redirect stderr (standard error) to a log file
git commit -m "Update bindings" 2> /dev/null
git push https://$1:$2@gitlab.com/q-dev/system-contract-bindings-go.git

# Check the exit code of the git push command
if [ $? -ne 0 ]; then
  # Handle the failure case here (e.g., display a custom error message)
  echo "Custom error message: Git push failed. Nothing to commit, working tree clean."
  # Optionally, you can also take other actions like sending notifications, etc.
fi
