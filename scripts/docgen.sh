#!/bin/sh

# First argument is the branch name
# Second argument is the directory name

# Checkout to the branch passed as the first argument
rm -rf docs/
git fetch
git checkout $1
git pull

# Install docsify
npm i docsify-cli -g

npm install

# Generate the docs
npx hardhat markup --outdir public/$2

mv public/$2/@dlsl public/$2/dlsl
mv public/$2/@opengsn public/$2/opengsn
mv public/$2/@openzeppelin public/$2/openzeppelin

cp ./README.md public/$2

rm -rf public/$2/_sidebar.md

docsify generate public/$2 --sidebar _sidebar.md

