import hre from "hardhat";

const AddressStorageFactory = hre.artifacts.require("AddressStorageFactory");
const CompoundRateKeeperFactory = hre.artifacts.require(
  "CompoundRateKeeperFactory"
);

const BorrowingCore = hre.artifacts.require("BorrowingCore");
const LiquidationAuction = hre.artifacts.require("LiquidationAuction");
const Saving = hre.artifacts.require("Saving");
const SystemBalance = hre.artifacts.require("SystemBalance");
const SystemDebtAuction = hre.artifacts.require("SystemDebtAuction");
const SystemSurplusAuction = hre.artifacts.require("SystemSurplusAuction");
const TokenBridgeAdminProxy = hre.artifacts.require("TokenBridgeAdminProxy");

const AccountAliases = hre.artifacts.require("AccountAliases");
const WithdrawAddresses = hre.artifacts.require("WithdrawAddresses");

const Constitution = hre.artifacts.require("Constitution");
const ConstitutionVoting = hre.artifacts.require("ConstitutionVoting");
const Roots = hre.artifacts.require("Roots");
const RootNodeSlashingEscrow = hre.artifacts.require("RootNodeSlashingEscrow");
const RootNodesSlashingVoting = hre.artifacts.require(
  "RootNodesSlashingVoting"
);
const RootsVoting = hre.artifacts.require("RootsVoting");
const Validators = hre.artifacts.require("Validators");
const ValidatorSlashingEscrow = hre.artifacts.require(
  "ValidatorSlashingEscrow"
);
const ValidatorsSlashingVoting = hre.artifacts.require(
  "ValidatorsSlashingVoting"
);
const ContractRegistryAddressVoting = hre.artifacts.require(
  "ContractRegistryAddressVoting"
);
const ContractRegistryUpgradeVoting = hre.artifacts.require(
  "ContractRegistryUpgradeVoting"
);
const EmergencyUpdateVoting = hre.artifacts.require("EmergencyUpdateVoting");
const GeneralUpdateVoting = hre.artifacts.require("GeneralUpdateVoting");
const VotingWeightProxy = hre.artifacts.require("VotingWeightProxy");

const DefaultAllocationProxy = hre.artifacts.require("DefaultAllocationProxy");
const SystemReserve = hre.artifacts.require("SystemReserve");
const PushPayments = hre.artifacts.require("PushPayments");
const QHolderRewardPool = hre.artifacts.require("QHolderRewardPool");
const QHolderRewardProxy = hre.artifacts.require("QHolderRewardProxy");
const QVault = hre.artifacts.require("QVault");
const RootNodeRewardProxy = hre.artifacts.require("RootNodeRewardProxy");
const ValidationRewardProxy = hre.artifacts.require("ValidationRewardProxy");
const ValidationRewardPools = hre.artifacts.require("ValidationRewardPools");
const Vesting = hre.artifacts.require("Vesting");

const EPDR_Membership = hre.artifacts.require("EPDR_Membership");
const EPDR_MembershipVoting = hre.artifacts.require("EPDR_MembershipVoting");
const EPDR_Parameters = hre.artifacts.require("EPDR_Parameters");
const EPDR_ParametersVoting = hre.artifacts.require("EPDR_ParametersVoting");
const EPQFI_Membership = hre.artifacts.require("EPQFI_Membership");
const EPQFI_MembershipVoting = hre.artifacts.require("EPQFI_MembershipVoting");
const EPQFI_Parameters = hre.artifacts.require("EPQFI_Parameters");
const EPQFI_ParametersVoting = hre.artifacts.require("EPQFI_ParametersVoting");
const EPRS_Membership = hre.artifacts.require("EPRS_Membership");
const EPRS_MembershipVoting = hre.artifacts.require("EPRS_MembershipVoting");
const EPRS_Parameters = hre.artifacts.require("EPRS_Parameters");
const EPRS_ParametersVoting = hre.artifacts.require("EPRS_ParametersVoting");

const StableCoin = hre.artifacts.require("StableCoin");

export const entries: Record<string, any> = {
  "tokeneconomics.defaultAllocationProxy": DefaultAllocationProxy,
  "common.factory.crKeeper": CompoundRateKeeperFactory,
  "common.factory.addressStorage": AddressStorageFactory,
  "tokeneconomics.validationRewardProxy": ValidationRewardProxy,
  "tokeneconomics.rootNodeRewardProxy": RootNodeRewardProxy,
  "tokeneconomics.qHolderRewardProxy": QHolderRewardProxy,
  "tokeneconomics.qHolderRewardPool": QHolderRewardPool,
  "tokeneconomics.validationRewardPools": ValidationRewardPools,
  "tokeneconomics.qVault": QVault,
  "tokeneconomics.vesting": Vesting,
  "tokeneconomics.pushPayments": PushPayments,
  "defi.tokenBridgeAdminProxy": TokenBridgeAdminProxy,
  "defi.QUSD.coin": StableCoin,
  "defi.QUSD.borrowing": BorrowingCore,
  "defi.QUSD.saving": Saving,
  "defi.QUSD.systemBalance": SystemBalance,
  "defi.QUSD.liquidationAuction": LiquidationAuction,
  "defi.QUSD.systemSurplusAuction": SystemSurplusAuction,
  "defi.QUSD.systemDebtAuction": SystemDebtAuction,
  "governance.constitution.parameters": Constitution,
  "governance.constitution.parametersVoting": ConstitutionVoting,
  "governance.rootNodes": Roots,
  "governance.rootNodes.membershipVoting": RootsVoting,
  "governance.rootNodes.slashingVoting": RootNodesSlashingVoting,
  "governance.rootNodes.slashingEscrow": RootNodeSlashingEscrow,
  "governance.validators": Validators,
  "governance.validators.slashingVoting": ValidatorsSlashingVoting,
  "governance.validators.slashingEscrow": ValidatorSlashingEscrow,
  "governance.experts.EPDR.membership": EPDR_Membership,
  "governance.experts.EPDR.membershipVoting": EPDR_MembershipVoting,
  "governance.experts.EPDR.parameters": EPDR_Parameters,
  "governance.experts.EPDR.parametersVoting": EPDR_ParametersVoting,
  "governance.experts.EPRS.membership": EPRS_Membership,
  "governance.experts.EPRS.membershipVoting": EPRS_MembershipVoting,
  "governance.experts.EPRS.parameters": EPRS_Parameters,
  "governance.experts.EPRS.parametersVoting": EPRS_ParametersVoting,
  "governance.experts.EPQFI.membership": EPQFI_Membership,
  "governance.experts.EPQFI.membershipVoting": EPQFI_MembershipVoting,
  "governance.experts.EPQFI.parameters": EPQFI_Parameters,
  "governance.experts.EPQFI.parametersVoting": EPQFI_ParametersVoting,
  "governance.emergencyUpdateVoting": EmergencyUpdateVoting,
  "governance.generalUpdateVoting": GeneralUpdateVoting,
  "governance.votingWeightProxy": VotingWeightProxy,
  "governance.upgrade.contractRegistryVoting": ContractRegistryUpgradeVoting,
  "governance.address.contractRegistryVoting": ContractRegistryAddressVoting,
  "tokeneconomics.systemReserve": SystemReserve,
  "tokeneconomics.withdrawAddresses": WithdrawAddresses,
  "governance.accountAliases": AccountAliases,
};
