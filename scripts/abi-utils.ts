import Eth from "web3-eth";
import hre from "hardhat";

import fs from "fs";
import path from "path";
import { request } from "undici";
import { normalize } from "@truffle/abi-utils";

import { entries } from "./contract-registry-entries";
import { apiToNetwork, IMPL_STORAGE_SLOT, ZERO_ADDRESS } from "./constants";
import { getRegistryContractsFromRPC } from "./extract-contract-registry-data";

async function getNormalizedABIFromNetwork(
  apiURL: string,
  apiKey: string,
  contractAddress: string
) {
  const requestURL = `${apiURL}?module=contract&action=getabi&address=${contractAddress}&apikey=${apiKey}`;
  const response = await request(requestURL, {
    method: "GET",
  });

  const responseBody: any = await response.body.json();

  if (responseBody["status"] !== "1") {
    throw new Error(
      `Error fetching ABI from Blockscout: ${responseBody["message"]}`
    );
  }

  return normalize(JSON.parse(responseBody["result"]));
}

export async function getNormalizedSnapshotFromNetwork(
  apiURL: string,
  apiKey = "abc"
) {
  const outputData: Record<any, any> = {};
  const web3Eth = new Eth(apiToNetwork[apiURL]);
  const resArr = await getRegistryContractsFromRPC(apiToNetwork[apiURL]);

  for (const line of resArr) {
    if (entries[line[0]] === undefined) {
      continue;
    }

    const contract = entries[line[0]].contractName;
    const implementation = await web3Eth.getStorageAt(
      line[1],
      IMPL_STORAGE_SLOT
    );
    const contractAddress =
      implementation === ZERO_ADDRESS
        ? line[1]
        : "0x" + implementation.slice(-40);

    try {
      outputData[contract] = await getNormalizedABIFromNetwork(
        apiURL,
        apiKey,
        contractAddress
      );
    } catch (e: any) {
      console.log(
        `Failed to get ABI from ${contract} contract on the next network ${apiToNetwork[apiURL]}`
      );
      console.log(e.message);
      outputData[contract] = [];
    }
  }

  return outputData;
}

export async function getNormalizedSnapshotFromProject() {
  const fullNames = await hre.artifacts.getAllFullyQualifiedNames();
  const outputData: Record<any, any> = {};

  for (const fullName of fullNames) {
    const { abi, contractName } = await hre.artifacts.readArtifact(fullName);
    outputData[contractName] = normalize(abi);
  }

  return outputData;
}

export async function saveSnapshot(
  path: string,
  fileName: string,
  data: string
) {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true });
  }

  const saveFilePath = resolvePathToFile(`${path}/abi`, `${fileName}.json`);

  fs.writeFile(saveFilePath, JSON.stringify(data, null, 2), function (err) {
    if (err) throw err;
    console.log(`Saved: ${saveFilePath}!`);
  });
}

// First argument should always be remote ABI mapping
export function deepEqualsAbi(
  object1: Record<any, any>,
  object2: Record<any, any>
) {
  const keys1 = Object.keys(object1);
  const table = [];

  for (const key of keys1) {
    // @ts-ignore
    if (object2[key] === [] || object2[key] === undefined) {
      console.log(`Could not find ABI for ${key} in the second snapshot`);
      continue;
    }

    const firstMappedABI = createMappingFromABI(object1[key]);
    const secondMappedABI = createMappingFromABI(object2[key]);

    if (
      Object.keys(firstMappedABI).length > 0 &&
      Object.keys(secondMappedABI).length > 0
    ) {
      const result = deepCompareMappedABIs(firstMappedABI, secondMappedABI);

      if (result.size > 0) {
        table.push({ Contract: key, Changes: new Array(...result).join(" ") });
      }
    } else if (
      Object.keys(secondMappedABI).length !== Object.keys(firstMappedABI).length
    ) {
      table.push({
        Contract: key,
        Changes: "Failed to find ABI on one of the network(s)",
      });
    }
  }

  if (table.length > 0) {
    console.table(table);
    console.log();
  } else {
    console.log("Nothing changed");
  }
}

const VALUE_UNCHANGED = "unchanged";
const VALUE_UPDATED = "updated";

export function deepCompareMappedABIs(obj1: any, obj2: any) {
  const diff: any = new Set();
  for (const key of Object.keys(obj1)) {
    const res = deepCompareABIs(obj1[key], obj2[key]);
    if (res === VALUE_UNCHANGED) {
      continue;
    }

    diff.add(key);
  }

  for (const key of Object.keys(obj2)) {
    if (diff[key] !== undefined) {
      continue;
    }

    const res = deepCompareABIs(obj1[key], obj2[key]);
    if (res === VALUE_UNCHANGED) {
      continue;
    }

    diff.add(key);
  }

  return diff;
}

function deepCompareABIs(obj1: any, obj2: any) {
  if (isValue(obj1) || isValue(obj2)) {
    const type = compareValues(obj1, obj2);

    if (type === VALUE_UNCHANGED) {
      return VALUE_UNCHANGED;
    }

    return {
      type: type,
      data: obj1 === undefined ? obj2 : obj1,
    };
  }

  const diff: any = {};
  for (const key of Object.keys(obj1)) {
    const res = deepCompareABIs(obj1[key], obj2[key]);
    if (res === VALUE_UNCHANGED) {
      continue;
    }

    diff[key] = res;
  }

  for (const key of Object.keys(obj2)) {
    if (diff[key] !== undefined) {
      continue;
    }

    const res = deepCompareABIs(obj1[key], obj2[key]);
    if (res === VALUE_UNCHANGED) {
      continue;
    }

    diff[key] = res;
  }

  return Object.keys(diff).length === 0 ? VALUE_UNCHANGED : diff;
}

function compareValues(value1: any, value2: any) {
  if (value1 === value2) {
    return VALUE_UNCHANGED;
  }

  return VALUE_UPDATED;
}

function isArray(x: any) {
  return Object.prototype.toString.call(x) === "[object Array]";
}

function isObject(x: any) {
  return Object.prototype.toString.call(x) === "[object Object]";
}

function isValue(x: any) {
  return !isObject(x) && !isArray(x);
}

function createMappingFromABI(abi: any) {
  const mapping: any = {};
  for (const item of abi) {
    mapping[item.name] = item;
  }

  return mapping;
}

export function resolvePathToFile(path_: string, file_ = "") {
  return path.normalize(fs.realpathSync(path_) + "/" + file_);
}
