export const IMPL_STORAGE_SLOT =
  "0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc";
export const ZERO_ADDRESS =
  "0x0000000000000000000000000000000000000000000000000000000000000000";

export const apiToNetwork: Record<string, string> = {
  "https://explorer.q.org/api": "https://rpc.q.org",
  "http://54.73.188.73:8080/api": "http://63.34.190.209:8545",
  "https://explorer.qtestnet.org/api": "https://rpc.qtestnet.org",
};

export const whitelist = [
  "https://rpc.q.org",
  "http://63.34.190.209:8545",
  "https://rpc.qtestnet.org",
];

export const CONTRACT_REGISTRY_ADDRESS =
  "0xc3E589056Ece16BCB88c6f9318e9a7343b663522";
